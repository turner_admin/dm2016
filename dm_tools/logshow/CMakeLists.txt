cmake_minimum_required(VERSION 3.10)
cmake_policy(SET CMP0074 NEW)
find_package(Boost REQUIRED COMPONENTS program_options)

include_directories(${Boost_INCLUDE_DIRS})

link_directories(${LIBRARY_OUTPUT_PATH})

aux_source_directory(src SRC)

add_executable(logshow ${SRC})

target_link_libraries(logshow oslog dmos string misc env tinyxml json ${Boost_LIBRARIES})

if( NOT WIN32 )
    target_link_libraries(logshow rt)
endif()

install(TARGETS logshow RUNTIME)
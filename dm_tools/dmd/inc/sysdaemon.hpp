﻿/*
 * sysdaemon.hpp
 *
 *  Created on: 2018年2月14日
 *      Author: work
 */

#ifndef DM_TOOLS_DMD_INC_SYSDAEMON_HPP_
#define DM_TOOLS_DMD_INC_SYSDAEMON_HPP_

#include <boost/process.hpp>
#include <dm/os/sys/task_record.hpp>

/**
 * 平台守护类
 */
class CSysDaemon{
public:
	CSysDaemon( const bool& console=false,const bool& nd=false );
	~CSysDaemon();

	inline void setExecInterval( long interval ){
		m_execInterval = interval;
	}
	
	bool runOnce();

protected:
	bool exec( dm::os::sys::STaskInfo& task );

protected:
	long m_execInterval;	// 启动间隔 毫秒
	bool m_me;
	bool m_console;
	bool m_nd;	// 非后台

	boost::process::pid_t m_pid;

	boost::process::group m_children;	// 子进程
};

#endif /* DM_TOOLS_DMD_INC_SYSDAEMON_HPP_ */

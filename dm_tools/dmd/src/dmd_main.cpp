﻿/*
 * dmd_main.cpp
 *
 *  Created on: 2018年2月14日
 *      Author: work
 */

#include <dm/os/console.hpp>
#include <boost/program_options.hpp>
#include <iostream>
#include "sysdaemon.hpp"

using namespace std;
using namespace boost::program_options;

int main( int argc,char* argv[] ){
	dm::os::consoleUtf8();
	options_description desc("【DM2016帮助文档】\n DM守护进程。\n\t基于DM2016系统构建\n支持以下选项");

	options_description opts_generic("一般选项");
	opts_generic.add_options()
			("help,h","显示本帮助信息")
			("version,v","显示版本及编译信息")
			;
	desc.add(opts_generic);

	options_description opts_run("运行选项");
	opts_run.add_options()
			("interval,i",value<long>()->default_value(500),"扫描周期：毫秒")
			("exe-interval",value<long>()->default_value(1000l),"程序启动最小间隔:毫秒")
			("console,c","控制台输出")
			("debug,d","调试模式")
			;

	desc.add(opts_run);

	variables_map vm;
	try{
		store(parse_command_line(argc,argv,desc),vm);
		notify(vm);
	}catch( std::exception& ex ){
		cout << ex.what()<<endl;
		return 1;
	}

	if( vm.count("version") ){
		cout <<"V1.0 构建于 "<<__DATE__<<endl;
		return 1;
	}

	if( vm.count("help") ){
		cout <<desc<<endl;
		return 1;
	}

	CSysDaemon daemon(vm.count("console"),vm.count("debug"));

	daemon.setExecInterval( vm["exe-interval"].as<long>());

	long long interval = vm["interval"].as<long>();

	interval *= 1000;
	while( daemon.runOnce() ){
#ifdef WIN32
		Sleep(interval);
#else
		usleep(interval);
#endif
	}

	return 0;
}

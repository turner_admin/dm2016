﻿/*
 * dm104slaver.h
 *
 *  Created on: 2017年5月14日
 *      Author: work
 */

#ifndef APPS_SLAVER104_INC_DM104SLAVER_H_
#define APPS_SLAVER104_INC_DM104SLAVER_H_

#include <dm/os/protocol/iec104slaver.hpp>

#include <dm/scada/eventmonitor.hpp>
#include <dm/scada/timedmeasuremonitor.hpp>
#include <dm/scada/timedcumulantmonitor.hpp>

/**
 * 104从站规约
 * 将本地数据全部上传
 */
class CDm104Slaver:public dm::os::protocol::CIec104Slaver{
public:
	/**
	 * 总召数据上传步骤
	 */
	enum ECallStep{
		CS_Status,  //!< CS_Status
		CS_Discrete,//!< CS_Discrete
		CS_Measure, //!< CS_Measure
		CS_Cumulant, //!< CS_Cumulant
		 CS_All
	};

	CDm104Slaver();

	const char* name()const;

protected:
	EAction taskStart_call( const ts_t& ts,const rts_t& rts );
	EAction taskDo_call( const ts_t& ts,const rts_t& rts );
	EAction taskEnd_call( const ts_t& ts,const rts_t& rts );

	EAction task_none( const ts_t& ts,const rts_t& rts );

	// 通过双点信息发送状态量信息
	EAction taskDo_call_3_M_DP_NA_1( frame_apdu_t& tf,const ts_t& ts,const rts_t& rts );

	// 通过标度化值发送离散量信息
	EAction taskDo_call_11_M_ME_NB_1( frame_apdu_t& tf,const ts_t& ts,const rts_t& rts );

	// 通过短浮点数发送测量值信息
	EAction taskDo_call_13_M_ME_NC_1( frame_apdu_t& tf,const ts_t& ts,const rts_t& rts );

	// 通过累计量发送累计量信息
	EAction taskDo_call_15_M_IT_NA_1( frame_apdu_t& tf,const ts_t& ts,const rts_t& rts );

	// 主动上报
	EAction task_report( frame_apdu_t& tf,const ts_t& ts,const rts_t& rts );
private:
	ECallStep m_callStep;
	int m_callTmpIndex;	// 上传的数据索引号

	dm::scada::CEventMonitor m_em;
	dm::scada::CTimedMeasureMonitor m_mm;
	dm::scada::CTimedCumulantMonitor m_cm;
};

#endif /* APPS_SLAVER104_INC_DM104SLAVER_H_ */

﻿/*
 * eventmonitor.cpp
 *
 *  Created on: 2017年3月27日
 *      Author: work
 */

#include <boost/program_options.hpp>
#include <dm/scada/timedcumulantmonitor.hpp>
#include <dm/os/console.hpp>
#include <string>
#include <iostream>

using namespace std;
using namespace boost::program_options;
using namespace dm::scada;

int main( int argc,char* argv[] ){
	dm::os::consoleUtf8();
	options_description desc("DM系统时标累计量工具.");
	desc.add_options()
			("help,h","显示帮助信息.")
			("version,v","显示版本信息.")
			;

	variables_map vm;
	try{
		store(parse_command_line(argc,argv,desc),vm);
		notify(vm);
	}catch( std::exception& ex ){
		cout << ex.what()<<endl;
		return 1;
	}

	if( vm.count("version") ){
		cout <<"build on "<<__DATE__<<endl;
		return 1;
	}

	if( vm.count("help") ){
		cout <<desc<<endl;
		return 1;
	}

	CTimedCumulantMonitor em;

	em.run();

	return 0;
}



cmake_minimum_required(VERSION 3.10)

cmake_policy(SET CMP0074 NEW)
find_package(Boost REQUIRED COMPONENTS program_options date_time)

include_directories(${Boost_INCLUDE_DIRS})

link_directories(${LIBRARY_OUTPUT_PATH})

aux_source_directory(src SRC)

add_executable(scadashow ${SRC})

DB_SELECT(scadashow)

if( WIN32 )
    target_link_libraries(scadashow scada oslog osmsg dmos osdb options string misc env tinyxml json ${Boost_LIBRARIES})
else()
    target_link_libraries(scadashow scada oslog osmsg dmos osdb options string misc env tinyxml json ${Boost_LIBRARIES} rt)
endif()

install(TARGETS scadashow RUNTIME)
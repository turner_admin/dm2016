/*
 * session_tool.cpp
 *
 *  Created on: 2019-1-12
 *      Author: work
 */

#include <iostream>
#include <boost/program_options.hpp>
#include <dm/session/sessionmgr.hpp>
#include <dm/datetime.hpp>

using namespace std;
using namespace boost::program_options;

void showSession( dm::session::CSession* s ){
	cout <<" 用户ID:"<<s->userId()
			<<" 魔数:"<<s->mod()
			<<" 有效期至:"<<dm::CDateTime(s->deadline())
			<<endl;
}

int main( int argc,char* argv[] ){
	options_description desc("【DM2016帮助文档】\n 会话管理工具。\n\t基于DM2016系统构建\n支持以下选项");

	options_description opts_generic("一般选项");
	opts_generic.add_options()
			("help,h","显示本帮助信息")
			("version,v","显示版本及编译信息")
			;
	desc.add(opts_generic);

	options_description opts_cmd("命令选项");
	opts_cmd.add_options()
			("reset","复位会话系统")
			("show",value<string>(),"查看会话信息.\n all:所有项目\n session: 会话信息\n rts: 实时监控器\n tds: 时标数据监视器\n logs: 日志监视器")
			("add",value<string>(),"申请新资源.\n session: 注册会话\n rts:注册会话的实时监视器")
			("del",value<string>(),"删除资源")
			("update",value<string>(),"更新资源")
			;
	desc.add(opts_cmd);

	options_description opts_para("辅助参数");
	opts_para.add_options()
			("user-id",value<int>(),"指定用户id")
			("session",value<unsigned long>(),"指定会话魔数")
			("rts",value<unsigned long>(),"指定实时监视器魔数")
			("tds",value<unsigned long>(),"指定时标监视器魔数")
			("logs",value<unsigned long>(),"指定日志监视器魔数")
			;

	desc.add(opts_para);

	variables_map vm;
	try{
		store(parse_command_line(argc,argv,desc),vm);
		notify(vm);
	}catch( std::exception& ex ){
		cout << ex.what()<<endl;
		return 1;
	}

	if( vm.count("version") ){
		cout <<"V1.0 构建于 "<<__DATE__<<endl;
		return 1;
	}

	if( vm.count("help") ){
		cout <<desc<<endl;
		return 1;
	}

	dm::session::CSessionMgr* sm = dm::session::CSessionMgr::ins();
	dm::CTimeStamp n = dm::CTimeStamp::cur();

	if( vm.count("reset") ){
		cout <<"复位会话数据。确定(Y):";
		cout.flush();
		if( cin.get()=='Y' ){
			if( sm->reset() )
				cout <<"复位成功"<<endl;
			else
				cout <<"复位失败"<<endl;
		}

		return 1;
	}

	if( vm.count("show") ){
		if( vm["show"].as<string>()=="session" || vm["show"].as<string>()=="all" ){
			cout <<"== 会话信息 =="
					<<"\n容量："<<sm->Size;
			for( int i=0;i<sm->Size;++i ){
				cout <<"\n"<<i+1<<"> ";
				if( sm->sessions()[i].isIdle(n) )
					cout <<"空闲";
				else{
					cout <<"使用"
							<<" user:"<<sm->sessions()[i].userId()
							<<" mod:"<<sm->sessions()[i].mod()
							<<" deadline:"<<dm::CDateTime(sm->sessions()[i].deadline()).toString();
				}
			}
		}

		cout <<endl<<"== 实时监视器信息 =="
				<<"\n容量:"<<sm->rtsMgr()->Size;

		cout <<endl;
		return 0;
	}

	if( vm.count("add")){
		string add = vm["add"].as<string>();

		if( !vm.count("user-id") ){
			cout <<"需要使用--user-id指定用户ID"<<endl;
			return 1;
		}

		if( add=="session" ){
			dm::session::CSession* s = sm->getNew(vm["user-id"].as<int>());
			if( s ){
				cout <<"添加会话成功"
						<<"\n 魔数:"<<s->mod()
						<<"\n 有效期:"<<dm::CDateTime(s->deadline()).toString()
						<<endl;
			}
		}

		return 0;
	}

	if( vm.count("update") ){
		string update = vm["update"].as<string>();

		if( !vm.count("user-id") ){
			cout <<"需要使用--user-id指定用户ID"<<endl;
			return 1;
		}

		if(!vm.count("session") ){
			cout <<"需要使用--session指定会话魔数"<<endl;
			return 1;
		}

		if( update=="session" ){
			dm::session::CSession* s = sm->get(vm["user-id"].as<int>(),vm["session"].as<unsigned long>());
			if( s ){
				cout <<"会话更新成功"<<endl;
				return 1;
			}else{
				cout <<"不存在会话 用户ID:"<<vm["user-id"].as<int>()<<" 会话魔数:"<<vm["session"].as<unsigned long>()<<endl;
				return 1;
			}
		}
	}

	return 0;
}

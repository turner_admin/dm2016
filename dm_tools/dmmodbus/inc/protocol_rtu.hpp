﻿/*
 * protocol_rtu.hpp
 *
 *  Created on: 2019-4-15
 *      Author: work
 */

#ifndef DM_TOOLS_DMMODBUS_INC_PROTOCOL_RTU_HPP_
#define DM_TOOLS_DMMODBUS_INC_PROTOCOL_RTU_HPP_

#include <dm/os/protocol/modbusrtumaster.hpp>
#include <dm/protocol/modbusmap.hpp>

class CProtocolRtu:public dm::os::protocol::CModbusRtuMaster{
public:
	CProtocolRtu( dm::protocol::CModbusMap* modbusMap );
	~CProtocolRtu();

	const char* name()const;

protected:

	EAction dealRxResponse_readCoils( const dm::protocol::CFrameModbusRtu& rf,dm::protocol::CFrameModbusRtu& tf );
	EAction dealRxResponse_readDiscreteInputs( const dm::protocol::CFrameModbusRtu& rf,dm::protocol::CFrameModbusRtu& tf );
	EAction dealRxResponse_readHoldingRegisters( const dm::protocol::CFrameModbusRtu& rf,dm::protocol::CFrameModbusRtu& tf );
	EAction dealRxResponse_readInputRegisters( const dm::protocol::CFrameModbusRtu& rf,dm::protocol::CFrameModbusRtu& tf );

	EAction taskStart_call( frame_modbus_t& tf );
	EAction taskDo_call( frame_modbus_t& tf );

	EAction taskStart_remoteControl( frame_modbus_t& tf );

	EAction taskStart_parameters( frame_modbus_t& tf );

	EAction taskDo_remoteControl( frame_modbus_t& tf );
	EAction taskDo_parameters( frame_modbus_t& tf );

	EAction task_none( frame_t* txFrame );

private:
	dm::protocol::CModbusMap* m_map;
};

#endif /* DM_TOOLS_DMMODBUS_INC_PROTOCOL_RTU_HPP_ */

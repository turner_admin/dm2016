﻿/*
 * protocol_tcp.hpp
 *
 *  Created on: 2019年3月9日
 *      Author: Dylan.Gao
 */

#ifndef INC_PROTOCOL_TCP_HPP_
#define INC_PROTOCOL_TCP_HPP_

#include <dm/os/protocol/modbustcpmaster.hpp>
#include <dm/protocol/modbusmap.hpp>

class CProtocolTcp:public dm::os::protocol::CModbusTcpMaster{
public:
	CProtocolTcp(dm::protocol::CModbusMap* modbusMap );

	const char* name()const;
	bool setDev( const char* name );

	inline void setDebug( bool s=true ){
		m_debug = s;
	}

	void reset( const ts_t& ts,const rts_t& rts );

protected:
	EAction dealRxResponse_readCoils( const dm::protocol::CFrameModbusTcp& rf,dm::protocol::CFrameModbusTcp& tf,const ts_t& ts,const rts_t& rts );
	EAction dealRxResponse_readDiscreteInputs( const dm::protocol::CFrameModbusTcp& rf,dm::protocol::CFrameModbusTcp& tf,const ts_t& ts,const rts_t& rts );
	EAction dealRxResponse_readHoldingRegisters( const dm::protocol::CFrameModbusTcp& rf,dm::protocol::CFrameModbusTcp& tf,const ts_t& ts,const rts_t& rts );
	EAction dealRxResponse_readInputRegisters( const dm::protocol::CFrameModbusTcp& rf,dm::protocol::CFrameModbusTcp& tf,const ts_t& ts,const rts_t& rts );

	EAction taskStart_call( frame_modbus_t& tf,const ts_t& ts,const rts_t& rts );
	EAction taskDo_call( frame_modbus_t& tf,const ts_t& ts,const rts_t& rts );

	EAction taskStart_remoteControl( frame_modbus_t& tf,const ts_t& ts,const rts_t& rts );
	EAction taskStart_parameters( frame_modbus_t& tf,const ts_t& ts,const rts_t& rts );

	EAction task_none( const ts_t& ts,const rts_t& rts );
private:
	bool m_debug;	// 是否输出详细调试信息
	dm::protocol::CModbusMap* m_map;
};

#endif

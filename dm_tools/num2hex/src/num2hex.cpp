/*
 * msgshow.cpp
 *
 *  Created on: 2016年11月13日
 *      Author: work
 */

#include <boost/program_options.hpp>

#include <dm/types.hpp>
#include <vector>
#include <string>
#include <cstdio>
#include <iostream>

#ifndef WIN32
#include <unistd.h>
#endif

using namespace std;
using namespace boost::program_options;

void showHex( const dm::uint8* b,int size ){
	for( int i=0;i<size;++i )
		printf(" %02X",b[i]);
}

int main( int argc,char* argv[] ){
	cout <<"DM2016 system check tool"<<endl;
	options_description desc("DM2016 system check tool.");
	desc.add_options()
			("help,h","显示帮助信息.")
			("version,v","显示版本信息.")
			("int16",value<vector<dm::int16> >(),"Int16格式数据")
			("uint16",value<vector<dm::uint16> >(),"Uint16格式数据")
			("int32",value<vector<dm::int32> >(),"Int32格式数据")
			("uint32",value<vector<dm::uint32> >(),"Uint32格式数据")
			("int64",value<vector<dm::int64> >(),"Int64格式数据")
			("uint64",value<vector<dm::uint64> >(),"Uint64格式数据")
			("float32",value<vector<dm::float32> >(),"Float32格式数据")
			("float64",value<vector<dm::float64> >(),"Float64格式数据")
			;

	variables_map vm;
	try{
		store(parse_command_line(argc,argv,desc),vm);
		notify(vm);
	}catch( std::exception& ex ){
		cout << ex.what()<<endl;
		return 1;
	}

	if( vm.count("version") ){
		cout <<"build on "<<__DATE__<<endl;
		return 1;
	}

	if( vm.count("help") ){
		cout <<desc<<endl;
		return 1;
	}

	if( vm.count("int16") ){
		vector<dm::int16> d = vm["int16"].as<vector<dm::int16> >();
		cout <<"int16 => hex:"<<endl;
		union{
			dm::int16 v;
			dm::uint8 byte[2];
		}u;

		for( unsigned int i=0;i<d.size();++i ){
			u.v = d[i];
			cout <<u.v<<':';
			showHex(u.byte,2);
			cout <<endl;
		}
	}

	if( vm.count("uint16") ){
		vector<dm::uint16> d = vm["uint16"].as<vector<dm::uint16> >();
		cout <<"uint16 => hex:"<<endl;
		union{
			dm::uint16 v;
			dm::uint8 byte[2];
		}u;

		for( unsigned int i=0;i<d.size();++i ){
			u.v = d[i];
			cout <<u.v<<':';
			showHex(u.byte,2);
			cout <<endl;
		}
	}

	if( vm.count("int32") ){
		vector<dm::int32> d = vm["int32"].as<vector<dm::int32> >();
		cout <<"int32 => hex:"<<endl;
		union{
			dm::int32 v;
			dm::uint8 byte[4];
		}u;

		for( unsigned int i=0;i<d.size();++i ){
			u.v = d[i];
			cout <<u.v<<':';
			showHex(u.byte,4);
			cout <<endl;
		}
	}

	if( vm.count("uint32") ){
		vector<dm::uint32> d = vm["uint32"].as<vector<dm::uint32> >();
		cout <<"uint32 => hex:"<<endl;
		union{
			dm::uint32 v;
			dm::uint8 byte[4];
		}u;

		for( unsigned int i=0;i<d.size();++i ){
			u.v = d[i];
			cout <<u.v<<':';
			showHex(u.byte,4);
			cout <<endl;
		}
	}

	if( vm.count("float32") ){
		vector<dm::float32> d = vm["float32"].as<vector<dm::float32> >();
		cout <<"float32 => hex:"<<endl;
		union{
			dm::float32 v;
			dm::uint8 byte[4];
		}u;

		for( unsigned int i=0;i<d.size();++i ){
			u.v = d[i];
			cout <<u.v<<':';
			showHex(u.byte,4);
			cout <<endl;
		}
	}

	if( vm.count("int64") ){
		vector<dm::int64> d = vm["int64"].as<vector<dm::int64> >();
		cout <<"int64 => hex:"<<endl;
		union{
			dm::int64 v;
			dm::uint8 byte[8];
		}u;

		for( unsigned int i=0;i<d.size();++i ){
			u.v = d[i];
			cout <<u.v<<':';
			showHex(u.byte,8);
			cout <<endl;
		}
	}

	if( vm.count("uint64") ){
		vector<dm::uint64> d = vm["uint64"].as<vector<dm::uint64> >();
		cout <<"uint64 => hex:"<<endl;
		union{
			dm::uint64 v;
			dm::uint8 byte[8];
		}u;

		for( unsigned int i=0;i<d.size();++i ){
			u.v = d[i];
			cout <<u.v<<':';
			showHex(u.byte,8);
			cout <<endl;
		}
	}

	if( vm.count("float64") ){
		vector<dm::float64> d = vm["float64"].as<vector<dm::float64> >();
		cout <<"float64 => hex:"<<endl;
		union{
			dm::float64 v;
			dm::uint8 byte[8];
		}u;

		for( unsigned int i=0;i<d.size();++i ){
			u.v = d[i];
			cout <<u.v<<':';
			showHex(u.byte,8);
			cout <<endl;
		}
	}

	return 0;
}



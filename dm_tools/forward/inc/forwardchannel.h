/*
 * forwardchannel.h
 *
 *  Created on: 2013-6-5
 *      Author: dylan
 */

#ifndef FORWARDCHANNEL_H_
#define FORWARDCHANNEL_H_

#include "iodevice.h"
#include "tcpclient.h"
#include "udpdevice.h"
#include "comdevice.h"
#include "ioselector.h"
#include "ioaddress.h"
#include "systhread.h"

class CForwardChannel{
public:
	CForwardChannel();
	~CForwardChannel();

	bool setIncome( CIoDevice* io );
	bool setOutcomeAddr( const CIoAddress& addr );
	bool setTimeout( const long& sec );

	bool start();
	bool stop();
	bool ifStart();

	void run();

protected:
	bool connectOutcome();
	bool readWrite( CIoDevice* read,CIoDevice* write );

private:
	CIoDevice* m_income;
	CIoDevice* m_outcome;

	CSysThread* m_thread;

	CIoAddress * m_addr;
	long m_timeout;
};

#endif /* FORWARDCHANNEL_H_ */

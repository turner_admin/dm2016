cmake_minimum_required(VERSION 3.10)

cmake_policy(SET CMP0074 NEW)
find_package(Boost COMPONENTS program_options date_time)

include_directories(${Boost_INCLUDE_DIRS})

link_directories(${LIBRARY_OUTPUT_PATH})

aux_source_directory(src SRC)

add_executable(rtdata ${SRC})

DB_SELECT(rtdata)

if( Boost_FOUND )
    if( WIN32 )
        target_link_libraries(rtdata scada oslog dmos options osdb string misc env tinyxml json ${Boost_LIBRARIES})
    else()
        target_link_libraries(rtdata scada oslog dmos options osdb string misc env tinyxml json ${Boost_LIBRARIES} rt)
    endif()
else()
    if( WIN32 )
        target_link_libraries(rtdata scada oslog dmos options osdb string misc env tinyxml json boost_program_options)
    else()
        target_link_libraries(rtdata scada oslog dmos options osdb string misc env tinyxml json boost_program_options rt)
    endif()
endif()

install(TARGETS rtdata RUNTIME)
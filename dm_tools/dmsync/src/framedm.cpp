﻿/*
 * framedm.cpp
 *
 *  Created on: 2017年4月11日
 *      Author: work
 */

#include "framedm.h"
#include <dm/os/log/logger.hpp>

static const char* logModule = "CFrameDm.dmsync.app.dm";

bool CFrameDm::setTest(){
	m_fun = Fun_Test;
	m_len = 0;
	return true;
}

bool CFrameDm::getTest()const{
	if( m_fun==Fun_Test ){
		if( m_len!=0 ){
			log().warnning(THISMODULE "数据长度不为0");
			return false;
		}

		return true;
	}
	return false;
}

bool CFrameDm::setTestAck( const bool& ack ){
	m_fun = Fun_TestAck;
	m_len = 1;
	m_data[0] = ack?1:0;
	return true;
}

bool CFrameDm::getTestAck( bool& ack )const{
	if( m_fun==Fun_TestAck ){
		if( m_len!=1 ){
			log().warnning(THISMODULE "数据长度不为1");
			return false;
		}

		ack = (m_data[0]!=0);
		return true;
	}

	return false;
}

bool CFrameDm::setData_status( const dm::uint32& start,const dm::uint16& count,const rt_status_t* rts ){
	dm::uint16 rc = setData_status(start,count);

	for( dm::uint16 i=0;i<rc;++i )
		setData_status(i,rts+i);

	return true;
}

bool CFrameDm::getData_status( dm::uint32& start,dm::uint16& count )const{
	if( m_len<6 ){
		log().warnning(THISMODULE "len(%d)<6",m_len);
		return false;
	}

	start = dm::bytes2uint32( m_data[0],m_data[1],m_data[2],m_data[3] );
	count = dm::bytes2uint16( m_data[4],m_data[5] );

	if( m_len!=6+(count*2) ){
		log().warnning(THISMODULE "len(%d)!=%d",m_len,6+(count*2));
		return false;
	}
	return true;
}

bool CFrameDm::getData_status( const dm::uint16& i,dm::scada::CStatus& value )const{
	int p = i*2;
	if( p+2+6>m_len ){
		log().warnning(THISMODULE "索引%d越限,len=%d",i,m_len);
		return false;
	}

	value.setValue(m_data[6+i*2]);
	value.setRaw(m_data[7+i*2]);

	return true;
}

dm::uint16 CFrameDm::setData_status( const dm::uint32& start,const dm::uint16& count ){
	dm::uint16 rc = (getBufMaxSize()-6)/2;	// 前6个字节对应start和count,每2字节代表一个状态量
	if( count < rc )
		rc = count;
	else{
		log().info(THISMODULE "实际传输%d,不能%d",rc,count);
	}

	set(Fun_Status,start,rc);

	m_len = 6 + rc*2;

	return rc;
}

bool CFrameDm::setData_status( const dm::uint16& idx,const rt_status_t* rts ){
	m_data[6+idx*2] = rts->getValue().getValue();
	m_data[7+idx*2] = rts->getValue().getRaw();

	return true;
}

bool CFrameDm::setData_discrete( const dm::uint32& start,const dm::uint16& count,const rt_discrete_t* rts ){
	dm::uint16 rc = setData_discrete(start,count);

	for( dm::uint16 i=0;i<rc;++i )
		setData_discrete(i,rts+i);

	return true;
}

bool CFrameDm::getData_discrete( dm::uint32& start,dm::uint16& count )const{
	if( m_len<6 ){
		log().warnning(THISMODULE "len(%d)<6",m_len);
		return false;
	}

	start = dm::bytes2uint32( m_data[0],m_data[1],m_data[2],m_data[3] );
	count = dm::bytes2uint16( m_data[4],m_data[5] );

	if( m_len!=6+count*2 ){
		log().warnning(THISMODULE "len(%d)!=%d",m_len,6+count*2);
		return false;
	}
	return true;
}

bool CFrameDm::getData_discrete( const dm::uint16& i,dm::scada::CDiscrete& value )const{
	int p = i*2;
	if( p+2+6>m_len ){
		log().warnning(THISMODULE "索引%d越限,len=%d",i,m_len);
		return false;
	}

	value.setValue(m_data[6+i*2]);
	value.setRaw(m_data[7+i*2]);

	return true;
}

dm::uint16 CFrameDm::setData_discrete( const dm::uint32& start,const dm::uint16& count ){
	dm::uint16 rc = (getBufMaxSize()-6)/2;
	if( count < rc )
		rc = count;
	else{
		log().info(THISMODULE "实际传输%d,不能%d",rc,count);
	}

	set(Fun_Discrete,start,rc);

	m_len = 6 + rc*2;

	return rc;
}

bool CFrameDm::setData_discrete( const dm::uint16& idx,const rt_discrete_t* rt ){
	m_data[6+idx*2] = rt->getValue().getValue();
	m_data[7+idx*2] = rt->getValue().getRaw();

	return true;
}

bool CFrameDm::setData_measure( const dm::uint32& start,const dm::uint16& count,const rt_measure_t* rts ){
	dm::uint16 rc = setData_measure(start,count);

	for( dm::uint16 i=0;i<rc;++i )
		setData_measure(i,rts+i);

	return true;
}

bool CFrameDm::getData_measure( dm::uint32& start,dm::uint16& count )const{
	if( m_len<6 ){
		log().warnning(THISMODULE "len(%d)<6",m_len);
		return false;
	}

	start = dm::bytes2uint32( m_data[0],m_data[1],m_data[2],m_data[3] );
	count = dm::bytes2uint16( m_data[4],m_data[5] );

	if( m_len!=6+count*4 ){
		log().warnning(THISMODULE "len(%d)!=%d",m_len,6+count*4);
		return false;
	}
	return true;
}

bool CFrameDm::getData_measure( const dm::uint16& i,dm::scada::CMeasure& value )const{
	int p = i*4;
	if( p+4+6>m_len ){
		log().warnning(THISMODULE "索引%d越限,len=%d",i,m_len);
		return false;
	}

	value.setValue(dm::bytes2float32(m_data[6+i*4],m_data[6+i*4+1],m_data[6+i*4+2],m_data[6+i*4+3]));
	return true;
}

dm::uint16 CFrameDm::setData_measure( const dm::uint32& start,const dm::uint16& count ){
	dm::uint16 rc = (getBufMaxSize()-6)/4;	// 采用4个字节传输
	if( count < rc )
		rc = count;
	else{
		log().info(THISMODULE "实际传输%d,不能%d",rc,count);
	}

	set(Fun_Measure,start,rc);

	m_len = 6 + rc*4;

	return rc;
}

bool CFrameDm::setData_measure( const dm::uint16& idx,const rt_measure_t* rt ){
	for( int b=0;b<4;++b ){
		m_data[6+idx*4+b] = dm::lowByte_f32(rt->getValue().getValue(),b);
	}

	return true;
}

/**
 * 累计量
 * @param start
 * @param count
 * @param rts
 * @return
 */
bool CFrameDm::setData_cumulant( const dm::uint32& start,const dm::uint16& count,const rt_cumulant_t* rts ){
	dm::uint16 rc = setData_cumulant(start,count);

	for( dm::uint16 i=0;i<rc;++i )
		setData_cumulant(i,rts+i);

	return true;
}

bool CFrameDm::getData_cumulant( dm::uint32& start,dm::uint16& count )const{
	if( m_len<6 ){
		log().warnning(THISMODULE "len(%d)<6",m_len);
		return false;
	}

	start = dm::bytes2uint32( m_data[0],m_data[1],m_data[2],m_data[3] );
	count = dm::bytes2uint16( m_data[4],m_data[5] );

	if( m_len!=6+count*8 ){
		log().warnning(THISMODULE "len(%d)!=%d",m_len,6+count*8);
		return false;
	}
	return true;
}

bool CFrameDm::getData_cumulant( const dm::uint16& i,dm::scada::CCumulant& value )const{
	int p = i*8;
	if( p+8+6>m_len ){
		log().warnning(THISMODULE "索引%d越限,len=%d",i,m_len);
		return false;
	}

	value.setRaw(dm::bytes2uint64(m_data[6+i*8],m_data[6+i*8+1],m_data[6+i*8+2],m_data[6+i*8+3],
			m_data[6+i*8+4],m_data[6+i*8+5],m_data[6+i*8+6],m_data[6+i*8+7]));

	return true;
}

dm::uint16 CFrameDm::setData_cumulant( const dm::uint32& start,const dm::uint16& count ){
	dm::uint16 rc = (getBufMaxSize()-6)/8;	// 采用8个字节传输
	if( count < rc )
		rc = count;
	else{
		log().info(THISMODULE "实际传输%d,不能%d",rc,count);
	}

	set(Fun_Cumulant,start,rc);

	m_len = 6 + rc*8;

	return rc;
}

bool CFrameDm::setData_cumulant( const dm::uint16& idx,const rt_cumulant_t* rt ){
	dm::uint64 ut = rt->getValue().getRaw();
	for( int b=0;b<8;++b )
		m_data[6+idx*8+b] = dm::lowByte_u64(ut,b);

	return true;
}

bool CFrameDm::startAppend_event(){
	m_fun = Fun_Report_Event;
	m_len = 2;

	m_data[0] = 0;
	m_data[1] = 0;

	return true;
}

/**
 * 增加事件
 * @param e 需要将idx字段转换成逻辑设备的idx
 * @return
 */
bool CFrameDm::appendEvent( const event_t& e ){
	int p = 0;
	int i;
	m_data[m_len+p] = e.type;	++p;

	i = 0;
	m_data[m_len+p] = dm::lowByte_i32(e.idx,i);	++p;++i;
	m_data[m_len+p] = dm::lowByte_i32(e.idx,i);	++p;++i;
	m_data[m_len+p] = dm::lowByte_i32(e.idx,i);	++p;++i;
	m_data[m_len+p] = dm::lowByte_i32(e.idx,i);	++p;++i;

	m_data[m_len+p] = dm::lowByte_u16(e.value.all); ++p;
	m_data[m_len+p] = dm::higByte_u16(e.value.all); ++p;

	i = 0;
	m_data[m_len+p] = dm::lowByte_i64((dm::int64)e.ts.seconds(),i); ++p;++i;
	m_data[m_len+p] = dm::lowByte_i64((dm::int64)e.ts.seconds(),i); ++p;++i;
	m_data[m_len+p] = dm::lowByte_i64((dm::int64)e.ts.seconds(),i); ++p;++i;
	m_data[m_len+p] = dm::lowByte_i64((dm::int64)e.ts.seconds(),i); ++p;++i;
	m_data[m_len+p] = dm::lowByte_i64((dm::int64)e.ts.seconds(),i); ++p;++i;
	m_data[m_len+p] = dm::lowByte_i64((dm::int64)e.ts.seconds(),i); ++p;++i;
	m_data[m_len+p] = dm::lowByte_i64((dm::int64)e.ts.seconds(),i); ++p;++i;
	m_data[m_len+p] = dm::lowByte_i64((dm::int64)e.ts.seconds(),i); ++p;++i;

	i = 0;
	m_data[m_len+p] = dm::lowByte_u32(e.ts.useconds(),i); ++p;++i;
	m_data[m_len+p] = dm::lowByte_u32(e.ts.useconds(),i); ++p;++i;
	m_data[m_len+p] = dm::lowByte_u32(e.ts.useconds(),i); ++p;++i;
	m_data[m_len+p] = dm::lowByte_u32(e.ts.useconds(),i); ++p;++i;


	m_len += p;

	m_data[0] = dm::lowByte_u16((m_len-2)/p);
	m_data[1] = dm::higByte_u16((m_len-2)/p);

	return m_len+p<getBufMaxSize();
}

bool CFrameDm::getReport_event( dm::uint16& count )const{
	if( m_len<2 ){
		log().warnning(THISMODULE "len(%d)<2",m_len);
		return false;
	}

	count = dm::bytes2uint16( m_data[0],m_data[1] );

	if( m_len!=2+count*19 ){
		log().warnning(THISMODULE "len(%d)!=%d",m_len,2+count*19);
		return false;
	}
	return true;
}

void CFrameDm::getReport_event( const dm::uint16& i,event_t& ev )const{
	ev.type = event_t::EType(m_data[2+i*19]);
	ev.idx = dm::bytes2int32(
			m_data[2+i*19+1],
			m_data[2+i*19+2],
			m_data[2+i*19+3],
			m_data[2+i*19+4]);
	ev.value.all = dm::bytes2uint16(
			m_data[2+i*19+5],
			m_data[2+i*19+6]);

	ev.ts.setSeconds( dm::bytes2int64(
			m_data[2+i*19+7],
			m_data[2+i*19+8],
			m_data[2+i*19+9],
			m_data[2+i*19+10],
			m_data[2+i*19+11],
			m_data[2+i*19+12],
			m_data[2+i*19+13],
			m_data[2+i*19+14]) );

	ev.ts.setUseconds( dm::bytes2uint32(
			m_data[2+i*19+15],
			m_data[2+i*19+16],
			m_data[2+i*19+17],
			m_data[2+i*19+18]));
}

// 开始追加
bool CFrameDm::startAppend_timedMeasure(){
	m_fun = Fun_Report_TimedMeasure;
	m_len = 2;

	m_data[0] = 0;
	m_data[1] = 0;

	return true;
}

// 返回值：是否可以再追加
bool CFrameDm::appendTimedMeasure( const ts_measure_t& tm ){
	int p = 0;
	int i;

	i = 0;
	m_data[m_len+p] = dm::lowByte_i32(tm.getIndex(),i); ++p;++i;
	m_data[m_len+p] = dm::lowByte_i32(tm.getIndex(),i); ++p;++i;
	m_data[m_len+p] = dm::lowByte_i32(tm.getIndex(),i); ++p;++i;
	m_data[m_len+p] = dm::lowByte_i32(tm.getIndex(),i); ++p;++i;

	i = 0;
	m_data[m_len+p] = dm::lowByte_f32(tm.getValue(),i); ++p;++i;
	m_data[m_len+p] = dm::lowByte_f32(tm.getValue(),i); ++p;++i;
	m_data[m_len+p] = dm::lowByte_f32(tm.getValue(),i); ++p;++i;
	m_data[m_len+p] = dm::lowByte_f32(tm.getValue(),i); ++p;++i;

	i = 0;
	m_data[m_len+p] = dm::lowByte_i64((dm::int64)tm.getTimeStamp().seconds(),i); ++p;++i;
	m_data[m_len+p] = dm::lowByte_i64((dm::int64)tm.getTimeStamp().seconds(),i); ++p;++i;
	m_data[m_len+p] = dm::lowByte_i64((dm::int64)tm.getTimeStamp().seconds(),i); ++p;++i;
	m_data[m_len+p] = dm::lowByte_i64((dm::int64)tm.getTimeStamp().seconds(),i); ++p;++i;
	m_data[m_len+p] = dm::lowByte_i64((dm::int64)tm.getTimeStamp().seconds(),i); ++p;++i;
	m_data[m_len+p] = dm::lowByte_i64((dm::int64)tm.getTimeStamp().seconds(),i); ++p;++i;
	m_data[m_len+p] = dm::lowByte_i64((dm::int64)tm.getTimeStamp().seconds(),i); ++p;++i;
	m_data[m_len+p] = dm::lowByte_i64((dm::int64)tm.getTimeStamp().seconds(),i); ++p;++i;

	i = 0;
	m_data[m_len+p] = dm::lowByte_u32(tm.getTimeStamp().useconds(),i); ++p;++i;
	m_data[m_len+p] = dm::lowByte_u32(tm.getTimeStamp().useconds(),i); ++p;++i;
	m_data[m_len+p] = dm::lowByte_u32(tm.getTimeStamp().useconds(),i); ++p;++i;
	m_data[m_len+p] = dm::lowByte_u32(tm.getTimeStamp().useconds(),i); ++p;++i;

	m_len += p;

	m_data[0] = dm::lowByte_u16((m_len-2)/p);
	m_data[1] = dm::higByte_u16((m_len-2)/p);

	return m_len+p<getBufMaxSize();
}

bool CFrameDm::getReport_timedMeasure( dm::uint16& count )const{
	if( m_len<2 ){
		log().warnning(THISMODULE "len(%d)<2",m_len);
		return false;
	}

	count = dm::bytes2uint16( m_data[0],m_data[1] );

	if( m_len!=2+count*20 ){
		log().warnning(THISMODULE "len(%d)!=%d",m_len,2+count*20);
		return false;
	}
	return true;
}

void CFrameDm::getReport_timedMeasure( const dm::uint16& i,ts_measure_t& ev )const{
	ev.setIndex(
			dm::bytes2int32(
					m_data[2+i*20],
					m_data[2+i*20+1],
					m_data[2+i*20+2],
					m_data[2+i*20+3]
						   )
	);
	ev.setValue(
			dm::bytes2float32(
					m_data[2+i*20+4],
					m_data[2+i*20+5],
					m_data[2+i*20+6],
					m_data[2+i*20+7]
						   )
	);

	ev.getTimeStamp().setSeconds(
			dm::bytes2int64(
					m_data[2+i*20+8],
					m_data[2+i*20+9],
					m_data[2+i*20+10],
					m_data[2+i*20+11],
					m_data[2+i*20+12],
					m_data[2+i*20+13],
					m_data[2+i*20+14],
					m_data[2+i*20+15]
						   )
	);

	ev.getTimeStamp().setUseconds(
			dm::bytes2uint32(
					m_data[2+i*20+16],
					m_data[2+i*20+17],
					m_data[2+i*20+18],
					m_data[2+i*20+19]
						   )
	);
}

// 开始追加
bool CFrameDm::startAppend_timedCumulant(){
	m_fun = Fun_Report_TimedCumulant;
	m_len = 2;

	m_data[0] = 0;
	m_data[1] = 0;

	return true;
}

// 返回值：是否可以再追加
bool CFrameDm::appendTimedCumulant( const ts_cumulant_t& tm ){
	int p = 0;
	int i;

	i = 0;
	m_data[m_len+p] = dm::lowByte_i32(tm.getIndex(),i); ++p;++i;
	m_data[m_len+p] = dm::lowByte_i32(tm.getIndex(),i); ++p;++i;
	m_data[m_len+p] = dm::lowByte_i32(tm.getIndex(),i); ++p;++i;
	m_data[m_len+p] = dm::lowByte_i32(tm.getIndex(),i); ++p;++i;

	i = 0;
	m_data[m_len+p] = dm::lowByte_u64(tm.getRaw(),i); ++p;++i;
	m_data[m_len+p] = dm::lowByte_u64(tm.getRaw(),i); ++p;++i;
	m_data[m_len+p] = dm::lowByte_u64(tm.getRaw(),i); ++p;++i;
	m_data[m_len+p] = dm::lowByte_u64(tm.getRaw(),i); ++p;++i;
	m_data[m_len+p] = dm::lowByte_u64(tm.getRaw(),i); ++p;++i;
	m_data[m_len+p] = dm::lowByte_u64(tm.getRaw(),i); ++p;++i;
	m_data[m_len+p] = dm::lowByte_u64(tm.getRaw(),i); ++p;++i;
	m_data[m_len+p] = dm::lowByte_u64(tm.getRaw(),i); ++p;++i;

	i = 0;
	m_data[m_len+p] = dm::lowByte_u64((dm::int64)tm.getTimeStamp().seconds(),i); ++p;++i;
	m_data[m_len+p] = dm::lowByte_u64((dm::int64)tm.getTimeStamp().seconds(),i); ++p;++i;
	m_data[m_len+p] = dm::lowByte_u64((dm::int64)tm.getTimeStamp().seconds(),i); ++p;++i;
	m_data[m_len+p] = dm::lowByte_u64((dm::int64)tm.getTimeStamp().seconds(),i); ++p;++i;
	m_data[m_len+p] = dm::lowByte_u64((dm::int64)tm.getTimeStamp().seconds(),i); ++p;++i;
	m_data[m_len+p] = dm::lowByte_u64((dm::int64)tm.getTimeStamp().seconds(),i); ++p;++i;
	m_data[m_len+p] = dm::lowByte_u64((dm::int64)tm.getTimeStamp().seconds(),i); ++p;++i;
	m_data[m_len+p] = dm::lowByte_u64((dm::int64)tm.getTimeStamp().seconds(),i); ++p;++i;

	i = 0;
	m_data[m_len+p] = dm::lowByte_u32(tm.getTimeStamp().useconds(),i); ++p;++i;
	m_data[m_len+p] = dm::lowByte_u32(tm.getTimeStamp().useconds(),i); ++p;++i;
	m_data[m_len+p] = dm::lowByte_u32(tm.getTimeStamp().useconds(),i); ++p;++i;
	m_data[m_len+p] = dm::lowByte_u32(tm.getTimeStamp().useconds(),i); ++p;++i;

	m_len += p;

	m_data[0] = dm::lowByte_u16((m_len-2)/p);
	m_data[1] = dm::higByte_u16((m_len-2)/p);

	return m_len+p<getBufMaxSize();
}

bool CFrameDm::getReport_timedCumulant( dm::uint16& count )const{
	if( m_len<2 ){
		log().warnning(THISMODULE "len(%d)<2",m_len);
		return false;
	}

	count = dm::bytes2uint16( m_data[0],m_data[1] );

	if( m_len!=2+count*24 ){
		log().warnning(THISMODULE "len(%d)!=%d",m_len,2+count*24);
		return false;
	}
	return true;
}

void CFrameDm::getReport_timedCumulant( const dm::uint16& i,ts_cumulant_t& tc )const{
	tc.setIndex( dm::bytes2int32(
			m_data[2+i*24],
			m_data[2+i*24+1],
			m_data[2+i*24+2],
			m_data[2+i*24+3]));
	tc.setRaw( dm::bytes2uint64(
			m_data[2+i*24+4],
			m_data[2+i*24+5],
			m_data[2+i*24+6],
			m_data[2+i*24+7],
			m_data[2+i*24+8],
			m_data[2+i*24+9],
			m_data[2+i*24+10],
			m_data[2+i*24+11]));

	tc.getTimeStamp().setSeconds( dm::bytes2int64(
			m_data[2+i*24+12],
			m_data[2+i*24+13],
			m_data[2+i*24+14],
			m_data[2+i*24+15],
			m_data[2+i*24+16],
			m_data[2+i*24+17],
			m_data[2+i*24+18],
			m_data[2+i*24+19]) );

	tc.getTimeStamp().setUseconds( dm::bytes2uint32(
			m_data[2+i*24+20],
			m_data[2+i*24+21],
			m_data[2+i*24+22],
			m_data[2+i*24+23]));
}

bool CFrameDm::setMsg( const dm::os::msg::CMsgInfo& msg ){
	m_fun = Fun_Msg;
	m_len = 0;

	m_data[m_len++] = msg.getType();
	m_data[m_len++] = msg.getAck();

	switch( msg.getType() ){
	case dm::msg::Msg_Call:
		m_data[m_len++] = dm::lowByte_u16(msg.getCall().group);
		m_data[m_len++] = dm::lowByte_u16(msg.getCall().group);
		break;
	case dm::msg::Msg_SyncClock:
		break;
	case dm::msg::Msg_RemoteCtl:
		m_data[m_len++] = dm::lowByte_i32(msg.getRemoteCtl().index,0);
		m_data[m_len++] = dm::lowByte_i32(msg.getRemoteCtl().index,1);
		m_data[m_len++] = dm::lowByte_i32(msg.getRemoteCtl().index,2);
		m_data[m_len++] = dm::lowByte_i32(msg.getRemoteCtl().index,3);

		m_data[m_len++] = msg.getRemoteCtl().remoteCtl;
		break;
	case dm::msg::Msg_Set:
		m_data[m_len++] = dm::lowByte_i32(msg.getPara().index,0);
		m_data[m_len++] = dm::lowByte_i32(msg.getPara().index,1);
		m_data[m_len++] = dm::lowByte_i32(msg.getPara().index,2);
		m_data[m_len++] = dm::lowByte_i32(msg.getPara().index,3);

		m_data[m_len++] = dm::lowByte_i32(msg.getPara().num,0);
		m_data[m_len++] = dm::lowByte_i32(msg.getPara().num,1);
		m_data[m_len++] = dm::lowByte_i32(msg.getPara().num,2);
		m_data[m_len++] = dm::lowByte_i32(msg.getPara().num,3);

		if( msg.isRequest() ){
			m_data[m_len++] = msg.getParaData().type;

			size_t l = msg.getParaData().dataLen();
			if( l+m_len>=(unsigned int)getBufMaxSize() ){
				log().error(THISMODULE "参数下发消息缓冲区超出帧缓冲区");
				return false;
			}

			if( l>0 )
				memcpy(m_data+m_len,msg.getParaData().as_binary(),l);
			m_len += l;
		}
		break;
	case dm::msg::Msg_Get:
		m_data[m_len++] = dm::lowByte_i32(msg.getPara().index,0);
		m_data[m_len++] = dm::lowByte_i32(msg.getPara().index,1);
		m_data[m_len++] = dm::lowByte_i32(msg.getPara().index,2);
		m_data[m_len++] = dm::lowByte_i32(msg.getPara().index,3);

		m_data[m_len++] = dm::lowByte_i32(msg.getPara().num,0);
		m_data[m_len++] = dm::lowByte_i32(msg.getPara().num,1);
		m_data[m_len++] = dm::lowByte_i32(msg.getPara().num,2);
		m_data[m_len++] = dm::lowByte_i32(msg.getPara().num,3);

		if( msg.getAck()==dm::msg::MaSuccess ){
			m_data[m_len++] = msg.getParaData().type;

			size_t l = msg.getParaData().dataLen();
			if( l+m_len>=(unsigned int)getBufMaxSize() ){
				log().error(THISMODULE "参数返回消息缓冲区超出帧缓冲区");
				return false;
			}

			if( l>0 )
				memcpy(m_data,msg.getParaData().as_binary(),l);
			m_len += l;
		}
		break;
	case dm::msg::Msg_Update:
		m_data[m_len++] = dm::lowByte_i32(msg.getPara().index,0);
		m_data[m_len++] = dm::lowByte_i32(msg.getPara().index,1);
		m_data[m_len++] = dm::lowByte_i32(msg.getPara().index,2);
		m_data[m_len++] = dm::lowByte_i32(msg.getPara().index,3);

		m_data[m_len++] = dm::lowByte_i32(msg.getPara().num,0);
		m_data[m_len++] = dm::lowByte_i32(msg.getPara().num,1);
		m_data[m_len++] = dm::lowByte_i32(msg.getPara().num,2);
		m_data[m_len++] = dm::lowByte_i32(msg.getPara().num,3);

		if( msg.isRequest() ){
			m_data[m_len++] = msg.getParaData().type;

			size_t l = msg.getParaData().dataLen();
			if( l+m_len>=(unsigned int)getBufMaxSize() ){
				log().error(THISMODULE "参数下发消息缓冲区超出帧缓冲区");
				return false;
			}

			if( l>0 )
				memcpy(m_data+m_len,msg.getParaData().as_binary(),l);
			m_len += l;
		}
		break;
	case dm::msg::Msg_SyncData:
		m_data[m_len++] = dm::lowByte_i32(msg.getSync().type,0);
		m_data[m_len++] = dm::lowByte_i32(msg.getSync().type,1);
		m_data[m_len++] = dm::lowByte_i32(msg.getSync().type,2);
		m_data[m_len++] = dm::lowByte_i32(msg.getSync().type,3);
		break;
	case dm::msg::Msg_RemoteReset:
		break;
	default:
		log().warnning(THISMODULE "未知消息类型%d",msg.getType());
		return false;
	}

	return true;
}

bool CFrameDm::getMsg( dm::os::msg::CMsgInfo& msg )const{
	if( m_len<2 )
		return false;

	int p = 0;
	msg.setType( (dm::msg::EMsgType)m_data[p++]);
	msg.setAck( (dm::msg::EMsgAck)m_data[p++] );

	switch( msg.getType() ){
	case dm::msg::Msg_Call:
		msg.getCall().group = dm::bytes2uint16( m_data[p],m_data[p+1]);
		p += 2;
		if( p!=m_len ){
			log().warnning(THISMODULE "召唤消息：帧长度%d于消息类型长度%d不匹配",m_len,p);
			return false;
		}
		break;
	case dm::msg::Msg_SyncClock:
		if( p!=m_len ){
			log().warnning(THISMODULE "对时消息：帧长度%d于消息类型长度%d不匹配",m_len,p);
			return false;
		}
		break;
	case dm::msg::Msg_RemoteCtl:
		msg.getRemoteCtl().index = dm::bytes2int32(m_data[p],m_data[p+1],m_data[p+2],m_data[p+3]);
		p += 4;
		msg.getRemoteCtl().remoteCtl = dm::msg::ERemoteControl(m_data[p]);
		p += 1;

		if( p!=m_len ){
			log().warnning(THISMODULE "远控消息：帧长度%d于消息类型长度%d不匹配",m_len,p);
			return false;
		}
		break;
	case dm::msg::Msg_Set:
		msg.getPara().index = dm::bytes2int32(m_data[p],m_data[p+1],m_data[p+2],m_data[p+3]);
		p += 4;
		msg.getPara().num = dm::bytes2int32(m_data[p],m_data[p+1],m_data[p+2],m_data[p+3]);
		p += 4;

		if( msg.isRequest() ){
			msg.getParaData().type = dm::msg::SParaData::EDataType(m_data[p++]);

			size_t l = msg.getParaData().dataLen();
			if( p+l!=m_len ){
				log().warnning(THISMODULE "参数设置消息：帧长度%d于消息类型长度%d不匹配",m_len,p+l);
				return false;
			}

			memcpy(msg.getParaData().as_binary(),m_data+p,l);
		}
		break;
	case dm::msg::Msg_Get:
		msg.getPara().index = dm::bytes2int32(m_data[p],m_data[p+1],m_data[p+2],m_data[p+3]);
		p += 4;
		msg.getPara().num = dm::bytes2int32(m_data[p],m_data[p+1],m_data[p+2],m_data[p+3]);
		p += 4;

		if( msg.getAck()==dm::msg::MaSuccess ){
			msg.getParaData().type = dm::msg::SParaData::EDataType(m_data[p++]);

			size_t l = msg.getParaData().dataLen();
			if( p+l!=m_len ){
				log().warnning(THISMODULE "参数返回消息：帧长度%d于消息类型长度%d不匹配",m_len,p+l);
				return false;
			}

			memcpy(msg.getParaData().as_binary(),m_data+p,l);
		}
		break;
	case dm::msg::Msg_Update:
		msg.getPara().index = dm::bytes2int32(m_data[p],m_data[p+1],m_data[p+2],m_data[p+3]);
		p += 4;
		msg.getPara().num = dm::bytes2int32(m_data[p],m_data[p+1],m_data[p+2],m_data[p+3]);
		p += 4;

		if( msg.getAck()==dm::msg::MaNoAck || msg.getAck()==dm::msg::MaAck ){
			msg.getParaData().type = dm::msg::SParaData::EDataType(m_data[p++]);

			size_t l = msg.getParaData().dataLen();
			if( p+l!=m_len ){
				log().warnning(THISMODULE "参数返回消息：帧长度%d于消息类型长度%d不匹配",m_len,p+l);
				return false;
			}

			memcpy(msg.getParaData().as_binary(),m_data+p,l);
		}
		break;
	case dm::msg::Msg_SyncData:
		msg.getSync().type = (dm::msg::ESyncDataType)dm::bytes2uint32(m_data[p],m_data[p+1],m_data[p+2],m_data[p+3]);
		p += 4;
		if( p!=m_len ){
			log().warnning(THISMODULE "同步消息：帧长度%d于消息类型长度%d不匹配",m_len,p);
			return false;
		}
		break;
	case dm::msg::Msg_RemoteReset:
		if( p!=m_len ){
			log().warnning(THISMODULE "复位消息：帧长度%d于消息类型长度%d不匹配",m_len,p);
			return false;
		}
		break;
	default:
		log().warnning(THISMODULE "未知消息类型%d",msg.getType());
		return false;
	}

	return true;
}

bool CFrameDm::setClock( dm::CTimeStamp::s_t t ){
	m_fun = Fun_Clock;
	for( m_len=0;m_len<8;++m_len )
		m_data[m_len] = dm::lowByte_u64(t,m_len);

	return true;
}

bool CFrameDm::getClock( dm::CTimeStamp::s_t& t )const{
	if(m_len!=8 )
		return false;

	t = dm::bytes2uint64(m_data[0],m_data[1],m_data[2],m_data[3], \
			m_data[4],m_data[5],m_data[6],m_data[7]);

	return true;
}

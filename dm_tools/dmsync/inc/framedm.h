﻿/*
 * framedm.hpp
 *
 *  Created on: 2016��3��21��
 *      Author: dylan
 */

#ifndef DM_FRAME_FRAMEDM_HPP_
#define DM_FRAME_FRAMEDM_HPP_

#include <dm/protocol/framedm.hpp>

#include <dm/scada/rtvalue.hpp>
#include <dm/scada/status.hpp>
#include <dm/scada/discrete.hpp>
#include <dm/scada/measure.hpp>
#include <dm/scada/cumulant.hpp>

#include <dm/scada/event.hpp>
#include <dm/scada/timedmeasure.hpp>
#include <dm/scada/timedcumulant.hpp>
#include <dm/os/msg/msginfo.hpp>

/**
 * DM同步报文帧结构
 */
class CFrameDm:public dm::protocol::TCFrameDm<896>{
public:
	typedef dm::scada::TCRtValue<dm::scada::CStatus> rt_status_t;
	typedef dm::scada::TCRtValue<dm::scada::CDiscrete> rt_discrete_t;
	typedef dm::scada::TCRtValue<dm::scada::CMeasure> rt_measure_t;
	typedef dm::scada::TCRtValue<dm::scada::CCumulant> rt_cumulant_t;

	typedef dm::scada::CEvent event_t;
	typedef dm::scada::CTimedMeasure ts_measure_t;
	typedef dm::scada::CTimedCumulant ts_cumulant_t;

	typedef dm::os::msg::CMsgInfo msg_t;

	enum EFun{
		Fun_Test = 0,				//!< 测试
		Fun_TestAck,				//!< 测试应答

		Fun_Read_Status,		//!< 读取状态量
		Fun_Status,					//!< 状态量

		Fun_Read_Discrete,		//!< 读取离散量
		Fun_Discrete,				//!< 离散量

		Fun_Read_Measure,	//!< 读取测量量
		Fun_Measure,				//!< 测量量

		Fun_Read_Cumulant,	//!< 读取累计量
		Fun_Cumulant,			//!< 累计量

		Fun_Report_Event,		//!< 上报事件

		Fun_Report_TimedMeasure,	//!< 上报时标测量量

		Fun_Report_TimedCumulant,	//!< 上报时标累计量

		Fun_Msg,	//!< 发送消息

		Fun_Clock,	//!< 时钟同步

		Fun_Reset,	//!< 复位系统

		Fun_ReadFile,	//!< 读取文件
		Fun_WriteFile,	//!< 写入文件
		Fun_OsCmd		//!< 执行系统命令
	};

	bool setTest();
	bool getTest()const;

	bool setTestAck( const bool& ack );
	bool getTestAck( bool& ack )const;

	bool setRead_status( const dm::uint32& start,const dm::uint16& count ){
		return setRead(Fun_Read_Status,start,count);
	}

	inline bool getRead_status( dm::uint32& start,dm::uint16& count )const{
		return getRead(Fun_Read_Status,start,count);
	}

	bool setData_status( const dm::uint32& start,const dm::uint16& count,const rt_status_t* rts );
	bool getData_status( dm::uint32& start,dm::uint16& count )const;
	bool getData_status( const dm::uint16& i,dm::scada::CStatus& value )const;

	dm::uint16 setData_status( const dm::uint32& start,const dm::uint16& count );
	bool setData_status( const dm::uint16& idx,const rt_status_t* rt );

	inline void setRead_discrete( const dm::uint32& start,const dm::uint16& count ){
		setRead(Fun_Read_Discrete,start,count);
	}

	inline bool getRead_discrete( dm::uint32& start,dm::uint16& count )const{
		return getRead(Fun_Read_Discrete,start,count);
	}

	bool setData_discrete( const dm::uint32& start,const dm::uint16& count,const rt_discrete_t* rts );
	bool getData_discrete( dm::uint32& start,dm::uint16& count )const;
	bool getData_discrete( const dm::uint16& i,dm::scada::CDiscrete& value )const;

	dm::uint16 setData_discrete( const dm::uint32& start,const dm::uint16& count );
	bool setData_discrete( const dm::uint16& idx,const rt_discrete_t* rt );

	inline void setRead_measure( const dm::uint32& start,const dm::uint16& count ){
		setRead(Fun_Read_Measure,start,count);
	}

	inline bool getRead_measure( dm::uint32& start,dm::uint16& count )const{
		return getRead(Fun_Read_Measure,start,count);
	}

	bool setData_measure( const dm::uint32& start,const dm::uint16& count,const rt_measure_t* rts );
	bool getData_measure( dm::uint32& start,dm::uint16& count )const;
	bool getData_measure( const dm::uint16& i,dm::scada::CMeasure& value )const;

	dm::uint16 setData_measure( const dm::uint32& start,const dm::uint16& count );
	bool setData_measure( const dm::uint16& idx,const rt_measure_t* rt );

	inline void setRead_cumulant( const dm::uint32& start,const dm::uint16& count ){
		setRead(Fun_Read_Cumulant,start,count);
	}

	inline bool getRead_cumulant( dm::uint32& start,dm::uint16& count )const{
		return getRead(Fun_Read_Cumulant,start,count);
	}

	bool setData_cumulant( const dm::uint32& start,const dm::uint16& count,const rt_cumulant_t* rts );
	bool getData_cumulant( dm::uint32& start,dm::uint16& count )const;
	bool getData_cumulant( const dm::uint16& i,dm::scada::CCumulant& value )const;

	dm::uint16 setData_cumulant( const dm::uint32& start,const dm::uint16& count );
	bool setData_cumulant( const dm::uint16& idx,const rt_cumulant_t* rt );

	// 开始追加事件
	bool startAppend_event();
	bool appendEvent( const event_t& e );	// 返回值：是否可以再追加

	bool getReport_event( dm::uint16& count )const;
	void getReport_event( const dm::uint16& i,event_t& ev )const;

	// 开始追加
	bool startAppend_timedMeasure();
	bool appendTimedMeasure( const ts_measure_t& tm );	// 返回值：是否可以再追加

	bool getReport_timedMeasure( dm::uint16& count )const;
	void getReport_timedMeasure( const dm::uint16& i,ts_measure_t& ev )const;

	// 开始追加
	bool startAppend_timedCumulant();
	bool appendTimedCumulant( const ts_cumulant_t& tm );	// 返回值：是否可以再追加

	bool getReport_timedCumulant( dm::uint16& count )const;
	void getReport_timedCumulant( const dm::uint16& i,ts_cumulant_t& tc )const;

	bool setMsg( const dm::os::msg::CMsgInfo& msg );
	bool getMsg( dm::os::msg::CMsgInfo& msg )const;

	bool setClock( dm::CTimeStamp::s_t t );
	bool getClock( dm::CTimeStamp::s_t& t )const;

protected:
	inline bool setRead( const EFun& fun,const dm::uint32& start,const dm::uint16& count ){
		return set(fun,start,count);
	}

	inline bool getRead( const EFun& fun,dm::uint32& start,dm::uint16& count )const{
		return get(fun,start,count);
	}
};

#endif /* INC_FRAME_FRAMEDM_HPP_ */

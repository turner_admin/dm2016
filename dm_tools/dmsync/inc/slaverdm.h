﻿/*
 * slaverdm.h
 *
 *  Created on: 2017年5月17日
 *      Author: work
 */

#ifndef APPS_DMSYNC_INC_SLAVERDM_H_
#define APPS_DMSYNC_INC_SLAVERDM_H_

#include <dm/os/protocol/protocolslaver.hpp>
#include <dm/scada/eventmonitor.hpp>
#include <dm/scada/timedmeasuremonitor.hpp>
#include <dm/scada/timedcumulantmonitor.hpp>
#include "framedm.h"

/**
 * DM从站类
 * 上报本地数据，接收主站控制指令。
 */
class CSlaverDm:public dm::os::protocol::CProtocolSlaver{
public:
	CSlaverDm();
	~CSlaverDm();

	const char* name()const;

	frame_t& getRxFrame();
	const frame_t& getTxFrame();

	inline void setWaitTimeout( int sec ){
		m_waitTimeout = sec;
	}

	void reset( const ts_t& ts,const rts_t& rts );

	EAction dealRxFrame( const ts_t& ts,const rts_t& rts );

	bool isWaiting( const rts_t& rts );

	inline void setClockInterval( unsigned int s ){
		m_clockInterval = s;
	}

	inline const unsigned int& getClockInterval()const{
		return m_clockInterval;
	}

	inline void setAcceptClock( bool a=true ){
		m_acceptClock = a;
	}

	inline const bool& ifAcceptClock()const{
		return m_acceptClock;
	}

	bool setStatusOffset( int offset );
	bool setStatusSize( int size );

	bool setDiscreteOffset( int offset );
	bool setDiscreteSize( int size );

	bool setMeasureOffset( int offset );
	bool setMeasureSize( int size );

	bool setCumulantOffset( int offset );
	bool setCumulantSize( int size );

	bool setRmtctlOffset( int offset );
	bool setRmtctlSize( int size );

	bool setParameterOffset( int offset );
	bool setParameterSize( int size );

	bool setActionOffset( int offset );
	bool setActionSize( int size );

protected:
	EAction taskStart_call( const ts_t& ts,const rts_t& rts );

	EAction task_none( const ts_t& ts,const rts_t& rts );

	EAction checkReport( const ts_t& ts,const rts_t& rts );

	EAction onMsgRequest_update( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts );

	EAction onMsgAnswer_call( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts );
	EAction onMsgAnswer_rmtCtl( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts );
	EAction onMsgAnswer_set( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts );
	EAction onMsgAnswer_get( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts );
	EAction onMsgAnswer_syncData( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts );
	EAction onMsgAnswer_remoteReset( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts );
private:
	CFrameDm m_rxFrame;
	CFrameDm m_txFrame;

	dm::scada::CEventMonitor m_em;
	dm::scada::CTimedMeasureMonitor m_tm;
	dm::scada::CTimedCumulantMonitor m_tc;

	int m_callIndex;

	int m_offset_s;
	int m_size_s;

	int m_offset_d;
	int m_size_d;

	int m_offset_m;
	int m_size_m;

	int m_offset_c;
	int m_size_c;

	int m_offset_r;
	int m_size_r;

	int m_offset_p;
	int m_size_p;

	int m_offset_a;
	int m_size_a;

	bool m_wait;	// 是否等待应答
	int m_waitTimeout;

	rts_t m_lastClock;	// 上次发送对时时刻
	unsigned int m_clockInterval;	// 发送对时命令间隔：秒
	bool m_acceptClock;					// 是否接受对端对时
};

#endif /* APPS_DMSYNC_INC_SLAVERDM_H_ */

﻿/*
 * logshow.cpp
 *
 *  Created on: 2016年11月5日
 *      Author: work
 */

#include <boost/program_options.hpp>
#include <dm/os/protocol/telegrammonitor.hpp>
#include <string>
#include <iostream>

#ifndef WIN32
#include <unistd.h>
#endif

using namespace std;
using namespace boost::program_options;
using namespace dm::os::protocol;

class CMyMonitor:public CTelegramMonitor{
public:
#ifdef WIN32
	typedef dm::os::protocol::CTelegramInfo::pid_t pid_t;
#endif

	CMyMonitor():CTelegramMonitor(),m_pid(-1),m_protocol(){
	}

	void setPid( pid_t p ){
		m_pid = p;
	}

	void setProtocol( const char* m ){
		m_protocol = m;
	}

	bool filter( const ringpos_t& pos,const info_t& info,const bool& overflow)const{
		if( m_pid!=-1 && info.pid!=m_pid )
			return true;

		if( m_protocol.len()>0 && m_protocol!=info.protocol )
			return true;

		return CTelegramMonitor::filter(pos,info,overflow);
	}

private:
	pid_t m_pid;
	info_t::protocol_t m_protocol;
};

int main( int argc,char* argv[] ){
	options_description desc("DM系统报文监视工具");
	desc.add_options()
			("help,h","显示本帮助信息")
			("version,v","显示版本及编译信息")
			("pid,p",value<int>(),"指定进程号")
			("protocol,p",value<string>(),"指定规约名")
			("raw","查看接收原始")
			("no-data","不显示数据")
			("reset","复位报文监视系统")
			("char","字符显示")
			;

	variables_map vm;
	try{
		store(parse_command_line(argc,argv,desc),vm);
		notify(vm);
	}catch( std::exception& ex ){
		cout << ex.what()<<endl;
		return 1;
	}

	if( vm.count("version") ){
		cout <<"V1.0 构建于 "<<__DATE__<<endl;
		return 1;
	}

	if( vm.count("help") ){
		cout <<desc<<endl;
		return 1;
	}

	if( vm.count("reset") ){
		cout <<"复位报文监视系统..."<<endl;
		dm::os::protocol::CTelegramMgr::remove();
		return 0;
	}


	CMyMonitor monitor;

	if( vm.count("pid") )
		monitor.setPid(vm["pid"].as<int>());

	if( vm.count("protocol") )
		monitor.setProtocol(vm["protocol"].as<string>().c_str());

	if( vm.count("raw") )
		monitor.setRaw(true);
	else
		monitor.setRaw(false);

	if( vm.count("char") )
		monitor.setChar(true);
	else
		monitor.setChar(false);

	if( vm.count("no-data") )
		monitor.setNoData();

	monitor.run();

	return 0;
}



﻿/*
 * dm104master.cpp
 *
 *  Created on: 2017年5月10日
 *      Author: work
 */

#include "dm104master.h"

#include <dm/os/log/logger.hpp>

static const char* logModule = "CDm104Master.app.dm";

CDm104Master::CDm104Master():dm::os::protocol::CIec104Master(),m_deviceAgent(),m_autoCalled(false){
	log().debug( THISMODULE "创建对象");
}

CDm104Master::~CDm104Master(){
	log().debug( THISMODULE "销毁对象");
}

const char* CDm104Master::name()const{
	return "DM104 Master";
}

void CDm104Master::reset( const ts_t& ts,const rts_t& rts ){
	m_autoCalled = false;
	dm::os::protocol::CIec104Master::reset(ts,rts);
}

CDm104Master::EAction CDm104Master::dealStartDtConf(const ts_t& ts,const rts_t& rts){
	EAction action = dm::os::protocol::CIec104Master::dealStartDtConf(ts,rts);
	if( isDtStarted() && m_autoCalled==false ){
		setTask_call(ts,rts,0);
		m_autoCalled = true;
	}

	return action;
}

// 双点信息
bool CDm104Master::onRecv_3_M_DP_NA_1( const frame_apdu_t::ECause& /*cause*/,const dm::uint32& address,const frame_apdu_t::UDiq& diq ){
	return m_deviceAgent.updateStatus(address,dm::scada::CStatus(diq.bits.dpi,diq.all));
}

bool CDm104Master::onRecv_31_M_DP_TB_1( const frame_apdu_t::ECause& /*cause*/,const dm::uint32& address,const frame_apdu_t::UDiq& diq,const frame_apdu_t::UCp56Time2a& /*time*/ ){
	return m_deviceAgent.updateStatus(address,dm::scada::CStatus(diq.bits.dpi,diq.all));
}

// 标度化值
bool CDm104Master::onRecv_11_M_ME_NB_1( const frame_apdu_t::ECause& /*cause*/,const dm::uint32& address,const frame_apdu_t::USva& nva,const frame_apdu_t::UQds& /*qds*/ ){
	return m_deviceAgent.updateDiscrete(address,dm::scada::CDiscrete(nva.all,nva.all));
}

bool CDm104Master::onRecv_35_M_ME_TE_1( const frame_apdu_t::ECause& /*cause*/,const dm::uint32& address,const frame_apdu_t::USva& nva,const frame_apdu_t::UQds& /*qds*/,const frame_apdu_t::UCp56Time2a& /*time*/ ){
	return m_deviceAgent.updateDiscrete(address,dm::scada::CDiscrete(nva.all,nva.all));
}

// 短浮点数
bool CDm104Master::onRecv_13_M_ME_NC_1( const frame_apdu_t::ECause& /*cause*/,const dm::uint32& address,const frame_apdu_t::UStd& frac,const frame_apdu_t::UQds& /*qds*/ ){
	return m_deviceAgent.updateMeasure(address,frac.all);
}

bool CDm104Master::onRecv_36_M_ME_TF_1( const frame_apdu_t::ECause& /*cause*/,const dm::uint32& address,const frame_apdu_t::UStd& frac,const frame_apdu_t::UQds& /*qds*/,const frame_apdu_t::UCp56Time2a& /*time*/ ){
	return m_deviceAgent.updateMeasure(address,frac.all);
}

// 累计量
bool CDm104Master::onRecv_15_M_IT_NA_1( const frame_apdu_t::ECause& /*cause*/,const dm::uint32& address,const dm::int32& value ){
	return m_deviceAgent.updateCumulant(address,value);
}

bool CDm104Master::onRecv_37_M_IT_TB_1( const frame_apdu_t::ECause& /*cause*/,const dm::uint32& address,const dm::int32& value,const frame_apdu_t::UCp56Time2a& /*time*/ ){
	return m_deviceAgent.updateCumulant(address,value);
}

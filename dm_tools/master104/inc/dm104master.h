﻿/*
 * protocol_master104.h
 *
 *  Created on: 2017年5月7日
 *      Author: work
 */

#ifndef APPS_MASTER104_INC_DM104MASTER_H_
#define APPS_MASTER104_INC_DM104MASTER_H_


#include <dm/protocol/protocolbase.hpp>
#include <dm/os/msg/msginfo.hpp>
#include <dm/scada/eventmonitor.hpp>
#include <dm/scada/timedmeasuremonitor.hpp>
#include <dm/scada/timedcumulantmonitor.hpp>
#include <dm/os/protocol/iec104master.hpp>
#include <dm/scada/deviceagent.hpp>

class CDm104Master:public dm::os::protocol::CIec104Master{
public:
	CDm104Master();
	~CDm104Master();

	/**
	 * 设置设备名
	 * @param dev
	 * @return
	 */
	inline bool setDeviceByName( const char* name ){
		return m_deviceAgent.setByName(name);
	}

	inline bool setDeviceById( const dm::scada::id_t& dev ){
		return m_deviceAgent.setById(dev);
	}

	const char* name()const;

	void reset( const ts_t& ts,const rts_t& rts );

protected:
	EAction dealStartDtConf( const ts_t& ts,const rts_t& rts );

	// 双点信息
	bool onRecv_3_M_DP_NA_1( const frame_apdu_t::ECause& cause,const dm::uint32& address,const frame_apdu_t::UDiq& diq );
	bool onRecv_31_M_DP_TB_1( const frame_apdu_t::ECause& cause,const dm::uint32& address,const frame_apdu_t::UDiq& diq,const frame_apdu_t::UCp56Time2a& time );

	// 标度化值
	bool onRecv_11_M_ME_NB_1( const frame_apdu_t::ECause& cause,const dm::uint32& address,const frame_apdu_t::USva& nva,const frame_apdu_t::UQds& qds );
	bool onRecv_35_M_ME_TE_1( const frame_apdu_t::ECause& cause,const dm::uint32& address,const frame_apdu_t::USva& nva,const frame_apdu_t::UQds& qds,const frame_apdu_t::UCp56Time2a& time );

	// 短浮点数
	bool onRecv_13_M_ME_NC_1( const frame_apdu_t::ECause& cause,const dm::uint32& address,const frame_apdu_t::UStd& frac,const frame_apdu_t::UQds& qds );
	bool onRecv_36_M_ME_TF_1( const frame_apdu_t::ECause& cause,const dm::uint32& address,const frame_apdu_t::UStd& frac,const frame_apdu_t::UQds& qds,const frame_apdu_t::UCp56Time2a& time );

	// 累计量
	bool onRecv_15_M_IT_NA_1( const frame_apdu_t::ECause& cause,const dm::uint32& address,const dm::int32& value );
	bool onRecv_37_M_IT_TB_1( const frame_apdu_t::ECause& cause,const dm::uint32& address,const dm::int32& value,const frame_apdu_t::UCp56Time2a& time );

private:
	dm::scada::CDeviceAgent m_deviceAgent;

	bool m_autoCalled;	// 是否主动召唤过
};


#endif /* APPS_MASTER104_INC_DM104MASTER_H_ */

/*
 * forwardmgr.cpp
 *
 *  Created on: 2016年12月15日
 *      Author: work
 */


#include "forwardmgr.hpp"
#include <boost/asio/serial_port.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/ip/address.hpp>

#include <boost/smart_ptr/shared_ptr.hpp>

using namespace boost::asio;

typedef boost::shared_ptr<serial_port> p_serial_t;

CForwardMgr::CForwardMgr():m_service(){
	p_serial_t serial;

	CForwardor * f = new CForwardor;

	serial.reset( new serial_port(m_service,"/dev/ttyS1" ));
	serial->set_option( serial_port::baud_rate(9600) );
	serial->set_option( serial_port::character_size(8) );
	serial->set_option( serial_port::flow_control(serial_port::flow_control::none) );
	serial->set_option( serial_port::stop_bits(serial_port::stop_bits::one) );
	serial->set_option( serial_port::parity(serial_port::parity::none) );

	f->setA(serial);

	serial.reset( new serial_port(m_service,"/dev/ttyS2" ));
	serial->set_option( serial_port::baud_rate(9600) );
	serial->set_option( serial_port::character_size(8) );
	serial->set_option( serial_port::flow_control(serial_port::flow_control::none) );
	serial->set_option( serial_port::stop_bits(serial_port::stop_bits::one) );
	serial->set_option( serial_port::parity(serial_port::parity::none) );

	f->setB(serial);


}


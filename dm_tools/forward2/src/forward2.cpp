/*
 * main.cpp
 *
 *  Created on: 2016年12月15日
 *      Author: work
 */

#include "forwardmgr.hpp"

#include <iostream>
#include <exception>

using namespace boost::asio;

int main( int argc,char* argv[] ){
	try{
		CForwardMgr mgr;
		mgr.run();
	}catch( std::exception& e ){
		std::cerr << e.what() << std::endl;
	}

	return 0;
}



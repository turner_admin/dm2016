/*
 * forwardmgr.hpp
 *
 *  Created on: 2016年12月15日
 *      Author: work
 */

#ifndef APPS_FORWARD2_INC_FORWARDMGR_HPP_
#define APPS_FORWARD2_INC_FORWARDMGR_HPP_

#include "forwardor.hpp"
#include <boost/asio/io_service.hpp>

class CForwardMgr{
public:
	CForwardMgr();

	inline std::size_t run(){
		return m_service.run();
	}

private:
	boost::asio::io_service m_service;
};

#endif /* APPS_FORWARD2_INC_FORWARDMGR_HPP_ */

/*
 * tcpserver.hpp
 *
 *  Created on: 2016年12月15日
 *      Author: work
 */

#ifndef APPS_FORWARD2_INC_TCPSERVER_HPP_
#define APPS_FORWARD2_INC_TCPSERVER_HPP_

#include <boost/asio/ip/tcp.hpp>

class CTcpServer{
public:
	typedef boost::system::error_code error_t;
	typedef boost::asio::ip::tcp::acceptor acceptor_t;
	typedef boost::asio::ip::tcp::socket socket_t;

	CTcpServer( boost::asio::io_service& service );

	void on_accept( const error_t& error,socket_t sock );

private:
	boost::asio::ip::tcp::acceptor m_acpt;
};

#endif /* APPS_FORWARD2_INC_TCPSERVER_HPP_ */

﻿/*
 * test_com.cpp
 *
 *  Created on: 2016年12月18日
 *      Author: work
 */

#include <iostream>
#include <cstdio>
#include <dm/bytes.hpp>
#include <dm/types.hpp>
#include <dm/msg/types.hpp>
#include <dm/os/msg/msginfo.hpp>

#include <ctime>

using namespace std;

int main( int /*argc*/,char** /*argv*/ ){
	dm::uint16 u16Tmp;
	dm::float32 f32Tmp;

	cout <<"测试基本类型:"<<endl;
	cout << "uint8:"<<sizeof(dm::uint8)<<(sizeof(dm::uint8)==1?"通过":"异常")<<endl;
	cout << "int8 :"<<sizeof(dm::int8)<<(sizeof(dm::int8)==1?"通过":"异常")<<endl;

	cout << "uint16:"<<sizeof(dm::uint16)<<(sizeof(dm::uint16)==2?"通过":"异常")<<endl;
	cout << "int16 :"<<sizeof(dm::int16)<<(sizeof(dm::int16)==2?"通过":"异常")<<endl;

	u16Tmp = 0x8001;
	cout << "uint16字节序： "<< ((dm::lowByte_u16(u16Tmp)==0x01)?"通过":"异常")<<endl;
	printf("%04X -> %02X %02X\n",u16Tmp,dm::lowByte_u16(u16Tmp),dm::higByte_u16(u16Tmp));

	cout << "uint32:"<<sizeof(dm::uint32)<<(sizeof(dm::uint32)==4?"通过":"异常")<<endl;
	cout << "int32 :"<<sizeof(dm::int32)<<(sizeof(dm::int32)==4?"通过":"异常")<<endl;

	cout << "uint64:"<<sizeof(dm::uint64)<<(sizeof(dm::uint64)==8?"通过":"异常")<<endl;
	cout << "int64 :"<<sizeof(dm::int64)<<(sizeof(dm::int64)==8?"通过":"异常")<<endl;
	cout << "float32:"<<sizeof(dm::float32)<<(sizeof(dm::float32)==4?"通过":"异常")<<endl;
	cout << "float64:"<<sizeof(dm::float64)<<(sizeof(dm::float64)==8?"通过":"异常")<<endl;

	f32Tmp = 0.1f;
	cout <<"浮点数字节从低到高("<<f32Tmp<<"):";
	for( int i=0;i<4;++i )
		printf("%02X ",dm::lowByte_f32(f32Tmp,i));
	cout <<endl;

	cout <<"\n测试消息结构:"<<endl;
	cout <<"para    :"<<sizeof(dm::msg::SParaInfo)<<(sizeof(dm::msg::SParaInfo)==8?"通过":"异常")<<endl;
	cout <<"paradata:"<<sizeof(dm::msg::SParaData)<<(sizeof(dm::msg::SParaData)==(512+8+sizeof(int))?"通过":"异常")<<endl;

#ifndef WIN32
	struct timespec ts;

	cout <<"\n悬挂时钟：";
	if( 0==clock_gettime(CLOCK_REALTIME,&ts) ){
		cout <<"sec "<<ts.tv_sec<<",nsec"<<ts.tv_nsec;
	}else{
		perror("获取失败");
	}

	cout <<" 精度：";
	if( 0==clock_getres(CLOCK_REALTIME,&ts) ){
		cout <<"sec "<<ts.tv_sec<<",nsec"<<ts.tv_nsec;
	}else{
		perror("获取失败");
	}

	cout <<"\n开机运行时钟：";
	if( 0==clock_gettime(CLOCK_MONOTONIC,&ts) ){
		cout <<"sec "<<ts.tv_sec<<",nsec"<<ts.tv_nsec;
	}else{
		perror("获取失败");
	}

	cout <<" 精度：";
	if( 0==clock_getres(CLOCK_MONOTONIC,&ts) ){
		cout <<"sec "<<ts.tv_sec<<",nsec"<<ts.tv_nsec;
	}else{
		perror("获取失败");
	}
#endif
	return 0;
}

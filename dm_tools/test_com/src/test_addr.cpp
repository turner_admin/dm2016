/*
 * test_addr.cpp
 *
 *  Created on: 2018年10月11日
 *      Author: turner
 */

#include <dm/os/com/serialaddr.hpp>
#include <dm/os/com/canaddr.hpp>
#include <dm/os/com/tcpclientaddr.hpp>
#include <dm/os/com/tcpbindclientaddr.hpp>
#include <dm/os/com/tcpconnectaddr.hpp>
#include <dm/os/com/tcpserveraddr.hpp>
#include <dm/os/com/udpclientaddr.hpp>
#include <dm/os/com/udpbindclientaddr.hpp>
#include <dm/os/com/udpconnectaddr.hpp>
#include <dm/os/com/udpserveraddr.hpp>

#include <iostream>
#include <boost/program_options.hpp>

int main( int argc,char* argv ){
	options_description desc("DM2016测试通信地址工具");
	desc.add_options()
			("help,h","显示本帮助信息")
			("version,v","显示版本及编译信息")
			("addr",value<string>(),"设备地址\n SRP:/dev/ttyS1:115200:8:0:0:0\n USR:localhost:2013\n UCL:server:2013\n UBL:localhost:2013:server:2013\n TSR:localhost:2013\n TCL:server:2013")
			("serial",value<string>(),"串口设备 格式:/dev/ttyS1:115200:8:0:0:0")
			("can",value<string>(),"CAN地址 格式:can0:0:0:0")
			;

	variables_map vm;
	try{
		store(parse_command_line(argc,argv,desc),vm);
		notify(vm);
	}catch( std::exception& ex ){
		cout << ex.what()<<endl;
		return 1;
	}

	if( vm.count("version") ){
		cout <<"V1.0 构建于 "<<__DATE__<<endl;
		return 1;
	}

	if( vm.count("help") ){
		cout <<desc<<endl;
		return 1;
	}
}



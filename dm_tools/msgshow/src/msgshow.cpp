﻿/*
 * msgshow.cpp
 *
 *  Created on: 2016年11月13日
 *      Author: work
 */

#include <boost/program_options.hpp>
#include <dm/os/msg/msgagent.hpp>
#include <dm/scada/scada.hpp>
#include <dm/scada/remotectlmgr.hpp>
#include <dm/scada/parametermgr.hpp>

#include <dm/datetime.hpp>
#include <string>
#include <cstdio>
#include <iostream>

#ifdef WIN32
#include <Windows.h>
#else
#include <unistd.h>
#endif

using namespace std;
using namespace boost::program_options;
using namespace dm::os::msg;

void showParaData( const dm::msg::SParaData& pd ){
	switch( pd.type ){
	case dm::msg::SParaData::DtBinary:
		if( pd.num>pd.maxNum_uint8() )
			cout <<"数据个数超出范围."<<endl;
		else if( pd.num>0 ){
			printf("0x%02X",pd.as_binary()[0]);
			for( int i=1;i<pd.num;++i )
				printf(",0x%02X",pd.as_binary()[i]);
		}
		break;
	case dm::msg::SParaData::DtInt8:
		if( pd.num>pd.maxNum_int8() )
			cout <<"数据个数超出范围."<<endl;
		else if( pd.num>0 ){
			printf("%d",pd.as_int8()[0]);
			for( int i=1;i<pd.num;++i )
				printf(",%d",pd.as_int8()[i]);
		}
		break;
	case dm::msg::SParaData::DtUint8:
		if( pd.num>pd.maxNum_uint8() )
			cout <<"数据个数超出范围."<<endl;
		else if( pd.num>0 ){
			printf("%u",pd.as_uint8()[0]);
			for( int i=1;i<pd.num;++i )
				printf(",%u",pd.as_uint8()[i]);
		}
		break;
	case dm::msg::SParaData::DtInt16:
		if( pd.num>pd.maxNum_uint16() )
			cout <<"数据个数超出范围."<<endl;
		else if( pd.num>0 ){
			printf("%d",pd.as_int16()[0]);
			for( int i=1;i<pd.num;++i )
				printf(",%d",pd.as_int16()[i]);
		}
		break;
	case dm::msg::SParaData::DtUint16:
		if( pd.num>pd.maxNum_uint16() )
			cout <<"数据个数超出范围."<<endl;
		else if( pd.num>0 ){
			printf("%u",pd.as_uint16()[0]);
			for( int i=1;i<pd.num;++i )
				printf(",%u",pd.as_uint16()[i]);
		}
		break;
	case dm::msg::SParaData::DtInt32:
		if( pd.num>pd.maxNum_int32() )
			cout <<"数据个数超出范围."<<endl;
		else if( pd.num>0 ){
			printf("%d",pd.as_int32()[0]);
			for( int i=1;i<pd.num;++i )
				printf(",%d",pd.as_int32()[i]);
		}
		break;
	case dm::msg::SParaData::DtUint32:
		if( pd.num>pd.maxNum_uint32() )
			cout <<"数据个数超出范围."<<endl;
		else if( pd.num>0 ){
			printf("%u",pd.as_uint32()[0]);
			for( int i=1;i<pd.num;++i )
				printf(",%u",pd.as_uint32()[i]);
		}
		break;
	case dm::msg::SParaData::DtInt64:
		if( pd.num>pd.maxNum_int64() )
			cout <<"数据个数超出范围."<<endl;
		else if( pd.num>0 ){
			printf("%lld",pd.as_int64()[0]);
			for( int i=1;i<pd.num;++i )
				printf(",%lld",pd.as_int64()[i]);
		}
		break;
	case dm::msg::SParaData::DtUint64:
		if( pd.num>pd.maxNum_uint64() )
			cout <<"数据个数超出范围."<<endl;
		else if( pd.num>0 ){
			printf("%llu",pd.as_uint64()[0]);
			for( int i=1;i<pd.num;++i )
				printf(",%llu",pd.as_uint64()[i]);
		}
		break;
	case dm::msg::SParaData::DtFloat32:
		if( pd.num>pd.maxNum_float32() )
			cout <<"数据个数超出范围."<<endl;
		else if( pd.num>0 ){
			printf("%f",pd.as_float32()[0]);
			for( int i=1;i<pd.num;++i )
				printf(",%f",pd.as_float32()[i]);
		}
		break;
	case dm::msg::SParaData::DtFloat64:
		if( pd.num>pd.maxNum_float64() )
			cout <<"数据个数超出范围."<<endl;
		else if( pd.num>0 ){
			printf("%lf",pd.as_float32()[0]);
			for( int i=1;i<pd.num;++i )
				printf(",%lf",pd.as_float32()[i]);
		}
		break;
	case dm::msg::SParaData::DtString:
		if( pd.num>pd.maxNum_string() )
			cout <<"数据个数超出范围."<<endl;
		else if( pd.num>0 ){
			printf("%c",pd.as_string()[0]);
			for( int i=1;i<pd.num;++i )
				printf("%c",pd.as_string()[i]);
		}
		break;
	}
}

void showMsg( const CMsgInfo& info,const bool& overFlow ){
	dm::CDateTime dt = info.getTs();

	cout <<dt.toString()<<" ";
	if( overFlow )
		cout <<"[O] ";

	cout <<info.getMod()<<' ';

	cout << dm::msg::msgTypeString(info.getType())<<":"
			<<dm::msg::msgAckString(info.getAck());

	switch( info.getType() ){
	case dm::msg::Msg_Call:
		cout <<" 召唤组号:"<<info.getCall().group;
		break;
	case dm::msg::Msg_RemoteCtl:
		cout <<" 命令："<<dm::msg::msgRemoteCtlString(info.getRemoteCtl().remoteCtl)
			<<" 名称:" <<dm::scada::CRemoteCtlMgr::ins().name(info.getRemoteCtl().index)
			<<" 地址:"<<info.getRemoteCtl().index
			<<" "<<dm::scada::CRemoteCtlMgr::ins().deviceDesc(info.getRemoteCtl().index)<<" "<<dm::scada::CRemoteCtlMgr::ins().desc(info.getRemoteCtl().index)<<" ";
		if( info.getRemoteCtl().remoteCtl==dm::msg::Rc_Open )
			cout <<dm::scada::CRemoteCtlMgr::ins().openDesc(info.getRemoteCtl().index);
		else if( info.getRemoteCtl().remoteCtl==dm::msg::Rc_PreOpen )
			cout <<dm::scada::CRemoteCtlMgr::ins().preOpenDesc(info.getRemoteCtl().index);
		else if( info.getRemoteCtl().remoteCtl==dm::msg::Rc_Close )
			cout <<dm::scada::CRemoteCtlMgr::ins().closeDesc(info.getRemoteCtl().index);
		else if( info.getRemoteCtl().remoteCtl==dm::msg::Rc_PreClose )
			cout <<dm::scada::CRemoteCtlMgr::ins().preCloseDesc(info.getRemoteCtl().index);
		else
			cout <<"未知命令";

		break;
	case dm::msg::Msg_Set:
		if( info.getAck()==dm::msg::MaAck || info.getAck()==dm::msg::MaNoAck ){
			cout <<" 名称:"<<dm::scada::CParameterMgr::ins().name(info.getParaData().index)
					<<" 地址:"<<info.getParaData().index <<" 个数:"<<info.getParaData().num
					<<" 描述:"<<dm::scada::CParameterMgr::ins().deviceDesc(info.getParaData().index)
					<<" "<<dm::scada::CParameterMgr::ins().desc(info.getParaData().index)
					<<" 类型:"<<info.getParaData().getTypeString()<<" ";
			showParaData(info.getParaData());
		}else{
			cout <<" 名称:"<<dm::scada::CParameterMgr::ins().name(info.getPara().index)
					<<" 地址:"<<info.getPara().index <<" 个数:"<<info.getPara().num
					<<" 描述:"<<dm::scada::CParameterMgr::ins().deviceDesc(info.getPara().index)
					<<" "<<dm::scada::CParameterMgr::ins().desc(info.getParaData().index);
		}

		break;
	case dm::msg::Msg_Get:
		if( info.getAck()==dm::msg::MaAck || info.getAck()==dm::msg::MaNoAck ){
			cout <<" 地址:"<<info.getPara().index <<" 个数:"<<info.getPara().num
					<<" "<<" "<<dm::scada::CParameterMgr::ins().desc(info.getParaData().index);
		}else{
			cout <<" 地址:"<<info.getParaData().index <<" 个数:"<<info.getParaData().num<<" 类型:"<<info.getParaData().getTypeString()
					<<" "<<" "<<dm::scada::CParameterMgr::ins().desc(info.getParaData().index);
			cout <<endl;
			showParaData(info.getParaData());
		}

		break;
	case dm::msg::Msg_Update:
		if( info.getAck()!=dm::msg::MaAck && info.getAck()!=dm::msg::MaNoAck ){
			cout <<" 地址:"<<info.getPara().index <<" 个数:"<<info.getPara().num
					<<" "<<" "<<dm::scada::CParameterMgr::ins().desc(info.getParaData().index);
		}else{
			cout <<" 地址:"<<info.getParaData().index <<" 个数:"<<info.getParaData().num<<" 类型:"<<info.getParaData().getTypeString()
					<<" "<<" "<<dm::scada::CParameterMgr::ins().desc(info.getParaData().index);
			cout <<endl;
			showParaData(info.getParaData());
		}

		break;
	case dm::msg::Msg_SyncData:
		cout <<" 同步内容:"<<dm::msg::msgSyncTypeString(info.getSync().type);
		break;
	default:
		break;
	}

	cout <<endl;
}

int main( int argc,char* argv[] ){
	options_description desc("DM系统消息工具.");
	desc.add_options()
			("help,h","显示帮助信息.")
			("version,v","显示版本信息.")
			("reset","消息系统复位")
			("ack",value<string>(),"应答消息类型:no,need,success,part,fail,timeout,deny,busy,err-paras,err-index")
			("test","测试消息")
			("call",value<int>(),"召唤消息,指定组号。默认0")
			("clock","对时消息")
			("remote-ctl",value<string>(),"远控消息:open,close,pre-open,pre-close")
			("paras",value<string>(),"参数消息:read,write,update")
			("sync",value<string>(),"数据同步消息:status,discrete,measure,cumulant,all")
			("remote-reset","远程复位消息")
			("id",value<int>(),"信息点ID")
			("num",value<int>(),"信息个数")
			("binary",value<vector<dm::uint16> >(),"整数表示的数据.在参数读反馈消息或者参数写消息中使用")
			("int8",value<vector<dm::int16> >(),"int8格式数据.在参数读反馈消息或者参数写消息中使用")
			("uint8",value<vector<dm::uint16> >(),"Uint8格式数据.在参数读反馈消息或者参数写消息中使用")
			("int16",value<vector<dm::int16> >(),"Int16格式数据.在参数读反馈消息或者参数写消息中使用")
			("uint16",value<vector<dm::uint16> >(),"Uint16格式数据.在参数读反馈消息或者参数写消息中使用")
			("int32",value<vector<dm::int32> >(),"Int32格式数据.在参数读反馈消息或者参数写消息中使用")
			("uint32",value<vector<dm::uint32> >(),"Uint32格式数据.在参数读反馈消息或者参数写消息中使用")
			("int64",value<vector<dm::int64> >(),"Int64格式数据.在参数读反馈消息或者参数写消息中使用")
			("uint64",value<vector<dm::uint64> >(),"Uint64格式数据.在参数读反馈消息或者参数写消息中使用")
			("float32",value<vector<dm::float32> >(),"Float32格式数据.在参数读反馈消息或者参数写消息中使用")
			("float64",value<vector<dm::float64> >(),"Float64格式数据.在参数读反馈消息或者参数写消息中使用")
			("string",value<string>(),"字符串表示的数据.在参数读反馈消息或者参数写消息中使用")
			("sync",value<string>(),"同步消息:none,status,discrete,measure,cumulant,all")
			("auto,a","自动发送")
			("interval,i",value<int>(),"发送间隔。单位:毫秒")
			("wait,w",value<int>(),"在单次发送消息后，等待消息时长。单位:毫秒。0表示长时间等待。")
			;

	variables_map vm;
	try{
		store(parse_command_line(argc,argv,desc),vm);
		notify(vm);
	}catch( std::exception& ex ){
		cout << ex.what()<<endl;
		return 1;
	}

	if( vm.count("version") ){
		cout <<"build on "<<__DATE__<<endl;
		return 1;
	}

	if( vm.count("help") ){
		cout <<desc<<endl;
		return 1;
	}

	dm::os::msg::CMsgAgent msgAgent;

	if( vm.count("reset") ){
		cout <<"复位消息系统...";
		msgAgent.remove();
		cout <<" 成功"<<endl;
		return 0;
	}

	// 是否自动发送
	bool au = false;

	// 发送间隔
	int interval = 10000;

	if( vm.count("auto") )
		au = true;

	if( vm.count("interval") )
		interval = vm["interval"].as<int>();

	dm::os::msg::CMsgInfo msg;

	if( vm.count("ack") ){
		string s = vm["ack"].as<string>();
		// no,need,success,part,fail,timeout,deny,busy,err-paras,err-index
		if( s=="need" )
			msg.setAck( dm::msg::MaAck );
		else if( s=="success" )
			msg.setAck( dm::msg::MaSuccess );
		else if( s=="part" )
			msg.setAck( dm::msg::MaPart );
		else if( s=="fail" )
			msg.setAck( dm::msg::MaFail );
		else if( s=="timeout")
			msg.setAck( dm::msg::MaTimeout );
		else if( s=="deny")
			msg.setAck( dm::msg::MaDeny );
		else if( s=="busy")
			msg.setAck( dm::msg::MaBusy );
		else if( s=="err-paras")
			msg.setAck( dm::msg::MaParas );
		else if( s=="err-index")
			msg.setAck( dm::msg::MaIndex );
		else{
			cout <<"选项--ack不支持该值："<<s<<endl;
			return -1;
		}
	}

	int id = -1;
	int index;
	if( vm.count("id") ){
		id = vm["id"].as<int>();
		if( id<0 ){
			cout <<"选项--id必须指定一个正确的值"<<endl;
			return -1;
		}
	}

	int num = 1;
	if( vm.count("num") ){
		num = vm["num"].as<int>();
		if( num<0 ){
			cout <<"选项--num必须指定一个正确的值"<<endl;
			return -1;
		}
	}

	if( vm.count("test") ){
		msg.setType(dm::msg::Msg_Test);
	}else if( vm.count("call") ){
		msg.setType(dm::msg::Msg_Call);
		msg.getCall().group = vm["call"].as<int>();
	}else if( vm.count("clock") ){
		msg.setType(dm::msg::Msg_SyncClock );
	}else if( vm.count("remote-ctl")){
		msg.setType(dm::msg::Msg_RemoteCtl );

		string s = vm["remote-ctl"].as<string>();
		if( s=="open" )
			msg.getRemoteCtl().remoteCtl = dm::msg::Rc_Open;
		else if( s=="close" )
			msg.getRemoteCtl().remoteCtl = dm::msg::Rc_Close;
		else if( s=="pre-open" )
			msg.getRemoteCtl().remoteCtl = dm::msg::Rc_PreOpen;
		else if( s=="pre-close" )
			msg.getRemoteCtl().remoteCtl = dm::msg::Rc_PreClose;
		else{
			cout <<"选项--remote-ctl必须指定一个正确的值"<<endl;
			return -1;
		}

		if( id<0 ){
			cout <<"必须使用选项--id设置远控点ID"<<endl;
			return -1;
		}

		index = dm::scada::CRemoteCtlMgr::ins().indexById(id);
		if( index<0 ){
			cout <<"系统内没有定义id为"<<id<<"的远控信息"<<endl;
			return -1;
		}

		msg.getRemoteCtl().index = index;
	}else if( vm.count("paras") ){
		string s = vm["paras"].as<string>();
		if( s=="read" ){
			msg.setType( dm::msg::Msg_Get );
		}else if( s=="write" ){
			msg.setType( dm::msg::Msg_Set );
		}else if( s=="update" ){
			msg.setType( dm::msg::Msg_Update );
		}else{
			cout <<"选项--paras必须指定一个正确的值"<<endl;
			return -1;
		}

		if( id<0 ){
			cout <<"必须使用选项--id设置参数起始ID"<<endl;
			return -1;
		}

		index = dm::scada::CParameterMgr::ins().indexById(id);
		if( index<0 ){
			cout <<"系统内没有定义id为"<<id<<"的参数"<<endl;
			return -1;
		}

		if( (msg.getType()==dm::msg::Msg_Get && (msg.getAck()==dm::msg::MaSuccess || msg.getAck()==dm::msg::MaPart))
				|| (msg.getType()==dm::msg::Msg_Set &&(msg.getAck()==dm::msg::MaAck || msg.getAck()==dm::msg::MaNoAck))
				|| (msg.getType()==dm::msg::Msg_Update && (msg.getAck()==dm::msg::MaAck || msg.getAck()==dm::msg::MaNoAck))){
			msg.getParaData().index = index;

			if( vm.count("binary") ){
				vector<dm::uint16> d = vm["binary"].as<vector<dm::uint16> >();
				if( d.size()>(unsigned int)msg.getParaData().maxNum_binary() ){
					cout <<"选项--binary指定的数据超过了允许的最多数量"<<msg.getParaData().maxNum_binary()<<endl;
					return -1;
				}

				msg.getParaData().num = d.size();
				msg.getParaData().type = msg.getParaData().DtBinary;

				for( unsigned int i=0;i<d.size();++i )
					msg.getParaData().as_binary()[i] = d.at(i);
			}else if( vm.count("int8") ){
				vector<dm::int16> d = vm["int8"].as<vector<dm::int16> >();
				if( d.size()>(unsigned int)msg.getParaData().maxNum_int8() ){
					cout <<"选项--int8指定的数据超过了允许的最多数量"<<msg.getParaData().maxNum_int8()<<endl;
					return -1;
				}

				msg.getParaData().num = d.size();
				msg.getParaData().type = msg.getParaData().DtInt8;

				for( unsigned int i=0;i<d.size();++i )
					msg.getParaData().as_int8()[i] = d.at(i);
			}else if( vm.count("uint8") ){
				vector<dm::uint16> d = vm["uint8"].as<vector<dm::uint16> >();
				if( d.size()>(unsigned int)msg.getParaData().maxNum_uint8() ){
					cout <<"选项--uint8指定的数据超过了允许的最多数量"<<msg.getParaData().maxNum_uint8()<<endl;
					return -1;
				}

				msg.getParaData().num = d.size();
				msg.getParaData().type = msg.getParaData().DtUint8;

				for( unsigned int i=0;i<d.size();++i )
					msg.getParaData().as_uint8()[i] = d.at(i);
			}else if( vm.count("int16") ){
				vector<dm::int16> d = vm["int16"].as<vector<dm::int16> >();
				if( d.size()>(unsigned int)msg.getParaData().maxNum_int16() ){
					cout <<"选项--int16指定的数据超过了允许的最多数量"<<msg.getParaData().maxNum_int16()<<endl;
					return -1;
				}

				msg.getParaData().num = d.size();
				msg.getParaData().type = msg.getParaData().DtInt16;

				for( unsigned int i=0;i<d.size();++i )
					msg.getParaData().as_int16()[i] = d.at(i);
			}else if( vm.count("uint16") ){
				vector<dm::uint16> d = vm["uint16"].as<vector<dm::uint16> >();
				if( d.size()>(unsigned int)msg.getParaData().maxNum_uint16() ){
					cout <<"选项--uint16指定的数据超过了允许的最多数量"<<msg.getParaData().maxNum_uint16()<<endl;
					return -1;
				}

				msg.getParaData().num = d.size();
				msg.getParaData().type = msg.getParaData().DtUint16;

				for( unsigned int i=0;i<d.size();++i )
					msg.getParaData().as_uint16()[i] = d.at(i);
			}else if( vm.count("int32") ){
				vector<dm::int32> d = vm["int32"].as<vector<dm::int32> >();
				if( d.size()>(unsigned int)msg.getParaData().maxNum_int32() ){
					cout <<"选项--int32指定的数据超过了允许的最多数量"<<msg.getParaData().maxNum_int32()<<endl;
					return -1;
				}

				msg.getParaData().num = d.size();
				msg.getParaData().type = msg.getParaData().DtInt32;

				for( unsigned int i=0;i<d.size();++i )
					msg.getParaData().as_int32()[i] = d.at(i);
			}else if( vm.count("uint32") ){
				vector<dm::uint32> d = vm["uint32"].as<vector<dm::uint32> >();
				if( d.size()>(unsigned int)msg.getParaData().maxNum_uint32() ){
					cout <<"选项--uint32指定的数据超过了允许的最多数量"<<msg.getParaData().maxNum_uint32()<<endl;
					return -1;
				}

				msg.getParaData().num = d.size();
				msg.getParaData().type = msg.getParaData().DtUint32;

				for( unsigned int i=0;i<d.size();++i )
					msg.getParaData().as_uint32()[i] = d.at(i);
			}else if( vm.count("int64") ){
				vector<dm::int64> d = vm["int64"].as<vector<dm::int64> >();
				if( d.size()>(unsigned int)msg.getParaData().maxNum_int64() ){
					cout <<"选项--int64指定的数据超过了允许的最多数量"<<msg.getParaData().maxNum_int64()<<endl;
					return -1;
				}

				msg.getParaData().num = d.size();
				msg.getParaData().type = msg.getParaData().DtInt64;

				for( unsigned int i=0;i<d.size();++i )
					msg.getParaData().as_int64()[i] = d.at(i);
			}else if( vm.count("uint64") ){
				vector<dm::uint64> d = vm["uint64"].as<vector<dm::uint64> >();
				if( d.size()>(unsigned int)msg.getParaData().maxNum_uint64() ){
					cout <<"选项--uint64指定的数据超过了允许的最多数量"<<msg.getParaData().maxNum_uint64()<<endl;
					return -1;
				}

				msg.getParaData().num = d.size();
				msg.getParaData().type = msg.getParaData().DtUint64;

				for( unsigned int i=0;i<d.size();++i )
					msg.getParaData().as_uint64()[i] = d.at(i);
			}else if( vm.count("float32") ){
				vector<dm::float32> d = vm["float32"].as<vector<dm::float32> >();
				if( d.size()>(unsigned int)msg.getParaData().maxNum_float32() ){
					cout <<"选项--float32指定的数据超过了允许的最多数量"<<msg.getParaData().maxNum_float32()<<endl;
					return -1;
				}

				msg.getParaData().num = d.size();
				msg.getParaData().type = msg.getParaData().DtFloat32;

				for( unsigned int i=0;i<d.size();++i )
					msg.getParaData().as_float32()[i] = d.at(i);
			}else if( vm.count("float64") ){
				vector<dm::float64> d = vm["float64"].as<vector<dm::float64> >();
				if( d.size()>(unsigned int)msg.getParaData().maxNum_float64() ){
					cout <<"选项--float64指定的数据超过了允许的最多数量"<<msg.getParaData().maxNum_float64()<<endl;
					return -1;
				}

				msg.getParaData().num = d.size();
				msg.getParaData().type = msg.getParaData().DtFloat64;

				for( unsigned int i=0;i<d.size();++i )
					msg.getParaData().as_float64()[i] = d.at(i);
			}else if( vm.count("string") ){
				string s = vm["string"].as<string>();
				if( s.size()>(unsigned int)msg.getParaData().maxNum_string() ){
					cout <<"选项--string指定的数据超过了允许的最多数量"<<msg.getParaData().maxNum_string()<<endl;
					return -1;
				}

				msg.getParaData().num = s.size();
				msg.getParaData().type = msg.getParaData().DtString;

				for( unsigned int i=0;i<s.size();++i )
					msg.getParaData().as_string()[i] = s.at(i);
			}else{
				cout <<"未指定数据"<<endl;
				return -1;
			}
		}else{
			msg.getPara().index = index;

			if( num<0 ){
				cout <<"必须使用选项--num设置参数个数"<<endl;
				return -1;
			}

			msg.getPara().num = num;
		}
	}else if( vm.count("sync")){
		msg.setType( dm::msg::Msg_SyncData );

		// status,discrete,measure,cumulant,all
		string s = vm["sync"].as<string>();
		if( s=="status" )
			msg.getSync().type = dm::msg::SDT_Status;
		else if( s=="discrete" )
			msg.getSync().type = dm::msg::SDT_Discrete;
		else if( s=="measure" )
			msg.getSync().type = dm::msg::SDT_Measure;
		else if( s=="cumulant" )
			msg.getSync().type = dm::msg::SDT_Cumulant;
		else if( s=="all" )
			msg.getSync().type = dm::msg::SDT_All;
		else{
			cout <<"选项--sync必须指定一个正确的值"<<endl;
			return -1;
		}
	}else if( vm.count("remote-reset") ){
		msg.setType( dm::msg::Msg_RemoteReset );
	}else{
		cout <<"开始监听消息..."<<endl;
		while( true ){
			if( msgAgent.tryGetRequeset(msg) )
				showMsg(msg,false);

			if( msgAgent.tryGetAnswer(msg) )
				showMsg(msg,false);

#ifdef WIN32
			Sleep(interval);
#else
			usleep(interval);
#endif
		}
	}

	// 发送消息会进入本过程
	while( true ){
		cout <<"发送消息:";
		msgAgent.sendMsg(msg);
		showMsg(msg,false);

		if( !au )
			break;

#ifdef WIN32
		Sleep(interval);
#else
		usleep(interval);
#endif
	}

	// 单次发送消息会进入本过程
	if( !vm.count("wait") )
		return 0;

	// 等待时长
	int waitMSec = vm["wait"].as<int>();

	dm::CTimeStamp delay( dm::CTimeStamp::cur());
	while( true ){
		if( waitMSec>0 && delay.isTimeout_msec(waitMSec) )
			break;

		if( msgAgent.tryGetRequeset(msg) )
			showMsg(msg,false);

		if( msgAgent.tryGetAnswer(msg) )
			showMsg(msg,false);
	}

	return 0;
}



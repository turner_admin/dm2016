﻿/*
l * histool.cpp
 *
 *  Created on: 2017年7月13日
 *      Author: work
 */

#include <boost/program_options.hpp>

#include <dm/os/options.hpp>

#include <iostream>
#include <string>

using namespace std;
using namespace boost::program_options;

int main( int argc,char* argv[] ){
	options_description desc("【DM2016帮助文档】\n 系统选项工具。\n\t基于DM2016系统构建\n支持以下选项");

	options_description opts_generic("一般选项");
	opts_generic.add_options()
			("help,h","显示本帮助信息")
			("version,v","显示版本及编译信息")
			;
	desc.add(opts_generic);

	options_description opts_cmd("命令选项");
	opts_cmd.add_options()
			("info","查询选项信息")
			("show","显示信息")
			("update","更新选项")
			("add","增加选项")
			("clear,c","清除记录")
			("init","重新初始化选项表")
			;
	desc.add(opts_cmd);

	options_description opts_paras("辅助参数");
	opts_paras.add_options()
			("confirm","确认")
			("group,g",value<string>(),"指定组")
			("option,o",value<string>(),"指定选项")
			("type,t",value<string>(),"值类型。bool,int,float,string")
			("value,v",value<string>(),"设定值")
			;
	desc.add(opts_paras);

	variables_map vm;
	try{
		store(parse_command_line(argc,argv,desc),vm);
		notify(vm);
	}catch( std::exception& ex ){
		cout << ex.what()<<endl;
		return 1;
	}

	if( vm.count("version") ){
		cout <<"V1.0 构建于 "<<__DATE__<<endl;
		return 1;
	}

	if( vm.count("help") ){
		cout <<desc<<endl;
		return 1;
	}

	dm::env::COptions op;
	if( vm.count("show") ){
		if( vm.count("option") ){
			if( vm.count("group") ){
				op.show(vm["option"].as<string>().c_str(),vm["group"].as<string>().c_str());
			}else{
				op.show(vm["option"].as<string>().c_str(),NULL);
			}
		}else{
			if( vm.count("group") ){
				op.show(NULL,vm["group"].as<string>().c_str());
			}else{
				op.show(NULL,NULL);
			}
		}
	}else	if( vm.count("info") ){
		if( vm.count("option") ){
			cout <<"组名.选项名\t布尔值\t整数值\t小数值\t字符串值"<<endl;
			if( vm.count("group") ){
				cout <<vm["group"].as<string>()<<"."<<vm["option"].as<string>()<<'\t';

				bool bv;
				if( op.get(vm["option"].as<string>().c_str(),bv,vm["group"].as<string>().c_str()) )
					cout << (bv?"true":"false");
				else
					cout <<"不存在";

				cout <<'\t';
				int iv;
				if( op.get(vm["option"].as<string>().c_str(),iv,vm["group"].as<string>().c_str()) )
					cout <<iv;
				else
					cout <<"不存在";

				cout <<'\t';

				float fv;
				if( op.get(vm["option"].as<string>().c_str(),fv,vm["group"].as<string>().c_str()) )
					cout <<fv;
				else
					cout <<"不存在";

				cout <<'\t';

				string sv;
				if( op.get(vm["option"].as<string>().c_str(),sv,vm["group"].as<string>().c_str()) )
					cout <<sv;
				else
					cout <<"不存在";

				cout <<endl;
			}else{
				cout <<vm["option"].as<string>()<<'\t';

				bool bv;
				if( op.get(vm["option"].as<string>().c_str(),bv) )
					cout << (bv?"true":"false");
				else
					cout <<"不存在";

				cout <<'\t';
				int iv;
				if( op.get(vm["option"].as<string>().c_str(),iv) )
					cout <<iv;
				else
					cout <<"不存在";

				cout <<'\t';

				float fv;
				if( op.get(vm["option"].as<string>().c_str(),fv) )
					cout <<fv;
				else
					cout <<"不存在";

				cout <<'\t';

				string sv;
				if( op.get(vm["option"].as<string>().c_str(),sv) )
					cout <<sv;
				else
					cout <<"不存在";

				cout <<endl;
			}
		}else{
			cout <<"请使用--option选项指定选项名"<<endl;
			return 0;
		}
	}else if( vm.count("update") ){
		if( !vm.count("option") ){
			cout <<"请使用--option选项指定选项名"<<endl;
			return 0;
		}

		if( !vm.count("type")  ){
			cout <<"请使用--type选项指定选项值类型"<<endl;
			return 0;
		}

		if( !vm.count("value")  ){
			cout <<"请使用--value选项指定选项值"<<endl;
			return 0;
		}

		if( vm["type"].as<string>()=="bool" ){
			if( vm.count("group") ){
				if( op.set(vm["option"].as<string>().c_str(),vm["value"].as<string>()=="true",vm["group"].as<string>().c_str()) )
					cout <<"更新成功"<<endl;
				else
					cout <<"更新失败"<<endl;
			}else{
				if( op.set(vm["option"].as<string>().c_str(),vm["value"].as<string>()=="true") )
					cout <<"更新成功"<<endl;
				else
					cout <<"更新失败"<<endl;
			}
		}else if( vm["type"].as<string>()=="int" ){
			if( vm.count("group") ){
				if( op.set(vm["option"].as<string>().c_str(),atoi(vm["value"].as<string>().c_str()),vm["group"].as<string>().c_str()) )
					cout <<"更新成功"<<endl;
				else
					cout <<"更新失败"<<endl;
			}else{
				if( op.set(vm["option"].as<string>().c_str(),atoi(vm["value"].as<string>().c_str())) )
					cout <<"更新成功"<<endl;
				else
					cout <<"更新失败"<<endl;
			}
		}else if( vm["type"].as<string>()=="float" ){
			if( vm.count("group") ){
				if( op.set(vm["option"].as<string>().c_str(),float(atof(vm["value"].as<string>().c_str())),vm["group"].as<string>().c_str()) )
					cout <<"更新成功"<<endl;
				else
					cout <<"更新失败"<<endl;
			}else{
				if( op.set(vm["option"].as<string>().c_str(),float(atof(vm["value"].as<string>().c_str()))) )
					cout <<"更新成功"<<endl;
				else
					cout <<"更新失败"<<endl;
			}
		}else if( vm["type"].as<string>()=="string" ){
			if( vm.count("group") ){
				if( op.set(vm["option"].as<string>().c_str(),vm["value"].as<string>().c_str(),vm["group"].as<string>().c_str()) )
					cout <<"更新成功"<<endl;
				else
					cout <<"更新失败"<<endl;
			}else{
				if( op.set(vm["option"].as<string>().c_str(),vm["value"].as<string>().c_str()) )
					cout <<"更新成功"<<endl;
				else
					cout <<"更新失败"<<endl;
			}
		}else{
			cout <<"选项--type不正确"<<endl;
			return 0;
		}
	}else if( vm.count("add") ){
		if( !vm.count("option") ){
			cout <<"请使用--option选项指定选项名"<<endl;
			return 0;
		}

		if( vm.count("group") ){
			cout <<"添加选项 ";
			if( op.add(vm["option"].as<string>().c_str(),vm["group"].as<string>().c_str()) )
				cout <<"成功"<<endl;
			else
				cout <<"失败"<<endl;
		}else{
			cout <<"添加选项 ";
			if( op.add(vm["option"].as<string>().c_str()) )
				cout <<"成功"<<endl;
			else
				cout <<"失败"<<endl;
		}

	}else if( vm.count("clear") ){
		if( vm.count("option") ){
			if( vm.count("group") ){
				if( !vm.count("confirm") ){
					cout <<"你确定要清除 "<<vm["group"].as<string>()<<"."<<vm["option"].as<string>() <<"的所有系统选项(Y/N):";
					cout.flush();
					if( cin.get()!='Y' )
						return 0;
				}
				op.clear(vm["option"].as<string>().c_str(),vm["group"].as<string>().c_str());
				cout <<"完成"<<endl;
			}else{
				if( !vm.count("confirm") ){
					cout <<"你确定要清除 "<<vm["option"].as<string>() <<"的系统选项(Y/N):";
					cout.flush();
					if( cin.get()!='Y' )
						return 0;
				}
				op.clear(vm["option"].as<string>().c_str());
				cout <<"完成"<<endl;
			}
		}else{
			if( vm.count("group") ){
				if( !vm.count("confirm") ){
					cout <<"你确定要清除 组"<<vm["group"].as<string>()<<"的所有系统选项(Y/N):";
					cout.flush();
					if( cin.get()!='Y' )
						return 0;
				}
				op.clear(NULL,vm["group"].as<string>().c_str());
				cout <<"完成"<<endl;
			}else{
				if( !vm.count("confirm") ){
					cout <<"你确定要清除 所有系统选项(Y/N):";
					cout.flush();
					if( cin.get()!='Y' )
						return 0;
				}
				op.clear();
				cout <<"完成"<<endl;
			}
		}

	}else if( vm.count("init") ){
		if( !vm.count("confirm") ){
			cout <<"你确定要初始化 系统选项表(Y/N):";
			cout.flush();
			if( cin.get()!='Y' )
				return 0;
		}

		cout <<"初始化 系统选项表...";
		cout.flush();
		if( op.init() )
			cout <<"成功";
		else
			cout <<"失败";
		cout <<endl;
	}

	return 0;
}

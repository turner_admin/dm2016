﻿/*
 * hisdata.cpp
 *
 *  Created on: 2017年7月13日
 *      Author: work
 */

#include <iostream>
#include <boost/program_options.hpp>
#include <dm/os/sys/task_agent.hpp>
#include "hisdatamgr.hpp"

using namespace std;
using namespace boost::program_options;

int main( int argc,char* argv[] ){
	options_description desc("历史数据处理程序 @DM2016");
	desc.add_options()
			("help,h","显示本帮助信息")
			("version,v","显示版本及编译信息")
			("delay,d",value<int>()->default_value(10),"进程延时时长")
			("interval,i",value<long>()->default_value(500),"扫描周期：毫秒")
			("task-id",value<int>()->default_value(-1),"任务ID，默认-1")
			;

	variables_map vm;
	try{
		store(parse_command_line(argc,argv,desc),vm);
		notify(vm);
	}catch( std::exception& ex ){
		cout << ex.what()<<endl;
		return 1;
	}

	if( vm.count("version") ){
		cout <<"V1.0 构建于 "<<__DATE__<<endl;
		return 1;
	}

	if( vm.count("help") ){
		cout <<desc<<endl;
		return 1;
	}

	dm::os::sys::CTaskAgent taskAgent(vm["task-id"].as<int>());

	dm::app::CHisDataMgr& mgr = dm::app::CHisDataMgr::ins();

	long i = vm["interval"].as<long>();
	while( taskAgent.runOnce() ){
		mgr.runOnce();
#ifdef WIN32
		Sleep(i);
#else
		usleep(i);
#endif
	}

	return 0;
}

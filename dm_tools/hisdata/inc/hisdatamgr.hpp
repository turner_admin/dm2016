﻿/*
 * hisdatamgr.hpp
 *
 *  Created on: 2017年7月13日
 *      Author: work
 */

#ifndef DM_APPS_HISDATAMGR_HPP_
#define DM_APPS_HISDATAMGR_HPP_

#include <dm/timestamp.hpp>
#include <list>
#include "timetrigger.hpp"
#include <dm/scada/eventmonitor.hpp>
#include <dm/scada/timedmeasuremonitor.hpp>
#include <dm/scada/timedcumulantmonitor.hpp>
#include <dm/scada/scadahis.hpp>

namespace dm{
namespace app{

class CHisDataMgr{
	CHisDataMgr();
	CHisDataMgr( const CHisDataMgr& );

public:
	static CHisDataMgr& ins();

	void runOnce();

protected:
	void getNextTriggerTime( dm::CTimeStamp& ts,const dm::CTimeStamp& now,std::list<CTimeTrigger>& triggers )const;

	void snapStatus( const dm::CTimeStamp& now );
	void snapDiscrete( const dm::CTimeStamp& now );
	void snapMeasure( const dm::CTimeStamp& now );
	void snapCumulant( const dm::CTimeStamp& now );

private:
	// 下次刷新时刻
	dm::CTimeStamp m_nextRefresh;

	// 刷新触发器
	CTimeTrigger m_refreshTrigger;

	bool m_enFrozen;
	// 下次冻结时刻
	dm::CTimeStamp m_nextFroze;

	std::list<CTimeTrigger> m_frozeTriggers;


	bool m_enMeasure;
	bool m_enStatus;
	bool m_enDiscrete;
	bool m_enCumulant;
	bool m_enAction;

	// 清理历史数据触发器
	std::list<CTimeTrigger> m_clearTriggers;
	dm::CTimeStamp m_nextClear;

	// 是否自动清理状态量记录
	bool m_autoClearStatus;
	unsigned int m_statusDuration;	// 状态量历史数据保存天数

	// 是否自动清理离散量
	bool m_autoClearDiscrete;
	unsigned int m_discreteDuration;	// 离散量历史数据保存天数

	bool m_autoClearMeasure;
	unsigned int m_measureDuration;

	bool m_autoClearCumulant;
	unsigned int m_cumulantDuration;

	bool m_autoClearAction;
	unsigned int m_actionDuration;

	dm::scada::CScadaHis& m_his;

	dm::scada::CEventMonitor m_eventMgr;
	dm::scada::CTimedMeasureMonitor m_measureMgr;
	dm::scada::CTimedCumulantMonitor m_cumulantMgr;
};

}
}

#endif /* APPS_HISDATA_INC_HISDATAMGR_HPP_ */

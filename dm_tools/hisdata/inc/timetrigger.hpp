﻿/*
 * timetrigger.hpp
 *
 *  Created on: 2017年7月13日
 *      Author: work
 */

#ifndef DM_APPS_HISDATA_TIMETRIGGER_HPP_
#define DM_APPS_HISDATA_TIMETRIGGER_HPP_

#include <dm/datetime.hpp>
#include <dm/timestamp.hpp>

namespace dm{
namespace app{

class CTimeTrigger{
public:
	enum ECycle{
		Yearly,
		Monthly,
		Daily,
		Hourly,
		Minutely
	};

	CTimeTrigger();
	CTimeTrigger( const CTimeTrigger& );

	CTimeTrigger& operator=( const CTimeTrigger& );

	/**
	 * 设置触发周期
	 * @param cycle
	 */
	void setCycle( const ECycle& cycle );

	/**
	 * 设置触发时刻信息
	 * @param info
	 */
	void setTimeInfo( const dm::CDateTime& info );

	void init();

	/**
	 * 计算下次触发时刻
	 * @param now
	 * @return
	 */
	const dm::CTimeStamp& nextTime( const dm::CTimeStamp& now=dm::CTimeStamp::cur() );

private:
	ECycle m_cycle;
	dm::CDateTime m_timeInfo;
	dm::CTimeStamp m_nextTime;
};

}
}

#endif /* APPS_HISDATA_INC_TIMETRIGGER_HPP_ */

/*
 * comtradefiles.cpp
 *
 *  Created on: 2019年11月14日
 *      Author: turne
 */

#include <dm/app/comtradefiles.hpp>
#include <dm/string/stringref.hpp>

namespace dm{
namespace app{

CComtradeFiles::CRate::CRate( const CRate& rate ):m_samp(rate.m_samp),m_endSamp(rate.m_endSamp){
}

CComtradeFiles::CRate::CRate( const float& samp,const long& endSamp ):m_samp(samp),m_endSamp(endSamp){
}

CComtradeFiles::CRate& CComtradeFiles::CRate::operator=( const CRate& rate ){
	m_samp = rate.m_samp;
	m_endSamp = rate.m_endSamp;

	return *this;
}

CComtradeFiles::CChannelInfo::CChannelInfo():m_chId(NULL),m_ph(NULL),m_ccbm(NULL){
}

CComtradeFiles::CChannelInfo::CChannelInfo( const CChannelInfo& info ):m_chId(NULL),m_ph(NULL),m_ccbm(NULL){
	dm::string::CStringRef(info.m_chId).dumpAutoMalloc(&m_chId);
	dm::string::CStringRef(info.m_ph).dumpAutoMalloc(&m_ph);
	dm::string::CStringRef(info.m_ccbm).dumpAutoMalloc(&m_ccbm);
}

CComtradeFiles::CChannelInfo::~CChannelInfo(){
	if( m_chId )
		delete [] m_chId;
	if( m_ph )
		delete [] m_ph;
	if( m_ccbm )
		delete [] m_ccbm;
}

CComtradeFiles::CChannelInfo& CComtradeFiles::CChannelInfo::operator =( const CChannelInfo& info ){
	dm::string::CStringRef(info.m_chId).dumpAutoMalloc(&m_chId);
	dm::string::CStringRef(info.m_ph).dumpAutoMalloc(&m_ph);
	dm::string::CStringRef(info.m_ccbm).dumpAutoMalloc(&m_ccbm);

	return *this;
}

void CComtradeFiles::CChannelInfo::setChId( const char* v ){
	dm::string::CStringRef(v).dumpAutoMalloc(&m_chId);
}

void CComtradeFiles::CChannelInfo::setPh( const char* v ){
	dm::string::CStringRef(v).dumpAutoMalloc(&m_ph);
}

void CComtradeFiles::CChannelInfo::setCcbm( const char* v ){
	dm::string::CStringRef(v).dumpAutoMalloc(&m_ccbm);
}

CComtradeFiles::CMeasureInfo::CMeasureInfo():CChannelInfo(),
		m_uu(NULL),m_a(1),m_b(0),m_skew(0),m_min(-99999),m_max(99999),m_primary(1),m_secondary(1),m_ps('P'){
}

CComtradeFiles::CMeasureInfo::CMeasureInfo( const CMeasureInfo& info):CChannelInfo(info),
		m_uu(NULL),m_a(info.m_a),m_b(info.m_b),m_skew(info.m_skew),m_min(info.m_min),m_max(info.m_max),
		m_primary(info.m_primary),m_secondary(info.m_secondary),m_ps(info.m_ps){
	dm::string::CStringRef(info.m_uu).dumpAutoMalloc(&m_uu);
}

CComtradeFiles::CMeasureInfo::~CMeasureInfo(){
	if( m_uu )
		delete m_uu;
}

CComtradeFiles::CMeasureInfo& CComtradeFiles::CMeasureInfo::operator =( const CMeasureInfo& info ){
	CChannelInfo::operator =(info);

	dm::string::CStringRef(info.m_uu).dumpAutoMalloc(&m_uu);
	m_a = info.m_a;
	m_b = info.m_b;
	m_skew = info.m_skew;
	m_min = info.m_min;
	m_max = info.m_max;
	m_primary = info.m_primary;
	m_secondary = info.m_secondary;
	m_ps = info.m_ps;

	return *this;
}

void CComtradeFiles::CMeasureInfo::setUu( const char* v ){
	dm::string::CStringRef(v).dumpAutoMalloc(&m_uu);
}

void CComtradeFiles::CMeasureInfo::setA( const float& v ){
	m_a = v;
}

void CComtradeFiles::CMeasureInfo::setB( const float& v ){
	m_b = v;
}

void CComtradeFiles::CMeasureInfo::setSkew( const float& v ){
	m_skew = v;
}

void CComtradeFiles::CMeasureInfo::setMin( const int& v ){
	m_min = v;
}

void CComtradeFiles::CMeasureInfo::setMax( const int& v ){
	m_max = v;
}

void CComtradeFiles::CMeasureInfo::setPrimary( const float& v ){
	m_primary = v;
}

void CComtradeFiles::CMeasureInfo::setSecondary( const float& v ){
	m_secondary = v;
}

void CComtradeFiles::CMeasureInfo::setPs( const char& v ){
	m_ps = v;
}

CComtradeFiles::CStatusInfo::CStatusInfo():CChannelInfo(),m_y(true){
}

CComtradeFiles::CStatusInfo::CStatusInfo( const CStatusInfo& info ):CChannelInfo(info),m_y(info.m_y){
}

CComtradeFiles::CStatusInfo::~CStatusInfo(){
}

CComtradeFiles::CStatusInfo& CComtradeFiles::CStatusInfo::operator =( const CStatusInfo& info ){
	CChannelInfo::operator =(info);
	m_y = info.m_y;

	return *this;
}

void CComtradeFiles::CStatusInfo::setY( const bool& v ){
	m_y = v;
}

//////////////////////////////////////////

CComtradeFiles::CComtradeFiles():m_name(NULL),m_dir(NULL),m_stationName(NULL),m_recDevId(NULL),m_revYear(0),m_if(0),
		m_tsStart(),m_tsTrig(),m_ft(FormatUnknow),m_timeMult(1){
}

CComtradeFiles::~CComtradeFiles(){
	if( m_name )
		delete [] m_name;
	if( m_dir )
		delete [] m_dir;
	if( m_stationName )
		delete [] m_stationName;
	if( m_recDevId )
		delete [] m_recDevId;
}

void CComtradeFiles::setName( const char* name ){
	dm::string::CStringRef(name).dumpAutoMalloc(&m_name);
}

void CComtradeFiles::setDir( const char* s ){
	dm::string::CStringRef(s).dumpAutoMalloc(&m_dir);
}

const CComtradeFiles::CRate& CComtradeFiles::rate( const unsigned int& idx )const{
	return m_rates.at(idx);
}

const CComtradeFiles::CMeasureInfo& CComtradeFiles::measure( const unsigned int& idx )const{
	return m_measures.at(idx);
}

const CComtradeFiles::CStatusInfo& CComtradeFiles::status( const unsigned int& idx )const{
	return m_status.at(idx);
}

unsigned int CComtradeFiles::rows()const{
	if( m_rates.size()>0 )
		return m_rates.back().endSamp();

	return 0;
}

}
}

/*
 * board.hpp
 *
 *  Created on: 2016年2月29日
 *      Author: dylan
 */

#ifndef BOARD_HPP_
#define BOARD_HPP_

#include <dm/hw/dsp281x/clocking.hpp>
#include <dm/app/dsp281x/sciactl.hpp>

#include <board/lt6591b/led.hpp>
#include <board/lt6591b/sram.hpp>

namespace NDm{
namespace NHw{
namespace NLt6591b{

/**
 * 开发板类
 * 提供开发板系统设置，资源对象等。
 */
class CBoard{
	CBoard();
public:
	static CBoard& ins();

	inline const NF281x::CClocking& clock()const{
		return m_clk;
	}

	inline NF281x::CClocking& clock(){
		return m_clk;
	}

	inline const NApp::NF281x::CSciaCtl& scia()const{
		return m_scia;
	}

	inline NApp::NF281x::CSciaCtl& scia(){
		return m_scia;
	}

	inline CLed& led(){
		return CLed::ins();
	}

	inline CSram& sram(){
		return CSram::ins();
	}
private:
	NF281x::CClocking m_clk;
	NApp::NF281x::CSciaCtl& m_scia;
};
}
}
}

#endif /* BOARD_HPP_ */

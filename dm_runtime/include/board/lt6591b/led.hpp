/*
 * led.hpp
 *
 *  Created on: 2016��2��26��
 *      Author: dylan
 */

#ifndef LED_HPP_
#define LED_HPP_

#include <dm/hw/dsp281x/gpa.hpp>

namespace NDm{
namespace NHw{
namespace NLt6591b{

class CLed{
	CLed();
public:
	static CLed& ins();

	inline void dled0On(){
		m_gpa.set(12);
	}

	inline void dled0Off(){
		m_gpa.clear(12);
	}

	inline void dled0Toggle(){
		m_gpa.toggle(12);
	}

	inline void dled1On(){
		m_gpa.set(13);
	}

	inline void dled1Off(){
		m_gpa.clear(13);
	}

	inline void dled1Toggle(){
		m_gpa.toggle(13);
	}

	inline void dled2On(){
		m_gpa.set(14);
	}

	inline void dled2Off(){
		m_gpa.clear(14);
	}

	inline void dled2Toggle(){
		m_gpa.toggle(14);
	}

	inline void dled3On(){
		m_gpa.set(15);
	}

	inline void dled3Off(){
		m_gpa.clear(15);
	}

	inline void dled3Toggle(){
		m_gpa.toggle(15);
	}

	void testOnce();
private:
	NF281x::CGpa m_gpa;
};
}
}
}


#endif /* LED_HPP_ */

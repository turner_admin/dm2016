/*
 * sram.hpp
 *
 *  Created on: 2016��3��9��
 *      Author: dylan
 */

#ifndef SRAM_HPP_
#define SRAM_HPP_

#include <dm/hw/dsp281x/xintfzone2.hpp>

namespace NDm{
namespace NHw{
namespace NLt6591b{

class CSram{
	CSram();
public:
	static CSram& ins();

	inline void* address(){
		return m_zone.address();
	}

	inline const void* address()const{
		return m_zone.address();
	}

	inline unsigned long size()const{
		return 512000ul;
	}

private:
	NDm::NHw::NF281x::CXintfZone2 m_zone;
};
}
}
}

#endif /* SRAM_HPP_ */

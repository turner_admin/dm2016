#ifndef _DM_RINGPOS_HPP_
#define _DM_RINGPOS_HPP_

#include <dm/ringpos_in.hpp>

namespace dm{

template<typename Tp,typename Ts,Ts Size,typename Tc=Tp>
class CRingPos{
public:
	CRingPos( const Tp& pos=0,const Tc& cycle=0 ):m_c(cycle),m_p(pos){}

	CRingPos( const CRingPos& p ):m_c(p.m_c),m_p(p.m_p){}

	template<typename TRingPos>
	inline CRingPos& operator=( const TRingPos& p ){
		m_c = p.getCycle();
		m_p = p.getPos();

		return *this;
	}

	inline const Tp& getPos()const{
		return m_p;
	}

	inline const Tc& getCycle()const{
		return m_c;
	}

	inline Ts getSize()const{
		return Size;
	}

	inline Ts getCount()const{
		return Size * m_c + m_p;
	}

#define RINGPOS CRingPos
#include <dm/ringpos_.hpp>
#undef RINGPOS

	inline CRingPos& moveRight( const Tp& n,const Tc& c=0 ){
		ringpos::moveRight(m_p,m_c,Size,n,c);
		return *this;
	}

	inline CRingPos& moveLeft( const Tp& n,const Tc& c=0 ){
		ringpos::moveLeft(m_p,m_c,Size,n,c);
		return *this;
	}

	inline void reset(){
		m_c = 0;
		m_p = 0;
	}

private:
	Tc m_c;
	Tp m_p;
};

}

#endif /* RINGPOS_HPP_ */

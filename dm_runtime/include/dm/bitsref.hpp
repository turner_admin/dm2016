/*
 * bitsref.hpp
 *
 *  Created on: 2015��8��11��
 *      Author: work
 */

#ifndef DM_BITSREF_HPP_
#define DM_BITSREF_HPP_

#include <dm/bits.hpp>

namespace dm{
	template<typename T>
	class CBitsRef{
	public:
		CBitsRef( T& bits ):m_r(bits){
		}

		inline bool isSet( int bIndex )const{
			return bit_get(m_r,bIndex);
		}

		inline bool isClr( int bIndex )const{
			return !isSet(bIndex);
		}

		inline CBitsRef<T>& set( int bIndex ){
			m_r = bit_set(m_r,bIndex);
			return *this;
		}

		inline CBitsRef<T>& clr( int bIndex ){
			m_r = bit_clr(m_r,bIndex);
			return *this;
		}

		inline const T& getRef()const{
			return m_r;
		}

		inline T& getRef(){
			return m_r;
		}

		inline CBitsRef<T>& operator=( const T& v ){
			m_r = v;
			return *this;
		}

		inline CBitsRef<T>& operator=( const CBitsRef<T>& r ){
			m_r = r.m_r;
			return *this;
		}

		inline bool operator==( const T& v )const{
			return m_r==v;
		}

		inline bool operator==( const CBitsRef<T>& r )const{
			return m_r==r.m_r;
		}

	private:
		T& m_r;
	};
}


#endif /* INCLUDE_BITSREF_HPP_ */

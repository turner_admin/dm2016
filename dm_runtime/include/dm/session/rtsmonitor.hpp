/*
 * rtsmonitor.hpp
 *
 *  Created on: 2019-1-9
 *      Author: Dylan.Gao
 */

#ifndef _DM_SESSION_RTSMONITOR_HPP_
#define _DM_SESSION_RTSMONITOR_HPP_

#include <dm/export.hpp>

#ifndef DM_API_SESSION
#define DM_API_SESSION DM_API_IMPORT
#endif

#include <dm/session/common.hpp>
#include <dm/session/rts.hpp>

namespace dm{
namespace session{

class DM_API_SESSION CRtsMonitor{
public:
	enum{
#ifdef SESSION_RTSMONITOR_LEN
		Len = SESSION_RTSMONITOR_LEN
#else
		Len = 1024*16
#endif
	};

public:
	CRtsMonitor();

	bool isIdle( const dm::CTimeStamp& n )const;
	bool isTimeout( const dm::CTimeStamp& n )const;

	inline const dm::CTimeStamp& deadline()const{
		return m_deadline;
	}

	inline void setDeadline( const dm::CTimeStamp& n ){
		m_deadline = n;
	}

	inline const mod_t& mod()const{
		return m_mod;
	}

	inline void setMod( const mod_t& m ){
		m_mod = m;
	}

	inline const int& num()const{
		return m_num;
	}

	inline void setNum( int n ){
		m_num = n;
	}

	CRts append( CRts::EGroupType type,dm::uint32 size );

	CRts get( int i );

private:
	dm::CTimeStamp m_deadline;
	mod_t m_mod;
	int m_num;
	unsigned char m_data[Len];
};

}
}

#endif /* INCLUDE_DM_SESSION_RTSMONITOR_HPP_ */

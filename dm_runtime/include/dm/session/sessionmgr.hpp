/*
 * sessionmgr.hpp
 *
 *  Created on: 2019-1-9
 *      Author: work
 */

#ifndef _DM_SESSION_SESSIONMGR_HPP_
#define _DM_SESSION_SESSIONMGR_HPP_

#include <dm/export.hpp>

#ifndef DM_API_SESSION
#define DM_API_SESSION DM_API_IMPORT
#endif

#include <dm/session/session.hpp>
#include <dm/session/rtsmonitormgr.hpp>
#include <dm/session/timedmonitormgr.hpp>
#include <dm/session/logmonitormgr.hpp>

namespace dm{
namespace session{

/**
 * 会话管理器
 */
class DM_API_SESSION CSessionMgr{
public:
	enum{
#ifdef SESSION_MAX
		Size = SESSION_MAX
#else
		Size = 128
#endif
	};
	CSessionMgr();

	static CSessionMgr* ins();

	bool reset();

	/**
	 * 获取一个新会话
	 * @param userId
	 * @return
	 */
	CSession* getNew( userid_t userId,dm::CTimeStamp::s_t aliveSecs=300 );

	/**
	 * 获取会话信息
	 * 如果成功，会刷新会话时间
	 * @param userId
	 * @param mod
	 * @return
	 */
	CSession* get( userid_t userId,mod_t mod,dm::CTimeStamp::s_t aliveSecs=300 );

	/**
	 * 释放会话
	 * @param userId
	 * @param mod
	 */
	void free( userid_t userId,mod_t mod );

	/**
	 * 系统允许的会话数量
	 * @return
	 */
	int maxSize()const;

	/**
	 * 当前空闲会话数量
	 * @return
	 */
	int emptySize()const;


	inline const CSession* sessions()const{
		return m_session;
	}

	inline const CRtsMonitorMgr* rtsMgr()const{
		return &m_rts;
	}

	inline const CTimedMonitorMgr* tdsMgr()const{
		return &m_tds;
	}

	inline const CLogMonitorMgr* logsMgr()const{
		return &m_logs;
	}

protected:
	CSession m_session[Size];
	CRtsMonitorMgr m_rts;
	CTimedMonitorMgr m_tds;
	CLogMonitorMgr m_logs;

	friend class CSession;
};

}
}

#endif /* INCLUDE_DM_SESSION_SESSIONMGR_HPP_ */

/*
 * login_mgr.hpp
 *
 *  Created on: 2024年7月9日
 *      Author: Dylan.Gao
 */

#ifndef _DM_SESSION_LOGIN_MGR_HPP_
#define _DM_SESSION_LOGIN_MGR_HPP_

#include <dm/session/common.hpp>
#include <dm/datetime.hpp>

namespace dm{
namespace session{

/**
 * 用户登陆管理器
 * 实现不同进程之间共享登陆信息
 */
class CLoginMgr{
	CLoginMgr();
public:
	/**
	 * 用户类型
	 */
	enum EAccountType{
		AtUnknow,/**< 未知类型 */
		AtUser,  /**< 用户 */
		AtWorker /**< 运维人员 */
	};

	/**
	 * 权限编码
	 */
	enum EAuthority{
		ViewSameOrg,   /**< 查看本级组织 */
		AdmSameOrg,    /**< 管理本级组织 */
		ViewSubOrg,    /**< 查看下级组织 */
		AdmSubOrg,     /**< 管理下级组织 */

		ViewSameHum,   /**< 查看本级人员 */
		AdmSameHum,    /**< 管理本级人员 */
		AuthSameHum,   /**< 授权本级人员 */
		ViewSubHum,    /**< 查看下级人员 */
		AdmSubHum,     /**< 管理下级人员 */
		AuthSubHum,    /**< 授权下级人员 */

		ViewSameGroup, /**< 查看本级客户集团 */
		AdmSameGroup,  /**< 管理本级客户集团 */
		ViewSubGroup,  /**< 查看下级客户集团 */
		AdmSubGroup,   /**< 管理下级客户集团 */

		ViewSameDev,   /**< 查看本级设备 */
		AdmSameDev,    /**< 管理本级设备 */
		CtlSameDev,    /**< 控制本级设备 */
		ViewSubDev,    /**< 查看下级设备 */
		AdmSubDev,     /**< 管理下级设备 */
		CtlSubDev,     /**< 控制下级设备 */

		ViewSameDevHis,/**< 查看本级设备历史数据 */
		AdmSameDevHis, /**< 管理本级设备历史数据 */
		ViewSubDevHis, /**< 查看下级设备历史数据 */
		AdmSubDevHis,  /**< 管理下级设备历史数据 */

		ViewHmi,       /**< 查看组态 */
		AdmHmi,        /**< 管理组态 */
		ViewRpt,       /**< 查看报表 */
		AdmRpt,        /**< 管理报表 */

		System,        /**< 系统权限。增加主机，任务，退出任务 */
		Export,        /**< 系统导出 */
		Import,        /**< 系统导入 */

		AuthorityExt   /**< 其他扩展权限 */
	};

	static CLoginMgr* ins();

	/**
	 * 用户登陆
	 * @param account 账户
	 * @param pwd 口令
	 * @param at 账户类型
	 * @return 是否登陆成功
	 */
	bool login( const char* account,const char* pwd,EAccountType at );

	/**
	 * 登出
	 * @return 是否登出成功
	 * 未登陆登出会失败
	 */
	bool logout();

	/**
	 * 刷新进程信息
	 * @return
	 */
	bool refresh();

	/**
	 * 是否已经登陆
	 * @return
	 */
	bool isLogined()const;

	/**
	 * 已经登陆账户类型
	 * @return
	 */
	EAccountType accountType()const;

	/**
	 * 已经登陆用户id
	 * @return
	 */
	userid_t id()const;

	/**
	 * 登陆时刻
	 * @return
	 */
	dm::CDateTime dateTime()const;

	/**
	 * 权限校验
	 * @param bit 权限位
	 * @param resouce 资源id
	 * @return
	 */
	bool isAuthoried( const int bit,id_t resouce=-1 )const;
};

}
}

#endif /* _DM_SESSION_LOGIN_MGR_HPP_ */

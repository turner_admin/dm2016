/*
 * rts.hpp
 *
 *  Created on: 2019-1-11
 *      Author: Dylan.Gao
 */

#ifndef _DM_SESSION_RTS_HPP_
#define _DM_SESSION_RTS_HPP_

#include <dm/export.hpp>

#ifndef DM_API_SESSION
#define DM_API_SESSION DM_API_IMPORT
#endif

#include <dm/session/common.hpp>

#include <dm/scada/statusmgr.hpp>
#include <dm/scada/discretemgr.hpp>
#include <dm/scada/measuremgr.hpp>
#include <dm/scada/cumulantmgr.hpp>
#include <dm/scada/rts.hpp>

namespace dm{
namespace session{

class DM_API_SESSION CRts{
public:
	enum EGroupType{
		GroupStatus,
		GroupDiscrete,
		GroupMeasure,
		GroupCumulant
	};

	struct SRtStatus{
		dm::scada::index_t idx;
		dm::scada::s_t v;
	}
#ifndef WIN32
	__attribute__((packed))
#endif
	;

	struct SRtDiscrete{
		dm::scada::index_t idx;
		dm::scada::d_t v;
	}
#ifndef WIN32
	__attribute__((packed))
#endif
	;

	struct SRtMeasure{
		dm::scada::index_t idx;
		dm::scada::m_t v;
	}
#ifndef WIN32
	__attribute__((packed))
#endif
	;

	struct SRtCumulant{
		dm::scada::index_t idx;
		dm::scada::c_t v;
	}
#ifndef WIN32
	__attribute__((packed))
#endif
	;

	struct SRts{
		EGroupType type;
		dm::uint32 num;
		unsigned char data;
	}
#ifndef WIN32
	__attribute__((packed))
#endif
	;

public:
	CRts( unsigned char* p=NULL );
	CRts( const CRts& rts );

	CRts& operator=( const CRts& r );

	void setP( unsigned char* p );

	inline SRts* rts(){
		return (SRts*)m_p;
	}

	inline const SRts* rts()const{
		return (const SRts*)m_p;
	}

	inline bool ifNotNull()const{
		return m_p!=NULL;
	}

	bool set( EGroupType type,dm::uint32 num,const unsigned char* end );

	inline EGroupType type()const{
		return rts()->type;
	}

	inline dm::uint32 num()const{
		return rts()->num;
	}

	SRtStatus* statuses();
	SRtDiscrete* discretes();
	SRtMeasure* measures();
	SRtCumulant* cumulants();

	unsigned char* next();
	unsigned long memSize()const;

private:
	unsigned char* m_p;
};

}
}

#endif /* DM_RUNTIME_INCLUDE_DM_SESSION_RTS_HPP_ */

/*
 * timedmonitormgr.hpp
 *
 *  Created on: 2019-1-9
 *      Author: work
 */

#ifndef _DM_SESSION_TIMEDMONITORMGR_HPP_
#define _DM_SESSION_TIMEDMONITORMGR_HPP_

#include <dm/export.hpp>

#ifndef DM_API_SESSION
#define DM_API_SESSION DM_API_IMPORT
#endif

#include <dm/session/timedmonitor.hpp>
#include <dm/session/common.hpp>

namespace dm{
namespace session{

class DM_API_SESSION CTimedMonitorMgr{
public:
	enum{
#ifdef SESSION_TIMEDMONITOR_SIZE
		Size = SESSION_TIMEDMONITOR_SIZE
#else
		Size = 128
#endif
	};

	CTimedMonitorMgr();

	CTimedMonitor* getNew( dm::CTimeStamp::s_t aliveSec=5 );
	CTimedMonitor* get( mod_t mod,dm::CTimeStamp::s_t aliveSec=5 );

	void free( mod_t mod );

private:
	lockshared_t m_lock;
	CTimedMonitor m_d[Size];
};

}
}

#endif /* INCLUDE_DM_SESSION_TIMEDMONITORMGR_HPP_ */

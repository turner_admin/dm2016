﻿/*
 * logmonitormgr.hpp
 *
 *  Created on: 2019-1-9
 *      Author: Dylan.Gao
 */

#ifndef _DM_SESSION_LOGMONITORMGR_HPP_
#define _DM_SESSION_LOGMONITORMGR_HPP_

#include <dm/export.hpp>

#ifndef DM_API_SESSION
#define DM_API_SESSION DM_API_IMPORT
#endif

#include <dm/session/logmonitor.hpp>
#include <dm/session/common.hpp>

namespace dm{
namespace session{

class DM_API_SESSION CLogMonitorMgr{
public:
	enum{
#ifdef SESSION_LOGMONITOR_SIZE
		Size = SESSION_LOGMONITOR_SIZE
#else
		Size = 128
#endif
	};

	CLogMonitorMgr();

	CLogMonitor* getNew( dm::CTimeStamp::s_t aliveSec=30 );
	CLogMonitor* get( mod_t mod,dm::CTimeStamp::s_t aliveSec=30 );

	void free( mod_t mod );

private:
	lockshared_t m_lock;
	CLogMonitor m_d[Size];
};

}
}

#endif /* INCLUDE_DM_SESSION_LOGMONITORMGR_HPP_ */

﻿/*
 * modgen.hpp
 *
 *  Created on: 2019-1-11
 *      Author: Dylan.Gao
 */

#ifndef _DM_SESSION_MODGEN_HPP_
#define _DM_SESSION_MODGEN_HPP_

#include <dm/export.hpp>

#ifndef DM_API_SESSION
#define DM_API_SESSION DM_API_IMPORT
#endif

#include <dm/session/common.hpp>

namespace dm{
namespace session{

/**
 * 魔数产生器
 */
class DM_API_SESSION CModGen{
	CModGen();
public:
	static CModGen& ins();

	mod_t gen();
};

}
}

#endif /* INCLUDE_DM_SESSION_MODGEN_HPP_ */

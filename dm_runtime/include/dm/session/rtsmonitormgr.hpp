/*
 * rtsmonitormgr.hpp
 *
 *  Created on: 2019-1-11
 *      Author: work
 */

#ifndef _DM_SESSION_RTSMONITORMGR_HPP_
#define _DM_SESSION_RTSMONITORMGR_HPP_

#include <dm/export.hpp>

#ifndef DM_API_SESSION
#define DM_API_SESSION DM_API_IMPORT
#endif

#include <dm/session/rtsmonitor.hpp>

namespace dm{
namespace session{

class DM_API_SESSION CRtsMonitorMgr{
public:
	enum{
#ifdef SESSION_RTSMONITOR_SIZE
		Size = SESSION_RTSMONITOR_SIZE
#else
		Size = 128
#endif
	};

	CRtsMonitorMgr();

	CRtsMonitor* getNew( dm::CTimeStamp::s_t aliveSec=5 );
	CRtsMonitor* get( mod_t mod,dm::CTimeStamp::s_t aliveSec=5 );

	void free( mod_t mod );

private:
	lockshared_t m_lock;
	CRtsMonitor m_d[Size];
};

}
}

#endif /* DM_RUNTIME_INCLUDE_DM_SESSION_RTSMONITORMGR_HPP_ */

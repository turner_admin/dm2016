/*
 * logmonitor.hpp
 *
 *  Created on: 2019-1-9
 *      Author: work
 */

#ifndef _DM_SESSION_LOGMONITOR_HPP_
#define _DM_SESSION_LOGMONITOR_HPP_

#include <dm/export.hpp>

#ifndef DM_API_SESSION
#define DM_API_SESSION DM_API_IMPORT
#endif

#include <dm/session/common.hpp>
#include <dm/os/log/logmonitor.hpp>
namespace dm{
namespace session{

class DM_API_SESSION CLogMonitor{
	CLogMonitor( const CLogMonitor& );

public:
	typedef dm::os::CLogMonitor::ringpos_t ringpos_t;
	typedef dm::os::CLogInfo::pid_t pid_t;

public:
	CLogMonitor();

	bool isIdle( const dm::CTimeStamp& n )const;

	bool isTimeout( const dm::CTimeStamp& n )const;

	inline const dm::CTimeStamp& deadline()const{
		return m_deadline;
	}

	inline void setDeadline( const dm::CTimeStamp& ts ){
		m_deadline = ts;
	}

	inline const mod_t& mod()const{
		return m_mod;
	}

	inline void setMod( const mod_t& m ){
		m_mod = m;
	}

	ringpos_t* pos();

	inline const pid_t& pid()const{
		return m_pid;
	}

	inline void setPid( const pid_t& p ){
		m_pid = p;
	}

	inline const dm::os::CLogInfo::ELevel& level()const{
		return m_level;
	}

	inline void setLevel( const dm::os::CLogInfo::ELevel& l ){
		m_level = l;
	}

private:
	dm::CTimeStamp m_deadline;
	mod_t m_mod;
	ringpos_t m_pos;
	pid_t m_pid;
	dm::os::CLogInfo::ELevel m_level;
};

}
}

#endif /* INCLUDE_DM_SESSION_LOGMONITOR_HPP_ */

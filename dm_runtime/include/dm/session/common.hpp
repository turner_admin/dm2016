/*
 * common.hpp
 *
 *  Created on: 2019-1-9
 *      Author: work
 */

#ifndef _DM_SESSION_COMMON_HPP_
#define _DM_SESSION_COMMON_HPP_

#include <dm/types.hpp>
#include <dm/os/mustlock.hpp>
#include <boost/interprocess/sync/sharable_lock.hpp>
#include <boost/interprocess/sync/interprocess_sharable_mutex.hpp>

namespace dm{
namespace session{

typedef dm::uint64 mod_t;
typedef dm::int32 userid_t;

typedef boost::interprocess::interprocess_sharable_mutex lockshared_t;
typedef dm::os::CMustLock<lockshared_t> msharelock_exclusive_t;

}
}

#endif /* INCLUDE_DM_SESSION_COMMON_HPP_ */

/*
 * combine.hpp
 *
 *  Created on: 2023年10月14日
 *      Author: 高德绵
 */

#ifndef _DM_COMBINE_HPP_
#define _DM_COMBINE_HPP_

namespace dm{

/**
 * 组合两个指针
 */
struct SCombine2{
	SCombine2( void* _p1,void* _p2):p1(_p1),p2(_p2){}

	void* p1;
	void* p2;
};

struct SCombine3{
	SCombine3( void* _p1,void* _p2,void* _p3):p1(_p1),p2(_p2),p3(_p3){}

	void* p1;
	void* p2;
	void* p3;
};

struct SCombine4{
	SCombine4( void* _p1,void* _p2,void* _p3,void* _p4):p1(_p1),p2(_p2),p3(_p3),p4(_p4){}

	void* p1;
	void* p2;
	void* p3;
	void* p4;
};

struct SCombine5{
	SCombine5( void* _p1,void* _p2,void* _p3,void* _p4,void* _p5):p1(_p1),p2(_p2),p3(_p3),p4(_p4),p5(_p5){}

	void* p1;
	void* p2;
	void* p3;
	void* p4;
	void* p5;
};

/**
 * 组合类型
 * @tparam T1
 * @tparam T2
 */
template<typename T1,typename T2>
struct TSCombine2{
	TSCombine2(T1 t1,T2 t2):p1(t1),p2(t2){}

	T1 p1;
	T2 p2;
};

template<typename T1,typename T2,typename T3>
struct TSCombine3{
	TSCombine3(T1 t1,T2 t2,T3 t3):e1(t1),e2(t2),e3(t3){}

	T1 e1;
	T2 e2;
	T3 e3;
};

template<typename T1,typename T2,typename T3,typename T4>
struct TSCombine4{
	TSCombine4(T1 t1,T2 t2,T3 t3,T4 t4):e1(t1),e2(t2),e3(t3),e4(t4){}

	T1 e1;
	T2 e2;
	T3 e3;
	T4 e4;
};

template<typename T1,typename T2,typename T3,typename T4,typename T5>
struct TSCombine5{
	TSCombine5(T1 t1,T2 t2,T3 t3,T4 t4,T5 t5):e1(t1),e2(t2),e3(t3),e4(t4),e5(t5){}

	T1 e1;
	T2 e2;
	T3 e3;
	T4 e4;
	T5 e5;
};

}

#endif /* _DM_COMBINE_HPP_ */

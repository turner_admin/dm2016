/*
 * scoped_ptr.hpp
 *
 *  Created on: 2022年11月7日
 *      Author: dylan
 */

#ifndef _DM_SCOPED_PTR_HPP_
#define _DM_SCOPED_PTR_HPP_

namespace dm{

/**
 * 智能指针，在局部作用域内，自动释放指针对象
 */
template<typename T>
class TScopedPtr{
public:
	TScopedPtr(T* p=0):m_p(p){
	}

	TScopedPtr( TScopedPtr& p ):m_p(p.m_p){
		p.m_p = 0;
	}

	~TScopedPtr(){
		if( m_p )
			delete m_p;
	}

	/**
	 * 复位指针
	 * 不会管控原指针
	 */
	void reset( T* p=0 ){
		m_p = p;
	}

	TScopedPtr& operator=( T* p ){
		if( m_p )
			delete m_p;
		m_p = p;
		return *this;
	}

	/**
	 * 赋值操作
	 * @param p
	 * @return
	 */
	TScopedPtr& operator=( TScopedPtr& p ){
		if( m_p )
			delete m_p;
		m_p = p.m_p;
		p.m_p = 0;

		return *this;
	}

	operator bool()const{
		return m_p!=0;
	}

	bool operator ! () const{
		return m_p==0;
	}

	T& operator*()const{
		return *m_p;
	}

	T* operator->()const{
		return m_p;
	}

	/**
	 * 取出指针对象，不再管控指针
	 */
	T* take(){
		T* p = m_p;
		m_p = 0;
		return p;
	}

	T* ptr()const{
		return m_p;
	}
protected:
	T* m_p;
};

}

#endif /* DM_RUNTIME_INCLUDE_DM_SCOPED_PTR_HPP_ */

﻿/*
 * ringpos_v.hpp
 *
 *  Created on: 2016年11月12日
 *      Author: Dylan.Gao
 */

#ifndef _DM_RINGPOS_V_HPP_
#define _DM_RINGPOS_V_HPP_

#include <dm/ringpos_in.hpp>

namespace dm{

/**
 * 位置指针易变
 * 主要用于多线程同步中
 */
template<typename Tp,typename Ts,Ts Size,typename Tc=Tp>
class CRingPosV{
public:
	CRingPosV():m_p(0),m_c(0){}
	CRingPosV( const Tp& p,const Tc& c):m_p(p),m_c(c){}
	CRingPosV( const CRingPosV& p ):m_p(p.m_p),m_c(p.m_c){};

	inline Tp getPos()const{
		return m_p;
	}

	inline Tc getCycle()const{
		return m_c;
	}

	inline Ts getSize()const{
		return Size;
	}

#define RINGPOS CRingPosV
#include <dm/ringpos_.hpp>
#undef RINGPOS

	inline CRingPosV& moveRight( const Tp& n,const Tc& c=0 ){
		ringpos::moveRight(m_p,m_c,Size,n,c);
		return *this;
	}

	inline CRingPosV& moveLeft( const Tp& n,const Tc& c=0 ){
		ringpos::moveLeft(m_p,m_c,Size,n,c);
		return *this;
	}
private:
	volatile Tp m_p;
	volatile Tc m_c;
};

}

#endif /* INCLUDE_DM_RINGPOS_V_HPP_ */

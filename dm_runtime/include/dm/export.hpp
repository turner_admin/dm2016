#ifndef _DM_EXPORT_HPP
#define _DM_EXPORT_HPP

#ifdef WIN32
#define DM_API_EXPORT __declspec(dllexport)
#define DM_API_IMPORT __declspec(dllimport)
#else
#define DM_API_EXPORT
#define DM_API_IMPORT
#endif

#endif

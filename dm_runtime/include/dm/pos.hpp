/*
 * pos.h
 *
 *  Created on: 2015-7-1
 *      Author: work
 */

#ifndef DM_POS_H_
#define DM_POS_H_

namespace Dm {

class CPos {
public:
	CPos();
	CPos( const CPos& p );

	inline const unsigned int& getPos()const{
		return m_p;
	}

	inline void setPos( unsigned int p ){
		m_p = p;
	}

	CPos& operator=( const CPos& p ){
		setPos(p.getPos());
		return *this;
	}

	CPos& plusplus( const unsigned int& size ){
		++m_p;
		if( m_p>=size )
			m_p = 0;
		return *this;
	}

	CPos plus( unsigned int n,const unsigned int& size )const;

	inline bool operator==( const CPos& p )const{
		return m_p==p.m_p;
	}

	inline bool operator!=( const CPos& p )const{
		return m_p!=p.m_p;
	}

	unsigned int minus( const CPos& p,const unsigned int& size )const;

private:
	unsigned int m_p;
};

} /* namespace Dm */
#endif /* POS_H_ */

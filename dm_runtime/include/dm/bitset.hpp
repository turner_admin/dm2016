/*
 * bitset.hpp
 *
 *  Created on: 2015��8��11��
 *      Author: work
 */

#ifndef DM_BITSET_HPP_
#define DM_BITSET_HPP_

#include "bitsref.hpp"

namespace NDm{

	template<typename T>
	class CBits:public CBitsRef<T>{
	public:
		CBits():CBitsRef<T>(m_d){
			m_d = 0;
		}

	private:
		T m_d;
	};
}



#endif /* INCLUDE_BITSET_HPP_ */

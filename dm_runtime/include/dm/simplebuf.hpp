﻿/*
 * simplebuf.hpp
 *
 *  Created on: 2017年2月4日
 *      Author: work
 */

#ifndef _DM_SIMPLEBUF_HPP_
#define _DM_SIMPLEBUF_HPP_

namespace dm{

/**
 * 简单缓冲区
 * Size 缓冲区大小
 */
template<typename Tv,typename Ts,Ts Size>
class CSimpleBuf{
	CSimpleBuf( const CSimpleBuf& );
	CSimpleBuf& operator=( const CSimpleBuf& );

public:
	CSimpleBuf():m_p(0){}

	inline void reset(){
		m_p = 0;
	}

	inline Ts getSize()const{
		return Size;
	}

	/**
	 * 当前缓冲区数据个数
	 * @return
	 */
	inline const Ts& getLen()const{
		return m_p;
	}

	inline const Ts& getPos()const{
		return m_p;
	}

	inline Ts getSpace()const{
		return Size - m_p;
	}

	inline const Tv& at( const Ts& p )const{
		return m_d[p];
	}

	inline const Tv* getData()const{
		return m_d;
	}

	inline bool push( const Tv& v ){
		if( m_p<Size ){
			m_d[m_p] = v;
			++m_p;
			return true;
		}else
			return false;
	}

	bool push( const Tv* buf,const Ts& size );

private:
	Tv m_d[Size];
	Ts m_p;
};

template<typename Tv,typename Ts,Ts Size>
bool CSimpleBuf<Tv,Ts,Size>::push( const Tv* buf,const Ts& size ){
	if( m_p+size<=Size ){
		for( Ts i=0;i<size;++i,++m_p,++buf )
			m_d[m_p] = *buf;
		return true;
	}else
		return false;
}

}

#endif /* INCLUDE_DM_SIMPLEBUF_HPP_ */

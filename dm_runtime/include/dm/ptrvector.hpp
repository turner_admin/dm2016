/*
 * ptrvector.hpp
 *
 *  Created on: 2018-7-25
 *      Author: work
 */

#ifndef DM_PTRVECTOR_H_
#define DM_PTRVECTOR_H_

#include <vector>

namespace dm {

/**
 * 指针向量
 * 析构时自动是否指针内容
 * @tparam T
 */
template<typename T>
class TCPtrVector {
    TCPtrVector( const TCPtrVector& p );
    TCPtrVector& operator=( const TCPtrVector& p );
public:
    TCPtrVector():m_e(){
    }

    ~TCPtrVector();

    inline unsigned int size()const{
        return m_e.size();
    }

    T* element( int idx ){
        if( idx<0 || idx>m_e.size() )
            return NULL;
        return m_e[idx];
    }

    const T* element( int idx )const{
        if( idx<0 || ((unsigned int)idx)>m_e.size() )
            return NULL;
        return m_e[idx];
    }

    inline T* operator []( unsigned int idx ){
    	return m_e[idx];
    }

    inline const T* operator []( unsigned int idx )const{
    	return m_e[idx];
    }

    void append( T*e ){
        m_e.push_back(e);
    }

    void remove( int idx ){
        if( idx>=0 && ((unsigned int)idx)<m_e.size() ){
            delete m_e[idx];
            m_e.erase(m_e.begin()+idx);
        }
    }

    void clear(){
    	// gcc中需要使用typename标记
        for( typename::std::vector<T*>::iterator it=m_e.begin();it!=m_e.end();++it ){
            if( *it )
                delete (*it);
        }
        m_e.clear();
    }

private:
    std::vector<T*> m_e;
};

template<typename T>
TCPtrVector<T>::~TCPtrVector(){
	// gcc中需要使用typename标记
    for( typename::std::vector<T*>::iterator it=m_e.begin();it!=m_e.end();++it ){
        if( *it )
            delete (*it);
    }
}

} /* namespace Dm */
#endif /* POS_H_ */

/*
 * ipckey.hpp
 *
 *  Created on: 2016��4��17��
 *      Author: work
 */

#ifndef DM_OSUNIX_IPCKEY_HPP_
#define DM_OSUNIX_IPCKEY_HPP_

#include <sys/types.h>

namespace NDm {
namespace NOsUnix {

class CIpcKey {
public:
	/**
	 * Get Key form a existed file
	 * @param key
	 * @param path
	 * @param id
	 * @return
	 * - true success
	 * - false path is not exist
	 */
	static bool getKey(key_t& key,const char* path,int id );
};

} /* namespace NOsUnix */
} /* namespace NDm */

#endif /* IPCKEY_HPP_ */

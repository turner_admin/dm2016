/*
 * flock.hpp
 *
 *  Created on: 2016��4��18��
 *      Author: work
 */

#ifndef DM_OSUNIX_FLOCK_HPP_
#define DM_OSUNIX_FLOCK_HPP_

#include <sys/file.h>

namespace NDm {
namespace NOsUnix {

class CFlock{
public:
	CFlock( const char* path );
	~CFlock();

	inline bool lock(){
		return l_flock(LOCK_EX);
	}

	inline bool lockShared(){
		return l_flock(LOCK_SH);
	}

	inline bool tryLock(){
		return l_flock(LOCK_EX|LOCK_NB);
	}

	inline bool tryLockShared(){
		return l_flock(LOCK_SH|LOCK_NB);
	}

	inline bool unlock(){
		return l_flock(LOCK_UN);
	}

protected:
	bool l_flock( int op );
private:
	int m_fd;
};

} /* namespace NOsUnix */
} /* namespace NDm */

#endif /* DM_OSUNIX_FLOCK_HPP_ */

/*
 * shm.hpp
 *
 *  Created on: 2016年4月17日
 *      Author: work
 */

#ifndef DM_OSUNIX_SHM_HPP_
#define DM_OSUNIX_SHM_HPP_

#include <sys/types.h>
#include <sys/ipc.h>
#include <cstring>

namespace NDm{
namespace NOsUnix{
class CShm{
public:
	CShm();
	~CShm();

	inline bool create( const key_t& key,size_t size ){
		return get(key,size,IPC_CREAT|IPC_EXCL);
	}

	inline bool open( const key_t& key,size_t size ){
		return get(key,size,0);
	}

	inline bool createOrOpen( const key_t& key,size_t size ){
		return get(key,size,IPC_CREAT);
	}

	inline bool create( const char* path,int id,size_t size ){
		return get(path,id,size,IPC_CREAT|IPC_EXCL);
	}

	inline bool open( const char* path,int id,size_t size ){
		return get(path,id,size,0);
	}

	inline bool createOrOpen( const char* path,int id,size_t size ){
		return get(path,id,size,IPC_CREAT);
	}

	inline void setAutoDelete( bool autoDel ){
		m_autoDelete = autoDel;
	}

	bool deleteShm();

	inline void* getMemAddr(){
		return m_memAddr;
	}

	const void* getMemAddr()const{
		return m_memAddr;
	}
protected:
	bool get( const key_t& key,size_t size,int cflag );
	bool get( const char* path,int id,size_t size,int cflag );

	bool map();
	void unmap();
	void autoDel();

private:
	bool m_autoDelete;	// 是否自动删除
	int m_shmid;
	void* m_memAddr;
};
}
}

#endif /* DM_OSUNIX_SHM_HPP_ */

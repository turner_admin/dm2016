#ifndef DM_OSUNIX_PROCESS_HPP_
#define DM_OSUNIX_PROCESS_HPP_

namespace NDm{
namespace NOsUnix{

class CProcess{
public:
	typedef long TPid;
	typedef long TUid;
	typedef long TGid;

	static TPid getPid();
	static TPid getParentPid();

	static TUid getUid();
	static TGid getGid();
	static TUid getEffectiveUid();
	static TGid getEffectiveGid();
};

}
}

#endif

﻿/*
 * stringlist.hpp
 *
 *  Created on: 2016年12月12日
 *      Author: Dylan.Gao
 */

#ifndef _DM_STRING_STRINGLIST_HPP_
#define _DM_STRING_STRINGLIST_HPP_

#include <dm/export.hpp>

#ifndef DM_API_STRING
#define DM_API_STRING DM_API_IMPORT
#endif

#include <dm/string/string.hpp>
#include <vector>

namespace dm{
namespace string{

class DM_API_STRING CStringList{
public:
	typedef std::vector<CString> item_t;

	CStringList();
	CStringList( const CStringList& );
	~CStringList();

	CStringList& operator=( const CStringList& strs );
	CStringList& append( const char* str );
	CStringList& append( const CString& str );
	CStringList& append( const CStringList& strs );

	inline void clear(){
		m_es->clear();
	}

	inline size_t size()const{
		return m_es->size();
	}

	inline const CString& at( int i )const{
		return m_es->at(i);
	}

	/**
	 * 总字符大小
	 * @return
	 */
	size_t totalSize()const;

	/**
	 * 转换成一个字符串
	 * @return
	 */
	CString toString()const;

	/**
	 * 转换成一个字符串，子串之间增加连接符c
	*/
	CString toString(const char& c)const;

	/**
	 * 转换成一个字符串，子串之间增加连接串
	*/
	CString toString( const char* str )const;

	/**
	 * 将字符串以字符分割
	 * @param str
	 * @param c
	 * @return
	 */
	CStringList& split( const char* str,const char& c );

	/**
	 * 将字符串的前len长度的字串以字符c分割
	 * @param str
	 * @param c
	 * @param len
	 * @return
	 */
	CStringList& split( const char* str,const char& c,int len );

	CStringList& split( const char* str,const char* detStr );
	CStringList& split( const char* str,const char* detStr,int len );

private:
	item_t* m_es;
};

}
}



#endif /* INCLUDE_DM_STRING_STRINGLIST_HPP_ */

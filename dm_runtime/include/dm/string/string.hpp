﻿/*
 * string.hpp
 *
 *  Created on: 2016年11月23日
 *      Author: work
 */

#ifndef _DM_STRING_STRING_HPP_
#define _DM_STRING_STRING_HPP_

#include <dm/export.hpp>


#ifndef DM_API_STRING
#define DM_API_STRING DM_API_IMPORT
#endif

#include <dm/string/stringref.hpp>

namespace dm{
namespace string{

/**
 * 字符串类型
 */
class DM_API_STRING CString:public CStringRef{
public:
	CString();
	CString( const CString& str );
	CString( const CStringRef& str );
	CString( const char* str );
	CString( const char* str,int len );
	~CString();

	CString& operator=( const CString& str );
	CString& operator=( const CStringRef& str );

	inline CString& operator=( const char* str ){
		return *this = CStringRef(str);
	}

	CString reverse()const;

	inline CString operator+( const char* str )const{
		return operator+(CStringRef(str) );
	}

	CString operator+( const CStringRef& str )const;
	CString operator+( const CString& str )const;

	CString sub( int start,int size=-1 )const;

	CString trimed()const;

	static CString fromHex( dm::uint8 v );
	static CString fromUint8( dm::uint8 v );
	static CString fromInt8( dm::int8 v );
	static CString fromUint16( dm::uint16 v );
	static CString fromInt16( dm::int16 v );
	static CString fromUint32( dm::uint32 v );
	static CString fromInt32( dm::int32 v );
	static CString fromUint64( dm::uint64 v );
	static CString fromInt64( dm::int64 v );

	static CString fromFloat32( dm::float32 v );
	static CString fromFloat64( dm::float64 v );

	static inline CString fromString( const char* str ){
		return CString(str);
	}

	static inline CString fromString( const char* str,int size ){
		return CString(str,size);
	}

private:
	char* m_b;

	friend class CStringList;
};

}
}

#endif /* INCLUDE_DM_STRING_STRING_HPP_ */

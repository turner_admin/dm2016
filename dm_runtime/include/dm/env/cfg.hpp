﻿/*
 * cfg.hpp
 *
 *  Created on: 2017年12月2日
 *      Author: work
 */

#ifndef _DM_ENV_CFG_HPP_
#define _DM_ENV_CFG_HPP_

#include <dm/export.hpp>

#ifndef DM_API_ENV
#define DM_API_ENV DM_API_IMPORT
#endif

#include <string>

namespace dm{
namespace env{

class DM_API_ENV CCfg{
	CCfg();
public:
	static CCfg& ins();

	// 日志系统
	int log_size()const;	// 日志记录最大条数

	int task_size()const;

	// scada系统参数
	int scada_allocate()const;	// 允许分配空间最大字节数kbyte
	int scada_eventQueueSize()const;
	int scada_measureQueueSize()const;
	int scada_cumulantQueueSize()const;

	std::string scada_dbEngine()const;
	std::string scada_dbHost()const;
	short scada_dbPort()const;
	std::string scada_dbName()const;
	std::string scada_dbUsr()const;
	std::string scada_dbPwd()const;

	std::string his_dbEngine()const;
	std::string his_dbHost()const;
	short his_dbPort()const;
	std::string his_dbName()const;
	std::string his_dbUsr()const;
	std::string his_dbPwd()const;

	std::string sys_dbEngine()const;
	std::string sys_dbHost()const;
	short sys_dbPort()const;
	std::string sys_dbName()const;
	std::string sys_dbUsr()const;
	std::string sys_dbPwd()const;

	// 选项配置数据库参数
	std::string option_dbEngine()const;
	std::string option_dbHost()const;
	short option_dbPort()const;
	std::string option_dbName()const;
	std::string option_dbUsr()const;
	std::string option_dbPwd()const;

	std::string user_dbEngine()const;
	std::string user_dbHost()const;
	short user_dbPort()const;
	std::string user_dbName()const;
	std::string user_dbUsr()const;
	std::string user_dbPwd()const;

	/**
	 * 获取图形库引擎
	 * @return
	 */
	std::string ui_dbEngine()const;

	std::string ui_dbHost()const;
	short ui_dbPort()const;
	std::string ui_dbName()const;
	std::string ui_dbUsr()const;
	std::string ui_dbPwd()const;
};

}
}

#endif /* _DM_ENV_CFG_HPP_ */

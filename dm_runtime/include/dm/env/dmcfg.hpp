﻿/*
 * dmcfg.hpp
 *
 *  Created on: 2016-08-01
 *      Author: work
 */

#ifndef DM_ENV_DMCFG_HPP_
#define DM_ENV_DMCFG_HPP_

#include <dm/export.hpp>

#ifndef DM_API_ENV
#define DM_API_ENV DM_API_IMPORT
#endif

namespace dm{
namespace env{

// scada
void DM_API_ENV setScada_sStatusSize( unsigned int n );
void DM_API_ENV setScada_dStatusSize( unsigned int n );
void DM_API_ENV setScada_samplesSize( unsigned int n );
void DM_API_ENV setScada_realsSize( unsigned int n );
void DM_API_ENV setScada_measuresSize( unsigned int n );

void DM_API_ENV setScada_timeQueue_sStatusSize( unsigned int n );
void DM_API_ENV setScada_timeQueue_dStatusSize( unsigned int n );
void DM_API_ENV setScada_timeQueue_samplesSize( unsigned int n );
void DM_API_ENV setScada_timeQueue_realsSize( unsigned int n );
void DM_API_ENV setScada_timeQueue_measuresSize( unsigned int n );
}
}

#endif /* DMCFG_HPP_ */

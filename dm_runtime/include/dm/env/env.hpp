﻿/*
 * env.hpp
 *
 *  Created on: 2016-07-22
 *      Author: work
 */

#ifndef DM_ENV_ENV_HPP_
#define DM_ENV_ENV_HPP_

#include <dm/export.hpp>

#ifndef DM_API_ENV
#define DM_API_ENV DM_API_IMPORT
#endif

#include <string>

namespace dm{
namespace env{
	std::string DM_API_ENV getDmDir();
	std::string DM_API_ENV getCfgDir();
	std::string DM_API_ENV getDataDir();
	std::string DM_API_ENV getLogDir();
    std::string DM_API_ENV getSvgDir();

	inline std::string getCfgFilePath( const char* file ){
		return (getCfgDir()+"/")+file;
	}

	inline std::string getDataFilePath( const char* file ){
		return (getDataDir()+"/")+file;
	}

	inline std::string getLogFilePath( const char* file ){
		return (getLogDir()+"/")+file;
	}
}
}

#endif /* ENV_HPP_ */

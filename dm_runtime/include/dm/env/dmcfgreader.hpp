﻿/*
 * dmcfg.hpp
 *
 *  Created on: 2016-08-01
 *      Author: Dylan
 */

#ifndef DM_ENV_DMCFGREADER_HPP_
#define DM_ENV_DMCFGREADER_HPP_

#include <dm/export.hpp>

namespace dm{
namespace env{

class CDmCfgReader{
	CDmCfgReader();
	CDmCfgReader( const CDmCfgReader& );
	CDmCfgReader& operator=( const CDmCfgReader& );

public:
	static CDmCfgReader& ins();

	const unsigned int& getScada_sStatusSize()const;
	const unsigned int& getScada_dStatusSize()const;
	const unsigned int& getScada_samplesSize()const;
	const unsigned int& getScada_realsSize()const;
	const unsigned int& getScada_measuresSize()const;

	const unsigned int& getScada_timeQueue_sStatusSize()const;
	const unsigned int& getScada_timeQueue_dStatusSize()const;
	const unsigned int& getScada_timeQueue_samplesSize()const;
	const unsigned int& getScada_timeQueue_realsSize()const;
	const unsigned int& getScada_timeQueue_measuresSize()const;
};
}
}

#endif /* DMCFG_HPP_ */

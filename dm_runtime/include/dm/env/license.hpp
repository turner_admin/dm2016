/*
 * license.hpp
 *
 *  Created on: 2017年6月5日
 *      Author: Dylan.Gao
 */

#ifndef _DM_ENV_LICENSE_HPP_
#define _DM_ENV_LICENSE_HPP_

#include <dm/export.hpp>

#ifndef DM_API_ENV
#define DM_API_ENV DM_API_IMPORT
#endif

#include <string>

namespace dm{
namespace env{

class DM_API_ENV CLicense{

public:

public:
	CLicense();
	~CLicense();

	/**
	 * 项目名称
	 * @return
	 */
	inline const std::string& project()const{
		return m_project;
	}

	inline const std::string& client()const{
		return m_client;
	}

	inline const std::string& manufacturer()const{
		return m_manufacturer;
	}

	inline const std::string& date()const{
		return m_date;
	}

	inline const std::string& sn()const{
		return m_sn;
	}

	bool isLicensed()const;

	std::string getMachineId()const;

protected:
	std::string m_project;
	std::string m_client;
	std::string m_manufacturer;
	std::string m_date;
	std::string m_sn;
};

}
}



#endif /* INCLUDE_DM_ENV_LICENSE_HPP_ */

﻿/*
 * jsoncfg.hpp
 *
 *  Created on: 2020年3月17日
 *      Author: Dylan.Gao
 */

#ifndef _DM_ENV_JSONCFG_HPP_
#define _DM_ENV_JSONCFG_HPP_

#include <dm/export.hpp>

#ifndef DM_API_ENV
#define DM_API_ENV DM_API_IMPORT
#endif

#include <json/json.h>

namespace dm{
namespace env{

/**
 * 基于Json的配置项
 */
class DM_API_ENV CJsonCfg:public Json::Value{
public:
	CJsonCfg();

	int load( const char* file,Json::String& errs );
	bool dump( const char* file )const;
};

}
}



#endif /* INCLUDE_DM_ENV_JSONCFG_HPP_ */

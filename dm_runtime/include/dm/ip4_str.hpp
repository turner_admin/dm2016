﻿/*
 * ip4_str.hpp
 *
 *  Created on: 2016年12月11日
 *      Author: Dylan.Gao
 */

#ifndef _DM_IP4_STR_HPP_
#define _DM_IP4_STR_HPP_

#include <dm/export.hpp>

#ifndef DM_API_MISC
#define DM_API_MISC DM_API_IMPORT
#endif

#include <dm/fixdstring.hpp>

namespace dm{

static const int IpStrLen = 16;

typedef CFixdString<IpStrLen> CIpStr;

class DM_API_MISC CIp4{
public:
	typedef CFixdString<4> field_str_t;
	typedef CFixdString<16> ip_str_t;

	CIp4();
	CIp4( const char* ip );
	CIp4( dm::uint32 ip );
	CIp4( dm::uint8 f1,dm::uint8 f2,dm::uint8 f3,dm::uint8 f4 );
	CIp4( const CIp4& ip );

	CIp4& operator=( const CIp4& ip );

	bool set( const char* ip );
	bool set( dm::uint8 f1,dm::uint8 f2,dm::uint8 f3,dm::uint8 f4 );

	bool setByField( int i,dm::uint8 f );

	inline const dm::uint8& getField( int i )const{
		if( i<0 || i>3 )
			i = 0;
		return m_f[i];
	}

	bool getFieldStr( int i,field_str_t& s )const;
	bool getStr( ip_str_t& ip )const;

private:
	dm::uint8 m_f[4];
};

}

#endif /* INCLUDE_DM_IP4_STR_HPP_ */

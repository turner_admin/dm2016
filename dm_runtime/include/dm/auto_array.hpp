/*
 * auto_array.hpp
 *
 *  Created on: 2018年3月22日
 *      Author: turner
 */

#ifndef _DM_AUTO_ARRAY_HPP_
#define _DM_AUTO_ARRAY_HPP_

namespace dm{

/**
 * 动态分配的数组
 */
template<typename T>
class TCAutoArray{
public:
	TCAutoArray():m_d(0),m_s(0){
	}

	~TCAutoArray(){
		if( m_s && m_d )
			delete [] m_d;
	}

	void reset( int c );

	inline const int& count()const{
		return m_s;
	}

	T* at( int i ){
		if( i<0 || i>=m_s )
			return 0;
		return m_d+i;
	}

	const T* at( int i )const{
		if( i<0 || i>=m_s )
			return 0;
		return m_d + i;
	}

	T& operator[]( int i ){
		return m_d[i];
	}

	const T& operator[]( int i )const{
		return m_d[i];
	}
private:
	T* m_d;
	int m_s;
};

template<typename T>
void TCAutoArray<T>::reset( int c ){
	if( m_s!=c ){
		if( m_s>0 )
			delete [] m_d;

		if( c>0 )
			m_d = new T[c];
		else
			m_d = 0;
		m_s = c;
	}
}

}

#endif /* DM_RUNTIME_INCLUDE_DM_AUTO_ARRAY_HPP_ */

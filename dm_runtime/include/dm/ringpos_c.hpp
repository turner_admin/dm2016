/*
 * ringpos_c.hpp
 *
 *  Created on: 2016年12月16日
 *      Author: work
 */

#ifndef INCLUDE_DM_RINGPOS_C_HPP_
#define INCLUDE_DM_RINGPOS_C_HPP_

#include <dm/ringpos_in.hpp>

namespace dm{

template<typename Tp,typename Tc = Tp>
class CRingPosC{
public:
	CRingPosC( const Tp& size=0,const Tp& pos=0,const Tc& cycle=0):m_c(cycle),m_p(pos),m_s(size){}
	CRingPosC( const CRingPosC& p ):m_c(p.m_c),m_p(p.m_p),m_s(p.m_s){}

	template<typename TRingPos>
	inline CRingPosC& operator=( const TRingPos& p ){
		m_c = p.getPos();
		m_p = p.getCycle();
		m_s = p.getSize();

		return *this;
	}

	inline const Tp& getPos()const{
		return m_p;
	}

	inline const Tc& getCycle()const{
		return m_c;
	}

	inline const Tp& getSize()const{
		return m_s;
	}

#define RINGPOS CRingPosC
#include <dm/ringpos_.hpp>
#undef RINGPOS

	inline CRingPosC& moveRight( const Tp& n,const Tc& c=0 ){
		ringpos::moveRight(m_p,m_c,m_s,n,c);
		return *this;
	}

	inline CRingPosC& moveLeft( const Tp& n,const Tc& c=0 ){
		ringpos::moveLeft(m_p,m_c,m_s,n,c);
		return *this;
	}

private:
	Tc m_c;	// 圈计数
	Tp m_p;	// 当前位置
	Tp m_s;	// 最大计数
};

}

#endif /* INCLUDE_DM_RINGPOS_C_HPP_ */

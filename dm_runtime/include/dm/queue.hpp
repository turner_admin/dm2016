/*
 * queue.h
 *
 *  Created on: 2015-7-17
 *      Author: work
 */

#ifndef _DM_QUEUE_H_
#define _DM_QUEUE_H_

namespace NDm {

template<typename T>
class TCQueue {
	struct SElement{
		T p;
		SElement* next;
	};
public:
	TCQueue(){
		m_header.p = 0;
		m_end = &m_header;
	}

	void push( T ){

	}
private:
	SElement m_header;
	SElement* m_end;
};

} /* namespace NDm */
#endif /* QUEUE_H_ */

﻿/*
 * link_base.hpp
 *
 *  Created on: 2020年2月22日
 *      Author: Dylan.Gao
 */

#ifndef _DM_LINK_BASE_HPP_
#define _DM_LINK_BASE_HPP_

#include <dm/types.hpp>

#ifndef NULL
#define NULL 0
#endif

namespace dm{

template<typename T>
class TCLinkBase{
public:
	TCLinkBase():m_d(){}
	TCLinkBase( const T& d ):m_d(d){}

	/**
	 * 为了兼容指针操作
	 * @return
	 */
	inline T& detach(){
		return m_d;
	}

	inline T& get(){
		return m_d;
	}

	inline const T& get()const{
		return m_d;
	}

	/**
	 * 设置新的数据指针
	 * @param p
	 */
	inline void set( const T& d ){
		m_d = d;
	}
protected:
	T m_d;
};

/**
 * 链表基类
 * 提供给单向链表和双向链表自身数据存取使用
 */
template<typename T>
class TCLinkBase<T*>{
public:
	TCLinkBase( T* p ):m_d(p){
	}

	virtual ~TCLinkBase(){
		if( m_d )
			delete m_d;
	}

	/**
	 * 获取数据指针
	 * @return
	 */
	inline T* get(){
		return m_d;
	}

	inline const T* get()const{
		return m_d;
	}

	/**
	 * 设置新的数据指针
	 * @param p
	 */
	inline void set( T* p ){
		if( m_d )
			delete m_d;
		m_d = p;
	}

	/**
	 * 分离数据指针
	 * 不再负责是否该数据
	 * @return
	 */
	inline T* detach(){
		T* r = m_d;
		m_d = NULL;
		return r;
	}

protected:
	T* m_d;	// 数据本身，由链表负责释放
};

}

#endif /* INCLUDE_DM_LINK_BASE_HPP_ */

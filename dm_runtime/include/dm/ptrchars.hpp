/*
 * ptrchars.hpp
 *
 *  Created on: 2023年4月23日
 *      Author: Dylan.Gao
 */

#ifndef _DM_PTRCHARS_HPP_
#define _DM_PTRCHARS_HPP_

namespace dm{

/**
 * 字符串指针类。
 * 实现对字符串指针的自动管理
 */
class CPtrChars{
	CPtrChars( const CPtrChars& );
	CPtrChars& operator=( const CPtrChars& );
public:
	CPtrChars( char* p=0 );
	~CPtrChars();

	CPtrChars& operator=( char* p );
	CPtrChars& operator=( const char* p );

	void clear();

	inline const char* p()const{
		return m_p;
	}

private:
	char* m_p;
};

};

#endif /* DM_RUNTIME_INCLUDE_DM_PTRCHARS_HPP_ */

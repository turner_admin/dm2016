/*
 * flaged.hpp
 *
 *  Created on: 2016年11月15日
 *      Author: work
 */

#ifndef INCLUDE_DM_FLAGED_HPP_
#define INCLUDE_DM_FLAGED_HPP_

#include <dm/bitmask.hpp>

namespace dm{

template<typename T>
class CFlaged{
public:
	CFlaged():flags(0){}
	CFlaged( const CFlaged& f):flags(f.flags){}

	CFlaged& operator=( const CFlaged& f ){
		flags = f.flags;
		return *this;
	}

	template<int f>
	inline bool ifFlag()const{
		return flags & (dm::BitMaskSet<T,f+1>::v);
	}

	template<int f>
	inline void setFlag(){
		flags |= dm::BitMaskSet<T,f+1>::v;
	}

	template<int f>
	inline void clrFlag(){
		flags &= dm::BitMaskClr<T,f+1>::v;
	}

public:
	T flags;
};

}


#endif /* INCLUDE_DM_FLAGED_HPP_ */

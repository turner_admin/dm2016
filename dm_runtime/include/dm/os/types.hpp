/*
 * types.hpp
 *
 *  Created on: 2023年8月30日
 *      Author: Dylan.Gao
 */

#ifndef _DM_OS_TYPES_HPP_
#define _DM_OS_TYPES_HPP_

#include <dm/types.hpp>

#include <boost/interprocess/sync/interprocess_upgradable_mutex.hpp>
#include <boost/interprocess/sync/upgradable_lock.hpp>
#include <boost/interprocess/sync/sharable_lock.hpp>
#include <boost/interprocess/sync/scoped_lock.hpp>

namespace dm{
namespace os{

/**
 * 进程间可升级锁
 */
typedef boost::interprocess::interprocess_upgradable_mutex upgradable_mutex_t;

typedef dm::int64 id_t;

#ifdef WIN32
typedef unsigned long pid_t;
#endif

}
}

#endif

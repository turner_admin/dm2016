﻿/*
 * stdindevice.hpp
 *
 *  Created on: 2016年12月19日
 *      Author: work
 */

#ifndef INCLUDE_DM_OS_COM_STDINDEVICE_HPP_
#define INCLUDE_DM_OS_COM_STDINDEVICE_HPP_

#include <dm/os/com/device.hpp>

namespace dm{
namespace os{
namespace com{

class CStdInDevice:virtual public CDevice{
public:
	typedef boost::asio::posix::stream_descriptor dev_t;

	CStdInDevice( ios_t& ios,const size_t& rxMaxSize=512 );
	virtual ~CStdInDevice();

protected:
	void start_rx();
	void cancelDev();

protected:
	dev_t m_dev;
};

}
}
}



#endif /* INCLUDE_DM_OS_COM_STDINDEVICE_HPP_ */

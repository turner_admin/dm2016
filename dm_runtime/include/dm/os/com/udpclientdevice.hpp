﻿/*
 * udpclientdevice.hpp
 *
 *  Created on: 2017年2月22日
 *      Author: work
 */

#ifndef _DM_OS_COM_UDPCLIENTDEVICE_HPP_
#define _DM_OS_COM_UDPCLIENTDEVICE_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_COM
#define DM_API_OS_COM DM_API_IMPORT
#endif

#include <dm/os/com/device.hpp>
#include <boost/asio/ip/udp.hpp>

namespace dm{
namespace os{
namespace com{

class DM_API_OS_COM CUdpClientDevice:virtual public CDevice{
public:
	typedef boost::asio::ip::udp::socket dev_t;
	typedef boost::asio::ip::udp::endpoint end_t;

	CUdpClientDevice( ios_t& ios,const size_t& rxBufSize=1024 );
	virtual ~CUdpClientDevice();

protected:
	virtual bool onConnected();

	bool checkAndSetAddress( const CUdpClientAddr& addr );

	bool startConnect();
	bool stopConnect();
	bool startSend( const dm::uint8* buf,const size_t& len );

	void start_rx();
	void cancelDev();

protected:
	dev_t m_dev;
	end_t m_peer;
};

}
}
}



#endif /* INCLUDE_DM_OS_COM_UDPCLIENTDEVICE_HPP_ */

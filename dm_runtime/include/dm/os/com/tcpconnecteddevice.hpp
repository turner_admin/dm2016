﻿/*
 * tcpconnecteddevice.hpp
 *
 *  Created on: 2017年2月16日
 *      Author: work
 */

#ifndef _DM_OS_COM_TCPCONNECTEDDEVICE_HPP_
#define _DM_OS_COM_TCPCONNECTEDDEVICE_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_COM
#define DM_API_OS_COM DM_API_IMPORT
#endif

#include <dm/os/com/device.hpp>

namespace dm{
namespace os{

namespace ascom{
	class CTcpListener_;
}

namespace com{

class DM_API_OS_COM CTcpConnectedDevice:virtual public CDevice{
public:
	typedef boost::asio::ip::tcp::socket dev_t;
	typedef boost::asio::ip::tcp::endpoint end_t;
	typedef boost::asio::ip::address ip_t;

	CTcpConnectedDevice( ios_t& ios,const size_t& rxBufSize=256  );
	virtual ~CTcpConnectedDevice();

protected:
	bool checkAndSetAddress( const CTcpConnectAddr& addr );

	bool stopConnect();
	bool startSend( const dm::uint8* buf,const size_t& len );

	void start_rx();
	void cancelDev();

	inline void setConnected(){
		handler_connect();
		start_rx();
	}

protected:
	dev_t m_dev;
	friend class CTcpListener;

	friend class ascom::CTcpListener_;
};
}
}
}



#endif /* INCLUDE_DM_OS_COM_TCPCONNECTEDDEVICE_HPP_ */

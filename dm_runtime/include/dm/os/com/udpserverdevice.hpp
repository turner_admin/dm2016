﻿/*
 * udpserverdevice.hpp
 *
 *  Created on: 2017年2月23日
 *      Author: work
 */

#ifndef _DM_OS_COM_UDPSERVERDEVICE_HPP_
#define _DM_OS_COM_UDPSERVERDEVICE_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_COM
#define DM_API_OS_COM DM_API_IMPORT
#endif

#include <dm/os/com/device.hpp>

namespace dm{
namespace os{
namespace com{

class DM_API_OS_COM CUdpServerDevice:virtual public CDevice{
public:
	typedef boost::asio::ip::udp::socket dev_t;
	typedef boost::asio::ip::udp::endpoint end_t;

	CUdpServerDevice( ios_t& ios,const size_t& rxBufSize=1024 );
	virtual ~CUdpServerDevice();

	void setDest( const char* host,const short& port );
	void setDest( const end_t& dest );

	inline const end_t& getDest()const{
		return m_dest;
	}

	bool asynConnect();
protected:
	void onReceived( const dm::uint8* buf,const size_t& len );

	virtual void onReceived( const end_t& peer,const dm::uint8* buf,const size_t& len );

	bool checkAndSetAddress( const CUdpServerAddr& addr );

	bool startConnect();
	bool stopConnect();
	bool startSend( const dm::uint8* buf,const size_t& len );

	void start_rx();
	void cancelDev();
protected:
	dev_t m_dev;
	end_t m_peer;
	end_t m_dest;
};

}
}
}



#endif /* INCLUDE_DM_OS_COM_UDPSERVERDEVICE_HPP_ */

﻿/*
 * udpclientaddr.hpp
 *
 *  Created on: 2016年12月16日
 *      Author: work
 */

#ifndef _DM_OS_COM_UDPCLIENTADDR_HPP_
#define _DM_OS_COM_UDPCLIENTADDR_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_COM
#define DM_API_OS_COM DM_API_IMPORT
#endif

#include <dm/os/com/addr.hpp>

namespace dm{
namespace os{
namespace com{

class DM_API_OS_COM CUdpClientAddr:public CAddr{
public:
	CUdpClientAddr( const char* host=NULL,short port=0 );
	CUdpClientAddr( const CUdpClientAddr& addr );

	CUdpClientAddr& operator=( const CUdpClientAddr& addr );

	bool operator==( const CUdpClientAddr& addr )const;

	// server:2013
	void output( std::ostream& ostr )const;

	inline void setServerHost( const char* host ){
		m_serverHost = host;
	}

	inline const host_t getServerHost()const{
		return m_serverHost;
	}

	inline void setServerPort( short port ){
		m_serverPort = port;
	}

	inline const short& getServerPort()const{
		return m_serverPort;
	}

protected:
	virtual bool fromStringList( const dm::string::CStringList& list );

protected:
	host_t m_serverHost;
	short m_serverPort;
};

}
}
}


#endif /* INCLUDE_DM_OS_COM_UDPCLIENTADDR_HPP_ */

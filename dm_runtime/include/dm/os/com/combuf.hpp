﻿/*
 * txbuf.hpp
 *
 *  Created on: 2016年12月16日
 *      Author: work
 */

#ifndef INCLUDE_DM_OS_COM_COMBUF_HPP_
#define INCLUDE_DM_OS_COM_COMBUF_HPP_

#include <dm/types.hpp>
#include <dm/ringbuf_v.hpp>

namespace dm{
namespace os{
namespace com{

typedef dm::CRingBufV<dm::uint8,dm::uint16,1024> CComBuf;

}
}
}


#endif /* INCLUDE_DM_OS_COM_COMBUF_HPP_ */

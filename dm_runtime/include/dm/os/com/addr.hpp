﻿/*
 * addr.hpp
 *
 *  Created on: 2016年12月11日
 *      Author: work
 */

#ifndef _DM_OS_COM_ADDR_HPP_
#define _DM_OS_COM_ADDR_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_COM
#define DM_API_OS_COM DM_API_IMPORT
#endif

#include <dm/fixdstring.hpp>
#include <dm/string/stringlist.hpp>
#include <string>
#include <iostream>

namespace dm{
namespace os{
namespace com{

class DM_API_OS_COM CAddr{
public:

	static const int MaxLenOfSerialDevName = 64;
	static const int MaxLenOfPortLen = 10;
	static const int MaxLenOfHostName = 32;

	static const int MaxLenOfAddr = MaxLenOfSerialDevName;

	typedef dm::CFixdString<MaxLenOfSerialDevName> serial_name_t;
	typedef dm::CFixdString<MaxLenOfPortLen> port_t;
	typedef dm::CFixdString<MaxLenOfHostName> host_t;

	CAddr(){}
	CAddr( const CAddr& ){}
	virtual ~CAddr(){}

	int getString( char* buf,int size )const;

	std::string toString()const;

	virtual bool fromString( const char* buf,const int& len=0 );

	virtual void output( std::ostream& ostr )const=0;

protected:
	virtual bool fromStringList( const dm::string::CStringList& list )=0;
};

}
}
}



#endif /* INCLUDE_DM_OS_COM_ADDR_HPP_ */

﻿/*
 * address.hpp
 *
 *  Created on: 2017年2月10日
 *      Author: work
 */

#ifndef _DM_OS_COM_ADDRESS_HPP_
#define _DM_OS_COM_ADDRESS_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_COM
#define DM_API_OS_COM DM_API_IMPORT
#endif

#include <dm/os/com/serialaddr.hpp>
#include <dm/os/com/tcpclientaddr.hpp>
#include <dm/os/com/tcpbindclientaddr.hpp>
#include <dm/os/com/tcpserveraddr.hpp>
#include <dm/os/com/tcpconnectaddr.hpp>
#include <dm/os/com/udpclientaddr.hpp>
#include <dm/os/com/udpbindclientaddr.hpp>
#include <dm/os/com/udpserveraddr.hpp>
#include <dm/os/com/udpconnectaddr.hpp>
#include <dm/os/com/canaddr.hpp>

#include <dm/types.hpp>

namespace dm{
namespace os{
namespace com{

class DM_API_OS_COM CAddress:public CAddr{
public:
	enum EType{
		SerialPort=0,
		TcpClient,
		TcpBindClient,
		TcpServer,
		TcpConnect,
		UdpClient,
		UdpBindClient,
		UdpServer,
		UdpConnect,
		Can,

		Unknow
	};

	static const char* typeString( const EType& t );

	CAddress();
	CAddress( const CAddress& address );

	CAddress( const CSerialAddr& address );

	CAddress( const CTcpClientAddr& address );
	CAddress( const CTcpBindClientAddr& address );
	CAddress( const CTcpServerAddr& address );
	CAddress( const CTcpConnectAddr& address );

	CAddress( const CUdpClientAddr& address );
	CAddress( const CUdpBindClientAddr& address );
	CAddress( const CUdpServerAddr& address );
	CAddress( const CUdpConnectAddr& address );
	CAddress( const CCanAddr& address );

	~CAddress();

	CAddress& operator=( const CAddress& address );
	CAddress& operator=( const CSerialAddr& address );

	CAddress& operator=( const CTcpClientAddr& address );
	CAddress& operator=( const CTcpBindClientAddr& address );
	CAddress& operator=( const CTcpServerAddr& address );
	CAddress& operator=( const CTcpConnectAddr& address );

	CAddress& operator=( const CUdpClientAddr& address );
	CAddress& operator=( const CUdpBindClientAddr& address );
	CAddress& operator=( const CUdpServerAddr& address );
	CAddress& operator=( const CUdpConnectAddr& address );
	CAddress& operator=( const CCanAddr& address );

	bool operator==( const CAddress& address )const;
	bool operator==( const CSerialAddr& address )const;

	bool operator==( const CTcpClientAddr& address )const;
	bool operator==( const CTcpBindClientAddr& address )const;
	bool operator==( const CTcpServerAddr& address )const;
	bool operator==( const CTcpConnectAddr& address )const;

	bool operator==( const CUdpClientAddr& address )const;
	bool operator==( const CUdpBindClientAddr& address )const;
	bool operator==( const CUdpServerAddr& address )const;
	bool operator==( const CUdpConnectAddr& address )const;
	bool operator==( const CCanAddr& address )const;

	inline const EType& getType()const{
		return m_type;
	}

	inline const char* typeName()const{
		return typeString(getType());
	}

	const CSerialAddr* asSerialPort()const;

	const CTcpClientAddr* asTcpClient()const;
	const CTcpBindClientAddr* asTcpBindClient()const;
	const CTcpServerAddr* asTcpServer()const;
	const CTcpConnectAddr* asTcpConnect()const;

	const CUdpClientAddr* asUdpClient()const;
	const CUdpBindClientAddr* asUdpBindClient()const;
	const CUdpServerAddr* asUdpServer()const;
	const CUdpConnectAddr* asUdpConnect()const;
	const CCanAddr* asCan()const;

	bool set( const EType& type,const char* str );

	void reset();

	bool fromString( const char* buf,const int& len=0 );

	void output( std::ostream& ostr )const;

protected:
	bool fromStringList( const dm::string::CStringList& list );

	template<typename T>
	inline const T* as( const EType& type )const{
		if( m_type!=type )
			return 0;
		return (const T*)m_addr;
	}

private:
	EType m_type;
	CAddr *m_addr;
};

}
}
}


#endif /* INCLUDE_DM_OS_COM_ADDRESS_HPP_ */

﻿/*
 * combufmonitor.hpp
 *
 *  Created on: 2016年12月16日
 *      Author: work
 */

#ifndef _DM_OS_COM_COMBUFMONITOR_HPP_
#define _DM_OS_COM_COMBUFMONITOR_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_COM
#define DM_API_OS_COM DM_API_IMPORT
#endif

#include <dm/os/com/combuf.hpp>

namespace dm{
namespace os{
namespace com{

class DM_API_OS_COM CComBufMonitor{
public:
	typedef CComBuf::pos_t pos_t;

	CComBufMonitor( const CComBuf* buf );

	int pop( dm::uint8* buf,int size );

private:
	pos_t m_p;

	const CComBuf* m_buf;
};

}
}
}



#endif /* INCLUDE_DM_OS_COM_COMBUFMONITOR_HPP_ */

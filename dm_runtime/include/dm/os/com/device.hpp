﻿/*
 * comdevice.hpp
 *
 *  Created on: 2017年2月7日
 *      Author: work
 */

#ifndef _DM_OS_COM_DEVICE_HPP_
#define _DM_OS_COM_DEVICE_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_COM
#define DM_API_OS_COM DM_API_IMPORT
#endif

#include <dm/os/com/types.hpp>
#include <boost/smart_ptr/scoped_array.hpp>
#include <dm/types.hpp>
#include <dm/os/com/address.hpp>

namespace dm{
namespace os{
namespace com{

class DM_API_OS_COM CDevice{
	CDevice( const CDevice& );
	CDevice& operator=( const CDevice& );

public:
	typedef boost::scoped_array<dm::uint8> buf_t;

	enum EState{
		Unconnected = 0,	// 未连接
		Connecting,			// 正在连接
		Connected,			// 已经连接
		Sending,				// 正在发送数据
		Disconnecting,		// 正在断开连接
		Error					// 故障
	};

	CDevice( const size_t& rxBufSize=256 );
	virtual ~CDevice();

	bool setAddress( const CAddress& addr );

	inline const CAddress& getAddress()const{
		return m_addr;
	}

	inline const EState& getState()const{
		return m_state;
	}

	virtual bool asynConnect();

	virtual bool asynDisconnect();

	virtual bool asynSend( const dm::uint8* buf,const size_t& len );

	virtual void handler_connect( const error_t& ec=error_t() );
	virtual void handler_disconnect( const error_t& ec=error_t() );
	virtual void handler_rx( const error_t& ec,size_t len );
	virtual void handler_tx( const error_t& ec=error_t() );
protected:
	/**
	 * 设置地址
	 * 对支持的地址类型进行重载
	 * @param addr
	 * @return
	 */
	virtual bool checkAndSetAddress( const CSerialAddr& addr );

	virtual bool checkAndSetAddress( const CTcpClientAddr& addr );
	virtual bool checkAndSetAddress( const CTcpServerAddr& addr );
	virtual bool checkAndSetAddress( const CTcpBindClientAddr& addr );
	virtual bool checkAndSetAddress( const CTcpConnectAddr& addr );

	virtual bool checkAndSetAddress( const CUdpClientAddr& addr );
	virtual bool checkAndSetAddress( const CUdpServerAddr& addr );
	virtual bool checkAndSetAddress( const CUdpBindClientAddr& addr );
	virtual bool checkAndSetAddress( const CUdpConnectAddr& addr );
	virtual bool checkAndSetAddress( const CCanAddr& addr );

	/**
	 * 连接成功时触发的函数。
	 * 默认时启动接收过程
	 * @return
	 */
	virtual bool onConnected();

	/**
	 * 连接失败时的触发函数
	 * 默认不操作
	 */
	virtual void onConnectFail();

	/**
	 * 接收到数据时的处理
	 * 默认无处理
	 * @param buf
	 * @param len
	 */
	virtual void onReceived( const dm::uint8* buf,const size_t& len );

	virtual void onReceiveFail();
	/**
	 * 发送成功处理
	 */
	virtual void onSendSuccess();
	virtual void onSendFail();
	virtual void onDisconnected();
	virtual void onDisconnectFail();

	/**
	 * 启动连接
	 * 如果支持连接，子类应该重载本函数。并打开连接，或开始连接过程
	 * 关联处理函数handler_connect
	 * @return
	 */
	virtual bool startConnect();

	/**
	 * 停止连接，或断开连接
	 * 如果连接支持断开操作，子类应该重载本函数。并关闭连接.
	 * 关联好处理函数handler_disconnect
	 * @return
	 */
	virtual bool stopConnect();

	/**
	 * 开始发送数据
	 * 如果本连接支持发送，子类应该重载本函数。
	 * 并关联好异步发送处理函数handler_tx
	 *
	 * @param buf
	 * @param len
	 * @return
	 */
	virtual bool startSend( const dm::uint8* buf,const size_t& len );

	/**
	 * 启动接收过程
	 * 关联函数handler_rx
	 */
	virtual void start_rx()=0;

	/**
	 * 取消异步事件
	 */
	virtual void cancelDev()=0;

protected:
	EState m_state;	// 状态

	buf_t m_rxBuf;
	size_t m_rxBufSize;

	CAddress  m_addr;
};

}
}
}



#endif /* INCLUDE_DM_OS_COM_DEVICE_HPP_ */

﻿/*
 * tcpclientaddr.hpp
 *
 *  Created on: 2016年12月17日
 *      Author: work
 */

#ifndef _DM_OS_COM_TCPCLIENTADDR_HPP_
#define _DM_OS_COM_TCPCLIENTADDR_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_COM
#define DM_API_OS_COM DM_API_IMPORT
#endif

#include <dm/os/com/addr.hpp>

namespace dm{
namespace os{
namespace com{

class DM_API_OS_COM CTcpClientAddr:public CAddr{
public:
	CTcpClientAddr( const char* server=NULL,short port=0 );
	CTcpClientAddr( const CTcpClientAddr& addr );

	CTcpClientAddr& operator=( const CTcpClientAddr& addr );
	bool operator==( const CTcpClientAddr& addr )const;

	// server:port
	virtual void output( std::ostream& ostr )const;

	inline void setServerHost( const char* host ){
		m_serverHost = host;
	}

	inline const host_t& getServerHost()const{
		return m_serverHost;
	}

	inline void setServerPort( short port ){
		m_serverPort = port;
	}

	inline const short& getServerPort()const{
		return m_serverPort;
	}

protected:
	virtual bool fromStringList( const dm::string::CStringList& list );

private:
	host_t m_serverHost;
	short m_serverPort;
};

}
}
}


#endif /* INCLUDE_DM_OS_COM_TCPCLIENTADDR_HPP_ */

﻿/*
 * canaddr.hpp
 *
 *  Created on: 2017年3月20日
 *      Author: work
 */

#ifndef _DM_OS_COM_CANADDR_HPP_
#define _DM_OS_COM_CANADDR_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_COM
#define DM_API_OS_COM DM_API_IMPORT
#endif

#include <dm/os/com/addr.hpp>
#include <dm/fixdstring.hpp>
#include <dm/string/stringlist.hpp>

namespace dm{
namespace os{
namespace com{

class DM_API_OS_COM CCanAddr:public CAddr{
public:
	enum{
		MaxLenOfDev = 16
	};

	typedef dm::CFixdString<MaxLenOfDev> device_t;
	typedef dm::uint32 can_id_t;

	CCanAddr( const char* dev=NULL,const can_id_t& bps=0,const can_id_t& id=0,const can_id_t& mask=0xFFFF );
	CCanAddr( const CCanAddr& addr );

	CCanAddr& operator=( const CCanAddr& addr );

	bool operator==( const CCanAddr& addr )const;

	void output( std::ostream& ostr )const;

	inline const device_t& getDevice()const{
		return m_dev;
	}

	inline void setDevice( const char* dev ){
		m_dev = dev;
	}

	inline const dm::uint32& getBps()const{
		return m_bps;
	}

	inline void setBps( const dm::uint32& bps ){
		m_bps = bps;
	}

	inline const can_id_t& getId()const{
		return m_id;
	}

	inline void setId( const can_id_t& id ){
		m_id = id;
	}

	inline const can_id_t& getMask()const{
		return m_mask;
	}

	inline void setMask( const can_id_t& mask ){
		m_mask = mask;
	}
protected:
	bool fromStringList( const dm::string::CStringList& list );

private:
	device_t m_dev;
	can_id_t m_id;		// 接收帧id
	can_id_t m_mask;	// 接收帧掩码
	dm::uint32 m_bps;	// 通信速率
};

}
}
}

#endif /* INCLUDE_DM_OS_COM_CANADDR_HPP_ */

﻿/*
 * udpserveraddr.hpp
 *
 *  Created on: 2016年12月16日
 *      Author: work
 */

#ifndef _DM_OS_COM_UDPSERVERADDR_HPP_
#define _DM_OS_COM_UDPSERVERADDR_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_COM
#define DM_API_OS_COM DM_API_IMPORT
#endif

#include <dm/os/com/addr.hpp>
#include <dm/fixdstring.hpp>

namespace dm{
namespace os{
namespace com{

class DM_API_OS_COM CUdpServerAddr:public CAddr{
public:
	CUdpServerAddr( const char* host=NULL,short port=0 );
	CUdpServerAddr( const CUdpServerAddr& addr );

	CUdpServerAddr& operator=( const CUdpServerAddr& addr );
	bool operator==( const CUdpServerAddr& addr )const;

	// localhost:port
	void output( std::ostream& ostr )const;

	inline void setLocalHost( const char* host ){
		m_localHost = host;
	}

	inline const host_t& getLocalHost()const{
		return m_localHost;
	}

	inline void setLocalPort( short port ){
		m_localPort = port;
	}

	inline const short& getLocalPort()const{
		return m_localPort;
	}

protected:
	virtual bool fromStringList( const dm::string::CStringList& list );

protected:
	host_t m_localHost;
	short m_localPort;
};

}
}
}



#endif /* INCLUDE_DM_OS_COM_UDPSERVERADDR_HPP_ */

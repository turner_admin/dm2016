﻿/*
 * tcpclientdevice.hpp
 *
 *  Created on: 2016年12月20日
 *      Author: work
 */

#ifndef _DM_OS_COM_TCPCLIENTDEVICE_HPP_
#define _DM_OS_COM_TCPCLIENTDEVICE_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_COM
#define DM_API_OS_COM DM_API_IMPORT
#endif

#include <dm/os/com/device.hpp>

namespace dm{
namespace os{
namespace com{

class DM_API_OS_COM CTcpClientDevice:virtual public CDevice{
public:
	typedef boost::asio::ip::tcp::socket dev_t;
	typedef boost::asio::ip::tcp::endpoint end_t;
	typedef boost::asio::ip::address ip_t;

	CTcpClientDevice( ios_t& ios,size_t rxBufSize=1024 );
	virtual ~CTcpClientDevice();

protected:
	bool checkAndSetAddress( const CTcpClientAddr& addr );

	bool startConnect();
	bool stopConnect();
	bool startSend( const dm::uint8* buf,const size_t& len );

	void start_rx();
	void cancelDev();

private:
	dev_t m_dev;
};

}
}
}



#endif /* INCLUDE_DM_OS_COM_TCPCLIENTDEVICE_HPP_ */

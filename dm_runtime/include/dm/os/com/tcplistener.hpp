﻿/*
 * tcplistener.hpp
 *
 *  Created on: 2017年2月16日
 *      Author: work
 */

#ifndef _DM_OS_COM_TCPLISTENER_HPP_
#define _DM_OS_COM_TCPLISTENER_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_COM
#define DM_API_OS_COM DM_API_IMPORT
#endif

#include <boost/asio/ip/tcp.hpp>
#include <dm/os/com/listener.hpp>
#include <dm/os/com/tcpconnecteddevice.hpp>

namespace dm{
namespace os{
namespace com{

class DM_API_OS_COM CTcpListener:virtual public CListener{
public:
	typedef boost::asio::ip::tcp::acceptor acceptor_t;
	typedef boost::asio::ip::tcp::endpoint end_t;

	CTcpListener( ios_t& ios );
	virtual ~CTcpListener();

protected:
	bool checkAndSetAddress( const CTcpServerAddr& addr );

	virtual void setConnectOnConnect( device_t* connect );

	bool openPort();
	bool closePort();
	virtual device_t* startAccept( const size_t& bufSize=1024 );

protected:
	ios_t* m_ios;
	acceptor_t m_acceptor;
	end_t m_peer;
};

}
}
}



#endif /* INCLUDE_DM_OS_COM_TCPLISTENER_HPP_ */

﻿/*
 * tcpbindclientaddr.hpp
 *
 *  Created on: 2017年2月10日
 *      Author: work
 */

#ifndef _DM_OS_COM_TCPBINDCLIENTADDR_HPP_
#define _DM_OS_COM_TCPBINDCLIENTADDR_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_COM
#define DM_API_OS_COM DM_API_IMPORT
#endif

#include <dm/os/com/addr.hpp>

namespace dm{
namespace os{
namespace com{

class DM_API_OS_COM CTcpBindClientAddr:public CAddr{
public:
	CTcpBindClientAddr( const char* local=NULL,const short& localPort=0,const char* server=NULL,const short& serverPort=0 );

	CTcpBindClientAddr( const CTcpBindClientAddr& addr );

	inline void setServerHost( const char* host ){
		m_serverHost = host;
	}

	inline const host_t& getServerHost()const{
		return m_serverHost;
	}

	inline void setServerPort( short port ){
		m_serverPort = port;
	}

	inline const short& getServerPort()const{
		return m_serverPort;
	}

	inline void setLocalHost( const char* host ){
		m_localHost = host;
	}

	inline const char* getLocalHost()const{
		return m_localHost.c_str();
	}

	inline void setLocalPort( short port ){
		m_localPort = port;
	}

	inline const short& getLocalPort()const{
		return m_localPort;
	}

	CTcpBindClientAddr& operator=( const CTcpBindClientAddr& addr );
	bool operator==( const CTcpBindClientAddr& addr )const;

	void output( std::ostream& ostr )const;

protected:
	bool fromStringList( const dm::string::CStringList& list );
private:
	host_t m_localHost;
	short m_localPort;
	host_t m_serverHost;
	short m_serverPort;
};

}
}
}



#endif /* INCLUDE_DM_OS_COM_TCPBINDCLIENTADDR_HPP_ */

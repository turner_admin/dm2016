﻿/*
 * candevice.hpp
 *
 *  Created on: 2017年3月20日
 *      Author: work
 */

#ifndef _DM_OS_COM_CANDEVICE_HPP_
#define _DM_OS_COM_CANDEVICE_HPP_

#include <dm/os/com/device.hpp>
#include <boost/asio/generic/raw_protocol.hpp>
#include <linux/can.h>

namespace dm{
namespace os{
namespace com{

class CCanDevice:virtual public CDevice{
public:
	typedef struct can_frame can_frame_t;

	typedef boost::asio::generic::raw_protocol::socket dev_t;

	CCanDevice( ios_t& ios,const size_t& rxBufSize=16 );
	virtual ~CCanDevice();

protected:
	bool checkAndSetAddress( const CCanAddr& addr );

	bool startConnect();
	bool stopConnect();
	bool startSend( const dm::uint8* buf,const size_t& len );

	void start_rx();
	void cancelDev();

protected:
	dev_t m_dev;
};

}
}
}



#endif /* INCLUDE_DM_OS_COM_CANDEVICE_HPP_ */

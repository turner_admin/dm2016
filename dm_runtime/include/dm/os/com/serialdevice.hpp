﻿/*
 * serialport.hpp
 *
 *  Created on: 2016年12月18日
 *      Author: work
 */

#ifndef _DM_OS_COM_SERIALDEVICE_HPP_
#define _DM_OS_COM_SERIALDEVICE_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_COM
#define DM_API_OS_COM DM_API_IMPORT
#endif

#include <dm/os/com/device.hpp>

namespace dm{
namespace os{
namespace com{

class DM_API_OS_COM CSerialDevice:virtual public CDevice{
public:
	typedef boost::asio::serial_port dev_t;

	CSerialDevice( ios_t& ios,size_t rxBufSize=256 );
	virtual ~CSerialDevice();

protected:
	bool checkAndSetAddress( const CSerialAddr& addr );

	bool startConnect();
	bool stopConnect();
	bool startSend( const dm::uint8* buf,const size_t& len );

	void start_rx();
	void cancelDev();

private:
	dev_t m_dev;
};

}
}
}



#endif /* INCLUDE_DM_OS_COM_SERIALDEVICE_HPP_ */

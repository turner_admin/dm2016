﻿/*
 * serialaddr.hpp
 *
 *  Created on: 2016年12月11日
 *      Author: work
 */

#ifndef _DM_OS_COM_SERIALADDR_HPP_
#define _DM_OS_COM_SERIALADDR_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_COM
#define DM_API_OS_COM DM_API_IMPORT
#endif

#include <dm/fixdstring.hpp>
#include <dm/string/stringlist.hpp>
#include <dm/os/com/addr.hpp>

#ifndef DM_OSCOM_MAX_LEN_OF_SERIALADDR_DEV
#define DM_OSCOM_MAX_LEN_OF_SERIALADDR_DEV 64
#endif

namespace dm{
namespace os{
namespace com{

class DM_API_OS_COM CSerialAddr:public CAddr{
public:
	enum{
		MaxLenOfDev = DM_OSCOM_MAX_LEN_OF_SERIALADDR_DEV,
		MaxLenOfString = MaxLenOfDev + 24
	};

	typedef dm::CFixdString<MaxLenOfDev> device_t;
	typedef dm::CFixdString<MaxLenOfString> string_t;

	typedef dm::uint32 baud_t;
	typedef dm::uint8 bits_t;

	enum EStopBits{
		One,
		OneAndHalf,
		Two
	};

	enum EParity{
		None,
		Odd,
		Even
	};

	enum EFlowCtl{
		NoCtl,
		Soft,
		Hardware
	};

	CSerialAddr( const char* dev=NULL,
			baud_t bps=9600,
			bits_t charBits=8,EStopBits stopBits=One,
			EParity parity=None,EFlowCtl flowCtl=NoCtl );

	CSerialAddr( const CSerialAddr& addr );

	CSerialAddr& operator=( const CSerialAddr& addr );

	bool operator==( const CSerialAddr& addr )const;

	// /dev/ttyS1:115200:8:1:0:0
	// 设备名
	// 波特率
	// 数据位
	// 停止位 0：1位 1：1.5 2：2位
	// 校验位 0：无 1：奇校验 2：偶校验
	// 硬件控制位 0：无 1：soft 2：hardware
	void output( std::ostream& ostr )const;

	inline void setDevice( const char* dev ){
		m_device = dev;
	}

	inline const char* getDevice()const{
		return m_device.c_str();
	}

	inline void setBps( baud_t bps ){
		m_bps = bps;
	}

	inline const baud_t& getBps()const{
		return m_bps;
	}

	inline void setCharBits( bits_t len ){
		m_charbits = len;
	}

	inline const bits_t& getCharBits()const{
		return m_charbits;
	}


	inline void setStop( EStopBits stop ){
		m_stop = stop;
	}

	inline const EStopBits& getStop()const{
		return m_stop;
	}

	inline void setParity( EParity mode ){
		m_parity = mode;
	}

	inline const EParity& getParity()const{
		return m_parity;
	}

	inline void setFlowCtl( EFlowCtl flowCtl ){
		m_flowCtl = flowCtl;
	}

	inline const EFlowCtl& getFlowCtl()const{
		return m_flowCtl;
	}

protected:
	bool fromStringList( const dm::string::CStringList& list );

private:
	device_t m_device;
	baud_t m_bps;
	bits_t m_charbits;
	EStopBits m_stop;
	EParity m_parity;
	EFlowCtl m_flowCtl;
};

}
}
}



#endif /* INCLUDE_DM_OS_COM_SERIALADDR_HPP_ */

﻿/*
 * devicestatus.hpp
 *
 *  Created on: 2016年12月18日
 *      Author: work
 */

#ifndef INCLUDE_DM_OS_COM_DEVICEINFO_HPP_
#define INCLUDE_DM_OS_COM_DEVICEINFO_HPP_

#include <dm/fixdstring.hpp>
#include <dm/os/com/comaddr.hpp>

namespace dm{
namespace os{
namespace com{

class CDeviceInfo{
	CDeviceInfo( const CDeviceInfo& );
	CDeviceInfo& operator=( const CDeviceInfo& );

public:
	enum {
		NameLen = 32
	};

	typedef dm::CFixdString<NameLen> name_t;

	CDeviceInfo();

public:
	name_t name;
	CComAddr addr;
};

}
}
}


#endif /* INCLUDE_DM_OS_COM_DEVICEINFO_HPP_ */

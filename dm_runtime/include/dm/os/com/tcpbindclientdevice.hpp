﻿/*
 * tcpbindclientdevice.hpp
 *
 *  Created on: 2017年2月21日
 *      Author: work
 */

#include <dm/export.hpp>

#ifndef DM_API_OS_COM
#define DM_API_OS_COM DM_API_IMPORT
#endif

#ifndef _DM_OS_COM_TCPBINDCLIENTDEVICE_HPP_
#define _DM_OS_COM_TCPBINDCLIENTDEVICE_HPP_

#include <dm/os/com/device.hpp>

namespace dm{
namespace os{
namespace com{

class DM_API_OS_COM CTcpBindClientDevice:virtual public CDevice{
public:
	typedef boost::asio::ip::tcp::socket dev_t;
	typedef boost::asio::ip::tcp::endpoint end_t;
	typedef boost::asio::ip::address ip_t;

	CTcpBindClientDevice( ios_t& ios,const size_t& rxBufSize=256 );
	virtual ~CTcpBindClientDevice();

protected:
	bool checkAndSetAddress( const CTcpBindClientAddr& addr );

	bool startConnect();
	bool stopConnect();
	bool startSend( const dm::uint8* buf,const size_t& len );

	void start_rx();
	void cancelDev();

private:
	dev_t m_dev;
};
}
}
}



#endif /* INCLUDE_DM_OS_COM_TCPBINDCLIENTDEVICE_HPP_ */

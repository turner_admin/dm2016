﻿/*
 * tcpserveraddr.hpp
 *
 *  Created on: 2016年12月11日
 *      Author: work
 */

#ifndef _DM_OS_COM_TCPSERVERADDR_HPP_
#define _DM_OS_COM_TCPSERVERADDR_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_COM
#define DM_API_OS_COM DM_API_IMPORT
#endif

#include <dm/os/com/addr.hpp>

namespace dm{
namespace os{
namespace com{

class DM_API_OS_COM CTcpServerAddr:public CAddr{
public:
	CTcpServerAddr( const char* host=NULL,short port=0 );
	CTcpServerAddr( const CTcpServerAddr& addr );

	CTcpServerAddr& operator=( const CTcpServerAddr& addr );
	bool operator==( const CTcpServerAddr& addr )const;

	// server:port
	void output( std::ostream& ostr )const;

	inline void setLocalHost( const char* host ){
		m_localHost = host;
	}

	inline const char* getLocalHost()const{
		return m_localHost.c_str();
	}

	inline void setLocalPort( short port ){
		m_localPort = port;
	}

	inline const short& getLocalPort()const{
		return m_localPort;
	}

protected:
	bool fromStringList( const dm::string::CStringList& list );

private:
	host_t m_localHost;
	short m_localPort;
};

}
}
}



#endif /* INCLUDE_DM_OS_COM_TCPSERVERADDR_HPP_ */

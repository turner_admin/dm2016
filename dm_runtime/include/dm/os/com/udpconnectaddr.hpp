﻿/*
 * udpconnectaddr.hpp
 *
 *  Created on: 2017年2月15日
 *      Author: work
 */

#ifndef _DM_OS_COM_UDPCONNECTADDR_HPP_
#define _DM_OS_COM_UDPCONNECTADDR_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_COM
#define DM_API_OS_COM DM_API_IMPORT
#endif

#include <dm/os/com/addr.hpp>

namespace dm{
namespace os{
namespace com{

class DM_API_OS_COM CUdpConnectAddr:public CAddr{
public:
	CUdpConnectAddr( const char* host=NULL,const short& port=0 ):CAddr(),m_peerHost(host),m_peerPort(port){
	}

	CUdpConnectAddr( const CUdpConnectAddr& addr ):CAddr(),m_peerHost(addr.m_peerHost),m_peerPort(addr.m_peerPort){
	}

	CUdpConnectAddr& operator=( const CUdpConnectAddr& addr );

	bool operator==( const CUdpConnectAddr& addr )const;

	void output( std::ostream& ostr )const;

	inline const host_t& getPeerHost()const{
		return m_peerHost;
	}

	inline void setPeerHost( const char* host ){
		m_peerHost = host;
	}

	inline const short& getPeerPort()const{
		return m_peerPort;
	}

	inline void setPeerPort( const short& port ){
		m_peerPort = port;
	}

protected:
	bool fromStringList( const dm::string::CStringList& list );
private:
	host_t m_peerHost;
	short m_peerPort;
};
}
}
}



#endif /* INCLUDE_DM_OS_COM_UDPCONNECTADDR_HPP_ */

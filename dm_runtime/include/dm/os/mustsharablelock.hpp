﻿/*
 * mustsharablelock.hpp
 *
 *  Created on: 2016年11月14日
 *      Author: Dylan.Gao
 */

#ifndef _DM_OS_MUSTSHARABLELOCK_HPP_
#define _DM_OS_MUSTSHARABLELOCK_HPP_

#include <boost/date_time/posix_time/posix_time.hpp>

namespace dm{
namespace os{

template< typename TSharableLock >
class CMustSharableLock{
public:
	CMustSharableLock( TSharableLock& l,const long& seconds=5 ):m_lock(l){
		m_lock.timed_lock_sharable(boost::posix_time::microsec_clock::universal_time()+boost::posix_time::seconds(seconds));
	}

	~CMustSharableLock(){
		m_lock.unlock_sharable();
	}

private:
	TSharableLock& m_lock;
};

}
}

#endif /* INCLUDE_DM_OS_MUSTSHARABLELOCK_HPP_ */

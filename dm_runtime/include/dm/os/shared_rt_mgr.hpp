/*
 * shared_rt_mgr.hpp
 *
 *  Created on: 2023年10月16日
 *      Author: Dylan.Gao
 */

#ifndef _DM_OS_SHARED_RT_MGR_HPP_
#define _DM_OS_SHARED_RT_MGR_HPP_

#include <dm/os/shared_buffer_mgr.hpp>
#include <dm/os/shared_rt_record.hpp>
#include <dm/combine.hpp>
#include <dm/os/types.hpp>
#include <dm/os/db/db.hpp>
#include <dm/os/db/resultset.hpp>
#include <dm/scoped_ptr.hpp>

namespace dm{
namespace os{

/**
 * 共享实时数据管理器
 * @tparam TInfo
 * @tparam TValue
 * @tparam SizeOfMasks
 * @tparam SizeOfName
 */
template<typename TInfo,typename TValue,typename TMask=dm::uint8,int SizeOfMasks=8,int SizeOfName=64>
class TCSharedRtMgr{
public:

	// 记录类型
	typedef dm::os::TSSharedRtRecord<TInfo,TValue,id_t> record_t;

	// 共享缓存管理器类
    typedef dm::os::TCSharedBufferMgr<record_t,id_t,TMask,SizeOfMasks,SizeOfName> buf_mgr_t;

	// 位置类型
	typedef buf_mgr_t pos_t;

	/**
	 * 查找器类
	 */
	class CFinder:public buf_mgr_t::CFinder{
	public:
		bool compareId( record_t& record,const id_t& id ){
			typename::dm::os::TSSharedRtRecord<TInfo,TValue,id_t>::CIdComparer comparer;
			return record.compareById(id, comparer);
		}

		bool compareName( record_t& record,const char* name ){
			typename::dm::os::TSSharedRtRecord<TInfo,TValue,id_t>::CNameComparer comparer;
			return record.compareByName(name, comparer);
		}
	};

	/**
	 * 读取器类
	 */
	class CReader:public buf_mgr_t::CReader{
	public:
		/**
		 * value类型
		 */
		typedef dm::TSCombine4<TInfo*,TValue*,dm::CTimeStamp*,dm::CRunTimeStamp*> value_t;

		bool readRecord( record_t& record,void* value ){
			value_t* pValue = reinterpret_cast<value_t*>(value);
			if( !pValue ){
				return false;
			}

			typename::dm::os::TSSharedRtRecord<TInfo,TValue,id_t>::CNormalReader reader;
			return record.get(nullptr, reader, pValue->e1, pValue->e2, pValue->e3, pValue->e4);
		}
	};

	/**
	 * 共享实时数据
	 */
	class CSharedRt{
	public:
		CSharedRt( TCSharedRtMgr& mgr );
		CSharedRt( TCSharedRtMgr& mgr,pos_t& pos );
		virtual ~CSharedRt();

		void setPos( pos_t& pos );

		/**
		 * 刷新数据信息
		 * 由于数据为共享，需要通过刷新，才能获取
		 * @return
		 */
		bool refresh();

		/**
		 * 获取信息
		 * @return
		 */
		inline const TInfo& getInfo()const{
			return m_info;
		}

		/**
		 * 获取数据值
		 * @return
		 */
		inline const TValue& getValue()const{
			return m_value;
		}

		/**
		 * 获取时标
		 * @return
		 */
		inline const dm::CTimeStamp& getTimeStamp()const{
			return m_ts;
		}

		inline const dm::CRunTimeStamp& getRunTimeStamp()const{
			return m_rts;
		}

		/**
		 * 获取位置信息
		 * @return
		 */
		inline const pos_t& getPos()const{
			return m_pos;
		}

		bool update( const TValue* value,const dm::CTimeStamp& tsNow=dm::CTimeStamp::cur(),const dm::CRunTimeStamp& rts=dm::CRunTimeStamp::cur() );

	protected:
		TCSharedRtMgr& m_mgr;	// 管理器
		pos_t m_pos;			// 数据位置

		TInfo m_info;	// 缓存信息
		TValue m_value;	// 缓存值

		dm::CTimeStamp m_ts;		// 时标
		dm::CRunTimeStamp m_rts;	// 运行时标
	};

    TCSharedRtMgr( const char* name,typename::dm::os::TCSharedBufferMgr<record_t,id_t,TMask,SizeOfMasks,SizeOfName>::CLoader& loader );

	/**
	 * 加载新的数据
	 * @return
	 */
	bool loadNews();

	/**
	 * 获取当前记录数
	 * @return
	 */
	inline dm::uint32 count(){
		return m_bufMgr->maskedCount();
	}

	/**
	 * 获取第一条记录的位置
	 * @return
	 */
	inline pos_t firstPos(){
		return m_bufMgr->firstMasked();
	}

	/**
	 * 获取下一个位置
	 * @param pos
	 * @return
	 */
	inline pos_t nextPos( pos_t& pos ){
		return m_bufMgr->nextMasked(pos);
	}

	/**
	 * 获取最后一条记录的位置
	 */
	inline pos_t lastPos(){
		return m_bufMgr->lastMasked();
	}

	/**
	 * 获取上一个位置
	 * @param pos
	 * @return
	 */
	inline pos_t lastPos( pos_t& pos ){
		return m_bufMgr->lastMasked(pos);
	}

	/**
	 * 根据ID获取位置
	 * @param id
	 * @return
	 */
	pos_t getPos( const id_t& id );

	/**
	 * 根据名字获取位置
	 * @param name
	 * @return
	 */
	pos_t getPos( const char* name );

	bool reload( pos_t& pos );
	bool reload( const id_t& id );
	bool reload( const char* name );

	bool unload( pos_t& pos );
	bool unload( const id_t& id );
	bool unload( const char* name );

	CSharedRt first();
	CSharedRt next( CSharedRt& sharedRt );
	CSharedRt last();
	CSharedRt last( CSharedRt& sharedRt );
	CSharedRt get( const id_t& id );
	CSharedRt get( const char* name );

	void reset();

private:
	dm::TScopedPtr<buf_mgr_t> m_bufMgr;

	friend class CSharedRt;
};

}
}

#endif /* _DM_OS_SHARED_RT_MGR_HPP_ */

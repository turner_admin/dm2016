﻿/*
 * mustlock.hpp
 *
 *  Created on: 2016年11月5日
 *      Author: Dylan.Gao
 */

#ifndef _DM_OS_MUSTLOCK_HPP_
#define _DM_OS_MUSTLOCK_HPP_

#include <boost/date_time/posix_time/posix_time.hpp>

namespace dm{
namespace os{

template< typename TLock >
class CMustLock{
public:
	CMustLock( TLock& l,const long& seconds=5 ):m_lock(l){
		m_lock.timed_lock(boost::posix_time::microsec_clock::universal_time()+boost::posix_time::seconds(seconds));
	}

	~CMustLock(){
		m_lock.unlock();
	}

private:
	TLock& m_lock;
};

}
}


#endif /* INCLUDE_DM_OS_MUSTLOCK_HPP_ */

#ifndef _DM_OS_CONSOLE_HPP
#define _DM_OS_CONSOLE_HPP

#ifdef WIN32
#include <Windows.h>
#endif

namespace dm{
namespace os{

/**
 * 命令行支持UTF8编码
*/
inline static void consoleUtf8(){
#ifdef WIN32
	SetConsoleCP(CP_UTF8);
	SetConsoleOutputCP(CP_UTF8);
#endif
}

}
}

#endif

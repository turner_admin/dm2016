﻿/*
 * options.hpp
 *
 *  Created on: 2018年2月22日
 *      Author: turner
 */

#ifndef _DM_ENV_OPTIONS_HPP_
#define _DM_ENV_OPTIONS_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_OPTIONS
#define DM_API_OS_OPTIONS DM_API_IMPORT
#endif

#include <string>
#include <iostream>
#include <dm/scoped_ptr.hpp>

namespace dm{
namespace os{
	class CDb;
}

namespace env{

/**
 * 选项配置
 */
class DM_API_OS_OPTIONS COptions{
	COptions( const COptions& );
public:
	COptions();
	~COptions();

	bool init();
	bool add( const char* option,const char* group=NULL );
	bool clear( const char* option=NULL,const char* group=NULL );

	bool set( const char* option,const bool& value,const char* group=NULL );
	bool set( const char* option,const int& value,const char* group=NULL );
	bool set( const char* option,const float& value,const char* group=NULL );
	bool set( const char* option,const char* value,const char* group=NULL );

	bool get( const char* option,bool& value,const char* group=NULL);
	bool get( const char* option,int& value,const char* group=NULL );
	bool get( const char* option,float& value,const char* group=NULL );
	bool get( const char* option,std::string& value,const char* group=NULL  );

	bool getBool( const char* option,const bool& defaultValue,const char* group=NULL );
	int getInt( const char* option,const int& defaultValue,const char* group=NULL );
	float getFloat( const char* option,const float& defaultValue,const char* group=NULL );
	std::string getString( const char* option,const char* defaultValue,const char* group=NULL );

	void show( const char* option=NULL,const char* group=NULL,std::ostream& ostr=std::cout );

private:
	dm::TScopedPtr<dm::os::CDb> m_db;
};

}
}

#endif /* DM_RUNTIME_INCLUDE_DM_ENV_OPTIONS_HPP_ */

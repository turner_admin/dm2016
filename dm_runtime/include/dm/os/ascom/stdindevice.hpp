﻿/*
 * stdindevice.hpp
 *
 *  Created on: 2016年12月19日
 *      Author: work
 */

#ifndef INCLUDE_DM_OS_ASCOM_STDINDEVICE_HPP_
#define INCLUDE_DM_OS_ASCOM_STDINDEVICE_HPP_

#include <dm/os/com/stdindevice.hpp>

#include <dm/os/ascom/device.hpp>

namespace dm{
namespace os{
namespace ascom{

typedef TDevice<com::CStdInDevice> CStdInDevice;

}
}
}



#endif /* INCLUDE_DM_OS_ASCOM_STDINDEVICE_HPP_ */

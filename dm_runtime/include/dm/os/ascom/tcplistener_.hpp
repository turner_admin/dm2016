﻿/*
 * tcplistener_.hpp
 *
 *  Created on: 2017年3月2日
 *      Author: work
 */

#ifndef _DM_OS_ASCOM_TCPLISTENER__HPP_
#define _DM_OS_ASCOM_TCPLISTENER__HPP_

#ifndef DM_API_OS_ASCOM
#define DM_API_OS_ASCOM DM_API_IMPORT
#endif

#include <dm/os/com/tcplistener.hpp>

namespace dm{
namespace os{
namespace ascom{

class DM_API_OS_ASCOM CTcpListener_:public com::CTcpListener{
public:
	CTcpListener_( com::ios_t& ios );
	virtual ~CTcpListener_();

protected:
	device_t* startAccept( const size_t& bufSize=1024 );
};

}
}
}

#endif /* INCLUDE_DM_OS_ASCOM_TCPLISTENER__HPP_ */

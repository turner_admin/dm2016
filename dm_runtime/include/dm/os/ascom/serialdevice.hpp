﻿/*
 * serialdevice.hpp
 *
 *  Created on: 2016年12月21日
 *      Author: work
 */

#ifndef INCLUDE_DM_OS_ASCOM_SERIALDEVICE_HPP_
#define INCLUDE_DM_OS_ASCOM_SERIALDEVICE_HPP_

#include <dm/os/com/serialdevice.hpp>
#include <dm/os/ascom/device.hpp>

namespace dm{
namespace os{
namespace ascom{

typedef TDevice<com::CSerialDevice> CSerialDevice;

}
}
}



#endif /* INCLUDE_DM_OS_ASCOM_SERIALDEVICE_HPP_ */

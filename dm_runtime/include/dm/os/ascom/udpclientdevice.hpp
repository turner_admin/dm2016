﻿/*
 * udpclientdevice.hpp
 *
 *  Created on: 2017年2月23日
 *      Author: work
 */

#ifndef INCLUDE_DM_OS_ASCOM_UDPCLIENTDEVICE_HPP_
#define INCLUDE_DM_OS_ASCOM_UDPCLIENTDEVICE_HPP_

#include <dm/os/ascom/device.hpp>
#include <dm/os/com/udpclientdevice.hpp>

namespace dm{
namespace os{
namespace ascom{

typedef TDevice<com::CUdpClientDevice> CUdpClientDevice;

}
}
}



#endif /* INCLUDE_DM_OS_ASCOM_UDPCLIENTDEVICE_HPP_ */

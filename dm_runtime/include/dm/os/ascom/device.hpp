﻿/*
 * device.hpp
 *
 *  Created on: 2017年2月23日
 *      Author: work
 */

#ifndef _DM_OS_ASCOM_DEVICE_HPP_
#define _DM_OS_ASCOM_DEVICE_HPP_

#ifndef DM_API_OS_ASCOM
#define DM_API_OS_ASCOM DM_API_IMPORT
#endif

#include <dm/os/ascom/types.hpp>
#include <dm/os/com/device.hpp>

namespace dm{
namespace os{
namespace ascom{

class DM_API_OS_ASCOM CDevice:virtual public com::CDevice{
	CDevice( const CDevice& );
public:
	CDevice( const size_t& rxBufSize=1024 );
	virtual ~CDevice();

	typedef void (f_buf_t)( const dm::uint8*,const size_t& );
	typedef void (f_void_t)();
	typedef bool (f_bool_t)( void );

	typedef boost::signals2::signal<f_buf_t> sig_buf_t;
	typedef boost::signals2::signal<f_void_t> sig_void_t;
	typedef boost::signals2::signal<f_bool_t> sig_bool_t;

	sig_buf_t sig_received;
	sig_void_t sig_receiveFail;
	sig_bool_t sig_connected;
	sig_void_t sig_connectFail;
	sig_void_t sig_sendSuccess;
	sig_void_t sig_sendFail;
	sig_void_t sig_disconnected;
	sig_void_t sig_disconnectFail;

protected:
	bool onConnected();
	void onConnectFail();
	void onReceived( const dm::uint8* buf,const size_t& len );
	void onReceiveFail();
	void onSendSuccess();
	void onSendFail();
	void onDisconnected();
	void onDisconnectFail();
};

template<typename T>
class TDevice:virtual public CDevice,virtual public T{
	TDevice( const TDevice& );
	TDevice& operator=( const TDevice& );
public:
	TDevice( com::ios_t& ios,const size_t& rxBufSize=1024 ):com::CDevice(rxBufSize),
		CDevice(rxBufSize),T(ios,rxBufSize){
	}
};

}
}
}


#endif /* INCLUDE_DM_OS_ASCOM_DEVICE_HPP_ */

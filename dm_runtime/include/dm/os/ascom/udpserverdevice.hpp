﻿/*
 * udpserverdevice.hpp
 *
 *  Created on: 2017年2月23日
 *      Author: work
 */

#ifndef INCLUDE_DM_OS_ASCOM_UDPSERVERDEVICE_HPP_
#define INCLUDE_DM_OS_ASCOM_UDPSERVERDEVICE_HPP_

#include <dm/os/ascom/device.hpp>
#include <dm/os/com/udpserverdevice.hpp>

namespace dm{
namespace os{
namespace ascom{

typedef TDevice<com::CUdpServerDevice> CUdpServerDevice;

}
}
}



#endif /* INCLUDE_DM_OS_ASCOM_UDPSERVERDEVICE_HPP_ */

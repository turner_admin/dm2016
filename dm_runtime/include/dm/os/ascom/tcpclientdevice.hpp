﻿/*
 * tcpclientdevice.hpp
 *
 *  Created on: 2016年12月20日
 *      Author: work
 */

#ifndef INCLUDE_DM_OS_ASCOM_TCPCLIENTDEVICE_HPP_
#define INCLUDE_DM_OS_ASCOM_TCPCLIENTDEVICE_HPP_

#include <dm/os/com/tcpclientdevice.hpp>
#include <dm/os/ascom/device.hpp>

namespace dm{
namespace os{
namespace ascom{

typedef TDevice<com::CTcpClientDevice> CTcpClientDevice;

}
}
}



#endif /* INCLUDE_DM_OS_ASCOM_TCPCLIENTDEVICE_HPP_ */

﻿/*
 * tcplistner.hpp
 *
 *  Created on: 2017年2月23日
 *      Author: work
 */

#ifndef INCLUDE_DM_OS_ASCOM_TCPLISTNER_HPP_
#define INCLUDE_DM_OS_ASCOM_TCPLISTNER_HPP_

#include <dm/os/ascom/listener.hpp>
#include <dm/os/ascom/tcplistener_.hpp>

namespace dm{
namespace os{
namespace ascom{

typedef TListener<CTcpListener_> CTcpListner;

}
}
}



#endif /* INCLUDE_DM_OS_ASCOM_TCPLISTNER_HPP_ */

﻿/*
 * timer.hpp
 *
 *  Created on: 2016年12月19日
 *      Author: work
 */

#ifndef _DM_OS_ASCOM_TIMER_HPP_
#define _DM_OS_ASCOM_TIMER_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_ASCOM
#define DM_API_OS_ASCOM DM_API_IMPORT
#endif

#include <dm/os/com/timer.hpp>
#include <dm/os/ascom/types.hpp>

namespace dm{
namespace os{
namespace ascom{

class DM_API_OS_ASCOM CTimer:public com::CTimer{
public:
	CTimer( com::CTimer::ios_t& ios );
	~CTimer();

	boost::signals2::signal<void()> sig_timeout;

protected:
	void on_timeout();
};

}
}
}



#endif /* INCLUDE_DM_OS_ASCOM_TIMER_HPP_ */

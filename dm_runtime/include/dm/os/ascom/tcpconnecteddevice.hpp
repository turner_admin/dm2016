﻿/*
 * tcpconnectdevice.hpp
 *
 *  Created on: 2017年2月23日
 *      Author: work
 */

#ifndef INCLUDE_DM_OS_ASCOM_TCPCONNECTEDDEVICE_HPP_
#define INCLUDE_DM_OS_ASCOM_TCPCONNECTEDDEVICE_HPP_

#include <dm/os/ascom/device.hpp>
#include <dm/os/com/tcpconnecteddevice.hpp>

namespace dm{
namespace os{
namespace ascom{

typedef TDevice<com::CTcpConnectedDevice> CTcpConnectedDevice;

}
}
}



#endif /* INCLUDE_DM_OS_ASCOM_TCPCONNECTEDDEVICE_HPP_ */

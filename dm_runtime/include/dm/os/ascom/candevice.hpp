﻿/*
 * candevice.hpp
 *
 *  Created on: 2017年3月22日
 *      Author: work
 */

#ifndef _DM_OS_ASCOM_CANDEVICE_HPP_
#define _DM_OS_ASCOM_CANDEVICE_HPP_

#include <dm/os/com/candevice.hpp>
#include <dm/os/ascom/device.hpp>

namespace dm{
namespace os{
namespace ascom{

typedef TDevice<com::CCanDevice> CCanDevice;

}
}
}



#endif /* INCLUDE_DM_OS_ASCOM_CANDEVICE_HPP_ */

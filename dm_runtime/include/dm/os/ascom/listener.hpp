﻿/*
 * listener.hpp
 *
 *  Created on: 2017年2月23日
 *      Author: work
 */

#ifndef _DM_OS_ASCOM_LISTENER_HPP_
#define _DM_OS_ASCOM_LISTENER_HPP_

#ifndef DM_API_OS_ASCOM
#define DM_API_OS_ASCOM DM_API_IMPORT
#endif

#include <dm/os/ascom/types.hpp>
#include <dm/os/com/listener.hpp>

namespace dm{
namespace os{
namespace ascom{

class DM_API_OS_ASCOM CListener:virtual public com::CListener{
public:
	typedef void (f_connect_t)( com::CListener::device_t* );

	typedef boost::signals2::signal<f_connect_t> sig_connect_t;

	sig_connect_t sig_newConnect;

	CListener();
	virtual ~CListener();

	void onNewConnection( com::CListener::device_t* connect );
};

template<typename T>
class TListener:public T,public CListener{
	TListener( const TListener& );
	TListener& operator=( const TListener& );
public:
	TListener( com::ios_t& ios ):
		T( ios ),com::CListener(),CListener(){
	}
};

}
}
}



#endif /* INCLUDE_DM_OS_ASCOM_LISTENER_HPP_ */

﻿/*
 * udpbindclientdevice.hpp
 *
 *  Created on: 2017年2月23日
 *      Author: work
 */

#ifndef INCLUDE_DM_OS_ASCOM_UDPBINDCLIENTDEVICE_HPP_
#define INCLUDE_DM_OS_ASCOM_UDPBINDCLIENTDEVICE_HPP_

#include <dm/os/ascom/device.hpp>
#include <dm/os/com/udpbindclientdevice.hpp>

namespace dm{
namespace os{
namespace ascom{

typedef TDevice<com::CUdpBindClientDevice> CUdpBindClientDevice;

}
}
}



#endif /* INCLUDE_DM_OS_ASCOM_UDPBINDCLIENTDEVICE_HPP_ */

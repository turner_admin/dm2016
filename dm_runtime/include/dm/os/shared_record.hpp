/*
 * shared_record.hpp
 *
 *  Created on: 2023年5月17日
 *      Author: Dylan.Gao
 */

#ifndef _DM_OS_SHARED_RECORD_HPP_
#define _DM_OS_SHARED_RECORD_HPP_

#include <os/types.hpp>

namespace dm{
namespace os{

/**
 * 可共享记录
 * 主要用于共享内存中
 * 访问是需要加载共享锁，升级锁，或者互斥锁
 * @tparam
 */
template<struct S>
struct TSSharedRecord{
	upgradable_mutex_t mutex_t;

	mutex_t mutex;	//!< 访问互斥量
	S d;			//!< 数据
};

}
}

#endif /* _DM_OS_SHARED_RECORD_HPP_ */

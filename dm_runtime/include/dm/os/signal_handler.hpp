/*
 * signal_handler.hpp
 *
 *  Created on: 2020年11月17日
 *      Author: Dylan.Gao
 */

#ifndef _DM_OS_SIGNAL_HANDLER_HPP_
#define _DM_OS_SIGNAL_HANDLER_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_COMMON
#define DM_API_OS_COMMON DM_API_IMPORT
#endif

namespace dm{
namespace os{

/**
 * 信号捕捉器类
 */
class DM_API_OS_COMMON CSignalHandler{
	CSignalHandler();
public:
	static CSignalHandler& ins();

	/**
	 * 当前是否有信号
	 * @return
	 */
	bool ifSignal()const;

	/**
	 * 信号名称
	 */
	int signal()const;

	/**
	 * 对于windows控制台的控制信号名称
	 */
	int ctlType()const;

	/**
	 * 清除信号
	 */
	void clearSignals();
};

}
}
#endif /* INCLUDE_DM_OS_SIGNAL_HANDLER_HPP_ */

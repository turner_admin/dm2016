﻿/*
 * loginfo.hpp
 *
 *  Created on: 2016年12月6日
 *      Author: work
 */

#ifndef _DM_OS_LOG_LOGINFO_HPP_
#define _DM_OS_LOG_LOGINFO_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_LOG
#define DM_API_OS_LOG DM_API_IMPORT
#else
#define DM_API_FIXDSTRING DM_API_OS_LOG
#endif

#include <dm/types.hpp>
#include <dm/fixdstring.hpp>
#include <dm/timestamp.hpp>

#ifdef WIN32
#include <Windows.h>
#else
#include <unistd.h>
#endif

namespace dm{
namespace os{

#ifndef DM_OS_LOG_LEN_OF_MSG
#define DM_OS_LOG_LEN_OF_MSG 1024
#endif

#ifndef DM_OS_LOG_LEN_OF_MODULE
#define DM_OS_LOG_LEN_OF_MODULE 128
#endif

#ifndef DM_OS_LOG_LEN_OF_FUN
#define DM_OS_LOG_LEN_OF_FUN 128
#endif

class DM_API_OS_LOG CLogInfo{
public:
	enum{
		LenOfMsg = DM_OS_LOG_LEN_OF_MSG,
		LenOfModule = DM_OS_LOG_LEN_OF_MODULE,
		LenOfFun = DM_OS_LOG_LEN_OF_FUN
	};

	enum ELevel{
		Debug = 0,
		Info,
		Warnning,
		Error
	};

	typedef dm::CFixdString<LenOfMsg> msg_t;
	typedef dm::CFixdString<LenOfModule> module_t;
	typedef dm::CFixdString<LenOfFun> fun_t;
	typedef const void * obj_t;

#ifdef WIN32
	typedef DWORD pid_t;
#else
	typedef ::pid_t pid_t;
#endif

public:
	CLogInfo();
	CLogInfo( const CLogInfo& );
	CLogInfo& operator=( const CLogInfo& loginfo );

	inline long getPid()const{
		return pid;
	}

	inline void setPid( long p ){
		pid = p;
	}

	inline const dm::CTimeStamp& getTs()const{
		return t;
	}

	inline void setTs( const dm::CTimeStamp& ts ){
		t = ts;
	}

	inline const ELevel& getLevel()const{
		return level;
	}

	inline void setLevel( const ELevel& l ){
		level = l;
	}

	inline const char* getModule()const{
		return module.c_str();
	}

	inline void setModule( const char* str ){
		module = str;
	}

	inline const char* getFun()const{
		return fun.c_str();
	}

	inline void setFun( const char* str ){
		fun = str;
	}

	inline const obj_t& getObj()const{
		return obj;
	}

	inline void setObj( const obj_t& o ){
		obj = o;
	}

	inline const char* getMsg()const{
		return msg.c_str();
	}

	inline void setMsg( const char* str ){
		msg = str;
	}

public:
	pid_t pid;
	dm::CTimeStamp t;
	ELevel level;
	module_t module;
	fun_t fun;
	obj_t obj;
	msg_t msg;
};

}
}

#endif /* INCLUDE_DM_OS_LOG_LOGINFO_HPP_ */

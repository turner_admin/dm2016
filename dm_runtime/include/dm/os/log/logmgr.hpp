﻿/*
 * logmgr.hpp
 *
 *  Created on: 2016年12月6日
 *      Author: Dylan.Gao
 */

#ifndef _DM_OS_LOG_LOGMGR_HPP_
#define _DM_OS_LOG_LOGMGR_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_LOG
#define DM_API_OS_LOG DM_API_IMPORT
#endif

#include <dm/os/syswaitqueue.hpp>
#include <dm/os/log/loginfo.hpp>

#ifndef DM_OS_LOG_SIZE_OF_QUEUE
#define DM_OS_LOG_SIZE_OF_QUEUE 1024
#endif

namespace dm{
namespace os{

class DM_API_OS_LOG CLogMgr{
	CLogMgr( const CLogMgr& );
	CLogMgr& operator=( const CLogMgr& );

public:
	enum {
		Size = DM_OS_LOG_SIZE_OF_QUEUE
	};

	typedef dm::os::CSysWaitQueue<CLogInfo,Size> queue_t;

public:
	CLogMgr();

	static void remove();

protected:
	queue_t* m_q;
};

}
}



#endif /* INCLUDE_DM_OS_LOG_LOGMGR_HPP_ */

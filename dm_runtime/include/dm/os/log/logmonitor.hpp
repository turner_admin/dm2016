﻿/*
 * logmonitor.hpp
 *
 *  Created on: 2016年12月6日
 *      Author: Dylan.Gao
 */

#ifndef _DM_OS_LOG_LOGMONITOR_HPP_
#define _DM_OS_LOG_LOGMONITOR_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_LOG
#define DM_API_OS_LOG DM_API_IMPORT
#endif

#include <dm/os/log/logmgr.hpp>

namespace dm{
namespace os{

class DM_API_OS_LOG CLogMonitor:public CLogMgr{
public:
	CLogMonitor();
	virtual ~CLogMonitor(){}

	typedef CLogMgr::queue_t::ringpos_t ringpos_t;

	virtual void onNew( const ringpos_t& pos,const CLogInfo& info,const bool& overflow )const;
	virtual bool filter( const ringpos_t& pos,const CLogInfo& info,const bool& overflow )const;

	void run();

	bool tryGet( CLogInfo& info,bool& overflow );
	void get( CLogInfo& info,bool& overFlag );

	inline const ringpos_t& pos()const{
		return m_p;
	}

	void reset();
private:
	ringpos_t m_p;
};

}
}


#endif /* INCLUDE_DM_OS_LOG_LOGMONITOR_HPP_ */

﻿/*
 * logger.hpp
 *
 *  Created on: 2016年12月6日
 *      Author: Dylan.Gao
 */

#ifndef _DM_OS_LOG_LOGGER_HPP_
#define _DM_OS_LOG_LOGGER_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_LOG
#define DM_API_OS_LOG DM_API_IMPORT
#endif

#include <dm/os/log/logmgr.hpp>

namespace dm{
namespace os{

class DM_API_OS_LOG CLogger:public CLogMgr{
	CLogger();
	CLogger( const CLogger& l);
	CLogger& operator=( const CLogger& l );
public:
	typedef CLogInfo info_t;
	typedef info_t::obj_t obj_t;
	typedef info_t::ELevel level_t;

#ifdef WIN32
	typedef CLogInfo::pid_t pid_t;
#endif

	static CLogger& ins();

	void resetPid();

	void logmsg( const dm::CTimeStamp& ts,const char* module,const obj_t& obj,const level_t& level,const char* fun,const char* msg );

	inline void logmsg( const char* module,const obj_t& obj,const level_t& level,const char* fun,const char* msg ){
		logmsg(dm::CTimeStamp::cur(),module,obj,level,fun,msg);
	}

	inline void logmsg( const char* module,const long& obj,const level_t& level,const char* fun,const char* msg ){
		int* p = (int*)obj;
		logmsg(module,p,level,fun,msg);
	}
	
	void log( const dm::CTimeStamp& ts,const char* module,const obj_t& obj,const level_t& level,const char* fun,const char* fmt,... );
	void log( const char* module,const obj_t& obj,const level_t& level,const char* fun,const char* fmt,... );

	void error( const dm::CTimeStamp& ts,const char* module,const obj_t& obj,const char* fun,const char* fmt,...);
	void error( const char* module,const obj_t& obj,const char* fun,const char* fmt,...);

	void warnning( const dm::CTimeStamp& ts,const char* module,const obj_t& obj,const char* fun,const char* fmt,...);
	void warnning( const char* module,const obj_t& obj,const char* fun,const char* fmt,...);

	void info( const dm::CTimeStamp& ts,const char* module,const obj_t& obj,const char* fun,const char* fmt,...);
	void info( const char* module,const obj_t& obj,const char* fun,const char* fmt,...);

	void debug( const dm::CTimeStamp& ts,const char* module,const obj_t& obj,const char* fun,const char* fmt,...);
	void debug( const char* module,const obj_t& obj,const char* fun,const char* fmt,...);
private:
	pid_t m_pid;
};

}
}

static inline dm::os::CLogger& log(){
	return dm::os::CLogger::ins();
}

#define THISMODULE  (logModule),(this),(__FUNCTION__),
#define STATICMODULE (logModule),0,(__FUNCTION__),

#endif /* INCLUDE_DM_OS_LOG_LOGGER_HPP_ */

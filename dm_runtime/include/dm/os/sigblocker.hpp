/*
 * sigblocker.hpp
 *
 *  Created on: 2019年10月23日
 *      Author: turne
 */

#ifndef _DM_OS_SIGBLOCKER_HPP_
#define _DM_OS_SIGBLOCKER_HPP_

#include <signal.h>

namespace dm{
namespace os{

class CSigBlocker{
	CSigBlocker(const CSigBlocker& );
	CSigBlocker& operator=(const CSigBlocker& );
public:
	CSigBlocker();
	~CSigBlocker();

private:
	sigset_t m_set;
};

}
}

#endif /* INCLUDE_DM_OS_SIGBLOCKER_HPP_ */

#ifndef _DM_OS_DB_DB_PG_HPP
#define _DM_OS_DB_DB_PG_HPP

#include <dm/export.hpp>

#ifndef DM_API_OS_DB
#define DM_API_OS_DB DM_API_IMPORT
#endif

#include <dm/os/db/db.hpp>
#include <dm/os/db/resultset_pg.hpp>

#include <postgresql/libpq-fe.h>

namespace dm{
namespace os{

class DM_API_OS_DB CDbPg:public CDb{
public:
	enum EExtention{
		ExNone,	// 未扩展
		ExHgdb,	// 瀚高扩展
	};

	CDbPg( const char* dbName,const char* host="localhost",short port=-1,const EExtention& extention=ExNone );
	~CDbPg();

	EDbType dbType();

    bool isConnected();

	bool connect( const char* user,const char* pw );
	void disconnect();

	bool exec( const char* sql );
	dm::int64 execWithLastId( const char* sql );

	CResultSet* query( const char* sql);

	CResultSet* getTables( const char* pattern );
private:
	PGconn* m_con;
	EExtention m_extention;
};

}
}

#endif

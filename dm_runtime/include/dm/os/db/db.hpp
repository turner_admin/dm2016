﻿/*
 * db.hpp
 *
 *  Created on: 2016年12月6日
 *      Author: work
 */

#ifndef _DM_OS_DB_DB_HPP_
#define _DM_OS_DB_DB_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_DB
#define DM_API_OS_DB DM_API_IMPORT
#endif

#include <dm/os/db/resultset.hpp>
#include <string>

namespace dm{
namespace os{

class DM_API_OS_DB CDb{
public:
    enum EDbType{
        DtSqlite,	//!< SQLite数据库
        DtMysql,	//!< MySql数据库
        DtPg,		//!< PostgreSQL数据库
        DtHgdb		//!< 国产瀚高数据库
    };

    CDb( const char* dbName,const char* host="localhost",short port=-1 );
    virtual ~CDb();

    virtual EDbType dbType()=0;

    virtual bool isConnected()=0;

    virtual bool connect( const char* user,const char* pw )=0;
    virtual void disconnect()=0;

    virtual bool exec( const char* sql )=0;

    /**
     * 执行插入或更新语句，并返回id
     * 本命令专用于
     * @param sql
     * @return
     */
    virtual dm::int64 execWithLastId( const char* sql )=0;

    virtual CResultSet* query( const char* sql )=0;

    virtual CResultSet* getTables( const char* pattern )=0;
protected:
    std::string m_host;
    short m_port;
    std::string m_dbName;
};

/**
 * 数据库工厂
 * 返回一个数据库类实例。失败返回空
 * engine:指定数据库引擎
 *  - sqlite3
 *  - mysql
 *  - postgresql
 *  - hgdb
*/
DM_API_OS_DB CDb* createDb( const char* dbName,const char* host="localhost",const char* engine=NULL,short port=-1 );
}

}



#endif /* INCLUDE_DM_OS_DB_DB_HPP_ */

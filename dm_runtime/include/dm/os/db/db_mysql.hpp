/*
 * db_mysql.hpp
 *
 *  Created on: 2022年11月7日
 *      Author: dylan
 */

#ifndef _DM_OS_DB_DB_MYSQL_HPP_
#define _DM_OS_DB_DB_MYSQL_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_DB
#define DM_API_OS_DB DM_API_IMPORT
#endif

#include <dm/os/db/db.hpp>
#include <dm/os/db/resultset_mysql.hpp>

#include <cppconn/driver.h>
#include <cppconn/connection.h>

namespace dm{
namespace os{

class DM_API_OS_DB CDbMysql:public CDb{
public:
	CDbMysql( const char* dbName,const char* host="localhost",short port=-1);
	~CDbMysql();

	EDbType dbType();

    bool isConnected();

	bool connect( const char* user,const char* pw );
	void disconnect();

	bool exec( const char* sql );

	dm::int64 execWithLastId( const char* sql );

	CResultSet* query( const char* sql);

	CResultSet* getTables( const char* pattern );
private:
	sql::Driver* m_driver;
	sql::Connection* m_con;
};

}
}

#endif /* _DM_OS_DB_DB_MYSQL_HPP_ */

#ifndef _DM_OS_DB_RESULTSET_PG_HPP
#define _DM_OS_DB_RESULTSET_PG_HPP

#include <dm/export.hpp>

#ifndef DM_API_OS_DB
#define DM_API_OS_DB DM_API_IMPORT
#endif

#include <dm/os/db/resultset.hpp>
#include <postgresql/libpq-fe.h>

namespace dm{
namespace os{

/**
 * PG数据库结果集
 **/
class DM_API_OS_DB CResultSetPg:public CResultSet{
	CResultSetPg(const CResultSetPg& );
	CResultSetPg& operator=( const CResultSetPg& );

public:
	CResultSetPg();
	~CResultSetPg();

	int size()const;

	bool next();
	void reset();
	int currentRow()const;

	bool asBool( int col )const;
	bool asBool( const char* col )const;
	int asInt( int col )const;
	int asInt( const char* col )const;
	dm::uint asUint( int col )const;
	dm::uint asUint( const char* col )const;
	double asDouble( int col )const;
	double asDouble( const char* col )const;
	std::string asString( int col )const;
	std::string asString( const char* col )const;
	bool isNull( int col )const;
	bool isNull( const char* col )const;

	dm::int64 asInt64( int col )const;
	dm::int64 asInt64( const char* col )const;

	int columnSize()const;
	std::string valueString( int col )const;
	std::string valueString( const char* col )const;

	void free();

private:
	PGresult* m_rst;
	int m_r;	// 当前行号

	friend class CDbPg;
};

}
}

#endif

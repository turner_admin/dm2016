﻿/*
 * resultset_sqlite.hpp
 *
 *  Created on: 2022年11月7日
 *      Author: dylan
 */

#ifndef _DM_OS_DB_RESULTSET_SQLITE_HPP_
#define _DM_OS_DB_RESULTSET_SQLITE_HPP_

#include <dm/export.hpp>
#include <dm/os/db/resultset.hpp>
#include <sqlite3.h>

#ifndef DM_API_OS_DB
#define DM_API_OS_DB DM_API_IMPORT
#endif

namespace dm{
namespace os{

class CDbSqlite;

/**
 * SQLITE3结果集
 */
class DM_API_OS_DB CResultSetSqlite:public CResultSet{
	CResultSetSqlite(const CResultSetSqlite&);
	CResultSetSqlite& operator=( const CResultSetSqlite& );

public:
	CResultSetSqlite();
	~CResultSetSqlite();

	int size()const;

	bool next();
	void reset();
	int currentRow()const;

	bool asBool( int col )const;
	bool asBool( const char* col )const;
	int asInt( int col )const;
	int asInt( const char* col )const;
	dm::uint asUint( int col )const;
	dm::uint asUint( const char* col )const;
	double asDouble( int col )const;
	double asDouble( const char* col )const;
	std::string asString( int col )const;
	std::string asString( const char* col )const;
	bool isNull( int col )const;
	bool isNull( const char* col )const;

	dm::int64 asInt64( int col )const;
	dm::int64 asInt64( const char* col )const;

	int columnSize()const;
	std::string valueString( int col )const;
	std::string valueString( const char* col )const;

	void free();

private:
	sqlite3_stmt* m_stmt;
	int m_size;
	int m_r;

	friend CDbSqlite;
};

}
}



#endif /* _DM_OS_DB_RESULTSET_SQLITE_HPP_ */

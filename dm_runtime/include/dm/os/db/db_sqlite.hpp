﻿/*
 * db_sqlite.hpp
 *
 *  Created on: 2022年11月7日
 *      Author: dylan
 */

#ifndef _DM_OS_DB_DB_SQLITE_HPP_
#define _DM_OS_DB_DB_SQLITE_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_DB
#define DM_API_OS_DB DM_API_IMPORT
#endif

#include <dm/os/db/db.hpp>
#include <dm/os/db/resultset_sqlite.hpp>

namespace dm{
namespace os{

class DM_API_OS_DB CDbSqlite:public CDb{
public:
	CDbSqlite( const char* dbName,const char* host="localhost",short port=-1);
	~CDbSqlite();

	EDbType dbType();

    bool isConnected();

	bool connect( const char* user,const char* pw );
	void disconnect();

	bool exec( const char* sql );
	dm::int64 execWithLastId( const char* sql );

	CResultSet* query( const char* sql );

	CResultSet* getTables( const char* pattern );

private:
	sqlite3 * m_db;
};

}
}
#endif /* DM_RUNTIME_INCLUDE_DM_OS_DB_DB_SQLITE_HPP_ */

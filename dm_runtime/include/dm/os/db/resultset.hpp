﻿/*
 * resultset.hpp
 *
 *  Created on: 2016年12月6日
 *      Author: work
 */

#ifndef _DM_OS_DB_RESULTSET_HPP_
#define _DM_OS_DB_RESULTSET_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_DB
#define DM_API_OS_DB DM_API_IMPORT
#endif

#include <dm/types.hpp>
#include <string>

namespace dm{
namespace os{

class DM_API_OS_DB CResultSet{
	CResultSet( const CResultSet& );
	CResultSet& operator=( const CResultSet& );
public:
	CResultSet();
	virtual ~CResultSet();

	virtual int size()const=0;

	virtual bool next()=0;
	virtual void reset()=0;
	virtual int currentRow()const=0;

	virtual bool asBool( int col )const=0;
	virtual bool asBool( const char* col )const=0;
	virtual int asInt( int col )const=0;
	virtual int asInt( const char* col )const=0;
	virtual dm::uint asUint( int col )const=0;
	virtual dm::uint asUint( const char* col )const=0;
	virtual double asDouble( int col )const=0;
	virtual double asDouble( const char* col )const=0;
	virtual std::string asString( int col )const=0;
	virtual std::string asString( const char* col )const=0;
	virtual bool isNull( int col )const=0;
	virtual bool isNull( const char* col )const=0;

	virtual dm::int64 asInt64( int col )const=0;
	virtual dm::int64 asInt64( const char* col )const=0;

	virtual int columnSize()const=0;

	/**
	 * 将列值转换成字符串
	 **/
	virtual std::string valueString( int col )const=0;

	/**
	 * 将列值转换成字符串
	 **/
	virtual std::string valueString( const char* col )const=0;

	virtual void free()=0;

	inline bool asBool( int col,const bool& defaultValue ){
		if( isNull(col) )
			return defaultValue;
		return asBool(col);
	}

	inline bool asBool( const char* col,const bool& defaultValue ){
		if( isNull(col) )
			return defaultValue;
		return asBool(col);
	}

	inline int asInt( int col,const int& defaultValue ){
		if( isNull(col) )
			return defaultValue;
		return asInt(col);
	}

	inline int asInt( const char* col,const int& defaultValue ){
		if( isNull(col) )
			return defaultValue;
		return asInt(col);
	}

	inline dm::uint asUint( int col,const dm::uint& defaultValue ){
		if( isNull(col) )
			return defaultValue;
		return asUint(col);
	}

	inline dm::uint asUint( const char* col,const dm::uint& defaultValue ){
		if( isNull(col) )
			return defaultValue;
		return asUint(col);
	}

	inline double asDouble( int col,const double& defaultValue ){
		if( isNull(col) )
			return defaultValue;
		return asDouble(col);
	}

	inline double asDouble( const char* col,const double& defaultValue ){
		if( isNull(col) )
			return defaultValue;
		return asDouble(col);
	}

	inline std::string asString( int col,const std::string& defaultValue ){
		if( isNull(col) )
			return defaultValue;
		return asString(col);
	}

	inline std::string asString( const char* col,const std::string& defaultValue ){
		if( isNull(col) )
			return defaultValue;
		return asString(col);
	}

	inline dm::int64 asInt64( int col,const dm::int64& defaultValue ){
		if( isNull(col) )
			return defaultValue;
		return asInt64(col);
	}

	inline dm::int64 asInt64( const char* col,const dm::int64& defaultValue ){
		if( isNull(col) )
			return defaultValue;
		return asInt64(col);
	}
};

}
}



#endif /* INCLUDE_DM_OS_DB_RESULTSET_HPP_ */

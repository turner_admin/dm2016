/*
 * tcplistenor.hpp
 *
 *  Created on: 2017年6月15日
 *      Author: work
 */

#ifndef _DM_OS_IO_TCPLISTENOR_HPP_
#define _DM_OS_IO_TCPLISTENOR_HPP_

#include <dm/os/io/io.hpp>
#include <dm/os/io/tcpconnect.hpp>
#include <dm/os/com/tcpserveraddr.hpp>

namespace dm{
namespace os{
namespace io{

class CTcpListener:public CIo{
public:
	bool accept( CTcpConnect& connect );

protected:
	bool open_in( const com::CAddress& address );
	bool open_in( const com::CTcpServerAddr* address );
};

}
}
}



#endif /* INCLUDE_DM_OS_IO_TCPLISTENOR_HPP_ */

/*
 * serial.hpp
 *
 *  Created on: 2017年6月15日
 *      Author: work
 */

#ifndef _DM_OS_IO_SERIAL_HPP_
#define _DM_OS_IO_SERIAL_HPP_

#include <dm/os/io/io.hpp>
#include <dm/os/com/serialaddr.hpp>

namespace dm{
namespace os{
namespace io{

/**
 * 串口设备类
 */
class CSerial:public CIo{
public:
	type_t type()const;

protected:
	bool open_in(const com::CAddress& address );
};

}
}
}


#endif /* INCLUDE_DM_OS_IO_SERIAL_HPP_ */

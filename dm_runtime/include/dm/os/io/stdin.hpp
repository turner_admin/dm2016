/*
 * stdin.hpp
 *
 *  Created on: 2017年7月15日
 *      Author: work
 */

#ifndef _DM_OS_IO_STDIN_HPP_
#define _DM_OS_IO_STDIN_HPP_

#include <dm/os/io/io.hpp>

namespace dm{
namespace os{
namespace io{

class CStdIn:public CIo{
public:
	CStdIn();
	type_t type()const;
};

}
}
}



#endif /* INCLUDE_DM_OS_IO_STDIN_HPP_ */

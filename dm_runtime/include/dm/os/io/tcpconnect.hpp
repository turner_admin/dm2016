/*
 * tcpconnect.hpp
 *
 *  Created on: 2017年6月15日
 *      Author: work
 */

#ifndef _DM_OS_IO_TCPCONNECT_HPP_
#define _DM_OS_IO_TCPCONNECT_HPP_

#include <dm/os/io/io.hpp>

namespace dm{
namespace os{
namespace io{

class CTcpConnect:public CIo{
public:
	type_t type()const;
protected:
	friend class CTcpListener;
};

}
}
}



#endif /* INCLUDE_DM_OS_IO_TCPCONNECT_HPP_ */

/*
 * cannet.hpp
 *
 *  Created on: 2017年6月15日
 *      Author: work
 */

#ifndef _DM_OS_IO_CANNET_HPP_
#define _DM_OS_IO_CANNET_HPP_

#include <dm/os/io/io.hpp>
#include <dm/os/com/canaddr.hpp>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/uio.h>
#include <linux/can.h>
#include <linux/can/raw.h>
#include <linux/socket.h>
#include <net/if.h>
#include <unistd.h>
#include <fcntl.h>

namespace dm{
namespace os{
namespace io{

class CCannet:public CIo{
public:
	type_t type()const;

	inline bool send( const struct can_frame& f ){
		return write<struct can_frame>(f);
	}

	inline bool recv( struct can_frame& f ){
		return read<struct can_frame>(f);
	}

protected:
	bool open_in( const com::CAddress& address );
};

}
}
}

#endif /* INCLUDE_DM_OS_IO_CANNET_HPP_ */

/*
 * tcpclient.hpp
 *
 *  Created on: 2017年6月15日
 *      Author: work
 */

#ifndef _DM_OS_IO_TCPCLIENT_HPP_
#define _DM_OS_IO_TCPCLIENT_HPP_

#include <dm/os/io/io.hpp>
#include <dm/os/com/tcpclientaddr.hpp>

namespace dm{
namespace os{
namespace io{

class CTcpClient:public CIo{
public:
	CTcpClient();

	type_t type()const;

protected:
	bool open_in(const com::CAddress& address );
};

}
}
}

#endif /* INCLUDE_DM_OS_IO_TCPCLIENT_HPP_ */

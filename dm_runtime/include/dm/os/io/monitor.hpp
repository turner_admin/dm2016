/*
 * monitor.hpp
 *
 *  Created on: 2017年6月15日
 *      Author: work
 */

#ifndef _DM_OS_IO_MONITOR_HPP_
#define _DM_OS_IO_MONITOR_HPP_

#include <sys/poll.h>
#include <dm/os/io/io.hpp>

namespace dm{
namespace os{
namespace io{

/**
 * IO监视器
 * 负责监视IO的可读等状态。
 */
class CMonitor{
	CMonitor( const CMonitor& );
	CMonitor& operator=( const CMonitor& );
public:
	CMonitor( const unsigned int& size=10 );
	~CMonitor();

	void setSize( const unsigned int& size );
	inline const unsigned int& getSize()const{
		return m_size;
	}

	void reset();

	inline const unsigned int& getCount()const{
		return m_count;
	}

	/**
	 * 等待设备可读，或者超时
	 * @param timeout_ms 超时时间：毫秒
	 * <0 一直等待，直到事件发生
	 * =0 立即返回。相当于测试事件
	 * >0 等待时长
	 * @return
	 * true 事件发生
	 * false 超时
	 */
	bool wait( const long timeout_ms=-1 );

	/**
	 * 增加IO
	 * 如果成功，或者系统内存在
	 * @param io
	 * @return 返回索引号
	 */
	int addIo( CIo& io );
	int addIo( int fd );

	bool isReady( const unsigned int& index )const;

	bool isIoReady( const int& fd )const;

	inline bool isIoReady( const CIo& io )const{
		return isIoReady(io.m_fd);
	}

	int getIndex( const int& fd )const;

private:
	unsigned int m_size;
	unsigned int m_count;
	struct pollfd* m_fds;
};

}
}
}

#endif /* INCLUDE_DM_OS_IO_MONITOR_HPP_ */

/*
 * tcplistener.hpp
 *
 *  Created on: 2017年6月15日
 *      Author: work
 */

#ifndef _DM_OS_IO_TCPLISTENER_HPP_
#define _DM_OS_IO_TCPLISTENER_HPP_

#include <dm/os/io/io.hpp>
#include <dm/os/io/tcpconnect.hpp>

namespace dm{
namespace os{
namespace io{

class CTcpListener:public CIo{
public:
	bool accept( CTcpConnect& connect );

	type_t type()const;

protected:
	bool open_in( const com::CAddress& address );
};

}
}
}



#endif /* INCLUDE_DM_OS_IO_TCPLISTENOR_HPP_ */

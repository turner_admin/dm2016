﻿/*
 * types.hpp
 *
 *  Created on: 2017年3月17日
 *      Author: work
 */

#ifndef _DM_OS_MSG_TYPES_HPP_
#define _DM_OS_MSG_TYPES_HPP_

#include <dm/types.hpp>
#include <dm/msg/types.hpp>

namespace dm{
namespace os{
namespace msg{

typedef dm::uint64 mod_t;

}
}
}

#endif /* INCLUDE_DM_OS_MSG_TYPES_HPP_ */

﻿/*
 * msginfo.hpp
 *
 *  Created on: 2017年3月17日
 *      Author: work
 */

#ifndef _DM_OS_MSG_MSGINFO_HPP_
#define _DM_OS_MSG_MSGINFO_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_MSG
#define DM_API_OS_MSG DM_API_IMPORT
#endif

#include <dm/os/msg/types.hpp>

namespace dm{
namespace os{
namespace msg{

/**
 * 消息类型类
 */
class DM_API_OS_MSG CMsgInfo{
public:
	CMsgInfo(
			const mod_t& mod=0,
			const dm::msg::EMsgType& type=dm::msg::Msg_Test,
			const dm::msg::EMsgAck& ack=dm::msg::MaNoAck
	);

	CMsgInfo( const CMsgInfo& msg );
	CMsgInfo& operator=( const CMsgInfo& msg );

	inline const mod_t& getMod()const{
		return m_mod;
	}

	inline void setMod( const mod_t& mod ){
		m_mod = mod;
	}

	inline const dm::msg::EMsgType& getType()const{
		return m_type;
	}

	inline void setType( const dm::msg::EMsgType& t ){
		m_type = t;
	}

	inline const dm::msg::EMsgAck& getAck()const{
		return m_ack;
	}

	inline void setAck( const dm::msg::EMsgAck& ack ){
		m_ack = ack;
	}

	inline const dm::CTimeStamp& getTs()const{
		return m_ts;
	}

	inline void setTs( const dm::CTimeStamp& ts ){
		m_ts = ts;
	}

	inline void setTs_now(){
		m_ts = m_ts.cur();
	}

	inline const dm::msg::SCallInfo& getCall()const{
		return m_data.call;
	}

	inline dm::msg::SCallInfo& getCall(){
		return m_data.call;
	}

	inline const dm::msg::SRemoteCtlInfo& getRemoteCtl()const{
		return m_data.remoteCtl;
	}

	inline dm::msg::SRemoteCtlInfo& getRemoteCtl(){
		return m_data.remoteCtl;
	}

	inline const dm::msg::SParaInfo& getPara()const{
		return m_data.para;
	}

	inline dm::msg::SParaInfo& getPara(){
		return m_data.para;
	}

	inline const dm::msg::SParaData& getParaData()const{
		return m_data.paraData;
	}

	inline dm::msg::SParaData& getParaData(){
		return m_data.paraData;
	}

	inline const dm::msg::SSyncInfo& getSync()const{
		return m_data.sync;
	}

	inline dm::msg::SSyncInfo& getSync(){
		return m_data.sync;
	}

	void setRequest_test( const bool& needAck=false );
	void setAnswer_test( const dm::msg::EMsgAck& ack=dm::msg::MaSuccess );

	void setRequest_call( const dm::msg::SCallInfo::group_t& group,const bool& needAck=false );
	void setAnswer_call( const dm::msg::SCallInfo::group_t& group,const dm::msg::EMsgAck& ack=dm::msg::MaSuccess );

	void setRequest_syncClock( const bool& needAck=false );
	void setAnswer_syncClock( const dm::msg::EMsgAck& ack=dm::msg::MaSuccess );

	void setRequest_remoteCtl( const dm::msg::index_t& index,const dm::msg::ERemoteControl& ctl,const bool& needAck=false,const dm::msg::index_t& device=-1 );
	void setAnswer_remoteCtl( const dm::msg::SRemoteCtlInfo& ctl,const dm::msg::EMsgAck& ack=dm::msg::MaSuccess );
	void setAnswer_remoteCtl( const dm::msg::index_t& index,const dm::msg::ERemoteControl& ctl,const dm::msg::EMsgAck& ack=dm::msg::MaSuccess,const dm::msg::index_t& device=-1 );

	void setRequest_set( const dm::msg::index_t& index,const dm::msg::index_t& num,const bool& needAck=false,const dm::msg::index_t& device=-1 );
	void setAnswer_set( const dm::msg::SParaData& paraData,const dm::msg::EMsgAck& ack=dm::msg::MaSuccess );
	void setAnswer_set( const dm::msg::index_t& index,const dm::msg::index_t& num,const dm::msg::EMsgAck& ack=dm::msg::MaSuccess,const dm::msg::index_t& device=-1 );

	void setRequest_get( const dm::msg::index_t& index,const dm::msg::index_t& num,const bool& needAck=false,const dm::msg::index_t& device=-1 );
	void setAnswer_get( const dm::msg::SParaInfo& paraInfo,const dm::msg::EMsgAck& ack=dm::msg::MaSuccess );
	void setAnswer_get( const dm::msg::index_t& index,const dm::msg::index_t& num,const dm::msg::EMsgAck& ack=dm::msg::MaSuccess,const dm::msg::index_t& device=-1 );
	void setAnswer_get( const dm::msg::SParaData& pd,const dm::msg::EMsgAck& ack=dm::msg::MaSuccess );

	void setRequest_update( const dm::msg::index_t& idx,const dm::msg::index_t& num,const bool& needAck=false,const dm::msg::index_t& device=-1 );
	void setAnswer_update( const dm::msg::SParaData& paraData,const dm::msg::EMsgAck& ack=dm::msg::MaSuccess );
	void setAnswer_update( const dm::msg::index_t& idx,const dm::msg::index_t& num,const dm::msg::EMsgAck& ack=dm::msg::MaSuccess,const dm::msg::index_t& device=-1 );

	void setRequest_sync( const dm::msg::ESyncDataType& type,const bool& needAck=false );
	void setAnswer_sync( const dm::msg::SSyncInfo& sync,const dm::msg::EMsgAck& ack=dm::msg::MaSuccess );
	void setAnswer_sync( const dm::msg::ESyncDataType& type,const dm::msg::EMsgAck& ack=dm::msg::MaSuccess );

	void setRequest_remoteReset( const bool& needAck=false );
	void setAnswer_remoteReset( const dm::msg::EMsgAck& ack=dm::msg::MaSuccess );

	inline bool isRequest()const{
		return m_ack==dm::msg::MaAck||m_ack==dm::msg::MaNoAck;
	}

	inline bool isAnswer()const{
		return !isRequest();
	}

private:
	mod_t m_mod;
	dm::msg::EMsgType m_type;
	dm::msg::EMsgAck  m_ack;
	dm::CTimeStamp m_ts;
	union{
		dm::msg::SCallInfo call;
		dm::msg::SRemoteCtlInfo remoteCtl;
		dm::msg::SParaInfo para;
		dm::msg::SParaData paraData;
		dm::msg::SSyncInfo sync;
	}m_data;
};

}
}
}



#endif /* INCLUDE_DM_OS_MSG_MSGINFO_HPP_ */

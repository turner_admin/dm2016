﻿/*
 * msgdriver.hpp
 *
 *  Created on: Aug 19, 2017
 *      Author: vte
 */

#ifndef _DM_OS_MSG_MSGDRIVER_HPP_
#define _DM_OS_MSG_MSGDRIVER_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_MSG
#define DM_API_OS_MSG DM_API_IMPORT
#endif

#include <dm/os/msg/msginfo.hpp>
#include <dm/msg/types.hpp>

namespace dm{
namespace os{
namespace msg{

class DM_API_OS_MSG CMsgDriver{
	CMsgDriver();
	CMsgDriver( const CMsgDriver& );
	CMsgDriver& operator=( const CMsgDriver& );
public:
	static CMsgDriver& ins();

	void sendMsg( CMsgInfo& msg );

	bool tryGet( CMsgInfo& msg );
	bool tryGetRequeset( CMsgInfo& msg );
	bool tryGetAnswer( CMsgInfo& msg );

	void wait( CMsgInfo& msg );
	void wait4requset( CMsgInfo& msg );
	void wait4answer( CMsgInfo& msg );

	void resetRequest();
	void resetAnswer();

	// ========= 一些便利的消息接口 ============

	void requestTest( bool needAck=false );
	void answerTest( dm::msg::EMsgAck ack=dm::msg::MaSuccess );

	void requesetCall( dm::msg::SCallInfo::group_t group=0,bool needAck=false );
	void answerCall( dm::msg::SCallInfo::group_t group=0,dm::msg::EMsgAck ack=dm::msg::MaSuccess );

	void requestSyncClock( bool needAck=false );
	void answerSyncClock( dm::msg::EMsgAck ack=dm::msg::MaSuccess );

	void requestRmtCtl( dm::msg::index_t idx,dm::msg::ERemoteControl ctl,bool needAck=false );
	void requestRmtCtl_preOpen( dm::msg::index_t idx,bool needAck=false );
	void requestRmtCtl_open( dm::msg::index_t idx,bool needAck=false );
	void requestRmtCtl_close( dm::msg::index_t idx,bool needAck=false );
	void requestRmtCtl_preClose( dm::msg::index_t idx,bool needAck=false );
	void answerRmtCtl( dm::msg::index_t idx,dm::msg::ERemoteControl ctl,dm::msg::EMsgAck ack=dm::msg::MaSuccess );
	void answerRmtCtl_preOpen( dm::msg::index_t idx,dm::msg::EMsgAck ack=dm::msg::MaSuccess );
	void answerRmtCtl_open( dm::msg::index_t idx,dm::msg::EMsgAck ack=dm::msg::MaSuccess );
	void answerRmtCtl_close( dm::msg::index_t idx,dm::msg::EMsgAck ack=dm::msg::MaSuccess );
	void answerRmtCtl_preClose( dm::msg::index_t idx,dm::msg::EMsgAck ack=dm::msg::MaSuccess );

};

}
}
}



#endif /* DM_RUNTIME_INCLUDE_DM_OS_MSG_MSGDRIVER_HPP_ */

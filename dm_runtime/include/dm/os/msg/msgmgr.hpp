﻿/*
 * msgmgr.hpp
 *
 *  Created on: 2017年3月17日
 *      Author: work
 */

#ifndef _DM_OS_MSG_MSGMGR_HPP_
#define _DM_OS_MSG_MSGMGR_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_MSG
#define DM_API_OS_MSG DM_API_IMPORT
#endif

#include <dm/os/syswaitqueue.hpp>
#include <dm/os/msg/msginfo.hpp>

namespace dm{
namespace os{
namespace msg{

/**
 * 消息管理器
 * 所有的消息访问都应该通过本类来实现。
 */
class DM_API_OS_MSG CMsgMgr{
	CMsgMgr( const CMsgMgr& );
	CMsgMgr& operator=( const CMsgMgr& );

public:
	enum{
		SizeOfQueue = 1024
	};

	typedef dm::os::CSysWaitQueue<CMsgInfo,SizeOfQueue> queue_t;
	static void remove();

	CMsgMgr();

	void push( const CMsgInfo& msg );

protected:
	queue_t* m_qRequest;	// 请求队列
	queue_t* m_qAnswer;		// 应答队列
};

}
}
}

#endif /* INCLUDE_DM_OS_MSG_MSGMGR_HPP_ */

﻿/*
 * msgagent.hpp
 *
 *  Created on: Aug 17, 2017
 *      Author: vte
 */

#ifndef _DM_OS_MSG_MSGAGENT_HPP_
#define _DM_OS_MSG_MSGAGENT_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_MSG
#define DM_API_OS_MSG DM_API_IMPORT
#endif

#include <dm/os/msg/msgmgr.hpp>
#include <dm/os/msg/types.hpp>

namespace dm{
namespace os{
namespace msg{

class DM_API_OS_MSG CMsgAgent:public CMsgMgr{
	CMsgAgent( const CMsgAgent& );
	CMsgAgent& operator=( const CMsgAgent& );

public:
	CMsgAgent();

	void sendMsg( CMsgInfo& msg );

	bool tryGet( CMsgInfo& msg );
	bool tryGetRequeset( CMsgInfo& msg );
	bool tryGetAnswer( CMsgInfo& msg );
	bool tryGetAnswer( const CMsgInfo& requestMsg,CMsgInfo& msg );

	void wait( CMsgInfo& msg );
	void wait4requset( CMsgInfo& msg );
	void wait4answer( CMsgInfo& msg );
	void wait4answer( const CMsgInfo& requestMsg,CMsgInfo& msg );

	void resetRequest( dm::CTimeStamp::s_t secsBefore=0,const dm::CTimeStamp& now=dm::CTimeStamp::cur() );
	void resetAnswer( dm::CTimeStamp::s_t secsBefore=0,const dm::CTimeStamp& now=dm::CTimeStamp::cur() );

protected:
	mod_t m_mod;
	queue_t::ringpos_t m_pRequest;
	queue_t::ringpos_t m_pAnswer;
};

}
}
}



#endif /* DM_RUNTIME_INCLUDE_DM_OS_MSG_MSGAGENT_HPP_ */

﻿/*
 * scopelock.hpp
 *
 *  Created on: 2018年3月21日
 *      Author: turner
 */

#ifndef _DM_OS_SCOPELOCK_HPP_
#define _DM_OS_SCOPELOCK_HPP_

#include <dm/os/spinlock.hpp>

namespace dm{
namespace os{

class CScopeLock{
public:
	CScopeLock( CSpinlock& lock,dm::uint64 times=0x1000u ):m_lock(lock){
		m_lock.lock(times);
	}

	~CScopeLock(){
		m_lock.unlock();
	}

private:
	CSpinlock& m_lock;
};

}
}

#endif /* DM_RUNTIME_INCLUDE_DM_OS_SCOPELOCK_HPP_ */

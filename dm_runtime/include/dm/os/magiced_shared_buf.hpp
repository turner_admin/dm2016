/*
 * magiced_shared_buf.hpp
 *
 *  Created on: 2023年5月17日
 *      Author: Dylan.Gao
 */

#ifndef _DM_OS_MAGICED_SHARED_BUF_HPP_
#define _DM_OS_MAGICED_SHARED_BUF_HPP_

#include <boost/interprocess/sync/interprocess_upgradable_mutex.hpp>
#include <dm/magic.hpp>
#include <dm/types.hpp>

namespace dm{
namespace os{

/**
 * 具备魔数的共享缓冲区
 * @tparam
 */
template<typename TRecord,int SizeOfMask=4,typename mutex_t=boost::interprocess::interprocess_upgradable_mutex>
struct TSMagicedSharedBuf{
	typedef dm::CMagic::magic_t magic_t;
	typedef dm::uint32 size_t;
	
	enum{
		SizeOfRecords = SizeOfMask * 8
	};

	mutex_t mutex;	//!< 互斥量
	magic_t magic;	//!< 魔数,代表缓冲区大小有删除操作
	size_t masked;	//!< 有效记录个数
	size_t size;	//!< 缓冲区大小. >size 可以表示有下一个缓冲区
	uint8 masks[SizeOfMask];	// !< 缓冲区使用掩码，0未使用
	TRecord data[SizeOfRecords];	//!< 缓冲区数据区

	TSMagicedSharedBuf():mutex(),magic(),size(0){}
	static inline unsigned int bufSize() {
		return BufSize;
	}
};

}
}



#endif /* _DM_OS_MAGICED_SHARED_BUF_HPP_ */

/*
 * task_mgr.hpp
 *
 *  Created on: 2023年9月19日
 *      Author: Dylan.Gao
 */

#ifndef _DM_OS_SYS_TASK_MGR_HPP_
#define _DM_OS_SYS_TASK_MGR_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_SYS
#define DM_API_OS_SYS DM_API_IMPORT
#endif

#include <dm/os/shared_buffer_mgr.hpp>
#include <dm/os/shared_rt_record.hpp>
#include <dm/os/sys/task_record.hpp>
#include <dm/scoped_ptr.hpp>

namespace dm{
namespace os{

namespace sys{

/**
 * 任务管理器类
 */
class DM_API_OS_SYS CTaskMgr{
	CTaskMgr();
public:

	enum{
#ifdef TASK_MGR_SIZE_OF_MASKS
		SizeOfMasks = TASK_MGR_SIZE_OF_MASKS,
#else
		SizeOfMasks = 8,
#endif

#ifdef TASK_MGR_SIZE_OF_NAME
		SizeOfName = TASK_MGR_SIZE_OF_NAME,
#else
		SizeOfName = 64,
#endif
	};

    typedef TSSharedRtRecord<STaskInfo,STaskState,id_t> store_record_t;
    typedef dm::os::TCSharedBufferMgr<store_record_t,dm::os::id_t,dm::uint8,SizeOfMasks,SizeOfName> buf_mgr_t;
	typedef buf_mgr_t::CPos pos_t;

	static CTaskMgr& ins();

    bool loadNew();

	/**
	 * 获取记录数
	 * @return
	 */
	inline dm::uint32 count(){
        return m_bufMgr->count();
	}

	/**
	 * 获取第一条记录的位置
	 * @return
	 */
    inline bool firstPos( pos_t& pos ){
        return m_bufMgr->first(pos);
	}

    inline pos_t firstPos(){
        pos_t pos;
        m_bufMgr->first(pos);
        return pos;
    }

	/**
	 * 获取下一条记录的位置
	 * @param pos
	 * @return
	 */
    inline pos_t next( const pos_t& pos ){
        return m_bufMgr->next(pos);
	}

	/**
	 * 获取最后记录位置
	 * @return
	 */
    inline pos_t last(){
        return m_bufMgr->last();
	}

    bool find( const id_t& id,pos_t& pos );

    pos_t getPos( const id_t& id );

	bool reload( pos_t& pos );
	bool reload( const id_t& id );

	bool unload( pos_t& pos );
	bool unload( const id_t& id );

	void reset();
private:
	dm::TScopedPtr<buf_mgr_t> m_bufMgr;

	friend class CTask;
};

}
}
}

#endif /* _DM_OS_SYS_TASK_MGR_HPP_ */

/*
 * task_loader.hpp
 *
 *  Created on: 2023年10月18日
 *      Author: Dylan.Gao
 */

#ifndef _DM_OS_SYS_TASK_LOADER_HPP_
#define _DM_OS_SYS_TASK_LOADER_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_SYS
#define DM_API_OS_SYS DM_API_IMPORT
#endif

#include <dm/os/sys/task_mgr.hpp>
#include <dm/os/db/db.hpp>
#include <dm/os/db/resultset.hpp>
#include <dm/scoped_ptr.hpp>
#include <vector>

namespace dm{
namespace os{
namespace sys{

/**
 * 任务信息加载器类
 */
class DM_API_OS_SYS CTaskLoader:public CTaskMgr::buf_mgr_t::CLoader{
public:
    typedef CTaskMgr::store_record_t record_t;

	CTaskLoader();
	~CTaskLoader();

	bool createTable();

	bool addTask( id_t& id,const char* descr,const char* program,const char* cmdline,STaskInfo::EType type,STaskInfo::EStartMode startMode,id_t hostA,id_t hostB,id_t group,CTimeStamp::s_t delay,CTimeStamp::s_t alive );
	bool delTask( const id_t& id );
	bool delTask( const char* program );

	bool addALoaded( record_t& r );

	bool filterLoaded();

	bool next();
	bool loadRecord( record_t& record );
	bool reloadRecord( record_t& record );

	static const char* type2string( STaskInfo::EType type );
	static STaskInfo::EType string2type( const char* str );

	static const char* startMode2string( STaskInfo::EStartMode startMode );
	static STaskInfo::EStartMode string2startMode( const char* str );

private:
	dm::TScopedPtr<dm::os::CDb> m_db;
	dm::TScopedPtr<dm::os::CResultSet> m_resultSet;

	std::vector<id_t> m_loadedRecordsId;

	dm::CTimeStamp m_ts;
	dm::CRunTimeStamp m_rts;
};

}
}
}

#endif /* _DM_OS_SYS_TASK_LOADER_HPP_ */

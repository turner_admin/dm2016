/*
 * task_updater.hpp
 *
 *  Created on: 2023年9月22日
 *      Author: Dylan.Gao
 */

#ifndef _DM_OS_SYS_TASK_UPDATER_HPP_
#define _DM_OS_SYS_TASK_UPDATER_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_SYS
#define DM_API_OS_SYS DM_API_IMPORT
#endif

#include <dm/os/sys/task_mgr.hpp>
#include <dm/combine.hpp>

namespace dm{
namespace os{
namespace sys{

/**
 * 任务更新器类
 */
class DM_API_OS_SYS CTaskUpdater:public CTaskMgr::buf_mgr_t::TCUpdater<dm::TSCombine3<STaskState*,dm::CTimeStamp*,dm::CRunTimeStamp*> >{
public:
	typedef dm::TSCombine3<STaskState*,dm::CTimeStamp*,dm::CRunTimeStamp*> value_t;
protected:
    bool updateRecord( value_t& value,CTaskMgr::store_record_t& record );
};

}
}
}

#endif /* _DM_OS_SYS_TASK_UPDATER_HPP_ */

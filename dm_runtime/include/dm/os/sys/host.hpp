/*
 * host.hpp
 *
 *  Created on: 2023年9月16日
 *      Author: Dylan.Gao
 */

#ifndef _DM_OS_SYS_HOST_HPP_
#define _DM_OS_SYS_HOST_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_SYS
#define DM_API_OS_SYS DM_API_IMPORT
#endif

#include <dm/os/sys/host_mgr.hpp>

namespace dm{
namespace os{
namespace sys{

/**
 * 主机信息类
 */
class DM_API_OS_SYS CHost{
public:
	typedef CHostMgr::pos_t pos_t;
	typedef SHostState::EState state_t;
	typedef SHostState::ENetState net_state_t;

	CHost();
	CHost( const pos_t& pos );
	~CHost();

	void setPos( const pos_t& pos );

	inline bool isValid(){
		return m_pos.isValid();
	}

	/**
	 * 刷新主机信息
	 * 由于主机信息是共享数据，需要通过刷新，才能获取
	 * @return
	 */
	bool refresh();

	inline const SHostInfo& getInfo()const{
		return m_info;
	}

	inline const SHostState& getState()const{
		return m_state;
	}

	inline const dm::CTimeStamp& getTimeStamp()const{
		return m_ts;
	}

	inline const dm::CRunTimeStamp& getRunTimeStamp()const{
		return m_rts;
	}

	inline const char* stateString()const{
		return m_state.state2string(m_state.state);
	}

	inline const char* net1stateString()const{
		return m_state.netState2string(m_state.net1State);
	}

	inline const char* net2stateString()const{
		return m_state.netState2string(m_state.net2State);
	}

	bool updateState( const state_t* state,const net_state_t* net1State,const net_state_t* net2State,const dm::CTimeStamp& tsNow=dm::CTimeStamp::cur(),const dm::CRunTimeStamp& rtsNow=dm::CRunTimeStamp::cur() );

	inline const pos_t& getPos()const{
		return m_pos;
	}

private:
	pos_t m_pos;

	SHostInfo m_info;
	SHostState m_state;
	dm::CTimeStamp m_ts;
	dm::CRunTimeStamp m_rts;
};

}
}
}

#endif /* _DM_OS_SYS_HOST_HPP_ */

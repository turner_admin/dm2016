/*
 * host_info.hpp
 *
 *  Created on: 2023年9月12日
 *      Author: Dylan.Gao
 */

#ifndef _DM_OS_SYS_HOST_RECORD_HPP_
#define _DM_OS_SYS_HOST_RECORD_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_SYS
#define DM_API_OS_SYS DM_API_IMPORT
#endif

#include <dm/os/types.hpp>
#include <dm/fixdstring.hpp>
#include <dm/ip4_str.hpp>

namespace dm{
namespace os{
namespace sys{

/**
 * 主机信息
 */
struct DM_API_OS_SYS SHostInfo{
	enum{
#ifdef SIZE_OF_HOST_NAME
		SizeOfName = SIZE_OF_HOST_NAME,
#else
		SizeOfName = 32,
#endif

#ifdef SIZE_OF_HOST_DESC
		SizeOfDesc = SIZE_OF_HOST_DESC,
#else
		SizeOfDesc = 128,
#endif
	};

	typedef dm::CFixdString<SizeOfName> name_t;
	typedef dm::CFixdString<SizeOfDesc> desc_t;

	id_t id;
	name_t name;
	desc_t desc;

	dm::CIp4 net1_ip;
	dm::CIp4 net2_ip;

	id_t backed;
};

/**
 * 主机状态
 */
struct DM_API_OS_SYS SHostState{
	enum EState:dm::uint8{
		SUnknow,
		SOffLine,
		SLinking,
		SMained,
		SBacked,
		SMaining,
		SBacking,
		SShutDown
	};

	enum ENetState:dm::uint8{
		NsUnknow,
		NsDirect,
		NsOnline,
		NsOffLine
	};

	inline void init(){
		state = SUnknow;
		net1State = NsUnknow;
		net2State = NsUnknow;
	}

	static const char* state2string( EState state );
	static EState string2state( const char* str );

	static const char* netState2string( ENetState netState );
	static ENetState string2netState( const char* str );

	EState state;
	ENetState net1State;
	ENetState net2State;
};

}
}
}

#endif /* _DM_OS_SYS_HOST_RECORD_HPP_ */

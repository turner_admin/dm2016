/*
 * host_mgr.hpp
 *
 *  Created on: 2023年9月7日
 *      Author: Dylan.Gao
 */

#ifndef _DM_OS_SYS_HOST_MGR_HPP_
#define _DM_OS_SYS_HOST_MGR_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_SYS
#define DM_API_OS_SYS DM_API_IMPORT
#endif

#include <dm/os/shared_rt_record.hpp>
#include <dm/os/shared_buffer_mgr.hpp>
#include <dm/os/sys/host_record.hpp>
#include <dm/scoped_ptr.hpp>

namespace dm{
namespace os{
namespace sys{

/**
 * 主机管理器类
 * 实现对主机信息的全局共享访问
 */
class DM_API_OS_SYS CHostMgr{
	CHostMgr();
public:
	enum{
#ifdef HOST_MGR_SIZE_OF_MASKS
		SizeOfMasks = HOST_MGR_SIZE_OF_MASKS,
#else
		SizeOfMasks = 8,
#endif

#ifdef HOST_MGR_SIZE_OF_NAME
		SizeOfName = HOST_MGR_SIZE_OF_NAME,
#else
		SizeOfName = 64,
#endif
	};

    typedef dm::os::TSSharedRtRecord<SHostInfo,SHostState,dm::os::id_t> store_record_t;
    typedef dm::os::TCSharedBufferMgr<store_record_t,dm::os::id_t,dm::uint8,SizeOfMasks,SizeOfName> buf_mgr_t;
	typedef buf_mgr_t::CPos pos_t;

	static CHostMgr& ins();

	/**
	 * 加载新的记录
	 * @return
	 */
    bool loadNews();

    pos_t loadNew();

	/**
	 * 获取记录数
	 * @return
	 */
	inline dm::uint32 count(){
        return m_bufMgr->count();
	}

    inline pos_t firstPos(){
        return m_bufMgr->first();
    }

	/**
	 * 获取第一条记录的位置
	 * @return
	 */
    inline bool firstPos(pos_t& pos){
        return m_bufMgr->first().isValid();
	}

	/**
	 * 获取下一条记录的位置
	 * @param pos
	 * @return
	 */
    inline pos_t next( const pos_t& pos ){
        return m_bufMgr->next(pos);
	}

	/**
	 * 获取最后记录位置
	 * @return
	 */
    inline pos_t last(){
        return m_bufMgr->last();
	}

    pos_t getPos( const id_t& id );
    pos_t getPos( const char* name );

    bool getPos( const id_t& id,pos_t& pos );
    bool getPos( const char* name,pos_t& pos );

	bool reload( pos_t& pos );
	bool reload( id_t id );
	bool reload( const char* name );

	bool unload( pos_t& pos );
	bool unload( const id_t& id );
	bool unload( const char* name );

	void reset();
private:
	dm::TScopedPtr<buf_mgr_t> m_bufMgr;

	friend class CHost;
};

}
}
}

#endif /* _DM_OS_SYS_HOST_MGR_HPP_ */

/*
 * host_reader.hpp
 *
 *  Created on: 2023年10月14日
 *      Author: Dylan.Gao
 */

#ifndef _DM_OS_SYS_HOST_READER_HPP_
#define _DM_OS_SYS_HOST_READER_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_SYS
#define DM_API_OS_SYS DM_API_IMPORT
#endif

#include <dm/os/sys/host_mgr.hpp>
#include <dm/combine.hpp>

namespace dm{
namespace os{
namespace sys{

/**
 * 主机读取器类
 */
class DM_API_OS_SYS CHostReader:public CHostMgr::buf_mgr_t::TCReader<dm::TSCombine4<SHostInfo*,SHostState*,dm::CTimeStamp*,dm::CRunTimeStamp*> >{
public:
	/**
	 * value类型
	 */
	typedef dm::TSCombine4<SHostInfo*,SHostState*,dm::CTimeStamp*,dm::CRunTimeStamp*> value_t;
protected:
    bool readRecord( CHostMgr::store_record_t& record,value_t& value );
};

}
}
}

#endif /* _DM_OS_SYS_HOST_READER_HPP_ */

/*
 * task_finder.hpp
 *
 *  Created on: 2023年10月18日
 *      Author: Dylan.Gao
 */

#ifndef _DM_OS_SYS_TASK_FINDER_HPP_
#define _DM_OS_SYS_TASK_FINDER_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_SYS
#define DM_API_OS_SYS DM_API_IMPORT
#endif

#include <dm/os/sys/task_mgr.hpp>

namespace dm{
namespace os{
namespace sys{

/**
 * 任务查找器类
 */
class DM_API_OS_SYS CTaskFinder:public CTaskMgr::buf_mgr_t::CFinder{
public:
    bool compareId( CTaskMgr::store_record_t& record,const id_t& id );
};

}
}
}

#endif /* _DM_OS_SYS_TASK_FINDER_HPP_ */

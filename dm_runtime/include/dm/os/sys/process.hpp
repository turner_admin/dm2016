/*
 * process.hpp
 *
 *  Created on: 2018年5月16日
 *      Author: turner
 */

#ifndef _DM_OS_SYS_PROCESS_HPP_
#define _DM_OS_SYS_PROCESS_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_SYS
#define DM_API_OS_SYS DM_API_IMPORT
#endif

#ifdef WIN32
#include <Windows.h>
#else
#include <unistd.h>
#endif

namespace dm{
namespace os{
namespace sys{

/**
 * 进程标识
 */

#ifdef WIN32
	typedef DWORD process_t;
#else
	typedef pid_t process_t;
#endif

DM_API_OS_SYS bool ifProcess( process_t process );
DM_API_OS_SYS void killProcess( process_t process );
DM_API_OS_SYS void killProcess( process_t process,int sig );

}
}
}

#endif /* _DM_OS_SYS_PROCESS_HPP_ */

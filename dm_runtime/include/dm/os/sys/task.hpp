/*
 * task.hpp
 *
 *  Created on: 2023年9月20日
 *      Author: Dylan.Gao
 */

#ifndef _DM_OS_SYS_TASK_HPP_
#define _DM_OS_SYS_TASK_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_SYS
#define DM_API_OS_SYS DM_API_IMPORT
#endif

#include <dm/os/sys/task_mgr.hpp>
#include <dm/os/sys/host.hpp>

namespace dm{
namespace os{
namespace sys{

/**
 * 任务类型
 */
class DM_API_OS_SYS CTask{
public:
	typedef CTaskMgr::pos_t pos_t;
	typedef STaskState state_t;

	CTask();
	CTask( const pos_t& pos);
	~CTask();

	void setPos( const pos_t& pos );

	inline bool isValid(){
		return m_pos.isValid();
	}

	bool refresh();

	inline const STaskInfo& getInfo()const{
		return m_info;
	}

	inline const STaskState& getState()const{
		return m_state;
	}

	inline const dm::CTimeStamp& getTimeStamp()const{
		return m_ts;
	}

	inline const dm::CRunTimeStamp& getRunTimeStamp()const{
		return m_rts;
	}

	bool updateState( const STaskState& state,const dm::CTimeStamp& tsNow,const dm::CRunTimeStamp& rtsNow );
	bool updateState( const state_t::EState* pState,const process_t* pPid,const dm::CTimeStamp* pTsBegin,const dm::CTimeStamp* pTsStop,const dm::CTimeStamp* pTsExec,const dm::CTimeStamp* pTsCheck,const dm::CRunTimeStamp* pRtsRefresh,const dm::CTimeStamp& tsNow=dm::CTimeStamp::cur(),const dm::CRunTimeStamp& rtsNow=dm::CRunTimeStamp::cur() );
	bool updateState( state_t::EState state,const dm::CTimeStamp& tsNow=dm::CTimeStamp::cur(),const dm::CRunTimeStamp& rtsNow=dm::CRunTimeStamp::cur() );

	inline const pos_t& getPos()const{
		return m_pos;
	}

	/**
	 * 获取主服务器
	 * @return
	 */
	CHost mainHost();

	/**
	 * 获取备服务器
	 * @return
	 */
	CHost backHost();

	/**
	 * 获取当前运行的服务器
	 * @return
	 */
	CHost runningHost();

private:
	pos_t m_pos;

	STaskInfo m_info;
	STaskState m_state;
	dm::CTimeStamp m_ts;
	dm::CRunTimeStamp m_rts;
};

}
}
}

#endif /* _DM_OS_SYS_TASK_HPP_ */

/*
 * host_writer.hpp
 *
 *  Created on: 2023年10月14日
 *      Author: 高德绵
 */

#ifndef _DM_OS_SYS_HOST_WRITER_HPP_
#define _DM_OS_SYS_HOST_WRITER_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_SYS
#define DM_API_OS_SYS DM_API_IMPORT
#endif

#include <dm/os/sys/host_mgr.hpp>
#include <dm/combine.hpp>

namespace dm{
namespace os{
namespace sys{

class DM_API_OS_SYS CHostWriter:public CHostMgr::buf_mgr_t::TCWriter<dm::TSCombine3<SHostState*,dm::CTimeStamp*,dm::CRunTimeStamp*> >{
public:
	typedef dm::TSCombine3<SHostState*,dm::CTimeStamp*,dm::CRunTimeStamp*> value_t;
protected:
    bool writeRecord( value_t& value,CHostMgr::store_record_t& record );
};

}
}
}

#endif /* _DM_OS_SYS_HOST_WRITER_HPP_ */

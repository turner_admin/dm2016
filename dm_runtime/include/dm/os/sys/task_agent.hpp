/*
 * task_agent.hpp
 *
 *  Created on: 2023年10月23日
 *      Author: Dylan.Gao
 */

#ifndef _DM_OS_SYS_TASK_AGENT_HPP_
#define _DM_OS_SYS_TASK_AGENT_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_SYS
#define DM_API_OS_SYS DM_API_IMPORT
#endif

#include <dm/os/sys/task.hpp>

namespace dm{
namespace os{
namespace sys{

/**
 * 任务代理器类
 */
class DM_API_OS_SYS CTaskAgent{
public:
	/**
	 * 一个进程，只允许有一个对象
	 */
	static CTaskAgent* TaskAgent;

	/**
	 * 任务代理器
	 * @param id
	 */
	CTaskAgent( id_t id=-1 );

	/**
	 * 获取任务id号
	 * @return
	 */
	inline id_t getId(){
		return m_task.isValid()?m_task.getInfo().id:-1;
	}

	/**
	 * 任务是否有效
	 * @return
	 */
	inline bool isValid(){
		return m_task.isValid();
	}

	bool ifCanRun( const dm::CTimeStamp& tsNow=dm::CTimeStamp::cur(),const dm::CRunTimeStamp& rtsNow=dm::CRunTimeStamp::cur() );

	bool runOnce( const dm::CTimeStamp& tsNow=dm::CTimeStamp::cur(),const dm::CRunTimeStamp& rtsNow=dm::CRunTimeStamp::cur() );

private:
	CTask m_task;
};

}
}
}

#endif /* _DM_OS_SYS_TASK_AGENT_HPP_ */

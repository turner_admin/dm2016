/*
 * sys_mgr.hpp
 *
 *  Created on: 2023年10月24日
 *      Author: Dylan.Gao
 */

#ifndef _DM_OS_SYS_SYS_MGR_HPP_
#define _DM_OS_SYS_SYS_MGR_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_SYS
#define DM_API_OS_SYS DM_API_IMPORT
#endif

#include <dm/fixdstring.hpp>
#include <dm/runtimestamp.hpp>
#include <dm/timestamp.hpp>
#include <dm/os/types.hpp>
#include <dm/magic.hpp>

namespace dm{
namespace os{
namespace sys{

/**
 * 系统管理器器类
 */
class DM_API_OS_SYS CSysMgr{
	CSysMgr();

public:
	enum EState:dm::uint8{
		SUninit,	// 未初始化
		SStarting,	// 启动中
		SRunning,	// 运行中
		SExit,		// 退出中
		SRestart,	// 正在重启
		SBackup,	// 待机
		SMax
	};

	static CSysMgr& ins();

	static const char* state2string( EState state );

	bool update( const pid_t& pid,EState& state,const dm::CRunTimeStamp& rtsNow=dm::CRunTimeStamp::cur() );

	bool set( EState state,const dm::CRunTimeStamp& rtsNow=dm::CRunTimeStamp::cur() );

	const char* getProject()const;

	const char* getHost()const;

	const dm::CTimeStamp::us_t getUsRefresh()const;

	EState getState();

	dm::CRunTimeStamp getRtsState();
	dm::CRunTimeStamp getRtsRefresh();
	dm::CRunTimeStamp getRtsNext();
	dm::CRunTimeStamp getRtsFirst();
	dm::CRunTimeStamp getRtsThis();
	dm::CRunTimeStamp getRtsExit();

	bool get( EState* pState,dm::CRunTimeStamp* pRtsState,dm::CRunTimeStamp* pRtsRefresh,dm::CRunTimeStamp* pRtsNext,dm::CRunTimeStamp* pRtsFirst,dm::CRunTimeStamp* pRtsThis,dm::CRunTimeStamp* pRtsExit );

	bool ifCanRun( const dm::CRunTimeStamp& rtsNow=dm::CRunTimeStamp::cur() );

	bool ifExit( const dm::CRunTimeStamp& rtsNow=dm::CRunTimeStamp::cur() );

	bool reload( const dm::CRunTimeStamp& rtsNow=dm::CRunTimeStamp::cur() );
};

}
}
}

#endif /* _DM_OS_SYS_SYS_MGR_HPP_ */

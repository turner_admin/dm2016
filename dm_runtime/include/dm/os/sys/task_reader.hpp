/*
 * task_reader.hpp
 *
 *  Created on: 2023年9月20日
 *      Author: Dylan.Gao
 */

#ifndef _DM_OS_SYS_TASK_READER_HPP_
#define _DM_OS_SYS_TASK_READER_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_SYS
#define DM_API_OS_SYS DM_API_IMPORT
#endif

#include <dm/os/sys/task_mgr.hpp>
#include <dm/combine.hpp>

namespace dm{
namespace os{
namespace sys{

/**
 * 任务读取器类
 */
class DM_API_OS_SYS CTaskReader:public CTaskMgr::buf_mgr_t::TCReader<dm::TSCombine4<STaskInfo*,STaskState*,dm::CTimeStamp*,dm::CRunTimeStamp*> >{
public:
	/**
	 * value类型
	 */
	typedef dm::TSCombine4<STaskInfo*,STaskState*,dm::CTimeStamp*,dm::CRunTimeStamp*> value_t;
protected:
    bool readRecord( CTaskMgr::store_record_t& record,value_t& value );
};

}
}
}

#endif /* _DM_OS_SYS_TASK_READER_HPP_ */

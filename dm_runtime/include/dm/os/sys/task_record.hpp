/*
 * task_record.hpp
 *
 *  Created on: 2023年9月15日
 *      Author: Dylan.Gao
 */

#ifndef _DM_OS_SYS_TASK_RECORD_HPP_
#define _DM_OS_SYS_TASK_RECORD_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_SYS
#define DM_API_OS_SYS DM_API_IMPORT
#endif

#include <dm/os/types.hpp>
#include <dm/fixdstring.hpp>
#include <dm/timestamp.hpp>
#include <dm/runtimestamp.hpp>
#include <dm/datetime.hpp>
#include <dm/os/sys/process.hpp>

namespace dm{
namespace os{
namespace sys{

/**
 * 任务信息
 */
struct DM_API_OS_SYS STaskInfo{
	enum{
#ifdef SIZE_OF_TASK_DESC
		SizeOfDesc = SIZE_OF_TASK_DESC,
#else
		SizeOfDesc = 128,
#endif

#ifdef SIZE_OF_TASK_PROGRAM
		SizeOfProgram = SIZE_OF_TASK_PROGRAM,
#else
		SizeOfProgram = 128,
#endif

#ifdef SIZE_OF_TASK_CMDLINE
		SizeOfCmdline = SIZE_OF_TASK_CMDLINE,
#else
		SizeOfCmdline = 128,
#endif

	};

	// 任务类型
	enum EType:dm::uint8{
		Normal,		// 普通任务
		Device,		// 设备数据任务
		Forward,	// 转发任务
		Caculate,	// 计算任务
		Ui,			// 人机交互任务
		System,		// 系统任务
		TypeMax
	};

	static const char* type2string( EType type );
	static EType string2type( const char* str );

	enum EStartMode:dm::uint8{
		StartDeny,		// 禁止启动
		StartDaemon,	// 守护启动
		StartSingle,	// 单次执行
		StartManual,	// 手动启动
		StartSecondly,	// 按秒执行
		StartMinutely,	// 按分钟执行
		StartHourly,	// 按小时执行
		StartDaily,		// 按天执行
		StartMonthly,	// 按月执行
		StartYearly,	// 按年执行
		StartMondays,	// 每周一执行
		StartTuesdays,
		StartWednesdays,
		StartThursdays,
		StartFridays,
		StartSaturdays,
		StartSundays
	};

	static const char* startMode2string( EStartMode startMode );
	static EStartMode string2startMode( const char* str );

	typedef dm::CFixdString<SizeOfDesc> desc_t;
	typedef dm::CFixdString<SizeOfProgram> program_t;
	typedef dm::CFixdString<SizeOfCmdline> cmdline_t;


	id_t id;		// 任务id号
	desc_t desc;	// 任务描述
	program_t program;	// 任务程序
	cmdline_t cmdline;	// 任务命令行参数

	id_t hostGroup;	// 所属主机分组
	id_t mainHost;	// 所属主机
	id_t backHost;	// 所属备机

	EType type;				// 任务类型
	EStartMode startMode;	// 任务启动模式

	dm::CTimeStamp::s_t delaySeconds;	// 启动延时
	dm::CTimeStamp::s_t aliveSeconds;	// 存活时长
	dm::CDateTime dateTime;	// 启动时刻
};

/**
 * 任务状态
 */
struct DM_API_OS_SYS STaskState{
	enum EState:dm::uint8{
		Uninit,		// 未初始化
		Stoped,		// 已经停止
		Starting,	// 正在启动
		Running,	// 运行中
		OtherStarting,	// 在其他主机上启动
		OtherRunning,	// 在其他主机上运行
		Stoping		// 停止中
	};

	static const char* state2string( EState state );
	static EState string2state( const char* str );

	EState state;
	process_t pid;
	CTimeStamp tsBegin;
	CTimeStamp tsStop;
	CTimeStamp tsExec;
	CTimeStamp tsCheck;

	CRunTimeStamp rtsRefresh;
};

}
}
}

#endif /* _DM_OS_SYS_TASK_RECORD_HPP_ */

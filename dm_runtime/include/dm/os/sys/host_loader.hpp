/*
 * host_loader.hpp
 *
 *  Created on: 2023年9月14日
 *      Author: Dylan.Gao
 */

#ifndef _DM_OS_SYS_HOST_LOADER_HPP_
#define _DM_OS_SYS_HOST_LOADER_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_SYS
#define DM_API_OS_SYS DM_API_IMPORT
#endif

#include <dm/os/sys/host_mgr.hpp>
#include <dm/os/db/db.hpp>
#include <dm/os/db/resultset.hpp>
#include <dm/scoped_ptr.hpp>
#include <vector>

namespace dm{
namespace os{
namespace sys{

/**
 * 主机信息加载器类
 */
class DM_API_OS_SYS CHostLoader:public CHostMgr::buf_mgr_t::CLoader{
public:
    typedef CHostMgr::store_record_t store_record_t;

	CHostLoader();
	~CHostLoader();

	bool createTable();

	bool addHost( id_t& id,const char* name,const char* descr );
	bool delHost( const id_t& id );
	bool delHost( const char* name );

    bool addALoaded( store_record_t& r );

	bool filterLoaded();

	bool next();
    bool loadRecord( store_record_t& record );
    bool reloadRecord( store_record_t& record );

private:
	dm::TScopedPtr<dm::os::CDb> m_db;
	dm::TScopedPtr<dm::os::CResultSet> m_resultSet;

	std::vector<id_t> m_loadedRecordsId;

	dm::CTimeStamp m_tsNow;
	dm::CRunTimeStamp m_rtsNow;
};

}
}
}



#endif /* DM_RUNTIME_INCLUDE_DM_OS_SYS_HOST_LOADER_HPP_ */

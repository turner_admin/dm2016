/*
 * host_finder.hpp
 *
 *  Created on: 2023年10月14日
 *      Author: Dylan.Gao
 */

#ifndef _DM_OS_SYS_HOST_FINDER_HPP_
#define _DM_OS_SYS_HOST_FINDER_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_SYS
#define DM_API_OS_SYS DM_API_IMPORT
#endif

#include <dm/os/sys/host_mgr.hpp>

namespace dm{
namespace os{
namespace sys{

/**
 * 主机查找器类
 */
class DM_API_OS_SYS CHostFinder:public CHostMgr::buf_mgr_t::CFinder{
public:
    bool compareId( CHostMgr::store_record_t& record,const id_t& id );

    bool compareName( CHostMgr::store_record_t& record,const char* name );
};

}
}
}

#endif /* _DM_OS_SYS_HOST_FINDER_HPP_ */

/*
 * spinlock.hpp
 *
 *  Created on: 2018年3月20日
 *      Author: Dylan.Gao
 */

#ifndef _DM_OS_SPINLOCK_HPP_
#define _DM_OS_SPINLOCK_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_COMMON
#define DM_API_OS_COMMON DM_API_IMPORT
#endif

#include <dm/types.hpp>

namespace dm{
namespace os{

class DM_API_OS_COMMON CSpinlock{
	CSpinlock( const CSpinlock& );
	CSpinlock& operator=( const CSpinlock& );

public:
	CSpinlock();

	/**
	 * 尝试获取锁
	 * @param times 尝试次数
	 * - 0 不锁，只是测试是否可以锁定。即当前是否未锁定
	 * @return
	 */
	bool tryLock( dm::uint64 times=0 );

	/**
	 * 锁定
	 * @param times 尝试次数
	 * - 0 一直尝试，直到锁定
	 */
	void lock( dm::uint64 times=0 );
	void unlock();

private:
	volatile unsigned int m_state;
};

}
}

#endif /* DM_RUNTIME_INCLUDE_DM_OS_SPINLOCK_HPP_ */

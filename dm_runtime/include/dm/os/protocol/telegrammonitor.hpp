﻿/*
 * telegrammonitor.hpp
 *
 *  Created on: 2017年3月3日
 *      Author: work
 */

#ifndef _DM_OS_PROTOCOL_TELEGRAMMONITOR_HPP_
#define _DM_OS_PROTOCOL_TELEGRAMMONITOR_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_PROTOCOL
#define DM_API_OS_PROTOCOL DM_API_IMPORT
#endif

#include <dm/os/protocol/telegrammgr.hpp>

namespace dm{
namespace os{
namespace protocol{

class DM_API_OS_PROTOCOL CTelegramMonitor:public CTelegramMgr{
public:
	typedef queue_t::ringpos_t ringpos_t;
	typedef CTelegramInfo info_t;

	CTelegramMonitor();
	virtual ~CTelegramMonitor(){};

	virtual void onNew( const ringpos_t& pos,const info_t& info,const bool& overflow )const;
	virtual bool filter( const ringpos_t& pos,const info_t& info,const bool& overflow )const;

	void run();

	bool tryGet( info_t& info,bool& overflow );
	void get( info_t& info,bool& overFlag );

	inline const ringpos_t& pos()const{
		return m_p;
	}

	inline const bool& isRaw()const{
		return m_raw;
	}

	inline void setRaw( const bool& b=true ){
		m_raw = b;
	}

	inline void setChar( const bool& b=true ){
		m_char = b;
	}

	inline void setNoData( const bool& b=true ){
		m_noData = b;
	}

private:
	ringpos_t m_p;
	bool m_raw;
	bool m_char;
	bool m_noData;	// 不显示报文
};

}
}
}



#endif /* INCLUDE_DM_OS_PROTOCOL_TELEGRAMMONITOR_HPP_ */

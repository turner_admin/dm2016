﻿/*
 * cannetmaster.hpp
 *
 *  Created on: 2017年4月1日
 *      Author: work
 */

#ifndef _DM_OS_PROTOCOL_CANNETMASTER_HPP_
#define _DM_OS_PROTOCOL_CANNETMASTER_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_PROTOCOL
#define DM_API_OS_PROTOCOL DM_API_IMPORT
#endif

#include <dm/protocol/cannetmaster.hpp>
#include <dm/os/protocol/protocolmasters.hpp>

namespace dm{
namespace os{
namespace protocol{

/**
 * 基于CAN网络的主站通信控制器
 */
class DM_API_OS_PROTOCOL CCannetMaster:virtual public dm::protocol::CCannetMaster,virtual public CProtocolMasters{
public:
	typedef dm::protocol::CFrameCan frame_can_t;

	CCannetMaster();
	virtual ~CCannetMaster();
};

}
}
}



#endif /* INCLUDE_DM_OS_PROTOCOL_CANNETMASTER_HPP_ */

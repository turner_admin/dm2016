﻿/*
 * protocollistenor.hpp
 *
 *  Created on: 2017年3月7日
 *      Author: work
 */

#ifndef _DM_OS_PROTOCOL_PROTOCOLLISTENOR_HPP_
#define _DM_OS_PROTOCOL_PROTOCOLLISTENOR_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_PROTOCOL
#define DM_API_OS_PROTOCOL DM_API_IMPORT
#endif

#include <dm/os/ascom/listener.hpp>
#include <vector>

namespace dm{
namespace os{
namespace protocol{

class DM_API_OS_PROTOCOL CProtocolControlor;

/**
 * 规约监听器类
 * 对端口进行监听，并分配新的连接到规约控制器。
 */
class DM_API_OS_PROTOCOL CProtocolListenor{
	CProtocolListenor( const CProtocolListenor& );
	CProtocolListenor& operator=( const CProtocolListenor& );
public:
	typedef ascom::CListener listenor_t;
	typedef listenor_t::device_t* connect_t;
	typedef CProtocolControlor* controlor_t;
	typedef std::vector<controlor_t> controlor_list_t;

	CProtocolListenor();
	virtual ~CProtocolListenor();

	void setListenor( listenor_t* listenor );

	bool addProtocolControlor( CProtocolControlor* controlor );
	void freeAllProtocolControlor();

	// 新连接处理函数
	void onNewConnect( connect_t connect );

	// 规约连接断开处理函数
	void onProtocolControlorDisconnected( CProtocolControlor* protocolControlor );

	bool startListen();
	bool stopListen();

	inline void setBufSize( const size_t& size ){
		m_bufSize = size;
	}

protected:
	controlor_t getAnIdleControlor();
	bool readyForAcceptNewConnection();

protected:
	listenor_t* m_listenor;

	std::vector<controlor_t> m_controlors;
	size_t m_bufSize;
};

}
}
}



#endif /* INCLUDE_DM_OS_PROTOCOL_PROTOCOLLISTENOR_HPP_ */

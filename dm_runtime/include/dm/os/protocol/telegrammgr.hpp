﻿/*
 * telegrammgr.hpp
 *
 *  Created on: 2017年3月3日
 *      Author: work
 */

#ifndef _DM_OS_PROTOCOL_TELEGRAMMGR_HPP_
#define _DM_OS_PROTOCOL_TELEGRAMMGR_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_PROTOCOL
#define DM_API_OS_PROTOCOL DM_API_IMPORT
#endif

#include <dm/os/protocol/telegraminfo.hpp>
#include <dm/os/syswaitqueue.hpp>

namespace dm{
namespace os{
namespace protocol{

class DM_API_OS_PROTOCOL CTelegramMgr{
	CTelegramMgr( const CTelegramMgr& );
	CTelegramMgr& operator=( const CTelegramMgr& );

public:
	enum{
		Size = 1024
	};

	typedef CTelegramInfo info_t;
	typedef dm::os::CSysWaitQueue<info_t,Size> queue_t;

public:
	CTelegramMgr();

	static void remove();

protected:
	queue_t* m_q;
};

}
}
}

#endif /* INCLUDE_DM_OS_PROTOCOL_TELEGRAMMGR_HPP_ */

﻿/*
 * iec104slaver.hpp
 *
 *  Created on: 2017年3月2日
 *      Author: work
 */

#ifndef _DM_OS_PROTOCOL_IEC104SLAVER_HPP_
#define _DM_OS_PROTOCOL_IEC104SLAVER_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_PROTOCOL
#define DM_API_OS_PROTOCOL DM_API_IMPORT
#endif

#include <dm/protocol/iec104slaver.hpp>
#include <dm/os/protocol/protocolslaver.hpp>

namespace dm{
namespace os{
namespace protocol{

class DM_API_OS_PROTOCOL CIec104Slaver:virtual public dm::protocol::CIec104Slaver,virtual public dm::os::protocol::CProtocolSlaver{
public:
	CIec104Slaver();
	virtual ~CIec104Slaver();

	void reset( const ts_t& ts,const rts_t& rts );

protected:
};

}
}
}



#endif /* INCLUDE_DM_OS_PROTOCOL_IEC104SLAVER_HPP_ */

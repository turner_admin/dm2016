﻿/*
 * protocolmaster.hpp
 *
 *  Created on: 2017年3月18日
 *      Author: work
 */

#ifndef _DM_OS_PROTOCOL_PROTOCOLMASTER_HPP_
#define _DM_OS_PROTOCOL_PROTOCOLMASTER_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_PROTOCOL
#define DM_API_OS_PROTOCOL DM_API_IMPORT
#endif

#include <dm/os/protocol/protocolmasterbase.hpp>
#include <dm/scada/deviceagent.hpp>
#include <dm/timestamp.hpp>

namespace dm{
namespace os{
namespace protocol{

/**
 * 主站规约类
 */
class DM_API_OS_PROTOCOL CProtocolMaster:public dm::os::protocol::CProtocolMasterBase{
public:
	CProtocolMaster();
	virtual ~CProtocolMaster();

	bool setDevice( const char* name,const dm::scada::CDeviceAgent::SSignalsSize* size=NULL );
	bool setDevice( const int& id,const dm::scada::CDeviceAgent::SSignalsSize* size=NULL );

	bool isLocalRemoteCtl( const dm::msg::index_t& idx )const;
	bool isLocalPara( const dm::msg::index_t& idx )const;

	virtual void onLinkSettled( const ts_t& ts,const rts_t& rts );
	virtual void onLinkClosed( const ts_t& ts,const rts_t& rts );

	virtual void updateRecvTime( dm::uint32 bytes,const rts_t& rts );
	virtual void updateSendTime( dm::uint32 bytes,const rts_t& rts );

protected:
	bool updateStatus( const index_t& status,const dm::scada::CStatus& value,const dm::scada::EDataSource& ds=dm::scada::ByCollector,const ts_t& ts=ts_t::cur() );
	bool updateDiscrete( const index_t& discrete,const dm::scada::CDiscrete& value,const dm::scada::EDataSource& ds=dm::scada::ByCollector,const ts_t& ts=ts_t::cur() );
	bool updateMeasure( const index_t& measure,const dm::scada::CMeasure::raw_t& raw,const dm::scada::EDataSource& ds=dm::scada::ByCollector,const ts_t& ts=ts_t::cur() );
	bool updateCumulant( const index_t& cumulant,const dm::scada::CCumulant::raw_t& raw,const dm::scada::EDataSource& ds=dm::scada::ByCollector,const ts_t& ts=ts_t::cur() );
	bool genAction( const index_t& action,const dm::scada::CAction::value_t& value,const ts_t& ts=ts_t::cur() );

	inline bool isMyRemoteCtl( const index_t& rc )const{
		return -1!=getLocalRemoteControlIndex(rc);
	}

	inline bool isMyParameter( const index_t& pm )const{
		return -1!=getLocalParamterIndex(pm);
	}

	index_t getLocalStatusIndex( const index_t& idx )const;
	index_t getLocalDiscreteIndex( const index_t& idx )const;
	index_t getLocalMeasureIndex( const index_t& idx )const;
	index_t getLocalCumulantIndex( const index_t& idx )const;
	index_t getLocalRemoteControlIndex( const index_t& idx )const;
	index_t getLocalParamterIndex( const index_t& idx )const;
	index_t getLocalActionIndex( const index_t& idx )const;

	index_t getStatusIndex( const index_t& idx )const;
	index_t getDiscreteIndex( const index_t& idx )const;
	index_t getMeasureIndex( const index_t& idx )const;
	index_t getCumulantIndex( const index_t& idx )const;
	index_t getRemoteControlIndex( const index_t& idx )const;
	index_t getParameterIndex( const index_t& idx )const;
	index_t getActionIndex( const index_t& idx )const;

	index_t sizeOfStatus()const;
	index_t sizeOfDiscrete()const;
	index_t sizeOfMeasure()const;
	index_t sizeOfCumulant()const;
	index_t sizeOfRmtctl()const;
	index_t sizeOfParameter()const;
	index_t sizeOfAction()const;

protected:
	/**
	 * 设备代理器
	 */
	dm::scada::CDeviceAgent* m_devAgent;
};

}
}
}



#endif /* INCLUDE_DM_OS_PROTOCOL_PROTOCOLMASTER_HPP_ */

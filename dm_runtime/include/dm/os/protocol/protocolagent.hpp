﻿/*
 * protocolagent.hpp
 *
 *  Created on: 2017年6月15日
 *      Author: work
 */

#ifndef _DM_OS_PROTOCOL_PROTOCOLAGENT_HPP_
#define _DM_OS_PROTOCOL_PROTOCOLAGENT_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_PROTOCOL
#define DM_API_OS_PROTOCOL DM_API_IMPORT
#endif

#include <dm/protocol/protocolbase.hpp>
#include <dm/os/io/io.hpp>

namespace dm{
namespace os{
namespace protocol{

class DM_API_OS_PROTOCOL CPrototoclAgent{
	CPrototoclAgent( const CPrototoclAgent& );
	CPrototoclAgent& operator=( const CPrototoclAgent& );
public:
	CPrototoclAgent();
	~CPrototoclAgent();

	bool setProtocol( dm::protocol::CProtocolBase* protocol );
	void setIo( dm::os::io::CIo* io );

private:
	io::CIo* m_io;

	bool m_autoConnect;

	dm::protocol::CTxBuffer m_txBuf;
	dm::protocol::CRxBuffer m_rxBuf;
	dm::protocol::CRxBuffer::pos_t m_rxPos;

	dm::protocol::CFrame* m_rxFrame;
	dm::protocol::CFrame* m_txFrame;

	dm::protocol::CProtocolBase* m_protocol;
};

}
}
}

#endif /* INCLUDE_DM_OS_PROTOCOL_PROTOCOLAGENT_HPP_ */

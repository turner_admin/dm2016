﻿/*
 * executor.hpp
 *
 *  Created on: 2017年6月18日
 *      Author: work
 */

#ifndef _DM_OS_PROTOCOL_EXECUTOR_HPP_
#define _DM_OS_PROTOCOL_EXECUTOR_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_PROTOCOL
#define DM_API_OS_PROTOCOL DM_API_IMPORT
#endif

#include <dm/protocol/protocolbase.hpp>
#include <dm/os/io/io.hpp>
#include <dm/runtimestamp.hpp>
#include <dm/counter.hpp>

namespace dm{
namespace os{

namespace protocol{

/**
 * 规约执行者
 */
class DM_API_OS_PROTOCOL CExecutor{
	CExecutor( const CExecutor& );
	CExecutor& operator=( const CExecutor& );

public:
	typedef dm::protocol::CProtocolBase protocol_t;

	typedef protocol_t::frame_t frame_t;
	typedef protocol_t::interval_t interval_t;

	typedef frame_t::txbuf_t txbuf_t;
	typedef frame_t::rxbuf_t rxbuf_t;
	typedef rxbuf_t::pos_t pos_t;

	CExecutor();
	~CExecutor();

	void setIo( io::CIo* io );
	void setProtocol( dm::protocol::CProtocolBase* protocol );

	inline const io::CIo* getIo()const{
		return m_io;
	}

	inline const dm::protocol::CProtocolBase* getProtocol()const{
		return m_protocol;
	}

	bool isConnected()const;

	bool tryConnect();

protected:
	friend class CDispatcher;

	/**
	 * IO可读的时候调用该函数
	 */
	void rx();

	/**
	 * IO可写的时候调用该函数
	 */
	void tx();

	/**
	 * 给内部友元使用
	 * @return
	 */
	inline io::CIo* getIo(){
		return m_io;
	}

private:
	io::CIo* m_io;

	dm::CRunTimeStamp m_lastTryConnect;		// 上次尝试连接时刻
	dm::CRunTimeStamp m_lastRun;			// 上次运行时刻

	dm::uint32 m_tryConnectDelay;		// 再次连接延时秒

	dm::TCounter<dm::uint16> m_connectTimes;	// 连接次数

	/**
	 * 发送缓冲区
	 */
	txbuf_t m_txBuf;

	/**
	 * 接收缓冲区
	 */
	rxbuf_t m_rxBuf;
	pos_t m_rxPos;

	protocol_t* m_protocol;
};

}
}
}



#endif /* INCLUDE_DM_OS_PROTOCOL_EXECUTOR_HPP_ */

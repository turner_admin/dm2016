﻿/*
 * dispatcher.hpp
 *
 *  Created on: 2017年6月18日
 *      Author: work
 */

#ifndef _DM_OS_PROTOCOL_DISPATCHER_HPP_
#define _DM_OS_PROTOCOL_DISPATCHER_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_PROTOCOL
#define DM_API_OS_PROTOCOL DM_API_IMPORT
#endif

#include <dm/os/protocol/executor.hpp>
#include <vector>

namespace dm{
namespace os{
namespace protocol{

/**
 * 规约通信分发器
 */
class DM_API_OS_PROTOCOL CDispatcher{
	CDispatcher( const CDispatcher& );
	CDispatcher& operator=( const CDispatcher& );

public:
	typedef std::vector<CExecutor*> executors_t;

	CDispatcher();
	~CDispatcher();

	void addExecutor( CExecutor* executor );

	inline void setIoCheckTime( const long& ms ){
		m_ioCheckTime = ms;
	}

	inline const long& ioCheckTime()const{
		return m_ioCheckTime;
	}

	void runOnce();

protected:
	void free();

protected:
	executors_t m_executors;

	long m_ioCheckTime;	// ms
};

}
}
}



#endif /* INCLUDE_DM_OS_PROTOCOL_DISPATCHER_HPP_ */

﻿/*
 * protocolslaver.hpp
 *
 *  Created on: 2017年3月9日
 *      Author: work
 */

#ifndef _DM_OS_PROTOCOL_PROTOCOLSLAVER_HPP_
#define _DM_OS_PROTOCOL_PROTOCOLSLAVER_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_PROTOCOL
#define DM_API_OS_PROTOCOL DM_API_IMPORT
#endif

#include <dm/os/protocol/protocolbase.hpp>
#include <dm/os/msg/msgagent.hpp>
#include <dm/scada/logicdevice.hpp>

#include <dm/scada/statusmgr.hpp>
#include <dm/scada/discretemgr.hpp>
#include <dm/scada/measuremgr.hpp>
#include <dm/scada/cumulantmgr.hpp>
#include <dm/scada/remotectlmgr.hpp>
#include <dm/scada/parametermgr.hpp>
#include <dm/scada/actionmgr.hpp>

namespace dm{
namespace os{
namespace protocol{

/**
 * 子站规约类
 * 任务一般由上位机规约通信触发，并转发给本地其他对象。本类封装了接收任务返回。
 * 作为子站，一般使用逻辑设备映射点表，也可以使用全体数据。
 */
class DM_API_OS_PROTOCOL CProtocolSlaver:virtual public CProtocolBase{
public:
	typedef dm::scada::index_t index_t;

	typedef dm::scada::CStatusMgr::rt_t status_rt_t;
	typedef dm::scada::CDiscreteMgr::rt_t discrete_rt_t;
	typedef dm::scada::CMeasureMgr::rt_t measure_rt_t;
	typedef dm::scada::CCumulantMgr::rt_t cumulant_rt_t;

	CProtocolSlaver();
	virtual ~CProtocolSlaver();

	bool setLogicDeviceById( const dm::scada::id_t& id );
	bool setLogicDeviceByName( const char* name );

	static ETaskState ack2taskState( const dm::msg::EMsgAck& ack );

protected:
	virtual EAction taskStart_call( const ts_t& ts,const rts_t& rts );
	virtual EAction taskStart_syncClock( const ts_t& ts,const rts_t& rts );
	virtual EAction taskStart_remoteControl( const ts_t& ts,const rts_t& rts );
	virtual EAction taskStart_parameters( const ts_t& ts,const rts_t& rts );
	virtual EAction taskStart_syncData( const ts_t& ts,const rts_t& rts );
	virtual EAction taskStart_reset( const ts_t& ts,const rts_t& rts );

	virtual EAction taskDo_call( const ts_t& ts,const rts_t& rts );
	virtual EAction taskDo_syncClock( const ts_t& ts,const rts_t& rts );
	virtual EAction taskDo_remoteControl( const ts_t& ts,const rts_t& rts );
	virtual EAction taskDo_parameters( const ts_t& ts,const rts_t& rts );
	virtual EAction taskDo_syncData( const ts_t& ts,const rts_t& rts );
	virtual EAction taskDo_reset( const ts_t& ts,const rts_t& rts );

	int statusSize()const;
	int discreteSize()const;
	int measureSize()const;
	int cumulantSize()const;
	int remoteControlSize()const;
	int parameterSize()const;
	int actionSize()const;

	const status_rt_t* statusRt( const index_t& idx )const;
	const discrete_rt_t* discreteRt( const index_t& idx )const;
	const measure_rt_t* measureRt( const index_t& idx )const;
	const cumulant_rt_t* cumulantRt( const index_t& idx )const;

	index_t statusIndex( const index_t& hostIdx )const;
	index_t discreteIndex( const index_t& hostIdx )const;
	index_t measureIndex( const index_t& hostIdx )const;
	index_t cumulantIndex( const index_t& hostIdx )const;
	index_t remoteControlIndex( const index_t& hostIdx )const;
	index_t parameterIndex( const index_t& hostIdx )const;
	index_t actionIndex( const index_t& hostIdx )const;

	index_t getHostStatusIndex( const index_t& idx )const;
	index_t getHostDiscreteIndex( const index_t& idx )const;
	index_t getHostMeasureIndex( const index_t& idx )const;
	index_t getHostCumulantIndex( const index_t& idx )const;
	index_t getHostRemoteControlIndex( const index_t& idx )const;
	index_t getHostParameterIndex( const index_t& idx )const;
	index_t getHostActionIndex( const index_t& idx )const;

	virtual bool filterMsgRequest_syncClock( const dm::os::msg::CMsgInfo& msg);
	virtual bool filterMsgRequest_call( const dm::os::msg::CMsgInfo& msg);
	virtual bool filterMsgRequest_rmtCtl( const dm::os::msg::CMsgInfo& msg);
	virtual bool filterMsgRequest_set( const dm::os::msg::CMsgInfo& msg);
	virtual bool filterMsgRequest_get( const dm::os::msg::CMsgInfo& msg);
	virtual bool filterMsgRequest_update( const dm::os::msg::CMsgInfo& msg);
	virtual bool filterMsgRequest_syncData( const dm::os::msg::CMsgInfo& msg);
	virtual bool filterMsgRequest_remoteReset( const dm::os::msg::CMsgInfo& msg);

	virtual bool filterMsgAnswer_syncClock( const dm::os::msg::CMsgInfo& msg);
	virtual bool filterMsgAnswer_call( const dm::os::msg::CMsgInfo& msg);
	virtual bool filterMsgAnswer_rmtCtl( const dm::os::msg::CMsgInfo& msg);
	virtual bool filterMsgAnswer_set( const dm::os::msg::CMsgInfo& msg);
	virtual bool filterMsgAnswer_get( const dm::os::msg::CMsgInfo& msg);
	virtual bool filterMsgAnswer_update( const dm::os::msg::CMsgInfo& msg);
	virtual bool filterMsgAnswer_syncData( const dm::os::msg::CMsgInfo& msg);
	virtual bool filterMsgAnswer_remoteReset( const dm::os::msg::CMsgInfo& msg);

	virtual EAction onMsgRequest_syncClock( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts );
	virtual EAction onMsgRequest_call( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts );
	virtual EAction onMsgRequest_rmtCtl( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts );
	virtual EAction onMsgRequest_set( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts );
	virtual EAction onMsgRequest_get( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts );
	virtual EAction onMsgRequest_update( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts );
	virtual EAction onMsgRequest_syncData( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts );
	virtual EAction onMsgRequest_remoteReset( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts );

	virtual EAction onMsgAnswer_syncClock( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts );
	virtual EAction onMsgAnswer_call( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts );
	virtual EAction onMsgAnswer_rmtCtl( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts );
	virtual EAction onMsgAnswer_set( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts );
	virtual EAction onMsgAnswer_get( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts );
	virtual EAction onMsgAnswer_update( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts );
	virtual EAction onMsgAnswer_syncData( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts );
	virtual EAction onMsgAnswer_remoteReset( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts );

protected:
	dm::scada::CLogicDevice *m_logicDevice;
};

}
}
}

#endif /* INCLUDE_DM_OS_PROTOCOL_PROTOCOLSLAVER_HPP_ */

﻿/*
 * iec104master.hpp
 *
 *  Created on: 2017年3月9日
 *      Author: work
 */

#ifndef _DM_OS_PROTOCOL_IEC104MASTER_HPP_
#define _DM_OS_PROTOCOL_IEC104MASTER_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_PROTOCOL
#define DM_API_OS_PROTOCOL DM_API_IMPORT
#endif

#include <dm/protocol/iec104master.hpp>
#include <dm/os/protocol/protocolmaster.hpp>

namespace dm{
namespace os{
namespace protocol{

class DM_API_OS_PROTOCOL CIec104Master:virtual public dm::protocol::CIec104Master,virtual public CProtocolMaster{
	CIec104Master( const CIec104Master& );
	CIec104Master& operator=( const CIec104Master& );
public:
	CIec104Master();
	virtual ~CIec104Master();

	virtual void onLinkSettled( const ts_t& ts,const rts_t& rts );
	virtual void reset( const ts_t& ts,const rts_t& rts );
};

}
}
}

#endif /* INCLUDE_DM_OS_PROTOCOL_IEC104MASTER_HPP_ */

﻿/*
 * telegram.hpp
 *
 *  Created on: 2017年3月3日
 *      Author: work
 */

#ifndef _DM_OS_PROTOCOL_TELEGRAM_HPP_
#define _DM_OS_PROTOCOL_TELEGRAM_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_PROTOCOL
#define DM_API_OS_PROTOCOL DM_API_IMPORT
#endif

#include <dm/os/protocol/telegrammgr.hpp>

namespace dm{
namespace os{
namespace protocol{

class DM_API_OS_PROTOCOL CTelegram:public CTelegramMgr{
	CTelegram();
	CTelegram( const CTelegram& );
	CTelegram& operator=( const CTelegram& );

public:
#ifdef WIN32
	typedef CTelegramInfo::pid_t pid_t;
#endif
	static CTelegram& ins();

	void resetPid();

	void push( const char* protocol,const info_t::EType& type,const dm::uint8* gram,const dm::uint& len );
	inline void pushRx( const char* protocol,const dm::uint8* gram,const dm::uint& len ){
		push(protocol,CTelegramInfo::Rx,gram,len);
	}

	void pushTx( const char* protocol,const dm::uint8* gram,const dm::uint& len ){
		push(protocol,CTelegramInfo::Tx,gram,len);
	}

	inline void pushRaw( const char* protocol,const dm::uint8* gram,const dm::uint& len ){
		push(protocol,CTelegramInfo::Raw,gram,len);
	}

private:
	pid_t m_pid;
};

}
}
}

static inline dm::os::protocol::CTelegram& telegram(){
	return dm::os::protocol::CTelegram::ins();
}
#endif /* INCLUDE_DM_OS_PROTOCOL_TELEGRAM_HPP_ */

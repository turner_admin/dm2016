﻿/*
 * modbustcpmaster.hpp
 *
 *  Created on: 2017年10月30日
 *      Author: work
 */

#ifndef _DM_OS_PROTOCOL_MODBUSTCPMASTER_HPP_
#define _DM_OS_PROTOCOL_MODBUSTCPMASTER_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_PROTOCOL
#define DM_API_OS_PROTOCOL DM_API_IMPORT
#endif

#include <dm/protocol/modbustcpmaster.hpp>
#include <dm/os/protocol/protocolmaster.hpp>

namespace dm{
namespace os{
namespace protocol{

class DM_API_OS_PROTOCOL CModbusTcpMaster:virtual public dm::protocol::CModbusTcpMaster,virtual public CProtocolMaster{
public:
	typedef dm::protocol::CFrameModbusTcp frame_modbus_t;

	CModbusTcpMaster();
	virtual ~CModbusTcpMaster();

	virtual void reset( const ts_t& ts,const rts_t& rts );

protected:
	EAction taskEnd_remoteControl( const ts_t& ts,const rts_t& rts );
	EAction taskEnd_parameters( const ts_t& ts,const rts_t& rts );
	EAction taskEnd_call( const ts_t& ts,const rts_t& rts );
};

}
}
}

#endif /* DM_RUNTIME_INCLUDE_DM_OS_PROTOCOL_MODBUSTCPMASTER_HPP_ */

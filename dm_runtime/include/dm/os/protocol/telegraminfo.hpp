﻿/*
 * telegraminfo.hpp
 *
 *  Created on: 2017年3月3日
 *      Author: work
 */

#ifndef _DM_OS_PROTOCOL_TELEGRAMINFO_HPP_
#define _DM_OS_PROTOCOL_TELEGRAMINFO_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_PROTOCOL
#define DM_API_OS_PROTOCOL DM_API_IMPORT
#endif

#ifdef WIN32
#include <Windows.h>
#else
#include <unistd.h>
#endif

#include <dm/types.hpp>
#include <dm/fixdstring.hpp>
#include <dm/timestamp.hpp>

#ifndef DM_OS_PROTOCOL_LEN_OF_GRAM
#define DM_OS_PROTOCOL_LEN_OF_GRAM 3072
#endif

#ifndef DM_OS_PROTOCOL_LEN_OF_PROTOCOL
#define DM_OS_PROTOCOL_LEN_OF_PROTOCOL 64
#endif

namespace dm{
namespace os{
namespace protocol{

class DM_API_OS_PROTOCOL CTelegramInfo{
public:
	enum{
		LenOfGram = DM_OS_PROTOCOL_LEN_OF_GRAM,
		LenOfProtocol = DM_OS_PROTOCOL_LEN_OF_PROTOCOL
	};

	enum EType{
		Rx = 0,
		Tx,
		Raw
	};

#ifdef WIN32
	typedef DWORD pid_t;
#endif

	typedef dm::CFixdString<LenOfProtocol> protocol_t;

	CTelegramInfo();
	CTelegramInfo( const CTelegramInfo& info);
	CTelegramInfo& operator=( const CTelegramInfo& info );

public:
	pid_t pid;
	dm::CTimeStamp ts;
	EType type;
	protocol_t protocol;

	dm::uint8 gram[LenOfGram];
	dm::uint16 len;
};

}
}
}

#endif /* INCLUDE_DM_OS_PROTOCOL_TELEGRAMINFO_HPP_ */

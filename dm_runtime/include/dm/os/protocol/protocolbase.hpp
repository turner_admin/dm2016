﻿/*
 * protocolbase.hpp
 *
 *  Created on: 2018年7月16日
 *      Author: turner
 */

#ifndef _DM_OS_PROTOCOL_PROTOCOLBASE_HPP_
#define _DM_OS_PROTOCOL_PROTOCOLBASE_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_PROTOCOL
#define DM_API_OS_PROTOCOL DM_API_IMPORT
#endif

#include <dm/os/msg/msgagent.hpp>
#include <dm/protocol/protocol.hpp>

namespace dm{
namespace os{
namespace protocol{

/**
 * 规约基类
 * 本软件包的规约基类，实现了消息检测功能。
 */
class DM_API_OS_PROTOCOL CProtocolBase:virtual public dm::protocol::CProtocol{
public:
	CProtocolBase();

	virtual void reset( const ts_t& ts,const rts_t& rts );

	/**
	 * 是否对请求消息进行检测
	 * 一般主站规约会对请求消息进程检测。用于检测其他任务发送的请求。
	 * @return
	 */
	inline const bool& ifCheckRequsetMsg()const{
		return m_checkMsgRequest;
	}

	/**
	 * 使能控制是否检测请求消息
	 * @param en
	 */
	inline void enableRequestMsgCheck( bool en=true ){
		m_checkMsgRequest = en;
	}

	/**
	 * 是否对应答消息进行检测
	 * @return
	 */
	inline const bool& ifCheckAnswerMsg()const{
		return m_checkMsgAnswer;
	}

	/**
	 * 使能控制是否检测应答消息
	 * @param en
	 */
	inline void enableAnswerMsgCheck( bool en=true ){
		m_checkMsgAnswer = en;
	}

protected:
	EAction refresh_msg( const ts_t& ts,const rts_t& rts );

	virtual bool filterMsgRequest_syncClock( const dm::os::msg::CMsgInfo& msg)=0;
	virtual bool filterMsgRequest_call( const dm::os::msg::CMsgInfo& msg)=0;
	virtual bool filterMsgRequest_rmtCtl( const dm::os::msg::CMsgInfo& msg)=0;
	virtual bool filterMsgRequest_set( const dm::os::msg::CMsgInfo& msg)=0;
	virtual bool filterMsgRequest_get( const dm::os::msg::CMsgInfo& msg)=0;
	virtual bool filterMsgRequest_update( const dm::os::msg::CMsgInfo& msg)=0;
	virtual bool filterMsgRequest_syncData( const dm::os::msg::CMsgInfo& msg)=0;
	virtual bool filterMsgRequest_remoteReset( const dm::os::msg::CMsgInfo& msg)=0;

	virtual bool filterMsgAnswer_syncClock( const dm::os::msg::CMsgInfo& msg)=0;
	virtual bool filterMsgAnswer_call( const dm::os::msg::CMsgInfo& msg)=0;
	virtual bool filterMsgAnswer_rmtCtl( const dm::os::msg::CMsgInfo& msg)=0;
	virtual bool filterMsgAnswer_set( const dm::os::msg::CMsgInfo& msg)=0;
	virtual bool filterMsgAnswer_get( const dm::os::msg::CMsgInfo& msg)=0;
	virtual bool filterMsgAnswer_update( const dm::os::msg::CMsgInfo& msg)=0;
	virtual bool filterMsgAnswer_syncData( const dm::os::msg::CMsgInfo& msg)=0;
	virtual bool filterMsgAnswer_remoteReset( const dm::os::msg::CMsgInfo& msg)=0;


	virtual EAction onMsgRequest_syncClock( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts )=0;
	virtual EAction onMsgRequest_call( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts )=0;
	virtual EAction onMsgRequest_rmtCtl( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts )=0;
	virtual EAction onMsgRequest_set( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts )=0;
	virtual EAction onMsgRequest_get( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts )=0;
	virtual EAction onMsgRequest_update( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts )=0;
	virtual EAction onMsgRequest_syncData( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts )=0;
	virtual EAction onMsgRequest_remoteReset( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts )=0;

	virtual EAction onMsgAnswer_syncClock( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts )=0;
	virtual EAction onMsgAnswer_call( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts )=0;
	virtual EAction onMsgAnswer_rmtCtl( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts )=0;
	virtual EAction onMsgAnswer_set( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts )=0;
	virtual EAction onMsgAnswer_get( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts )=0;
	virtual EAction onMsgAnswer_update( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts )=0;
	virtual EAction onMsgAnswer_syncData( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts )=0;
	virtual EAction onMsgAnswer_remoteReset( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts )=0;

protected:
	/**
	 * 消息接口
	 */
	dm::os::msg::CMsgAgent m_msgAgent;

	/**
	 * 是否进行请求消息检测
	 */
	bool m_checkMsgRequest;

	/**
	 * 是否进行应答消息检测
	 */
	bool m_checkMsgAnswer;

	/**
	 * 上次是否检测请求消息
	 */
	bool m_lastCheckRequest;
};

}
}
}

#endif /* _DM_OS_PROTOCOL_PROTOCOLBASE_HPP_ */

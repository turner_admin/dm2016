﻿/*
 * protocolmasterbase.hpp
 *
 *  Created on: 2017年8月27日
 *      Author: work
 */

#ifndef _DM_OS_PROTOCOL_PROTOCOLMASTERBASE_HPP_
#define _DM_OS_PROTOCOL_PROTOCOLMASTERBASE_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_PROTOCOL
#define DM_API_OS_PROTOCOL DM_API_IMPORT
#endif

#include <dm/os/protocol/protocolbase.hpp>

namespace dm{
namespace os{
namespace protocol{

/**
 * 作为主站的程序基类
 */
class DM_API_OS_PROTOCOL CProtocolMasterBase:virtual public CProtocolBase{
public:
	CProtocolMasterBase();
	virtual ~CProtocolMasterBase();

	/**
	 * 将任务状态转换为消息的ACK
	 * @param state
	 * @return
	 */
	static dm::msg::EMsgAck taskState2ack( const ETaskState& state );

	/**
	 * 测试远控号是否属于本地设备
	 * @param idx
	 * @return
	 */
	virtual bool isLocalRemoteCtl( const dm::msg::index_t& idx )const=0;

	/**
	 * 测试参数号是否属于本地设备
	 * @param idx
	 * @return
	 */
	virtual bool isLocalPara( const dm::msg::index_t& idx )const=0;

protected:
	virtual EAction taskEnd_call( const ts_t& ts,const rts_t& rts );
	virtual EAction taskEnd_syncClock( const ts_t& ts,const rts_t& rts );
	virtual EAction taskEnd_remoteControl( const ts_t& ts,const rts_t& rts );
	virtual EAction taskEnd_parameters( const ts_t& ts,const rts_t& rts );
	virtual EAction taskEnd_syncData( const ts_t& ts,const rts_t& rts );
	virtual EAction taskEnd_reset( const ts_t& ts,const rts_t& rts );

	virtual bool filterMsgRequest_syncClock( const dm::os::msg::CMsgInfo& msg);
	virtual bool filterMsgRequest_call( const dm::os::msg::CMsgInfo& msg);
	virtual bool filterMsgRequest_rmtCtl( const dm::os::msg::CMsgInfo& msg);
	virtual bool filterMsgRequest_set( const dm::os::msg::CMsgInfo& msg);
	virtual bool filterMsgRequest_get( const dm::os::msg::CMsgInfo& msg);
	virtual bool filterMsgRequest_update( const dm::os::msg::CMsgInfo& msg);
	virtual bool filterMsgRequest_syncData( const dm::os::msg::CMsgInfo& msg);
	virtual bool filterMsgRequest_remoteReset( const dm::os::msg::CMsgInfo& msg);

	virtual bool filterMsgAnswer_syncClock( const dm::os::msg::CMsgInfo& msg);
	virtual bool filterMsgAnswer_call( const dm::os::msg::CMsgInfo& msg);
	virtual bool filterMsgAnswer_rmtCtl( const dm::os::msg::CMsgInfo& msg);
	virtual bool filterMsgAnswer_set( const dm::os::msg::CMsgInfo& msg);
	virtual bool filterMsgAnswer_get( const dm::os::msg::CMsgInfo& msg);
	virtual bool filterMsgAnswer_update( const dm::os::msg::CMsgInfo& msg);
	virtual bool filterMsgAnswer_syncData( const dm::os::msg::CMsgInfo& msg);
	virtual bool filterMsgAnswer_remoteReset( const dm::os::msg::CMsgInfo& msg);

	virtual EAction onMsgRequest_syncClock( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts );
	virtual EAction onMsgRequest_call( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts );
	virtual EAction onMsgRequest_rmtCtl( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts );
	virtual EAction onMsgRequest_set( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts );
	virtual EAction onMsgRequest_get( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts );
	virtual EAction onMsgRequest_update( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts );
	virtual EAction onMsgRequest_syncData( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts );
	virtual EAction onMsgRequest_remoteReset( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts );

	virtual EAction onMsgAnswer_syncClock( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts );
	virtual EAction onMsgAnswer_call( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts );
	virtual EAction onMsgAnswer_rmtCtl( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts );
	virtual EAction onMsgAnswer_set( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts );
	virtual EAction onMsgAnswer_get( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts );
	virtual EAction onMsgAnswer_update( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts );
	virtual EAction onMsgAnswer_syncData( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts );
	virtual EAction onMsgAnswer_remoteReset( dm::os::msg::CMsgInfo& msg,const ts_t& ts,const rts_t& rts );
};

}
}
}

#endif /* DM_RUNTIME_INCLUDE_DM_OS_PROTOCOL_PROTOCOLMASTERBASE_HPP_ */

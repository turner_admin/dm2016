﻿/*
 * modbusrtu.hpp
 *
 *  Created on: 2017年3月1日
 *      Author: work
 */

#ifndef INCLUDE_DM_OS_PROTOCOL_MODBUSRTUSLAVER_HPP_
#define INCLUDE_DM_OS_PROTOCOL_MODBUSRTUSLAVER_HPP_

#include <dm/export.hpp>

#ifndef DM_API_OS_PROTOCOL
#define DM_API_OS_PROTOCOL DM_API_IMPORT
#endif

#include <dm/protocol/protocol.hpp>

namespace dm{
namespace os{
namespace protocol{

class DM_API_OS_PROTOCOL CModbusRtu:public dm::protocol::CProtocol{
public:
	CModbusRtu();
	virtual ~CModbusRtu();
};

}
}
}



#endif /* INCLUDE_DM_OS_PROTOCOL_MODBUSRTUSLAVER_HPP_ */

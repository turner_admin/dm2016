﻿/*
 * hisrcd_cumulant.hpp
 *
 *  Created on: 2017年7月10日
 *      Author: work
 */

#ifndef _DM_SCADA_HISRCD_CUMULANT_HPP_
#define _DM_SCADA_HISRCD_CUMULANT_HPP_

#include <dm/scada/hisrcd.hpp>
#include <dm/scada/cumulant.hpp>

namespace dm{
namespace scada{

/**
 * 历史累计量记录信息
 */
typedef THisRecord<CCumulant::value_t> SHisRcdCumulant;

}
}

#endif /* INCLUDE_DM_SCADA_HISRCD_CUMULANT_HPP_ */

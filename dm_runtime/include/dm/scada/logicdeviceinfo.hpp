﻿/*
 * logicdeviceinfo.hpp
 *
 *  Created on: 2017年11月23日
 *      Author: work
 */

#ifndef _DM_SCADA_LOGICDEVICEINFO_HPP_
#define _DM_SCADA_LOGICDEVICEINFO_HPP_

#include <dm/scada/types.hpp>

namespace dm{
namespace scada{

/**
 * 逻辑设备信息
 * 每个逻辑设备包含了各自的信号。
 * 这些信号可以被映射到实际物理信号上
 */
struct SLogicDeviceInfo{
	id_t id;	// ID
	name_t name;	// 名字
	desc_t desc;	// 描述

	index_t sizeOfStatus;	// 状态量数量
	index_t posOfStatus;	// 状态量偏移 

	index_t sizeOfDiscrete;
	index_t posOfDiscrete;

	index_t sizeOfMeasure;
	index_t posOfMeasure;

	index_t sizeOfCumulant;
	index_t posOfCumulant;

	index_t sizeOfRemoteCtl;
	index_t posOfRemoteCtl;

	index_t sizeOfParameter;
	index_t posOfParameter;

	index_t sizeOfAction;
	index_t posOfAction;
};

}
}

#endif /* DM_RUNTIME_INCLUDE_DM_SCADA_LOGICDEVICEINFO_HPP_ */

﻿/*
 * timedcumulant.hpp
 *
 *  Created on: 2017年4月11日
 *      Author: work
 */

#ifndef _DM_SCADA_TIMEDCUMULANT_HPP_
#define _DM_SCADA_TIMEDCUMULANT_HPP_

#include <dm/export.hpp>

#ifndef DM_API_SCADA
#define DM_API_SCADA DM_API_IMPORT
#endif

#include <dm/scada/types.hpp>
#include <dm/scada/cumulant.hpp>
#include <dm/timestamp.hpp>

namespace dm{
namespace scada{

class DM_API_SCADA CTimedCumulant{
public:
	typedef CCumulant::raw_t raw_t;
	typedef dm::CTimeStamp ts_t;

	CTimedCumulant( const index_t& idx=0,const raw_t& raw=0,const ts_t& ts=ts_t::cur() );
	CTimedCumulant( const CTimedCumulant& );
	CTimedCumulant& operator=( const CTimedCumulant& );

	inline const index_t& getIndex()const{
		return m_idx;
	}

	inline void setIndex( const index_t& idx ){
		m_idx = idx;
	}

	inline const raw_t& getRaw()const{
		return m_raw;
	}

	inline void setRaw( const raw_t& raw ){
		m_raw = raw;
	}

	inline const ts_t& getTimeStamp()const{
		return m_ts;
	}

	inline ts_t& getTimeStamp(){
		return m_ts;
	}

	inline void setTimeStamp( const ts_t& ts ){
		m_ts = ts;
	}

private:
	index_t m_idx;
	raw_t m_raw;
	ts_t m_ts;
};

}
}

#endif /* INCLUDE_DM_SCADA_TIMEDCUMULANT_HPP_ */

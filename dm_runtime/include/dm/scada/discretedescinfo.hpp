﻿/*
 * discretedescinfo.hpp
 *
 *  Created on: 2017年3月24日
 *      Author: work
 */

#ifndef _DM_SCADA_DISCRETEDESCINFO_HPP_
#define _DM_SCADA_DISCRETEDESCINFO_HPP_

#include <dm/scada/types.hpp>

namespace dm{
namespace scada{

/**
 * 离散量描述信息
 */
struct SDiscreteDescInfo{
	id_t id;			//!< id
	desc_t desc;	//!< 描述信息
};

}
}



#endif /* INCLUDE_DM_SCADA_DISCRETEDESCINFO_HPP_ */

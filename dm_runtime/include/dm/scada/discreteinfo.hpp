﻿/*
 * discreteinfo.hpp
 *
 *  Created on: 2017年3月24日
 *      Author: work
 */

#ifndef _DM_SCADA_DISCRETEINFO_HPP_
#define _DM_SCADA_DISCRETEINFO_HPP_

#include <dm/scada/types.hpp>
#include <dm/bits.hpp>

namespace dm{
namespace scada{

/**
 * 离散量信息
 */
struct SDiscreteInfo{
	/**
	 * 标志位
	 */
	enum EFlag{
		FUse = 0,		//!< 是否使用
		FSave,     		//!< 是否存盘
		FGenTimed,		//!< 是否产生时标数据
		FReportTimed,	//!< 是否上报时标数据

		FNone
	};

	inline bool isFlagSave()const{
		return dm::bit_get(flag,FSave);
	}

	inline void setFlagSave(){
		flag = dm::bit_set(flag,FSave);
	}

	inline void clrFlagSave(){
		flag = dm::bit_clr(flag,FSave);
	}

	inline void setFlagSave( const bool& s ){
		if( s )
			setFlagSave();
		else
			clrFlagSave();
	}

	inline bool isFlagGenTimed()const{
		return dm::bit_get(flag,FGenTimed);
	}

	inline void setFlagGenTimed(){
		flag = dm::bit_set(flag,FGenTimed);
	}

	inline void clrFlagGenTimed(){
		flag = dm::bit_clr(flag,FGenTimed);
	}

	inline void setFlagGenTimed( const bool& s ){
		if( s )
			setFlagGenTimed();
		else
			clrFlagGenTimed();
	}

	/**
	 * 是否上报时标数据
	 * @return
	 */
	inline bool isFlagReportTimed()const{
		return dm::bit_get(flag,FReportTimed);
	}

	/**
	 * 设置上报时标数据标志
	 */
	inline void setFlagReportTimed(){
		flag = dm::bit_set(flag,FReportTimed);
	}

	inline void clrFlagReportTimed(){
		flag = dm::bit_clr(flag,FReportTimed);
	}

	/**
	 * 设置上报时标数据标志
	 * @param s
	 */
	inline void setFlagReportTimed( const bool& s ){
		if( s )
			setFlagReportTimed();
		else
			clrFlagReportTimed();
	}

	id_t   id;		//!< id
	name_t name;	//!< 名字
	desc_t desc;	//!< 描述

	int desc_pos;	//!< 状态起始
	int desc_num;	//!< 状态数

	flag_t flag;	//!< 标志
};

}
}



#endif /* INCLUDE_DM_SCADA_DISCRETEINFO_HPP_ */

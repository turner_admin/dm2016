﻿/*
 * eventmgr.hpp
 *
 *  Created on: 2016年11月18日
 *      Author: work
 */

#ifndef INCLUDE_DM_SCADA_EVENTMGR_HPP_
#define INCLUDE_DM_SCADA_EVENTMGR_HPP_

#include <dm/export.hpp>

#ifndef DM_API_SCADA
#define DM_API_SCADA DM_API_IMPORT
#endif

#include <dm/scada/scada.hpp>

namespace dm{
namespace scada{

/**
 * 事件管理器
 */
class DM_API_SCADA CEventMgr:public CScada{
	CEventMgr( CScada& scada );
public:
	static CEventMgr& ins();

	/**
	 * 产生事件到事件队列中去
	 * @param e
	 */
	inline void gen( const CEvent& e ){
		m_q->push(e);
	}

protected:
	event_queue_t* m_q;
};

}
}

#endif /* INCLUDE_DM_SCADA_EVENTMGR_HPP_ */

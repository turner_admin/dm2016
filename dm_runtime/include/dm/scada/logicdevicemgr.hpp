﻿/*
 * logicdevicemgr.hpp
 *
 *  Created on: 2017年12月3日
 *      Author: work
 */

#ifndef _DM_SCADA_LOGICDEVICEMGR_HPP_
#define _DM_SCADA_LOGICDEVICEMGR_HPP_

#include <dm/export.hpp>

#ifndef DM_API_SCADA
#define DM_API_SCADA DM_API_IMPORT
#endif

#include <dm/scada/scada.hpp>
#include <dm/scada/logicdeviceinfo.hpp>
#include <dm/scada/cfg.hpp>

#include <iostream>

namespace dm{
namespace scada{

/**
 * 逻辑设备管理器
 */
class DM_API_SCADA CLogicDeviceMgr:protected CScada{
	CLogicDeviceMgr( CScada& scada );
public:
	static CLogicDeviceMgr& ins();

	inline index_t size()const{
		return m_cfg.size();
	}

	inline index_t indexById( const id_t& id )const{
		return m_cfg.indexOf(id);
	}

	inline index_t indexByName( const char* name )const{
		return m_cfg.indexOf(name);
	}

	inline index_t indexOfInfo( const SLogicDeviceInfo* info )const{
		return m_cfg.indexOf(info);
	}

	inline const SLogicDeviceInfo* info( const index_t& idx )const{
		return m_cfg.at(idx);
	}

	inline id_t id( const index_t& idx )const{
		const SLogicDeviceInfo* p = info(idx);
		return p?p->id:Id_Inv;
	}

	inline const char* name( const index_t& idx )const{
		const SLogicDeviceInfo* p = info(idx);
		return p?p->name.c_str():NULL;
	}

	inline const char* desc( const index_t& idx )const{
		const SLogicDeviceInfo* p = info(idx);
		return p?p->desc.c_str():"无效索引";
	}

	bool setDesc( const index_t& idx,const char* str );

	void print( const index_t& idx,std::ostream& ostr=std::cout )const;

private:
	TCfg<SLogicDeviceInfo> m_cfg;
};

}
}

#endif /* DM_RUNTIME_INCLUDE_DM_SCADA_LOGICDEVICEMGR_HPP_ */

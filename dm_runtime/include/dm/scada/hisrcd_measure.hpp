﻿/*
 * hisrcd_measure.hpp
 *
 *  Created on: 2017年7月10日
 *      Author: work
 */

#ifndef _DM_SCADA_HISRCD_MEASURE_HPP_
#define _DM_SCADA_HISRCD_MEASURE_HPP_

#include <dm/scada/hisrcd.hpp>
#include <dm/scada/measure.hpp>

namespace dm{
namespace scada{

/**
 * 历史测量量记录信息
 */
typedef THisRecord<CMeasure::value_t> SHisRcdMeasure;

}
}

#endif /* INCLUDE_DM_SCADA_HISRCD_MEASURE_HPP_ */

﻿/*
 * actiondescmgr.hpp
 *
 *  Created on: 2017年11月30日
 *      Author: work
 */

#ifndef _DM_SCADA_ACTIONDESCMGR_HPP_
#define _DM_SCADA_ACTIONDESCMGR_HPP_

#include <dm/export.hpp>

#ifndef DM_API_SCADA
#define DM_API_SCADA DM_API_IMPORT
#endif

#include <dm/scada/scada.hpp>
#include <dm/scada/actiondescinfo.hpp>
#include <dm/scada/cfg.hpp>
#include <iostream>

namespace dm{
namespace scada{

/**
 * 动作描述管理器
 * 封装了对所有动作描述的访问接口。
 */
class DM_API_SCADA CActionDescMgr:protected CScada{
protected:
	CActionDescMgr( CScada& scada );
public:
	static CActionDescMgr& ins();

	/**
	 * 所有描述的数量
	 * @return
	 */
	inline int size()const{
		return m_cfg.size();
	}

	/**
	 * 获取指定id的描述索引
	 * @param id
	 * @return
	 */
	inline index_t indexById( const id_t& id )const{
		return m_cfg.indexOf(id);
	}

	/**
	 * 获取指定描述信息的索引
	 * @param info
	 * @return
	 */
	inline index_t indexOfInfo( const SActionDescInfo* info )const{
		return m_cfg.indexOf(info);
	}

	/**
	 * 获取描述信息
	 * @param idx
	 * @return
	 */
	inline const SActionDescInfo* info( const index_t& idx )const{
		return m_cfg.at(idx);
	}

	/**
	 * 获取id
	 * @param idx
	 * @return
	 */
	inline id_t id( const index_t& idx )const{
		const SActionDescInfo* p = info(idx);
		return p?p->id:Id_Inv;
	}

	/**
	 * 获取描述字符串
	 * @param idx
	 * @return
	 */
	inline const char* desc( const index_t& idx )const{
		const SActionDescInfo* p = info(idx);
		return p?p->desc.c_str():"无效索引";
	}

	bool setDesc( const index_t& idx,const char* str );
private:
	TCfg<SActionDescInfo> m_cfg;
};

}
}

#endif /* _DM_SCADA_ACTIONDESCMGR_HPP_ */

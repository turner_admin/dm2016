﻿/*
 * actionmgr.hpp
 *
 *  Created on: 2017年11月30日
 *      Author: work
 */

#ifndef _DM_SCADA_ACTIONMGR_HPP_
#define _DM_SCADA_ACTIONMGR_HPP_

#include <dm/export.hpp>

#ifndef DM_API_SCADA
#define DM_API_SCADA DM_API_IMPORT
#endif

#include <dm/scada/scada.hpp>

#include <dm/scada/actioninfo.hpp>
#include <dm/scada/actiondescmgr.hpp>

#include <iostream>

namespace dm{
namespace scada{

/**
 * 动作管理器类
 * 封装了对系统内动作配置信息的访问接口。
 */
class DM_API_SCADA CActionMgr:protected CScada{
	CActionMgr( CScada& scada );
public:
	static CActionMgr& ins();

	/**
	 * 系统内配置的动作信息个数。
	 * @return
	 */
	inline const int& size()const{
		return m_cfg.size();
	}

	/**
	 * 通过id查找动作信息配置索引
	 * @param id
	 * @return
	 * - Idx_Inv 未找到
	 */
	inline index_t indexById( const id_t& id )const{
		return m_cfg.indexOf(id);
	}

	/**
	 * 通过名字查找动作信息配置索引
	 * @param name
	 * @return
	 * - Idx_Inv 未找到
	 */
	inline index_t indexByName( const char* name )const{
		return m_cfg.indexOf(name);
	}

	/**
	 * 获取动作信息的索引
	 * @param info
	 * @return
	 * - Idx_Inv 未找到
	 */
	inline index_t indexOfInfo( const SActionInfo* info )const{
		return m_cfg.indexOf(info);
	}

	/**
	 * 获取动作信息
	 * @param idx 指定索引
	 * @return
	 */
	inline const SActionInfo* info( const index_t& idx )const{
		return m_cfg.at(idx);
	}

	/**
	 * 获取动作信息的id
	 * @param idx 指定索引
	 * @return
	 */
	inline id_t id( const index_t& idx )const{
		const SActionInfo* p = info(idx);
		return p?p->id:Id_Inv;
	}

	/**
	 * 获取动作信息的名字
	 * @param idx 指定索引
	 * @return
	 */
	inline const char* name( const index_t& idx )const{
		const SActionInfo* p = info(idx);
		return p?p->name.c_str():NULL;
	}

	/**
	 * 获取动作信息的描述
	 * @param idx 指定索引
	 * @return
	 */
	inline const char* desc( const index_t& idx )const{
		const SActionInfo* p = info(idx);
		return p?p->desc.c_str():"无效索引";
	}

	bool setDesc( const index_t& idx,const char* str );

	/**
	 * 获取动作信息的合描述
	 * @param idx 指定索引
	 * @return
	 */
	inline const SActionDescInfo* onInfo( const index_t& idx )const{
		const SActionInfo* p = info(idx);
		return p?CActionDescMgr::ins().info(p->desc_on):NULL;
	}

	/**
	 * 获取动作信息的分描述
	 * @param idx 指定索引
	 * @return
	 */
	inline const SActionDescInfo* offInfo( const index_t& idx )const{
		const SActionInfo* p = info(idx);
		return p?CActionDescMgr::ins().info(p->desc_off):NULL;
	}

	/**
	 * 获取动作信息的无效合描述
	 * @param idx 指定索引
	 * @return
	 */
	inline const SActionDescInfo* invOnInfo( const index_t& idx )const{
		const SActionInfo* p = info(idx);
		return p?CActionDescMgr::ins().info(p->desc_invOn):NULL;
	}

	/**
	 * 获取动作信息的无效分
	 * @param idx 指定索引
	 * @return
	 */
	inline const SActionDescInfo* invOffInfo( const index_t& idx )const{
		const SActionInfo* p = info(idx);
		return p?CActionDescMgr::ins().info(p->desc_invOff):NULL;
	}

	/**
	 * 获取动作信息的合描述的id
	 * @param idx 指定索引
	 * @return
	 */
	inline id_t onId( const index_t& idx )const{
		const SActionInfo* p = info(idx);
		return p?CActionDescMgr::ins().id(p->desc_on):Id_Inv;
	}

	/**
	 * 获取动作信息的分描述的id
	 * @param idx 指定索引
	 * @return
	 */
	inline id_t offId( const index_t& idx )const{
		const SActionInfo* p = info(idx);
		return p?CActionDescMgr::ins().id(p->desc_off):Id_Inv;
	}

	/**
	 * 获取动作信息的无效合描述的id
	 * @param idx 指定索引
	 * @return
	 */
	inline id_t invOnId( const index_t& idx )const{
		const SActionInfo* p = info(idx);
		return p?CActionDescMgr::ins().id(p->desc_invOn):Id_Inv;
	}

	/**
	 * 获取动作信息的无效分描述的id
	 * @param idx 指定索引
	 * @return
	 */
	inline id_t invOffId( const index_t& idx )const{
		const SActionInfo* p = info(idx);
		return p?CActionDescMgr::ins().id(p->desc_invOff):Id_Inv;
	}

	/**
	 * 获取动作信息的合描述的字符信息
	 * @param idx 指定索引
	 * @return
	 */
	inline const char* onDesc( const index_t& idx )const{
		const SActionInfo* p = info(idx);
		return p?CActionDescMgr::ins().desc(p->desc_on):"无效配置";
	}

	/**
	 * 获取动作信息的分描述的字符信息
	 * @param idx 指定索引
	 * @return
	 */
	inline const char* offDesc( const index_t& idx )const{
		const SActionInfo* p = info(idx);
		return p?CActionDescMgr::ins().desc(p->desc_off):"无效配置";
	}

	/**
	 * 获取动作信息的无效合描述的字符信息
	 * @param idx 指定索引
	 * @return
	 */
	inline const char* invOnDesc( const index_t& idx )const{
		const SActionInfo* p = info(idx);
		return p?CActionDescMgr::ins().desc(p->desc_invOn):"无效配置";
	}

	/**
	 * 获取动作信息的无效分描述的字符信息
	 * @param idx 指定索引
	 * @return
	 */
	inline const char* invOffDesc( const index_t& idx )const{
		const SActionInfo* p = info(idx);
		return p?CActionDescMgr::ins().desc(p->desc_invOff):"无效配置";
	}

    inline SActionInfo::ELevel level( const index_t& idx )const{
        const SActionInfo* p = info(idx);
        return p?p->level:SActionInfo::ELevel(-1);
    }

    bool setLevel( const index_t& idx,SActionInfo::ELevel level );

    bool setFlagSave( const index_t& idx,bool s=true );
    bool setFlagReportTimed( const index_t& idx,bool s=true );

	const char* valueDesc( const index_t& idx,const CAction::value_t& value )const;

	index_t deviceIndex( const index_t& idx )const;
	const char* deviceDesc( const index_t& idx )const;

	bool update( const index_t& idx,const CAction::value_t& v,const dm::CTimeStamp& ts=dm::CTimeStamp::cur() );

	void printInfo( const index_t& idx,std::ostream& ostr=std::cout )const;

protected:
	const SActionDescInfo* descInfo( const index_t& idx )const;

private:
	TCfg<SActionInfo> m_cfg;
};

}
}



#endif /* _DM_SCADA_ACTIONMGR_HPP_ */

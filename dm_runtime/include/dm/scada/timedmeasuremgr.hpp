﻿/*
 * timedmeasuremgr.hpp
 *
 *  Created on: 2016年11月26日
 *      Author: work
 */

#ifndef _DM_SCADA_TIMEDMEASUREMGR_HPP_
#define _DM_SCADA_TIMEDMEASUREMGR_HPP_

#include <dm/export.hpp>

#ifndef DM_API_SCADA
#define DM_API_SCADA DM_API_IMPORT
#endif

#include <dm/scada/scada.hpp>

namespace dm{
namespace scada{

class DM_API_SCADA CTimedMeasureMonitor;

class DM_API_SCADA CTimedMeasureMgr:public CScada{
	CTimedMeasureMgr( CScada& scada );
public:
	static CTimedMeasureMgr& ins();

	void push( const CTimedMeasure& tm ){
		m_q->push(tm);
	}

protected:
	friend class CTimedMeasureMonitor;

	measure_queue_t* m_q;
};

}
}



#endif /* INCLUDE_DM_SCADA_TIMEDMEASUREMGR_HPP_ */

﻿/*
 * rts.hpp
 *
 *  Created on: 2018年7月5日
 *      Author: turner
 */

#ifndef _DM_SCADA_RTS_HPP_
#define _DM_SCADA_RTS_HPP_

#include <dm/scada/statusmgr.hpp>
#include <dm/scada/discretemgr.hpp>
#include <dm/scada/measuremgr.hpp>
#include <dm/scada/cumulantmgr.hpp>
#include <dm/scada/remotectlmgr.hpp>
#include <dm/scada/parametermgr.hpp>

namespace dm{
namespace scada{

typedef dm::scada::CStatusMgr::rt_t rt_s_t;
typedef dm::scada::CDiscreteMgr::rt_t rt_d_t;
typedef dm::scada::CMeasureMgr::rt_t rt_m_t;
typedef dm::scada::CCumulantMgr::rt_t rt_c_t;

typedef dm::scada::CStatus::value_t s_t;
typedef dm::scada::CDiscrete::value_t d_t;
typedef dm::scada::CMeasure::value_t m_t;
typedef dm::scada::CCumulant::value_t c_t;

/**
 * 计算真实索引号
 * @param base
 * @param p
 * @return
 */
inline index_t rid( const index_t& base,int p ){
	return base==-1?-1:(base+p);
}

template<typename Trt>
inline const Trt* rtp( const Trt* base,int p ){
	return base==NULL?NULL:base+p;
}

template<typename Tv,typename Trt>
inline Tv rtv( const Trt* r ){
	return r==NULL?0:r->getValue().getValue();
}


inline s_t sv( const rt_s_t* r ){
	return rtv<s_t,rt_s_t>(r);
}

inline d_t dv( const rt_d_t* r ){
	return rtv<d_t,rt_d_t>(r);
}

inline m_t mv( const rt_m_t* r ){
	return rtv<m_t,rt_m_t>(r);
}

inline c_t cv( const rt_c_t* r ){
	return rtv<c_t,rt_c_t>(r);
}

}
}

#endif /* DM_RUNTIME_INCLUDE_DM_SCADA_RTS_HPP_ */

﻿/*
 * parainfo.hpp
 *
 *  Created on: 2017年3月24日
 *      Author: work
 */

#ifndef _DM_SCADA_PARAINFO_HPP_
#define _DM_SCADA_PARAINFO_HPP_

#include <dm/scada/types.hpp>

namespace dm{
namespace scada{

struct SParaInfo{
	enum EType{
		Int,
		Float,
		String,
		Binary
	};

	id_t id;
	name_t name;
	desc_t desc;
	EType type;
};

}
}

#endif /* INCLUDE_DM_SCADA_PARAINFO_HPP_ */

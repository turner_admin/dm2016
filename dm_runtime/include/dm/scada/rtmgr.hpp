﻿/*
 * rtmgr.hpp
 *
 *  Created on: 2017年3月27日
 *      Author: work
 */

#ifndef _DM_SCADA_RTMGR_HPP_
#define _DM_SCADA_RTMGR_HPP_

#include <dm/scada/rtvalue.hpp>

namespace dm{
namespace scada{

template<typename T,typename TCfg>
class TCRtMgr{
	TCRtMgr( const TCRtMgr& );
	TCRtMgr& operator=( const TCRtMgr& );
public:
	typedef TCRtValue<T> rt_t;

	TCRtMgr( rt_t* data,const TCfg* cfg ):m_cfg(cfg),m_d(data){
	}

	inline const int& size()const{
		return m_cfg->size();
	}

	inline rt_t* at( const int& idx ){
		if( idx<0 || idx>=m_cfg->size() )
			return NULL;
		return m_d + idx;
	}

	inline const rt_t* at( const int& idx )const{
		if( idx<0 || idx>=m_cfg->size() )
			return NULL;
		return m_d + idx;
	}

	inline rt_t& operator[]( const index_t& i ){
		return m_d[i];
	}

	inline const rt_t& operator[](const index_t& i )const{
		return m_d[i];
	}

	inline const rt_t* byId( const int& id )const{
		int idx = m_cfg->indexOf(id);
		return idx<0?NULL:m_d+idx;
	}

	inline rt_t* byId( const int& id ){
		int idx = m_cfg->indexOf(id);
		return idx<0?NULL:m_d+idx;
	}

	inline const rt_t* byName( const char* name )const{
		int idx = m_cfg->indexOf(name);
		return idx<0?NULL:m_d+idx;
	}

	inline rt_t* byName( const char* name ){
		int idx = m_cfg->indexOf(name);
		return idx<0?NULL:m_d+idx;
	}

	inline index_t indexOf( const rt_t* p )const{
		if( p<m_d )
			return -1;
		return p - m_d;
	}

	inline index_t indexOf( volatile const rt_t* p )const{
		if( p<m_d )
			return -1;
		return p - m_d;
	}

	inline int id( const int& idx )const{
		return (*m_cfg)[idx].id;
	}

	inline const char* name( const int& idx )const{
		return (*m_cfg)[idx].name.c_str();
	}

	inline const char* desc( const int& idx )const{
		return (*m_cfg)[idx].desc.c_str();
	}

	inline const TCfg* cfg()const{
		return m_cfg;
	}

private:
	const TCfg* m_cfg;
	rt_t* m_d;
};

}
}

#endif /* INCLUDE_DM_SCADA_RTMGR_HPP_ */

﻿/*
 * timedcumulantmgr.hpp
 *
 *  Created on: 2017年4月11日
 *      Author: work
 */

#ifndef _DM_SCADA_TIMEDCUMULANTMGR_HPP_
#define _DM_SCADA_TIMEDCUMULANTMGR_HPP_

#include <dm/export.hpp>

#ifndef DM_API_SCADA
#define DM_API_SCADA DM_API_IMPORT
#endif

#include <dm/scada/scada.hpp>

namespace dm{
namespace scada{

class DM_API_SCADA CTimedCumulantMonitor;

class DM_API_SCADA CTimedCumulantMgr:public CScada{
	CTimedCumulantMgr( CScada& scada );
public:
	static CTimedCumulantMgr& ins();

	void push( const CTimedCumulant& tm ){
		m_q->push(tm);
	}

protected:
	friend class CTimedCumulantMonitor;

	cumulant_queue_t* m_q;
};

}
}



#endif /* INCLUDE_DM_SCADA_TIMEDCUMULANTMGR_HPP_ */

﻿/*
 * measure.hpp
 *
 *  Created on: 2017年3月27日
 *      Author: work
 */

#ifndef _DM_SCADA_MEASURE_HPP_
#define _DM_SCADA_MEASURE_HPP_

#include <dm/export.hpp>

#ifndef DM_API_SCADA
#define DM_API_SCADA DM_API_IMPORT
#endif

#include <dm/types.hpp>

namespace dm{
namespace scada{

class DM_API_SCADA CMeasure{
public:
	typedef dm::float32 value_t;
	typedef dm::float32 raw_t;

	CMeasure( const value_t& v=0,const raw_t& r=0 ):m_v(v),m_r(r){
	}

	CMeasure( const CMeasure& m ):m_v(m.m_v),m_r(m.m_r){
	}

	CMeasure( const volatile CMeasure& m ):m_v(m.m_v),m_r(m.m_r){
	}

	CMeasure& operator=( const CMeasure& m ){
		m_v = m.m_v;
		m_r = m.m_r;
		return *this;
	}

	CMeasure& operator=( volatile const CMeasure& m ){
		m_v = m.m_v;
		m_r = m.m_r;
		return *this;
	}

	inline bool operator==( const CMeasure& m )const{
		return m_v==m.m_v;
	}

	inline bool operator==( volatile const CMeasure& m )const{
		return m_v==m.m_v;
	}

	inline const value_t& getValue()const{
		return m_v;
	}

	inline const raw_t& getRaw()const{
		return m_r;
	}

	inline void setValue( const value_t& v ){
		m_v = v;
	}

	inline void setRaw( const raw_t& r ){
		m_r = r;
	}

	bool update( const CMeasure& m );
	bool update( const CMeasure& m )volatile;

private:
	value_t m_v;
	raw_t m_r;
};

}
}

#endif /* INCLUDE_DM_SCADA_MEASURE_HPP_ */

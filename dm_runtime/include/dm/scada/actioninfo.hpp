﻿/*
 * actioninfo.hpp
 *
 *  Created on: 2017年11月21日
 *      Author: work
 */

#ifndef _DM_SCADA_ACTIONINFO_HPP_
#define _DM_SCADA_ACTIONINFO_HPP_

#include <dm/scada/types.hpp>
#include <dm/bits.hpp>

namespace dm{
namespace scada{

/**
 * 动作信息结构体。
 * 用于描述动作的配置信息。
 */
struct SActionInfo{
	/**
	 * 标志位
	 */
	enum EFlag{
		FUse = 0,//!< 是否使用
		FSave,   //!< 是否存盘
		FReportTimed,	//!< 是否上报时标数据
		FNone
	};

	inline bool isFlagSave()const{
		return dm::bit_get(flag,FSave);
	}

	inline void setFlagSave(){
		flag = dm::bit_set(flag,FSave);
	}

	inline void clrFlagSave(){
		flag = dm::bit_clr(flag,FSave);
	}

	inline void setFlagSave( const bool& s ){
		if( s )
			setFlagSave();
		else
			clrFlagSave();
	}

	/**
	 * 是否上报时标数据
	 * @return
	 */
	inline bool isFlagReportTimed()const{
		return dm::bit_get(flag,FReportTimed);
	}

	/**
	 * 设置上报时标数据标志
	 */
	inline void setFlagReportTimed(){
		flag = dm::bit_set(flag,FReportTimed);
	}

	inline void clrFlagReportTimed(){
		flag = dm::bit_clr(flag,FReportTimed);
	}

	/**
	 * 设置上报时标数据标志
	 * @param s
	 */
	inline void setFlagReportTimed( const bool& s ){
		if( s )
			setFlagReportTimed();
		else
			clrFlagReportTimed();
	}

	/**
	 * 信息级别
	 */
	enum ELevel{
		Normal = 0,//!< 普通动作
		Info,      //!< 提示级。该级别动作只需显示即可
		Warn,      //!< 警告级。该级动作说明有警告
		Exception, //!< 异常级。该级动作说明程序有异常
		Error,     //!< 错误级。该级动作说明有系统级别的错误
		Danguer,   //!< 危险级。该级动作说明有系统或严重危险
		LevelMax   //!< 最高级，一般不用
	};

	id_t id;					//!< id
	name_t name;			//!< 名字。系统唯一
	desc_t desc;			//!< 描述。用已提示给用户看的信息

	index_t desc_invOff;	//!< 无效分描述索引
	index_t desc_off;		//!< 分描述索引
	index_t desc_on;		//!< 合描述索引
	index_t desc_invOn;	//!< 无效合描述索引

	ELevel level;			//!< 动作级别

	flag_t flag;			//!< 标志
};

}
}

#endif /* DM_RUNTIME_INCLUDE_DM_SCADA_ACTIONINFO_HPP_ */

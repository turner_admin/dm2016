﻿/*
 * js.hpp
 *
 *  Created on: 2018-12-6
 *      Author: work
 */

#ifndef _DM_SCADA_JSIF_HPP_
#define _DM_SCADA_JSIF_HPP_

#include <dm/export.hpp>

#ifndef DM_API_SCADA
#define DM_API_SCADA DM_API_IMPORT
#endif

namespace dm{
namespace scada{

class DM_API_SCADA CJsDeviceIf{
public:
	enum EColumn{
		CIdx,
		CId,
		CName,
		CDesc,

		CParent,
		CSSize,
		CSPos,
		CDSize,
		CDPos,
		CMSize,
		CMPos,
		CCSize,
		CCPos,
		CRSize,
		CRPos,
		CPSize,
		CPPos,
		CASize,
		CAPos,

		COnline,
		CState,
		CTs,
		CRxCnt,
		CRxStart,
		CRxTs,
		CTxCnt,
		CTxStart,
		CTxTs,

		CMax
	};

	static const char* string( int c );
	static int index( const char * c );
};

class CJsStatusIf{
public:
	enum EColumn{
		CIdx,
		CId,
		CName,
		CDesc,

		CFlagSave,
		CFlagGen,
		CFlagRpt,

		CInvOff,
		COff,
		COn,
		CInvOn,
		CLevel,

		CValue,
		CRaw,
		CDs,
		CTs,
		CValueDesc,

		CDevice,
		CDeviceDesc,

		CMax
	};

	static const char* string( int c );
	static int index( const char * c );
};

class CJsDiscreteIf{
public:
	enum EColumn{
		CIdx,
		CId,
		CName,
		CDesc,

		CFlagSave,
		CFlagGen,
		CFlagRpt,

		CDescs,

		CValue,
		CRaw,
		CDs,
		CTs,
		CValueDesc,

		CDevice,
		CDeviceDesc,

		CMax
	};

	static const char* string( int c );
	static int index( const char * c );
};

class CJsMeasureIf{
public:
	enum EColumn{
		CIdx,
		CId,
		CName,
		CDesc,

		CFlagSave,
		CFlagGen,
		CFlagRpt,

		CDelta,
		CBase,
		CCoef,
		CUnit,

		CValue,
		CRaw,
		CDs,
		CTs,

		CDevice,
		CDeviceDesc,

		CMax
	};

	static const char* string( int c );
	static int index( const char * c );
};

class CJsCumulantIf{
public:
	enum EColumn{
		CIdx,
		CId,
		CName,
		CDesc,

		CFlagSave,
		CFlagGen,
		CFlagRpt,

		CDelta,
		CMaxCode,
		CCoef,

		CValue,
		CRaw,
		CDs,
		CTs,

		CDevice,
		CDeviceDesc,

		CMax
	};

	static const char* string( int c );
	static int index( const char * c );
};

class CJsRmtctlIf{
public:
	enum EColumn{
		CIdx,
		CId,
		CName,
		CDesc,

		CPreOpen,
		COpen,
		CClose,
		CPreClose,

		CDevice,
		CDeviceDesc,

		CMax
	};

	static const char* string( int c );
	static int index( const char * c );
};

class CJsParamIf{
public:
	enum EColumn{
		CIdx,
		CId,
		CName,
		CDesc,

		CType,

		CDevice,
		CDeviceDesc,

		CMax
	};

	static const char* string( int c );
	static int index( const char * c );
};

class CJsActionIf{
public:
	enum EColumn{
		CIdx,
		CId,
		CName,
		CDesc,

		CFlagSave,
		CFlagRpt,

		CInvOff,
		COff,
		COn,
		CInvOn,
		CLevel,

		CDevice,
		CDeviceDesc,

		CMax
	};

	static const char* string( int c );
	static int index( const char * c );
};

}
}

#endif /* INCLUDE_DM_SCADA_JS_HPP_ */

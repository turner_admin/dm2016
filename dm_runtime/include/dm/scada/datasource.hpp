﻿/*
 * datasource.hpp
 *
 *  Created on: 2017年1月23日
 *      Author: work
 */

#ifndef INCLUDE_DM_SCADA_DATASOURCE_HPP_
#define INCLUDE_DM_SCADA_DATASOURCE_HPP_

#include <dm/types.hpp>

namespace dm{
namespace scada{

/**
 * 数据更新原因
 */
enum EDataSource{
	BySys = 0,		//!< 系统初始化
	ByCollector,	//!< 采集的数据
	ByCalculation,	//!< 计算的数据
	ByUser,			//!< 人工置数
	ByTest,			//!< 测试数据
	BySync,			//!< 外部系统同步过来的数据
	BySyncAdjust,	//!< 外部系统同步过来并校正时间的数据
	ByUnknow			//!< 未知
};

}
}

#endif /* INCLUDE_DM_SCADA_DATASOURCE_HPP_ */

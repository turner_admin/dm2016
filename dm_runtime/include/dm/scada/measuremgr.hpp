﻿/*
 * measuremgr.hpp
 *
 *  Created on: 2017年3月27日
 *      Author: work
 */

#ifndef _DM_SCADA_MEASUREMGR_HPP_
#define _DM_SCADA_MEASUREMGR_HPP_

#include <dm/export.hpp>

#ifndef DM_API_SCADA
#define DM_API_SCADA DM_API_IMPORT
#endif

#include <dm/scada/scada.hpp>

#include <dm/scada/measureinfo.hpp>
#include <dm/scada/timedmeasuremgr.hpp>
#include <dm/scada/cfg.hpp>

#include <iostream>

namespace dm{
namespace scada{

class DM_API_SCADA CMeasureMgr:protected CScada{
	CMeasureMgr( CScada& scada );
public:
	typedef CScada::measure_t rt_t;

	static CMeasureMgr& ins();

	inline const index_t& size()const{
		return m_cfg.size();
	}

	inline index_t indexById( const id_t& id )const{
		return m_cfg.indexOf(id);
	}

	inline index_t indexByName( const char* name )const{
		return m_cfg.indexOf(name);
	}

	inline index_t indexOfInfo( const SMeasureInfo* info )const{
		return m_cfg.indexOf(info);
	}

	index_t indexOfRt( const rt_t* value )const;
	index_t indexOfRt( const volatile rt_t* value )const;

	inline const SMeasureInfo* info( const index_t& idx )const{
		return m_cfg.at(idx);
	}

	inline id_t id( const index_t& idx )const{
		const SMeasureInfo* p = info(idx);
		return p?p->id:Id_Inv;
	}

	inline const char* name( const index_t& idx )const{
		const SMeasureInfo* p = info(idx);
		return p?p->name.c_str():NULL;
	}

	inline const char* desc( const index_t& idx )const{
		const SMeasureInfo* p = info(idx);
		return p?p->desc.c_str():"无效索引";
	}

	bool setDesc( const index_t& idx,const char* str );

	inline SMeasureInfo::base_t base( const index_t& idx )const{
		const SMeasureInfo* p = info(idx);
		return SMeasureInfo::base_t(p?p->base:0.0);
	}

	bool setBase( const index_t& idx,SMeasureInfo::base_t base );

	inline SMeasureInfo::coef_t coef( const index_t& idx )const{
		const SMeasureInfo* p = info(idx);
		return p?p->coef: SMeasureInfo::coef_t(0.0);
	}

	bool setCoef( const index_t& idx,SMeasureInfo::coef_t coef );

	inline SMeasureInfo::delta_t delta( const index_t& idx )const{
		const SMeasureInfo* p = info(idx);
		return p?p->delta: SMeasureInfo::delta_t(0.0);
	}

	bool setDelta( const index_t& idx,SMeasureInfo::delta_t delta );

	inline const char* unit( const index_t& idx )const{
		const SMeasureInfo* p = info(idx);
		return p?p->unit.c_str():"无效索引";
	}

	bool setUnit( const index_t& idx,const char* unit );

	bool setFlagSave( const index_t& idx,bool s=true );
	bool setFlagGenTimed( const index_t& idx,bool s=true );
	bool setFlagReportTimed( const index_t& idx,bool s=true );

	index_t deviceIndex( const index_t& index )const;
	const char* deviceDesc( const index_t& index )const;

	inline const rt_t* rt( const index_t& idx )const{
		return (idx<0||idx>=size())?NULL:measures()+idx;
	}

	inline rt_t* rt( const index_t& idx ){
		return (idx<0||idx>=size())?NULL:measures()+idx;
	}

	inline CMeasure::value_t rtValue( const index_t& idx ){
		rt_t* p = rt(idx);
		return p?p->getValue().getValue(): CMeasure::value_t(0.0);
	}

	inline CMeasure::raw_t rtRaw( const index_t& idx ){
		rt_t* p = rt(idx);
		return p?p->getValue().getRaw(): CMeasure::raw_t(0.0);
	}

	inline EDataSource rtDs( const index_t& idx ){
		rt_t* p = rt(idx);
		return p?p->getDataSource():ByUnknow;
	}

	inline dm::CTimeStamp rtTs( const index_t& idx ){
		rt_t* p = rt(idx);
		return p?p->getTimeStamp():dm::CTimeStamp();
	}

	CMeasure::value_t calcValue( const index_t& idx,const CMeasure::raw_t& raw )const;

	bool update( const index_t& idx,const CMeasure::raw_t& raw,const EDataSource& ds,const dm::CTimeStamp& ts=dm::CTimeStamp::cur() );
	bool set( const index_t& idx,const CMeasure::raw_t& raw,const EDataSource& ds,const dm::CTimeStamp& ts=dm::CTimeStamp::cur() );
	bool gen( const index_t& idx,const CMeasure::raw_t& raw,const dm::CTimeStamp& ts=dm::CTimeStamp::cur() );

	void printInfo( const index_t& idx,std::ostream& ostr=std::cout )const;
	void printInfoValue( const index_t& idx,std::ostream& ostr=std::cout );

private:
	TCfg<SMeasureInfo> m_cfg;
};

}
}

#endif /* INCLUDE_DM_SCADA_MEASUREMGR_HPP_ */

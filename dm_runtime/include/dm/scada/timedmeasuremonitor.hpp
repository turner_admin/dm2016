﻿/*
 * timedmeasuremonitor.hpp
 *
 *  Created on: 2017年4月14日
 *      Author: work
 */

#ifndef _DM_SCADA_TIMEDMEASUREMONITOR_HPP_
#define _DM_SCADA_TIMEDMEASUREMONITOR_HPP_

#include <dm/export.hpp>

#ifndef DM_API_SCADA
#define DM_API_SCADA DM_API_IMPORT
#endif

#include <dm/scada/timedmeasuremgr.hpp>

namespace dm{
namespace scada{

class DM_API_SCADA CTimedMeasure;

class DM_API_SCADA CTimedMeasureMonitor{
public:
	typedef CScada::measure_queue_t::ringpos_t ringpos_t;

	CTimedMeasureMonitor();
	CTimedMeasureMonitor( const ringpos_t* p );
	virtual ~CTimedMeasureMonitor();

	virtual void onNew( const ringpos_t& pos,const CTimedMeasure& info,const bool& overflow )const;
	virtual bool filter( const ringpos_t& pos,const CTimedMeasure& info,const bool& overflow )const;

	void run();

	bool tryGet( CTimedMeasure& info,bool& overflow );
	void get( CTimedMeasure& info,bool& overflow );

	void reset();
	inline const ringpos_t& pos()const{
		return m_p;
	}

private:
	CTimedMeasureMgr& m_mgr;
	ringpos_t m_p;
};

}
}

#endif /* INCLUDE_DM_SCADA_TIMEDMEASUREMONITOR_HPP_ */

﻿/*
 * cumulant.hpp
 *
 *  Created on: 2017年3月27日
 *      Author: work
 */

#ifndef _DM_SCADA_CUMULANT_HPP_
#define _DM_SCADA_CUMULANT_HPP_

#include <dm/export.hpp>

#ifndef DM_API_SCADA
#define DM_API_SCADA DM_API_IMPORT
#endif

#include <dm/types.hpp>

namespace dm{
namespace scada{

/**
 * 累计量
 * 累计量代表量一类基于表递增记录的物理量。该类量具有满码的时候，满码后，会自动归0，重新计数。
 */
class DM_API_SCADA CCumulant{
public:
	typedef dm::uint64 raw_t;		//!< 原始值
	typedef dm::float32 value_t;	//!< 值

	CCumulant( const value_t& v=0,const raw_t& r=0 ):m_v(v),m_r(r){
	}

	CCumulant( const CCumulant& c ):m_v(c.m_v),m_r(c.m_r){
	}

	CCumulant( const volatile CCumulant& c ):m_v(c.m_v),m_r(c.m_r){
	}

	inline CCumulant& operator=( const CCumulant& c ){
		m_v = c.m_v;
		m_r = c.m_r;
		return *this;
	}

	inline CCumulant& operator=( volatile const CCumulant& c ){
		m_v = c.m_v;
		m_r = c.m_r;
		return *this;
	}

	inline bool operator==( const CCumulant& c )const{
		return m_v==c.m_v && m_r==c.m_r;
	}

	inline bool operator==( volatile const CCumulant& c )const{
		return m_v==c.m_v && m_r==c.m_r;
	}

	/**
	 * 获取值
	 * @return
	 */
	inline const value_t& getValue()const{
		return m_v;
	}

	/**
	 * 设置值
	 * @param v
	 */
	inline void setValue( const value_t& v ){
		m_v = v;
	}

	/**
	 * 获取原始值
	 * @return
	 */
	inline const raw_t& getRaw()const{
		return m_r;
	}

	/**
	 * 设置原始值
	 * @param r
	 */
	inline void setRaw( const raw_t& r ){
		m_r = r;
	}

	bool update( const CCumulant& c );
	bool update( const CCumulant& c )volatile;
private:
	value_t m_v;
	raw_t m_r;
};

}
}

#endif /* INCLUDE_DM_SCADA_CUMULANT_HPP_ */

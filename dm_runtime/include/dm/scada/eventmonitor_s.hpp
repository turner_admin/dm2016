﻿/*
 * eventmonitor_s.hpp
 *
 *  Created on: 2017年11月3日
 *      Author: work
 */

#ifndef _DM_SCADA_EVENTMONITOR_S_HPP_
#define _DM_SCADA_EVENTMONITOR_S_HPP_

#include <dm/export.hpp>

#ifndef DM_API_SCADA
#define DM_API_SCADA DM_API_IMPORT
#endif

#include <dm/scada/event.hpp>

namespace dm{
namespace scada{

class DM_API_SCADA CEventMonitor;

/**
 * 影子事件监视器
 * 本类的设置主要是为了屏蔽事件监视器内部的结构。
 * 比如给Qt使用。
 */
class CEventMonitorShadow{
public:
	CEventMonitorShadow();
	~CEventMonitorShadow();

	bool tryGet( CEvent& info,bool& overFlow );
	void get( CEvent& info,bool& overFlow );

	void reset();
private:
	CEventMonitor* m_eventMonitor;
};

}
}

#endif /* DM_RUNTIME_INCLUDE_DM_SCADA_EVENTMONITOR_S_HPP_ */

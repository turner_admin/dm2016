﻿/*
 * eventmonitor.hpp
 *
 *  Created on: 2016年12月9日
 *      Author: work
 */

#ifndef INCLUDE_DM_SCADA_EVENTMONITOR_HPP_
#define INCLUDE_DM_SCADA_EVENTMONITOR_HPP_

#include <dm/export.hpp>

#ifndef DM_API_SCADA
#define DM_API_SCADA DM_API_IMPORT
#endif

#include <dm/scada/eventmgr.hpp>

namespace dm{
namespace scada{

/**
 * 事件监视器类
 */
class DM_API_SCADA CEventMonitor:public CEventMgr{
public:
	typedef event_queue_t::ringpos_t ringpos_t;

	CEventMonitor();

	CEventMonitor( const ringpos_t* pos );
	virtual ~CEventMonitor();

	inline const ringpos_t& pos()const{
		return m_p;
	}

	virtual void onNew( const ringpos_t& pos,const CEvent& info,const bool& overflow )const;
	virtual bool filter( const ringpos_t& pos,const CEvent& info,const bool& overflow )const;

	void run();

	bool tryGet( CEvent& info,bool& overflow );
	void get( CEvent& info,bool& overflow );

	/**
	 * 复位监视器
	 * 未取出的事件将丢失
	 */
	inline void reset(){
		m_p = m_q->getCurPos();
	}

private:
	ringpos_t m_p;
};

}
}



#endif /* INCLUDE_DM_SCADA_EVENTMONITOR_HPP_ */

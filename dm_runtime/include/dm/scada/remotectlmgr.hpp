﻿/*
 * remotectlmgr.hpp
 *
 *  Created on: 2017年12月3日
 *      Author: work
 */

#ifndef _DM_SCADA_REMOTECTLMGR_HPP_
#define _DM_SCADA_REMOTECTLMGR_HPP_

#include <dm/export.hpp>

#ifndef DM_API_SCADA
#define DM_API_SCADA DM_API_IMPORT
#endif

#include <dm/scada/scada.hpp>

#include <dm/scada/remotectlinfo.hpp>
#include <dm/scada/remotectldescmgr.hpp>

#include <iostream>

namespace dm{
namespace scada{

class DM_API_SCADA CRemoteCtlMgr:protected CScada{
	CRemoteCtlMgr( CScada& scada );
public:
	static CRemoteCtlMgr& ins();

	inline const index_t& size()const{
		return m_cfg.size();
	}

	inline index_t indexById( const id_t& id )const{
		return m_cfg.indexOf(id);
	}

	inline index_t indexByName( const char* name )const{
		return m_cfg.indexOf(name);
	}

	inline index_t indexOfInfo( const SRemoteCtlInfo* info )const{
		return m_cfg.indexOf(info);
	}

	inline const SRemoteCtlInfo* info( const index_t& idx )const{
		return m_cfg.at(idx);
	}

	inline id_t id( const index_t& idx )const{
		const SRemoteCtlInfo* p = info(idx);
		return p?p->id:Id_Inv;
	}

	inline const char* name( const index_t& idx )const{
		const SRemoteCtlInfo* p = info(idx);
		return p?p->name.c_str():NULL;
	}

	inline const char* desc( const index_t& idx )const{
		const SRemoteCtlInfo* p = info(idx);
		return p?p->desc.c_str():"无效索引";
	}

	bool setDesc( const index_t& idx,const char* str );

	inline const SRemoteCtlDescInfo* preOpenInfo( const index_t& idx )const{
		const SRemoteCtlInfo* p = info(idx);
		return p?CRemoteCtlDescMgr::ins().info(p->descPreOpen):NULL;
	}

	inline const SRemoteCtlDescInfo* preCloseInfo( const index_t& idx )const{
		const SRemoteCtlInfo* p = info(idx);
		return p?CRemoteCtlDescMgr::ins().info(p->descPreClose):NULL;
	}

	inline const SRemoteCtlDescInfo* openInfo( const index_t& idx )const{
		const SRemoteCtlInfo* p = info(idx);
		return p?CRemoteCtlDescMgr::ins().info(p->descOpen):NULL;
	}

	inline const SRemoteCtlDescInfo* closeInfo( const index_t& idx )const{
		const SRemoteCtlInfo* p = info(idx);
		return p?CRemoteCtlDescMgr::ins().info(p->descClose):NULL;
	}

	inline id_t preOpenId( const index_t& idx )const{
		const SRemoteCtlInfo* p = info(idx);
		return p?CRemoteCtlDescMgr::ins().id(p->descPreOpen):Id_Inv;
	}

	inline id_t preCloseId( const index_t& idx )const{
		const SRemoteCtlInfo* p = info(idx);
		return p?CRemoteCtlDescMgr::ins().id(p->descPreClose):Id_Inv;
	}

	inline id_t openId( const index_t& idx )const{
		const SRemoteCtlInfo* p = info(idx);
		return p?CRemoteCtlDescMgr::ins().id(p->descOpen):Id_Inv;
	}

	inline id_t closeId( const index_t& idx )const{
		const SRemoteCtlInfo* p = info(idx);
		return p?CRemoteCtlDescMgr::ins().id(p->descClose):Id_Inv;
	}

	inline const char* preOpenDesc( const index_t& idx )const{
		const SRemoteCtlInfo* p = info(idx);
		return p?CRemoteCtlDescMgr::ins().desc(p->descPreOpen):"无效配置";
	}

	inline const char* preCloseDesc( const index_t& idx )const{
		const SRemoteCtlInfo* p = info(idx);
		return p?CRemoteCtlDescMgr::ins().desc(p->descPreClose):"无效配置";
	}

	inline const char* openDesc( const index_t& idx )const{
		const SRemoteCtlInfo* p = info(idx);
		return p?CRemoteCtlDescMgr::ins().desc(p->descOpen):"无效配置";
	}

	inline const char* closeDesc( const index_t& idx )const{
		const SRemoteCtlInfo* p = info(idx);
		return p?CRemoteCtlDescMgr::ins().desc(p->descClose):"无效配置";
	}

	index_t deviceIndex( const index_t& idx )const;
	const char* deviceDesc( const index_t& idx )const;

	void printInfo( const index_t& idx,std::ostream& ostr=std::cout )const;
private:
	TCfg<SRemoteCtlInfo> m_cfg;
};
}
}



#endif /* _DM_SCADA_REMOTECTLMGR_HPP_ */

﻿/*
 * hisrcd_discrete.hpp
 *
 *  Created on: 2017年7月10日
 *      Author: work
 */

#ifndef _DM_SCADA_HISRCD_DISCRETE_HPP_
#define _DM_SCADA_HISRCD_DISCRETE_HPP_

#include <dm/scada/hisrcd.hpp>
#include <dm/scada/discrete.hpp>

namespace dm{
namespace scada{

/**
 * 历史离散量记录信息
 */
typedef THisRecord<CDiscrete::value_t> SHisRcdDiscrete;

}
}

#endif /* INCLUDE_DM_SCADA_HISRCD_DISCRETE_HPP_ */

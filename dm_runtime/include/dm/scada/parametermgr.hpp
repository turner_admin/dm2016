﻿/*
 * parametermgr.hpp
 *
 *  Created on: 2017年12月3日
 *      Author: work
 */

#ifndef _DM_SCADA_PARAMETERMGR_HPP_
#define _DM_SCADA_PARAMETERMGR_HPP_

#include <dm/export.hpp>

#ifndef DM_API_SCADA
#define DM_API_SCADA DM_API_IMPORT
#endif

#include <dm/scada/scada.hpp>

#include <dm/scada/parainfo.hpp>
#include <dm/scada/cfg.hpp>

#include <iostream>

namespace dm{
namespace scada{

class DM_API_SCADA CParameterMgr:protected CScada{
	CParameterMgr( CScada& scada );
public:
	static CParameterMgr& ins();

	inline const index_t& size()const{
		return m_cfg.size();
	}

	inline index_t indexById( const id_t& id )const{
		return m_cfg.indexOf(id);
	}

	inline index_t indexByName( const char* name )const{
		return m_cfg.indexOf(name);
	}

	inline index_t indexOfInfo( const SParaInfo* info )const{
		return m_cfg.indexOf(info);
	}

	inline const SParaInfo* info( const index_t& idx )const{
		return m_cfg.at(idx);
	}

	inline id_t id( const index_t& idx )const{
		const SParaInfo* p = info(idx);
		return p?p->id:Id_Inv;
	}

	inline const char* name( const index_t& idx )const{
		const SParaInfo* p = info(idx);
		return p?p->name.c_str():NULL;
	}

	inline const char* desc( const index_t& idx )const{
		const SParaInfo* p = info(idx);
		return p?p->desc.c_str():"无效索引";
	}

	bool setDesc( const index_t& idx,const char* str );

	inline SParaInfo::EType type( const index_t& idx )const{
		const SParaInfo* p = info(idx);
		return p?p->type:SParaInfo::Int;
	}

	bool setType( const index_t& idx,SParaInfo::EType type );

	index_t deviceIndex( const index_t& index )const;
	const char* deviceDesc( const index_t& index )const;

	void printInfo( const index_t& idx,std::ostream& ostr=std::cout )const;
private:
	TCfg<SParaInfo> m_cfg;
};

}
}

#endif /* _DM_SCADA_PARAMETERMGR_HPP_ */

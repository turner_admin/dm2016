﻿/*
 * cumulantinfo.hpp
 *
 *  Created on: 2017年3月24日
 *      Author: work
 */

#ifndef _DM_SCADA_CUMULANTINFO_HPP_
#define _DM_SCADA_CUMULANTINFO_HPP_

#include <dm/scada/types.hpp>
#include <dm/bits.hpp>

namespace dm{
namespace scada{

/**
 * 累计量描述信息
 */
struct SCumulantInfo{
	/**
	 * 标志位
	 */
	enum EFlag{
		FUse = 0,		//!< 是否使用
		FSave,     		//!< 是否存盘
		FGenTimed,		//!< 是否产生时标数据
		FReportTimed,	//!< 是否上报时标数据

		 FNone
	};

	inline bool isFlagSave()const{
		return dm::bit_get(flag,FSave);
	}

	inline void setFlagSave(){
		flag = dm::bit_set(flag,FSave);
	}

	inline void clrFlagSave(){
		flag = dm::bit_clr(flag,FSave);
	}

	inline void setFlagSave( const bool& s ){
		if( s )
			setFlagSave();
		else
			clrFlagSave();
	}

	inline bool isFlagGenTimed()const{
		return dm::bit_get(flag,FGenTimed);
	}

	inline void setFlagGenTimed(){
		flag = dm::bit_set(flag,FGenTimed);
	}

	inline void clrFlagGenTimed(){
		flag = dm::bit_clr(flag,FGenTimed);
	}

	inline void setFlagGenTimed( const bool& s ){
		if( s )
			setFlagGenTimed();
		else
			clrFlagGenTimed();
	}

	/**
	 * 是否上报时标数据
	 * @return
	 */
	inline bool isFlagReportTimed()const{
		return dm::bit_get(flag,FReportTimed);
	}

	/**
	 * 设置上报时标数据标志
	 */
	inline void setFlagReportTimed(){
		flag = dm::bit_set(flag,FReportTimed);
	}

	inline void clrFlagReportTimed(){
		flag = dm::bit_clr(flag,FReportTimed);
	}

	/**
	 * 设置上报时标数据标志
	 * @param s
	 */
	inline void setFlagReportTimed( const bool& s ){
		if( s )
			setFlagReportTimed();
		else
			clrFlagReportTimed();
	}

	typedef dm::uint64 delta_t;
	typedef dm::uint64 code_t;
	typedef dm::float32 coef_t;

	id_t id;
	name_t name;
	desc_t desc;
	code_t maxCode;	//!< 满码。当累计到满码后，会复归为0后重新累计
	delta_t delta;		//!< 实际值变化范围。值变化不小于该范围有效。
	coef_t coef;		//!< 表码系数。计算实际物理量的变化系数。

	flag_t flag;			//!< 标志
};

}
}


#endif /* INCLUDE_DM_SCADA_CUMULANTINFO_HPP_ */

﻿/*
 * hisrcd_status.hpp
 *
 *  Created on: 2017年7月10日
 *      Author: work
 */

#ifndef _DM_SCADA_HISRCD_STATUS_HPP_
#define _DM_SCADA_HISRCD_STATUS_HPP_

#include <dm/scada/hisrcd.hpp>
#include <dm/scada/status.hpp>

namespace dm{
namespace scada{

/**
 * 历史状态量记录信息
 */
typedef THisRecord<CStatus::value_t> SHisRcdStatus;

}
}

#endif /* INCLUDE_DM_SCADA_HISRCD_STATUS_HPP_ */

﻿/*
 * hisrcd.hpp
 *
 *  Created on: 2017年7月10日
 *      Author: work
 */

#ifndef _DM_SCADA_HISRCD_HPP_
#define _DM_SCADA_HISRCD_HPP_

#include <dm/timestamp.hpp>
#include <dm/scada/types.hpp>

namespace dm{
namespace scada{

/**
 * 历史记录结构体模板
 */
template<typename T>
struct THisRecord{
	/**
	 * 历史数据记录原因
	 */
	enum ECause{
		Test = 0,	//!< 测试记录
		Clocking,	//!< 定时记录
		Varied,		//!< 变化记录
		Manual,		//!< 人工记录
		Complement	//!< 补填
	};

	typedef dm::CTimeStamp ts_t;	//!< 记录时标类型

	THisRecord():
		ts(),op(),name(),cause(Test),v(){
	}

	THisRecord( const name_t& name_,const T& v_,const ts_t& ts_,const ts_t& op_,const ECause& cause_ ):
		ts(ts_),op(op_),name(name_),cause(cause_),v(v_){
	}

	THisRecord( const THisRecord& r ):name(r.name),v(r.v),ts(r.ts),op(r.op),cause(r.cause){
	}

	inline THisRecord& operator=( const THisRecord& r ){
		name = r.name;
		v = r.v;
		ts = r.ts;
		op = r.op;
		cause = r.cause;

		return *this;
	}

	ts_t ts;		//!< 数据时标
	ts_t op;		//!< 操作时刻
	name_t name;	//!< 点名字

	ECause cause;	//!< 记录原因

	T v;		//!< 值
};

}
}



#endif /* INCLUDE_DM_SCADA_HISRCD_HPP_ */

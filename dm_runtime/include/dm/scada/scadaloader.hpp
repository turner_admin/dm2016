﻿/*
 * scadaloader.hpp
 *
 *  Created on: 2017年11月22日
 *      Author: work
 */

#ifndef _DM_SCADA_SCADALOADER_HPP_
#define _DM_SCADA_SCADALOADER_HPP_

#include <dm/export.hpp>

#ifndef DM_API_SCADA
#define DM_API_SCADA DM_API_IMPORT
#endif

#include <dm/scada/scada.hpp>
#include <dm/scada/cfg.hpp>
#include <dm/os/db/db.hpp>

namespace dm{
namespace scada{

/**
 * SCADA加载类。只提供给scada内部加载使用.
 */
class DM_API_SCADA CScadaLoader{
	CScadaLoader( const CScadaLoader& );
	CScadaLoader& operator=( const CScadaLoader& );
public:

	CScadaLoader( CScada* scada );

private:
	bool load( os::CDb& db,SDeviceInfo* buf,const index_t& size );

	bool load( os::CDb& db,SStatusDescInfo* buf,const index_t& size );
	bool load( os::CDb& db,SDiscreteDescInfo* buf,const index_t& size );
	bool load( os::CDb& db,SRemoteCtlDescInfo* buf,const index_t& size );
	bool load( os::CDb& db,SActionDescInfo* buf,const index_t& size );

	bool load( os::CDb& db,SStatusInfo* buf,const index_t& size,SDeviceInfo* devices,const index_t& deviceSize,const TCfg<SStatusDescInfo>& desc );
	bool load( os::CDb& db,SDiscreteInfo* buf,const index_t& size,SDeviceInfo* devices,const index_t& deviceSize,const TCfg<SDiscreteDescInfo>& desc );
	bool load( os::CDb& db,SMeasureInfo* buf,const index_t& size,SDeviceInfo* devices,const index_t& deviceSize );
	bool load( os::CDb& db,SCumulantInfo* buf,const index_t& size,SDeviceInfo* devices,const index_t& deviceSize );
	bool load( os::CDb& db,SRemoteCtlInfo* buf,const index_t& size,SDeviceInfo* devices,const index_t& deviceSize,const TCfg<SRemoteCtlDescInfo>& desc );
	bool load( os::CDb& db,SParaInfo* buf,const index_t& size,SDeviceInfo* devices,const index_t& deviceSize );
	bool load( os::CDb& db,SActionInfo* buf,const index_t& size,SDeviceInfo* devices,const index_t& deviceSize,const TCfg<SActionDescInfo>& desc );

	bool loadLogicDevice( os::CDb& db,SLogicDeviceInfo* buf,const index_t& size );

	bool loadLogicStatus( os::CDb& db,index_t* map2device,const index_t& size,index_t* map2logic,const index_t& statussSize,SLogicDeviceInfo* logicDevices,const index_t& logicDeviceSize,const TCfg<SStatusInfo>& cfg );
	bool loadLogicDiscrete( os::CDb& db,index_t* map2device,const index_t& size,index_t* map2logic,const index_t& discreteSize,SLogicDeviceInfo* logicDevices,const index_t& logicDeviceSize,const TCfg<SDiscreteInfo>& cfg );

	bool loadLogicMeasure( os::CDb& db,index_t* map2device,const index_t& size,index_t* map2logic,const index_t& measureSize,SLogicDeviceInfo* logicDevices,const index_t& logicDeviceSize,const TCfg<SMeasureInfo>& cfg );
	bool loadLogicCumulant( os::CDb& db,index_t* map2device,const index_t& size,index_t* map2logic,const index_t& cumulantSize,SLogicDeviceInfo* logicDevices,const index_t& logicDeviceSize,const TCfg<SCumulantInfo>& cfg );
	bool loadLogicRemoteCtl( os::CDb& db,index_t* map2device,const index_t& size,index_t* map2logic,const index_t& remoteCtlSize,SLogicDeviceInfo* logicDevices,const index_t& logicDeviceSize,const TCfg<SRemoteCtlInfo>& cfg );
	bool loadLogicParameter( os::CDb& db,index_t* map2device,const index_t& size,index_t* map2logic,const index_t& parameterSize,SLogicDeviceInfo* logicDevices,const index_t& logicDeviceSize,const TCfg<SParaInfo>& cfg );
	bool loadLogicAction( os::CDb& db,index_t* map2device,const index_t& size,index_t* map2logic,const index_t& actionSize,SLogicDeviceInfo* logicDevices,const index_t& logicDeviceSize,const TCfg<SActionInfo>& cfg );
};

}
}

#endif /* DM_RUNTIME_INCLUDE_DM_SCADA_SCADALOADER_HPP_ */

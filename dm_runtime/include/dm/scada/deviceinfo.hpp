﻿/*
 * deviceinfo.hpp
 *
 *  Created on: 2016年11月17日
 *      Author: work
 */

#ifndef INCLUDE_DM_SCADA_DEVICEINFO_HPP_
#define INCLUDE_DM_SCADA_DEVICEINFO_HPP_

#include <dm/scada/types.hpp>

namespace dm{
namespace scada{

/**
 * 设备信息
 */
struct SDeviceInfo{
	id_t id;				//!< 设备ID。0,无效。唯一标识设备
	name_t name;		//!< 设备名称。全局唯一
	desc_t desc;		//!< 设备描述

	id_t parent;			//!< 父设备ID。0，无父设备。暂时未用

	index_t sizeOfStatus;	//!< 状态量个数
	index_t posOfStatus;	//!< 状态量位置

	index_t sizeOfDiscrete;	//!< 离散量个数
	index_t posOfDiscrete;	//!< 离散量位置

	index_t sizeOfTeleCtl;	//!< 远控个数
	index_t posOfTeleCtl;		//!< 远控位置

	index_t sizeOfMeasure;	//!< 测量量个数
	index_t posOfMeasure;		//!< 测量量位置

	index_t sizeOfCumulant;	//!< 累计量个数
	index_t posOfCumulant;	//!< 累计量位置

	index_t sizeOfPara;		//!< 参数个数
	index_t posOfPara;			//!< 参数位置

	index_t sizeOfAction;		//!< 动作个数
	index_t posOfAction;		//!< 动作位置
};

}
}


#endif /* INCLUDE_DM_SCADA_DEVICEINFO_HPP_ */

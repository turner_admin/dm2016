﻿/*
 * scadainfo.hpp
 *
 *  Created on: 2017年11月21日
 *      Author: work
 */

#ifndef _DM_SCADA_SCADAINFO_HPP_
#define _DM_SCADA_SCADAINFO_HPP_

#include <dm/export.hpp>

#ifndef DM_API_SCADA
#define DM_API_SCADA DM_API_IMPORT
#endif

#include <dm/scada/types.hpp>

namespace dm{

namespace os{
class CDb;
}

namespace scada{

/**
 * SCADA系统信息
 */
class DM_API_SCADA CScadaInfo{
	CScadaInfo( const CScadaInfo& );
	CScadaInfo& operator=( const CScadaInfo& );
public:
	CScadaInfo();

	/**
	 * 设备数量
	 * @return
	 */
	inline const index_t& deviceCount()const{
		return m_deviceCount;
	}

	inline const index_t& statusCount()const{
		return m_statusCount;
	}

	inline const index_t& statusDescCount()const{
		return m_statusDescCount;
	}

	inline const index_t& discreteCount()const{
		return m_discreteCount;
	}

	inline const index_t& discreteDescCount()const{
		return m_discreteDescCount;
	}

	inline const index_t& measureCount()const{
		return m_measureCount;
	}

	inline const index_t& cumulantCount()const{
		return m_cumulantCount;
	}

	inline const index_t& remoteCtlCount()const{
		return m_remoteCtlCount;
	}

	inline const index_t& remoteCtlDescCount()const{
		return m_remoteCtlDescCount;
	}

	inline const index_t& parameterCount()const{
		return m_parameterCount;
	}

	inline const index_t& actionCount()const{
		return m_actionCount;
	}

	inline const index_t& actionDescCount()const{
		return m_actionDescCount;
	}

	inline const index_t& logicDeviceCount()const{
		return m_logicDeviceCount;
	}

	inline const index_t& logicStatusCount()const{
		return m_logicStatusCount;
	}

	inline const index_t& logicDiscreteCount()const{
		return m_logicDiscreteCount;
	}

	inline const index_t& logicMeasureCount()const{
		return m_logicMeasureCount;
	}

	inline const index_t& logicCumulantCount()const{
		return m_logicCumulantCount;
	}

	inline const index_t& logicRemoteCtlCount()const{
		return m_logicRemoteCtlCount;
	}

	inline const index_t& logicParameterCount()const{
		return m_logicParameterCount;
	}

	inline const index_t& logicActionCount()const{
		return m_logicActionCount;
	}

	inline index_t mapedStatusCount()const{
		return m_statusCount*m_logicDeviceCount;
	}

	inline index_t mapedDiscreteCount()const{
		return m_discreteCount*m_logicDeviceCount;
	}

	inline index_t mapedMeasureCount()const{
		return m_measureCount*m_logicDeviceCount;
	}

	inline index_t mapedCumulantCount()const{
		return m_cumulantCount*m_logicDeviceCount;
	}

	inline index_t mapedRemoteCtlCount()const{
		return m_remoteCtlCount*m_logicDeviceCount;
	}

	inline index_t mapedParameterCount()const{
		return m_parameterCount*m_logicDeviceCount;
	}

	inline index_t mapedActionCount()const{
		return m_actionCount*m_logicDeviceCount;
	}

private:
	int getDbCount( dm::os::CDb& db,const char* table )const;

private:
	index_t m_deviceCount;	//!< 设备数量

	index_t m_statusCount;	//!< 状态量数量
	index_t m_statusDescCount;	//!< 状态量描述数量

	index_t m_discreteCount;	//!< 离散量数量
	index_t m_discreteDescCount;	//!< 离散量描述数量

	index_t m_measureCount;	//!< 测量量数量

	index_t m_cumulantCount;	//!< 累计量数量

	index_t m_remoteCtlCount;	//!< 远控数量
	index_t m_remoteCtlDescCount;	//!< 远控描述数量

	index_t m_parameterCount;	//!< 参数数量

	index_t m_actionCount;	//!< 动作数量
	index_t m_actionDescCount;	//!< 动作描述数量

	index_t m_logicDeviceCount;	//!< 逻辑设备数量

	index_t m_logicStatusCount;	//!< 逻辑状态量数量
	index_t m_logicDiscreteCount;	//!< 逻辑离散量数量
	index_t m_logicMeasureCount;	//!< 逻辑测量量数量
	index_t m_logicCumulantCount;	//!< 逻辑累计量数量

	index_t m_logicRemoteCtlCount;	//!< 逻辑远控数量
	index_t m_logicParameterCount;	//!< 逻辑参数数量

	index_t m_logicActionCount;	//!< 逻辑动作数量
};

}
}

#endif /* DM_RUNTIME_INCLUDE_DM_SCADA_SCADAINFO_HPP_ */

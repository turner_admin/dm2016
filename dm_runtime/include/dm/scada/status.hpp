﻿/*
 * rtstatus.hpp
 *
 *  Created on: 2016年11月17日
 *      Author: work
 */

#ifndef _DM_SCADA_RTSTATUS_HPP_
#define _DM_SCADA_RTSTATUS_HPP_

#include <dm/export.hpp>

#ifndef DM_API_SCADA
#define DM_API_SCADA DM_API_IMPORT
#endif

#include <dm/types.hpp>

namespace dm{
namespace scada{

/**
 * 状态量实时值
 * 状态量系统使用双点表示
 */
class DM_API_SCADA CStatus{
public:
	typedef dm::uint8 raw_t;
	typedef dm::uint8 value_t;

	enum{
		InvOff = 0x00,
		Off,
		On,
		InvOn,
		Other
	};

	CStatus( const value_t& s=InvOff,const raw_t& r=0):m_v(s),m_raw(r){}
	CStatus( const CStatus& s):m_v(s.m_v),m_raw(s.m_raw){}
	CStatus( const volatile CStatus& s):m_v(s.m_v),m_raw(s.m_raw){}
	CStatus& operator=( const CStatus& );
	CStatus& operator=( volatile const CStatus& );

	inline bool operator==( const CStatus& s )const{
		return m_v==s.m_v;
	}

	inline bool operator==( volatile const CStatus& s )const{
		return m_v==s.m_v;
	}

	inline bool isOn()const{
		return m_v==On;
	}

	inline bool isOff()const{
		return m_v==Off;
	}

	inline bool isInvOff()const{
		return m_v==InvOff;
	}

	inline bool isInvOn()const{
		return m_v == InvOn;
	}

	inline bool isOther()const{
		return m_v>=Other;
	}

	inline const value_t& getValue()const{
		return m_v;
	}

	inline const raw_t& getRaw()const{
		return m_raw;
	}

	inline void setValue( const value_t& s ){
		m_v = s;
	}

	inline void setRaw( const raw_t& r ){
		m_raw = r;
	}

	bool update( const CStatus& s );
	bool update( const CStatus& s )volatile;

private:
	value_t	m_v;	//
	raw_t 	m_raw;
};

}
}

#endif /* INCLUDE_DM_SCADA_RTSTATUS_HPP_ */

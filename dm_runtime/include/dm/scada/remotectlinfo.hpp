﻿/*
 * telecontrolinfo.hpp
 *
 *  Created on: 2017年1月6日
 *      Author: work
 */

#ifndef _DM_SCADA_REMOTECTLINFO_HPP_
#define _DM_SCADA_REMOTECTLINFO_HPP_

#include <dm/scada/types.hpp>

namespace dm{
namespace scada{

struct SRemoteCtlInfo{
	id_t id;
	name_t name;
	desc_t desc;

	int descPreOpen;		// 预置的描述
	int descOpen;			// 分开控制描述
	int descClose;			// 闭合控制描述
	int descPreClose;		// 复位控制描述
};

}
}



#endif /* INCLUDE_DM_SCADA_TELECONTROLINFO_HPP_ */

﻿/*
 * discretedescmgr.hpp
 *
 *  Created on: 2017年12月2日
 *      Author: work
 */

#ifndef _DM_SCADA_DISCRETEDESCMGR_HPP_
#define _DM_SCADA_DISCRETEDESCMGR_HPP_

#include <dm/export.hpp>

#ifndef DM_API_SCADA
#define DM_API_SCADA DM_API_IMPORT
#endif

#include <dm/scada/scada.hpp>
#include <dm/scada/discretedescinfo.hpp>
#include <dm/scada/cfg.hpp>

namespace dm{
namespace scada{

/**
 * 离散量描述管理器
 */
class DM_API_SCADA CDiscreteDescMgr:protected CScada{
	CDiscreteDescMgr( CScada& scada );
public:
	static CDiscreteDescMgr& ins();

	/**
	 * 离散量描述记录条数
	 * @return
	 */
	inline int size()const{
		return m_cfg.size();
	}

	/**
	 * 根据id获取离散量描述索引
	 * @param id
	 * @return
	 */
	inline index_t indexById( const id_t& id )const{
		return m_cfg.indexOf(id);
	}

	/**
	 * 获取离散量描述信息的索引
	 * @param info
	 * @return
	 */
	inline index_t indexOfInfo( const SDiscreteDescInfo* info )const{
		return m_cfg.indexOf(info);
	}

	/**
	 * 离散量描述的信息
	 * @param idx
	 * @return
	 */
	inline const SDiscreteDescInfo* info( const index_t& idx )const{
		return m_cfg.at(idx);
	}

	/**
	 * 离散量描述的id
	 * @param idx
	 * @return
	 */
	inline id_t id( const index_t& idx )const{
		const SDiscreteDescInfo* p = info(idx);
		return p?p->id:Id_Inv;
	}

	/**
	 * 离散量描述的文字信息
	 * @param idx
	 * @return
	 */
	inline const char* desc( const index_t& idx )const{
		const SDiscreteDescInfo* p = info(idx);
		return p?p->desc.c_str():"无效索引";
	}

	bool setDesc( const index_t& idx,const char* str );
private:
	TCfg<SDiscreteDescInfo> m_cfg;
};

}
}

#endif /* DM_RUNTIME_INCLUDE_DM_SCADA_DISCRETEDESCMGR_HPP_ */

#ifndef _DM_SCADA_MODEL_DISCRETE_INFO_HPP
#define _DM_SCADA_MODEL_DISCRETE_INFO_HPP

#include <dm/scada/model/signal_common.hpp>
#include <dm/bitmask.hpp>

namespace dm{
namespace scada{
namespace model{

/**
 * 离散量信息
*/
struct SDiscreteInfo{
    enum EFlag{
        FSave,
        FGenTimed,
        FReport
    };

    SSignalCommonInfo common;

    size_t sizeOfDesc;
    size_t posOfDesc;

    signal_flags_t flags;

    inline bool ifSave()const{
        return dm::isBitSet<signal_flags_t,FSave>(flags);
    }

    inline bool ifGenTimed()const{
        return dm::isBitSet<signal_flags_t,FGenTimed>(flags);
    }

    inline bool ifReport()const{
        return dm::isBitSet<signal_flags_t,FReport>(flags);
    }

    inline void setSave(){
        dm::bitSet<signal_flags_t,FSave>(flags);
    }

    inline void clrSave(){
        dm::bitClr<signal_flags_t,FSave>(flags);
    }

    inline void setGenTimed(){
        dm::bitSet<signal_flags_t,FGenTimed>(flags);
    }

    inline void clrGenTimed(){
        dm::bitClr<signal_flags_t,FGenTimed>(flags);
    }

    inline void setReport(){
        dm::bitSet<signal_flags_t,FReport>(flags);
    }

    inline void clrReport(){
        dm::bitClr<signal_flags_t,FReport>(flags);
    }
};

}
}
}

#endif
#ifndef _DM_SCADA_MODEL_MEASURE_INFO_HPP_H
#define _DM_SCADA_MODEL_MEASURE_INFO_HPP_H

#include <dm/scada/model/signal_common.hpp>

namespace dm{
namespace scada{
namespace model{

/**
 * 测量量信息
*/
struct SMeasureInfo{
    enum EFlag{
        FSave,
        FGenTimed,
        FReport
    };

    SSignalCommonInfo common;

    float base;
    float coef;
    unit_t unit;

    float lmtUup;
    float lmtUp;
    float lmtDn;
    float lmtDdn;

    level_t levelUup;
    level_t levelUp;
    level_t levelDn;
    level_t levelDdn;

    signal_flags_t flags;

    inline bool ifSave()const{
        return dm::isBitSet<signal_flags_t,FSave>(flags);
    }

    inline bool ifGenTimed()const{
        return dm::isBitSet<signal_flags_t,FGenTimed>(flags);
    }

    inline bool ifReport()const{
        return dm::isBitSet<signal_flags_t,FReport>(flags);
    }

    inline void setSave(){
        dm::bitSet<signal_flags_t,FSave>(flags);
    }

    inline void clrSave(){
        dm::bitClr<signal_flags_t,FSave>(flags);
    }

    inline void setGenTimed(){
        dm::bitSet<signal_flags_t,FGenTimed>(flags);
    }

    inline void clrGenTimed(){
        dm::bitClr<signal_flags_t,FGenTimed>(flags);
    }

    inline void setReport(){
        dm::bitSet<signal_flags_t,FReport>(flags);
    }

    inline void clrReport(){
        dm::bitClr<signal_flags_t,FReport>(flags);
    }
};

}
}
}
#endif
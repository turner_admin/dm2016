#ifndef _DM_SCADA_MODEL_PARAMETER_INFO_HPP_
#define _DM_SCADA_MODEL_PARAMETER_INFO_HPP_

#include <dm/scada/model/signal_common.hpp>

namespace dm{
namespace scada{
namespace model{

/**
 * 参数信息
*/
struct SParameterInfo{
    enum EFlag{
        FSave,
        FGenTimed,
        FReport
    };

    enum EType{
        TUnknow,
        TBoolean,
        TInt,
        TFloat,
        TString
    };

    SSignalCommonInfo common;

    EType type;

    union{
        bool v_bool;
        int v_int;
        float v_float;
        char v_string;
    }value;

    signal_flags_t flags;

    inline bool ifSave()const{
        return dm::isBitSet<signal_flags_t,FSave>(flags);
    }

    inline bool ifGenTimed()const{
        return dm::isBitSet<signal_flags_t,FGenTimed>(flags);
    }

    inline bool ifReport()const{
        return dm::isBitSet<signal_flags_t,FReport>(flags);
    }

    inline void setSave(){
        dm::bitSet<signal_flags_t,FSave>(flags);
    }

    inline void clrSave(){
        dm::bitClr<signal_flags_t,FSave>(flags);
    }

    inline void setGenTimed(){
        dm::bitSet<signal_flags_t,FGenTimed>(flags);
    }

    inline void clrGenTimed(){
        dm::bitClr<signal_flags_t,FGenTimed>(flags);
    }

    inline void setReport(){
        dm::bitSet<signal_flags_t,FReport>(flags);
    }

    inline void clrReport(){
        dm::bitClr<signal_flags_t,FReport>(flags);
    }
};

}
}
}

#endif
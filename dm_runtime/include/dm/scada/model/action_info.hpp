#ifndef _DM_SCADA_MODEL_ACTION_INFO_HPP_
#define _DM_SCADA_MODEL_ACTION_INFO_HPP_

#include <dm/scada/model/signal_common.hpp>

namespace dm{
namespace scada{
namespace model{

/**
 * 
*/
struct SActionInfo{
    enum EFlag{
        FSave,
        FReport,
        FReverse
    };

    SSignalCommonInfo common;

    desc_t descInvOff;
    desc_t descOff;
    desc_t descOn;
    desc_t descInvOn;

    sound_t soundInvOff;
    sound_t soundOff;
    sound_t soundOn;
    sound_t soundInvOn;

    level_t levelInvOff;
    level_t levelOff;
    level_t levelOn;
    level_t levelInvOn;

    signal_flags_t flags;

    inline bool ifSave()const{
        return dm::isBitSet<signal_flags_t,FSave>(flags);
    }

    inline bool ifGenTimed()const{
        return dm::isBitSet<signal_flags_t,FGenTimed>(flags);
    }

    inline bool ifReport()const{
        return dm::isBitSet<signal_flags_t,FReport>(flags);
    }

    inline bool ifReverse()const{
        return dm::isBitSet<signal_flags_t,FReverse>(flags);
    }

    inline void setSave(){
        dm::bitSet<signal_flags_t,FSave>(flags);
    }

    inline void clrSave(){
        dm::bitClr<signal_flags_t,FSave>(flags);
    }

    inline void setGenTimed(){
        dm::bitSet<signal_flags_t,FGenTimed>(flags);
    }

    inline void clrGenTimed(){
        dm::bitClr<signal_flags_t,FGenTimed>(flags);
    }

    inline void setReport(){
        dm::bitSet<signal_flags_t,FReport>(flags);
    }

    inline void clrReport(){
        dm::bitClr<signal_flags_t,FReport>(flags);
    }

    inline void setReverse(){
        dm::bitSet<signal_flags_t,FReverse>(flags);
    }

    inline void clrReverse(){
        dm::bitClr<signal_flags_t,FReverse>(flags);
    }
};

}
}
}

#endif
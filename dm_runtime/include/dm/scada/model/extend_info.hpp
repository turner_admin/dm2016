#ifndef _DM_SCADA_MODEL_EXTEND_INFO_HPP_
#define _DM_SCADA_MODEL_EXTEND_INFO_HPP_

#include <dm/scada/types.hpp>

namespace dm{
namespace scada{
namespace model{

/**
 * 扩展信息
*/
struct SExtendInfo{
    id_t id;
    magic_t magic;
};

}
}
}

#endif
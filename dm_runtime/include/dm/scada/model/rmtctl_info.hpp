#ifndef _DM_SCADA_MODEL_RMTCTL_INFO_HPP_
#define _DM_SCADA_MODEL_RMTCTL_INFO_HPP_

#include <dm/scada/model/signal_common.hpp>

namespace dm{
namespace scada{
namespace model{

/**
 * 远控信息
*/
struct SRmtctlInfo{
    SSignalCommonInfo common;

    desc_t descPreset;
    desc_t descOpen;
    desc_t descClose;
    desc_t descReset;

    sound_t soundPreset;
    sound_t soundOpen;
    sound_t soundClose;
    sound_t soundReset;
};

}
}
}

#endif
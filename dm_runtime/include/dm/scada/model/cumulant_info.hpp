#ifndef _DM_SCADA_MODEL_CUMULANT_INFO_HPP_
#define _DM_SCADA_MODEL_CUMULANT_INFO_HPP_

#include <dm/scada/model/signal_common.hpp>

namespace dm{
namespace scada{
namespace model{

/**
 * 累计量信息
*/
struct  SCumulantInfo{
    enum EFlag{
        FSave,
        FGenTimed,
        FReport
    };

    SSignalCommonInfo common;

    float64 base;
    float64 coef;
    unit_t unit;

    float64 fast;
    float64 maxCode;

    level_t levelMax;
    level_t levelFast;
    level_t levelReverse;

    signal_flags_t flags;

    inline bool ifSave()const{
        return dm::isBitSet<signal_flags_t,Save>(flags);
    }

    inline bool ifGenTimed()const{
        return dm::isBitSet<signal_flags_t,GenTimed>(flags);
    }

    inline bool ifReport()const{
        return dm::isBitSet<signal_flags_t,Report>(flags);
    }

    inline void setSave(){
        dm::bitSet<signal_flags_t,Save>(flags);
    }

    inline void clrSave(){
        dm::bitClr<signal_flags_t,Save>(flags);
    }

    inline void setGenTimed(){
        dm::bitSet<signal_flags_t,GenTimed>(flags);
    }

    inline void clrGenTimed(){
        dm::bitClr<signal_flags_t,GenTimed>(flags);
    }

    inline void setReport(){
        dm::bitSet<signal_flags_t,Report>(flags);
    }

    inline void clrReport(){
        dm::bitClr<signal_flags_t,Report>(flags);
    }
};

}
}
}

#endif
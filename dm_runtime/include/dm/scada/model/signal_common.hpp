#ifndef _DM_SCADA_MODEL_SIGNAL_COMMON_HPP_
#define _DM_SCADA_MODEL_SIGNAL_COMMON_HPP_

#include <dm/scada/types.hpp>

namespace dm{
namespace scada{
namespace model{

/**
 * 信号信息
 * 所有信号的公共信息
*/
struct SSignalCommonInfo{
    id_t id;    //!< 信号标识
    name_t name;
    desc_t desc;
    sound_t sound;
};

}
}
}

#endif
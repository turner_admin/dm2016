/*
 * model_info.hpp
 *
 *  Created on: 2023年9月18日
 *      Author: Dylan.Gao
 */

#ifndef _DM_SCADA_MODEL_MODEL_INFO_HPP_
#define _DM_SCADA_MODEL_MODEL_INFO_HPP_

#include <dm/scada/types.hpp>

namespace dm{
namespace scada{
namespace model{

/**
 * 模型信息类型
 */
struct SModelInfo{
	typedef dm::uint32 size_t;
	typedef dm::uint32 version_t;

	id_t id;
	version_t version;
	
	name_t name;
	desc_t desc;
	sound_t sound;

	magic_t magic;	// 代表是否初始化该记录

	size_t sizeOfExtend;	// 扩展模型数量

	size_t sizeOfSubDevices;	// 本模型子设备数量
	size_t sizeOfStatus;		// 本模型状态量数量
	size_t sizeOfDiscrete;		// 本模型离散量数量
	size_t sizeOfMeasure;		// 本模型测量量数量
	size_t sizeOfCumulant;		// 本模型累计量数量
	size_t sizeOfParameter;		// 本模型参数数量
	size_t sizeOfRmtctl;		// 本模型远控数量
	size_t sizeOfAction;		// 本模型动作数量

	// 包含扩展过来的，统计数量的有效性由其扩展模型魔数和子设备模型魔数来保证
	size_t sizeOfAllSubDevices;	// 所有子设备数量。
	size_t sizeOfAllStatus;		// 所有状态量数量
	size_t sizeOfAllDiscrete;	// 所有离散量数量
	size_t sizeOfAllMeasure;	// 所有测量量数量
	size_t sizeOfAllCumulant;	// 所有累计量数量
	size_t sizeOfAllParameter;	// 所有参数数量
	size_t sizeOfAllRmtctl;		// 所有远控数量
	size_t sizeOfAllAction;		// 所有动作数量
};

}
}
}


#endif /* _DM_SCADA_MODEL_MODEL_INFO_HPP_ */

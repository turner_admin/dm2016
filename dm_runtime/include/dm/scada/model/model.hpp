/*
 * model.hpp
 *
 *  Created on: 2023年9月17日
 *      Author: Dylan.Gao
 */

#ifndef _DM_SCADA_MODEL_MODEL_HPP_
#define _DM_SCADA_MODEL_MODEL_HPP_

#include <dm/scada/model/model_info.hpp>

namespace dm{
namespace scada{
namespace model{

struct SSubDeviceInfo;
struct SStatusInfo;
struct SDiscreteInfo;
struct SMeasureInfo;
struct SCumulantInfo;
struct SParameterInfo;
struct SRmtctlInfo;
struct SActionInfo;

struct SDiscreteDesc;

/**
 * 模型类
 */
class CModel{
    CModel();
    CModel( const CModel& );
    CModel& operator=( const CModel& );

public:
    bool isValid( dm::ECause& cause );

    inline const name_t& name()const{
        return m_info.name;
    }

    inline const desc_t& desc()const{
        return m_info.desc;
    }

    inline const sound_t& sound()const{
        return m_info.sound;
    }

    inline const size_t& sizeOfSubDevices()const{
        return m_info.sizeOfAllSubDevices;
    }

    inline const size_t& sizeOfStatuses()const{
        return m_info.sizeOfAllStatus;
    }

    inline const size_t& sizeOfDiscretes()const{
        return m_info.sizeOfAllDiscrete;
    }

    inline const size_t& sizeOfMeasures()const{
        return m_info.sizeOfAllMeasure;
    }

    inline const size_t& sizeOfCumulants()const{
        return m_info.sizeOfAllCumulant;
    }

    inline const size_t& sizeOfParameters()const{
        return m_info.sizeOfAllParameter;
    }

    inline const size_t& sizeOfRmtctls()const{
        return m_info.sizeOfAllRmtctl;
    }

    inline const size_t& sizeOfActions()const{
        return m_info.sizeOfAllAction;
    }

    bool getSubDeviceInfo( index_t idx,SSubDeviceInfo& info,dm::ECause& cause );
    bool getStatusInfo( index_t idx,SStatusInfo& info,dm::ECause& cause );
    bool getDiscreteInfo( index_t idx,SDiscreteInfo& info,dm::ECause& cause, SDiscreteDesc* descs=nullptr );
    bool getMeasureInfo( index_t idx,SMeasureInfo& info,dm::ECause& cause );
    bool getCumulantInfo( index_t idx,SCumulantInfo& info,dm::ECause& cause );
    bool getParameterInfo( index_t idx,SParameterInfo& info,dm::ECause& cause );
    bool getRmtctlInfo( index_t idx,SRmtctlInfo& info,dm::ECause& cause );
    bool getActionInfo( index_t idx,SActionInfo& info,dm::ECause& cause );

    inline const size_t& sizeOfSelfExtends()const{
        return m_info.sizeOfExtend;
    }

    inline const size_t& sizeOfSelfSubDevices()const{
        return m_info.sizeOfSubDevices;
    }

    inline const size_t& sizeOfSelfStatuses()const{
        return m_info.sizeOfStatus;
    }

    inline const size_t& sizeOfSelfDiscretes()const{
        return m_info.sizeOfDiscrete;
    }

    inline const size_t& sizeOfSelfMeasures()const{
        return m_info.sizeOfMeasure;
    }

    inline const size_t& sizeOfSelfCumulants()const{
        return m_info.sizeOfCumulant;
    }

    inline const size_t& sizeOfSelfParameters()const{
        return m_info.sizeOfParameter;
    }

    inline const size_t& sizeOfSelfRmtctls()const{
        return m_info.sizeOfRmtctl;
    }

    inline const size_t& sizeOfSelfActions()const{
        return m_info.sizeOfAction;
    }

    bool getSelfExtend( index_t idx,CModel& model,dm::ECause& cause );
    bool getSelfSubDeviceInfo( index_t idx,SSubDeviceInfo& info,dm::ECause& cause );
    bool getSelfStatusInfo( index_t idx,SStatusInfo& info,dm::ECause& cause );
    bool getSelfDiscreteInfo( index_t idx,SDiscreteInfo& info,dm::ECause& cause,SDiscreteDesc* descs=nullptr );
    bool getSelfMeasureInfo( index_t idx,SMeasureInfo& info,dm::ECause& cause );
    bool getSelfCumulantInfo( index_t idx,SCumulantInfo& info,dm::ECause& cause );
    bool getSelfParameterInfo( index_t idx,SParameterInfo& info,dm::ECause& cause );
    bool getSelfRmtctlInfo( index_t idx,SRmtctlInfo& info,dm::ECause& cause );
    bool getSelfActionInfo( index_t idx,SActionInfo& info,dm::ECause& cause );

    bool reload( dm::ECause& cause );
    bool unload( dm::ECause& cause );
private:
    SModelInfo m_info;
};

}
}
}


#endif /* _DM_SCADA_MODEL_MODEL_HPP_ */

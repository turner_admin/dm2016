/*
 * model_mgr.hpp
 *
 *  Created on: 2023年9月17日
 *      Author: Dylan.Gao
 */

#ifndef _DM_SCADA_MODEL_MODEL_MGR_HPP_
#define _DM_SCADA_MODEL_MODEL_MGR_HPP_

#include <dm/export.hpp>

#ifndef DM_API_SCADA
#define DM_API_SCADA DM_API_IMPORT
#endif

#include <dm/scada/types.hpp>
#include <dm/scada/model/model_info.hpp>
#include <dm/os/shared_buffer_mgr.hpp>

namespace dm{
namespace scada{
namespace model{

class DM_API_SCADA CModel;

/**
 * 模型管理器类
 */
class DM_API_SCADA CModelMgr{
    CModelMgr();
    CModelMgr( const CModelMgr& );
public:
    typedef dm::os::TCSharedBufferMgr<SModelInfo> buf_mgr_t;
    typedef buf_mgr_t::CPos pos_t;

    static CModelMgr& ins();

    ~CModelMgr();

    size_t size();

    void reset();

    bool get( index_t idx,CModel& model,dm::ECause& cause );

    bool find( id_t id,CModel& model,dm::ECause& cause );
    bool find( const char* name,CModel& model,dm::ECause& cause );
    bool find( const name_t& name,CModel& model,dm::ECause& cause );

    bool load( id_t id,dm::ECause& cause);
    bool load( id_t id,CModel& model,dm::ECause& cause );

    bool reload( id_t id,dm::ECause& cause );
    bool reload( CModel& model,dm::ECause& cause );
    bool reload( id_t id,CModel& model,dm::ECause& cause );

    bool unload( id_t id,dm::ECause& cause );
    bool unload( const char* name,dm::ECause& cause );
private:
    buf_mgr_t* m_bufMgr;
};

}
}
}



#endif /* _DM_SCADA_MODEL_MODEL_MGR_HPP_ */

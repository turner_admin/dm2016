#ifndef _DM_SCADA_MODEL_LOADER_HPP_
#define _DM_SCADA_MODEL_LOADER_HPP_

#include <dm/export.hpp>

#ifndef DM_API_SCADA
#define DM_API_SCADA DM_API_IMPORT
#endif

#include <dm/scada/model/model_mgr.hpp>
#include <dm/os/db/db.hpp>
#include <dm/scoped_ptr.hpp>
#include <vector>

namespace dm{
namespace scada{
namespace model{

/**
 * 模型加载器
*/
class DM_API_SCADA CLoader:public CModelMgr::buf_mgr_t::CLoader{
public:
    CLoader( dm::os::CDb& db );

    bool setAll();
    bool setId( const id_t& id );
    bool setName( const char* name );

    bool addALoaded( SModelInfo& info );
    bool filterLoaded();

    bool next();
    bool loadRecord( SModelInfo& info );
private:
    dm::os::CDb& m_db;
    dm::TScopedPtr<dm::os::CResultSet> m_resultSet;

    std::vector<id_t> m_loaded;
};

}
}
}

#endif
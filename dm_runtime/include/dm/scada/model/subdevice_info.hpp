#ifndef _DM_SCADA_MODEL_SUBDEVICE_INFO_HPP_
#define _DM_SCADA_MODEL_SUBDEVICE_INFO_HPP_

#include <dm/scada/types.hpp>

namespace dm{
namespace scada{
namespace model{

/**
 * 子设备信息
*/
struct SSubDeviceInfo{
    id_t id;
    magic_t magic;

    location_t location;
    sound_t sound;
};

}
}
}

#endif
#ifndef _DM_SCADA_MODEL_DISCRETE_DESC_HPP_
#define _DM_SCADA_MODEL_DISCRETE_DESC_HPP_

#include <dm/scada/types.hpp>

namespace dm{
namespace scada{
namespace model{

/**
 * 离散量描述信息
*/
struct SDiscreteDesc{
    desc_t desc;
    sound_t sound;
    level_t level;
};

}
}
}

#endif
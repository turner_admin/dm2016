﻿/*
 * actiondescinfo.hpp
 *
 *  Created on: 2017年11月21日
 *      Author: work
 */

#ifndef _DM_SCADA_ACTIONDESCINFO_HPP_
#define _DM_SCADA_ACTIONDESCINFO_HPP_

#include <dm/scada/types.hpp>

namespace dm{
namespace scada{

/**
 * 动作描述信息
 */
struct SActionDescInfo{
	id_t id;			//!< id号
	desc_t desc;	//!< 描述
};

}
}

#endif /* DM_RUNTIME_INCLUDE_DM_SCADA_ACTIONDESCINFO_HPP_ */

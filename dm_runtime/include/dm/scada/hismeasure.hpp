﻿/*
 * hismeasure.hpp
 *
 *  Created on: 2017年7月10日
 *      Author: work
 */

#ifndef _DM_SCADA_HISMEASURE_HPP_
#define _DM_SCADA_HISMEASURE_HPP_

#include <dm/timestamp.hpp>
#include <dm/scada/hisrcd_measure.hpp>

namespace dm{
namespace scada{

/**
 * 历史测量值管理类
 */
class CHisMeasure{
	CHisMeasure( const CHisMeasure& );
	CHisMeasure& operator=( const CHisMeasure& );

public:
	CHisMeasure();

	bool getBegin( dm::CTimeStamp& ts )const;
	bool getEnd( dm::CTimeStamp& ts )const;

	void append( const SHisRcdMeasure);
};

}
}

#endif /* INCLUDE_DM_SCADA_HISMEASURE_HPP_ */

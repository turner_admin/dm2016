﻿/*
 * statusdescinfo.hpp
 *
 *  Created on: 2017年3月23日
 *      Author: work
 */

#ifndef _DM_SCADA_STATUSDESCINFO_HPP_
#define _DM_SCADA_STATUSDESCINFO_HPP_

#include <dm/scada/types.hpp>

namespace dm{
namespace scada{

struct SStatusDescInfo{
	id_t id;
	desc_t desc;
};

}
}

#endif /* INCLUDE_DM_SCADA_STATUSDESCINFO_HPP_ */

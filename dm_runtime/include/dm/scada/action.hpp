﻿/*
 * action.hpp
 *
 *  Created on: 2017年12月2日
 *      Author: work
 */

#ifndef _DM_SCADA_ACTION_HPP_
#define _DM_SCADA_ACTION_HPP_

#include <dm/types.hpp>

namespace dm{
namespace scada{

/**
 * 动作数据
 * 使用双点表示，与状态量类似。但是没有实时值。
 * 动作描述的是一个瞬态信息。与CStatus描述的状态有本质的区别。
 * @see class CStatus
 */
class CAction{
public:
	typedef dm::uint8 value_t;	//!< 值类型

	enum{
		InvOff = 0x00,	//!< 值编码：无效分
		Off,			//!< 值编码：分
		On,				//!< 值编码：合
		InvOn,			//!< 值编码：无效合
		Other			//!< 无效值
	};
};

}
}

#endif /* _DM_SCADA_ACTION_HPP_ */

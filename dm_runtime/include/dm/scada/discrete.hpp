﻿/*
 * discrete.hpp
 *
 *  Created on: 2017年3月27日
 *      Author: work
 */

#ifndef _DM_SCADA_DISCRETE_HPP_
#define _DM_SCADA_DISCRETE_HPP_

#include <dm/export.hpp>

#ifndef DM_API_SCADA
#define DM_API_SCADA DM_API_IMPORT
#endif

#include <dm/types.hpp>

namespace dm{
namespace scada{

/**
 * 离散量
 */
class DM_API_SCADA CDiscrete{
public:
	typedef dm::uint16 value_t;	//!< 值类型
	typedef dm::uint16 raw_t;		//!< 原始值类型

	/**
	 * 构造函数
	 * @param v
	 */
	CDiscrete( const value_t& v=0 ):m_v(v),m_r(v){
	}

	CDiscrete( const value_t& v,const raw_t& r ):m_v(v),m_r(r){
	}

	CDiscrete( const CDiscrete& v ):m_v(v.m_v),m_r(v.m_r){
	}

	CDiscrete( const volatile CDiscrete& v ):m_v(v.m_v),m_r(v.m_r){
	}

	inline CDiscrete& operator=( const CDiscrete& v ){
		m_v = v.m_v;
		m_r = v.m_r;
		return *this;
	}

	inline CDiscrete& operator=( volatile const CDiscrete& v ){
		m_v = v.m_v;
		m_r = v.m_r;
		return *this;
	}

	/**
	 * 比较操作
	 * 只比较值。
	 * @param v
	 * @return
	 */
	inline bool operator==( const CDiscrete& v )const{
		return m_v==v.m_v;
	}

	inline bool operator==( volatile const CDiscrete& v )const{
		return m_v==v.m_v;
	}

	inline const value_t& getValue()const{
		return m_v;
	}

	inline void setValue( const value_t& v ){
		m_v = v;
	}

	inline const raw_t& getRaw()const{
		return m_r;
	}

	inline void setRaw( const raw_t& r ){
		m_r = r;
	}

	bool update( const CDiscrete& v );
	bool update( const CDiscrete& v )volatile;

private:
	value_t m_v;	//!< 值
	raw_t m_r;		//!< 原始值
};

}
}

#endif /* INCLUDE_DM_SCADA_DISCRETE_HPP_ */

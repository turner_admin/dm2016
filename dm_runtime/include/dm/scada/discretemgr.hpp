﻿/*
 * discretemgr.hpp
 *
 *  Created on: 2017年3月27日
 *      Author: work
 */

#ifndef _DM_SCADA_DISCRETEMGR_HPP_
#define _DM_SCADA_DISCRETEMGR_HPP_

#include <dm/export.hpp>

#ifndef DM_API_SCADA
#define DM_API_SCADA DM_API_IMPORT
#endif

#include <dm/scada/scada.hpp>

#include <dm/scada/discreteinfo.hpp>
#include <dm/scada/eventmgr.hpp>
#include <dm/scada/discretedescmgr.hpp>

#include <iostream>

namespace dm{
namespace scada{

/**
 * 离散量管理器
 */
class DM_API_SCADA CDiscreteMgr:protected CScada{
	CDiscreteMgr( CScada& scada );
public:
	typedef CScada::discrete_t rt_t;	//!< 实时数据类型

	static CDiscreteMgr& ins();

	/**
	 * 离散量个数
	 * @return
	 */
	inline const int& size()const{
		return m_cfg.size();
	}

	/**
	 * 根据id获取离散量索引
	 * @param id
	 * @return
	 */
	inline index_t indexById( const id_t& id )const{
		return m_cfg.indexOf(id);
	}

	/**
	 * 根据名字获取离散量索引
	 * @param name
	 * @return
	 */
	inline index_t indexByName( const char* name )const{
		return m_cfg.indexOf(name);
	}

	/**
	 * 获取离散量信息的离散量索引号
	 * @param info
	 * @return
	 */
	inline index_t indexOfInfo( const SDiscreteInfo* info )const{
		return m_cfg.indexOf(info);
	}

	index_t indexOfRt( const rt_t* value )const;
	index_t indexOfRt( const volatile rt_t* value )const;

	/**
	 * 获取离散量信息
	 * @param idx 离散量索引
	 * @return
	 */
	inline const SDiscreteInfo* info( const index_t& idx )const{
		return m_cfg.at(idx);
	}

	/**
	 * 获取离散量id
	 * @param idx 离散量索引
	 * @return
	 */
	inline id_t id( const index_t& idx )const{
		const SDiscreteInfo* p = info(idx);
		return p?p->id:Id_Inv;
	}

	/**
	 * 获取离散量名字
	 * @param idx 离散量索引
	 * @return
	 */
	inline const char* name( const index_t& idx )const{
		const SDiscreteInfo* p = info(idx);
		return p?p->name.c_str():NULL;
	}

	/**
	 * 获取离散量描述
	 * @param idx 离散量索引
	 * @return
	 */
	inline const char* desc( const index_t& idx )const{
		const SDiscreteInfo* p = info(idx);
		return p?p->desc.c_str():"无效索引";
	}

	bool setDesc( const index_t& idx,const char* str );

	/**
	 * 获取离散量状态个数
	 * @param idx 离散量索引
	 * @return
	 */
	inline int valueCount( const index_t& idx )const{
		const SDiscreteInfo* p = info(idx);
		return p?p->desc_num:0;
	}

	const char* valueDesc( const index_t& idx,const CDiscrete::value_t& value )const;

	index_t deviceIndex( const index_t& index )const;
	const char* deviceDesc( const index_t& index )const;

	bool setFlagSave( const index_t& idx,bool s=true );
	bool setFlagGenTimed( const index_t& idx,bool s=true );
	bool setFlagReportTimed( const index_t& idx,bool s=true );

	/**
	 * 获取离散量实时值指针
	 * @param idx 离散量索引
	 * @return
	 */
	inline const rt_t* rt( const index_t& idx )const{
		return (idx<0||idx>=size())?NULL:discretes()+idx;
	}

	/**
	 * 获取离散量实时值指针
	 * @param idx 离散量索引
	 * @return
	 */
	inline rt_t* rt( const index_t& idx ){
		return (idx<0||idx>=size())?NULL:discretes()+idx;
	}

	/**
	 * 获取离散量实时值
	 * @param idx 离散量索引
	 * @return
	 */
	inline CDiscrete::value_t rtValue( const index_t& idx ){
		rt_t* p = rt(idx);
		return p?p->getValue().getValue():0;
	}

	/**
	 * 获取离散量实时原始值
	 * @param idx 离散量索引
	 * @return
	 */
	inline CDiscrete::raw_t rtRaw( const index_t& idx ){
		rt_t* p = rt(idx);
		return p?p->getValue().getRaw():0;
	}

	/**
	 * 获取离散量实时数据原因
	 * @param idx 离散量索引
	 * @return
	 */
	inline EDataSource rtDs( const index_t& idx ){
		rt_t* p = rt(idx);
		return p?p->getDataSource():ByUnknow;
	}

	/**
	 * 获取离散量实时时标
	 * @param idx 离散量索引
	 * @return
	 */
	inline dm::CTimeStamp rtTs( const index_t& idx ){
		rt_t* p = rt(idx);
		return p?p->getTimeStamp():dm::CTimeStamp();
	}

	const char* rtDesc( const index_t& idx );

	bool update( const index_t& idx,const CDiscrete& value,const EDataSource& ds,const dm::CTimeStamp& ts=dm::CTimeStamp::cur() );

	bool set( const index_t& idx,const CDiscrete& value,const EDataSource& ds,const dm::CTimeStamp& ts=dm::CTimeStamp::cur() );

	bool gen( const index_t& idx,const CDiscrete& value,const dm::CTimeStamp& ts=dm::CTimeStamp::cur() );

	void printInfo( const index_t& idx,std::ostream& ostr=std::cout )const;
	void printValue( const index_t& idx,std::ostream& ostr=std::cout );
	void printInfoValue( const index_t& idx,std::ostream& ostr=std::cout );

protected:
	TCfg<SDiscreteInfo> m_cfg;
};

}
}

#endif /* INCLUDE_DM_SCADA_DISCRETEMGR_HPP_ */

﻿/*
 * logicdevice.hpp
 *
 *  Created on: 2017年11月25日
 *      Author: work
 */

#ifndef _DM_SCADA_LOGICDEVICE_HPP_
#define _DM_SCADA_LOGICDEVICE_HPP_

#include <dm/export.hpp>

#ifndef DM_API_SCADA
#define DM_API_SCADA DM_API_IMPORT
#endif

#include <dm/scada/scada.hpp>
#include <dm/scada/logicdeviceinfo.hpp>

namespace dm{
namespace scada{

class DM_API_SCADA CEvent;
class DM_API_SCADA CTimedMeasure;
class DM_API_SCADA CTimedCumulant;

/**
 * 逻辑设备类
 */
class DM_API_SCADA CLogicDevice{
	CLogicDevice( const CLogicDevice& );
	CLogicDevice& operator=( const CLogicDevice& );
public:
	typedef CScada::status_t status_t;
	typedef CScada::discrete_t discrete_t;
	typedef CScada::measure_t measure_t;
	typedef CScada::cumulant_t cumulant_t;

	CLogicDevice( const id_t& id=Id_Inv );
	CLogicDevice( const char* name );

	bool initByIndex( const index_t& idx );
	bool initById( const id_t& id );
	bool initByName( const char* name );

	/**
	 * 本实例是否有效
	 * 用于测试是否初始化成功
	 * @return
	 */
	inline bool isValiable()const{
		return m_info!=NULL;
	}

	inline const SLogicDeviceInfo* info()const{
		return m_info;
	}
	
	/**
	 * 逻辑设备的状态量个数
	 * @return
	 */
	inline index_t statusSize()const{
		return m_info==NULL?0:m_info->sizeOfStatus;
	}

	/**
	 * 逻辑设备的离散量个数
	 * @return
	 */
	inline index_t discreteSize()const{
		return m_info==NULL?0:m_info->sizeOfDiscrete;
	}

	/**
	 * 逻辑设备的测量量个数
	 * @return
	 */
	inline index_t measureSize()const{
		return m_info==NULL?0:m_info->sizeOfMeasure;
	}

	/**
	 * 逻辑设备的累计量个数
	 * @return
	 */
	inline index_t cumulantSize()const{
		return m_info==NULL?0:m_info->sizeOfCumulant;
	}

	/**
	 * 逻辑设备的远控个数
	 * @return
	 */
	inline index_t remoteCtlSize()const{
		return m_info==NULL?0:m_info->sizeOfRemoteCtl;
	}

	/**
	 * 逻辑设备的参数个数
	 * @return
	 */
	inline index_t parameterSize()const{
		return m_info==NULL?0:m_info->sizeOfParameter;
	}

	/**
	 * 逻辑设备的动作个数
	 * @return
	 */
	inline index_t actionSize()const{
		return m_info==NULL?0:m_info->sizeOfAction;
	}

	const SStatusInfo* statusInfo( const index_t& idx )const;
	const SDiscreteInfo* discreteInfo( const index_t& idx )const;
	const SMeasureInfo* measureInfo( const index_t& idx )const;
	const SCumulantInfo* cumulantInfo( const index_t& idx )const;
	const SRemoteCtlInfo* remoteCtlInfo( const index_t& idx )const;
	const SParaInfo* parameterInfo( const index_t& idx )const;
	const SActionInfo* actionInfo( const index_t& idx )const;

	// 从逻辑设备映射到物理设备。Idx_Inv无效
	index_t physicalStatus( const index_t& logic )const;
	index_t physicalDiscrete( const index_t& logic )const;
	index_t physicalMeasure( const index_t& logic )const;
	index_t physicalCumulant( const index_t& logic )const;
	index_t physicalRemoteCtl( const index_t& logic )const;
	index_t physicalParameter( const index_t& logic )const;
	index_t physicalAction( const index_t& logic )const;

	// 从物理设备映射到逻辑设备。Idx_Inv无效
	index_t logicalStatus( const index_t& physic )const;
	index_t logicalDiscrete( const index_t& physic )const;
	index_t logicalMeasure( const index_t& physic )const;
	index_t logicalCumulant( const index_t& physic )const;
	index_t logicalRemoteCtl( const index_t& physic )const;
	index_t logicalParameter( const index_t& physic )const;
	index_t logicalAction( const index_t& physic )const;

	bool chg2logicEvent( CEvent& ev )const;
	bool chg2logicMeasure( CTimedMeasure& tm )const;
	bool chg2logicCumulant( CTimedCumulant& tc )const;

	/**
	 * 获取物理值
	 * @param idx
	 * @return
	 */
	const status_t* status( const index_t& idx )const;
	const discrete_t* discrete( const index_t& idx )const;
	const measure_t* measure( const index_t& idx )const;
	const cumulant_t* cumulant( const index_t& idx )const;

private:
	const SLogicDeviceInfo* m_info;	//!< 逻辑设备信息

	const index_t* m_statusMap;		//!< 逻辑设备到物理状态量映射表
	const index_t* m_discreteMap;		//!< 逻辑设备到物理离散量映射表
	const index_t* m_measureMap;		//!< 逻辑设备到物理测量量映射表
	const index_t* m_cumulantMap;		//!< 逻辑设备到物理累计量映射表
	const index_t* m_remoteCtlMap;	//!< 逻辑设备到物理远控映射表
	const index_t* m_parameterMap;	//!< 逻辑设备到物理参数映射表
	const index_t* m_actionMap;		//!< 逻辑设备到物理动作映射表

	const index_t* m_2logicStatus;	//!< 物理状态量到逻辑设备的映射表
	const index_t* m_2logicDiscrete;	//!< 物理离散量到逻辑设备的映射表
	const index_t* m_2logicMeasure;	//!< 物理测量量到逻辑设备的映射表
	const index_t* m_2logicCumulant;	//!< 物理累计量到逻辑设备的映射表
	const index_t* m_2logicRemoteCtl; 	//!< 物理远控到逻辑设备的映射表
	const index_t* m_2logicParameter;	//!< 物理参数到逻辑设备的映射表
	const index_t* m_2logicAction;		//!< 物理动作到逻辑设备的映射表
};

}
}

#endif /* DM_RUNTIME_INCLUDE_DM_SCADA_LOGICDEVICE_HPP_ */

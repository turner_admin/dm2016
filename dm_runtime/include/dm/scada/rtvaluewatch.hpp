﻿/*
 * rtvaluewatch.hpp
 *
 *  Created on: 2017年3月30日
 *      Author: work
 */

#ifndef _DM_SCADA_RTVALUEWATCH_HPP_
#define _DM_SCADA_RTVALUEWATCH_HPP_

#include <dm/scada/rtvalue.hpp>

namespace dm{
namespace scada{

template<typename T>
class TCRtValueWatch{
public:
	typedef TCRtValue<T> rt_t;

	TCRtValueWatch( rt_t* o=NULL ):m_p(o){
	}

	inline void setPointer( rt_t* p ){
        m_p = p;
	}

	inline rt_t* getPointer()const{
		return m_p;
	}

	inline const T& getValue()const{
		return m_v;
	}

	inline const dm::CTimeStamp& getTs()const{
		return m_ts;
	}

	inline const EDataSource& getDs()const{
		return m_ds;
	}

	inline bool refresh(){
		return m_p?m_p->checkAndLoad(m_v,m_ds,m_ts):false;
	}

    inline void reload(){
        if( m_p )
            m_p->load(m_v,m_ds,m_ts);
    }

private:
	rt_t* m_p;

	T m_v;
	dm::CTimeStamp m_ts;
	EDataSource m_ds;
};

}
}

#endif /* INCLUDE_DM_SCADA_RTVALUEWATCH_HPP_ */

#ifndef _DM_SCADA_SIGNALS_PARAMETER_INFO_HPP_
#define _DM_SCADA_SIGNALS_PARAMETER_INFO_HPP_

#ifndef DM_API_SCADA
#define DM_API_SCADA DM_API_IMPORT
#endif

#include <dm/scada/types.hpp>

namespace dm{
namespace scada{
namespace signals{

/**
 * 参数信息
*/
struct DM_API_SCADA SParameterInfo{
    /**
     * 参数值类型
    */
    enum EType:uint8{
        TUnknow = 0u,
        TInt8,
        TUint8,
        TInt16,
        TUint16,
        TInt32,
        TUint32,
        TInt64,
        TUint64,
        TFloat32,
        TFloat64,
        TString
    };

    id_t    id;
    name_t  name;
    desc_t desc;

    sound_t sound;

    EType type;
    uint8 len;  // 参数长度
};

}
}    
}

#endif
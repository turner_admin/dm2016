#ifndef _DM_SCADA_SIGNALS_DISCRETE_DESC_HPP_
#define _DM_SCADA_SIGNALS_DISCRETE_DESC_HPP_

#ifndef DM_API_SCADA
#define DM_API_SCADA DM_API_IMPORT
#endif

#include <dm/scada/types.hpp>

namespace dm{
namespace scada{
namespace signals{

/**
 * 离散量值描述
*/
struct DM_API_SCADA SDiscreteDesc{
    desc_t desc;
    sound_t sound;
};

}
}    
}

#endif
#ifndef _DM_SCADA_SIGNALS_RMTCTL_INFO_HPP_
#define _DM_SCADA_SIGNALS_RMTCTL_INFO_HPP_

#ifndef DM_API_SCADA
#define DM_API_SCADA DM_API_IMPORT
#endif

#include <dm/scada/types.hpp>

namespace dm{
namespace scada{
namespace signals{

/**
 * 远控信息
*/
struct DM_API_SCADA SRmtctlInfo{
    id_t    id;
    name_t  name;
    desc_t desc;
    desc_t descPreset;
    desc_t descOpen;
    desc_t descClose;
    desc_t descReset;

    sound_t sound;
    sound_t soundPreset;
    sound_t soundOpen;
    sound_t soundClose;
    sound_t soundReset;
};

}
}    
}

#endif
#ifndef _DM_SCADA_SIGNALS_MEASURE_INFO_HPP_
#define _DM_SCADA_SIGNALS_MEASURE_INFO_HPP_

#ifndef DM_API_SCADA
#define DM_API_SCADA DM_API_IMPORT
#endif

#include <dm/scada/types.hpp>

namespace dm{
namespace scada{
namespace signals{

/**
 * 测量量信息
*/
struct DM_API_SCADA SMeasureInfo{
    id_t    id;
    name_t  name;
    desc_t desc;

    sound_t sound;
};

}
}    
}

#endif
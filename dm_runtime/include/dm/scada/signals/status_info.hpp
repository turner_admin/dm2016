#ifndef _DM_SCADA_SIGNALS_STATUS_INFO_HPP_
#define _DM_SCADA_SIGNALS_STATUS_INFO_HPP_

#ifndef DM_API_SCADA
#define DM_API_SCADA DM_API_IMPORT
#endif

#include <dm/scada/types.hpp>

namespace dm{
namespace scada{
namespace signals{

/**
 * 状态量信息
*/
struct DM_API_SCADA SStatusInfo{
    id_t    id;
    name_t  name;
    desc_t desc;
    desc_t descInvOff;
    desc_t descOff;
    desc_t descOn;
    desc_t descInvOn;

    sound_t sound;
    sound_t soundInvOff;
    sound_t soundOff;
    sound_t soundOn;
    sound_t soundInvOn;

    SStatusInfo& operator=( const SStatusInfo& info ){
    	id = info.id;
    	name = info.name;
    	desc = info.desc;
    	descInvOff = info.descInvOff;
    	descOff = info.descOff;
    	descOn = info.descOn;
    	descInvOn = info.descInvOn;

    	sound = info.sound;
    	soundInvOff = info.soundInvOff;
    	soundOff = info.soundOff;
    	soundOn = info.soundOn;
    	soundInvOn = info.soundInvOn;

    	return *this;
    }
};

}
}    
}

#endif

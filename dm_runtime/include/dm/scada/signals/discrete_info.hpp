#ifndef _DM_SCADA_SIGNALS_DISCRETE_INFO_HPP_
#define _DM_SCADA_SIGNALS_DISCRETE_INFO_HPP_

#ifndef DM_API_SCADA
#define DM_API_SCADA DM_API_IMPORT
#endif

#include <dm/scada/types.hpp>

namespace dm{
namespace scada{
namespace signals{

/**
 * 离散量信息
*/
struct DM_API_SCADA SDiscreteInfo{
    id_t    id;
    name_t  name;
    desc_t desc;

    sound_t sound;

    index_t posOfValues;
    size_t sizeOfValues;
};

}
}    
}

#endif
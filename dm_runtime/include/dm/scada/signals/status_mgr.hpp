#ifndef _DM_SCADA_SIGNALS_STATUS_MGR_HPP_
#define _DM_SCADA_SIGNALS_STATUS_MGR_HPP_

#include <dm/export.hpp>
#ifndef DM_API_SCADA
#define DM_API_SCADA DM_API_IMPORT
#endif

#include <dm/scada/types.hpp>
#include <dm/scada/signals/status_info.hpp>
#include <dm/os/shared_buffer_mgr.hpp>

namespace dm{
namespace scada{
namespace signals{

/**
 * 状态量管理器
*/
class DM_API_SCADA CStatusMgr{
    CStatusMgr();

public:
    enum{

#ifndef SCADA_SIGNAL_STATUS_SIZE_OF_MASK
        SizeOfMask = 8,
#else
        SizeOfMask = SCADA_SIGNAL_STATUS_SIZE_OF_MASK,
#endif
    };

    typedef SStatusInfo record_t;
    typedef dm::os::TCSharedBufferMgr<record_t,id_t,dm::uint32,SizeOfMask,MaxLenOfName,size_t,index_t> buf_mgr_t;
    typedef buf_mgr_t::CPos pos_t;

    static CStatusMgr& ins();

    /**
     * 复位共享数据
    */
    void reset();

    /**
     * 初始化表
    */
    static bool init();

    inline size_t size(){
    	return m_bufMgr.count();
    }

    inline pos_t first(){
    	return m_bufMgr.first();
    }

    pos_t first( SStatusInfo& info );

    inline pos_t last(){
    	return m_bufMgr.last();
    }

    pos_t last( SStatusInfo& info );

    inline pos_t next( const pos_t& pos ){
    	return m_bufMgr.next(pos);
    }

    pos_t next( const pos_t& pos,SStatusInfo& info );

    inline pos_t previous( const pos_t& pos ){
    	return m_bufMgr.previous(pos);
    }

    pos_t previous( const pos_t& pos,SStatusInfo& info );

    bool loadNews();

    pos_t loadNew( SStatusInfo& info );

    bool reload( id_t id );
    bool reload( id_t id,pos_t& pos );
    bool reload( id_t id,SStatusInfo& info );
    bool reload( id_t id,pos_t& pos,SStatusInfo& info );

    bool reload( const pos_t& pos );
    bool reload( const pos_t& pos,SStatusInfo& info );

    bool unload( const pos_t& pos );
    bool unload( id_t id );
    bool unload( const char* name );

    bool find( id_t id,pos_t& pos );
    bool find( id_t id,SStatusInfo& info );
    bool find( id_t id,pos_t& pos,SStatusInfo& info );

    bool find( const char* name,pos_t& pos );
    bool find( const char* name,SStatusInfo& info );
    bool find( const char* name,pos_t& pos,SStatusInfo& info );

    bool get( const pos_t& pos,SStatusInfo& info );
    bool get( index_t idx,offset_t offset,pos_t& pos,SStatusInfo& info );

private:
    buf_mgr_t m_bufMgr;
};

}
}
}

#endif

﻿/*
 * timedmeasure.hpp
 *
 *  Created on: 2016年11月26日
 *      Author: work
 */

#ifndef _DM_SCADA_TIMEDMEASURE_HPP_
#define _DM_SCADA_TIMEDMEASURE_HPP_

#include <dm/export.hpp>

#ifndef DM_API_SCADA
#define DM_API_SCADA DM_API_IMPORT
#endif

#include <dm/scada/types.hpp>
#include <dm/scada/measure.hpp>
#include <dm/timestamp.hpp>

namespace dm{
namespace scada{

class DM_API_SCADA CTimedMeasure{
public:
	typedef CMeasure::value_t value_t;
	typedef dm::CTimeStamp ts_t;

	CTimedMeasure( const index_t& idx=0,const value_t& value=0,const ts_t& ts=ts_t::cur() );
	CTimedMeasure( const CTimedMeasure& );
	CTimedMeasure& operator=( const CTimedMeasure& );

	inline const index_t& getIndex()const{
		return m_idx;
	}

	inline void setIndex( const index_t& idx ){
		m_idx = idx;
	}

	inline const value_t& getValue()const{
		return m_value;
	}

	inline void setValue( const value_t& value ){
		m_value = value;
	}

	inline const ts_t& getTimeStamp()const{
		return m_ts;
	}

	inline ts_t& getTimeStamp(){
		return m_ts;
	}

	inline void setTimeStamp( const ts_t& ts ){
		m_ts = ts;
	}
private:
	index_t m_idx;
	value_t m_value;
	ts_t m_ts;
};

}
}


#endif /* INCLUDE_DM_SCADA_TIMEDMEASURE_HPP_ */

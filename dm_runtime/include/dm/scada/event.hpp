﻿/*
 * eventqueue.hpp
 *
 *  Created on: 2016年11月17日
 *      Author: work
 */

#ifndef _DM_SCADA_EVENT_HPP_
#define _DM_SCADA_EVENT_HPP_

#include <dm/export.hpp>

#ifndef DM_API_SCADA
#define DM_API_SCADA DM_API_IMPORT
#endif

#include <dm/types.hpp>
#include <dm/timestamp.hpp>
#include <dm/scada/types.hpp>
#include <dm/scada/status.hpp>
#include <dm/scada/discrete.hpp>
#include <dm/scada/action.hpp>

namespace dm{
namespace scada{

/**
 * 事件
 */
class DM_API_SCADA CEvent{
public:
	/**
	 * 事件类型
	 */
	enum EType{
		Unknow = 0,		    //!< 测试事件
		Device,			        //!< 设备事件
		Status,			        //!< 状态事件
		Discrete,		        //!< 离散量事件
		Action,			        //!< 动作事件
		MeasureAlarm,	    //!< 遥测事件
		CumulantAlarm,	//!< 表码事件
		Rmtctl,
		TypeMax
	};
	typedef dm::CTimeStamp ts_t;	//!< 时标类型

	CEvent( const EType& type=Unknow,const index_t& idx=0,const ts_t& t=ts_t::cur(),const index_t& deviceIdx=-1 );
	CEvent( const CEvent& e );
	CEvent& operator=( const CEvent& e );

	ts_t 	ts;	//!< 时标
	EType 	type;	//!< 事件类型
	index_t deviceIdx;	//!< 设备索引号
	index_t	idx;	//!< 索引号

	union{
		dm::uint8 device;	// 0:未出现
		CStatus::value_t status;		//!< 事件Status时，本字段代表变化后的状态值
		CDiscrete::value_t discrete;	//!< 事件Discrete时，本字段代表离散量变化后的值
		CAction::value_t action;		//!< 事件Action时，本字段代表动作的值
		dm::int8 measure;					//!< 事件MeasureAlarm时， 0:恢复，<0:下限级别 >0:上限级别
		dm::uint8 cumulant;				//!< 事件CumulantAlarm时， 0:复位 1:溢出
		dm::uint8 rmtctl;
		dm::uint16 all;
	}value;	//!< 值
};

}
}

#endif /* INCLUDE_DM_SCADA_EVENT_HPP_ */

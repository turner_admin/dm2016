﻿/*
 * devicemgr.hpp
 *
 *  Created on: 2017年11月30日
 *      Author: work
 */

#ifndef _DM_SCADA_DEVICEMGR_HPP_
#define _DM_SCADA_DEVICEMGR_HPP_

#include <dm/export.hpp>

#ifndef DM_API_SCADA
#define DM_API_SCADA DM_API_IMPORT
#endif

#include <dm/scada/scada.hpp>
#include <dm/scada/deviceinfo.hpp>
#include <dm/scada/cfg.hpp>
#include <iostream>

namespace dm{
namespace scada{

/**
 * 设备管理类
 */
class DM_API_SCADA CDeviceMgr:protected CScada{
	CDeviceMgr( CScada& scada );
public:
	static CDeviceMgr& ins();

	/**
	 * 获取设备数量
	 * @return
	 */
	inline index_t size()const{
		return m_cfg.size();
	}

	/**
	 * 根据Id获取设备索引
	 * @param id
	 * @return
	 */
	inline index_t indexById( const id_t& id )const{
		return m_cfg.indexOf(id);
	}

	/**
	 * 根据名字获取设备索引
	 * @param name
	 * @return
	 */
	inline index_t indexByName( const char* name )const{
		return m_cfg.indexOf(name);
	}

	/**
	 * 获取设备信息对应的设备索引
	 * @param info
	 * @return
	 */
	inline index_t indexOfInfo( const SDeviceInfo* info )const{
		return m_cfg.indexOf(info);
	}

	index_t indexOfState( const CDeviceState* state )const;
	index_t indexOfState( const volatile CDeviceState* state )const;

	/**
	 * 获取设备信息
	 * @param idx
	 * @return
	 */
	inline const SDeviceInfo* info( const index_t& idx )const{
		return m_cfg.at(idx);
	}

	/**
	 * 获取设备id
	 * @param idx
	 * @return
	 */
	inline id_t id( const index_t& idx )const{
		const SDeviceInfo* p = info(idx);
		return p?p->id:Id_Inv;
	}

	/**
	 * 获取设备名字
	 * @param idx
	 * @return
	 */
	inline const char* name( const index_t& idx )const{
		const SDeviceInfo* p = info(idx);
		return p?p->name.c_str():NULL;
	}

	/**
	 * 获取设备描述
	 * @param idx
	 * @return
	 */
	inline const char* desc( const index_t& idx )const{
		const SDeviceInfo* p = info(idx);
		return p?p->desc.c_str():"无效索引";
	}

	bool setDesc( const index_t& idx,const char* str );

	const CDeviceState* state( const index_t& idx )const;

	CDeviceState* state( const index_t& idx );

	bool isAlive( const index_t& idx )const;

	void print( const index_t& idx,std::ostream& ostr=std::cout )const;

private:
	TCfg<SDeviceInfo> m_cfg;
};

}
}

#endif /* _DM_SCADA_DEVICEMGR_HPP_ */

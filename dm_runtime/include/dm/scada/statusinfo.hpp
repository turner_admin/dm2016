﻿/*
 * statusinfo.hpp
 *
 *  Created on: 2016年11月15日
 *      Author: work
 */

#ifndef INCLUDE_DM_SCADA_STATUSINFO_HPP_
#define INCLUDE_DM_SCADA_STATUSINFO_HPP_

#include <dm/scada/types.hpp>
#include <dm/bits.hpp>

namespace dm{
namespace scada{

struct SStatusInfo{
	/**
	 * 标志位
	 */
	enum EFlag{
		FUse = 0,		//!< 是否使用
		FSave,   		//!< 是否存盘
		FGenTimed,		//!< 是否产生时标数据
		FReportTimed,	//!< 是否上报时标数据

		 FNone
	};

	/**
	 * 是否存盘
	 * @return
	 */
	inline bool isFlagSave()const{
		return dm::bit_get(flag,FSave);
	}

	/**
	 * 设置存盘标志
	 */
	inline void setFlagSave(){
		flag = dm::bit_set(flag,FSave);
	}

	/**
	 * 清除存盘标志
	 */
	inline void clrFlagSave(){
		flag = dm::bit_clr(flag,FSave);
	}

	inline void setFlagSave( const bool& s ){
		if( s )
			setFlagSave();
		else
			clrFlagSave();
	}

	/**
	 * 是否产生时标数据
	 * @return
	 */
	inline bool isFlagGenTimed()const{
		return dm::bit_get(flag,FGenTimed);
	}

	inline void setFlagGenTimed(){
		flag = dm::bit_set(flag,FGenTimed);
	}

	inline void clrFlagGenTimed(){
		flag = dm::bit_clr(flag,FGenTimed);
	}

	inline void setFlagGenTimed( const bool& s ){
		if( s )
			setFlagGenTimed();
		else
			clrFlagGenTimed();
	}

	/**
	 * 是否上报时标数据
	 * @return
	 */
	inline bool isFlagReportTimed()const{
		return dm::bit_get(flag,FReportTimed);
	}

	/**
	 * 设置上报时标数据标志
	 */
	inline void setFlagReportTimed(){
		flag = dm::bit_set(flag,FReportTimed);
	}

	inline void clrFlagReportTimed(){
		flag = dm::bit_clr(flag,FReportTimed);
	}

	/**
	 * 设置上报时标数据标志
	 * @param s
	 */
	inline void setFlagReportTimed( const bool& s ){
		if( s )
			setFlagReportTimed();
		else
			clrFlagReportTimed();
	}

	/**
	 * 状态量级别
	 */
	enum ELevel{
		Normal = 0,//!< Normal 正常
		Info,      //!< Info 信息
		Warn,      //!< Warn 告警
		Exception, //!< Exception 异常
		Error,     //!< Error 错误
		Danguer,   //!< Danguer 危险
		LevelMax   //!< LevelMax
	};

	id_t id;		//!< ID
	name_t name;	//!< 名字
	desc_t desc;	//!< 秒杀

	int desc_invOff;	//!< 无效分状态描述
	int desc_off;		//!< 分状态描述
	int desc_on;		//!< 合状态描述
	int desc_invOn;		//!< 无效合状态描述

	ELevel level;			//!< 级别

	flag_t flag;			//!< 标志
};

}
}

#endif /* INCLUDE_DM_SCADA_STATUSINFO_HPP_ */

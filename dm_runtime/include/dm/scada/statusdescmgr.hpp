﻿/*
 * statusdescmgr.hpp
 *
 *  Created on: 2017年12月1日
 *      Author: work
 */

#ifndef _DM_SCADA_STATUSDESCMGR_HPP_
#define _DM_SCADA_STATUSDESCMGR_HPP_

#include <dm/export.hpp>

#ifndef DM_API_SCADA
#define DM_API_SCADA DM_API_IMPORT
#endif

#include <dm/scada/scada.hpp>
#include <dm/scada/statusdescinfo.hpp>
#include <dm/scada/cfg.hpp>

namespace dm{
namespace scada{

class DM_API_SCADA CStatusDescMgr:protected CScada{
protected:
	CStatusDescMgr( CScada& scada );
public:
	static CStatusDescMgr& ins();

	inline const index_t& size()const{
		return m_cfg.size();
	}

	inline index_t indexById( const id_t& id )const{
		return m_cfg.indexOf(id);
	}

	inline index_t indexOfInfo( const SStatusDescInfo* info )const{
		return m_cfg.indexOf(info);
	}

	inline const SStatusDescInfo* info( const index_t& idx )const{
		return m_cfg.at(idx);
	}

	inline id_t id( const index_t& idx )const{
		const SStatusDescInfo* p = info(idx);
		return p?p->id:Id_Inv;
	}

	inline const char* desc( const index_t& idx )const{
		const SStatusDescInfo* p = info(idx);
		return p?p->desc.c_str():"无效索引";
	}

	bool setDesc( const index_t& idx,const char* str );
private:
	TCfg<SStatusDescInfo> m_cfg;
};

}
}

#endif /* DM_RUNTIME_INCLUDE_DM_SCADA_STATUSDESCMGR_HPP_ */

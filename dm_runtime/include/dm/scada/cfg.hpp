﻿/*
 * cfg.hpp
 *
 *  Created on: 2017年3月23日
 *      Author: work
 */

#ifndef _DM_SCADA_CFG_HPP_
#define _DM_SCADA_CFG_HPP_

#include <dm/scada/types.hpp>

namespace dm{
namespace scada{

/**
 * 配置信息模板类
 */
template<typename TInfo>
class TCfg{
	TCfg& operator=( const TCfg& );
public:
	/**
	 * 构造函数
	 * @param info 信息缓冲区
	 * @param size 信息记录数
	 */
	TCfg( TInfo* info=NULL,const index_t& size=0 ):m_size(size),m_infos(info){
	}

	TCfg( TCfg& cfg):m_size(cfg.m_size),m_infos(cfg.m_infos){
	}

	/**
	 * 记录数
	 * @return
	 */
	inline const index_t& size()const{
		return m_size;
	}

	const TInfo* at( const index_t& idx )const;
	TInfo* at( const index_t& idx );

	/**
	 * 直接访问。
	 * 注意，如果idx越界，可能会导致程序崩溃。不确定idx范围时，建议使用at()函数。
	 * @param idx
	 * @return
	 */
	inline const TInfo& operator[]( const index_t& idx )const{
		return m_infos[idx];
	}

	const TInfo* byId( const id_t& id,index_t offset=0 )const;
	const TInfo* byName( const char* name,index_t offset=0 )const;

	index_t indexOf( const TInfo* info )const;
	index_t indexOf( const id_t& id,index_t offset=0 )const;
	index_t indexOf( const char* name,index_t offset=0 )const;
protected:
	index_t m_size;
	TInfo* m_infos;
};

/**
 * 获取指定索引的信息
 * @param idx
 * @return
 */
template<typename TInfo>
const TInfo* TCfg<TInfo>::at( const index_t& idx )const{
	if( idx<0 || idx>=m_size )
		return NULL;
	return m_infos+idx;
}

template<typename TInfo>
TInfo* TCfg<TInfo>::at( const index_t& idx ){
	if( idx<0 || idx>=m_size )
		return NULL;
	return m_infos+idx;
}

/**
 * 根据id查找信息
 * @param id
 * @param offset 查找的起始偏移。
 * @return
 */
template<typename TInfo>
const TInfo* TCfg<TInfo>::byId( const id_t& id,index_t offset )const{
	for( ;offset<m_size;++offset )
		if( m_infos[offset].id==id )
			return m_infos+offset;
	return NULL;
}

/**
 * 根据名字来查找
 * @param name
 * @param offset
 * @return
 */
template<typename TInfo>
const TInfo* TCfg<TInfo>::byName( const char* name ,index_t offset )const{
	for( ;offset<m_size;++offset )
		if( m_infos[offset].name==name )
			return m_infos+offset;
	return NULL;
}

/**
 * 获取信息的索引
 * @param info
 * @return
 */
template<typename TInfo>
index_t TCfg<TInfo>::indexOf( const TInfo* info )const{
	if( info==NULL || info<m_infos )
		return -1;

	return info - m_infos;
}

/**
 * 获取id的索引
 * @param id
 * @param offset
 * @return
 */
template<typename TInfo>
index_t TCfg<TInfo>::indexOf( const id_t& id,index_t offset )const{
	for( ;offset<m_size;++offset )
		if( m_infos[offset].id==id )
			return offset;
	return -1;
}

/**
 * 获取指定名字的索引
 * @param name
 * @param offset
 * @return
 */
template<typename TInfo>
index_t TCfg<TInfo>::indexOf( const char* name ,index_t offset )const{
	for( ;offset<m_size;++offset )
		if( m_infos[offset].name==name )
			return offset;
	return -1;
}

}
}

#endif /* INCLUDE_DM_SCADA_CFG_HPP_ */

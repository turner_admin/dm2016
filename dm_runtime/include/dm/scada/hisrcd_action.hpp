﻿/*
 * hisrcd_action.hpp
 *
 *  Created on: 2017年7月10日
 *      Author: work
 */

#ifndef _DM_SCADA_HISRCD_ACTION_HPP_
#define _DM_SCADA_HISRCD_ACTION_HPP_

#include <dm/scada/hisrcd.hpp>
#include <dm/scada/action.hpp>

namespace dm{
namespace scada{

/**
 * 历史状态量记录信息
 */
typedef THisRecord<CAction::value_t> SHisRcdAction;

}
}

#endif /* INCLUDE_DM_SCADA_HISRCD_STATUS_HPP_ */

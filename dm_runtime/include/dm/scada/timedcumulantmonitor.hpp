﻿/*
 * timedcumulantmonitor.hpp
 *
 *  Created on: 2017年4月14日
 *      Author: work
 */

#ifndef _DM_SCADA_TIMEDCUMULANTMONITOR_HPP_
#define _DM_SCADA_TIMEDCUMULANTMONITOR_HPP_

#include <dm/export.hpp>

#ifndef DM_API_SCADA
#define DM_API_SCADA DM_API_IMPORT
#endif

#include <dm/scada/timedcumulantmgr.hpp>

namespace dm{
namespace scada{

class DM_API_SCADA CTimedCumulantMonitor{
public:
	typedef CTimedCumulantMgr::cumulant_queue_t::ringpos_t ringpos_t;

	CTimedCumulantMonitor();
	CTimedCumulantMonitor( const ringpos_t* p );
	virtual ~CTimedCumulantMonitor();

	inline const ringpos_t& pos()const{
		return m_p;
	}

	virtual void onNew( const ringpos_t& pos,const CTimedCumulant& info,const bool& overflow )const;
	virtual bool filter( const ringpos_t& pos,const CTimedCumulant& info,const bool& overflow )const;

	void run();

	bool tryGet( CTimedCumulant& info,bool& overflow );
	void get( CTimedCumulant& info,bool& overflow );

	void reset();

private:
	CTimedCumulantMgr& m_mgr;
	ringpos_t m_p;
};

}
}

#endif /* INCLUDE_DM_SCADA_TIMEDCUMULANTMONITOR_HPP_ */

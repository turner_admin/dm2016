﻿/*
 * types.hpp
 *
 *  Created on: 2016年11月12日
 *      Author: work
 */

#ifndef _DM_SCADA_TYPES_HPP_
#define _DM_SCADA_TYPES_HPP_

#include <cstdlib>
#include <dm/types.hpp>
#include <dm/fixdstring.hpp>
#include <dm/simplebuf.hpp>
#include <dm/magic.hpp>

namespace dm{
namespace scada{

typedef dm::int32 index_t;	//!< 索引类型
typedef dm::int16 offset_t;	//!< 偏移类型
typedef dm::uint32 id_t;		//!< id类型

typedef dm::uint8 flag_t;	//!< 标志类型

static const index_t Idx_Inv = -1;	//!< 无效索引

static const id_t Id_Inv = 0xFFFFFFFF;	//!< 无效ID

enum{
#ifdef SCADA_MAX_LEN_OF_NAME
	MaxLenOfName = SCADA_MAX_LEN_OF_NAME,
#else
	MaxLenOfName  = 64,	//!< 对象名称最大长度
#endif

#ifdef SCADA_MAX_LEN_OF_DESC
	MaxLenOfDesc = SCADA_MAX_LEN_OF_DESC,
#else
	MaxLenOfDesc    = 64, 	//!< 对象描述最大长度
#endif

#ifdef SCADA_MAX_LEN_OF_LOCATION
	MaxLenOfLocation = SCADA_MAX_LEN_OF_LOCATION,
#else
	MaxLenOfLocation = 64,
#endif

#ifdef SCADA_MAX_LEN_OF_SOUND
	MaxLenOfSound = SCADA_MAX_LEN_OF_SOUND,
#else
	MaxLenOfSound    = 128, 	//!< 对象语音最大长度
#endif

	MaxLenOfUnit	= 24	//!< 物理量单位最大长度
};

typedef dm::CFixdString<MaxLenOfName> name_t;
typedef dm::CFixdString<MaxLenOfDesc> desc_t;
typedef dm::CFixdString<MaxLenOfSound> sound_t;
typedef dm::CFixdString<MaxLenOfLocation> location_t;
typedef dm::CFixdString<MaxLenOfUnit> unit_t;

typedef dm::CMagic::magic_t magic_t;

typedef dm::int8 level_t;	//!< 告警级别

typedef dm::uint8 signal_flags_t;	// 信号标志

}
}


#endif /* INCLUDE_DM_SCADA_TYPES_HPP_ */

﻿/*
 * counter.hpp
 *
 *  Created on: 2017年7月15日
 *      Author: work
 */

#ifndef _DM_COUNTER_HPP_
#define _DM_COUNTER_HPP_

namespace dm{

/**
 * 计数器
 */
template<typename T>
class TCounter{
	TCounter( const TCounter& );
public:
	TCounter( const T& c=0 ):m_c(c),m_of(false){
	}

	/**
	 * 增加计数器
	 * @param c
	 * @return
	 */
	inline TCounter& operator+( const T& c ){
		m_c += c;
		if( m_c<c )
			m_of = true;
		return *this;
	}

	inline TCounter& operator++(){
		++m_c;
		if( m_c==0 )
			m_of = true;
		return *this;
	}

	inline TCounter& operator+=( const T& c ){
		m_c += c;
		if( m_c<c )
			m_of = true;
		return *this;
	}

	inline const T& counter()const{
		return m_c;
	}

	inline const bool& overFlow()const{
		return m_of;
	}

	operator T(){
		return m_c;
	}

	/**
	 * 复位计数器
	 * @param i 初始值
	 */
	inline void reset( const T& i=0 ){
		m_c = i;
		m_of = false;
	}
private:
	T m_c;
	bool m_of;
};

}

#endif /* INCLUDE_DM_COUNTER_HPP_ */

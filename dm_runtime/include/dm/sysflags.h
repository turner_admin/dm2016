/*
 * sysflags.h
 *
 *  Created on: 2015��7��23��
 *      Author: work
 */

#ifndef SYSFLAGS_H_
#define SYSFLAGS_H_

#include <bitset.hpp>
#include "types.hpp"

namespace NDm{
	template<typename T>
	class CSysFlags {
	public:
		CSysFlags():m_flags(){
		}

		inline void reset(){
			m_flags = 0;
		}

		inline bool isFlag( const unsigned short& flag )const{
			return m_flags.isSet(flag);
		}

		inline void setFlag( const unsigned short& flag ){
			m_flags.set(flag);
		}

		inline void clrFlag( const unsigned short& flag ){
			m_flags.clr(flag);
		}

	private:
		CBits<T> m_flags;
	};
}

#endif /* SYSFLAGS_H_ */

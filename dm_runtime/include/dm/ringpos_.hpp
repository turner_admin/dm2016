
template<typename TRingPos>
inline bool operator==( const TRingPos& p )const{
	return ringpos::isEquare(*this,p);
}

template<typename TRingPos>
inline bool operator!=( const TRingPos& p )const{
	return ringpos::isNotEquare(*this,p);
}

inline RINGPOS& operator++(){
	return *this += 1;
}

inline RINGPOS& operator++(int){
	return *this += 1;
}

inline RINGPOS& operator--(){
	return *this -= 1;
}

inline RINGPOS& operator--(int){
	return *this -= 1;
}

inline RINGPOS& operator+=( const Tp& n){
	return moveRight(n);
}

inline RINGPOS& operator-=( const Tp& n ){
	return moveLeft(n);
}

inline RINGPOS operator+( const Tp& n )const{
	return right(n);
}

inline RINGPOS operator-( const Tp& n )const{
	return left(n);
}

inline RINGPOS right( const Tp& n,const Tc& c=0 )const{
	RINGPOS rt(*this);
	return rt.moveRight(n,c);
}

inline RINGPOS left( const Tp& n,const Tc& c=0 )const{
	RINGPOS rt(*this);
	return rt.moveLeft(n,c);
}

inline bool isNoMoreThanACycle( const RINGPOS& p )const{
	return ringpos::isNoMoreThanACycle(*this,p);
}

inline bool isNoLessThanACycle( const RINGPOS& p )const{
	return ringpos::isNoLessThanACycle(*this,p);
}

inline Tp operator-( const RINGPOS& p )const{
	return ringpos::distance<RINGPOS,Tp>(*this,p);
}

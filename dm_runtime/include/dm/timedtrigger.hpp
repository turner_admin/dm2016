﻿/*
 * timedtrigger.hpp
 *
 *  Created on: 2018年2月18日
 *      Author: work
 */

#ifndef _DM_TIMEDTRIGGER_HPP_
#define _DM_TIMEDTRIGGER_HPP_

#include <dm/export.hpp>

#ifndef DM_API_MISC
#define DM_API_MISC DM_API_IMPORT
#endif

#include <dm/datetime.hpp>
#include <dm/timestamp.hpp>

namespace dm{

/**
 * 基于时钟的触发器
 */
class DM_API_MISC CTimedTrigger{
public:
	/**
	 * 触发类型
	 */
	enum ETriggerType{
		TtNone = 0,	 //!< 禁止触发
		TtSingle,    //!< 单次触发
		TtContinus,  //!< 连续触发

		TtSecondly,  //!< 每秒触发
		TtMinutely,  //!< 每分钟触发
		TtHourly,    //!< 每小时触发
		TtDaily,     //!< 每天触发
		TtMonthly,   //!< 每月触发
		TtYearly,    //!< 每年触发

		TtSundays,   //!< 周日触发
		TtMondays,   //!< 周一触发
		TtTuesdays,  //!< 周二触发
		TtWednesdays,//!< 周三触发
		TtThursdays, //!< 周四触发
		TtFridays,   //!< 周五触发
		TtSaturdays, //!< 周六触发

		TtUnknow     //!< 未定义
	};

	CTimedTrigger( const ETriggerType& type=TtNone,const CDateTime& start=CDateTime(),const dm::CTimeStamp& now=dm::CTimeStamp::cur() );
	CTimedTrigger( const CTimedTrigger& trigger );

	CTimedTrigger& operator=( const CTimedTrigger& trigger );

	inline const ETriggerType& getTriggerType()const{
		return m_tt;
	}

	inline const CTimeStamp& getLastTime()const{
		return m_lastTime;
	}

	inline const CDateTime& getStartTime()const{
		return m_startTime;
	}

	inline const CTimeStamp& getNextTime()const{
		return m_nextTime;
	}

	inline void setTriggerType( const ETriggerType& type ){
		m_tt = type;
	}

	inline void setStartTime( const CDateTime& dt ){
		m_startTime = dt;
	}

	void reset( const dm::CTimeStamp& now=dm::CTimeStamp::cur() );

	bool ifTimeUp( const dm::CTimeStamp& now=dm::CTimeStamp::cur() );

	bool prepairNext( const dm::CTimeStamp& now=dm::CTimeStamp::cur() );

	inline const char* typeString()const{
		return typeString(m_tt);
	}

	static const char* typeString( const ETriggerType& type );

protected:
	static int getDaysByWeek( const CDateTime::EWeekDay& today,const CDateTime::EWeekDay& dday );
private:
	ETriggerType m_tt;		//!< 触发类型
	CDateTime m_startTime;	//!< 开始时刻
	CTimeStamp m_lastTime;	//!< 上次触发时刻
	CTimeStamp m_nextTime;	//!< 下次触发时刻
};

}

#endif /* DM_RUNTIME_INCLUDE_DM_TIMEDTRIGGER_HPP_ */

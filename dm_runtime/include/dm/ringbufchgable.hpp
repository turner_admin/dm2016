/*
 * ringbufchgable.hpp
 *
 *  Created on: 2015-7-1
 *      Author: work
 */

#ifndef RINGBUFCHGABLE_HPP_
#define RINGBUFCHGABLE_HPP_

#include "ringbuf.hpp"

namespace Dm{

template<typename Tv>
class TRingBufChangeable:public TRingBuf<Tv>{
	TRingBufChangeable(){
		m_buf = 0;
		m_size = 0;
	}

	void setSize( unsigned int size ){
		if( m_buf!=0 )
			delete [] m_buf;
		m_size = size;
		m_buf = new Tv[m_size];
	}

	const unsigned int& getSize()const{
		return m_size;
	}

	inline unsigned int getLen( const CPos& p )const{
		return m_pos.minus(p,m_size);
	}

	unsigned int getData( Tv* buf,unsigned int size,const CPos& p )const{
		int i= getLen(p);
		if( i<size )
			size = i;
		for( i=0;i<size;++i )
			getData(buf[i],p.plus(i,m_size));
		return size;
	}

protected:
	inline const Tv& dataAt( const unsigned int& i )const{
		return m_buf[i];
	}

	inline virtual Tv& dataAt( const unsigned int& i ){
		return m_buf[i];
	}

private:
	Tv *m_buf;
	unsigned int m_size;
};
};


#endif /* RINGBUFCHGABLE_HPP_ */

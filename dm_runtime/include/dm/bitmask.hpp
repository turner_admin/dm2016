/*
 * bitmask.hpp
 *
 *  Created on: 2016��3��3��
 *      Author: dylan
 */

#ifndef DM_BITMASK_HPP_
#define DM_BITMASK_HPP_

namespace dm{

template<typename T,int i>
class BitMaskSet{
public:
	static const T v ;
};

template<typename T,int i>
const T BitMaskSet<T,i>::v = ((T)1<<i);

template<typename T,int i>
class BitMaskClr{
public:
	static const T v;
};

template<typename T,int i>
const T BitMaskClr<T,i>::v = ~((T)1<<i);

template<typename Tv,int i,typename Tmask>
inline Tmask getBit( const Tv& v ){
	return v&(BitMaskSet<Tmask,i>::v);
}

template<typename Tv,int i>
inline Tv getBit( const Tv& v ){
        return v&(BitMaskSet<Tv,i>::v);
}

template<typename Tv,int i,typename Tmask>
inline bool isBitSet( const Tv& v ){
	return 0!=(v&(BitMaskSet<Tmask,i>::v));
}

template<typename Tv,int i>
inline bool isBitSet( const Tv& v ){
        return 0!=(v&(BitMaskSet<Tv,i>::v));
}

template<typename Tv,int i,typename Tmask>
inline void bitSet( Tv& v ){
	v |= BitMaskSet<Tmask,i>::v;
}

template<typename Tv,int i>
inline void bitSet( Tv& v ){
        v |= BitMaskSet<Tv,i>::v;
}

template<typename Tv,int i,typename Tmask>
inline void bitClr( Tv& v ){
	v &= BitMaskClr<Tmask,i>::v;
}

template<typename Tv,int i>
inline void bitClr( Tv& v ){
        v &= BitMaskClr<Tv,i>::v;
}

template<typename Tv,int i,typename Tmask>
inline void bitSet( Tv& v,const Tmask& vplus ){
	v = ((v|BitMaskSet<Tmask,i>::v)|vplus);
}

template<typename Tv,int i>
inline void bitSet( Tv& v,const Tv& vplus ){
        v = ((v|BitMaskSet<Tv,i>::v)|vplus);
}

template<typename Tv,int i,typename Tmask>
inline void bitClr( Tv& v,const Tmask& vplus ){
	v = ((v&BitMaskClr<Tmask,i>::v)|vplus);
}

template<typename Tv,int i>
inline void bitClr( Tv& v,const Tv& vplus ){
        v = ((v&BitMaskClr<Tv,i>::v)|vplus);
}

}

#endif /* DM_BITMASK_HPP_ */

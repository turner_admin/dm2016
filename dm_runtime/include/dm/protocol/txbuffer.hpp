﻿/*
 * txbuffer.hpp
 *
 *  Created on: 2017年2月4日
 *      Author: work
 */

#ifndef _DM_PROTOCOL_TXBUFFER_HPP_
#define _DM_PROTOCOL_TXBUFFER_HPP_

#include <dm/simplebuf.hpp>
#include <dm/types.hpp>

namespace dm{
namespace protocol{

#ifndef TX_BUF_LEN
#define TX_BUF_LEN 2048
#endif

typedef dm::CSimpleBuf<dm::uint8,dm::uint,TX_BUF_LEN> CTxBuffer;

}
}



#endif /* INCLUDE_DM_FRAME_TXBUFFER_HPP_ */

﻿/*
 * modbustcpmaster.hpp
 *
 *  Created on: 2017年10月29日
 *      Author: work
 */

#ifndef _DM_PROTOCOL_MODBUSTCPMASTER_HPP_
#define _DM_PROTOCOL_MODBUSTCPMASTER_HPP_

#include <dm/export.hpp>

#ifndef DM_API_PROTOCOL
#define DM_API_PROTOCOL DM_API_IMPORT
#endif

#include <dm/protocol/modbustcp.hpp>
#include <dm/protocol/modbusmaster.hpp>

namespace dm{
namespace protocol{

class DM_API_PROTOCOL CModbusTcpMaster:virtual public CModbusTcp,virtual public CModbusMaster{
public:
	CModbusTcpMaster();
	virtual ~CModbusTcpMaster();

	inline const dm::uint16& getUnitFlag()const{
		return m_unitFlag;
	}

	inline void setUnitFlag( const dm::uint8& unit ){
		m_unitFlag = unit;
	}

	virtual void reset( const ts_t& ts,const rts_t& rts );

protected:
	virtual EAction taskStart_call( CFrameModbus& tf,const ts_t& ts,const rts_t& rts );
	virtual EAction taskDo_call( CFrameModbus& tf,const ts_t& ts,const rts_t& rts );

	virtual EAction taskStart_syncClock( CFrameModbus& tf,const ts_t& ts,const rts_t& rts );
	virtual EAction taskDo_syncClock( CFrameModbus& tf,const ts_t& ts,const rts_t& rts );

	virtual EAction taskStart_remoteControl( CFrameModbus& tf,const ts_t& ts,const rts_t& rts );
	virtual EAction taskDo_remoteControl( CFrameModbus& tf,const ts_t& ts,const rts_t& rts );

	virtual EAction taskStart_parameters( CFrameModbus& tf,const ts_t& ts,const rts_t& rts );
	virtual EAction taskDo_parameters( CFrameModbus& tf,const ts_t& ts,const rts_t& rts );

	virtual EAction taskStart_call( CFrameModbusTcp& tf,const ts_t& ts,const rts_t& rts );
	virtual EAction taskDo_call( CFrameModbusTcp& tf,const ts_t& ts,const rts_t& rts );

	virtual EAction taskStart_syncClock( CFrameModbusTcp& tf,const ts_t& ts,const rts_t& rts );
	virtual EAction taskDo_syncClock( CFrameModbusTcp& tf,const ts_t& ts,const rts_t& rts );

	virtual EAction taskStart_remoteControl( CFrameModbusTcp& tf,const ts_t& ts,const rts_t& rts );
	virtual EAction taskDo_remoteControl( CFrameModbusTcp& tf,const ts_t& ts,const rts_t& rts );

	virtual EAction taskStart_parameters( CFrameModbusTcp& tf,const ts_t& ts,const rts_t& rts );
	virtual EAction taskDo_parameters( CFrameModbusTcp& tf,const ts_t& ts,const rts_t& rts );

	EAction setFrameTransflag( const EAction& act,CFrameModbusTcp& tf );

protected:
	dm::uint16 m_transFlag;
	dm::uint16 m_unitFlag;
};

}
}

#endif /* DM_RUNTIME_INCLUDE_DM_PROTOCOL_MODBUSTCPMASTER_HPP_ */

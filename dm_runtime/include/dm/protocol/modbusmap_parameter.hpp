﻿/*
 * modbusmap_parameter.hpp
 *
 *  Created on: 2019-4-13
 *      Author: work
 */

#ifndef DM_PROTOCOL_MODBUSMAP_PARAMETER_HPP_
#define _DM_PROTOCOL_MODBUSMAP_PARAMETER_HPP_

#include <dm/export.hpp>

#ifndef DM_API_PROTOCOL
#define DM_API_PROTOCOL DM_API_IMPORT
#endif

#include <dm/protocol/modbusmapitem.hpp>

namespace dm{
namespace protocol{

class DM_API_PROTOCOL CModbusMapParameter:public CModbusMapItem{
public:
	CModbusMapParameter();

	bool init( const TiXmlElement* cfg,const bool& plc=false );

	inline const EDataType& type()const{
		return m_type;
	}
private:
	EDataType m_type;
};

}
}

#endif /* DM_RUNTIME_INCLUDE_DM_PROTOCOL_MODBUSMAP_PARAMETER_HPP_ */

﻿/*
 * modbusmap_rmtctl.hpp
 *
 *  Created on: 2019-4-13
 *      Author: work
 */

#ifndef _DM_PROTOCOL_MODBUSMAP_RMTCTL_HPP_
#define _DM_PROTOCOL_MODBUSMAP_RMTCTL_HPP_

#include <dm/export.hpp>

#ifndef DM_API_PROTOCOL
#define DM_API_PROTOCOL DM_API_IMPORT
#endif

#include <dm/protocol/modbusmapitem.hpp>

namespace dm{
namespace protocol{

class DM_API_PROTOCOL CModbusMapRmtctl:public CModbusMapItem{
public:
	CModbusMapRmtctl();

	bool init( const TiXmlElement* cfg,const bool& plc=false );

	inline const int& bitOffset()const{
		return m_bitOffset;
	}

	inline const int& bitLen()const{
		return m_bitLen;
	}

	inline const dm::uint16& valueOpen()const{
		return m_valueOpen;
	}

	inline const dm::uint16& valueClose()const{
		return m_valueClose;
	}

	inline const dm::uint16& valuePreOpen()const{
		return m_valuePreOpen;
	}

	inline const dm::uint16& valuePreClose()const{
		return m_valuePreClose;
	}

	inline const fun_t& refFun()const{
		return m_refFun;
	}

	inline const dm::uint16& refAddress()const{
		return m_refAddr;
	}

	inline const std::string& openDesc()const{
		return m_openDesc;
	}

	inline const std::string& closeDesc()const{
		return m_closeDesc;
	}

	inline const std::string& preOpenDesc()const{
		return m_preOpenDesc;
	}

	inline const std::string& preCloseDesc()const{
		return m_preCloseDesc;
	}

private:
	int m_bitOffset;
	int m_bitLen;

	dm::uint16 m_valueOpen;
	dm::uint16 m_valueClose;
	dm::uint16 m_valuePreOpen;
	dm::uint16 m_valuePreClose;

	fun_t m_refFun;
	dm::uint16 m_refAddr;

	std::string m_openDesc;
	std::string m_closeDesc;
	std::string m_preOpenDesc;
	std::string m_preCloseDesc;
};

}
}

#endif /* DM_RUNTIME_INCLUDE_DM_PROTOCOL_MODBUSMAP_RMTCTL_HPP_ */

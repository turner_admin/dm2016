﻿/*
 * modbusmapitem.hpp
 *
 *  Created on: 2019-4-13
 *      Author: work
 */

#ifndef _DM_PROTOCOL_MODBUSMAPITEM_HPP_
#define _DM_PROTOCOL_MODBUSMAPITEM_HPP_

#include <dm/export.hpp>

#ifndef DM_API_PROTOCOL
#define DM_API_PROTOCOL DM_API_IMPORT
#endif

#include <dm/protocol/framemodbus.hpp>
#include <string>
#include <tinyxml.h>

namespace dm{
namespace protocol{

/**
 * Modbus映射对象
 */
class DM_API_PROTOCOL CModbusMapItem{
public:
	typedef CFrameModbus::EFunc fun_t;

	/**
	 * 对象数据类型
	 * 按位取值的使用DtUint16类型
	 */
	enum EDataType{
		DtInt16,
		DtUint16,

		DtInt32,
		DtInt32Inv,	// Inverse 低字在先，否则，高字在先
		DtUint32,
		DtUint32Inv,

		DtInt64,
		DtInt64Inv,
		DtUint64,
		DtUint64Inv,

		DtFloat32,
		DtFloat32Inv,

		DtFloat64,
		DtFloat64Inv,

		DtBcdUint16,
		DtBcdUint32,
		DtBcdUint32Inv,
		DtBcdUint64,
		DtBcdUint64Inv,

		DtUnknow
	};

	static const char* typeString( const EDataType& type );
	static int typeWords( const EDataType& type );

	CModbusMapItem();
	virtual ~CModbusMapItem();

	inline const std::string& name()const{
		return m_name;
	}

	inline const std::string& desc()const{
		return m_desc;
	}

	inline const fun_t& fun()const{
		return m_fun;
	}

	inline const dm::uint16& address()const{
		return m_addr;
	}

	virtual bool init( const TiXmlElement* cfg,const bool& plc=false );

protected:
	std::string m_name;
	std::string m_desc;

	fun_t m_fun;
	dm::uint16 m_addr;
};

}
}

#endif /* DM_RUNTIME_INCLUDE_DM_PROTOCOL_MODBUSMAPITEM_HPP_ */

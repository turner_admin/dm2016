﻿/*
 * iec104slaver.hpp
 *
 *  Created on: 2017年5月7日
 *      Author: work
 */

#ifndef _DM_PROTOCOL_IEC104SLAVER_HPP_
#define _DM_PROTOCOL_IEC104SLAVER_HPP_

#include <dm/export.hpp>

#ifndef DM_API_PROTOCOL
#define DM_API_PROTOCOL DM_API_IMPORT
#endif

#include <dm/protocol/iec104.hpp>

namespace dm{
namespace protocol{

class DM_API_PROTOCOL CIec104Slaver:virtual public CIec104{
public:
	CIec104Slaver();
	virtual ~CIec104Slaver();

protected:
	EAction dealStopDtConf( const ts_t& ts,const rts_t& rts );
	EAction dealStopDt( const ts_t& ts,const rts_t& rts );
	EAction dealStartDtConf( const ts_t& ts,const rts_t& rts );
	EAction dealStartDt( const ts_t& ts,const rts_t& rts );

	// 召唤
	virtual EAction dealAsdu_C_IC_NA_1( const frame_apdu_t& rf,frame_apdu_t& tf,const ts_t& ts,const rts_t& rts );

	// 时钟同步
	virtual EAction dealAsdu_C_CS_NA_1( const frame_apdu_t& rf,frame_apdu_t& tf,const ts_t& ts,const rts_t& rts );

	// 远控
	virtual EAction dealAsdu_C_DC_NA_1( const frame_apdu_t& rf,frame_apdu_t& tf,const ts_t& ts,const rts_t& rts );

	// 参数读
	virtual EAction dealAsdu_C_RD_NA_1( const frame_apdu_t& rf,frame_apdu_t& tf,const ts_t& ts,const rts_t& rts );

	// 参数写
	virtual EAction dealAsdu_C_SE_NB_1( const frame_apdu_t& rf,frame_apdu_t& tf,const ts_t& ts,const rts_t& rts );

	virtual EAction taskEnd_call( const ts_t &ts,const rts_t &rts);
};

}
}

#endif /* INCLUDE_DM_PROTOCOL_IEC104SLAVER_HPP_ */

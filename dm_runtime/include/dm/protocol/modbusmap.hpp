﻿/*
 * modbusmap.hpp
 *
 *  Created on: 2019-4-13
 *      Author: work
 */

#ifndef _DM_PROTOCOL_MODBUSMAP_HPP_
#define _DM_PROTOCOL_MODBUSMAP_HPP_

#include <dm/export.hpp>

#ifndef DM_API_PROTOCOL
#define DM_API_PROTOCOL DM_API_IMPORT
#endif

#include <tinyxml.h>
#include <vector>
#include <string>

#include <dm/protocol/modbusregs.hpp>
#include <dm/protocol/modbusmap_status.hpp>
#include <dm/protocol/modbusmap_discrete.hpp>
#include <dm/protocol/modbusmap_measure.hpp>
#include <dm/protocol/modbusmap_cumulant.hpp>
#include <dm/protocol/modbusmap_parameter.hpp>
#include <dm/protocol/modbusmap_rmtctl.hpp>

namespace dm{
namespace protocol{

/**
 * Modbus映射器
 */
class DM_API_PROTOCOL CModbusMap{
public:
	typedef std::vector<CModbusMapStatus*> statuses_t;
	typedef std::vector<CModbusMapDiscrete*> discretes_t;
	typedef std::vector<CModbusMapMeasure*> measures_t;
	typedef std::vector<CModbusMapCumulant*> cumulants_t;
	typedef std::vector<CModbusMapParameter*> parameters_t;
	typedef std::vector<CModbusMapRmtctl*> rmtctls_t;

	CModbusMap();
	~CModbusMap();

	bool init( const char* file,const bool& debug=false );

	inline const std::string& title()const{
		return m_title;
	}

	inline const std::string& desc()const{
		return m_desc;
	}

	inline const bool& ifPlcAddress()const{
		return m_plcAddress;
	}

	inline CModbusRegs& regs(){
		return m_regs;
	}

	inline const statuses_t& statuses()const{
		return m_statuses;
	}

	inline const discretes_t& discretes()const{
		return m_discretes;
	}

	inline const measures_t& measures()const{
		return m_measures;
	}

	inline const cumulants_t& cumulants()const{
		return m_cumulants;
	}

	inline const parameters_t& parameters()const{
		return m_parameters;
	}

	inline const rmtctls_t& rmtctls()const{
		return m_rmtctls;
	}

	bool isUpdatedStatus( int i )const;
	bool isUpdatedDiscrete( int i )const;
	bool isUpdatedMeasure( int i )const;
	bool isUpdatedCumulant( int i )const;

	dm::uint16 valueStatus( int i,bool debug=false )const;
	dm::uint16 valueDiscrete( int i,bool debug=false )const;
	dm::float32 valueMeasure( int i,bool debug=false )const;
	dm::uint64 valueCumulant( int i,bool debug=false )const;

private:
	std::string m_title;
	std::string m_desc;
	bool m_plcAddress;

	CModbusRegs m_regs;

	statuses_t m_statuses;
	discretes_t m_discretes;
	measures_t m_measures;
	cumulants_t m_cumulants;
	parameters_t m_parameters;
	rmtctls_t m_rmtctls;
};

}
}

#endif /* DM_RUNTIME_INCLUDE_DM_PROTOCOL_MODBUSMAP_HPP_ */

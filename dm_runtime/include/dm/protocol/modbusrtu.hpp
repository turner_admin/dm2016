﻿/*
 * modbusrtu.hpp
 *
 *  Created on: 2017年3月14日
 *      Author: work
 */

#ifndef _DM_PROTOCOL_MODBUSRTU_HPP_
#define _DM_PROTOCOL_MODBUSRTU_HPP_

#include <dm/export.hpp>

#ifndef DM_API_PROTOCOL
#define DM_API_PROTOCOL DM_API_IMPORT
#endif

#include <dm/protocol/framemodbusrtu.hpp>
#include <dm/protocol/modbus.hpp>

namespace dm{
namespace protocol{

/**
 * ModbusRTU规约基类
 * @see CModbusRtuSlaver 和 CModbusRtuMaster
 */
class DM_API_PROTOCOL CModbusRtu:virtual public CModbus{
public:
	typedef CFrameModbusRtu::address_t address_t;

	CModbusRtu();
	virtual ~CModbusRtu();

	inline const address_t& getModbusAddr()const{
		return m_curAddr;
	}

	inline void setModbusAddr( const address_t& addr ){
		m_curAddr = addr;
	}

	bool filterRxFrame()const;

protected:
	CFrameModbus& getTxModbusFrame();
	CFrameModbus& getRxModbusFrame();

protected:
	address_t m_curAddr;	// 当前modbus地址，子站地址或者本地地址

	CFrameModbusRtu m_rxFrame;
	CFrameModbusRtu m_txFrame;
};

}
}



#endif /* INCLUDE_DM_OS_PROTOCOL_MODBUSRTU_HPP_ */

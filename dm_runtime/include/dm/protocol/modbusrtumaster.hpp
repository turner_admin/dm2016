﻿/*
 * modbusrtmaster.hpp
 *
 *  Created on: 2017年3月14日
 *      Author: work
 */

#ifndef _DM_PROTOCOL_MODBUSRTMASTER_HPP_
#define _DM_PROTOCOL_MODBUSRTMASTER_HPP_

#include <dm/export.hpp>

#ifndef DM_API_PROTOCOL
#define DM_API_PROTOCOL DM_API_IMPORT
#endif

#include <dm/protocol/modbusrtu.hpp>
#include <dm/protocol/modbusmaster.hpp>

namespace dm{
namespace protocol{

/**
 * Modbus主站类
 * 定时主动发送数据，并等待反馈
 */
class DM_API_PROTOCOL CModbusRtuMaster:virtual public CModbusRtu,virtual public CModbusMaster{
public:
	CModbusRtuMaster();
	virtual ~CModbusRtuMaster();
};

}
}

#endif /* INCLUDE_DM_PROTOCOL_MODBUSRTMASTER_HPP_ */

﻿/*
 * modbusmap_status.hpp
 *
 *  Created on: 2019-4-13
 *      Author: work
 */

#ifndef _DM_PROTOCOL_MODBUSMAP_STATUS_HPP_
#define _DM_PROTOCOL_MODBUSMAP_STATUS_HPP_

#include <dm/export.hpp>

#ifndef DM_API_PROTOCOL
#define DM_API_PROTOCOL DM_API_IMPORT
#endif

#include <dm/protocol/modbusmapdataitem.hpp>

namespace dm{
namespace protocol{

class DM_API_PROTOCOL CModbusMapStatus:public CModbusMapDataItem{
public:
	CModbusMapStatus();
	~CModbusMapStatus();

	bool init( const TiXmlElement* cfg,const bool& plc=false );

	inline const int& bitOffset()const{
		return m_bitOffset;
	}

	inline const int& bitLen()const{
		return m_bitLen;
	}

	inline const int& invalidValue()const{
		return m_invalidValue;
	}

	inline const dm::uint16& valueOn()const{
		return m_valueOn;
	}

	inline const dm::uint16& valueOff()const{
		return m_valueOff;
	}

	inline const dm::uint16& valueInvOn()const{
		return m_valueInvOn;
	}

	inline const dm::uint16& valueInvOff()const{
		return m_valueInvOff;
	}

	inline const std::string& onDesc()const{
		return m_onDesc;
	}

	inline const std::string& offDesc()const{
		return m_offDesc;
	}

	inline const std::string& invOnDesc()const{
		return m_invOnDesc;
	}

	inline const std::string& invOffDesc()const{
		return m_invOffDesc;
	}

	inline const int& level()const{
		return m_level;
	}
protected:
	int m_bitOffset;
	int m_bitLen;

	int m_invalidValue;	// 无效值。当值无效时，状态值 0,1,2,3. 4:丢弃

	dm::uint16 m_valueOn;
	dm::uint16 m_valueOff;
	dm::uint16 m_valueInvOn;
	dm::uint16 m_valueInvOff;

	std::string m_onDesc;
	std::string m_offDesc;
	std::string m_invOnDesc;
	std::string m_invOffDesc;

	int m_level;
};

}
}

#endif /* DM_RUNTIME_INCLUDE_DM_PROTOCOL_MODBUSMAP_STATUS_HPP_ */

﻿/*
 * iec104master.hpp
 *
 *  Created on: 2017年3月20日
 *      Author: work
 */

#ifndef _DM_PROTOCOL_IEC104MASTER_HPP_
#define _DM_PROTOCOL_IEC104MASTER_HPP_

#include <dm/export.hpp>
#ifndef DM_API_PROTOCOL
#define DM_API_PROTOCOL DM_API_IMPORT
#endif

#include <dm/protocol/iec104.hpp>

namespace dm{
namespace protocol{

class DM_API_PROTOCOL CIec104Master:virtual public CIec104{
public:
	CIec104Master();
	virtual ~CIec104Master();

protected:
	virtual EAction dealStopDtConf( const ts_t& ts,const rts_t& rts );
	virtual EAction dealStopDt( const ts_t& ts,const rts_t& rts );
	virtual EAction dealStartDtConf( const ts_t& ts,const rts_t& rts );
	virtual EAction dealStartDt( const ts_t& ts,const rts_t& rts );

	// 不带时标的双点信息
	virtual EAction dealAsdu_M_DP_NA_1( const frame_apdu_t& rf,frame_apdu_t& tf,const ts_t& ts,const rts_t& rts );

	// 不带时标的标度化值
	virtual EAction dealAsdu_M_ME_NB_1( const frame_apdu_t& rf,frame_apdu_t& tf,const ts_t& ts,const rts_t& rts );

	// 不带时标的测量值：短浮点数
	virtual EAction dealAsdu_M_ME_NC_1( const frame_apdu_t& rf,frame_apdu_t& tf,const ts_t& ts,const rts_t& rts );

	// 不带时标的累计量
	virtual EAction dealAsdu_M_IT_NA_1( const frame_apdu_t& rf,frame_apdu_t& tf,const ts_t& ts,const rts_t& rts );

	// 带时标的双点信息
	virtual EAction dealAsdu_M_DP_TB_1( const frame_apdu_t& rf,frame_apdu_t& tf,const ts_t& ts,const rts_t& rts );

	// 带时标的标度化值
	virtual EAction dealAsdu_M_ME_TE_1( const frame_apdu_t& rf,frame_apdu_t& tf,const ts_t& ts,const rts_t& rts );

	// 带时标的短浮点数
	virtual EAction dealAsdu_M_ME_TF_1( const frame_apdu_t& rf,frame_apdu_t& tf,const ts_t& ts,const rts_t& rts );

	// 带时标的累计量
	virtual EAction dealAsdu_M_IT_TB_1( const frame_apdu_t& rf,frame_apdu_t& tf,const ts_t& ts,const rts_t& rts );

	// 召唤返回
	virtual EAction dealAsdu_C_IC_NA_1( const frame_apdu_t& rf,frame_apdu_t& tf,const ts_t& ts,const rts_t& rts );

	// 时钟同步返回
	virtual EAction dealAsdu_C_CS_NA_1( const frame_apdu_t& rf,frame_apdu_t& tf,const ts_t& ts,const rts_t& rts );

	// 远控返回
	virtual EAction dealAsdu_C_DC_NA_1( const frame_apdu_t& rf,frame_apdu_t& tf,const ts_t& ts,const rts_t& rts );

	// 参数读返回
	virtual EAction dealAsdu_C_RD_NA_1( const frame_apdu_t& rf,frame_apdu_t& tf,const ts_t& ts,const rts_t& rts );

	// 参数写返回
	virtual EAction dealAsdu_C_SE_NB_1( const frame_apdu_t& rf,frame_apdu_t& tf,const ts_t& ts,const rts_t& rts );

	// 参数，标度化值
	virtual EAction dealAsdu_P_ME_NB_1( const frame_apdu_t& rf,frame_apdu_t& tf,const ts_t& ts,const rts_t& rts );

	//////////////////  接收到的数据处理  子类应该重载这些函数 保存到数据库 //////////////////

	// 双点信息
	virtual bool onRecv_3_M_DP_NA_1( const frame_apdu_t::ECause& cause,const dm::uint32& address,const frame_apdu_t::UDiq& diq,const ts_t& ts,const rts_t& rts );
	virtual bool onRecv_31_M_DP_TB_1( const frame_apdu_t::ECause& cause,const dm::uint32& address,const frame_apdu_t::UDiq& diq,const frame_apdu_t::UCp56Time2a& time,const ts_t& ts,const rts_t& rts );

	// 标度化值
	virtual bool onRecv_11_M_ME_NB_1( const frame_apdu_t::ECause& cause,const dm::uint32& address,const frame_apdu_t::USva& nva,const frame_apdu_t::UQds& qds,const ts_t& ts,const rts_t& rts );
	virtual bool onRecv_35_M_ME_TE_1( const frame_apdu_t::ECause& cause,const dm::uint32& address,const frame_apdu_t::USva& nva,const frame_apdu_t::UQds& qds,const frame_apdu_t::UCp56Time2a& time,const ts_t& ts,const rts_t& rts );

	// 短浮点数
	virtual bool onRecv_13_M_ME_NC_1( const frame_apdu_t::ECause& cause,const dm::uint32& address,const frame_apdu_t::UStd& frac,const frame_apdu_t::UQds& qds,const ts_t& ts,const rts_t& rts );
	virtual bool onRecv_36_M_ME_TF_1( const frame_apdu_t::ECause& cause,const dm::uint32& address,const frame_apdu_t::UStd& frac,const frame_apdu_t::UQds& qds,const frame_apdu_t::UCp56Time2a& time,const ts_t& ts,const rts_t& rts );

	// 累计量
	virtual bool onRecv_15_M_IT_NA_1( const frame_apdu_t::ECause& cause,const dm::uint32& address,const dm::int32& value,const ts_t& ts,const rts_t& rts );
	virtual bool onRecv_37_M_IT_TB_1( const frame_apdu_t::ECause& cause,const dm::uint32& address,const dm::int32& value,const frame_apdu_t::UCp56Time2a& time,const ts_t& ts,const rts_t& rts );

	// 测量值参数 标度化值
	virtual bool onRecv_111_P_ME_NA_1( const frame_apdu_t::ECause& cause,const dm::uint32& address,const frame_apdu_t::USva& sva,const frame_apdu_t::UQpm& qpm,const ts_t& ts,const rts_t& rts );


	/**
	 * 发送总召帧
	 * @note 这里的任务都是基于应用层的，在实现中不应该处理链路信息
	 * @param txFrame
	 * @return
	 */
	EAction taskStart_call( const ts_t& ts,const rts_t& rts );
	EAction taskDo_call( const ts_t& ts,const rts_t& rts );

	/**
	 * 发送对时命令
	 * @param txFrame
	 * @return
	 */
	EAction taskStart_syncClock( const ts_t& ts,const rts_t& rts );
	EAction taskDo_syncClock( const ts_t& ts,const rts_t& rts );

	/**
	 * 发送遥控命令
	 * @param txFrame
	 * @return
	 */
	EAction taskStart_remoteControl( const ts_t& ts,const rts_t& rts );
	EAction taskDo_remoteControl( const ts_t& ts,const rts_t& rts );

	/**
	 * 发送参数命令
	 * @param txFrame
	 * @return
	 */
	EAction taskStart_parameters( const ts_t& ts,const rts_t& rts );
	EAction taskDo_parameters( const ts_t& ts,const rts_t& rts );
};

}
}

#endif /* INCLUDE_DM_PROTOCOL_IEC104MASTER_HPP_ */

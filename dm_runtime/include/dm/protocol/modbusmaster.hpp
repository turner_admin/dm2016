/*
 * modbusmaster.hpp
 *
 *  Created on: 2024年3月5日
 *      Author: Dylan.Gao
 */

#ifndef _DM_PROTOCOL_MODBUSMASTER_HPP_
#define _DM_PROTOCOL_MODBUSMASTER_HPP_

#include <dm/export.hpp>

#ifndef DM_API_PROTOCOL
#define DM_API_PROTOCOL DM_API_IMPORT
#endif

#include <dm/protocol/modbus.hpp>

namespace dm{
namespace protocol{

/**
 * Modbus采集规约基类
 */
class DM_API_PROTOCOL CModbusMaster:virtual public CModbus{
public:
	CModbusMaster();
	virtual ~CModbusMaster();

	EAction dealRxFrame( const ts_t& ts,const rts_t& rts );

protected:
	virtual EAction taskStart_call( CFrameModbus& tf,const ts_t& ts,const rts_t& rts );
	virtual EAction taskDo_call( CFrameModbus& tf,const ts_t& ts,const rts_t& rts );

	virtual EAction taskStart_syncClock( CFrameModbus& tf,const ts_t& ts,const rts_t& rts );
	virtual EAction taskDo_syncClock( CFrameModbus& tf,const ts_t& ts,const rts_t& rts );

	virtual EAction taskStart_remoteControl( CFrameModbus& tf,const ts_t& ts,const rts_t& rts );
	virtual EAction taskDo_remoteControl( CFrameModbus& tf,const ts_t& ts,const rts_t& rts );

	virtual EAction taskStart_parameters( CFrameModbus& tf,const ts_t& ts,const rts_t& rts );
	virtual EAction taskDo_parameters( CFrameModbus& tf,const ts_t& ts,const rts_t& rts );

	virtual EAction dealRxResponse_readCoils( const CFrameModbus& rf,CFrameModbus& tf,const ts_t& ts,const rts_t& rts );
	virtual EAction dealRxResponse_readDiscreteInputs( const CFrameModbus& rf,CFrameModbus& tf,const ts_t& ts,const rts_t& rts );
	virtual EAction dealRxResponse_readHoldingRegisters( const CFrameModbus& rf,CFrameModbus& tf,const ts_t& ts,const rts_t& rts );
	virtual EAction dealRxResponse_readInputRegisters( const CFrameModbus& rf,CFrameModbus& tf,const ts_t& ts,const rts_t& rts );

	virtual EAction dealRxResponse_writeSingleCoil( const CFrameModbus& rf,CFrameModbus& tf,const ts_t& ts,const rts_t& rts );
	virtual EAction dealRxResponse_writeSingleRegister( const CFrameModbus& rf,CFrameModbus& tf,const ts_t& ts,const rts_t& rts );

	virtual EAction dealRxResponse_writeMultipleCoils( const CFrameModbus& rf,CFrameModbus& tf,const ts_t& ts,const rts_t& rts );
	virtual EAction dealRxResponse_writeMultipleRegisters( const CFrameModbus& rf,CFrameModbus& tf,const ts_t& ts,const rts_t& rts );

	virtual void coils_01_error( const dm::uint16& address,const CFrameModbus::EErrCode& ec,const ts_t& ts,const rts_t& rts );
	virtual void coils_01( const dm::uint16& address,const dm::uint8& value,const ts_t& ts,const rts_t& rts );

	virtual void discreteInputs_02_error( const dm::uint16& address,const CFrameModbus::EErrCode& ec,const ts_t& ts,const rts_t& rts );
	virtual void discreteInputs_02( const dm::uint16& address,const dm::uint8& value,const ts_t& ts,const rts_t& rts );

	virtual void holdingRegisters_03_error( const dm::uint16& address,const CFrameModbus::EErrCode& ec,const ts_t& ts,const rts_t& rts );
	virtual void holdingRegisters_03( const dm::uint16& address,const dm::uint16& value,const ts_t& ts,const rts_t& rts );

	virtual void inputRegisters_04_error( const dm::uint16& address,const CFrameModbus::EErrCode& ec,const ts_t& ts,const rts_t& rts );
	virtual void inputRegisters_04( const dm::uint16& address,const dm::uint16& value,const ts_t& ts,const rts_t& rts );

	EAction taskStart_call( const ts_t& ts,const rts_t& rts );

	EAction taskDo_call( const ts_t& ts,const rts_t& rts );

	EAction taskStart_syncClock( const ts_t& ts,const rts_t& rts );

	EAction taskDo_syncClock( const ts_t& ts,const rts_t& rts );

	EAction taskStart_remoteControl( const ts_t& ts,const rts_t& rts );
	EAction taskDo_remoteControl( const ts_t& ts,const rts_t& rts );


	EAction taskStart_parameters( const ts_t& ts,const rts_t& rts );
	EAction taskDo_parameters( const ts_t& ts,const rts_t& rts );
};

}
}

#endif /* _DM_PROTOCOL_MODBUSMASTER_HPP_ */

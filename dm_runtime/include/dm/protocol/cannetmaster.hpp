﻿/*
 * cannetmaster.hpp
 *
 *  Created on: 2017年4月1日
 *      Author: work
 */

#ifndef _DM_PROTOCOL_CANNETMASTER_HPP_
#define _DM_PROTOCOL_CANNETMASTER_HPP_

#include <dm/protocol/cannet.hpp>

namespace dm{
namespace protocol{

class CCannetMaster:virtual public CCannet{
public:
	CCannetMaster();
	virtual ~CCannetMaster();

protected:
	EAction taskStart_call( frame_t* txFrame );
	EAction taskDo_call( frame_t* txFrame );

	EAction taskStart_syncClock( frame_t* txFrame );
	EAction taskDo_syncClock( frame_t* txFrame );

	EAction taskStart_remoteControl( frame_t* txFrame );
	EAction taskDo_remoteControl( frame_t* txFrame );

	EAction taskStart_parameters( frame_t* txFrame );
	EAction taskDo_parameters( frame_t* txFrame );

	//============
	virtual EAction taskStart_call( CFrameCan& tf );
	virtual EAction taskDo_call( CFrameCan& tf );

	virtual EAction taskStart_syncClock( CFrameCan& tf );
	virtual EAction taskDo_syncClock( CFrameCan& tf );

	virtual EAction taskStart_remoteControl( CFrameCan& tf );
	virtual EAction taskDo_remoteControl( CFrameCan& tf );

	virtual EAction taskStart_parameters( CFrameCan& tf );
	virtual EAction taskDo_parameters( CFrameCan& tf );
};
}
}



#endif /* INCLUDE_DM_PROTOCOL_CANNETMASTER_HPP_ */

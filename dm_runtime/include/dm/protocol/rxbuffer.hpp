﻿/*
 * rxbuf.hpp
 *
 *  Created on: 2017年2月3日
 *      Author: Dylan.Gao
 */

#ifndef _DM_PROTOCOL_RXBUFFER_HPP_
#define _DM_PROTOCOL_RXBUFFER_HPP_

#include <dm/types.hpp>
#include <dm/ringbuf.hpp>

namespace dm{
namespace protocol{

#ifndef RX_BUF_LEN
#define RX_BUF_LEN 4096
#endif

typedef dm::CRingBuf<dm::uint8,dm::uint,RX_BUF_LEN,dm::uint16> CRxBuffer;

}
}

#endif /* INCLUDE_DM_FRAME_RXBUFFER_HPP_ */

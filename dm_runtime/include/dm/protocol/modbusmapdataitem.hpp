﻿/*
 * modbusmapdataitem.hpp
 *
 *  Created on: 2019-4-13
 *      Author: work
 */

#ifndef _DM_PROTOCOL_MODBUSMAPDATAITEM_HPP_
#define _DM_PROTOCOL_MODBUSMAPDATAITEM_HPP_

#include <dm/export.hpp>

#ifndef DM_API_PROTOCOL
#define DM_API_PROTOCOL DM_API_IMPORT
#endif

#include <dm/protocol/modbusmapitem.hpp>

namespace dm{
namespace protocol{

/**
 * 数据对象基类
 * 采集到的数值类型对象
 */
class DM_API_PROTOCOL CModbusMapDataItem:public CModbusMapItem{
public:
	CModbusMapDataItem();
	virtual ~CModbusMapDataItem();

	inline bool ifStore()const{
		return m_store;
	}

	inline bool ifGenTimed()const{
		return m_genTimed;
	}

	inline bool ifReportTimed()const{
		return m_reportTimed;
	}

	bool init( const TiXmlElement* cfg,const bool& plc=false );

protected:
	bool m_store;			// 是否存储标志
	bool m_genTimed;		// 是否产生时标数据标志
	bool m_reportTimed;		// 是否上报时标数据标志
};

}
}

#endif /* DM_RUNTIME_INCLUDE_DM_PROTOCOL_MODBUSMAPDATAITEM_HPP_ */

﻿/*
 * modbusmap_cumulant.hpp
 *
 *  Created on: 2019-4-13
 *      Author: work
 */

#ifndef _DM_PROTOCOL_MODBUSMAP_CUMULANT_HPP_
#define _DM_PROTOCOL_MODBUSMAP_CUMULANT_HPP_

#include <dm/export.hpp>

#ifndef DM_API_PROTOCOL
#define DM_API_PROTOCOL DM_API_IMPORT
#endif

#include <dm/protocol/modbusmapdataitem.hpp>

namespace dm{
namespace protocol{

class DM_API_PROTOCOL CModbusMapCumulant:public CModbusMapDataItem{
public:
	CModbusMapCumulant();

	bool init( const TiXmlElement* cfg,const bool& plc = false );

	inline const EDataType& type()const{
		return m_type;
	}

	inline const dm::int64& maxCode()const{
		return m_maxCode;
	}

	inline const float& coef()const{
		return m_coef;
	}

	inline const float& delta()const{
		return m_delta;
	}
private:
	EDataType m_type;

	dm::int64 m_maxCode;
	float m_coef;
	float m_delta;
};

}
}

#endif /* DM_RUNTIME_INCLUDE_DM_PROTOCOL_MODBUSMAP_CUMULANT_HPP_ */

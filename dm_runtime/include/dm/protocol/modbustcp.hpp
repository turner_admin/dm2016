﻿/*
 * modbustcp.hpp
 *
 *  Created on: 2017年10月28日
 *      Author: work
 */

#ifndef _DM_PROTOCOL_MODBUSTCP_HPP_
#define _DM_PROTOCOL_MODBUSTCP_HPP_

#include <dm/export.hpp>

#ifndef DM_API_PROTOCOL
#define DM_API_PROTOCOL DM_API_IMPORT
#endif

#include <dm/protocol/framemodbustcp.hpp>
#include <dm/protocol/modbus.hpp>

namespace dm{
namespace protocol{

/**
 * Modbus TCP规约基类
 */
class DM_API_PROTOCOL CModbusTcp:virtual public CModbus{
public:
	CModbusTcp();
	virtual ~CModbusTcp();

protected:
	CFrameModbus& getTxModbusFrame();
	CFrameModbus& getRxModbusFrame();

protected:
	CFrameModbusTcp m_rxFrame;
	CFrameModbusTcp m_txFrame;
};

}
}

#endif /* DM_RUNTIME_INCLUDE_DM_PROTOCOL_MODBUSTCP_HPP_ */

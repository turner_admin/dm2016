﻿/*
 * cannet.hpp
 *
 *  Created on: 2017年3月22日
 *      Author: work
 */

#ifndef _DM_PROTOCOL_CANNET_HPP_
#define _DM_PROTOCOL_CANNET_HPP_

#include <dm/protocol/framecan.hpp>
#include <dm/protocol/protocol.hpp>

namespace dm{
namespace protocol{

/**
 * CAN网络协议
 * 本协议提供了基于CAN总线邮箱的通信处理框架
 */
class CCannet:virtual public CProtocol{
public:
	CCannet();
	virtual ~CCannet();

	CFrame& getRxFrame();
	const CFrame& getTxFrame();

	EAction dealRxFrame( const ts_t& ts,const rts_t& rts );

protected:
	virtual EAction dealRxErrFrame( const CFrameCan& rf,CFrameCan& tf,const ts_t& ts,const rts_t& rts );
	virtual EAction dealRxRemoteFrame( const CFrameCan& rf,CFrameCan& tf,const ts_t& ts,const rts_t& rts );
	virtual EAction dealRxExternFrame( const CFrameCan& rf,CFrameCan& tf,const ts_t& ts,const rts_t& rts );
	virtual EAction dealRxStandardFrame( const CFrameCan& rf,CFrameCan& tf,const ts_t& ts,const rts_t& rts );

	inline CFrameCan& rxFrame(){
		return m_rxFrame;
	}

	inline CFrameCan& txFrame(){
		return m_txFrame;
	}

	inline const CFrameCan& rxFrame()const{
		return m_rxFrame;
	}

	inline const CFrameCan& txFrame()const{
		return m_txFrame;
	}
private:
	CFrameCan m_rxFrame;
	CFrameCan m_txFrame;
};

}
}

#endif /* INCLUDE_DM_PROTOCOL_CANNET_HPP_ */

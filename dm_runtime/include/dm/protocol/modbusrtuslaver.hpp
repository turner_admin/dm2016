﻿/*
 * modbusrtuslaver.hpp
 *
 *  Created on: 2017年3月18日
 *      Author: work
 */

#ifndef _DM_PROTOCOL_MODBUSRTUSLAVER_HPP_
#define _DM_PROTOCOL_MODBUSRTUSLAVER_HPP_

#include <dm/export.hpp>

#ifndef DM_API_PROTOCOL
#define DM_API_PROTOCOL DM_API_IMPORT
#endif

#include <dm/protocol/modbusrtu.hpp>

namespace dm{
namespace protocol{

/**
 * Modbus子站
 * 在接收后才处理。不需要任务流转
 */
class DM_API_PROTOCOL CModbusRtuSlaver:virtual public CModbusRtu{
public:
	CModbusRtuSlaver();
	virtual ~CModbusRtuSlaver();

	EAction dealRxFrame( const CRunTimeStamp& now );

protected:
	CFrameModbusRtu* genRxTempFrame();
	CFrameModbusRtu* genTxTempFrame();

	EAction dealRxModbusRtFrame( const CFrameModbusRtu& rf,CFrameModbusRtu& tf );

	virtual EAction dealRx_readCoils( const CFrameModbusRtu& rf,CFrameModbusRtu& tf,const CRunTimeStamp& now );
	virtual EAction dealRx_readDiscreteInputs( const CFrameModbusRtu& rf,CFrameModbusRtu& tf,const CRunTimeStamp& now );
	virtual EAction dealRx_readHoldingRegisters( const CFrameModbusRtu& rf,CFrameModbusRtu& tf,const CRunTimeStamp& now );
	virtual EAction dealRx_readInputRegisters( const CFrameModbusRtu& rf,CFrameModbusRtu& tf,const CRunTimeStamp& now );

	virtual EAction dealRx_writeSingleCoil( const CFrameModbusRtu& rf,CFrameModbusRtu& tf,const CRunTimeStamp& now );
	virtual EAction dealRx_writeSingleRegister( const CFrameModbusRtu& rf,CFrameModbusRtu& tf,const CRunTimeStamp& now );

	virtual EAction dealRx_writeMultipleCoils( const CFrameModbusRtu& rf,CFrameModbusRtu& tf,const CRunTimeStamp& now );
	virtual EAction dealRx_writeMultipleRegisters( const CFrameModbusRtu& rf,CFrameModbusRtu& tf,const CRunTimeStamp& now );
};

}
}

#endif /* INCLUDE_DM_PROTOCOL_MODBUSRTUSLAVER_HPP_ */

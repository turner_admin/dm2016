﻿/*
 * modbustcpslaver.hpp
 *
 *  Created on: 2017年10月30日
 *      Author: work
 */

#ifndef _DM_PROTOCOL_MODBUSTCPSLAVER_HPP_
#define _DM_PROTOCOL_MODBUSTCPSLAVER_HPP_

#include <dm/export.hpp>

#ifndef DM_API_PROTOCOL
#define DM_API_PROTOCOL DM_API_IMPORT
#endif

#include <dm/protocol/modbustcp.hpp>

namespace dm{
namespace protocol{

class DM_API_PROTOCOL CModbusTcpSlaver:virtual public CModbusTcp{
public:
	CModbusTcpSlaver();
	virtual ~CModbusTcpSlaver();

	EAction dealRxFrame( const dm::CRunTimeStamp &now );

protected:
	CFrameModbusTcp* genRxTempFrame();
	CFrameModbusTcp* genTxTempFrame();

	virtual EAction dealRx_readCoils( const CFrameModbusTcp& rf,CFrameModbusTcp& tf,const CRunTimeStamp& now );
	virtual EAction dealRx_readDiscreteInputs( const CFrameModbusTcp& rf,CFrameModbusTcp& tf,const CRunTimeStamp& now );
	virtual EAction dealRx_readHoldingRegisters( const CFrameModbusTcp& rf,CFrameModbusTcp& tf,const CRunTimeStamp& now );
	virtual EAction dealRx_readInputRegisters( const CFrameModbusTcp& rf,CFrameModbusTcp& tf,const CRunTimeStamp& now );

	virtual EAction dealRx_writeSingleCoil( const CFrameModbusTcp& rf,CFrameModbusTcp& tf,const CRunTimeStamp& now );
	virtual EAction dealRx_writeSingleRegister( const CFrameModbusTcp& rf,CFrameModbusTcp& tf,const CRunTimeStamp& now );

	virtual EAction dealRx_writeMultipleCoils( const CFrameModbusTcp& rf,CFrameModbusTcp& tf,const CRunTimeStamp& now );
	virtual EAction dealRx_writeMultipleRegisters( const CFrameModbusTcp& rf,CFrameModbusTcp& tf,const CRunTimeStamp& now );
};

}
}



#endif /* DM_RUNTIME_INCLUDE_DM_PROTOCOL_MODBUSTCPSLAVER_HPP_ */

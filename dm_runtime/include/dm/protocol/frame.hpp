﻿#ifndef _DM_PROTOCOL_FRAME_HPP
#define _DM_PROTOCOL_FRAME_HPP

#include <dm/export.hpp>

#ifndef DM_API_PROTOCOL
#define DM_API_PROTOCOL DM_API_IMPORT
#endif

#include <dm/types.hpp>

#include <dm/protocol/rxbuffer.hpp>
#include <dm/protocol/txbuffer.hpp>

namespace dm{
namespace protocol{

class DM_API_PROTOCOL CFrame{
public:
	typedef CRxBuffer rxbuf_t;
	typedef CTxBuffer txbuf_t;

	typedef CRxBuffer::pos_t pos_t;
public:
	virtual  bool encode( txbuf_t& buf )const=0;
	virtual bool decode( const rxbuf_t& buf,const pos_t& end,pos_t& start,const CFrame* txFrame )=0;
};

}
}

#endif

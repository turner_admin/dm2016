/*
 * modbus.hpp
 *
 *  Created on: 2024年3月5日
 *      Author: Dylan.Gao
 */

#ifndef _DM_PROTOCOL_MODBUS_HPP_
#define _DM_PROTOCOL_MODBUS_HPP_

#include <dm/export.hpp>

#ifndef DM_API_PROTOCOL
#define DM_API_PROTOCOL DM_API_IMPORT
#endif

#include <dm/protocol/framemodbus.hpp>
#include <dm/protocol/protocol.hpp>

namespace dm{
namespace protocol{

/**
 * Modbus通信基类
 */
class DM_API_PROTOCOL CModbus:virtual public CProtocol{
public:
	CModbus();
	virtual ~CModbus();

	const dm::protocol::CFrame& getTxFrame();
	dm::protocol::CFrame& getRxFrame();

protected:
	virtual CFrameModbus& getTxModbusFrame()=0;
	virtual CFrameModbus& getRxModbusFrame()=0;
};

}
}

#endif /* _DM_PROTOCOL_MODBUS_HPP_ */

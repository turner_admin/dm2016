﻿/*
 * framecan.hpp
 *
 *  Created on: 2017年3月22日
 *      Author: Dylan.Gao
 */

#ifndef _DM_PROTOCOL_FRAMECAN_HPP_
#define _DM_PROTOCOL_FRAMECAN_HPP_

#include <dm/export.hpp>

#ifndef DM_API_PROTOCOL
#define DM_API_PROTOCOL DM_API_IMPORT
#endif

#include <dm/protocol/frame.hpp>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/uio.h>
#include <linux/can.h>
#include <linux/can/raw.h>
#include <net/if.h>
#include <unistd.h>
#include <fcntl.h>

namespace dm{
namespace protocol{

class DM_API_PROTOCOL CFrameCan:public CFrame{
public:
	CFrameCan();
	CFrameCan( const CFrameCan& frame );

	CFrameCan& operator=( const CFrameCan& frame );

	bool encode( txbuf_t& buf )const;
	bool decode( const rxbuf_t& buf,const pos_t& end,pos_t& start,const CFrame* txFrame );

	inline const can_frame& can()const{
		return m_can;
	}

	inline can_frame& can(){
		return m_can;
	}

	inline bool isExtFrame()const{
		return m_can.can_id&CAN_EFF_FLAG;
	}

	inline bool isRemoteFrame()const{
		return m_can.can_id&CAN_RTR_FLAG;
	}

	inline bool isErrorFrame()const{
		return m_can.can_id&CAN_ERR_FLAG;
	}

	dm::uint32 getStdFrameId()const{
		return m_can.can_id&CAN_SFF_MASK;
	}

	void set( dm::uint32 id,const dm::uint8* buf,int len );

private:
	can_frame m_can;
};

}
}

#endif /* INCLUDE_DM_PROTOCOL_FRAMECAN_HPP_ */

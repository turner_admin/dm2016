﻿/*
 * framemodbusrtu.hpp
 *
 *  Created on: 2017年3月1日
 *      Author: work
 */

#ifndef _DM_PROTOCOL_FRAMEMODBUSRTU_HPP_
#define _DM_PROTOCOL_FRAMEMODBUSRTU_HPP_

#include <dm/export.hpp>

#ifndef DM_API_PROTOCOL
#define DM_API_PROTOCOL DM_API_IMPORT
#endif

#include <dm/protocol/framemodbus.hpp>
#include <dm/types.hpp>

namespace dm{
namespace protocol{

class DM_API_PROTOCOL CFrameModbusRtu:public CFrameModbus{
public:
	typedef dm::uint8 address_t;

	enum{
		MaxBufLen = 255,

		Address_All = 0	// 广播地址
	};

	CFrameModbusRtu();
	CFrameModbusRtu( const CFrameModbusRtu& frame );
	virtual ~CFrameModbusRtu(){}

	CFrameModbusRtu& operator=( const CFrameModbusRtu& frame );

	inline const address_t& getAddress()const{
		return m_address;
	}

	inline void setAddress( const address_t& addr ){
		m_address = addr;
	}

	bool encode( txbuf_t& buf )const;
	bool decode( const rxbuf_t& buf,const pos_t& end,pos_t& start,const CFrame* txFrame );
protected:
	void crcCalc( const dm::uint8* buf,const int& size,dm::uint8* sum )const;
	bool crcCheck( const rxbuf_t& buf,const pos_t& start,const int& size,const dm::uint8* sum )const;
protected:
	address_t m_address;
};

}
}



#endif /* INCLUDE_DM_PROTOCOL_FRAMEMODBUSRTU_HPP_ */

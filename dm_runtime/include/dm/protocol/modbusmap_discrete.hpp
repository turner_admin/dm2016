﻿/*
 * modbusmap_discrete.hpp
 *
 *  Created on: 2019-4-13
 *      Author: work
 */

#ifndef _DM_PROTOCOL_MODBUSMAP_DISCRETE_HPP_
#define _DM_PROTOCOL_MODBUSMAP_DISCRETE_HPP_

#include <dm/export.hpp>

#ifndef DM_API_PROTOCOL
#define DM_API_PROTOCOL DM_API_IMPORT
#endif

#include <dm/protocol/modbusmapdataitem.hpp>
#include <vector>

namespace dm{
namespace protocol{

class DM_API_PROTOCOL CModbusMapDiscrete:public CModbusMapDataItem{
public:
	CModbusMapDiscrete();

	bool init( const TiXmlElement* cfg,const bool& plc=false );

	inline const int& bitOffset()const{
		return m_bitOffset;
	}

	inline const int& bitLen()const{
		return m_bitLen;
	}

	inline const dm::uint16& valueBase()const{
		return m_valueBase;
	}

	inline const int& invalidValue()const{
		return m_invalidValue;
	}

	inline const std::vector<std::string>& valueDescs()const{
		return m_valueDescs;
	}

private:
	int m_bitOffset;
	int m_bitLen;

	dm::uint16 m_valueBase;
	int m_invalidValue;

	std::vector<std::string> m_valueDescs;
};

}
}

#endif /* DM_RUNTIME_INCLUDE_DM_PROTOCOL_MODBUSMAP_DISCRETE_HPP_ */

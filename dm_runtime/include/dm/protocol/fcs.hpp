﻿/*
 * fcs.hpp
 *
 *  Created on: 2020年2月26日
 *      Author: Dylan.Gao
 */

#ifndef _DM_PROTOCOL_FCS_HPP_
#define _DM_PROTOCOL_FCS_HPP_

#include <dm/export.hpp>

#ifndef DM_API_PROTOCOL
#define DM_API_PROTOCOL DM_API_IMPORT
#endif

#include <dm/types.hpp>

namespace dm{
namespace protocol{

static const dm::uint16 Fcs16Init = 0xFFFF;	// FCS初始值
static const dm::uint16 Fcs16Good = 0xF0B8;	// 校验成功值

/**
 * 计算FCS16
 * @param buf
 * @param len
 * @param fcs
 * @return
 */
dm::uint16 DM_API_PROTOCOL calcFcs16( const dm::uint8* buf,int len,dm::uint16 fcs=Fcs16Init );

/**
 * 使用Fcs16Good进行校验
 * 这个方法永远校验算法本身，程序中不用
 * @param buf
 * @param len
 * @param fcs
 * @return
 */
bool DM_API_PROTOCOL checkFcs16( const dm::uint8* buf,int len,dm::uint16 fcs );

}
}

#endif /* INCLUDE_DM_PROTOCOL_FCS_HPP_ */

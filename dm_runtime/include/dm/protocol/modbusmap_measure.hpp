﻿/*
 * modbusmap_measure.hpp
 *
 *  Created on: 2019-4-13
 *      Author: work
 */

#ifndef _DM_PROTOCOL_MODBUSMAP_MEASURE_HPP_
#define _DM_PROTOCOL_MODBUSMAP_MEASURE_HPP_

#include <dm/export.hpp>

#ifndef DM_API_PROTOCOL
#define DM_API_PROTOCOL DM_API_IMPORT
#endif

#include <dm/protocol/modbusmapdataitem.hpp>

namespace dm{
namespace protocol{

/**
 * 测量量配置项
 */
class DM_API_PROTOCOL CModbusMapMeasure:public CModbusMapDataItem{
public:
	CModbusMapMeasure();

	bool init( const TiXmlElement* cfg,const bool& plc=false );

	inline const EDataType& type()const{
		return m_type;
	}

	inline const float& base()const{
		return m_base;
	}

	inline const float& coef()const{
		return m_coef;
	}

	inline const float& delta()const{
		return m_delta;
	}

	inline const std::string& unit()const{
		return m_unit;
	}

private:
	EDataType m_type;

	float m_base;
	float m_coef;
	float m_delta;
	std::string m_unit;
};

}
}

#endif /* DM_RUNTIME_INCLUDE_DM_PROTOCOL_MODBUSMAP_MEASURE_HPP_ */

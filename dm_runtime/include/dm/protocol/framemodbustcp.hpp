﻿/*
 * framemodbustcp.hpp
 *
 *  Created on: 2017年10月27日
 *      Author: work
 */

#ifndef _DM_PROTOCOL_FRAMEMODBUSTCP_HPP_
#define _DM_PROTOCOL_FRAMEMODBUSTCP_HPP_

#include <dm/export.hpp>

#ifndef DM_API_PROTOCOL
#define DM_API_PROTOCOL DM_API_IMPORT
#endif

#include <dm/protocol/framemodbus.hpp>
#include <dm/types.hpp>

namespace dm{
namespace protocol{

class DM_API_PROTOCOL CFrameModbusTcp:public CFrameModbus{
public:
	CFrameModbusTcp();
	CFrameModbusTcp( const CFrameModbusTcp& frame );

	CFrameModbusTcp& operator=( const CFrameModbusTcp& frame );

	inline const dm::uint16& getTransFlag()const{
		return m_transFlag;
	}

	inline void setTransFlag( const dm::uint16& flag ){
		m_transFlag = flag;
	}

	inline const dm::uint16& getProtocolFlag()const{
		return m_protocolFlag;
	}

	inline void setProtocolFlag( const dm::uint16& flag ){
		m_protocolFlag = flag;
	}

	inline const dm::uint8& getUnitFlag()const{
		return m_unitFlag;
	}

	inline void setUnitFlag( const dm::uint8& flag ){
		m_unitFlag = flag;
	}

	bool encode( txbuf_t& buf )const;
	bool decode( const rxbuf_t& buf,const pos_t& end,pos_t& start,const CFrame* txFrame );

protected:
	dm::uint16 m_transFlag;		// 传输标志
	dm::uint16 m_protocolFlag;	// 协议标志
	dm::uint8 m_unitFlag;		// 单元标志
};

}
}



#endif /* DM_RUNTIME_INCLUDE_DM_PROTOCOL_FRAMEMODBUSTCP_HPP_ */

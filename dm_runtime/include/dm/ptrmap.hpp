/*
 * ptrmap.hpp
 *
 *  Created on: 2023年4月23日
 *      Author: Dylan.Gao
 */

#ifndef _DM_PTRMAP_HPP_
#define _DM_PTRMAP_HPP_

#include <map>

namespace dm{

template<typename Key,typename T>
class TCPtrMap{
public:
	typedef T* TP;
	typedef typename::std::map<Key,T*> map_t;
	typedef typename::std::map<Key,T*>::iterator iterator;
	typedef typename::std::map<Key,T*>::const_iterator const_iterator;

	TCPtrMap():m_e(){
	}

	~TCPtrMap(){
		for( typename::std::map<Key,T*>::iterator it=m_e.begin();it!=m_e.end();++it ){
			if( it->second )
				delete ( it->second );
		}
	}

	inline TP & operator[]( const Key& k ){
		return m_e[k];
	}

	inline const TP & operator[]( const Key& k )const{
		return m_e[k];
	}

	inline T* find( const Key& k ){
		iterator it = m_e.find(k);
		if( it==m_e.end() )
			return NULL;
		return it->second;
	}

	inline const T* find( const Key& k )const{
		const_iterator it = m_e.find(k);
		if( it==m_e.end() )
			return NULL;
		return it->second;
	}

	inline void clear(){
		for( typename::std::map<Key,T*>::iterator it=m_e.begin();it!=m_e.end();++it ){
			if( it->second )
				delete (it->second);
		}

		m_e.clear();
	}

	inline size_t size()const{
		return m_e.size();
	}

private:
	std::map<Key,T*> m_e;
};

};

#endif /* DM_RUNTIME_INCLUDE_DM_PTRMAP_HPP_ */

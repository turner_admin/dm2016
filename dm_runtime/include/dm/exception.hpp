/*
 * exception.hpp
 *
 *  Created on: 2019年11月15日
 *      Author: turner
 */

#ifndef _DM_EXCEPTION_HPP_
#define _DM_EXCEPTION_HPP_

#include <exception>

#include <dm/fixdstring.hpp>
#include <dm/string/stringref.hpp>
#include <dm/timestamp.hpp>

namespace dm{

/**
 * 异常
 */
class CException:public std::exception{
public:
	enum{
		LenOfMsg = 1024,
		LenOfModule = 128,
		LenOfFun = 128
	};

	typedef dm::CFixdString<LenOfMsg> msg_t;
	typedef dm::CFixdString<LenOfModule> module_t;
	typedef dm::CFixdString<LenOfFun> fun_t;
	typedef const void * obj_t;

	CException( const char* m="DM2016 Exception",const dm::CTimeStamp& now=dm::CTimeStamp::cur() ):std::exception(){
		m_msg=m;
		m_ts = now;
	}

	CException( const char* m,const char* fun,const dm::CTimeStamp& now=dm::CTimeStamp::cur() ):std::exception(){
		m_msg=m;
		m_fun = fun;
		m_ts = now;
	}

	CException( const char* m,const char* fun,const char* module,const dm::CTimeStamp& now=dm::CTimeStamp::cur() ):std::exception(){
		m_msg=m;
		m_fun = fun;
		m_module = module;
		m_ts = now;
	}

	CException( const char* m,const char* fun,const char* module,const obj_t& obj,const dm::CTimeStamp& now=dm::CTimeStamp::cur() ):std::exception(){
		m_msg=m;
		m_fun = fun;
		m_module = module;
		m_obj = obj;

		m_ts = now;
	}

	const char* what()const throw(){
		return m_msg.c_str();
	}

protected:
	dm::CTimeStamp m_ts;
	module_t m_module;
	fun_t m_fun;
	obj_t m_obj;
	msg_t m_msg;
};

}

#endif /* DM_EXCEPTION_HPP_ */

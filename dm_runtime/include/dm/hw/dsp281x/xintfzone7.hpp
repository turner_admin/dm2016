/*
 * xintfzone7.hpp
 *
 *  Created on: 2016��3��9��
 *      Author: dylan
 */

#ifndef XINTFZONE7_HPP_
#define XINTFZONE7_HPP_

#include <dm/hw/dsp281x/xintfzone.hpp>

namespace NDm{
namespace NHw{
namespace NF281x{
class CXintfZone7:public CXintfZone{
public:
	CXintfZone7();
};
}
}
}



#endif /* XINTFZONE7_HPP_ */

/*
 * timer0.hpp
 *
 *  Created on: 2016年2月26日
 *      Author: dylan
 */

#ifndef TIMER0_HPP_
#define TIMER0_HPP_

#include <dm/hw/dsp281x/timer.hpp>

namespace NDm{
namespace NHw{
namespace NF281x{

/**
 * CPU定时器0
 * 使用中断INT1.7
 */
class CTimer0:public CTimer{
public:
	CTimer0();
};

}
}
}

#endif /* TIMER0_HPP_ */

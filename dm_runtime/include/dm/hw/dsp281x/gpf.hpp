/*
 * gpf.hpp
 *
 *  Created on: 2016��2��26��
 *      Author: dylan
 */

#ifndef GPF_HPP_
#define GPF_HPP_


#include <dm/hw/dsp281x/gpio.hpp>

namespace NDm{
namespace NHw{
namespace NF281x{

class CGpf:public CGpio{
	CGpf( const CGpf& );
public:
	CGpf();
};
}
}
}


#endif /* GPF_HPP_ */

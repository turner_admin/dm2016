/*
 * xintfzone2.hpp
 *
 *  Created on: 2016��3��9��
 *      Author: dylan
 */

#ifndef XINTFZONE2_HPP_
#define XINTFZONE2_HPP_

#include <dm/hw/dsp281x/xintfzone.hpp>

namespace NDm{
namespace NHw{
namespace NF281x{
class CXintfZone2:public CXintfZone{
public:
	CXintfZone2();
};
}
}
}



#endif /* XINTFZONE2_HPP_ */

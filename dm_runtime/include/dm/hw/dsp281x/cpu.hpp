/*
 * cpu.hpp
 *
 *  Created on: 2016年2月29日
 *      Author: dylan
 */

#ifndef CPU_HPP_
#define CPU_HPP_

namespace NDm{
namespace NHw{
namespace NF281x{

/**
 * CPU类实现了对CPU级别的操作的封装
 */
class CCpu{
public:
	/**
	 * 使能全局中断
	 */
	static inline void eint(){
		asm(" clrc INTM");
	}

	/**
	 * 禁用全局中断
	 */
	static inline void dint(){
		asm(" setc INTM");
	}

	/**
	 * 使能全局实时中断
	 * 一般在eint()之后调用
	 */
	static inline void ertm(){
		asm(" clrc DBGM");
	}

	/**
	 * 禁用全局实时中断
	 * 本函数一般不使用
	 */
	static inline void drtm(){
		asm(" setc DBGM");
	}

	static inline void eallow(){
		asm(" EALLOW");
	}

	static inline void edis(){
		asm(" EDIS");
	}

	/**
	 * 清除所有的中断使能
	 */
	void clrIER();

	/**
	 * 清除所有的中断标志
	 */
	void clrIFR();

	/**
	 * 中断是否被使能
	 * @param x 中断号，0~11：INT1~INT12
	 * @return
	 */
	bool isIntEn( const int& x )const;

	/**
	 * 使能中断
	 * @param x
	 */
	void enInt( const int& x );

	/**
	 * 禁用中断
	 * @param x
	 */
	void disInt( const int& x );

	inline void enInt1(){
		enInt(0);
	}

	inline void enInt2(){
		enInt(1);
	}

	inline void enInt3(){
		enInt(2);
	}

	inline void enInt4(){
		enInt(3);
	}

	inline void enInt5(){
		enInt(4);
	}

	inline void enInt6(){
		enInt(5);
	}

	inline void enInt7(){
		enInt(6);
	}

	inline void enInt8(){
		enInt(7);
	}

	inline void enInt9(){
		enInt(8);
	}

	inline void enInt10(){
		enInt(9);
	}

	inline void enInt11(){
		enInt(10);
	}

	inline void enInt12(){
		enInt(11);
	}

	/**
	 * 当前是否有中断发生
	 * 一般CPU中断和PIE配合使用，不单独使用
	 * 中断标志也是由CPU自动清除的
	 * @param x
	 * @return
	 */
	bool isIntFlag( const int& x )const;
};

}
}
}

#endif /* CPU_HPP_ */

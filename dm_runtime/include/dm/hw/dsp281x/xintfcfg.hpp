/*
 * xintfcfg.hpp
 *
 *  Created on: 2016年3月5日
 *      Author: dylan
 */

#ifndef XINTFCFG_HPP_
#define XINTFCFG_HPP_

#include <dm/bitmask.hpp>

namespace NDm{
namespace NHw{
namespace NF281x{

class CXintfCfg{
public:
	CXintfCfg();

	/**
	 * 是否时钟为系统时钟的一半
	 * @return
	 */
	inline bool isClk_halfSysClk()const{	return (m_cnf2&0x00070000)==0x00010000;	}

	/**
	 * 是否时钟为系统时钟
	 * @return
	 */
	inline bool isClk_sysClk()const{		return (m_cnf2&0x00070000)==0x00000000;	}

	inline void setClk_halfSysClk(){		m_cnf2 = ((m_cnf2&0xFFF8FFFF)|0x00010000);	}
	inline void setClk_sysClk(){		m_cnf2 &= 0xFFF8FFFF;	}

	/**
	 * XCLKOUT引脚输出的时钟是否是模块时钟的一半
	 * @return
	 */
	inline bool isXClkOut_halfClk()const{
		return NDm::isBitSet<volatile unsigned long,2,unsigned long>(m_cnf2);
	}

	inline bool isXClkOut_clk()const{
		return !isXClkOut_halfClk();
	}

	inline void setXClkOut_halfClk(){
		NDm::bitSet<volatile unsigned long,2,unsigned long>(m_cnf2);
	}

	inline void setXClkOut_clk(){
		NDm::bitClr<volatile unsigned long,2,unsigned long>(m_cnf2);
	}
	/**
	 * 获取MC引脚的状态
	 * 该状态代表了Zone7模式还是BootRom模式，其他zone没影响
	 * @return
	 */
	inline bool isMcState_zone7()const{
		return NDm::isBitSet<volatile unsigned long,8,unsigned int>(m_cnf2);
	}

	inline bool isMcState_bootRom()const{
		return !isMcState_zone7();
	}

	/**
	 * XINTF接口的时钟输出是否使能
	 * 关闭该时钟将关闭对外部接口设备的驱动
	 * @return
	 */
	inline bool isEn_xClkOut()const{
		return NDm::getBit<volatile unsigned long,3,unsigned long>(m_cnf2)==0;
	}

	inline bool isDis_xClkOut()const{
		return NDm::isBitSet<volatile unsigned long,3,unsigned long>(m_cnf2);
	}

	inline void en_xClkOut(){
		NDm::bitClr<volatile unsigned long,3,unsigned long>(m_cnf2);
	}

	inline void dis_xClkOut(){
		NDm::bitSet<volatile unsigned long,3,unsigned long>(m_cnf2);
	}

	// 写缓存功能使能
	inline void setWriteBufferDepth( const unsigned long& depth ){
		m_cnf2 |= (depth&0x0003);
	}

	void setBankSwitchDelay( const unsigned long& timClk );
	void setBankSwitchZone( const unsigned long& zone );

private:
	volatile unsigned long& m_cnf2;
};
}
}
}

#endif /* XINTFCFG_HPP_ */

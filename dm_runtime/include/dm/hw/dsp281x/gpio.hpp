/*
 * gpio.hpp
 *
 *  Created on: 2016年2月25日
 *      Author: dylan
 */

#ifndef GPIO_HPP_
#define GPIO_HPP_

namespace NDm{
namespace NHw{
namespace NF281x{
class CGpio{
	CGpio( const CGpio& );
public:
	CGpio( volatile unsigned int& mux,volatile unsigned int& dir,
			volatile unsigned int& dat,volatile unsigned int& set,volatile unsigned int& clr,
			volatile unsigned int& tog );

	/**
	 * 获取该组GPIO第i个io口的MUX
	 * @param i 0~15
	 * @return
	 * - 0 作为IO
	 * - 1 作为外设
	 */
	inline unsigned int getMux( const int& i )const{
		return m_mux&(0x0001<<i);
	}

	inline bool isMuxIo( const int& i )const{
		return getMux(i)==0;
	}

	inline bool isMuxPeripheral( const int& i )const{
		return getMux(i)!=0;
	}

	/**
	 * 设置GPIO作为IO
	 * @param i
	 */
	void setMuxIo( const int& i );

	/**
	 * 设置GPIO为外设模式
	 * @param i
	 */
	void setMuxPeripheral( const int& i );

	/**
	 * 设置GPIO的MUX
	 * @param i
	 * @param mux
	 * - 0 IO模式
	 * - 1 外设模式
	 */
	inline void setMux( const int& i,const unsigned int& mux ){
		if( mux==0 )
			setMuxIo(i);
		else
			setMuxPeripheral(i);
	}

	inline unsigned int getIoDir( const int& i )const{
		return m_dir&(0x0001<<i);
	}

	inline void setIoDir( const int& i,const unsigned int& dir ){
		if( dir==0 )
			setIoIn(i);
		else
			setIoOut(i);
	}

	void setIoIn( const int& i );
	void setIoOut( const int& i );

	inline bool isIoIn( const int& i )const{
		return getIoDir(i)==0;
	}

	inline bool isIoOut( const int& i )const{
		return getIoDir(i)!=0;
	}

	inline unsigned int getIoState( const int& i )const{
		return m_dat&(0x0001<<i);
	}

	inline bool isIoSet( const int& i )const{
		return getIoState(i)!=0;
	}

	inline bool isIoUnset( const int& i )const{
		return getIoState(i)==0;
	}

	inline void set( const int& i ){
		m_set |= (0x0001<<i);
	}

	inline void clear( const int& i ){
		m_clr |= (0x0001<<i);
	}

	inline void toggle( const int& i ){
		m_toggle |= (0x0001<<i);
	}
protected:
	volatile unsigned int& m_mux;
	volatile unsigned int& m_dir;

	volatile unsigned int& m_dat;
	volatile unsigned int& m_set;
	volatile unsigned int& m_clr;
	volatile unsigned int& m_toggle;
};
}
}
}

#endif /* GPIO_HPP_ */

/*
 * pclk.hpp
 *
 *  Created on: 2016年2月24日
 *      Author: dylan
 */

#ifndef PCLK_HPP_
#define PCLK_HPP_

namespace NDm{
namespace NHw{
namespace NF281x{
/**
 * 外设时钟控制器
 * 控制外设时钟的使能
 * @note 外设时钟使能操作有2时钟周期的延时，该外设才能正常工作
 */
class CPclk{
	CPclk( const CPclk& );
public:
	CPclk();

	bool isEn_ecan()const;
	void en_ecan( const bool& en=true );

	bool isEn_mcbsp()const;
	void en_mcbsp( const bool& en=true );

	bool isEn_scia()const;
	void en_scia( const bool& en=true );

	bool isEn_scib()const;
	void en_scib( const bool& en=true );

	bool isEn_spia()const;
	void en_spia( const bool& en=true );

	bool isEn_adc()const;
	void en_adc( const bool& en=true );

	bool isEn_eva()const;
	void en_eva( const bool& en=true );

	bool isEn_evb()const;
	void en_evb( const bool& en=true );
};
}
}
}

#endif /* PCLK_HPP_ */

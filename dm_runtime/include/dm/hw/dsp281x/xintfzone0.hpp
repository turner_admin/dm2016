/*
 * xintfzone0.hpp
 *
 *  Created on: 2016��3��9��
 *      Author: dylan
 */

#ifndef XINTFZONE0_HPP_
#define XINTFZONE0_HPP_

#include <dm/hw/dsp281x/xintfzone.hpp>

namespace NDm{
namespace NHw{
namespace NF281x{
class CXintfZone0:public CXintfZone{
public:
	CXintfZone0();
};
}
}
}


#endif /* XINTFZONE0_HPP_ */

/*
 * timer.hpp
 *
 *  Created on: 2016年2月26日
 *      Author: dylan
 */

#ifndef TIMER_HPP_
#define TIMER_HPP_


#include <dm/bitmask.hpp>

namespace NDm{
namespace NHw{
namespace NF281x{

class CTimer{
	CTimer( const CTimer& );
public:
	CTimer( volatile unsigned long& tim,volatile unsigned long& prd,volatile unsigned int& tcr,volatile unsigned int& tpr,volatile unsigned int& tprh );

	inline const volatile unsigned long& getCounter()const{
		return m_tim;
	}

	inline void setCounter( const unsigned long& count ){
		m_tim = count;
	}

	inline const volatile unsigned long& getPeriod()const{
		return m_prd;
	}

	inline void setPeriod( const unsigned long& prd ){
		m_prd = prd;
	}

	/**
	 * 定时器计时到0是否发生
	 * @return
	 */
	inline bool isTimerZeroFlag()const{
		return NDm::isBitSet<volatile unsigned int,15,unsigned int>(m_tcr);
	}

	inline void clrTimerZeroFlag(){
		NDm::bitSet<volatile unsigned int,15,unsigned int>(m_tcr);
	}

	/**
	 * 定时器中断是否使能
	 * @return
	 */
	inline bool isInterruptEn()const{
		return NDm::isBitSet<volatile unsigned int,14,unsigned int>(m_tcr);
	}

	inline void enInterrupt(){
		NDm::bitSet<volatile unsigned int,14,unsigned int>(m_tcr);
	}

	inline void disInterrupt(){
		NDm::bitClr<volatile unsigned int,14,unsigned int>(m_tcr);
	}

	/**
	 * 定时器重新开始运行
	 */
	inline void restart(){
		NDm::bitClr<volatile unsigned int,4,unsigned int>(m_tcr);
	}

	/**
	 * 听事情停止
	 */
	inline void stop(){
		NDm::bitSet<volatile unsigned int,4,unsigned int>(m_tcr);
	}

	/**
	 * 定时器是否已经运行
	 * @return
	 */
	inline bool isStarting()const{
		return 0==NDm::getBit<volatile unsigned int,4,unsigned int>(m_tcr);
	}

	/**
	 * 重新加载
	 */
	inline void reload(){
		NDm::bitSet<volatile unsigned int,5,unsigned int>(m_tcr);
	}

	/**
	 * 获取时钟周期
	 * 没SysCycle个系统时钟周期，定时器计数减1
	 * @return
	 */
	inline unsigned int getSysCycle()const{
		return (m_tprh<<8)|(m_tpr&0x00FF);
	}

	/**
	 * 设置时钟周期
	 * @param cycle
	 */
	inline void setSysCycle( const unsigned int& cycle ){
		m_tpr = (cycle&0x00FF);
		m_tprh = (cycle>>8);
	}
protected:
	volatile unsigned long& m_tim;
	volatile unsigned long& m_prd;

	volatile unsigned int& m_tcr;
	volatile unsigned int& m_tpr;
	volatile unsigned int& m_tprh;
};

}
}
}

#endif /* TIMER_HPP_ */

/*
 * timer1.hpp
 *
 *  Created on: 2016年2月26日
 *      Author: dylan
 */

#ifndef TIMER1_HPP_
#define TIMER1_HPP_


#include <dm/hw/dsp281x/timer.hpp>

namespace NDm{
namespace NHw{
namespace NF281x{

/**
 * CPU中断
 * 使用中断INT13
 */
class CTimer1:public CTimer{
public:
	CTimer1();
};

}
}
}


#endif /* TIMER1_HPP_ */

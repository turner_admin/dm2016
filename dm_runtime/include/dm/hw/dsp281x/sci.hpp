/*
 * sci.hpp
 *
 *  Created on: 2016年2月25日
 *      Author: dylan
 */

#ifndef SCI_HPP_
#define SCI_HPP_

#include <dm/hw/dsp281x/clocking.hpp>
#include <dm/bitmask.hpp>

namespace NDm{
namespace NHw{
namespace NF281x{

/**
 * SCI基类
 */
class CSci{
public:
	CSci( volatile unsigned int& ccr,
			volatile unsigned int& ctl1,
			volatile unsigned int& ctl2,
			volatile unsigned int& lbaud,
			volatile unsigned int& hbaud,
			volatile unsigned int& rxst,
			volatile unsigned int& rxbuf,
			volatile unsigned int& txbuf,
			volatile unsigned int& fftx,
			volatile unsigned int& ffrx,
			volatile unsigned int& ffct);

	/**
	 * 获取停止位数
	 * @return
	 * - 0x00 1bit停止位
	 * - 0x80 2bits停止位
	 */
	inline unsigned int getStop()const{
		return NDm::getBit<volatile unsigned int,7,unsigned int>(m_ccr);
	}

	/**
	 * 是否是1位停止位
	 * @return
	 */
	inline bool isStop_1()const{
		return getStop()==0;
	}

	/**
	 * 是否是2位停止位
	 * @return
	 */
	inline bool isStop_2()const{
		return getStop()!=0;
	}

	/**
	 * 设置停止位
	 * @param st
	 * @see getStop()
	 */
	inline void setStop( const unsigned int& st ){
		if( st==0 )
			setStop_1();
		else
			setStop_2();
	}

	/**
	 * 设置1位停止位
	 */
	inline void setStop_1(){
		NDm::bitClr<volatile unsigned int,7,unsigned int>(m_ccr);
	}

	/**
	 * 设置2位停止位
	 */
	inline void setStop_2(){
		NDm::bitSet<volatile unsigned int,7,unsigned int>(m_ccr);
	}

	/**
	 * 获取校验方式
	 * @return
	 * - 0x40: 奇校验
	 * - 0x60: 偶校验
	 * - 其他： 无校验
	 */
	inline unsigned int getParity()const{
		return m_ccr & 0x60;	// bit 6-5
	}

	/**
	 * 是否奇校验
	 * @return
	 */
	inline bool isParity_odd()const{
		return getParity()==0x40;
	}

	/**
	 * 是否偶校验
	 * @return
	 */
	inline bool isParity_even()const{
		return getParity()==0x60;
	}

	/**
	 * 是否无校验
	 * @return
	 */
	inline bool isParity_none()const{
		return (getParity())&0x20==0;
	}

	/**
	 * 设置校验方式
	 * @param p
	 * @see getParity();
	 */
	inline void setParity( const unsigned int& p ){
		if( p==0x60 )
			setParity_even();
		else if( p==0x40 )
			setParity_odd();
		else
			setParity_none();
	}

	/**
	 * 设置无校验
	 */
	inline void setParity_none(){
		NDm::bitClr<volatile unsigned int,5,unsigned int>(m_ccr);
	}

	/**
	 * 设置为偶校验
	 */
	inline void setParity_even(){
		m_ccr |= 0x60;
	}

	/**
	 * 设置为奇校验
	 */
	inline void setParity_odd(){
		m_ccr = (m_ccr&0xDF) | 0x20;
	}

	/**
	 * 获取字符长度
	 * @return 0~7:1~8
	 */
	inline unsigned int getChar()const{
		return m_ccr&0x07;	// bit 0~3
	}

	/**
	 * 设置字符长度
	 * @param len
	 * @see getChar();
	 */
	inline void setChar( const unsigned int& len ){
		m_ccr = (m_ccr&0xF8) | (len&0x07);
	}

	/****************  通信参数  **************/

	inline unsigned long getBps( const CClocking& clk )const{
		return clk.getClk_sci()/(( getBrr()+1 ) * 8);
	}

	inline void setBps( const unsigned long& bps,const CClocking& clk ){
		setBrr(clk.getClk_sci()/(8*bps) -1 );
	}

	void init( const unsigned int& ch=8,const unsigned int& stop=1,const unsigned int& parity=0 );

	/****************  运行状态  **************/

	/**
	 * 发送缓冲区可写
	 * @return
	 */
	inline bool isTxRdy()const{
		return NDm::isBitSet<volatile unsigned int,7,unsigned int>(m_ctl2);
	}

	/**
	 * 发送线路空闲
	 * @return
	 */
	inline bool isTxEmpty()const{
		return NDm::isBitSet<volatile unsigned int,6,unsigned int>(m_ctl2);
	}

	/**
	 * 接收缓冲区可读
	 * @return
	 */
	inline bool isRxRdy()const{
		return NDm::isBitSet<volatile unsigned int,6,unsigned int>(m_rxst);
	}

	/****************  操作接口  **************/

	inline bool isTxEn()const{
		return NDm::isBitSet<volatile unsigned int,1,unsigned int>(m_ctl1);
	}

	inline void enTx( const bool& en ){
		if( en )
			enTx();
		else
			disTx();
	}

	inline void enTx(){
		NDm::bitSet<volatile unsigned int,1,unsigned int>(m_ctl1);
	}

	inline void disTx(){
		NDm::bitClr<volatile unsigned int,1,unsigned int>(m_ctl1);
	}

	inline bool isRxEn()const{
		return NDm::isBitSet<volatile unsigned int,0,unsigned int>(m_ctl1);
	}

	inline void enRx( const bool& en ){
		if( en )
			enRx();
		else
			disRx();
	}

	inline void enRx(){
		NDm::bitSet<volatile unsigned int,0,unsigned int>(m_ctl1);
	}

	inline void disRx(){
		NDm::bitClr<volatile unsigned int,0,unsigned int>(m_ctl1);
	}

	inline void tx( const unsigned char& c ){
		m_txbuf = c;
	}

	inline void rx( unsigned char& c )const{
		c = m_rxbuf&0xFF;
	}

	/**
	 * 复位模块状态机
	 * 该操作不会修改参数
	 * @param delay 处于复位时常
	 */
	void reset(  const unsigned int& delay=100 );

	/****************  错误检测  **************/

	/**
	 * 接收错误
	 * 这是一个总的接收错误标志。
	 * 一般程序测试错误，都应该测试该标志
	 * 当有错误标志时，必须使用reset来进行复位。
	 * @return
	 */
	inline bool isRxErr()const{
		return NDm::isBitSet<volatile unsigned int,7,unsigned int>(m_rxst);
	}

	/**
	 * 接收信号至少有10bit的低电平
	 * @return
	 */
	inline bool isRxErr_break()const{
		return NDm::isBitSet<volatile unsigned int,5,unsigned int>(m_rxst);
	}

	/**
	 * 没有收到正确的停止位
	 * @return
	 */
	inline bool isRxErr_frame()const{
		return NDm::isBitSet<volatile unsigned int,4,unsigned int>(m_rxst);
	}

	/**
	 * 校验没通过
	 * @return
	 */
	inline bool isRxErr_parity()const{
		return NDm::isBitSet<volatile unsigned int,2,unsigned int>(m_rxst);
	}

	/**
	 * 程序或者DMA没有及时取走接收到的数据
	 * @return
	 */
	inline bool isRxErr_overrun()const{
		return NDm::isBitSet<volatile unsigned int,3,unsigned int>(m_rxst);
	}

	/**
	 * FIFO顶层数据有帧错误
	 * @note 只在FIFO模式下有效
	 * @return
	 */
	inline bool isFFErr_frame()const{
		return NDm::isBitSet<volatile unsigned int,15,unsigned int>(m_rxbuf);
	}

	/**
	 * FIFO顶层数据有校验错误
	 * @note 只在FIFO模式下有效
	 * @return
	 */
	inline bool isFFErr_parity()const{
		return NDm::isBitSet<volatile unsigned int,14,unsigned int>(m_rxbuf);
	}

	/****************  中断使能  **************/

	/**
	 * 接收错误时产生中断
	 * @return
	 */
	inline bool isIntEn_rxErr()const{
		return NDm::isBitSet<volatile unsigned int,6,unsigned int>(m_ctl1);
	}

	inline void enInt_rxErr( const bool& en ){
		if( en )
			enInt_rxErr();
		else
			disInt_rxErr();
	}

	inline void enInt_rxErr(){
		NDm::bitSet<volatile unsigned int,6,unsigned int>(m_ctl1);
	}

	inline void disInt_rxErr(){
		NDm::bitClr<volatile unsigned int,6,unsigned int>(m_ctl1);
	}

	/**
	 * 可以读取接收缓冲区或者检测到Break时产生中断
	 * @return
	 */
	inline bool isIntEn_rxRdyOrBreak()const{
		return NDm::isBitSet<volatile unsigned int,1,unsigned int>(m_ctl2);
	}

	inline void enInt_rxRdyOrBreak( const bool& en ){
		if( en )
			enInt_rxRdyOrBreak();
		else
			disInt_rxRdyOrBreak();
	}

	inline void enInt_rxRdyOrBreak(){
		NDm::bitSet<volatile unsigned int,1,unsigned int>(m_ctl2);
	}

	inline void disInt_rxRdyOrBreak(){
		NDm::bitClr<volatile unsigned int,1,unsigned int>(m_ctl2);
	}

	/**
	 * 可以写数据到发送缓冲区时产生中断
	 * @return
	 */
	inline bool isIntEn_txRdy()const{
		return NDm::isBitSet<volatile unsigned int,0,unsigned int>(m_ctl2);
	}

	inline void enInt_txRdy( const bool& en ){
		if( en )
			enInt_txRdy();
		else
			disInt_txRdy();
	}

	inline void enInt_txRdy(){
		NDm::bitSet<volatile unsigned int,0,unsigned int>(m_ctl2);
	}

	inline void disInt_txRdy(){
		NDm::bitClr<volatile unsigned int,0,unsigned int>(m_ctl2);
	}

	////////////////////////////////////////

	inline bool isEnFifo()const{
		return NDm::isBitSet<volatile unsigned int,14,unsigned int>(m_fftx);
	}

	inline void enFifo(){
		NDm::bitSet<volatile unsigned int,14,unsigned int>(m_fftx);
	}

	/**
	 * 复位
	 * 复位FIFO与SCI模块
	 * @param wait
	 */
	void resetFf( const unsigned int& wait=1000 );

	/**
	 * 复位发送队列
	 * @param wait
	 */
	void resetTxFf( const unsigned int& wait=100 );

	/**
	 * 复位接收队列
	 * @param wait
	 */
	void resetRxFf( const unsigned int& wait=100 );

	/**
	 * 发送字符延时
	 * @return
	 */
	inline unsigned int getFfTxDelay()const{
		return m_ffct & 0x00FF;
	}

	inline void setFfTxDelay( const unsigned int& delay=0 ){
		m_ffct = (m_ffct&0xFF00)| (delay&0x00FF);
	}

	int tx( const unsigned char* buf,const int& size );
	int rx( unsigned char* buf,const int& size )const;

	/**
	 * 发送队列数据个数
	 * @return
	 */
	inline unsigned int txFfState()const{
		return (m_fftx&0x1F00)>>8;
	}

	/**
	 * 接收队列数据个数
	 * @return
	 */
	inline unsigned int rxFfState()const{
		return (m_ffrx&0x1F00)>>8;
	}

	/**
	 * 接收队列溢出
	 * @return
	 */
	inline bool isRxFfOverFlow()const{
		return NDm::isBitSet<volatile unsigned int,15,unsigned int>(m_ffrx);
	}

	/**
	 * 清除接收队列溢出标志
	 */
	inline void clrRxFfOverFlow(){
		NDm::bitSet<volatile unsigned int,14,unsigned int>(m_ffrx);
	}

	/**
	 * 使能发送队列中断
	 */
	inline void enInt_txFf(){
		NDm::bitSet<volatile unsigned int,5,unsigned int>(m_fftx);
	}

	/**
	 * 使能接收队列中断
	 */
	inline void enInt_rxFf(){
		NDm::bitSet<volatile unsigned int,5,unsigned int>(m_ffrx);
	}

	/**
	 * 禁用发送队列中断
	 */
	inline void disInt_txFf(){
		NDm::bitClr<volatile unsigned int,5,unsigned int>(m_fftx);
	}

	/**
	 * 禁用接收队列中断
	 */
	inline void disInt_rxFf(){
		NDm::bitClr<volatile unsigned int,5,unsigned int>(m_ffrx);
	}

	/**
	 * 发送队列中断是否使能
	 * @return
	 */
	inline bool isIntEn_txFf()const{
		return NDm::isBitSet<volatile unsigned int,5,unsigned int>(m_fftx);
	}

	/**
	 * 接收队列中断是否使能
	 * @return
	 */
	inline bool isIntEn_rxFf()const{
		return NDm::isBitSet<volatile unsigned int,5,unsigned int>(m_ffrx);
	}

	/**
	 * 是否有发送队列中断
	 * @return
	 */
	inline bool isInt_txFf()const{
		return NDm::isBitSet<volatile unsigned int,7,unsigned int>(m_fftx);
	}

	/**
	 * 是否有接收队列中断
	 * @return
	 */
	inline bool isInt_rxFf()const{
		return NDm::isBitSet<volatile unsigned int,7,unsigned int>(m_ffrx);
	}

	/**
	 * 清除发送队列中断
	 */
	inline void clrInt_txFf(){
		NDm::bitSet<volatile unsigned int,6,unsigned int>(m_fftx);
	}

	/**
	 * 清除接收队列中断
	 */
	inline void clrInt_rxFf(){
		NDm::bitSet<volatile unsigned int,6,unsigned int>(m_ffrx);
	}

	/**
	 * 获取发送队列中断级别
	 * @return
	 */
	inline unsigned int getTxFfIntLevel()const{
		return m_fftx & 0x001F;
	}

	/**
	 * 获取接收队列中断级别
	 * @return
	 */
	inline unsigned int getRxFfIntLevel()const{
		return m_ffrx & 0x001F;
	}

	/**
	 * 设置发送队列中断级别
	 * @param level
	 */
	inline void setTxFfIntLevel( const unsigned int& level=0 )const{
		m_fftx = ((m_fftx&0xFFE0)|(level&0x001F));
	}

	/**
	 * 设置发送队列中断级别
	 * @param level
	 */
	inline void setRxFfIntLevel( const unsigned int& level=16 )const{
		m_ffrx = ((m_ffrx&0xFFE0)|(level&0x001F));
	}

protected:
	inline unsigned int getBrr()const{
		if( m_lbaud==0 && m_hbaud==0 )
			return 2;
		else
			return (m_lbaud&0x00FF)|(m_hbaud<<8);
	}

	inline void setBrr( const unsigned int& brr ){
		m_lbaud = (brr&0x00FF);
		m_hbaud = (brr>>8);
	}

	/**
	 * 默认的初始化参数
	 * 这个函数给后续简单使用做封装
	 * 默认设置使用FIFO模式，但是参数不对通信速率初始化
	 */
	void defaultInit();
private:
	volatile unsigned int& m_ccr;
	volatile unsigned int& m_ctl1;
	volatile unsigned int& m_ctl2;
	volatile unsigned int& m_lbaud;
	volatile unsigned int& m_hbaud;

	volatile unsigned int& m_rxst;
	volatile unsigned int& m_rxbuf;
	volatile unsigned int& m_txbuf;

	volatile unsigned int& m_fftx;
	volatile unsigned int& m_ffrx;
	volatile unsigned int& m_ffct;
};

}
}
}

#endif /* SCI_HPP_ */

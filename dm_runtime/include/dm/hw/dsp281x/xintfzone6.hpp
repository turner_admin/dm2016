/*
 * xintfzone6.hpp
 *
 *  Created on: 2016��3��9��
 *      Author: dylan
 */

#ifndef XINTFZONE6_HPP_
#define XINTFZONE6_HPP_

#include <dm/hw/dsp281x/xintfzone.hpp>

namespace NDm{
namespace NHw{
namespace NF281x{
class CXintfZone6:public CXintfZone{
public:
	CXintfZone6();
};
}
}
}



#endif /* XINTFZONE6_HPP_ */

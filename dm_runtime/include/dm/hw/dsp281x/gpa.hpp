/*
 * gpa.hpp
 *
 *  Created on: 2016��2��25��
 *      Author: dylan
 */

#ifndef GPA_HPP_
#define GPA_HPP_

#include <dm/hw/dsp281x/gpio.hpp>

namespace NDm{
namespace NHw{
namespace NF281x{

class CGpa:public CGpio{
	CGpa( const CGpa& );
public:
	CGpa();
};
}
}
}

#endif /* GPA_HPP_ */

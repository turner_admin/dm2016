/*
 * gpe.hpp
 *
 *  Created on: 2016��2��26��
 *      Author: dylan
 */

#ifndef GPE_HPP_
#define GPE_HPP_


#include <dm/hw/dsp281x/gpio.hpp>

namespace NDm{
namespace NHw{
namespace NF281x{

class CGpe:public CGpio{
	CGpe( const CGpe& );
public:
	CGpe();
};
}
}
}


#endif /* GPE_HPP_ */

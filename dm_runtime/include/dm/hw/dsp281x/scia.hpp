/*
 * scia.hpp
 *
 *  Created on: 2016��2��25��
 *      Author: dylan
 */

#ifndef SCIA_HPP_
#define SCIA_HPP_

#include <dm/hw/dsp281x/sci.hpp>

namespace NDm{
namespace NHw{
namespace NF281x{

class CScia:public CSci{
public:
	CScia();
};

}
}
}

#endif /* SCIA_HPP_ */

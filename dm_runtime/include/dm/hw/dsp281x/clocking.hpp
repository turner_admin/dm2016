/*
 * clocking.hpp
 *
 *  Created on: 2016年2月24日
 *      Author: dylan
 */

#ifndef CLOCKING_HPP_
#define CLOCKING_HPP_

namespace NDm{
namespace NHw{
namespace NF281x{

/**
 * 系统时钟控制类
 */
class CClocking{
public:
	/**
	 * 构造时，必须指定系统晶振或者输入时钟频率
	 * @param oscClk 输入时钟频率，单位：赫兹
	 */
	CClocking( const unsigned long& oscClk );

	/**
	 * 获取系统主频
	 */
	inline unsigned long getSysClk()const{
		return m_oscClk * getPllDiv() / 2;
	}

	/**
	 * 设置系统主频
	 * @param clk 要设置的主频
	 * @param wait 设置后等待计数次数
	 * @warning 本函数会禁用片上看门狗
	 */
	void setSysClk( unsigned long clk,const unsigned long& wait=10000 );

	inline unsigned long getHiSpClk()const{
		return getSysClk()/getHiSpRate();
	}

	inline unsigned long getLoSpClk()const{
		return getSysClk()/getLoSpRate();
	}

	inline unsigned long getClk_ecan()const{
		return getSysClk();
	}

	inline unsigned long getClk_timer()const{
		return getSysClk();
	}

	inline unsigned long getClk_sci()const{
		return getLoSpClk();
	}

	inline unsigned long getClk_spi()const{
		return getLoSpClk();
	}

	inline unsigned long getClk_mcbsp()const{
		return getLoSpClk();
	}

	inline unsigned long getClk_ev()const{
		return getHiSpClk();
	}

	inline unsigned long getClk_adc()const{
		return getHiSpClk();
	}

	/**
	 * 设置高速外设分频倍率
	 * @param rate
	 * 取值为部分值，@see setHiSpPrescaler()
	 */
	inline bool setHiSpRate( const int& rate ){
		if( (rate!=1 && rate%2) || rate<1 || rate>14 )
			return false;
		else
			setHiSpPrescaler(rate/2);
		return true;
	}

	inline bool setLoSpRate( const int& rate ){
		if( (rate!=1 && rate%2) || rate<1 || rate>14 )
			return false;
		else
			setLoSpPrescaler(rate/2);
		return true;
	}

protected:
	/**
	 * 获取PLL的分频
	 * @return 分频值
	 * @see getPllDiv()
	 */
	unsigned int getPllDiv()const;

	/**
	 * 设置PLL的分频
	 * @param div
	 * - 0 CLKIN = OSCCLK/2(PLL bypass )
	 * - 1 CLKIN = (OSCCLK*1.0)/2
	 * - 2 CLKIN = (OSCCLK*2.0)/2
	 * - 3 CLKIN = (OSCCLK*3.0)/2
	 * - 4 CLKIN = (OSCCLK*4.0)/2
	 * - 5 CLKIN = (OSCCLK*5.0)/2
	 * - 6 CLKIN = (OSCCLK*6.0)/2
	 * - 7 CLKIN = (OSCCLK*7.0)/2
	 * - 8 CLKIN = (OSCCLK*8.0)/2
	 * - 9 CLKIN = (OSCCLK*9.0)/2
	 * - 10 CLKIN = (OSCCLK*10.0)/2
	 * - 其他 保留
	 */
	void setPllDiv( const unsigned int& div );

	/**
	 * 获取锁相环倍频比例
	 * @return
	 */
	inline float getPllRate()const{
		return getPllDiv()/2.0;
	}

	/**
	 * 获取高速外设分频
	 * @return
	 * @see setHiSpPrescaler()
	 */
	unsigned int getHiSpPrescaler()const;

	/**
	 * 设置高速外设分频
	 * @param ps
	 * - 0 HSCLK = SYSCLKOUT/1
	 * - 1 HSCLK = SYSCLKOUT/2 默认
	 * - 2 HSCLK = SYSCLKOUT/4
	 * - 3 HSCLK = SYSCLKOUT/6
	 * - 4 HSCLK = SYSCLKOUT/8
	 * - 5 HSCLK = SYSCLKOUT/10
	 * - 6 HSCLK = SYSCLKOUT/12
	 * - 7 HSCLK = SYSCLKOUT/14
	 */
	void setHiSpPrescaler( const unsigned int& ps );

	/**
	 * 获取高速外设分频倍率
	 * @see setHiSpPrescaler()
	 * @return
	 */
	inline int getHiSpRate()const{
		int rt = getHiSpPrescaler()*2;
		if( rt==0 )
			return 1;
		else
			return rt;
	}



	/**
	 * 获取低速外设分频
	 * @see setHiSpPrescaler()
	 * @return
	 */
	unsigned int getLoSpPrescaler()const;

	/**
	 * 设置低速外设分频
	 * @see setHiSpPrescaler()
	 * @param ps
	 */
	void setLoSpPrescaler( const unsigned int& ps );

	inline int getLoSpRate()const{
		int rt = getLoSpPrescaler()*2;
		if( rt==0 )
			return 1;
		else
			return rt;
	}

private:
	unsigned long m_oscClk;
};

}
}
}

#endif /* CLOCKING_HPP_ */

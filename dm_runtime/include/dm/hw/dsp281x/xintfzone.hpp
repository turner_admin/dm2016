/*
 * xintfzone.hpp
 *
 *  Created on: 2016年3月9日
 *      Author: dylan
 */

#ifndef XINTFZONE_HPP_
#define XINTFZONE_HPP_

#include <dm/bitmask.hpp>

namespace NDm{
namespace NHw{
namespace NF281x{

class CXintfZone{
public:
	CXintfZone( volatile unsigned long& xtiming,void* address,const unsigned long& size );

	void setSlowest();
	void setFastest();

	/**
	 * 时钟参数是否双倍
	 * @return
	 */
	inline bool isTimingDoubled()const{
		return NDm::isBitSet<volatile unsigned long,22,unsigned long>(m_xtiming);
	}

	/**
	 * 设置时钟参数为双倍的
	 * @param en
	 */
	inline void setTimingDouble( const bool& en ){
		if( en )
			NDm::bitSet<volatile unsigned long,22,unsigned long>(m_xtiming,xtimingSetFix());
		else
			NDm::bitClr<volatile unsigned long,22,unsigned long>(m_xtiming,xtimingSetFix());
	}

	/**
	 * 当使能XREADY信号时，是否是异步信号
	 * @return
	 */
	inline bool isXReadyAsync()const{
		return NDm::isBitSet<volatile unsigned long,15,unsigned long>(m_xtiming);
	}

	/**
	 * 设置XReady信号为异步信号
	 */
	inline void setXReadyAsync(){
		NDm::bitSet<volatile unsigned long,15,unsigned long>(m_xtiming,xtimingSetFix());
	}

	/**
	 * 设置XReady信号为同步信号
	 */
	inline void setXReadySync(){
		NDm::bitClr<volatile unsigned long,15,unsigned long>(m_xtiming,xtimingSetFix());
	}

	/**
	 * XReady信号是否使用
	 * @return
	 */
	inline bool isEnXready()const{
		return NDm::isBitSet<volatile unsigned long,14,unsigned long>(m_xtiming);
	}

	/**
	 * 使用XReady信号
	 */
	inline void enXready(){
		NDm::bitSet<volatile unsigned long,14,unsigned long>(m_xtiming,xtimingSetFix());
	}

	/**
	 * 不使用XReady信号
	 */
	inline void disXready(){
		NDm::bitClr<volatile unsigned long,14,unsigned long>(m_xtiming,xtimingSetFix());
	}

	/**
	 * 设置Read操作建立延时
	 * @param xTimClk 0~3
	 */
	inline void setReadLead( const unsigned long& xTimClk ){
		m_xtiming = ((m_xtiming&NDm::BitMaskClr<unsigned long,12>::v&NDm::BitMaskClr<unsigned long,13>::v)|xtimingSetFix()|v2b_2(xTimClk,12));
	}

	/**
	 * 设置Read操作时延时
	 * @param xTimClk
	 */
	inline void setReadActive( const unsigned long& xTimClk ){
		m_xtiming = ((m_xtiming&NDm::BitMaskClr<unsigned long,9>::v&NDm::BitMaskClr<unsigned long,10>::v&NDm::BitMaskClr<unsigned long,11>::v)|xtimingSetFix()|v2b_3(xTimClk,9));
	}

	/**
	 * 设置Read操作后延时
	 * @param xTimClk
	 */
	inline void setReadTrail( const unsigned long& xTimClk ){
		m_xtiming = ((m_xtiming&NDm::BitMaskClr<unsigned long,7>::v&NDm::BitMaskClr<unsigned long,8>::v)|xtimingSetFix()|v2b_2(xTimClk,7));
	}

	/**
	 * 设置Write操作建立延时
	 * @param xTimClk 0~3
	 */
	inline void setWriteLead( const unsigned long& xTimClk ){
		m_xtiming = ((m_xtiming&NDm::BitMaskClr<unsigned long,5>::v&NDm::BitMaskClr<unsigned long,6>::v)|xtimingSetFix()|v2b_2(xTimClk,5));
	}

	inline void setWriteActive( const unsigned long& xTimClk ){
		m_xtiming = ((m_xtiming&NDm::BitMaskClr<unsigned long,2>::v&NDm::BitMaskClr<unsigned long,3>::v&NDm::BitMaskClr<unsigned long,4>::v)|xtimingSetFix()|v2b_3(xTimClk,2));
	}

	inline void setWriteTrail( const unsigned long& xTimClk ){
		m_xtiming = ((m_xtiming&NDm::BitMaskClr<unsigned long,0>::v&NDm::BitMaskClr<unsigned long,1>::v)|xtimingSetFix()|v2b_2(xTimClk,0));
	}

	inline void* address(){
		return m_addr;
	}

	inline const void* address()const{
		return m_addr;
	}

	inline const unsigned long& size()const{
		return m_size;
	}

protected:
	inline unsigned long xtimingSetFix()const{
		return NDm::BitMaskSet<unsigned long,16>::v|NDm::BitMaskSet<unsigned long,17>::v;
	}

	inline unsigned long v2b_2( const unsigned long& v,const int& shift ){
		return (v&0x03)<<shift;
	}

	inline unsigned long v2b_3( const unsigned long& v,const int& shift ){
		return (v&0x07)<<shift;
	}

private:
	volatile unsigned long m_xtiming;
	void* m_addr;
	unsigned long m_size;
};
}
}
}



#endif /* XINTFZONE_HPP_ */

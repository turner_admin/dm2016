/*
 * scib.hpp
 *
 *  Created on: 2016��3��1��
 *      Author: dylan
 */

#ifndef SCIB_HPP_
#define SCIB_HPP_

#include <dm/hw/dsp281x/sci.hpp>

namespace NDm{
namespace NHw{
namespace NF281x{
class CScib:public CSci{
public:
	CScib();
};
}
}
}

#endif /* SCIB_HPP_ */

/*
 * gpb.hpp
 *
 *  Created on: 2016��2��25��
 *      Author: dylan
 */

#ifndef GPB_HPP_
#define GPB_HPP_

#include <dm/hw/dsp281x/gpio.hpp>

namespace NDm{
namespace NHw{
namespace NF281x{

class CGpb:public CGpio{
	CGpb( const CGpb& );
public:
	CGpb();
};
}
}
}
#endif /* GPB_HPP_ */

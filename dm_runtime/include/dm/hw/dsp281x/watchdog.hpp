/*
 * watchdog.hpp
 *
 *  Created on: 2016年2月24日
 *      Author: dylan
 */

#ifndef WATCHDOG_HPP_
#define WATCHDOG_HPP_

namespace NDm{
namespace NHw{
namespace NF281x{

/**
 * 硬件看门狗
 */
class CWatchDog{
	CWatchDog( const CWatchDog& );
public:
	CWatchDog();

	/**
	 * 模块是否可以进行使能或者禁用操作
	 * @return
	 */
	bool isEnableAllowed()const;

	/**
	 * 允许进行模块的使能和禁用操作
	 * @param al
	 */
	void allowEnabling( const bool& al=true );

	/**
	 * 模块是否使能
	 * 默认为使能状态
	 * @return
	 */
	bool isEnabled()const;

	/**
	 * 使能模块
	 * @param en
	 * @note 必须在允许使能的状态下操作 @see isEnableAllowed()
	 */
	void enable( const bool& en=true );

	/**
	 * 获取时钟分频器
	 * @return 返回模块对OSCCLK/512的分频次数的2的幂次
	 * - 0,1 2^0=1 OSCCLK/512/1
	 * - 2   2^1=2 OSCCLK/512/2
	 * - 3   2^2=4 OSCCLK/512/4
	 * - 4   2^3=8 OSCCLK/512/8
	 * - 5   2^4=16 OSCCLK/512/16
	 * - 6   2^5=32 OSCCLK/512/32
	 * - 7   2^6=64 OSCCLK/512/64
	 */
	unsigned int getPrescaler()const;

	/**
	 * 设置时钟分频器
	 * @param ps 0~7
	 * @see getPrescaler()
	 */
	void setPrescaler( const unsigned int& ps );

	/**
	 * 获取时钟分频的倍率
	 * @see getPrescaler()
	 * @return
	 */
	int getClockRate()const;

	/**
	 * 设置时钟分频的倍率
	 * @see getPrescaler()
	 * @param rate @note 倍率只能取约定的值
	 * 1,2,4,8,16,32,64
	 */
	void setClockRate( const int& rate );

	/**
	 * 获取模块工作模式
	 * @return 工作模式
	 * -1 看门狗复位模式
	 * -0 中断模式.默认模式
	 */
	int getMode()const;

	/**
	 * 设置模块工作模式
	 * @param mode @see getMode()
	 *
	 */
	void setMode( const int& mode );

	inline bool isMode_reset()const{
		return getMode()==1;
	}

	inline bool isMode_interrupt()const{
		return getMode()==0;
	}

	inline void setMode_reset(){
		setMode(1);
	}

	inline void setMode_interrupt(){
		setMode(0);
	}

	/**
	 * 喂狗
	 */
	inline void feed(){
		m_key = 0x55;
		m_key = 0xAA;
	}

	/**
	 * 判断系统复位是否是因为看门狗
	 * @return
	 */
	bool isResetByWd()const;

	/**
	 * 清除看门狗复位系统标志
	 */
	void clrResetByWd();
private:
	volatile unsigned int& m_key;
};

}
}
}


#endif /* WATCHDOG_HPP_ */

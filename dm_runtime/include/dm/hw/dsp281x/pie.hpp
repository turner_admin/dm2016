/*
 * pie.hpp
 *
 *  Created on: 2016年2月26日
 *      Author: dylan
 */

#ifndef PIE_HPP_
#define PIE_HPP_

#include <dm/bitmask.hpp>

namespace NDm{
namespace NHw{
namespace NF281x{

extern "C"{
	typedef interrupt void(*INT_FUN)(void);
}

/**
 * PIE操作类
 * 一般的中断设置过程：
 * 1 禁用CPU全局中断，清除CPU中断使能和中断标志
 * 2 禁用PIE模块。初始化中断向量表
 * 3 设置中断服务，使能相应的PIEIER，使能相应的CPU中断
 * 4 使能PIE模块， 启用CPU全局中断
 */
class CPie{
public:
	CPie();

	void reset();

	bool isEnabled()const;

	/**
	 *
	 */
	void enable();
	void disable();

	// group 1

	inline void en_pdpIntA(){	NDm::bitSet<volatile unsigned int,0,unsigned int>(m_ier1);	}
	inline void dis_pdpIntA(){	NDm::bitClr<volatile unsigned int,0,unsigned int>(m_ier1);    }
	bool isEnabled_pdpIntA()const;
	void setIrs_pdpIntA( INT_FUN irs );
	inline void ack_pdpIntA(){
		ack(0);
	}

	inline void en_pdpIntB(){ NDm::bitSet<volatile unsigned int,1,unsigned int>(m_ier1); }
	inline void dis_pdpIntB(){	NDm::bitClr<volatile unsigned int,1,unsigned int>(m_ier1);    }
	bool isEnabled_pdpIntB()const;
	void setIrs_pdpIntB( INT_FUN irs );
	inline void ack_pdpIntB(){
		ack(0);
	}

	inline void en_xint1(){ NDm::bitSet<volatile unsigned int,3,unsigned int>(m_ier1); }
	inline void dis_xint1(){	NDm::bitClr<volatile unsigned int,3,unsigned int>(m_ier1);    }
	bool isEnabled_xInt1()const;
	void setIrs_xInt1( INT_FUN irs );
	inline void ack_xInt1(){
		ack(0);
	}

	inline void en_xint2(){ NDm::bitSet<volatile unsigned int,4,unsigned int>(m_ier1); }
	inline void dis_xint2(){	NDm::bitClr<volatile unsigned int,4,unsigned int>(m_ier1);    }
	bool isEnabled_xInt2()const;
	void setIrs_xInt2( INT_FUN irs );
	inline void ack_xInt2(){
		ack(0);
	}

	inline void en_adcInt(){ NDm::bitSet<volatile unsigned int,5,unsigned int>(m_ier1); }
	inline void dis_adcInt(){	NDm::bitClr<volatile unsigned int,5,unsigned int>(m_ier1);    }
	bool isEnabled_adcInt()const;
	void setIrs_adcInt( INT_FUN irs );
	inline void ack_adcInt(){
		ack(0);
	}

	inline void en_tInt0(){ NDm::bitSet<volatile unsigned int,6,unsigned int>(m_ier1); }
	inline void dis_tInt0(){	NDm::bitClr<volatile unsigned int,6,unsigned int>(m_ier1);    }
	bool isEnabled_tInt0()const;
	void setIrs_tInt0( INT_FUN irs );
	inline void ack_tInt0(){
		ack(0);
	}

	inline void en_wakeInt(){ NDm::bitSet<volatile unsigned int,7,unsigned int>(m_ier1); }
	inline void dis_wakeInt(){	NDm::bitClr<volatile unsigned int,7,unsigned int>(m_ier1);    }
	bool isEnabled_wakeInt()const;
	void setIrs_wakeInt( INT_FUN irs );
	inline void ack_wakeInt(){
		ack(0);
	}

	// group 2

	inline void en_cmp1Int(){ NDm::bitSet<volatile unsigned int,0,unsigned int>(m_ier2); }
	inline void dis_cmp1Int(){	NDm::bitClr<volatile unsigned int,0,unsigned int>(m_ier2);    }
	bool isEnabled_cmp1Int()const;
	void setIrs_cmp1Int( INT_FUN irs );
	inline void ack_cmp1Int(){
		ack(1);
	}

	inline void en_cmp2Int(){ NDm::bitSet<volatile unsigned int,1,unsigned int>(m_ier2); }
	inline void dis_cmp2Int(){	NDm::bitClr<volatile unsigned int,1,unsigned int>(m_ier2);    }
	bool isEnabled_cmp2Int()const;
	void setIrs_cmp2Int( INT_FUN irs );
	inline void ack_cmp2Int(){
		ack(1);
	}

	inline void en_cmp3Int(){ NDm::bitSet<volatile unsigned int,2,unsigned int>(m_ier2); }
	inline void dis_cmp3Int(){	NDm::bitClr<volatile unsigned int,2,unsigned int>(m_ier2);    }
	bool isEnabled_cmp3Int()const;
	void setIrs_cmp3Int( INT_FUN irs );
	inline void ack_cmp3Int(){
		ack(1);
	}

	inline void en_t1PInt(){ NDm::bitSet<volatile unsigned int,3,unsigned int>(m_ier2); }
	inline void dis_t1PInt(){	NDm::bitClr<volatile unsigned int,3,unsigned int>(m_ier2);    }
	bool isEnabled_t1PInt()const;
	void setIrs_t1PInt( INT_FUN irs );
	inline void ack_t1PInt(){
		ack(1);
	}

	inline void en_t1CInt(){ NDm::bitSet<volatile unsigned int,4,unsigned int>(m_ier2); }
	inline void dis_t1CInt(){	NDm::bitClr<volatile unsigned int,4,unsigned int>(m_ier2);    }
	bool isEnabled_t1CInt()const;
	void setIrs_t1CInt( INT_FUN irs );
	inline void ack_t1CInt(){
		ack(1);
	}

	inline void en_t1UfInt(){ NDm::bitSet<volatile unsigned int,5,unsigned int>(m_ier2); }
	inline void dis_t1UfInt(){	NDm::bitClr<volatile unsigned int,5,unsigned int>(m_ier2);    }
	bool isEnabled_t1UfInt()const;
	void setIrs_t1UfInt( INT_FUN irs );
	inline void ack_t1UfInt(){
		ack(1);
	}

	inline void en_t1OfInt(){ NDm::bitSet<volatile unsigned int,6,unsigned int>(m_ier2); }
	inline void dis_t1OfInt(){	NDm::bitClr<volatile unsigned int,6,unsigned int>(m_ier2);    }
	bool isEnabled_t1OfInt()const;
	void setIrs_t1OfInt( INT_FUN irs );
	inline void ack_t1OfInt(){
		ack(1);
	}

	// group 3

	inline void en_t2PInt(){ NDm::bitSet<volatile unsigned int,0,unsigned int>(m_ier3); }
	inline void dis_t2PInt(){	NDm::bitClr<volatile unsigned int,0,unsigned int>(m_ier3);    }
	bool isEnabled_t2PInt()const;
	void setIrs_t2PInt( INT_FUN irs );
	inline void ack_t2PInt(){
		ack(2);
	}

	inline void en_t2CInt(){ NDm::bitSet<volatile unsigned int,1,unsigned int>(m_ier3); }
	inline void dis_t2CInt(){	NDm::bitClr<volatile unsigned int,1,unsigned int>(m_ier3);    }
	bool isEnabled_t2CInt()const;
	void setIrs_t2CInt( INT_FUN irs );
	inline void ack_t2CInt(){
		ack(2);
	}

	inline void en_t2UfInt(){ NDm::bitSet<volatile unsigned int,2,unsigned int>(m_ier3); }
	inline void dis_t2UfInt(){	NDm::bitClr<volatile unsigned int,2,unsigned int>(m_ier3);    }
	bool isEnabled_t2UfInt()const;
	void setIrs_t2UfInt( INT_FUN irs );
	inline void ack_t2UfInt(){
		ack(2);
	}

	inline void en_t2OfInt(){ NDm::bitSet<volatile unsigned int,3,unsigned int>(m_ier3); }
	inline void dis_t2OfInt(){	NDm::bitClr<volatile unsigned int,3,unsigned int>(m_ier3);    }
	bool isEnabled_t2OfInt()const;
	void setIrs_t2OfInt( INT_FUN irs );
	inline void ack_t2OfInt(){
		ack(2);
	}

	inline void en_capInt1(){ NDm::bitSet<volatile unsigned int,4,unsigned int>(m_ier3); }
	inline void dis_capInt1(){	NDm::bitClr<volatile unsigned int,4,unsigned int>(m_ier3);    }
	bool isEnabled_capInt1()const;
	void setIrs_capInt1( INT_FUN irs );
	inline void ack_capInt1(){
		ack(2);
	}

	inline void en_capInt2(){ NDm::bitSet<volatile unsigned int,5,unsigned int>(m_ier3); }
	inline void dis_capInt2(){	NDm::bitClr<volatile unsigned int,5,unsigned int>(m_ier3);    }
	bool isEnabled_capInt2()const;
	void setIrs_capInt2( INT_FUN irs );
	inline void ack_capInt2(){
		ack(2);
	}

	inline void en_capInt3(){ NDm::bitSet<volatile unsigned int,6,unsigned int>(m_ier3); }
	inline void dis_capInt3(){	NDm::bitClr<volatile unsigned int,6,unsigned int>(m_ier3);    }
	bool isEnabled_capInt3()const;
	void setIrs_capInt3( INT_FUN irs );
	inline void ack_capInt3(){
		ack(2);
	}

	// group 4

	inline void en_cmp4Int(){ NDm::bitSet<volatile unsigned int,0,unsigned int>(m_ier4); }
	inline void dis_cmp4Int(){	NDm::bitClr<volatile unsigned int,0,unsigned int>(m_ier4);    }
	bool isEnabled_cmp4Int()const;
	void setIrs_cmp4Int( INT_FUN irs );
	inline void ack_cmp4Int(){
		ack(3);
	}

	inline void en_cmp5Int(){ NDm::bitSet<volatile unsigned int,1,unsigned int>(m_ier4); }
	inline void dis_cmp5Int(){	NDm::bitClr<volatile unsigned int,1,unsigned int>(m_ier4);    }
	bool isEnabled_cmp5Int()const;
	void setIrs_cmp5Int( INT_FUN irs );
	inline void ack_cmp5Int(){
		ack(3);
	}

	inline void en_cmp6Int(){ NDm::bitSet<volatile unsigned int,2,unsigned int>(m_ier4); }
	inline void dis_cmp6Int(){	NDm::bitClr<volatile unsigned int,2,unsigned int>(m_ier4);    }
	bool isEnabled_cmp6Int()const;
	void setIrs_cmp6Int( INT_FUN irs );
	inline void ack_cmp6Int(){
		ack(3);
	}

	inline void en_t3PInt(){ NDm::bitSet<volatile unsigned int,3,unsigned int>(m_ier4); }
	inline void dis_t3PInt(){	NDm::bitClr<volatile unsigned int,3,unsigned int>(m_ier4);    }
	bool isEnabled_t3PInt()const;
	void setIrs_t3PInt( INT_FUN irs );
	inline void ack_t3PInt(){
		ack(3);
	}

	inline void en_t3CInt(){ NDm::bitSet<volatile unsigned int,4,unsigned int>(m_ier4); }
	inline void dis_t3CInt(){	NDm::bitClr<volatile unsigned int,4,unsigned int>(m_ier4);    }
	bool isEnabled_t3CInt()const;
	void setIrs_t3CInt( INT_FUN irs );
	inline void ack_t3CInt(){
		ack(3);
	}

	inline void en_t3UfInt(){ NDm::bitSet<volatile unsigned int,5,unsigned int>(m_ier4); }
	inline void dis_t3UfInt(){	NDm::bitClr<volatile unsigned int,5,unsigned int>(m_ier4);    }
	bool isEnabled_t3UfInt()const;
	void setIrs_t3UfInt( INT_FUN irs );
	inline void ack_t3UfInt(){
		ack(3);
	}

	inline void en_t3OfInt(){ NDm::bitSet<volatile unsigned int,6,unsigned int>(m_ier4); }
	inline void dis_t3OfInt(){	NDm::bitClr<volatile unsigned int,6,unsigned int>(m_ier4);    }
	bool isEnabled_t3OfInt()const;
	void setIrs_t3OfInt( INT_FUN irs );
	inline void ack_t3OfInt(){
		ack(3);
	}

	// group 5

	inline void en_t4PInt(){ NDm::bitSet<volatile unsigned int,0,unsigned int>(m_ier5); }
	inline void dis_t4PInt(){	NDm::bitClr<volatile unsigned int,0,unsigned int>(m_ier5);    }
	bool isEnabled_t4PInt()const;
	void setIrs_t4PInt( INT_FUN irs );
	inline void ack_t4PInt(){
		ack(4);
	}

	inline void en_t4CInt(){ NDm::bitSet<volatile unsigned int,1,unsigned int>(m_ier5); }
	inline void dis_t4CInt(){	NDm::bitClr<volatile unsigned int,1,unsigned int>(m_ier5);    }
	bool isEnabled_t4CInt()const;
	void setIrs_t4CInt( INT_FUN irs );
	inline void ack_t4CInt(){
		ack(4);
	}

	inline void en_t4UfInt(){ NDm::bitSet<volatile unsigned int,2,unsigned int>(m_ier5); }
	inline void dis_t4UfInt(){	NDm::bitClr<volatile unsigned int,2,unsigned int>(m_ier5);    }
	bool isEnabled_t4UfInt()const;
	void setIrs_t4UfInt( INT_FUN irs );
	inline void ack_t4UfInt(){
		ack(4);
	}

	inline void en_t4OfInt(){ NDm::bitSet<volatile unsigned int,3,unsigned int>(m_ier5); }
	inline void dis_t4OfInt(){	NDm::bitClr<volatile unsigned int,3,unsigned int>(m_ier5);    }
	bool isEnabled_t4OfInt()const;
	void setIrs_t4OfInt( INT_FUN irs );
	inline void ack_t4OfInt(){
		ack(4);
	}

	inline void en_capInt4(){ NDm::bitSet<volatile unsigned int,4,unsigned int>(m_ier5); }
	inline void dis_capInt4(){	NDm::bitClr<volatile unsigned int,4,unsigned int>(m_ier5);    }
	bool isEnabled_capInt4()const;
	void setIrs_capInt4( INT_FUN irs );
	inline void ack_capInt4(){
		ack(4);
	}

	inline void en_capInt5(){ NDm::bitSet<volatile unsigned int,5,unsigned int>(m_ier5); }
	inline void dis_capInt5(){	NDm::bitClr<volatile unsigned int,5,unsigned int>(m_ier5);    }
	bool isEnabled_capInt5()const;
	void setIrs_capInt5( INT_FUN irs );
	inline void ack_capInt5(){
		ack(4);
	}

	inline void en_capInt6(){ NDm::bitSet<volatile unsigned int,6,unsigned int>(m_ier5); }
	inline void dis_capInt6(){	NDm::bitClr<volatile unsigned int,6,unsigned int>(m_ier5);    }
	bool isEnabled_capInt6()const;
	void setIrs_capInt6( INT_FUN irs );
	inline void ack_capInt6(){
		ack(4);
	}

	// group 6

	inline void en_spiRxIntA(){ NDm::bitSet<volatile unsigned int,0,unsigned int>(m_ier6); }
	inline void dis_spiRxIntA(){	NDm::bitClr<volatile unsigned int,0,unsigned int>(m_ier6);    }
	bool isEnabled_spiRxIntA()const;
	void setIrs_spiRxIntA( INT_FUN irs );
	inline void ack_spiRxIntA(){
		ack(5);
	}

	inline void en_spiTxIntA(){ NDm::bitSet<volatile unsigned int,1,unsigned int>(m_ier6); }
	inline void dis_spiTxIntA(){	NDm::bitClr<volatile unsigned int,1,unsigned int>(m_ier6);    }
	bool isEnabled_spiTxIntA()const;
	void setIrs_spiTxIntA( INT_FUN irs );
	inline void ack_spiTxIntA(){
		ack(5);
	}

	inline void en_mrIntA(){ NDm::bitSet<volatile unsigned int,4,unsigned int>(m_ier6); }
	inline void dis_mrIntA(){	NDm::bitClr<volatile unsigned int,4,unsigned int>(m_ier6);    }
	bool isEnabled_mrIntA()const;
	void setIrs_mrIntA( INT_FUN irs );
	inline void ack_mrIntA(){
		ack(5);
	}

	inline void en_mxIntA(){ NDm::bitSet<volatile unsigned int,5,unsigned int>(m_ier6); }
	inline void dis_mxIntA(){	NDm::bitClr<volatile unsigned int,5,unsigned int>(m_ier6);    }
	bool isEnabled_mxIntA()const;
	void setIrs_mxIntA( INT_FUN irs );
	inline void ack_mxIntA(){
		ack(5);
	}

	// group 7

	// group 8

	// group 9

	inline void en_rxAInt(){ NDm::bitSet<volatile unsigned int,0,unsigned int>(m_ier9); }
	inline void dis_rxAInt(){	NDm::bitClr<volatile unsigned int,0,unsigned int>(m_ier9);    }
	bool isEnabled_rxAInt()const;
	void setIrs_rxAInt( INT_FUN irs );
	inline void ack_rxAInt(){
		ack(8);
	}

	inline void en_txAInt(){ NDm::bitSet<volatile unsigned int,1,unsigned int>(m_ier9); }
	inline void dis_txAInt(){	NDm::bitClr<volatile unsigned int,1,unsigned int>(m_ier9);    }
	bool isEnabled_txAInt()const;
	void setIrs_txAInt( INT_FUN irs );
	inline void ack_txAInt(){
		ack(8);
	}

	inline void en_rxBInt(){ NDm::bitSet<volatile unsigned int,2,unsigned int>(m_ier9); }
	inline void dis_rxBInt(){	NDm::bitClr<volatile unsigned int,2,unsigned int>(m_ier9);    }
	bool isEnabled_rxBInt()const;
	void setIrs_rxBInt( INT_FUN irs );
	inline void ack_rxBInt(){
		ack(8);
	}

	inline void en_txBInt(){ NDm::bitSet<volatile unsigned int,3,unsigned int>(m_ier9); }
	inline void dis_txBInt(){	NDm::bitClr<volatile unsigned int,3,unsigned int>(m_ier9);    }
	bool isEnabled_txBInt()const;
	void setIrs_txBInt( INT_FUN irs );
	inline void ack_txBInt(){
		ack(8);
	}

	inline void en_eCan0IntA(){ NDm::bitSet<volatile unsigned int,4,unsigned int>(m_ier9); }
	inline void dis_eCan0IntA(){	NDm::bitClr<volatile unsigned int,4,unsigned int>(m_ier9);    }
	bool isEnabled_eCan0IntA()const;
	void setIrs_eCan0IntA( INT_FUN irs );
	inline void ack_eCan0IntA(){
		ack(8);
	}

	inline void en_eCan1IntA(){ NDm::bitSet<volatile unsigned int,5,unsigned int>(m_ier9); }
	inline void dis_eCan1IntA(){	NDm::bitClr<volatile unsigned int,5,unsigned int>(m_ier9);    }
	bool isEnabled_eCan1IntA()const;
	void setIrs_eCan1IntA( INT_FUN irs );
	inline void ack_eCan1IntA(){
		ack(8);
	}

	// group 10

	// group 11

	// group 12

protected:
	/**
	 * 中断确认
	 * @param group
	 */
	inline void ack( const int& group ){
		m_ack |= (0x0001<<group);
	}
private:
	volatile unsigned int& m_ack;

	volatile unsigned int& m_ier1;
	volatile unsigned int& m_ier2;
	volatile unsigned int& m_ier3;
	volatile unsigned int& m_ier4;
	volatile unsigned int& m_ier5;
	volatile unsigned int& m_ier6;

	volatile unsigned int& m_ier7;
	volatile unsigned int& m_ier8;
	volatile unsigned int& m_ier9;
	volatile unsigned int& m_ier10;
	volatile unsigned int& m_ier11;
	volatile unsigned int& m_ier12;
};
}
}
}

#endif /* PIE_HPP_ */

/*
 * gpg.hpp
 *
 *  Created on: 2016��2��26��
 *      Author: dylan
 */

#ifndef GPG_HPP_
#define GPG_HPP_

#include <dm/hw/dsp281x/gpio.hpp>

namespace NDm{
namespace NHw{
namespace NF281x{

class CGpg:public CGpio{
	CGpg( const CGpg& );
public:
	CGpg();
};
}
}
}



#endif /* GPG_HPP_ */

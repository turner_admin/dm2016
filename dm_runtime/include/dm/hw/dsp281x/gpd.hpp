/*
 * gpd.hpp
 *
 *  Created on: 2016��2��26��
 *      Author: dylan
 */

#ifndef GPD_HPP_
#define GPD_HPP_

#include <dm/hw/dsp281x/gpio.hpp>

namespace NDm{
namespace NHw{
namespace NF281x{

class CGpd:public CGpio{
	CGpd( const CGpd& );
public:
	CGpd();
};
}
}
}

#endif /* GPD_HPP_ */

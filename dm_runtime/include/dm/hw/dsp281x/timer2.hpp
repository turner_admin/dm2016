/*
 * timer2.hpp
 *
 *  Created on: 2016年2月26日
 *      Author: dylan
 */

#ifndef TIMER2_HPP_
#define TIMER2_HPP_

#include <dm/hw/dsp281x/timer.hpp>

namespace NDm{
namespace NHw{
namespace NF281x{

/**
 * CPU Timer2
 * 一般用于实时操作系统.使用BIOS的应用程序不应该使用这个定时器
 * 使用中断INT14
 */
class CTimer2:public CTimer{
public:
	CTimer2();
};

}
}
}
#endif /* TIMER2_HPP_ */

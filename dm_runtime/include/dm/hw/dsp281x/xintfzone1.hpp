/*
 * xintfzone1.hpp
 *
 *  Created on: 2016��3��9��
 *      Author: dylan
 */

#ifndef XINTFZONE1_HPP_
#define XINTFZONE1_HPP_

#include <dm/hw/dsp281x/xintfzone.hpp>

namespace NDm{
namespace NHw{
namespace NF281x{
class CXintfZone1:public CXintfZone{
public:
	CXintfZone1();
};
}
}
}


#endif /* XINTFZONE1_HPP_ */

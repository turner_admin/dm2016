/*
 * comtradereader.hpp
 *
 *  Created on: 2019年11月14日
 *      Author: turne
 */

#ifndef _DM_APP_COMTRADE_READER_HPP_
#define _DM_APP_COMTRADE_READER_HPP_

#include <dm/app/comtradefiles.hpp>
#include <fstream>

namespace dm{
namespace app{

class CComtradeReader:public CComtradeFiles{
public:
	CComtradeReader();
	~CComtradeReader();

	bool load();
	bool getNext( measure_t* measures,int measureSize,status_t* status,int statusSize,dm::CTimeStamp* ts );
private:
	dm::uint32 m_index;
	std::ifstream m_istr;
};

}
}

#endif /* _DM_APP_COMTRADE_READER_HPP_ */

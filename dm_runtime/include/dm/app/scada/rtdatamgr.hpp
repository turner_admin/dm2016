/*
 * rtdatamgr.hpp
 *
 *  Created on: 2016��7��31��
 *      Author: work
 */

#ifndef DM_APP_SCADA_RTDATAMGR_HPP_
#define DM_APP_SCADA_RTDATAMGR_HPP_

#include <dm/app/scada/types.hpp>
#include <boost/atomic.hpp>

namespace dm
{
namespace app{
namespace scada{

class CRtDataMgr{
public:
	template<typename value_t>
	class TRtData{
	public:
		TRtData( value_t v=0 ):m_v(v){
		}

		inline void set( const value_t& v ){
			m_v.store(v);
		}

		inline value_t get(){
			return m_v.load();
		}

		inline bool update( const value_t& v ){
			return m_v.exchange(v)!=v;
		}
	private:
		boost::atomic<value_t> m_v;
	};

	typedef int value_real_s;

	typedef TRtData<value_sstatus> CSStatus;
	typedef TRtData<value_dstatus> CDStatus;
	typedef TRtData<value_sample> CSample;
	typedef TRtData<value_real_s> CRealShadow;
	typedef TRtData<value_measure> CMeasure;

private:
	CRtDataMgr();
public:
	static CRtDataMgr& ins();

	inline CSStatus* sStatus(){
		return m_sStatus;
	}

	const unsigned int& getSStatusSize()const;

	inline CDStatus* dStatus(){
		return m_dStatus;
	}

	const unsigned int& getDStatusSize()const;

	inline CSample* samples(){
		return m_samples;
	}

	const unsigned int& getSampleSize()const;

	inline CRealShadow* realShadows(){
		return m_reals;
	}

	const unsigned int& getRealSize()const;

	value_real getReal( const unsigned int& no );
	void setReal( const unsigned int& no,const value_real& v );
	bool updateReal( const unsigned int& no,const value_real& v );

	inline CMeasure* measures(){
		return m_measures;
	}

	const unsigned int& getMeasureSize()const;
private:
	CSStatus* m_sStatus;
	CDStatus* m_dStatus;
	CSample* m_samples;
	CRealShadow* m_reals;
	CMeasure* m_measures;
};
}
}
}

#endif /* RTDATAMGR_HPP_ */

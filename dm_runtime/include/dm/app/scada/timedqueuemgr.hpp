/*
 * timedqueuemgr.hpp
 *
 *  Created on: 2016��7��31��
 *      Author: work
 */

#ifndef DM_APP_SCADA_TIMEDQUEUEMGR_HPP_
#define DM_APP_SCADA_TIMEDQUEUEMGR_HPP_

#include <dm/app/scada/types.hpp>
#include <sys/time.h>
#include <boost/interprocess/sync/interprocess_mutex.hpp>
#include <boost/interprocess/sync/interprocess_condition.hpp>
#include <boost/interprocess/sync/scoped_lock.hpp>
#include <boost/interprocess/offset_ptr.hpp>

namespace dm{
namespace app{
namespace scada{

class CTimedQueueMgr{
	CTimedQueueMgr();
public:
	typedef unsigned int no_t;
	typedef timeval tp_t;
	typedef unsigned int pos_t;
	typedef boost::interprocess::interprocess_mutex mutex_t;
	typedef boost::interprocess::interprocess_condition condition_t;
	typedef boost::interprocess::scoped_lock<mutex_t> scoped_lock;

	template<typename value_t>
	class TRing{
		TRing( const TRing& );
		TRing& operator=( const TRing& );
	public:
		struct SRecord{
			SRecord& operator=( const SRecord& r ){
				no = r.no;
				v = r.v;
				t.tv_sec = r.t.tv_sec;
				t.tv_usec = r.t.tv_usec;

				return *this;
			}

			no_t no;
			value_t v;
			tp_t t;
		};

		class CPos{
		public:
			static const pos_t size_overflow = (pos_t(0) - 1);

			CPos( pos_t s ):m_p(0),m_c(0){
				size = s;
			}

			CPos( const CPos& p ):m_p(p.m_p),m_c(p.m_c){
			}

			CPos& operator=( const CPos& p ){
				m_p = p.m_p;
				m_c = p.m_c;
				return *this;
			}

			CPos& operator++(){
				++m_p;
				if( m_p==size ){
					++m_c;
					m_p = 0;
				}

				return *this;
			}

			bool operator==( const CPos& p )const{
				return (m_p==p.m_p) && (m_c==p.m_c);
			}

			bool operator!=( const CPos& p )const{
				return (m_c!=p.m_c)||(m_p!=p.m_p );
			}

			pos_t operator-( const CPos& p )const{
				if( m_c==p.m_c ){
					if( m_p<p.m_p )
						return size_overflow;
					else
						return m_p - p.m_p;
				}else if( (p.m_c+1)==m_c ){
					return m_p + (size-p.m_p);
				}else
					return size_overflow;
			}

			inline const pos_t& p()const{
				return m_p;
			}

			inline const pos_t& c()const{
				return m_c;
			}

			inline const pos_t& s()const{
				return size;
			}
		private:
			pos_t m_p;
			pos_t m_c;
			static pos_t size;
			friend class TRing;
		};

		typedef boost::interprocess::offset_ptr<SRecord> record_ptr;
	public:
		TRing( record_ptr buf,pos_t size ):m_pos(size),m_buf(buf),m_mutex(){
		}

		void push( const SRecord& r ){
			scoped_lock lock(m_mutex);
			m_buf[m_pos.m_p] = r;
			++m_pos;
			m_condition.notify_all();
		}

		CPos curPos(){
			scoped_lock lock(m_mutex);
			return m_pos;
		}

		bool ifOverflow( const CPos& pos ){
			return (curPos()-pos)>CPos::size;
		}

		bool fetchAndMove( CPos& pos,SRecord& record ){
			if( pos==curPos() )
				return false;
			record = m_buf[pos.m_p];
			return true;
		}

		bool fetchAndMove_wait( CPos& pos,SRecord& record ){
			if( fetchAndMove(pos,record) )
				return true;
			else{
				scoped_lock lock(m_mutex);
				m_condition.wait(lock);
				return fetchAndMove(pos,record);
			}
		}

	private:
		record_ptr m_buf;
		CPos m_pos;
		mutex_t m_mutex;
		condition_t m_condition;
	};
public:
	typedef TRing<value_sstatus> queue_sstatus_t;
	typedef TRing<value_dstatus> queue_dstatus_t;
	typedef TRing<value_sample> queue_sample_t;
	typedef TRing<value_real> queue_real_t;
	typedef TRing<value_measure> queue_measure_t;

	static CTimedQueueMgr& ins();

	inline queue_sstatus_t* sStatusQueue(){
		return m_sstatus;
	}

	inline queue_dstatus_t* dStatusQueue(){
		return m_dstatus;
	}

	inline queue_sample_t* sampleQueue(){
		return m_sample;
	}

	inline queue_real_t* realQueue(){
		return m_real;
	}

	inline queue_measure_t* measureQueue(){
		return m_measure;
	}
private:
	queue_sstatus_t* m_sstatus;
	queue_dstatus_t* m_dstatus;
	queue_sample_t* m_sample;
	queue_real_t* m_real;
	queue_measure_t* m_measure;
};

template<typename value_t>
CTimedQueueMgr::pos_t CTimedQueueMgr::TRing<value_t>::CPos::size(0);

}
}
}



#endif /* TIMEDQUEUEMGR_HPP_ */

/*
 * types.hpp
 *
 *  Created on: 2016��7��31��
 *      Author: work
 */

#ifndef DM_APP_SCADA_TYPES_HPP_
#define DM_APP_SCADA_TYPES_HPP_

namespace dm{
namespace app{
namespace scada{

typedef unsigned char value_sstatus;
typedef unsigned char value_dstatus;
typedef short value_sample;
typedef float value_real;
typedef unsigned int value_measure;

}
}
}

#endif /* TYPES_HPP_ */

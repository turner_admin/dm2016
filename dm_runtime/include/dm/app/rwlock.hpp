/*
 * rwlock.hpp
 *
 *  Created on: 2016��4��18��
 *      Author: work
 */

#ifndef DM_APP_RWLOCK_HPP_
#define DM_APP_RWLOCK_HPP_

#include <dm/app/lock.hpp>

namespace NDm{
namespace NApp{
class CRwLock:public CLock{
public:
	virtual bool lockShared()=0;
	virtual bool tryLockShared()=0;
};
}
}

#endif /* RWLOCK_HPP_ */

/*
 * protocolbase.hpp
 *
 *  Created on: 2016年3月18日
 *      Author: dylan
 */

#ifndef DM_PROTOCOLBASE_HPP_
#define DM_PROTOCOLBASE_HPP_

#include <dm/types.hpp>
#include <dm/ringbuf.hpp>
#include <dm/frame/frame.hpp>
#include <dm/bitmask.hpp>

namespace NDm{
namespace NProtocol{

class CProtocolBase{
public:
	typedef NDm::TRingBuf<NDm::uint8,NDm::uint> CRingBuf;
	typedef CRingBuf::CRingPos CRingPos;

	enum EErrFlag{
		ErrSession = 0,	// 规约会话错误
		ErrRxOver,			// 接收缓冲区溢出
		ErrTxOver,			// 发送缓冲区溢出

		ErrMax
	};

public:
	CProtocolBase( NDm::NFrame::CFrame& rxFrame,NDm::uint8* rxBuf,const NDm::uint& rxSize,NDm::uint8* txBuf,const NDm::uint& txSize );

	void reset();

	inline bool isErr()const{
		return m_errFlags!=0;
	}

	inline void checkError(){
		if( isErr() )
			reset();
	}

	bool pushRx( const NDm::uint8* buf,const NDm::uint& size );

	NDm::uint fetchTx( NDm::uint8* buf,const NDm::uint& size );

	inline void removeTx( const NDm::uint& len ){
                m_txRPos += len;
	}

protected:
	virtual bool checkRxFrame( const NDm::NFrame::CFrame& frame );
	virtual bool dealWithRxFrame( const NDm::NFrame::CFrame& frame )=0;
	virtual bool doSomeTx();

	virtual void rxGram( const CRingBuf& rbuf,const CRingPos& end,const NDm::uint& size,const NDm::NFrame::CFrame& frame );
	virtual void txGram( const CRingBuf& rbuf,const CRingPos& end,const NDm::uint& size,const NDm::NFrame::CFrame& frame );

	bool sendFrame( const NDm::NFrame::CFrame& frame );

	inline void setErr_session(){
		NDm::bitSet<NDm::uint,ErrSession>(m_errFlags);
	}

	inline void setErr_rxOver(){
		NDm::bitSet<NDm::uint,ErrRxOver>(m_errFlags);
	}

	inline void setErr_txOver(){
		NDm::bitSet<NDm::uint,ErrTxOver>(m_errFlags);
	}
protected:
	NDm::uint m_errFlags;

        CRingBuf m_rxRBuf;
        CRingBuf m_txRBuf;
        CRingPos m_rxRPos;
        CRingPos m_txRPos;

	NDm::NFrame::CFrame& m_rxFrame;
};
}
}



#endif /* INC_PROTOCOL_PROTOCOLBASE_HPP_ */

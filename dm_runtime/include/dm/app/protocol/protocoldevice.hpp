/*
 * protocoldevice.hpp
 *
 *  Created on: 2016��3��18��
 *      Author: dylan
 */

#ifndef DM_PROTOCOL_PROTOCOLDEVICE_HPP_
#define DM_PROTOCOL_PROTOCOLDEVICE_HPP_

#include <dm/protocol/protocolbase.hpp>
namespace NDm{
namespace NProtocol{

class CProtocolDevice:public CProtocolBase{
public:
	CProtocolDevice( NDm::NFrame::CFrame& rxFrame,NDm::uint8* rxBuf,const NDm::uint& rxSize,NDm::uint8* txBuf,const NDm::uint& txSize );
	virtual ~CProtocolDevice();

	void runOnce();

	virtual void resetDevice();
	virtual bool isDeviceReset();

	inline void resetDeviceAndProtocol(){
		resetDevice();
		reset();
	}

protected:
	virtual NDm::uint rx( NDm::uint8* buf,const NDm::uint& size )=0;
	virtual NDm::uint tx( const NDm::uint8* buf,const NDm::uint& size )=0;
};
}
}

#endif /* INC_PROTOCOL_PROTOCOLDEVICE_HPP_ */

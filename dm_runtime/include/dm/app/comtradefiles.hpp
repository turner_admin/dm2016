/*
 * comtrade.hpp
 *
 *  Created on: 2019年11月14日
 *      Author: turne
 */

#ifndef _DM_APP_COMTRADE_FILES_HPP_
#define _DM_APP_COMTRADE_FILES_HPP_

#include <dm/timestamp.hpp>
#include <vector>

namespace dm{
namespace app{

class CComtradeFiles{
public:
	/**
	 * 数据文件格式
	 */
	enum EFormat{
		FormatUnknow,//!< FormatUnknow 未知
		FormatAscii, //!< FormatAscii ASCII
		FormatBinary //!< FormatBinary BINARY
	};

	/**
	 * 速率配置项
	 */
	class CRate{
	public:
		CRate( const CRate& rate );
		CRate( const float& samp=0,const long& endSamp=0 );

		CRate& operator=( const CRate& rate );

		inline const float& samp()const{
			return m_samp;
		}

		inline const long& endSamp()const{
			return m_endSamp;
		}

		inline void setSamp( const float& samp ){
			m_samp = samp;
		}

		inline void setEndSamp( const long& endSamp ){
			m_endSamp = endSamp;
		}
	private:
		float m_samp;	// 采样频率
		long m_endSamp;	// 采样数
	};

	class CChannelInfo{
	public:
		CChannelInfo();
		CChannelInfo( const CChannelInfo& info );
		~CChannelInfo();

		CChannelInfo& operator=( const CChannelInfo& info );

		inline const char* chId()const{
			return m_chId;
		}

		void setChId( const char* v );

		inline const char* ph()const{
			return m_ph;
		}

		void setPh( const char* v );

		inline const char* ccbm()const{
			return m_ccbm;
		}

		void setCcbm( const char* v );

	protected:
		char* m_chId;	// 通道标识
		char* m_ph;		// 相别
		char* m_ccbm;	// 所属元件
	};

	class CMeasureInfo:public CChannelInfo{
	public:
		CMeasureInfo();
		CMeasureInfo( const CMeasureInfo& info );
		~CMeasureInfo();

		CMeasureInfo& operator=( const CMeasureInfo& info );

		inline const char* uu()const{
			return m_uu;
		}

		void setUu( const char* v );

		inline const float& a()const{
			return m_a;
		}

		void setA( const float& v );

		inline const float& b()const{
			return m_b;
		}

		void setB( const float& v );

		inline const float& skew()const{
			return m_skew;
		}

		void setSkew( const float& v );

		inline const int& min()const{
			return m_min;
		}

		void setMin( const int& v );

		inline const int& max()const{
			return m_max;
		}

		void setMax( const int& v );

		inline const float& primary()const{
			return m_primary;
		}

		void setPrimary( const float& v );

		inline const float& secondary()const{
			return m_secondary;
		}

		void setSecondary( const float& v );

		inline const char& ps()const{
			return m_ps;
		}

		void setPs( const char& v );
	private:
		char* m_uu;

		float m_a;
		float m_b;
		float m_skew;
		int m_min;
		int m_max;
		float m_primary;
		float m_secondary;

		char m_ps;
	};

	class CStatusInfo:public CChannelInfo{
	public:
		CStatusInfo();
		CStatusInfo( const CStatusInfo& info );
		~CStatusInfo();

		CStatusInfo& operator=( const CStatusInfo& info );

		inline const bool& y()const{
			return m_y;
		}

		void setY( const bool& v );
	private:
		bool m_y;
	};

    typedef dm::int16 measure_t;
	typedef bool status_t;

public:
	CComtradeFiles();
	~CComtradeFiles();

	inline const char* name()const{
		return m_name;
	}

	void setName( const char* name );

	inline const char* dir()const{
		return m_dir;
	}

	void setDir( const char* dir );

	inline const char* stationName()const{
		return m_stationName;
	}

	inline const char* recDevId()const{
		return m_recDevId;
	}

	inline const int& revYear()const{
		return m_revYear;
	}

	inline const float& netFreq()const{
		return m_if;
	}

	inline unsigned int ratesCount()const{
		return m_rates.size();
	}

	const CRate& rate( const unsigned int& idx )const;

	inline const dm::CTimeStamp& tsStart()const{
		return m_tsStart;
	}

	inline const dm::CTimeStamp& tsTrig()const{
		return m_tsTrig;
	}

	inline const EFormat& ft()const{
		return m_ft;
	}

	inline const float& timeMult()const{
		return m_timeMult;
	}

	inline unsigned int measureCount()const{
		return m_measures.size();
	}

	const CMeasureInfo& measure( const unsigned int& index )const;

	inline unsigned int statusCount()const{
		return m_status.size();
	}

	const CStatusInfo& status( const unsigned int& index )const;

	unsigned int rows()const;
protected:
	char* m_name;
	char* m_dir;

	char* m_stationName;
	char* m_recDevId;
	int m_revYear;
	float m_if;

	std::vector<CRate> m_rates;

	dm::CTimeStamp m_tsStart;
	dm::CTimeStamp m_tsTrig;

	EFormat m_ft;
	float m_timeMult;

	std::vector<CMeasureInfo> m_measures;
	std::vector<CStatusInfo> m_status;
};

}
}



#endif /* _DM_APP_COMTRADE_FILES_HPP_ */

/*
 * comtradewriter.hpp
 *
 *  Created on: 2019年11月14日
 *      Author: turne
 */

#ifndef _DM_APP_COMTRADE_WRITER_HPP_
#define _DM_APP_COMTRADE_WRITER_HPP_

#include <dm/app/comtradefiles.hpp>
#include <fstream>

namespace dm{
namespace app{

class CComtradeWriter:public CComtradeFiles{
public:
	CComtradeWriter();
	~CComtradeWriter();

	void setStationName( const char* s );
	void setRecDevId( const char* s );
	void setNetFreq( const float& f );

	void clearRates();
	void addRate( const CRate& rate );

	void setTsStart( const dm::CTimeStamp& ts );
	void setTsTrig( const dm::CTimeStamp& ts );

	void setFt( const EFormat& f );
	void setTimeMult( const float& f );

	void clearMeasures();
	void addMeasureInfo( const CMeasureInfo& m );

	void clearStatus();
	void addStatusInfo( const CStatusInfo& s );

	void reset();
	bool dump();
	bool append( const measure_t* measures,const unsigned int& measureSize,const status_t* status,const unsigned int& statusSize,const dm::CTimeStamp* ts=NULL );
private:
	dm::uint32 m_index;
	std::ofstream m_ostr;
};

}
}

#endif /* _DM_APP_COMTRADE_WRITER_HPP_ */

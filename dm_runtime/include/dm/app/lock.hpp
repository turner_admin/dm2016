/*
 * lock.hpp
 *
 *  Created on: 2016��4��18��
 *      Author: work
 */

#ifndef DM_APP_LOCK_HPP_
#define DM_APP_LOCK_HPP_

namespace NDm{
namespace NApp{

class CLock{
public:
	virtual bool lock()=0;
	virtual bool tryLock()=0;
	virtual bool unlock()=0;
};

}
}

#endif /* LOCK_HPP_ */

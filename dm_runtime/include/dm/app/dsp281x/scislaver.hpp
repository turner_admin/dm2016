/*
 * scislaver.hpp
 *
 *  Created on: 2016年3月3日
 *      Author: dylan
 */

#ifndef SCISLAVER_HPP_
#define SCISLAVER_HPP_

#include <dm/ringbuf.hpp>
#include <dm/hw/dsp281x/sci.hpp>
#include <dm/hw/dsp281x/pie.hpp>

namespace NDm {
namespace NApp {
namespace NF281x {

/**
 * SCI从模块驱动
 * 可实现串口驱动型功能
 */
class CSciSlaver{
public:
	typedef TRingBuf<unsigned char,unsigned int,unsigned int> CRingBuf;
	typedef CRingBuf::CRingPos CRingPos;

	/**
	 * 	回调函数类型
	 * @note 回调函数的处理时间应该尽量短,否则可能会导致系统阻塞
	 * @param buf 收到的数据缓冲区
	 * @param size 收到的数据长度
	 * @param slaver
	 */
	typedef void (*FUN_RxProc)( const unsigned char* buf,const unsigned int& size,CSciSlaver& slaver );

public:
	CSciSlaver( NDm::NHw::NF281x::CSci& sci  );

	/**
	 * 设置接收处理函数
	 * @param proc
	 */
	void setRxFun( FUN_RxProc proc );

	/**
	 * 复位
	 * 复位发送队列，接收队列
	 */
	void reset();

	bool isError()const;

	inline NDm::NHw::NF281x::CSci& sci(){
		return m_sci;
	}

	inline const NDm::NHw::NF281x::CSci& sci()const{
		return m_sci;
	}

	inline void setTxBuf( unsigned char* buf,const unsigned int& size ){
		m_tx.setBuf(buf,size);
	}

	int send( const unsigned char* buf,const int& size );
	int send( const char* str );

	inline bool ifTxEmpty()const{
		return m_tx.getPos()==m_txPos;
	}

	/**
	 * 中断处理函数
	 * 这些函数应该在中断函数中被调用执行
	 * 用户程序应该执行PIE级别的ACK操作
	 */
	void irsRx();
	void irsTx();

	inline NDm::NHw::NF281x::CPie& pie(){
		return m_pie;
	}
protected:
	/**
	 * 启动发送过程
	 */
	void startTx();
private:
	unsigned char m_rx[16];
	unsigned int m_rxSize;

	CRingBuf m_tx;
	CRingPos m_txPos;

	NDm::NHw::NF281x::CSci& m_sci;

	NDm::NHw::NF281x::CPie& m_pie;

	FUN_RxProc m_rxProc;
};
}
}
}

#endif /* SCISLAVER_HPP_ */

/*
 * scimaster.hpp
 *
 *  Created on: 2016年3月2日
 *      Author: dylan
 */

#ifndef SCIMASTER_HPP_
#define SCIMASTER_HPP_

#include <dm/ringbuf.hpp>
#include <dm/hw/dsp281x/sci.hpp>
#include <dm/app/dsp281x/piectl.hpp>

namespace NDm {
namespace NApp {
namespace NF281x {

/**
 * SCI主模块驱动
 * 由作为主动站的程序调用本类接口。
 */
class CSciMaster {
public:
	typedef TRingBuf<unsigned char,unsigned int,unsigned int> CRingBuf;
	typedef CRingBuf::CRingPos CRingPos;
public:
	CSciMaster( NDm::NHw::NF281x::CSci& sci  );

	void reset();
	bool isError()const;

	inline NDm::NHw::NF281x::CSci& sci(){
		return m_sci;
	}

	inline const NDm::NHw::NF281x::CSci& sci()const{
		return m_sci;
	}

	inline void setTxBuf( unsigned char* buf,const unsigned int& size ){
		m_tx.setBuf(buf,size);
	}

	inline void setRxBuf( unsigned char* buf,const unsigned int& size ){
		m_rx.setBuf(buf,size);
	}

	inline bool ifTxEmpty()const{
		return m_tx.getPos()==m_txPos;
	}

	inline bool ifRxEmpty()const{
		return m_rx.getPos()==m_rxPos;
	}

	int send( const unsigned char* buf,const int& size );

	int recv( unsigned char* buf,const int& size );

	/**
	 * 中断处理函数
	 * 这些函数应该在中断函数中被调用执行
	 * 用户程序应该执行PIE级别的ACK操作
	 */
	void irsRx();
	void irsTx();

	inline NDm::NHw::NF281x::CPie& pie(){
		return m_pie;
	}

protected:
	/**
	 * 启动发送过程
	 */
	void startTx();

private:
	CRingBuf m_rx;
	CRingPos m_rxPos;
	CRingBuf m_tx;
	CRingPos m_txPos;

	NDm::NHw::NF281x::CSci& m_sci;

	NDm::NApp::NF281x::CPieCtl& m_pie;
};

} /* namespace NF281x */
} /* namespace NApp */
} /* namespace NDm */

#endif /* SCIMASTER_HPP_ */

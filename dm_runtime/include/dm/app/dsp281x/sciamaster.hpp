/*
 * sciamaster.hpp
 *
 *  Created on: 2016��3��3��
 *      Author: dylan
 */

#ifndef SCIAMASTER_HPP_
#define SCIAMASTER_HPP_

#include <dm/app/dsp281x/scimaster.hpp>

namespace NDm {
namespace NApp {
namespace NF281x {

class CSciAMaster:public CSciMaster{
	CSciAMaster();
public:
	static CSciAMaster& ins();
};

}
}
}

#endif /* SCIAMASTER_HPP_ */

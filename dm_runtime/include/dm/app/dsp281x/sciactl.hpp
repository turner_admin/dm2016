/*
 * sciactl.hpp
 *
 *  Created on: 2016年3月3日
 *      Author: dylan
 */

#ifndef SCIACTL_HPP_
#define SCIACTL_HPP_

#include <dm/hw/dsp281x/scia.hpp>

namespace NDm {
namespace NApp {
namespace NF281x {

/**
 * 基于FIFO模式的应用
 * 用户需要自行设置通讯速率等参数
 */
class CSciaCtl:public NDm::NHw::NF281x::CScia{
	CSciaCtl();
public:
	static CSciaCtl& ins();
};

}
}
}

#endif /* SCIACTL_HPP_ */

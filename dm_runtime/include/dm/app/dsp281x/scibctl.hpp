/*
 * scibctl.hpp
 *
 *  Created on: 2016年3月3日
 *      Author: dylan
 */

#ifndef SCIBCTL_HPP_
#define SCIBCTL_HPP_

#include <dm/hw/dsp281x/scib.hpp>

namespace NDm {
namespace NApp {
namespace NF281x {

/**
 * 基于FIFO模式的应用
 * 用户需要自行设置通讯速率等参数
 */
class CScibCtl:public NDm::NHw::NF281x::CScib{
	CScibCtl();
public:
	static CScibCtl& ins();
};

}
}
}



#endif /* SCIBCTL_HPP_ */

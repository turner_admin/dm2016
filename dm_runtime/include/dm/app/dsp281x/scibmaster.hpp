/*
 * scibmaster.hpp
 *
 *  Created on: 2016��3��3��
 *      Author: dylan
 */

#ifndef SCIBMASTER_HPP_
#define SCIBMASTER_HPP_

#include <dm/app/dsp281x/scimaster.hpp>

namespace NDm {
namespace NApp {
namespace NF281x {

class CSciBMaster:public CSciMaster{
	CSciBMaster();
public:
	static CSciBMaster& ins();
};

}
}
}



#endif /* SCIBMASTER_HPP_ */

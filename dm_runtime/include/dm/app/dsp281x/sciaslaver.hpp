/*
 * sciaslaver.hpp
 *
 *  Created on: 2016��3��4��
 *      Author: dylan
 */

#ifndef SCIASLAVER_HPP_
#define SCIASLAVER_HPP_


#include <dm/app/dsp281x/scislaver.hpp>

namespace NDm {
namespace NApp {
namespace NF281x {

class CSciASlaver:public CSciSlaver{
	CSciASlaver();
public:
	static CSciASlaver& ins();
};

}
}
}


#endif /* SCIASLAVER_HPP_ */

/*
 * piectl.hpp
 *
 *  Created on: 2016年3月3日
 *      Author: dylan
 */

#ifndef PIECTL_HPP_
#define PIECTL_HPP_

#include <dm/hw/dsp281x/pie.hpp>

namespace NDm {
namespace NApp {
namespace NF281x {

/**
 * PIE控制器类
 * 这个类自动初始化了PIE控制器
 */
class CPieCtl:public NDm::NHw::NF281x::CPie{
	CPieCtl();
public:
	static CPieCtl& ins();
};

}
}
}

#endif /* PIECTL_HPP_ */

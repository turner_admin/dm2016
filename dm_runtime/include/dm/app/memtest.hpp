/*
 * memtest.hpp
 *
 *  Created on: 2016��3��9��
 *      Author: dylan
 */

#ifndef DM_APP_MEMTEST_HPP_
#define DM_APP_MEMTEST_HPP_

namespace NDm{
namespace NApp{

template<typename T>
class CMemTest{
public:
	enum EState{
		SUnstart,
		STesting0,	// Test clear
		STesting1,	// Test Set
		STesting,	// Test Value
		SFail0,
		SFail1,
		SFail,
		SSuccess
	};

	CMemTest( T* start,unsigned long& size ){
		m_start = start;
		m_size = size;
	}

	inline const volatile EState& state()const{
		return m_state;
	}

	inline const volatile unsigned long& cur()const{
		return m_cur;
	}

	inline const unsigned long& size()const{
		return m_size;
	}

	void test( const unsigned long& blockSize=8 ){
		m_cur = 0;

		while( m_cur<m_size && m_state!=SFail ){
			if( !test0(blockSize) )
				break;

			m_cur += blockSize;
		}
	}

	bool test0( const unsigned long& block ){
		unsigned long cur;
		m_state = STesting0;

		for( cur = m_cur;cur<size && cur-m_cur<block ;++cur )
			*(m_start+cur) = (T)0;

		for( cur=m_cur;cur<size&&cur-m_cur<block;++cur ){
			if( *(m_start+cur)!=(T)0 ){
				m_cur = cur;
				m_state = SFail0;
				return false;
			}
		}

		return true;
	}

	bool test1( const unsigned long& block ){
		unsigned long cur;
		m_state = STesting1;

		for( cur = m_cur;cur<size && cur-m_cur<block ;++cur )
			*(m_start+cur) = (~(T)0);

		for( cur=m_cur;cur<size&&cur-m_cur<block;++cur ){
			if( *(m_start+cur)!=(~(T)0) ){
				m_cur = cur;
				m_state = SFail1;
				return false;
			}
		}

		return true;
	}

	bool testx( const unsigned long& block ){
		unsigned long cur;
		m_state = STesting1;

		for( cur = m_cur;cur<size && cur-m_cur<block ;++cur )
			*(m_start+cur) = (~(T)0);

		for( cur=m_cur;cur<size&&cur-m_cur<block;++cur ){
			if( *(m_start+cur)!=(~(T)0) ){
				m_cur = cur;
				m_state = SFail1;
				return false;
			}
		}

		return true;
	}
protected:
	inline T* curP(){
		return m_start+m_cur;
	}

	inline T* data( const unsigned long& i ){
		return m_start+i;
	}

	inline void write0(){
		*curP() = (T)0;
	}

	inline void write1(){
		*curP() = ~((T)0);
	}

	inline void write(){
		*curP() = (T)(m_cur);
	}

	inline bool read0(){
		return *curP()==(T)0;
	}

	inline bool read1(){
		return *curP()==(~((T)0));
	}

	inline bool read(){
		return *curP()==(T)(m_cur);
	}
private:
	T* m_start;
	unsigned long m_size;

	volatile unsigned long m_cur;

	volatile EState m_state;
};
}
}

#endif /* INC_APP_MEMTEST_HPP_ */

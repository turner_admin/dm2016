/*
 * bit_len.hpp
 *
 *  Created on: 2024年3月28日
 *      Author: Dylan.Gao
 */

#ifndef _DM_BIT_INFO_HPP_
#define _DM_BIT_INFO_HPP_

#include <dm/types.hpp>

namespace dm{

/**
 * 未知类型位长为0
 * @tparam T
 * @return
 */
template<typename T>
class TCBitInfo{
public:
	enum{
		Len = 0
	};

	static const T BitsNone = 0;
	static const T BitsAll = 1;
};

template<>
class TCBitInfo<dm::int8>{
public:
	enum{
		Len = 8
	};

	static const dm::int8 BitsNone = 0x00;
	static const dm::int8 BitsAll = 0xFF;
};

template<>
class TCBitInfo<dm::uint8>{
public:
	enum{
		Len = 8
	};

	static const dm::uint8 BitsNone = 0x00;
	static const dm::uint8 BitsAll = 0xFF;
};

template<>
class TCBitInfo<dm::int16>{
public:
	enum{
		Len = 16
	};

	static const dm::int16 BitsNone = 0x0000;
	static const dm::int16 BitsAll = 0xFFFF;
};

template<>
class TCBitInfo<dm::uint16>{
public:
	enum{
		Len = 16
	};

	static const dm::uint16 BitsNone = 0x0000;
	static const dm::uint16 BitsAll = 0xFFFF;
};

template<>
class TCBitInfo<dm::int32>{
public:
	enum{
		Len = 32
	};

	static const dm::int32 BitsNone = 0x00000000;
	static const dm::int32 BitsAll = 0xFFFFFFFF;
};

template<>
class TCBitInfo<dm::uint32>{
public:
	enum{
		Len = 32
	};

	static const dm::uint32 BitsNone = 0x00000000;
	static const dm::uint32 BitsAll = 0xFFFFFFFF;
};

template<>
class TCBitInfo<dm::int64>{
public:
	enum{
		Len = 64
	};

	static const dm::int64 BitsNone = 0x0000000000000000;
	static const dm::int64 BitsAll = 0xFFFFFFFFFFFFFFFF;
};

template<>
class TCBitInfo<dm::uint64>{
public:
	enum{
		Len = 64
	};

	static const dm::uint64 BitsNone = 0x0000000000000000;
	static const dm::uint64 BitsAll = 0xFFFFFFFFFFFFFFFF;
};

}

#endif /* _DM_BIT_LEN_HPP_ */

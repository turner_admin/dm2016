/*
 * rtstatusmgr.hpp
 *
 *  Created on: 2016年12月9日
 *      Author: work
 */

#ifndef _DM_HMI_RTSTATUSMGR_HPP_
#define _DM_HMI_RTSTATUSMGR_HPP_

#include <dm/scada/statusinfo.hpp>
#include <dm/types.hpp>
#include <dm/scada/scadart.hpp>

namespace dm{
namespace hmi{

/**
 * 这个类提供界面访问实时数据的接口
 */
class CRtStatusMgr{
	CRtStatusMgr();
public:
	typedef scada::CScadaRt::status_t status_mgr_t;
	typedef status_mgr_t::rt_t status_t;
	typedef dm::scada::SStatusInfo status_info_t;

	static CRtStatusMgr& ins();

	int getSize()const;

	dm::uint size()const;

	const status_info_t* getInfo( const dm::scada::id_t& id  )const;

	const status_t* getData( const dm::scada::id_t& id )const;
    const status_t* getData( const dm::scada::id_t& device,const dm::scada::index_t& index )const;
};

}
}



#endif /* INCLUDE_DM_HMI_RTSTATUSMGR_HPP_ */

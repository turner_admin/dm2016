/*
 * eventmonitor.hpp
 *
 *  Created on: 2016年12月9日
 *      Author: work
 */

#ifndef INCLUDE_DM_HMI_EVENTMONITOR_HPP_
#define INCLUDE_DM_HMI_EVENTMONITOR_HPP_

#include <dm/scada/event.hpp>

namespace dm{
namespace hmi{

class CEventMonitor{
public:
	CEventMonitor();
	~CEventMonitor();

	typedef dm::scada::CEvent CEvent;

	bool tryGet( CEvent& info,bool& overflow );
	void get( CEvent& info,bool& overflow );

private:
	void* m_mgr;
};

}
}



#endif /* INCLUDE_DM_HMI_EVENTMONITOR_HPP_ */

/*
 * logmonitor.hpp
 *
 *  Created on: 2016年12月9日
 *      Author: work
 */

#ifndef _DM_HMI_LOGMONITOR_HPP_
#define _DM_HMI_LOGMONITOR_HPP_

#include <dm/os/log/loginfo.hpp>

namespace dm{
namespace hmi{

class CLogMonitor{
public:
	CLogMonitor();
	~CLogMonitor();

	typedef dm::os::CLogInfo CLogInfo;

	bool tryGet( CLogInfo& info,bool& overFlag );
	void get( CLogInfo& info,bool& overFlag );

private:
	void* m_mgr;
};

}
}



#endif /* INCLUDE_DM_HMI_LOGMONITOR_HPP_ */

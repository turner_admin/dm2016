/*
 * sysstrlist.hpp
 *
 *  Created on: 2016��4��18��
 *      Author: work
 */

#ifndef DM_UTILITY_SYSSTRLIST_HPP_
#define DM_UTILITY_SYSSTRLIST_HPP_

#include <dm/utility/syslist.hpp>

namespace NDm{
namespace NUtility{

template<int StringLen>
struct SChars{
	char str[StringLen];
};

template<int Size,int StringLen>
class TCSysStrList:public TCSysList<SChars<StringLen>,Size>{
public:
	TCSysStrList( const char* shm,int shmId,const char* lock ):TCSysList<SChars<StringLen>,Size>(shm,shmId,lock){
	}

	int pushStr( const char* str ){
		int pos = -1;
		if( TCSysList<SChars<StringLen>,Size>::m_lock.lock() ){
			if( TCSysList<SChars<StringLen>,Size>::m_data!=NULL ){
				if( TCSysList<SChars<StringLen>,Size>::m_data->pos<Size ){
					pos = TCSysList<SChars<StringLen>,Size>::m_data->pos;
					strncpy(TCSysList<SChars<StringLen>,Size>::m_data->items[pos],str,StringLen);
					++TCSysList<SChars<StringLen>,Size>::m_data->pos;
				}
			}
			TCSysList<SChars<StringLen>,Size>::m_lock.unlock();
		}
		return pos;
	}
};

}
}

#endif /* SYSSTRLIST_HPP_ */

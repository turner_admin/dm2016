/*
 * syslist.hpp
 *
 *  Created on: 2016��4��18��
 *      Author: work
 */

#ifndef DM_UTILITY_SYSLIST_HPP_
#define DM_UTILITY_SYSLIST_HPP_

#include <dm/os_unix/shm.hpp>
#include <dm/os_unix/flock.hpp>

#include <cstring>

namespace NDm{
namespace NUtility{

template<typename T,int Size>
class TCSysList{
protected:
	struct SData{
		volatile int pos;
		T items[Size];
	};

public:
	TCSysList( const char* shm,int shmid,const char* lock ):m_lock(lock){
		m_data = NULL;
		m_lock.lock();
		bool init = false;
		if( !m_shm.open(shm,shmid,sizeof(SData)) ){
			if( m_shm.createOrOpen(shm,shmid,sizeof(SData)) )
				init = true;
		}

		m_data = (SData*)m_shm.getMemAddr();
		if( m_data!=NULL && init )
			memset(m_shm.getMemAddr(),0,sizeof(SData));
		m_lock.unlock();
	}

	int push( const T& item ){
		int pos = -1;
		if( m_lock.lock() ){
			if( m_data!=NULL ){
				if( m_data->pos<Size ){
					pos = m_data->pos;
					memcpy(m_data->items+pos,&item,sizeof(item));
					++m_data->pos;
				}
			}
			m_lock.unlock();
		}
		return pos;
	}

	int find( const T& item ){
		int num = getNum();
		for( int i=0;i<num;++i )
			if( get(i)==item )
				return i;
		return -1;
	}

	inline const T& get( int index )const{
		return m_data->items[index];
	}

	T& get( int index ){
		return m_data->items[index];
	}

	int getNum(){
		int num = 0;
		if( m_lock.lock() ){
			if( m_data!=NULL )
				num = m_data->pos;
			m_lock.unlock();
		}
		return num;
	}


	inline int getSize()const{
		return Size;
	}

protected:
	SData* m_data;

	NDm::NOsUnix::CShm m_shm;
	NDm::NOsUnix::CFlock m_lock;
};

}

}

#endif /* SYSLIST_HPP_ */

/*
 * sysringlist.hpp
 *
 *  Created on: 2016��4��18��
 *      Author: work
 */

#ifndef DM_UTILITY_SYSRINGLIST_HPP_
#define DM_UTILITY_SYSRINGLIST_HPP_

#include <dm/os_unix/shm.hpp>
#include <dm/os_unix/flock.hpp>
#include <dm/ringpos.hpp>

#include <cstring>

namespace NDm{
namespace NUtility{

template<typename T,int Size>
class TCSysRingList{
	struct SData{
		volatile NDm::TRingPos<int> pos;
		T items[Size];
	};
public:
	int push( const T& item ){

	}

	inline const T& get( const NDm::TRingPos<int>& pos )const{
		return m_data->items[pos.getPos()];
	}

	NDm::TRingPos getPos(){
		if( m_lock.lockShared() ){
			return m_data->pos;
			m_lock.unlock();
		}else
			return NDm::TRingPos<int>;
	}
protected:
	SData* m_data;

	NDm::NOsUnix::CShm m_shm;
	NDm::NOsUnix::CFlock m_lock;
};

}
}

#endif /* DM_UTILITY_SYSRINGLIST_HPP_ */

/*
 * ringfifo.hpp
 *
 *  Created on: 2017年2月4日
 *      Author: work
 */

#ifndef INCLUDE_DM_RINGFIFO_HPP_
#define INCLUDE_DM_RINGFIFO_HPP_

#include <dm/ringpos.hpp>

namespace dm{

template<typename Tv,typename Ts,Ts Size,typename Tc=Ts >
class CRingFifo{
public:
	typedef CRingPos<Ts,Size,Tc> pos_t;

	CRingFifo():m_rP(),m_wP(){}

	inline Ts getSize()const{
		return Size;
	}

	inline bool isOverFlow()const{
		return !m_wP.isNoMoreThanACycle(m_rP);
	}

	inline void reset(){
		m_rP = m_wP;
	}

	Ts getLen()const;

	inline Ts getSpace()const{
		return getSize() - getLen();
	}

	bool push( const Tv& v );
	bool push( const Tv* buf,const Ts& size );

	bool pop( Tv& v );
	Ts pop( Tv* buf,const Ts& size );

private:
	Tv m_d[Size];
	pos_t m_rP;
	pos_t m_wP;
};

}

#endif /* INCLUDE_DM_RINGFIFO_HPP_ */

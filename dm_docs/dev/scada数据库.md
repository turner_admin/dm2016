SCADA数据库设计。

[TOC]

## 概述

设备信息保存在scada数据库表中。

* 包括设备表(t_device),设备的信号表(t_status,t_discrete,t_measure,t_cumulant,t_remote_ctl,t_parameter,t_action)，

* 设备信号描述表(t_status_desc,t_discrete_desc,t_remote_ctl_desc,t_action_desc)，

* 逻辑设备表(t_logic_device)，逻辑设备信号映射表(t_logic_status,t_logic_discrete,t_logic_measure,t_logic_cumulant,t_logic_remotectl,t_logic_parameter,t_logic_action)。

```plantuml
entity t_device
entity t_status
entity t_status_desc
entity t_discrete
entity t_discrete_desc
entity t_measure
entity t_cumulant
entity t_remote_ctl
entity t_remote_ctl_desc
entity t_parameter
entity t_action
entity t_action_desc

entity t_logic_device
entity t_logic_status
entity t_logic_discrete
entity t_logic_measure
entity t_logic_cumulant
entity t_logic_remotectl
entity t_logic_parameter
entity t_logic_action

t_device ||--o{ t_status
t_device ||--o{ t_discrete
t_device ||--o{ t_measure
t_device ||--o{ t_cumulant
t_device ||--o{ t_remote_ctl
t_device ||--o{ t_parameter
t_device ||--o{ t_action

t_status_desc|o--o{ t_status
t_discrete_desc |o--o{ t_discrete
t_remote_ctl_desc |o--o{ t_remote_ctl
t_action_desc |o--o{ t_action

t_logic_status }|--|| t_logic_device
t_logic_discrete }|--|| t_logic_device
t_logic_measure }|--|| t_logic_device
t_logic_cumulant }|--|| t_logic_device
t_logic_remotectl }|--|| t_logic_device
t_logic_parameter }|--|| t_logic_device
t_logic_action }|--|| t_logic_device

t_status}o--|| t_logic_status
t_discrete }o--|| t_logic_discrete
t_measure }o--|| t_logic_measure
t_cumulant }o--|| t_logic_cumulant
t_remote_ctl }o--|| t_logic_remotectl
t_parameter }o--|| t_logic_parameter
t_action }o--|| t_logic_action
```

## 设备表t_device

```plantuml
entity t_device {
    * id:integer(11) <<generated>>
    * name:varchar(64)
    * no:integer(11)
    descr:varchar(64)
    parent:integer(11)<<FK>>
    comment:varchar(255)
}
```

设备表记录了设备的必要信息。字段说明如下:

| 字段      | 类型  | 说明                     | 举例      |
| ------- | --- | ---------------------- | ------- |
| id      | 整数  | 设备的唯一ID                | 数据库自动产生 |
| name    | 字符串 | 设备名，唯一。系统内用于查找设备使用。    | 'pcs'   |
| no      | 整数  | 序号，系统内用于对设备进行排序。不能重复   | 1       |
| descr   | 字符串 | 描述。设备的描述，一般是现场设备名称，编号等 |         |
| parent  | 整数  | 上级设备id，如果没有，可以为空       |         |
| comment | 字符串 | 备注信息，可以为空。             |         |

## 状态量表t_status及状态描述表t_status_desc

```plantuml
entity t_status {
    * id:integer(11) <<generated>>
    * name:varchar(64)
    * device:integer(11)
    * no:integer(11)
    descr:varchar(64)
    level:integer(5)
    desc_invoff:integer(11)<<FK>>
    desc_off:integer(11)<<FK>>
    desc_on:integer(11)<<FK>>
    desc_invon:integer(11)<<FK>>
    store_flag:integer(1)
    gen_timed_flag:integer(1)
    rpt_timed_flag:integer(1)
    comment:varchar(255)
}

entity t_status_desc {
    * id:integer(11) <<generated>>
    * no:integer(11)
    descr:varchar(64)
    comment:varchar(255)
}

t_status |o--|| t_status_desc
```

设备的状态量信息记录在状态量表中，状态量的描述记录在状态量描述表中。
状态量描述表字段说明如下:

| 字段      | 类型  | 说明         | 举例      |
| ------- | --- | ---------- | ------- |
| id      | 整数  | 设备的唯一ID    | 数据库自动产生 |
| descr   | 字符串 | 描述。状态量值的描述 |         |
| comment | 字符串 | 备注信息，可以为空。 |         |

状态量表字段描述如下：

| 字段             | 类型  | 说明                            | 举例          |
| -------------- | --- | ----------------------------- | ----------- |
| id             | 整数  | 设备的唯一ID                       | 数据库自动产生     |
| name           | 字符串 | 信号名，唯一。系统内用于查找设备使用。一般是设备名+信号名 | 'pcs.state' |
| device         | 整数  | 所属设备ID                        |             |
| no             | 整数  | 序号，系统内用于排序。设备范围内不能重复          | 1           |
| descr          | 字符串 | 描述。状态量的描述，一般是现场设备名称，编号等       | '状态‘        |
| level          | 整数  | 状态量告警级别                       | 1           |
| desc_invoff    | 整数  | 状态描述无效分的ID                    |             |
| desc_off       | 整数  | 状态描述分的ID                      |             |
| desc_on        | 整数  | 状态描述合的ID                      |             |
| desc_invon     | 整数  | 状态描述无效合的ID                    |             |
| store_flag     | 整数  | 允许数据存盘标志。1表示允许存盘              |             |
| gen_timed_flag | 整数  | 产生时标数据标志。1表示产生                |             |
| rpt_timed_flag | 整数  | 上报时标数据标志。1表示上报                |             |
| comment        | 字符串 | 备注信息，可以为空。                    |             |

## 离散量表t_discrete及离散量述表t_discrete_desc

```plantuml
entity t_discrete {
    * id:integer(11) <<generated>>
    * name:varchar(64)
    * device:integer(11)
    * no:integer(11)
    descr:varchar(64)
    desc_start:integer(11)<<FK>>
    desc_num:integer(11)
    store_flag:integer(1)
    gen_timed_flag:integer(1)
    rpt_timed_flag:integer(1)
    comment:varchar(255)
}

entity t_discrete_desc {
    * id:integer(11) <<generated>>
    * no:integer(11)
    descr:varchar(64)
    comment:varchar(255)
}

t_discrete |o--|| t_discrete_desc
```

设备的离散量信息记录在离散量表中，离散量的描述记录在离散量描述表中。
离散量描述表字段说明如下:

| 字段      | 类型  | 说明         | 举例      |
| ------- | --- | ---------- | ------- |
| id      | 整数  | 设备的唯一ID    | 数据库自动产生 |
| descr   | 字符串 | 描述。状态量值的描述 |         |
| comment | 字符串 | 备注信息，可以为空。 |         |

离散量表字段描述如下：

| 字段             | 类型  | 说明                            | 举例          |
| -------------- | --- | ----------------------------- | ----------- |
| id             | 整数  | 设备的唯一ID                       | 数据库自动产生     |
| name           | 字符串 | 信号名，唯一。系统内用于查找设备使用。一般是设备名+信号名 | 'pcs.state' |
| device         | 整数  | 所属设备ID                        |             |
| no             | 整数  | 序号，系统内用于排序。设备范围内不能重复          | 1           |
| descr          | 字符串 | 描述。状态量的描述，一般是现场设备名称，编号等       | '状态‘        |
| desc_start     | 整数  | 状态描述起始ID                      |             |
| desc_num       | 整数  | 状态描个数。从起始描述之后的num个描述都属于本离散量状态 |             |
| store_flag     | 整数  | 允许数据存盘标志。1表示允许存盘              |             |
| gen_timed_flag | 整数  | 产生时标数据标志。1表示产生                |             |
| rpt_timed_flag | 整数  | 上报时标数据标志。1表示上报                |             |
| comment        | 字符串 | 备注信息，可以为空。                    |             |

## 测量量表t_measure

```plantuml
entity t_measure {
    * id:integer(11) <<generated>>
    * name:varchar(64)
    * device:integer(11)
    * no:integer(11)
    descr:varchar(64)
    unit:varchar(64)
    value_base:float
    value_coef:float
    delta:float
    store_flag:integer(1)
    gen_timed_flag:integer(1)
    rpt_timed_flag:integer(1)
    comment:varchar(255)
}

```

测量量表字段描述如下：

| 字段             | 类型  | 说明                                            | 举例       |
| -------------- | --- | --------------------------------------------- | -------- |
| id             | 整数  | 设备的唯一ID                                       | 数据库自动产生  |
| name           | 字符串 | 信号名，唯一。系统内用于查找设备使用。一般是设备名+信号名                 | 'pcs.Ia' |
| device         | 整数  | 所属设备ID                                        |          |
| no             | 整数  | 序号，系统内用于排序。设备范围内不能重复                          | 1        |
| descr          | 字符串 | 描述。测量量的描述，一般是现场设备名称，编号等                       | 'A相电流‘   |
| unit           | 字符串 | 单位描述                                          |          |
| value_base     | 浮点数 | 工程变换基数。默认为0.0。工程值=(原始值+value_base)xvalue_coef |          |
| value_coef     | 浮点数 | 工程变换系数。默认为1.0                                 |          |
| delta          | 浮点数 | 变换限值                                          |          |
| store_flag     | 整数  | 允许数据存盘标志。1表示允许存盘                              |          |
| gen_timed_flag | 整数  | 产生时标数据标志。1表示产生                                |          |
| rpt_timed_flag | 整数  | 上报时标数据标志。1表示上报                                |          |
| comment        | 字符串 | 备注信息，可以为空。                                    |          |

## 累计量表t_cumulant

```plantuml
entity t_cumulant {
    * id:integer(11) <<generated>>
    * name:varchar(64)
    * device:integer(11)
    * no:integer(11)
    descr:varchar(64)
    max:bigint(20)
    delta:integer(11)
    coef:float()
    store_flag:integer(1)
    gen_timed_flag:integer(1)
    rpt_timed_flag:integer(1)
    comment:varchar(255)
}

```

累计量表字段描述如下：

| 字段             | 类型  | 说明                            | 举例        |
| -------------- | --- | ----------------------------- | --------- |
| id             | 整数  | 设备的唯一ID                       | 数据库自动产生   |
| name           | 字符串 | 信号名，唯一。系统内用于查找设备使用。一般是设备名+信号名 | 'pcs.Kwh' |
| device         | 整数  | 所属设备ID                        |           |
| no             | 整数  | 序号，系统内用于排序。设备范围内不能重复          | 1         |
| descr          | 字符串 | 描述。累计量的描述，一般是现场设备名称，编号等       | '电量‘      |
| max            | 长整形 | 满码值                           |           |
| coef           | 浮点数 | 工程变换系数。默认为1.0                 |           |
| delta          | 整形  | 变换限值                          |           |
| store_flag     | 整数  | 允许数据存盘标志。1表示允许存盘              |           |
| gen_timed_flag | 整数  | 产生时标数据标志。1表示产生                |           |
| rpt_timed_flag | 整数  | 上报时标数据标志。1表示上报                |           |
| comment        | 字符串 | 备注信息，可以为空。                    |           |

## 远控表t_remote_ctl及远控描述表t_remote_ctl_desc

```plantuml
entity t_remote_ctl {
    * id:integer(11) <<generated>>
    * name:varchar(64)
    * device:integer(11)
    * no:integer(11)
    descr:varchar(64)
    desc_preopen:integer(11)<<FK>>
    desc_open:integer(11)<<FK>>
    desc_close:integer(11)<<FK>>
    desc_preclose:integer(11)<<FK>>
    comment:varchar(255)
}

entity t_remote_ctl_desc {
    * id:integer(11) <<generated>>
    * no:integer(11)
    descr:varchar(64)
    comment:varchar(255)
}

t_remote_ctl |o--|| t_remote_ctl_desc
```

设备的远控信息记录在远控表中，远控的描述记录在远控描述表中。
远控描述表字段说明如下:

| 字段      | 类型  | 说明         | 举例      |
| ------- | --- | ---------- | ------- |
| id      | 整数  | 设备的唯一ID    | 数据库自动产生 |
| descr   | 字符串 | 描述。值的描述    |         |
| no      | 整数  | 序号。系统内排序   |         |
| comment | 字符串 | 备注信息，可以为空。 |         |

远控表字段描述如下：

| 字段            | 类型  | 说明                            | 举例        |
| ------------- | --- | ----------------------------- | --------- |
| id            | 整数  | 设备的唯一ID                       | 数据库自动产生   |
| name          | 字符串 | 信号名，唯一。系统内用于查找设备使用。一般是设备名+信号名 | 'pcs.ctl' |
| device        | 整数  | 所属设备ID                        |           |
| no            | 整数  | 序号，系统内用于排序。设备范围内不能重复          | 1         |
| descr         | 字符串 | 描述。远控的描述，一般是现场设备名称，编号等        | '控制‘      |
| desc_preopen  | 整数  | 远控描述预控分(预置)的ID                |           |
| desc_open     | 整数  | 远控描述控分的ID                     |           |
| desc_close    | 整数  | 远控描述控合的ID                     |           |
| desc_preclose | 整数  | 远控描述预控合(复位)的ID                |           |
| comment       | 字符串 | 备注信息，可以为空。                    |           |

## 参数表t_parameter

```plantuml
entity t_parameter {
    * id:integer(11) <<generated>>
    * name:varchar(64)
    * device:integer(11)
    * no:integer(11)
    descr:varchar(64)
    comment:varchar(255)
}

```

参数表字段描述如下：

| 字段      | 类型  | 说明                            | 举例        |
| ------- | --- | ----------------------------- | --------- |
| id      | 整数  | 设备的唯一ID                       | 数据库自动产生   |
| name    | 字符串 | 信号名，唯一。系统内用于查找设备使用。一般是设备名+信号名 | 'pcs.Kwh' |
| device  | 整数  | 所属设备ID                        |           |
| no      | 整数  | 序号，系统内用于排序。设备范围内不能重复          | 1         |
| descr   | 字符串 | 描述。参数的描述，一般是现场设备名称，编号等        | '电量‘      |
| comment | 字符串 | 备注信息，可以为空。                    |           |

> 参数表并不约定参数的具体类型，有应用程序自身来处理。读取参数和设置参数的消息中指定消息的类型。

## 动作表t_action及动作描述表t_action_desc

```plantuml
entity t_action {
    * id:integer(11) <<generated>>
    * name:varchar(64)
    * device:integer(11)
    * no:integer(11)
    descr:varchar(64)
    level:integer(5)
    desc_invoff:integer(11)<<FK>>
    desc_off:integer(11)<<FK>>
    desc_on:integer(11)<<FK>>
    desc_invon:integer(11)<<FK>>
    store_flag:integer(1)
    rpt_timed_flag:integer(1)
    comment:varchar(255)
}

entity t_action_desc {
    * id:integer(11) <<generated>>
    * no:integer(11)
    descr:varchar(64)
    comment:varchar(255)
}

t_action |o--|| t_action_desc
```

设备的动作信息记录在动作表中，动作的描述记录在动作描述表中。
动作描述表字段说明如下:

| 字段      | 类型  | 说明         | 举例      |
| ------- | --- | ---------- | ------- |
| id      | 整数  | 设备的唯一ID    | 数据库自动产生 |
| descr   | 字符串 | 描述。状态量值的描述 |         |
| comment | 字符串 | 备注信息，可以为空。 |         |

动作表字段描述如下：

| 字段             | 类型  | 说明                            | 举例          |
| -------------- | --- | ----------------------------- | ----------- |
| id             | 整数  | 设备的唯一ID                       | 数据库自动产生     |
| name           | 字符串 | 信号名，唯一。系统内用于查找设备使用。一般是设备名+信号名 | 'pcs.state' |
| device         | 整数  | 所属设备ID                        |             |
| no             | 整数  | 序号，系统内用于排序。设备范围内不能重复          | 1           |
| descr          | 字符串 | 描述。状态量的描述，一般是现场设备名称，编号等       | '状态‘        |
| level          | 整数  | 状态量告警级别                       | 1           |
| desc_invoff    | 整数  | 状态描述无效分的ID                    |             |
| desc_off       | 整数  | 状态描述分的ID                      |             |
| desc_on        | 整数  | 状态描述合的ID                      |             |
| desc_invon     | 整数  | 状态描述无效合的ID                    |             |
| store_flag     | 整数  | 允许数据存盘标志。1表示允许存盘              |             |
| rpt_timed_flag | 整数  | 上报时标数据标志。1表示上报                |             |
| comment        | 字符串 | 备注信息，可以为空。                    |             |

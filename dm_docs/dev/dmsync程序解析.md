[TOC]

----



# 概述

dmsync主要包含4部分内容：主函数，帧，采集站规约和转发站规约。

主函数实现了对命令行的解析，调用任务管理模块，创建规约控制器，创建通信设备，创建规约实例，运行规约循环。

帧实现了同步通信规约报文的解析和编码工作。

采集站规约实现了作为采集端规约，实现命令下发和数据采集功能。

转发站诡异实现了作为转发端规约，实现数据上报，命令接收执行的功能。

# 主函数

## 头文件的包含

头文件包含主要是做一个约定习惯，统一风格。

* 未使用的头文件，不应该包含。避免编译依赖无序，消耗编译资源。

* 标准c/c++头文件使用不含.h结尾的。使用命名空间std引用。

* 内部头文件放到inc目录下，并配置编译环境，包含该目录。

* 头文件不使用using namespace，避免名字污染。

## main函数原型

使用标准C++风格，传递命令行参数，返回整形值

```cpp
int main( int argc,char* argv[] )
```

## 命令行解析

命令行解析使用了boost::program_options模块。

### 包含头文件

```cpp
#include <boost/program_options.hpp>
using namespace boost::program_options;
```

### 定义参数项

类options_description可以用于定义参数项描述。options_description具有一个描述，可以通过构造函数传入字符串。在输出的时候作为参数主体描述输出。

以下定义了相关参数项描述。

desc是工具描述。opts_generic是通用的选项描述。

```cpp
options_description desc("【DM2016帮助文档】\n DM同步工具。\n\t基于DM2016系统构建\n支持以下选项");
options_description opts_generic("一般选项");
```

options_description.add_options()方法可以定义多个参数选项及相关参数。每组参数有一对括号来定义：包括了选项提示符，选项值定义，选项描述。

以下在通用选项描述中定义参数：

```cpp
opts_generic.add_options()
            ("help,h","显示本帮助信息")
            ("version,v","显示版本及编译信息")
            ("test","测试模式运行")
            ;
```

其中其中定义了三个选项。比如：("help,h","显示本帮助信息")：选项提示符为--help或者-h，未定义值，选项描述是"显示本帮助信息"

参数项描述具有层级关系，通过options_description.add()函数来添加下级描述项。

```cpp
desc.add(opts_generic);
```

以下定义了命令选项

```cpp
    options_description opts_cmd("命令选项");
    opts_cmd.add_options()
                    ("master,m","做为主站运行。否则作为子站")
                    ("server,s","作为服务端。否则作为客户端")
                    ("ip",value<string>(),"ip")
                    ("port,p",value<int>()->default_value(12404),"端口")
                    ("serial",value<string>(),"串口通信：/dev/ttyS1:115200:8:1:0:0")
                    ("refresh,r",value<int>()->default_value(500),"刷新间隔：毫秒")
                    ("rx-timeout",value<int>()->default_value(30),"接收数据超时时间: 秒")
                    ("wait",value<int>()->default_value(10),"接收等待超时时间: 秒")
                    ("con-max",value<unsigned int>()->default_value(0),"允许的最大连接次数。默认无限制")
                    ("clock",value<int>()->default_value(0),"对时间隔: 秒. 0表示不对时")
                    ("offset-s",value<int>()->default_value(0),"状态量偏移")
                    ("offset-d",value<int>()->default_value(0),"离散量偏移")
                    ("offset-m",value<int>()->default_value(0),"测量量偏移")
                    ("offset-c",value<int>()->default_value(0),"累计量偏移")
                    ("offset-r",value<int>()->default_value(0),"远控偏移")
                    ("offset-p",value<int>()->default_value(0),"参数偏移")
                    ("offset-a",value<int>()->default_value(0),"动作偏移")
                    ("size-s",value<int>()->default_value(-1),"状态量数量。-1使用系统配置")
                    ("size-d",value<int>()->default_value(-1),"离散量数量。-1使用系统配置")
                    ("size-m",value<int>()->default_value(-1),"测量量数量。-1使用系统配置")
                    ("size-c",value<int>()->default_value(-1),"累计量数量。-1使用系统配置")
                    ("size-r",value<int>()->default_value(-1),"远控数量。-1使用系统配置")
                    ("size-p",value<int>()->default_value(-1),"参数数量。-1使用系统配置")
                    ("size-a",value<int>()->default_value(-1),"动作数量。-1使用系统配置")
                    ;
    desc.add(opts_cmd);
```

其中：

* ("ip",value<string>(),"ip"): 选项--ip需要一个string类型的值

* ("port,p",value<int>()->default_value(12404),"端口")：选项--port或者-p需要一个int类型值，不指定时使用默认值12404.

作为主站的选项描述：

```cpp
    options_description opts_master("作为主站的附加选项选项");
    opts_master.add_options()
        ("device,d",value<string>(),"设备名")
        ("call-interval,c",value<int>()->default_value(60),"召唤间隔（秒）")
        ("tx-interval",value<int>()->default_value(0),"发送间隔（毫秒）")
        ("acpt-clk","接受子站对时")
        ;

    desc.add(opts_master);
```

作为子站的选项描述:

```cpp
    options_description opts_slaver("作为子站的附加选项选项");
    opts_slaver.add_options()
        ("logicdevice,l",value<string>(),"逻辑设备名.为设置则使用全设备")
        ("dny-clk","不接受主站对时")
        ;

    desc.add(opts_slaver);
```

### 命令行的解析

使用以下代码对命令行进行解析

```cpp
    variables_map vm;
    try{
        store(parse_command_line(argc,argv,desc),vm);
        notify(vm);
    }catch( std::exception& ex ){
        cout << ex.what()<<endl;
        return 1;
    }
```

如果命令行参数输入有和定义不相符的，以上代码会抛出异常，并退出执行。

### 选项的处理

解析完成后，选项的值会被包含到映射变量variables_map中。

函数variables_map.count()可以用来判断命令行中包含多少个指定选项。选项名使用长描述方式指定。使用短描述的也会自动匹配。

```cpp
    if( vm.count("version") ){
        cout <<"V1.0 构建于 "<<__DATE__<<endl;
        return 1;
    }

      if( vm.count("help") ){
        cout <<desc<<endl;
        return 1;
    }
```

以上

* 如果命令行中包含version，则输出构建日期并退出。
* 如果命令行中包含help，则输出描述并退出。

## 任务模块管理

将执行进程添加到DM2016的任务管理模块中的方法很简单，只需要加入CTaskAgent对象即可。

向CTaskAgent类的构造函数传入命令行，这些信息可以作为任务的唯一标识。

实际使用时，为了测试方便，我们使用参数test来指定是否加入任务管理中控制。

```cpp
dm::os::sys::CTaskAgent* taskAgent = NULL;

if( !vm.count("test") )
    taskAgent = new dm::os::sys::CTaskAgent(argc,argv);
```

进入任务循环过程时，需要调用CTaskAgent::runOnce()来刷新任务状态。

## 创建规约控制器

```cpp
    com::ios_t ios;

    protocol::CProtocolControlor* controlor = new protocol::CProtocolControlor(ios);
    protocol::CProtocolControlor::protocol_t* protocol;
```

规约框架使用了异步io服务。在使用之前，必须先创建io服务对象.com::ios_t。

创建将来使用的规约对象指针，方便后面根据参数真实赋值和调度方便。

## 创建规约对象

### 创建主站规约对象

如果命令行参数指定了以主站规约模式运行，则创建主站规约，并进行初始化。

```cpp
    if( vm.count("master") ){
        CMasterDm* protocolDm = new CMasterDm;
        if( vm.count("device") ){
            if( !protocolDm->setDevice(vm["device"].as<string>().c_str()) ){
                cout <<"不存在设备"<<vm["device"].as<string>()<<endl;
                return -1;
            }
        }
          ...

        protocolDm->setCallInterval(vm["call-interval"].as<int>());
        protocolDm->setWaitTimeout( vm["wait"].as<int>());
        protocolDm->setSendInterval( vm["tx-interval"].as<int>());
        cout <<"设置发送间隔 "<<vm["tx-interval"].as<int>()<<endl;

        protocolDm->setAcceptClock( vm.count("acpt-clk"));

        if( vm.count("clock")  )
            protocolDm->setClockInterval( vm["clock"].as<int>());

        protocol = protocolDm;
    }
```

主站规约的具体参数，在后面再介绍。
规约的通用参数有一些在这里介绍：

* setCallInterval()：设置总之间隔

* setWaitTimeout():设置等待应答超时

* setSendInterval():设置发送间隔

* setAcceptClock():设置是否接收子站对时

* setClockInterval():设置对时间隔

### 创建子站规约对象

子站规约对象的创建和主站规约对象的创建类似：

```cpp
        CSlaverDm* protocolDm = new CSlaverDm;
        if( vm.count("logicdevice") ){
            if( !protocolDm->setLogicDeviceByName(vm["logicdevice"].as<string>().c_str()) ){
                cout <<"不存在逻辑设备"<<vm["logicdevice"].as<string>()<<endl;
                return -1;
            }
        }

        if( !protocolDm->setStatusOffset(vm["offset-s"].as<int>()) ){
            cout <<"参数--offset-s无效"<<endl;
            return -1;
        }

        if( !protocolDm->setStatusSize( vm["size-s"].as<int>()) ){
            cout <<"参数--size-s无效"<<endl;
            return -1;
        }

        if( !protocolDm->setDiscreteOffset(vm["offset-d"].as<int>()) ){
            cout <<"参数--offset-d无效"<<endl;
            return -1;
        }

        if( !protocolDm->setDiscreteSize(vm["size-d"].as<int>()) ){
            cout <<"参数--size-d无效"<<endl;
            return -1;
        }

        if( !protocolDm->setMeasureOffset(vm["offset-m"].as<int>())){
            cout <<"参数--offset-m无效"<<endl;
            return -1;
        }

        if( !protocolDm->setMeasureSize(vm["size-m"].as<int>()) ){
            cout <<"参数--size-m无效"<<endl;
            return -1;
        }

        if( !protocolDm->setCumulantOffset(vm["offset-c"].as<int>()) ){
            cout <<"参数--offset-c无效"<<endl;
            return -1;
        }

        if( !protocolDm->setCumulantSize(vm["size-c"].as<int>()) ){
            cout <<"参数--size-c无效"<<endl;
            return -1;
        }

        if( !protocolDm->setRmtctlOffset(vm["offset-r"].as<int>()) ){
            cout <<"参数--offset-r无效"<<endl;
            return -1;
        }

        if( !protocolDm->setRmtctlSize(vm["size-r"].as<int>()) ){
            cout <<"参数--size-r无效"<<endl;
            return -1;
        }

        if( !protocolDm->setParameterOffset(vm["offset-p"].as<int>()) ){
            cout <<"参数--offset-p无效"<<endl;
            return -1;
        }

        if( !protocolDm->setParameterSize(vm["size-p"].as<int>()) ){
            cout <<"参数--size-p无效"<<endl;
            return -1;
        }

        if( !protocolDm->setActionOffset(vm["offset-a"].as<int>()) ){
            cout <<"参数--offset-a无效"<<endl;
            return -1;
        }

        if( !protocolDm->setActionSize(vm["size-a"].as<int>()) ){
            cout <<"参数--size-a无效"<<endl;
            return -1;
        }

        protocolDm->setWaitTimeout( vm["wait"].as<int>());
        protocolDm->setAcceptClock(!vm.count("dny-ckl"));
        if( vm.count("clock")  )
            protocolDm->setClockInterval( vm["clock"].as<int>());

        protocol = protocolDm;
```

## 创建通信设备

通信设备支持tcp服务端模式，串口模式，和tcp客户端模式。通信设备和规约类型是相互独立的。

### tcp服务端模式

```cpp
if( vm.count("server") ){
        // 运行在服务端模式
        protocol::CProtocolListenor* listner = new protocol::CProtocolListenor;
        ascom::CTcpListner* port = new ascom::CTcpListner(ios);

        com::CTcpServerAddr address;
        if( vm.count("ip") )
            address.setLocalHost(vm["ip"].as<string>().c_str());
        else
            address.setLocalHost("0.0.0.0");

        address.setLocalPort(vm["port"].as<int>());

        port->setAddress(address);

        listner->setListenor( port );

        controlor->setProtocol(protocol );

        listner->addProtocolControlor(controlor);
        listner->startListen();
    }
```

服务端模式需要创建一个监听器类型。并将规约控制器传递给监听器。

监听器作为主调度对象。

### 串口模式

```cpp
else if(vm.count("serial")){
        // 运行在串口模式
        com::CSerialAddr address;
        if( !address.fromString(vm["serial"].as<string>().c_str()) ){
            cout <<"请使用--serial设置正确的串口地址"<<endl;
            return 1;
        }

        ascom::CSerialDevice* device = new ascom::CSerialDevice(ios);
        device->setAddress(address);

        controlor->setProtocol(protocol );
        controlor->setDevice(device);
        controlor->setAutoConnect();
        controlor->openDevice();
    }
```

创建串口后，将串口设置给规约控制器使用即可。

### TCP客户端模式

```cpp
        // 运行在客户端模式
        com::CTcpClientAddr address;

        if( vm.count("ip") )
            address.setServerHost(vm["ip"].as<string>().c_str());
        else{
            cout <<"请指定主机IP"<<endl;
            return 0;
        }

        address.setServerPort( vm["port"].as<int>());

        ascom::CTcpClientDevice* device = new ascom::CTcpClientDevice(ios);
        device->setAddress(address);

        controlor->setProtocol(protocol );
        controlor->setDevice(device);
        controlor->setAutoConnect();
        controlor->openDevice();
```

TCP客户端模式和串口模式相似。

## 调度过程

创建和初始化好各对象后，进入调度循环中。

如果使用任务管理，需要周期更新任务。如果未使用任务管理，直接启动异步io服务运行即可。

```cpp
    if( taskAgent ){
        dm::os::com::CTimer timer(ios);
        timer.start_seconds(true,10);
        while( taskAgent->runOnce() ){
            ios.run_one();

            if( controlor->isExiting() )
                break;
        }

        delete taskAgent;
    }else{
        ios.run();
    }
```

# 协议帧

协议帧使用了TCFrameDm框架。

```plantuml
class CFrameDm{
.. 测试帧 ..
+setTest():bool
+getTest():bool
+setTestAck(ack:bool&):bool
+getTestAck(ack:bool&):bool
.. 状态量读取命令 ..
+setRead_status(start:uint32&,count:uint16&):bool
+getRead_status(start:uint32&,count:uint16&):bool
.. 状态量数据处理 ..
+setData_status(start:uint32&,count:uint16&,rts:rt_status_t*):bool
+getData_status(start:uint32&,count:uint16&):bool
+getData_status(i:uint16&,value:CStatus&):bool
+setData_status(start:uint32&,count:uint16&):uint16
+setData_status(idx:uint16&,rt:rt_status_t*):bool
.. 离散量读命令 ..
+setRead_discrete(start:uint32&,count:uint16&):bool
+getRead_discrete(start:uint32&,count:uint16&):bool
.. 离散量数据处理 ..
+setData_discrete(start:uint32&,count:uint16&,rts:rt_discrete_t*):bool
+getData_discrete(start:uint32&,count:uint16&):bool
+getData_discrete(i:uint16&,value:CStatus&):bool
+setData_discrete(start:uint32&,count:uint16&):uint16
+setData_discrete(idx:uint16&,rt:rt_discrete_t*):bool
.. 测量量读命令 ..
+setRead_measure(start:uint32&,count:uint16&):bool
+getRead_measure(start:uint32&,count:uint16&):bool
.. 测量量数据处理 ..
+setData_measure(start:uint32&,count:uint16&,rts:rt_measure_t*):bool
+getData_measure(start:uint32&,count:uint16&):bool
+getData_measure(i:uint16&,value:CMeasure&):bool
+setData_measure(start:uint32&,count:uint16&):uint16
+setData_measure(idx:uint16&,rt:rt_measure_t*):bool
.. 累计量读命令 ..
+setRead_cumulant(start:uint32&,count:uint16&):bool
+getRead_cumulant(start:uint32&,count:uint16&):bool
.. 累计量数据处理 ..
+setData_cumulant(start:uint32&,count:uint16&,rts:rt_cumulant_t*):bool
+getData_cumulant(start:uint32&,count:uint16&):bool
+getData_cumulant(i:uint16&,value:CCumulant&):bool
+setData_cumulant(start:uint32&,count:uint16&):uint16
+setData_cumulant(idx:uint16&,rt:rt_cumulant_t*):bool
.. 事件处理 ..
+startAppend_event():bool
+appendEvent(e:event_t&):bool
+getReport_event(count:uint16&):bool
+getReport_event(i:uint16&,ev:event_t&):void
.. 时标测量量处理 ..
+startAppend_timedMeasure():bool
+appendTimedMeasure(tm:ts_measure_t&):bool
+getReport_timedMeasure(count:uint16&):bool
+getReport_timedMeasure(i:uint16&,ev:ts_measure_t&):void
.. 时标累计量处理 ..
+startAppend_timedCumulant():bool
+appendTimedCumulant(tm:ts_cumulant_t&):bool
+getReport_timedCumulant(count:uint16&):bool
+getReport_timedCumulant(i:uint16&,ev:ts_cumulant_t&):void
.. 消息处理 ..
+setMsg(msg:CMsgInfo&):bool
+getMsg(msg:CMsgInfo&):bool
.. 时钟处理 ..
+setClock(t:s_t):bool
+getClock(t:s_t&):bool
}

enum EFun{
    .. 测试帧 ..
    Fun_Test
    Fun_TestAck,
    .. 状态量同步 ..
    Fun_Read_Status
    Fun_Status
    .. 离散量同步 ..
    Fun_Read_Discrete
    Fun_Discrete
    .. 测量量同步 ..
    Fun_Read_Measure
    Fun_Measure
    .. 累计量同步 ..
    Fun_Read_Cumulant
    Fun_Cumulant
    .. 时标数据同步 ..
    Fun_Report_Event
    Fun_Report_TimedMeasure
    Fun_Report_TimedCumulant
    .. 消息同步 ..
    Fun_Msg
    .. 时钟同步..
    Fun_Clock
    .. 复位系统 ..
    Fun_Reset
    ..文件同步..
    Fun_ReadFile
    Fun_WriteFile
    ..系统命令执行..
    Fun_OsCmd
}

class dm.protocol.TCFrameDm<896>

dm.protocol.TCFrameDm <|-- CFrameDm
EFun --+ CFrameDm
```

功能包括：状态量同步，离散量同步，测量量同步，累计量同步，事件同步，时标数据同步，消息同步，时钟同步，以及系统复位等功能。

> 远程执行命令未实现

# 主站规约实现

主站规约由类CMasterDm实现。实现数据获取，命令下方功能。

CMasterDM继承自dm::os::protocol::CProtocolMaster类。

主站支持设置信号偏移，可以将一个子站的数据分成多分，由不同的主站进程来分区域同步。



# 子站规约实现

子站规约由类CSlaverDm实现。实现数据的转发，命令的接收和转发。

CSlaverDm继承自dm::os::protocol::CProtocolSlaver类。

转发子站规约的功能与主站规约功能向对应。

```plantuml
class dm.CSlaverDm
dm.os.protocol.CProtocolSlaver <|-- CSlaverDm
CSlaverDm *-- dm.scada.CEventMonitor
CSlaverDm *-- dm.scada.CTimedMeasureMonitor
CSlaverDm *-- dm.scdad.CTimedCumulantMonitor
```

时标数据的获取使用了监视器类来实现。

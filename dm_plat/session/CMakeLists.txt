cmake_minimum_required(VERSION 3.10)

cmake_policy(SET CMP0074 NEW)
find_package(Boost REQUIRED COMPONENTS date_time)

include_directories(${Boost_INCLUDE_DIRS})

aux_source_directory(src SRC)

add_library(session SHARED ${SRC}) 

DB_SELECT(session)

target_link_libraries(session dmsys options ${Boost_LIBRARIES})

set_target_properties(session PROPERTIES WIN32_MANIFEST "NO")

install(TARGETS session LIBRARY)
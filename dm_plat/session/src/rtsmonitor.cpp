/*
 * rtsmonitor.cpp
 *
 *  Created on: 2019-1-11
 *      Author: Dylan.Gao
 */

#include <dm/export.hpp>
#define DM_API_SESSION DM_API_EXPORT

#include <dm/session/rtsmonitor.hpp>

namespace dm{
namespace session{

CRtsMonitor::CRtsMonitor():m_deadline(),m_mod(0),m_num(0){
}

bool CRtsMonitor::isIdle( const dm::CTimeStamp& n )const{
	return m_mod<=0 || m_deadline<=n;
}

bool CRtsMonitor::isTimeout( const dm::CTimeStamp& n )const{
	return m_deadline < n;
}

CRts CRtsMonitor::append( CRts::EGroupType type,dm::uint32 size ){
	unsigned char* p = m_data;
	if( m_num>0 )
		p = get(m_num-1).next();

	CRts rt(p);

	if( rt.set(type,size,m_data+Len) ){
		++m_num;
		return rt;
	}

	rt.setP(NULL);

	return rt;
}

CRts CRtsMonitor::get( int i ){
	CRts rt;
	if( i>=m_num )
		return rt;

	unsigned char* p = m_data;
	for( int j=0;j<i;++j ){
		rt.setP(p);
		p = rt.next();
	}

	rt.setP(p);
	return rt;
}

}
}

/*
 * modgen.cpp
 *
 *  Created on: 2019-1-11
 *      Author: work
 */

#include <dm/export.hpp>
#define DM_API_SESSION DM_API_EXPORT

#include <dm/session/modgen.hpp>

#include <dm/timestamp.hpp>
#include <cmath>

namespace dm{
namespace session{

CModGen::CModGen(){
}

CModGen& CModGen::ins(){
	static CModGen i;
	return i;
}

mod_t CModGen::gen(){
	dm::CTimeStamp c = dm::CTimeStamp::cur();
	mod_t rt = std::rand()*360*3600;
	rt += c.seconds();
	rt *= 1000;
	rt += c.mseconds();

	if( rt==0 )
		rt = 1;

	return rt;
}

}
}



/*
 * rts.cpp
 *
 *  Created on: 2019-1-11
 *      Author: work
 */

#include <dm/export.hpp>
#define DM_API_SESSION DM_API_EXPORT

#include <dm/session/rts.hpp>

namespace dm{
namespace session{

static inline unsigned long dataLen( const CRts::EGroupType& t,const dm::uint32& num ){
	switch( t ){
	case CRts::GroupStatus:
		return num * sizeof(CRts::SRtStatus);
	case CRts::GroupDiscrete:
		return num * sizeof(CRts::SRtDiscrete);
	case CRts::GroupMeasure:
		return num * sizeof(CRts::SRtMeasure);
	case CRts::GroupCumulant:
		return num * sizeof( CRts::SRtCumulant );
	default:
		return 0;
	}
}

CRts::CRts( unsigned char* p ){
	m_p = p;
}

CRts::CRts( const CRts& rts ):m_p(rts.m_p){
}

CRts& CRts::operator =( const CRts& rts ){
	m_p = rts.m_p;
	return *this;
}

void CRts::setP( unsigned char* p ){
	m_p = p;
}

bool CRts::set( EGroupType type,dm::uint32 num,const unsigned char* end ){
	if( (dataLen(type,num) + sizeof(SRts) - 1) > (unsigned long)(end - m_p ) )
		return false;
	rts()->type = type;
	rts()->num = num;

	return true;
}

CRts::SRtStatus* CRts::statuses(){
	return (SRtStatus*)(&(rts()->data));
}

CRts::SRtDiscrete* CRts::discretes(){
	return (SRtDiscrete*)(&(rts()->data));
}

CRts::SRtMeasure* CRts::measures(){
	return (SRtMeasure*)(&(rts()->data));
}

CRts::SRtCumulant* CRts::cumulants(){
	return (SRtCumulant*)(&(rts()->data));
}

unsigned char* CRts::next(){
	return m_p+memSize();
}

unsigned long CRts::memSize()const{
	return dataLen(rts()->type,rts()->num) + sizeof(SRts) - 1;
}

}
}

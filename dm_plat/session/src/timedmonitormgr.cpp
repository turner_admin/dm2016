/*
 * timedmonitormgr.cpp
 *
 *  Created on: 2019-1-11
 *      Author: Dylan.Gao
 */

#include <dm/export.hpp>
#define DM_API_SESSION DM_API_EXPORT

#include <dm/session/timedmonitormgr.hpp>
#include <dm/session/modgen.hpp>

#include <dm/os/log/logger.hpp>

static const char* logModule = "CTimedMonitorMgr.session.dm";

namespace dm{
namespace session{

CTimedMonitorMgr::CTimedMonitorMgr():m_lock(){
	log().debug(THISMODULE "Size:%d",Size);
}

CTimedMonitor* CTimedMonitorMgr::getNew( dm::CTimeStamp::s_t aliveSec ){
	msharelock_exclusive_t ml(m_lock);
	dm::CTimeStamp n = dm::CTimeStamp::cur();
	for( int i=0;i<Size;++i ){
		if( m_d[i].isIdle(n) ){
			n.setSeconds( n.seconds() + aliveSec );
			m_d[i].setDeadline(n);
			m_d[i].setMod(CModGen::ins().gen());

			log().debug(THISMODULE "new monitor %X",m_d[i].mod());

			return m_d + i;
		}
	}

	log().warnning(THISMODULE "fail to new");

	return NULL;
}

CTimedMonitor* CTimedMonitorMgr::get( mod_t mod,dm::CTimeStamp::s_t aliveSec ){
	msharelock_exclusive_t ml(m_lock);
	dm::CTimeStamp n = dm::CTimeStamp::cur();
	for( int i=0;i<Size;++i ){
		if( m_d[i].mod()==mod ){
			if( m_d[i].isTimeout(n) ){
				log().warnning(THISMODULE "monitor %X timeout",mod);
				return NULL;
			}else{
				if( aliveSec>0 ){
					n.setSeconds(n.seconds()+aliveSec);
					m_d[i].setDeadline(n);
				}

				return m_d + i;
			}
		}
	}

	log().warnning(THISMODULE "no monitor %X",mod);
	return NULL;
}

void CTimedMonitorMgr::free( mod_t mod ){
	msharelock_exclusive_t ml(m_lock);
	for( int i=0;i<Size;++i ){
		if( m_d[i].mod()==mod ){
			m_d[i].setMod(0);
			return;
		}
	}
}

}
}

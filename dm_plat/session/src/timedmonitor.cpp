/*
 * timedmonitor.hpp
 *
 *  Created on: 2019-1-11
 *      Author: work
 */

#include <dm/export.hpp>
#define DM_API_SESSION DM_API_EXPORT

#include <dm/session/timedmonitor.hpp>

namespace dm{
namespace session{

CTimedMonitor::CTimedMonitor():m_deadline(),m_mod(0),m_type(Unknow),m_pos(){
}

bool CTimedMonitor::isIdle( const dm::CTimeStamp& n )const{
	return m_mod<=0 || m_deadline<=n;
}

bool CTimedMonitor::isTimeout( const dm::CTimeStamp& n )const{
	return m_deadline<n;
}

dm::scada::CEventMonitor::ringpos_t* CTimedMonitor::eventPos(){
	if( m_type==Event )
		return dynamic_cast<dm::scada::CEventMonitor::ringpos_t*>(&m_pos);
	return NULL;
}

dm::scada::CTimedMeasureMonitor::ringpos_t* CTimedMonitor::measurePos(){
	if( m_type==TimedMeasure )
		return dynamic_cast<dm::scada::CTimedMeasureMonitor::ringpos_t*>(&m_pos);
	return NULL;
}

dm::scada::CTimedCumulantMonitor::ringpos_t* CTimedMonitor::cumulantPos(){
	if( m_type==TimedCumulant )
		return dynamic_cast<dm::scada::CTimedCumulantMonitor::ringpos_t*>(&m_pos);
	return NULL;
}

}
}

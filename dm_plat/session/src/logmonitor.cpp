/*
 * logmonitor.cpp
 *
 *  Created on: 2019-1-11
 *      Author: work
 */

#include <dm/export.hpp>
#define DM_API_SESSION DM_API_EXPORT

#include <dm/session/logmonitor.hpp>

namespace dm{
namespace session{

CLogMonitor::CLogMonitor():m_deadline(),m_mod(0),m_pos(),m_pid(0),m_level(dm::os::CLogInfo::Debug){
}

bool CLogMonitor::isIdle( const dm::CTimeStamp& n )const{
	return m_mod<=0 || m_deadline<=n;
}

bool CLogMonitor::isTimeout( const dm::CTimeStamp& n )const{
	return m_deadline<n;
}

}
}

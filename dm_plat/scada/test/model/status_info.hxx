#ifndef _DM_SCADA_MODEL_TEST_STATUS_INFO_HXX_
#define _DM_SCADA_MODEL_TEST_STATUS_INFO_HXX_

#include <cxxtest/TestSuite.h>
#include <dm/scada/model/status_info.hpp>

class TestModelStatusInfo:public CxxTest::TestSuite{
public:
    void testFlag(){
        dm::scada::model::SStatusInfo info;

        info.flags = 0xAA;

        TSM_ASSERT("保存标记",!info.ifSave());
        TSM_ASSERT("产生时标标记",info.ifGenTimed());
        TSM_ASSERT("主动上报标记",!info.ifReport());
        TSM_ASSERT("取反标记",info.ifReverse());

        // 取反验证
        info.setSave();
        info.clrGenTimed();
        info.setReport();
        info.clrReverse();

        TSM_ASSERT("保存标记",info.ifSave());
        TSM_ASSERT("产生时标标记",!info.ifGenTimed());
        TSM_ASSERT("主动上报标记",info.ifReport());
        TSM_ASSERT("取反标记",!info.ifReverse());
    }
};


#endif
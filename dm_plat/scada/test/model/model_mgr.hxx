#ifndef _DM_SCADA_MODEL_TEST_MODEL_MGR_HXX_
#define _DM_SCADA_MODEL_TEST_MODEL_MGR_HXX_

#include <cxxtest/TestSuite.h>
#include <dm/scada/model/model_mgr.hpp>

class TestModelModelMgr:public CxxTest::TestSuite{
public:
    void testDefault(){
        dm::scada::model::CModelMgr& mgr = dm::scada::model::CModelMgr::ins();

        TSM_ASSERT_EQUALS("保存标记",mgr.size(),0);
    }
};


#endif
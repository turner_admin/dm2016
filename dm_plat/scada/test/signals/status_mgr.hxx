/*
 * status_mgr.hxx
 *
 *  Created on: 2024年4月9日
 *      Author: Dylan.Gao
 */

#ifndef _DM_PLAT_SCADA_TEST_SIGNALS_STATUS_MGR_HXX_
#define _DM_PLAT_SCADA_TEST_SIGNALS_STATUS_MGR_HXX_

#include <cxxtest/TestSuite.h>
#include <dm/scada/signals/status_mgr.hpp>

class TestSignalsStatusMgr:public CxxTest::TestSuite{
public:
	void testInit(){
		dm::scada::signals::CStatusMgr::init();
	}
};

#endif /* _DM_PLAT_SCADA_TEST_SIGNALS_STATUS_MGR_HXX_ */

drop table if exists t_logic_action;
drop table if exists t_logic_parameter;
drop table if exists t_logic_remotectl;
drop table if exists t_logic_cumulant;
drop table if exists t_logic_measure;
drop table if exists t_logic_discrete;
drop table if exists t_logic_status;
drop table if exists t_logic_device;

drop table if exists t_action;
drop table if exists t_parameter;
drop table if exists t_remote_ctl;
drop table if exists t_cumulant;
drop table if exists t_measure;
drop table if exists t_discrete;
drop table if exists t_status;

drop table if exists t_action_desc;
drop table if exists t_remote_ctl_desc;
drop table if exists t_discrete_desc;
drop table if exists t_status_desc;

drop table if exists t_device;

CREATE TABLE t_device (id int(11) NOT NULL comment '设备ID', name varchar(64) NOT NULL UNIQUE, no int(11) NOT NULL UNIQUE, descr varchar(64) NOT NULL comment '描述', parent int(11) comment '所属设备', comment varchar(255), PRIMARY KEY (id)) comment='设备';
ALTER TABLE t_device ADD INDEX FKt_device690251 (parent), ADD CONSTRAINT FKt_device690251 FOREIGN KEY (parent) REFERENCES t_device (id);

CREATE TABLE t_status_desc (id int(11) NOT NULL, no int(11) NOT NULL UNIQUE, descr varchar(64) NOT NULL comment '文字描述', comment varchar(255), PRIMARY KEY (id)) comment='状态描述表';
CREATE TABLE t_discrete_desc (id int(11) NOT NULL comment '编号', no int(11) NOT NULL UNIQUE comment '序号', descr varchar(64) NOT NULL comment '描述', comment varchar(255), PRIMARY KEY (id)) comment='离散量描述';
CREATE TABLE t_remote_ctl_desc (id int(11) NOT NULL, no int(11) NOT NULL UNIQUE, descr varchar(64) NOT NULL, comment varchar(255), PRIMARY KEY (id)) comment='远动信号描述';
CREATE TABLE t_action_desc (id int(11) NOT NULL, no int(11) NOT NULL UNIQUE, descr varchar(64) NOT NULL, comment varchar(255), PRIMARY KEY (id));

CREATE TABLE t_status (id int(11) NOT NULL comment '序号', name varchar(64) NOT NULL UNIQUE, device int(11) NOT NULL, no int(11) NOT NULL, descr varchar(64) NOT NULL comment '描述', level int(5) NOT NULL, desc_invoff int(11) comment '无效值描述', desc_off int(11) comment '分状态描述', desc_on int(11), desc_invon int(11), store_flag int(1) comment '是否存盘', gen_timed_flag int(1) comment '是否产生时标数据', rpt_timed_flag int(1) comment '是否主动上报时标数据', comment varchar(255), PRIMARY KEY (id)) comment='遥信';
CREATE TABLE t_discrete (id int(11) NOT NULL comment '设备内编号', name varchar(64) NOT NULL UNIQUE comment '描述', device int(11) NOT NULL comment '所属设备编号', no int(11) NOT NULL, descr varchar(64) NOT NULL comment '描述', desc_start int(11) NOT NULL comment '状态描述起始编号', desc_num int(11) NOT NULL comment '状态描述个数', store_flag int(1) comment '是否存盘', gen_timed_flag int(1) comment '是否产生时标数据', rpt_timed_flag int(1) comment '是否主动上报时标数据', comment varchar(255), PRIMARY KEY (id)) comment='离散量';
CREATE TABLE t_measure (id int(11) NOT NULL, name varchar(64) NOT NULL UNIQUE comment '描述', device int(11) NOT NULL comment '所属设备', no int(11) NOT NULL comment '模拟量编号', descr varchar(64) NOT NULL comment '描述', unit char(64) comment '数值单位', value_base float NOT NULL, value_coef float NOT NULL, delta float NOT NULL, store_flag int(1) comment '是否存盘', gen_timed_flag int(1) comment '是否产生时标数据', rpt_timed_flag int(1) comment '是否主动上报时标数据', comment varchar(255), PRIMARY KEY (id)) comment='测量值信息';
CREATE TABLE t_cumulant (id int(11) NOT NULL, name varchar(64) NOT NULL UNIQUE, device int(11) NOT NULL, no int(11) NOT NULL, descr varchar(64) NOT NULL, max bigint(20) NOT NULL, delta int(11) NOT NULL, coef float NOT NULL, store_flag int(1), gen_timed_flag int(1), rpt_timed_flag int(1), comment varchar(255), PRIMARY KEY (id));
CREATE TABLE t_remote_ctl (id int(11) NOT NULL, name varchar(64) NOT NULL UNIQUE, device int(11) NOT NULL, no int(11) NOT NULL, descr varchar(64) NOT NULL, desc_preopen int(11), desc_preclose int(11), desc_close int(11), desc_open int(11), comment varchar(255), PRIMARY KEY (id)) comment='远控';
CREATE TABLE t_parameter (id int(11) NOT NULL, name varchar(64) NOT NULL UNIQUE, device int(11) NOT NULL, no int(11) NOT NULL, descr varchar(64) NOT NULL, comment varchar(255), PRIMARY KEY (id)) comment='参数';
CREATE TABLE t_action (id int(11) NOT NULL, name varchar(64) NOT NULL UNIQUE, device int(11) NOT NULL, no int(11) NOT NULL, descr varchar(64) NOT NULL, level int(11) NOT NULL, desc_invoff int(11), desc_off int(11), desc_on int(11), desc_invon int(11), store_flag int(1), rpt_timed_flag int(1), comment varchar(255), PRIMARY KEY (id));

CREATE TABLE t_logic_device (id int(11) NOT NULL, name varchar(64) NOT NULL UNIQUE, no int(11) NOT NULL, descr varchar(64) NOT NULL, comment varchar(255), PRIMARY KEY (id));
CREATE TABLE t_logic_status (logic_device int(11) NOT NULL, no int(11) NOT NULL, status int(11) NOT NULL, PRIMARY KEY (logic_device, no));
CREATE TABLE t_logic_discrete (logic_device int(11) NOT NULL, no int(11) NOT NULL, discrete int(11) NOT NULL, PRIMARY KEY (logic_device, no));
CREATE TABLE t_logic_measure (logic_device int(11) NOT NULL, no int(11) NOT NULL, measure int(11) NOT NULL, PRIMARY KEY (logic_device, no));
CREATE TABLE t_logic_cumulant (logic_device int(11) NOT NULL, no int(11) NOT NULL, cumulant int(11) NOT NULL, PRIMARY KEY (logic_device, no));
CREATE TABLE t_logic_remotectl (logic_device int(11) NOT NULL, no int(11) NOT NULL, remote_ctl int(11) NOT NULL, PRIMARY KEY (logic_device, no));
CREATE TABLE t_logic_parameter (logic_device int(11) NOT NULL, no int(11) NOT NULL, parameter int(11) NOT NULL, PRIMARY KEY (logic_device, no));
CREATE TABLE t_logic_action (logic_device int(11) NOT NULL, no int(11) NOT NULL, action int(11) NOT NULL, PRIMARY KEY (logic_device, no));

drop table if exists t_logic_action;
drop table if exists t_logic_parameter;
drop table if exists t_logic_remotectl;
drop table if exists t_logic_cumulant;
drop table if exists t_logic_measure;
drop table if exists t_logic_discrete;
drop table if exists t_logic_status;
drop table if exists t_logic_device;

drop table if exists t_action;
drop table if exists t_parameter;
drop table if exists t_remote_ctl;
drop table if exists t_cumulant;
drop table if exists t_measure;
drop table if exists t_discrete;
drop table if exists t_status;

drop table if exists t_action_desc;
drop table if exists t_remote_ctl_desc;
drop table if exists t_discrete_desc;
drop table if exists t_status_desc;

drop table if exists t_device;

CREATE TABLE t_device (id int4 NOT NULL, name varchar(64) NOT NULL UNIQUE, no int4 NOT NULL UNIQUE, descr varchar(64) NOT NULL, parent int4 NOT NULL, comment varchar(255), PRIMARY KEY (id));
ALTER TABLE t_device ADD CONSTRAINT FKt_device690251 FOREIGN KEY (parent) REFERENCES t_device (id);

CREATE TABLE t_status_desc (id int4 NOT NULL, no int4 NOT NULL UNIQUE, descr varchar(64) NOT NULL, comment varchar(255), PRIMARY KEY (id));
CREATE TABLE t_discrete_desc (id int4 NOT NULL, no int4 NOT NULL UNIQUE, descr varchar(64) NOT NULL, comment varchar(255), PRIMARY KEY (id));
CREATE TABLE t_remote_ctl_desc (id int4 NOT NULL, no int4 NOT NULL UNIQUE, descr varchar(64) NOT NULL, comment varchar(255), PRIMARY KEY (id));
CREATE TABLE t_action_desc (id int4 NOT NULL, no int4 NOT NULL UNIQUE, descr varchar(64) NOT NULL, comment varchar(255), PRIMARY KEY (id));

CREATE TABLE t_status (id int4 NOT NULL, name varchar(64) NOT NULL UNIQUE, device int4 NOT NULL, no int4 NOT NULL, descr varchar(64) NOT NULL, level int4 NOT NULL, desc_invoff int4, desc_off int4, desc_on int4, desc_invon int4, store_flag int4, gen_timed_flag int4, rpt_timed_flag int4, comment varchar(255), PRIMARY KEY (id));
CREATE TABLE t_discrete (id int4 NOT NULL, name varchar(64) NOT NULL UNIQUE, device int4 NOT NULL, no int4 NOT NULL, descr varchar(64) NOT NULL, desc_start int4 NOT NULL, desc_num int4 NOT NULL, store_flag int4, gen_timed_flag int4, rpt_timed_flag int4, comment varchar(255), PRIMARY KEY (id));
CREATE TABLE t_measure (id int4 NOT NULL, name varchar(64) NOT NULL UNIQUE, device int4 NOT NULL, no int4 NOT NULL, descr varchar(64) NOT NULL, unit char(64), value_base float4 NOT NULL, value_coef float4 NOT NULL, delta float4 NOT NULL, store_flag int4, gen_timed_flag int4, rpt_timed_flag int4, comment varchar(255), PRIMARY KEY (id));
CREATE TABLE t_cumulant (id int4 NOT NULL, name varchar(64) NOT NULL UNIQUE, device int4 NOT NULL, no int4 NOT NULL, descr varchar(64) NOT NULL, max int8 NOT NULL, delta int4 NOT NULL, coef float4 NOT NULL, store_flag int4, gen_timed_flag int4, rpt_timed_flag int4, comment varchar(255), PRIMARY KEY (id));
CREATE TABLE t_remote_ctl (id int4 NOT NULL, name varchar(64) NOT NULL UNIQUE, device int4 NOT NULL, no int4 NOT NULL, descr varchar(64) NOT NULL, desc_preopen int4, desc_preclose int4, desc_close int4, desc_open int4, comment varchar(255), PRIMARY KEY (id));
CREATE TABLE t_parameter (id int4 NOT NULL, name varchar(64) NOT NULL UNIQUE, device int4 NOT NULL, no int4 NOT NULL, descr varchar(64) NOT NULL, comment varchar(255), PRIMARY KEY (id));
CREATE TABLE t_action (id int4 NOT NULL, name varchar(64) NOT NULL UNIQUE, device int4 NOT NULL, no int4 NOT NULL, descr varchar(64) NOT NULL, level int4 NOT NULL, desc_invoff int4, desc_off int4, desc_on int4, desc_invon int4, store_flag int4, rpt_timed_flag int4, comment varchar(255), PRIMARY KEY (id));

CREATE TABLE t_logic_device (id int4 NOT NULL, name varchar(64) NOT NULL UNIQUE, no int4 NOT NULL, descr varchar(64) NOT NULL, comment varchar(255), PRIMARY KEY (id));
CREATE TABLE t_logic_status (logic_device int4 NOT NULL, no int4 NOT NULL, status int4 NOT NULL, t_statusname varchar(64), PRIMARY KEY (logic_device, no));
CREATE TABLE t_logic_discrete (logic_device int4 NOT NULL, no int4 NOT NULL, discrete int4 NOT NULL, t_discretename varchar(64), PRIMARY KEY (logic_device, no));
CREATE TABLE t_logic_measure (logic_device int4 NOT NULL, no int4 NOT NULL, measure int4 NOT NULL, PRIMARY KEY (logic_device, no));
CREATE TABLE t_logic_cumulant (logic_device int4 NOT NULL, no int4 NOT NULL, cumulant int4 NOT NULL, PRIMARY KEY (logic_device, no));
CREATE TABLE t_logic_remotectl (logic_device int4 NOT NULL, no int4 NOT NULL, remote_ctl int4 NOT NULL, PRIMARY KEY (logic_device, no));
CREATE TABLE t_logic_parameter (logic_device int4 NOT NULL, no int4 NOT NULL, parameter int4 NOT NULL, PRIMARY KEY (logic_device, no));
CREATE TABLE t_logic_action (logic_device int4 NOT NULL, no int4 NOT NULL, action int4 NOT NULL, PRIMARY KEY (logic_device, no));

CREATE TABLE r_status_shadow (name varchar(64) NOT NULL, value int(2) NOT NULL, dts_s int(20) NOT NULL comment '时标秒', dts_ms int(6) NOT NULL, ots timestamp(3) NOT NULL, cause int(11) NOT NULL, PRIMARY KEY (name, dts_s, dts_ms)) comment='状态量记录';
CREATE TABLE r_discrete_shadow (name varchar(64) NOT NULL, value int(11) NOT NULL, dts_s int(20) NOT NULL, dts_ms int(6) NOT NULL comment '数据时标毫秒', ots timestamp(3) NOT NULL, cause int(11) NOT NULL, PRIMARY KEY (name, dts_s, dts_ms));
CREATE TABLE r_measure_shadow (name varchar(64) NOT NULL, value float NOT NULL, dts_s int(20) NOT NULL comment '数据时标', dts_ms int(6) NOT NULL, ots timestamp(3) NOT NULL comment '操作时标', cause int(11) NOT NULL comment '记录原因', PRIMARY KEY (name, dts_s, dts_ms)) comment='测量值记录表';
CREATE TABLE r_cumulant_shadow (name varchar(64) NOT NULL, value float NOT NULL, dts_s int(20) NOT NULL comment '数据时标', dts_ms int(6) NOT NULL, ots timestamp(3) NOT NULL comment '操作时标', cause int(11) NOT NULL comment '记录原因', PRIMARY KEY (name, dts_s, dts_ms)) comment='测量值记录表';
CREATE TABLE r_action_shadow (name varchar(64) NOT NULL, value int(2) NOT NULL, dts_s int(20) NOT NULL, dts_ms int(6) NOT NULL, ots timestamp(3) NOT NULL, cause int(11) NOT NULL, PRIMARY KEY (name, dts_s, dts_ms));

CREATE VIEW v_status as select name,value,dts_s,dts_ms,ots,cause from r_status UNION select name,value,dts_s,dts_ms,ots,cause from r_status_shadow;
CREATE VIEW v_discrete as select name,value,dts_s,dts_ms,ots,cause from r_discrete UNION select name,value,dts_s,dts_ms,ots,cause from r_discrete_shadow;
CREATE VIEW v_measure as select name,value,dts_s,dts_ms,ots,cause from r_measure UNION select name,value,dts_s,dts_ms,ots,cause from r_measure_shadow;
CREATE VIEW v_cumulant as select name,value,dts_s,dts_ms,ots,cause from r_cumulant UNION select name,value,dts_s,dts_ms,ots,cause from r_cumulant_shadow;
CREATE VIEW v_action as select name,value,dts_s,dts_ms,ots,cause from r_action UNION select name,value,dts_s,dts_ms,ots,cause from r_action_shadow;

delete from t_logic_status;
delete from t_logic_discrete;
delete from t_logic_measure;
delete from t_logic_cumulant;
delete from t_logic_parameter;
delete from t_logic_remotectl;
delete from t_logic_action;

delete from t_logic_device;

delete from t_status;
delete from t_discrete;
delete from t_measure;
delete from t_cumulant;
delete from t_parameter;
delete from t_remote_ctl;
delete from t_action;

delete from t_status_desc;
delete from t_discrete_desc;
delete from t_remote_ctl_desc;

select "status desc";
source sqlite_init_statusdesc.sql
select "discrete desc";
source sqlite_init_discretedesc.sql
select "remotectl desc";
source sqlite_init_remotectldesc.sql
select "action desc";
source sqlite_init_actiondesc.sql

select "device";
source sqlite_init_device.sql
select "status";
source sqlite_init_status.sql
select "discrete";
source sqlite_init_discrete.sql
select "measure";
source sqlite_init_measure.sql
select "cumulant";
source sqlite_init_cumulant.sql
select "remotectl";
source sqlite_init_remotectl.sql
select "parameter";
source sqlite_init_parameter.sql
select "action";
source sqlite_init_action.sql

select "logic device";
source sqlite_init_logicdevice.sql
select "logic device";
source sqlite_init_logicstatus.sql
select "logic discrete";
source sqlite_init_logicdiscrete.sql
select "logic measure";
source sqlite_init_logicmeasure.sql
select "logic cumulant";
source sqlite_init_logiccumulant.sql
select "logic parameter";
source sqlite_init_logicparameter.sql
select "logic remotectl";
source sqlite_init_logicremotectl.sql
select "logic action";
source sqlite_init_logicaction.sql


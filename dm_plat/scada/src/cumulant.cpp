﻿/*
 * cumulant.cpp
 *
 *  Created on: 2017年3月27日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_SCADA DM_API_EXPORT

#include <dm/scada/cumulant.hpp>

namespace dm{
namespace scada{

/**
 * 更新值
 * @param c
 * @return 是否更新
 * - true 更新
 * - false 未更新。值相等
 */
bool CCumulant::update( const CCumulant& c ){
	if( m_v==c.m_v )
		return false;
	m_v = c.m_v;
	m_r = c.m_r;

	return true;
}

bool CCumulant::update( const CCumulant& c )volatile{
	if( m_v==c.m_v )
		return false;
	m_v = c.m_v;
	m_r = c.m_r;

	return true;
}

}
}



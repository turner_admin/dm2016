﻿/*
 * logicdevicemgr.cpp
 *
 *  Created on: 2017年12月3日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_SCADA DM_API_EXPORT

#include <dm/scada/logicdevicemgr.hpp>
#include <dm/scada/scadainfo.hpp>

namespace dm{
namespace scada{

CLogicDeviceMgr::CLogicDeviceMgr( CScada& scada ):CScada(scada),
		m_cfg(scada.logicDeviceInfos(),scada.info()->logicDeviceCount()){
}

CLogicDeviceMgr& CLogicDeviceMgr::ins(){
	static CLogicDeviceMgr i(CScada::ins());
	return i;
}

bool CLogicDeviceMgr::setDesc( const index_t& idx,const char* str ){
	SLogicDeviceInfo* p = m_cfg.at(idx);
	if( p ){
		p->desc = str;
		return true;
	}else
		return false;
}

void CLogicDeviceMgr::print( const index_t& idx,std::ostream& ostr )const{
	const SLogicDeviceInfo* p = info(idx);
	if( p ){
		ostr <<"ID:"<<p->id
				<<" 名字:"<<p->name.c_str()
				<<" 描述:"<<p->desc.c_str()
				<<" 状态量:"<<p->sizeOfStatus<<"<-"<<p->posOfStatus
				<<" 离散量:"<<p->sizeOfDiscrete<<"<-"<<p->posOfDiscrete
				<<" 测量量:"<<p->sizeOfMeasure<<"<-"<<p->posOfMeasure
				<<" 累计量:"<<p->sizeOfCumulant<<"<-"<<p->posOfCumulant
				<<" 远控:"<<p->sizeOfRemoteCtl<<"<-"<<p->posOfRemoteCtl
				<<" 参数:"<<p->sizeOfParameter<<"<-"<<p->posOfParameter
				<<" 动作:"<<p->sizeOfAction<<"<-"<<p->posOfAction;
	}else{
		ostr <<"无效索引("<<idx<<")";
	}
}

}
}



﻿/*
 * timedcumulant.cpp
 *
 *  Created on: 2017年4月11日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_SCADA DM_API_EXPORT

#include <dm/scada/timedcumulant.hpp>

namespace dm{
namespace scada{

CTimedCumulant::CTimedCumulant( const index_t&  idx,const raw_t& raw, const ts_t& ts ):m_idx(idx),m_raw(raw),m_ts(ts){
}

CTimedCumulant::CTimedCumulant( const CTimedCumulant& tm ):m_idx(tm.m_idx),m_raw(tm.m_raw),m_ts(tm.m_ts){
}

CTimedCumulant& CTimedCumulant::operator =( const CTimedCumulant& tm ){
	m_idx = tm.m_idx;
	m_raw = tm.m_raw;
	m_ts = tm.m_ts;
	return *this;
}

}
}




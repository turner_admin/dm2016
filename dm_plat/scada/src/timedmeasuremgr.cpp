﻿/*
 * timedmeasuremgr.cpp
 *
 *  Created on: 2016年11月26日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_SCADA DM_API_EXPORT

#include <dm/scada/timedmeasuremgr.hpp>

namespace dm{
namespace scada{

CTimedMeasureMgr::CTimedMeasureMgr( CScada& scada ):CScada(scada),
		m_q(measureQueue()){
}

CTimedMeasureMgr& CTimedMeasureMgr::ins(){
	static CTimedMeasureMgr i(CScada::ins());
	return i;
}

}
}



﻿/*
 * jsif.cpp
 *
 *  Created on: 2018-12-6
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_SCADA DM_API_EXPORT

#include <dm/scada/jsif.hpp>
#include <cstring>

namespace dm{
namespace scada{

static inline const char* colByIdx( const char* cs[],int size,int idx ){
	if( idx<0 || idx>=size )
		return "unknow";
	return cs[idx];
}

static inline int colByStr( const char* cs[],int size,const char* s ){
	for( int i=0;i<size;++i ){
		if( std::strcmp(s,cs[i])==0 )
			return i;
	}

	return -1;
}

static const char* ColsDevice[CJsDeviceIf::CMax] = {
		"idx","id","name","desc",
		"parent",
		"ssize","spos","dsize","dpos","msize","mpos","csize","cpos","rsize","rpos","psize","ppos","asize","apos",
		"online","state","ts","rxCnt","rxStart","rxTs","txCnt","txStart","txTs"
};

const char* CJsDeviceIf::string( int c ){
	return colByIdx(ColsDevice,CMax,c);
}

int CJsDeviceIf::index( const char* c ){
	return colByStr(ColsDevice,CMax,c);
}

const char* ColsStatus[CJsStatusIf::CMax] = {
		"idx","id","name","desc",
		"flagSave","flagGen","flagRpt",
		"invOff","off","on","invOn","level",
		"value","raw","ds","ts","valueDesc",
		"device","deviceDesc"
};

const char* CJsStatusIf::string( int c ){
	return colByIdx(ColsStatus,CMax,c);
}

int CJsStatusIf::index( const char* c ){
	return colByStr(ColsStatus,CMax,c);
}

const char* ColsDiscrete[CJsDiscreteIf::CMax] = {
		"idx","id","name","desc",
		"flagSave","flagGen","flagRpt",
		"descs",
		"value","raw","ds","ts","valueDesc",
		"device","deviceDesc"
};

const char* CJsDiscreteIf::string( int c ){
	return colByIdx(ColsDiscrete,CMax,c);
}

int CJsDiscreteIf::index( const char* c ){
	return colByStr(ColsDiscrete,CMax,c);
}

const char* ColsMeasure[CJsMeasureIf::CMax] = {
		"idx","id","name","desc",
		"flagSave","flagGen","flagRpt",
		"delta","base","coef","unit",
		"value","raw","ds","ts",
		"device","deviceDesc"
};

const char* CJsMeasureIf::string( int c ){
	return colByIdx(ColsMeasure,CMax,c);
}

int CJsMeasureIf::index( const char* c ){
	return colByStr(ColsMeasure,CMax,c);
}

const char* ColsCumulant[CJsCumulantIf::CMax] = {
		"idx","id","name","desc",
		"flagSave","flagGen","flagRpt",
		"delta","maxCode","coef",
		"value","raw","ds","ts",
		"device","deviceDesc"
};

const char* CJsCumulantIf::string( int c ){
	return colByIdx(ColsCumulant,CMax,c);
}

int CJsCumulantIf::index( const char* c ){
	return colByStr(ColsCumulant,CMax,c);
}

const char* ColsRmtctl[CJsRmtctlIf::CMax] = {
		"idx","id","name","desc",
		"preOpen","open","close","preClose",
		"device","deviceDesc"
};

const char* CJsRmtctlIf::string( int c ){
	return colByIdx(ColsRmtctl,CMax,c);
}

int CJsRmtctlIf::index( const char* c ){
	return colByStr(ColsRmtctl,CMax,c);
}

const char* ColsParam[CJsParamIf::CMax] = {
		"idx","id","name","desc",
		"type",
		"device","deviceDesc"
};

const char* CJsParamIf::string( int c ){
	return colByIdx(ColsParam,CMax,c);
}

int CJsParamIf::index( const char* c ){
	return colByStr(ColsParam,CMax,c);
}

const char* ColsAction[CJsActionIf::CMax] = {
		"idx","id","name","desc",
		"flagSave","flagRpt",
		"invOff","off","on","invOn","level",
		"device","deviceDesc"
};

const char* CJsActionIf::string( int c ){
	return colByIdx(ColsAction,CMax,c);
}

int CJsActionIf::index( const char* c ){
	return colByStr(ColsAction,CMax,c);
}

}
}



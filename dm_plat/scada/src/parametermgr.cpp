﻿/*
 * parametermgr.cpp
 *
 *  Created on: 2017年12月3日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_SCADA DM_API_EXPORT

#include <dm/scada/parametermgr.hpp>

#include <dm/scada/scadainfo.hpp>
#include <dm/scada/devicemgr.hpp>

namespace dm{
namespace scada{

CParameterMgr::CParameterMgr( CScada& scada ):CScada(scada),
		m_cfg(scada.parameterInfos(),scada.info()->parameterCount()){
}

CParameterMgr& CParameterMgr::ins(){
	static CParameterMgr i(CScada::ins());
	return i;
}

/**
 * 修改描述
 * @param idx
 * @param str
 * @return
 */
bool CParameterMgr::setDesc( const index_t& idx,const char* str ){
	SParaInfo* p = m_cfg.at(idx);
	if( p ){
		p->desc = str;
		return true;
	}else
		return false;
}

index_t CParameterMgr::deviceIndex( const index_t& idx )const{
	if( idx<0 || idx>=size() )
		return Idx_Inv;
	CDeviceMgr& d = CDeviceMgr::ins();

	for( index_t i=0;i<d.size();++i ){
		if( d.info(i)->posOfPara<=idx &&
				(d.info(i)->posOfPara+d.info(i)->sizeOfPara)>idx )
			return i;
	}

	return Idx_Inv;
}

const char* CParameterMgr::deviceDesc( const index_t& idx )const{
	if( idx<0 || idx>=size() )
		return NULL;
	CDeviceMgr& d = CDeviceMgr::ins();

	for( index_t i=0;i<d.size();++i ){
		if( d.info(i)->posOfPara<=idx &&
				(d.info(i)->posOfPara+d.info(i)->sizeOfPara)>idx )
			return d.info(i)->desc.c_str();
	}

	return NULL;
}

void CParameterMgr::printInfo( const index_t& idx,std::ostream& ostr )const{
	const SParaInfo* p = info(idx);
	if( p ){
		ostr <<"ID:"<<p->id
				<<" 名字:"<<p->name.c_str()
				<<" 描述:"<<p->desc.c_str()
				<<" 类型:"<<p->type
				;
	}else{
		ostr <<"无效索引("<<idx<<')';
	}
}

}
}



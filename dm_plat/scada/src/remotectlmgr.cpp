﻿/*
 * remotectlmgr.cpp
 *
 *  Created on: 2017年12月3日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_SCADA DM_API_EXPORT

#include <dm/scada/remotectlmgr.hpp>

#include <dm/scada/scadainfo.hpp>
#include <dm/scada/devicemgr.hpp>

namespace dm{
namespace scada{

CRemoteCtlMgr::CRemoteCtlMgr( CScada& scada ):CScada(scada),
		m_cfg(scada.remoteCtlInfos(),scada.info()->remoteCtlCount()){
}

CRemoteCtlMgr& CRemoteCtlMgr::ins(){
	static CRemoteCtlMgr i(CScada::ins());
	return i;
}

bool CRemoteCtlMgr::setDesc( const index_t& idx,const char* str ){
	SRemoteCtlInfo* p = m_cfg.at(idx);
	if( p ){
		p->desc = str;
		return true;
	}else
		return false;
}

index_t CRemoteCtlMgr::deviceIndex( const index_t& idx )const{
	if( idx<0 || idx>=size() )
		return Idx_Inv;
	CDeviceMgr& d = CDeviceMgr::ins();

	for( index_t i=0;i<d.size();++i ){
		if( d.info(i)->posOfTeleCtl<=idx &&
				(d.info(i)->posOfTeleCtl+d.info(i)->sizeOfTeleCtl)>idx )
			return i;
	}

	return Idx_Inv;
}

const char* CRemoteCtlMgr::deviceDesc( const index_t& idx )const{
	if( idx<0 || idx>=size() )
		return NULL;
	CDeviceMgr& d = CDeviceMgr::ins();

	for( index_t i=0;i<d.size();++i ){
		if( d.info(i)->posOfTeleCtl<=idx &&
				(d.info(i)->posOfTeleCtl+d.info(i)->sizeOfTeleCtl)>idx )
			return d.info(i)->desc.c_str();
	}

	return NULL;
}

void CRemoteCtlMgr::printInfo( const index_t& idx,std::ostream& ostr )const{
	const SRemoteCtlInfo* p = info(idx);
	if( p ){
		ostr <<"Id:"<<p->id
				<<" 名字:"<<p->name.c_str()
				<<" 描述:"<<p->desc.c_str()
				<<" 预控分:"<<CRemoteCtlDescMgr::ins().desc(p->descPreOpen)
				<<" 控分:"<<CRemoteCtlDescMgr::ins().desc(p->descOpen)
				<<" 控合:"<<CRemoteCtlDescMgr::ins().desc(p->descClose)
				<<" 预控合:"<<CRemoteCtlDescMgr::ins().desc(p->descPreClose)
				;
	}else{
		ostr <<"无效索引("<<idx<<')';
	}
}

}
}



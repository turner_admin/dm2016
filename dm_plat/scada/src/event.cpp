﻿/*
 * event.cpp
 *
 *  Created on: 2016年11月24日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_SCADA DM_API_EXPORT

#include <dm/scada/event.hpp>

namespace dm{
namespace scada{

CEvent::CEvent( const EType& t,const index_t& i,const ts_t& tt,const index_t& devIdx ):
		ts(tt),type(t),deviceIdx(devIdx),idx(i){
	value.all = 0;
}

CEvent::CEvent( const CEvent& e ):ts(e.ts),type(e.type),deviceIdx(e.deviceIdx),idx(e.idx){
	value.all = e.value.all;
}

CEvent& CEvent::operator =( const CEvent& e ){
	type = e.type;
	deviceIdx = e.deviceIdx;
	idx = e.idx;
	value.all = e.value.all;
	ts = e.ts;

	return *this;
}

}
}



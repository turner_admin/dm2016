﻿/*
 * eventmgr.cpp
 *
 *  Created on: 2016年11月24日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_SCADA DM_API_EXPORT

#include <dm/scada/eventmgr.hpp>

namespace dm{
namespace scada{

CEventMgr::CEventMgr( CScada& scada ):CScada(scada),m_q(eventQueue()){
}

CEventMgr& CEventMgr::ins(){
	static CEventMgr i(CScada::ins());
	return i;
}

}
}


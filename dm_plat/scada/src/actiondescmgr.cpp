﻿/*
 * actiondescmgr.cpp
 *
 *  Created on: 2017年11月30日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_SCADA DM_API_EXPORT

#include <dm/scada/actiondescmgr.hpp>
#include <dm/scada/scadainfo.hpp>

namespace dm{
namespace scada{

/**
 * 构造函数
 * @param scada
 */
CActionDescMgr::CActionDescMgr( CScada& scada ):CScada(scada),
		m_cfg(actionDescInfos(),CScada::info()->actionDescCount()){
}

/**
 * 获取唯一对象
 * @return
 */
CActionDescMgr& CActionDescMgr::ins(){
	static CActionDescMgr i(CScada::ins());
	return i;
}

/**
 * 修改描述
 * @param idx
 * @param str
 * @return
 */
bool CActionDescMgr::setDesc( const index_t& idx,const char* str ){
	SActionDescInfo* p = m_cfg.at(idx);
	if( p ){
		p->desc = str;
		return true;
	}else
		return false;
}

}
}



﻿/*
 * eventmonitor_s.cpp
 *
 *  Created on: 2017年11月3日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_SCADA DM_API_EXPORT

#include <dm/scada/eventmonitor_s.hpp>
#include <dm/scada/eventmonitor.hpp>

namespace dm{
namespace scada{

/**
 * 构造函数
 */
CEventMonitorShadow::CEventMonitorShadow():m_eventMonitor(new CEventMonitor){
}

/**
 * 析构函数
 */
CEventMonitorShadow::~CEventMonitorShadow(){
	delete m_eventMonitor;
}

/**
 * 尝试从事件队列获取新的事件
 * @param info 事件缓存
 * @param overFlow 是否有溢出
 * @return 是否获取成功
 */
bool CEventMonitorShadow::tryGet( CEvent& info,bool& overFlow ){
	return m_eventMonitor->tryGet(info,overFlow);
}

/**
 * 从事件队列中获取新的事件
 * @param info
 * @param overFlow
 */
void CEventMonitorShadow::get( CEvent& info,bool& overFlow ){
	m_eventMonitor->get(info,overFlow);
}

/**
 * 复位事件队列监视
 * 未取出的事件将丢失
 */
void CEventMonitorShadow::reset(){
	m_eventMonitor->reset();
}

}
}



﻿/*
 * deviceagent.cpp
 *
 *  Created on: 2017年3月27日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_SCADA DM_API_EXPORT

#include <dm/scada/deviceagent.hpp>
#include <dm/os/options.hpp>
#include <dm/os/log/logger.hpp>

namespace dm{
namespace scada{

static const char* logModule = "CDeviceAgent.scada.dm";

CDeviceAgent* CDeviceAgent::findByName( const char* name ){
	CDeviceAgent* d = new CDeviceAgent;
	if( d->setByName(name) )
		return d;
	delete d;
	return NULL;
}

CDeviceAgent* CDeviceAgent::findByName( const char* name,const SSignalsSize& s ){
	CDeviceAgent* d = new CDeviceAgent;
	if( d->setByName(name,s) )
		return d;
	delete d;
	return NULL;
}

CDeviceAgent::CDeviceAgent():m_mgr(CDeviceMgr::ins()),m_idx(-1),m_state(NULL){
}

CDeviceAgent::CDeviceAgent( const id_t& id ):m_mgr(CDeviceMgr::ins()){
	m_idx = m_mgr.indexById(id);
	if( m_idx<0 ){
		m_state = NULL;
		log().error(THISMODULE "不能找到设备id=%d",id);
	}else{
		m_state = m_mgr.state(m_idx);
	}
}

CDeviceAgent::CDeviceAgent( const char* name ):m_mgr(CDeviceMgr::ins()){
	m_idx = m_mgr.indexByName(name);
	if( m_idx<0 ){
		m_state = NULL;
		log().error(THISMODULE "不能找到设备name=%s",name);
	}else{
		m_state = m_mgr.state(m_idx);
	}
}

/**
 * 通过索引来初始化
 * @param idx
 * @return
 */
bool CDeviceAgent::setByIndex( const index_t& idx ){
	if( idx<0 || idx>CDeviceMgr::ins().size() ){
		m_idx = Idx_Inv;
		log().error(THISMODULE "不能找到设备idx=%d",idx);
		return false;
	}else{
		m_idx = idx;
		m_state = CDeviceMgr::ins().state(idx);
		return true;
	}
}

/**
 * 通过Id来初始化
 * @param id
 * @return
 */
bool CDeviceAgent::setById( const id_t& id ){
	m_idx = CDeviceMgr::ins().indexById(id);
	if( m_idx<0 ){
		log().error(THISMODULE "不能找到设备id=%d",id);
		return false;
	}

	m_state = CDeviceMgr::ins().state(m_idx);
	return true;
}

/**
 * 通过名字
 * @param name
 * @return
 */
bool CDeviceAgent::setByName( const char* name ){
	m_idx = CDeviceMgr::ins().indexByName(name);
	if( m_idx<0 ){
		log().error(THISMODULE "不能找到设备name=%s",name);
		return false;
	}

	m_state = CDeviceMgr::ins().state(m_idx);
	return true;
}

bool CDeviceAgent::setByIndex( const index_t& idx,const SSignalsSize& size,bool e ){
	if( idx<0 || idx>CDeviceMgr::ins().size() ){
		m_idx = Idx_Inv;
		log().error(THISMODULE "不能找到设备idx=%d",idx);
		return false;
	}else{
		const SDeviceInfo& info = CScada::ins().deviceInfos()[idx];
		if( info.sizeOfStatus!=size.status ){
			log().warnning(THISMODULE "要设置的设备(idx=%d)状态量数量%d不对%d",idx,size.status,info.sizeOfStatus);
			if( e )
				return false;
		}

		if( info.sizeOfDiscrete!=size.discrete ){
			log().warnning(THISMODULE "要设置的设备(idx=%d)离散量数量%d不对%d",idx,size.discrete,info.sizeOfDiscrete);
			if( e )
				return false;
		}

		if( info.sizeOfMeasure!=size.measure ){
			log().warnning(THISMODULE "要设置的设备(idx=%d)测量量数量%d不对%d",idx,size.measure,info.sizeOfMeasure);
			if( e )
				return false;
		}

		if( info.sizeOfCumulant!=size.cumulant ){
			log().warnning(THISMODULE "要设置的设备(idx=%d)累计量数量%d不对%d",idx,size.cumulant,info.sizeOfCumulant);
			if( e )
				return false;
		}

		if( info.sizeOfTeleCtl!=size.remoteCtl ){
			log().warnning(THISMODULE "要设置的设备(idx=%d)远控数量%d不对%d",idx,size.remoteCtl,info.sizeOfTeleCtl);
			if( e )
				return false;
		}

		if( info.sizeOfPara!=size.parameter ){
			log().warnning(THISMODULE "要设置的设备(idx=%d)参数数量%d不对%d",idx,size.parameter,info.sizeOfPara);
			if( e )
				return false;
		}

		if( info.sizeOfAction!=size.action ){
			log().warnning(THISMODULE "要设置的设备(idx=%d)动作数量%d不对%d",idx,size.action,info.sizeOfAction);
			if( e )
				return false;
		}

		m_idx = idx;
		m_state = CDeviceMgr::ins().state(idx);

		return true;
	}
}

bool CDeviceAgent::setById( const id_t& id,const SSignalsSize& size,bool e ){
	bool rt = setByIndex( m_mgr.indexById(id),size,e );
	if( !rt ){
		log().warnning(THISMODULE "不能找到设备id=%d或点表不一致",id);
	}

	return rt;
}

bool CDeviceAgent::setByName( const char* name,const SSignalsSize& size,bool e ){
	index_t idx = m_mgr.indexByName(name);
	if( idx==Idx_Inv ){
		log().info(THISMODULE "不能找到设备%s",name);
		return false;
	}

	if(! setByIndex( idx,size,e ) ){
		log().warnning(THISMODULE "设备%s点表不一致",name);
		return false;
	}

	return true;
}

/**
 * 更新本设备状态信息。
 * 如果状态有变化，会自动产生事件
 * @param status 状态索引。指本设备内的索引号。从0开始
 * @param value
 * @param ds
 * @param ts
 * @return
 */
bool CDeviceAgent::updateStatus( const index_t& status,const CStatus& value,const EDataSource& ds,const ts_t& ts ){
	if( m_idx<0 ){
		log().error(THISMODULE "未设置设备");
		return false;
	}

	const CScada& scada = CScada::ins();
	if( status<0 || status>=scada.deviceInfos()[m_idx].sizeOfStatus ){
		log().warnning(THISMODULE "索引%d溢出(0~%d)",status,scada.deviceInfos()[m_idx].sizeOfStatus );
		return false;
	}

	return CStatusMgr::ins().update(status+info()->posOfStatus,value,ds,ts);
}

/**
 * 更新本地离散量数值
 * 如果数值有变化，会自动产生事件
 * @param discrete 离散量索引号。指本设备内的索引号。从0开始
 * @param value
 * @param ds
 * @param ts
 * @return
 */
bool CDeviceAgent::updateDiscrete( const index_t& discrete,const CDiscrete& value,const EDataSource& ds,const ts_t& ts ){
	if( m_idx<0 ){
		log().error(THISMODULE "未设置设备");
		return false;
	}

	const CScada& scada = CScada::ins();
	if( discrete<0 || discrete>=scada.deviceInfos()[m_idx].sizeOfDiscrete ){
		log().warnning(THISMODULE "索引%d溢出(0~%d)",discrete,scada.deviceInfos()[m_idx].sizeOfDiscrete );
		return false;
	}

	return CDiscreteMgr::ins().update(discrete+info()->posOfDiscrete,value,ds,ts);
}

/**
 * 更新本设备测量量数值
 * @param measure
 * @param raw
 * @param ds
 * @param ts
 * @return
 */
bool CDeviceAgent::updateMeasure( const index_t& measure,const CMeasure::raw_t& raw,const EDataSource& ds,const ts_t& ts ){
	if( m_idx<0 ){
		log().error(THISMODULE "未设置设备");
		return false;
	}

	const CScada& scada = CScada::ins();
	if( measure<0 || measure>=scada.deviceInfos()[m_idx].sizeOfMeasure ){
		log().warnning(THISMODULE "索引%d溢出(0~%d)",measure,scada.deviceInfos()[m_idx].sizeOfMeasure );
		return false;
	}

	return CMeasureMgr::ins().update(measure+info()->posOfMeasure,raw,ds,ts);
}

/**
 * 更新本设备累计量数值
 * @param cumulant
 * @param raw
 * @param ds
 * @param ts
 * @return
 */
bool CDeviceAgent::updateCumulant( const index_t& cumulant,const CCumulant::raw_t& raw,const EDataSource& ds,const ts_t& ts ){
	if( m_idx<0 ){
		log().error(THISMODULE "未设置设备");
		return false;
	}

	const CScada& scada = CScada::ins();
	if( cumulant<0 || cumulant>=scada.deviceInfos()[m_idx].sizeOfCumulant ){
		log().warnning(THISMODULE "索引%d溢出(0~%d)",cumulant,scada.deviceInfos()[m_idx].sizeOfCumulant );
		return false;
	}

	return CCumulantMgr::ins().update(cumulant+info()->posOfCumulant,raw,ds,ts);
}

bool CDeviceAgent::setStatus( const index_t& status,const CStatus& value,const EDataSource& ds,const ts_t& ts ){
	if( m_idx<0 ){
		log().error(THISMODULE "未设置设备");
		return false;
	}

	const CScada& scada = CScada::ins();
	if( status<0 || status>=scada.deviceInfos()[m_idx].sizeOfStatus ){
		log().warnning(THISMODULE "索引%d溢出(0~%d)",status,scada.deviceInfos()[m_idx].sizeOfStatus );
		return false;
	}

	return CStatusMgr::ins().set(status+info()->posOfStatus,value,ds,ts);
}

bool CDeviceAgent::setDiscrete( const index_t& discrete,const CDiscrete& value,const EDataSource& ds,const ts_t& ts ){
	if( m_idx<0 ){
		log().error(THISMODULE "未设置设备");
		return false;
	}

	const CScada& scada = CScada::ins();
	if( discrete<0 || discrete>=scada.deviceInfos()[m_idx].sizeOfDiscrete ){
		log().warnning(THISMODULE "索引%d溢出(0~%d)",discrete,scada.deviceInfos()[m_idx].sizeOfDiscrete );
		return false;
	}

	return CDiscreteMgr::ins().set(discrete+info()->posOfDiscrete,value,ds,ts);
}

bool CDeviceAgent::setMeasure( const index_t& measure,const CMeasure::raw_t& raw,const EDataSource& ds,const ts_t& ts ){
	if( m_idx<0 ){
		log().error(THISMODULE "未设置设备");
		return false;
	}

	const CScada& scada = CScada::ins();
	if( measure<0 || measure>=scada.deviceInfos()[m_idx].sizeOfMeasure ){
		log().warnning(THISMODULE "索引%d溢出(0~%d)",measure,scada.deviceInfos()[m_idx].sizeOfMeasure );
		return false;
	}

	return CMeasureMgr::ins().set(measure+info()->posOfMeasure,raw,ds,ts);
}

bool CDeviceAgent::setCumulant( const index_t& cumulant,const CCumulant::raw_t& raw,const EDataSource& ds,const ts_t& ts ){
	if( m_idx<0 ){
		log().error(THISMODULE "未设置设备");
		return false;
	}

	const CScada& scada = CScada::ins();
	if( cumulant<0 || cumulant>=scada.deviceInfos()[m_idx].sizeOfCumulant ){
		log().warnning(THISMODULE "索引%d溢出(0~%d)",cumulant,scada.deviceInfos()[m_idx].sizeOfCumulant );
		return false;
	}

	return CCumulantMgr::ins().set(cumulant+info()->posOfCumulant,raw,ds,ts);
}

bool CDeviceAgent::genStatus( const index_t& status,const CStatus& value,const ts_t& ts ){
	if( m_idx<0 ){
		log().error(THISMODULE "未设置设备");
		return false;
	}

	const CScada& scada = CScada::ins();
	if( status<0 || status>=scada.deviceInfos()[m_idx].sizeOfStatus ){
		log().warnning(THISMODULE "索引%d溢出(0~%d)",status,scada.deviceInfos()[m_idx].sizeOfStatus );
		return false;
	}

	return CStatusMgr::ins().gen(status+info()->posOfStatus,value,ts);
}

bool CDeviceAgent::genDiscrete( const index_t& discrete,const CDiscrete& value,const ts_t& ts ){
	if( m_idx<0 ){
		log().error(THISMODULE "未设置设备");
		return false;
	}

	const CScada& scada = CScada::ins();
	if( discrete<0 || discrete>=scada.deviceInfos()[m_idx].sizeOfDiscrete ){
		log().warnning(THISMODULE "索引%d溢出(0~%d)",discrete,scada.deviceInfos()[m_idx].sizeOfDiscrete );
		return false;
	}

	return CDiscreteMgr::ins().gen(discrete+info()->posOfDiscrete,value,ts);
}

bool CDeviceAgent::genMeasure( const index_t& measure,const CMeasure::raw_t& raw,const ts_t& ts ){
	if( m_idx<0 ){
		log().error(THISMODULE "未设置设备");
		return false;
	}

	const CScada& scada = CScada::ins();
	if( measure<0 || measure>=scada.deviceInfos()[m_idx].sizeOfMeasure ){
		log().warnning(THISMODULE "索引%d溢出(0~%d)",measure,scada.deviceInfos()[m_idx].sizeOfMeasure );
		return false;
	}

	return CMeasureMgr::ins().gen(measure+info()->posOfMeasure,raw,ts);
}

bool CDeviceAgent::genCumulant( const index_t& cumulant,const CCumulant::raw_t& raw,const ts_t& ts ){
	if( m_idx<0 ){
		log().error(THISMODULE "未设置设备");
		return false;
	}

	const CScada& scada = CScada::ins();
	if( cumulant<0 || cumulant>=scada.deviceInfos()[m_idx].sizeOfCumulant ){
		log().warnning(THISMODULE "索引%d溢出(0~%d)",cumulant,scada.deviceInfos()[m_idx].sizeOfCumulant );
		return false;
	}

	return CCumulantMgr::ins().gen(cumulant+info()->posOfCumulant,raw,ts);
}

/**
 * 产生本设备动作信息
 * @param action
 * @param value
 * @param ts
 * @return
 */
bool CDeviceAgent::genAction( const index_t& action,const CAction::value_t& value,const ts_t& ts ){
	if( m_idx<0 ){
		log().error(THISMODULE "未设置设备");
		return false;
	}

	const CScada& scada = CScada::ins();
	if( action<0 || action>=scada.deviceInfos()[m_idx].sizeOfAction ){
		log().warnning(THISMODULE "索引%d溢出(0~%d)",action,scada.deviceInfos()[m_idx].sizeOfAction );
		return false;
	}

	return CActionMgr::ins().update(action+info()->posOfAction,value,ts);
}

index_t CDeviceAgent::getLocalStatusIndex( const index_t& idx )const{
	if( m_idx<0 ){
		log().error(THISMODULE "未设置设备");
		return Idx_Inv;
	}

	if( idx<info()->posOfStatus || idx-info()->posOfStatus>=info()->sizeOfStatus )
		return Idx_Inv;

	return idx - info()->sizeOfStatus;
}

index_t CDeviceAgent::getLocalDiscreteIndex( const index_t& idx )const{
	if( m_idx<0 ){
		log().error(THISMODULE "未设置设备");
		return Idx_Inv;
	}

	if( idx<info()->posOfDiscrete || idx-info()->posOfDiscrete>=info()->sizeOfDiscrete )
		return Idx_Inv;

	return idx - info()->posOfDiscrete;
}

index_t CDeviceAgent::getLocalMeasureIndex( const index_t& idx )const{
	if( m_idx<0 ){
		log().error(THISMODULE "未设置设备");
		return Idx_Inv;
	}

	if( idx<info()->posOfMeasure || idx-info()->posOfMeasure>=info()->sizeOfMeasure )
		return Idx_Inv;

	return idx - info()->posOfMeasure;
}

index_t CDeviceAgent::getLocalCumulantIndex( const index_t& idx )const{
	if( m_idx<0 ){
		log().error(THISMODULE "未设置设备");
		return Idx_Inv;
	}

	if( idx<info()->posOfCumulant || idx-info()->posOfCumulant>=info()->sizeOfCumulant )
		return Idx_Inv;

	return idx - info()->posOfCumulant;
}

/**
 * 将系统内的远控信息，转换为本设备的远控信息。
 * @param rc
 * @return
 */
index_t CDeviceAgent::getLocalRemoteControlIndex( const index_t& rc )const{
	if( m_idx<0 ){
		log().error(THISMODULE "未设置设备");
		return Idx_Inv;
	}

	if( rc<info()->posOfTeleCtl || rc-info()->posOfTeleCtl>=info()->sizeOfTeleCtl )
		return Idx_Inv;

	return rc - info()->posOfTeleCtl;
}

/**
 * 将系统内的参数信息转换程本设备的参数索引号
 * @param pm
 * @return
 */
index_t CDeviceAgent::getLocalParamterIndex( const index_t& pm )const{
	if( m_idx<0 ){
		log().error(THISMODULE "未设置设备");
		return -1;
	}

	if( pm<info()->posOfPara || pm-info()->posOfPara>=info()->sizeOfPara )
		return -1;

	return pm - info()->posOfPara;
}

index_t CDeviceAgent::getLocalActionIndex( const index_t& idx )const{
	if( m_idx<0 ){
		log().error(THISMODULE "未设置设备");
		return Idx_Inv;
	}

	if( idx<info()->posOfAction || idx-info()->posOfAction>=info()->sizeOfAction )
		return Idx_Inv;

	return idx - info()->posOfAction;
}

/**
 * 将本设备状态索引转换为全局状态索引
 * @param idx
 * @return
 */
index_t CDeviceAgent::getStatusIndex( const index_t& idx )const{
	if( m_idx<0 || idx<0 || idx>=info()->sizeOfStatus )
		return Idx_Inv;
	return idx + info()->posOfStatus;
}

/**
 * 将本设备索引转换为全局索引
 * @param idx
 * @return
 */
index_t CDeviceAgent::getDiscreteIndex( const index_t& idx )const{
	if( m_idx<0 || idx<0 || idx>=info()->sizeOfDiscrete )
		return -1;
	return idx + info()->posOfDiscrete;
}

/**
 * 将本设备索引转换为全局索引
 * @param idx
 * @return
 */
index_t CDeviceAgent::getMeasureIndex( const index_t& idx )const{
	if( m_idx<0 || idx<0 || idx>=info()->sizeOfMeasure )
		return -1;
	return idx + info()->posOfMeasure;
}

/**
 * 将本设备索引转换为全局索引
 * @param idx
 * @return
 */
index_t CDeviceAgent::getCumulantIndex( const index_t& idx )const{
	if( m_idx<0 || idx<0 || idx>=info()->sizeOfCumulant )
		return -1;
	return idx + info()->posOfCumulant;
}

index_t CDeviceAgent::getRemoteControlIndex( const index_t& idx )const{
	if( m_idx<0 || idx<0 || idx>=info()->sizeOfTeleCtl )
		return -1;
	return idx + info()->posOfTeleCtl;
}

index_t CDeviceAgent::getParameterIndex( const index_t& idx )const{
	if( m_idx<0 || idx<0 || idx>=info()->sizeOfPara )
		return -1;
	return idx + info()->posOfPara;
}

index_t CDeviceAgent::getActionIndex( const index_t& idx )const{
	if( m_idx<0 || idx<0 || idx>=info()->sizeOfAction )
		return -1;
	return idx + info()->posOfAction;
}

const CDeviceAgent::rt_s_t* CDeviceAgent::rtStatuses()const{
	if( m_idx<0 )
		return NULL;
	return CStatusMgr::ins().rt(info()->posOfStatus);
}

const CDeviceAgent::rt_d_t* CDeviceAgent::rtDiscretes()const{
	if( m_idx<0 )
		return NULL;
	return CDiscreteMgr::ins().rt(info()->posOfDiscrete);
}

const CDeviceAgent::rt_m_t* CDeviceAgent::rtMeasures()const{
	if( m_idx<0 )
		return NULL;
	return CMeasureMgr::ins().rt(info()->posOfMeasure);
}

const CDeviceAgent::rt_c_t* CDeviceAgent::rtCumulants()const{
	if( m_idx<0 )
		return NULL;
	return CCumulantMgr::ins().rt(info()->posOfCumulant);
}

const CDeviceAgent::rt_s_t* CDeviceAgent::rtStatus( index_t idx )const{
	if( m_idx<0 || idx<0 || idx>=info()->sizeOfStatus )
		return NULL;
	return CStatusMgr::ins().rt(info()->posOfStatus+idx);
}

const CDeviceAgent::rt_d_t* CDeviceAgent::rtDiscrete( index_t idx )const{
	if( m_idx<0 || idx<0 || idx>=info()->sizeOfDiscrete )
		return NULL;
	return CDiscreteMgr::ins().rt(info()->posOfDiscrete+idx);
}

const CDeviceAgent::rt_m_t* CDeviceAgent::rtMeasure( index_t idx )const{
	if( m_idx<0 || idx<0 || idx>=info()->sizeOfMeasure )
		return NULL;
	return CMeasureMgr::ins().rt(info()->posOfMeasure+idx);
}

const CDeviceAgent::rt_c_t* CDeviceAgent::rtCumulant( index_t idx )const{
	if( m_idx<0 || idx<0 || idx>=info()->sizeOfCumulant )
		return NULL;
	return CCumulantMgr::ins().rt(info()->posOfCumulant+idx);
}

CDeviceAgent::s_t CDeviceAgent::rtStatusV(index_t idx )const{
	const rt_s_t* p = rtStatus(idx);
	return p==NULL?0:p->getValue().getValue();
}

CDeviceAgent::d_t CDeviceAgent::rtDiscreteV(index_t idx )const{
	const rt_d_t* p = rtDiscrete(idx);
	return p==NULL?0:p->getValue().getValue();
}

CDeviceAgent::m_t CDeviceAgent::rtMeasureV(index_t idx )const{
	const rt_m_t* p = rtMeasure(idx);
	return p==NULL?0:p->getValue().getValue();
}

CDeviceAgent::c_t CDeviceAgent::rtCumulantV(index_t idx )const{
	const rt_c_t* p = rtCumulant(idx);
	return p==NULL?0:p->getValue().getValue();
}

bool CDeviceAgent::loadStatusByOption_int( const char* group,const char* option,index_t idx,int v ){
	dm::env::COptions op;

	return loadStatusByOption_int(op,group,option,idx,v);
}

/**
 * 从系统选项中加载状态量
 * @param group 选项组
 * @param option 选项名
 * @param idx 本设备状态量索引
 * @param v 默认值
 * @return 是否成功
 */
bool CDeviceAgent::loadStatusByOption_int( dm::env::COptions& op,const char* group,const char* option,index_t idx,int v ){
	if( m_idx<0 || idx<0 || info()->sizeOfStatus<=idx )
		return false;

	if( !op.get(option,v,group) ){
		op.add(option,group);
		op.set(option,v,group);
	}

	updateStatus(idx,v);

	return true;
}

bool CDeviceAgent::loadDiscreteByOption_int( const char* group,const char* option,index_t idx,int v ){
	dm::env::COptions op;
	return loadDiscreteByOption_int(op,group,option,idx,v);
}

bool CDeviceAgent::loadDiscreteByOption_int( dm::env::COptions& op,const char* group,const char* option,index_t idx,int v ){
	if( m_idx<0 || idx<0 || info()->sizeOfDiscrete<=idx )
		return false;

	if( !op.get(option,v,group) ){
		op.add(option,group);
		op.set(option,v,group);
	}

	updateDiscrete(idx,v);

	return true;
}

bool CDeviceAgent::loadMeasureByOption_float( const char* group,const char* option,index_t idx,float v ){
	dm::env::COptions op;
	return loadMeasureByOption_float(op,group,option,idx,v);
}

bool CDeviceAgent::loadMeasureByOption_float( dm::env::COptions& op,const char* group,const char* option,index_t idx,float v ){
	if( m_idx<0 || idx<0 || info()->sizeOfMeasure<=idx )
		return false;

	if( !op.get(option,v,group) ){
		op.add(option,group);
		op.set(option,v,group);
	}

	updateMeasure(idx,v);

	return true;
}

bool CDeviceAgent::loadMeasureByOption_int( const char* group,const char* option,index_t idx,int v ){
	dm::env::COptions op;
	return loadMeasureByOption_int(op,group,option,idx,v);
}

bool CDeviceAgent::loadMeasureByOption_int( dm::env::COptions& op,const char* group,const char* option,index_t idx,int v ){
	if( m_idx<0 || idx<0 || info()->sizeOfMeasure<=idx )
		return false;

	if( !op.get(option,v,group) ){
		op.add(option,group);
		op.set(option,v,group);
	}

	updateMeasure(idx,v);

	return true;
}

bool CDeviceAgent::loadCumulantByOption_int( const char* group,const char* option,index_t idx,int v ){
	dm::env::COptions op;
	return loadCumulantByOption_int(op,group,option,idx,v);
}

bool CDeviceAgent::loadCumulantByOption_int( dm::env::COptions& op,const char* group,const char* option,index_t idx,int v ){
	if( m_idx<0 || idx<0 || info()->sizeOfCumulant<=idx )
		return false;

	if( !op.get(option,v,group) ){
		op.add(option,group);
		op.set(option,v,group);
	}

	updateCumulant(idx,v);

	return true;
}

bool CDeviceAgent::updateStatusWithOption_int( const char* group,const char* option,index_t idx,int v ){
	dm::env::COptions op;
	return updateStatusWithOption_int(op,group,option,idx,v);
}

bool CDeviceAgent::updateStatusWithOption_int( dm::env::COptions& op,const char* group,const char* option,index_t idx,int v ){
	if( m_idx<0 || idx<0 || info()->sizeOfStatus<=idx )
		return false;

	if( updateStatus(idx,v) ){
		op.set(option,v,group);
		return true;
	}else
		return false;
}

bool CDeviceAgent::updateDiscreteWithOption_int( const char* group,const char* option,index_t idx,int v ){
	dm::env::COptions op;
	return updateDiscreteWithOption_int(op,group,option,idx,v);
}

bool CDeviceAgent::updateDiscreteWithOption_int( dm::env::COptions& op,const char* group,const char* option,index_t idx,int v ){
	if( m_idx<0 || idx<0 || info()->sizeOfDiscrete<=idx )
		return false;

	if( updateDiscrete(idx,v) ){
		op.set(option,v,group);
		return true;
	}else
		return false;
}

bool CDeviceAgent::updateMeasureWithOption_float( const char* group,const char* option,index_t idx,float v ){
	dm::env::COptions op;
	return updateMeasureWithOption_float(op,group,option,idx,v);
}

bool CDeviceAgent::updateMeasureWithOption_float( dm::env::COptions& op,const char* group,const char* option,index_t idx,float v ){
	if( m_idx<0 || idx<0 || info()->sizeOfMeasure<=idx )
		return false;

	if( updateMeasure(idx,v) ){
		op.set(option,v,group);
		return true;
	}else
		return false;
}

bool CDeviceAgent::updateMeasureWithOption_int( const char* group,const char* option,index_t idx,int v ){
	dm::env::COptions op;
	return updateMeasureWithOption_int(op,group,option,idx,v);
}

bool CDeviceAgent::updateMeasureWithOption_int( dm::env::COptions& op,const char* group,const char* option,index_t idx,int v ){
	if( m_idx<0 || idx<0 || info()->sizeOfMeasure<=idx )
		return false;

	if( updateMeasure(idx,v) ){
		op.set(option,v,group);
		return true;
	}else
		return false;
}

bool CDeviceAgent::updateCumulantWithOption_int( const char* group,const char* option,index_t idx,int v ){
	dm::env::COptions op;
	return updateCumulantWithOption_int(op,group,option,idx,v);
}

bool CDeviceAgent::updateCumulantWithOption_int( dm::env::COptions& op,const char* group,const char* option,index_t idx,int v ){
	if( m_idx<0 || idx<0 || info()->sizeOfCumulant<=idx )
		return false;

	if( updateCumulant(idx,v) ){
		op.set(option,v,group);
		return true;
	}else
		return false;
}

}
}



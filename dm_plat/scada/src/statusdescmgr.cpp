﻿/*
 * statusdescmgr.cpp
 *
 *  Created on: 2017年12月1日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_SCADA DM_API_EXPORT

#include <dm/scada/statusdescmgr.hpp>
#include <dm/scada/scadainfo.hpp>

namespace dm{
namespace scada{

CStatusDescMgr::CStatusDescMgr( CScada& scada ):CScada(scada),m_cfg(statusDescInfos(),CScada::info()->statusDescCount()){
}

CStatusDescMgr& CStatusDescMgr::ins(){
	static CStatusDescMgr i(CScada::ins());
	return i;
}

/**
 * 修改描述
 * @param idx
 * @param str
 * @return 是否成功
 */
bool CStatusDescMgr::setDesc( const index_t& idx,const char* str ){
	SStatusDescInfo* p = m_cfg.at(idx);
	if( p ){
		p->desc = str;
		return true;
	}else
		return false;
}

}
}



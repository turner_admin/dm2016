﻿/*
 * timedcumulantmgr.cpp
 *
 *  Created on: 2017年4月11日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_SCADA DM_API_EXPORT

#include <dm/scada/timedcumulantmgr.hpp>

namespace dm{
namespace scada{

CTimedCumulantMgr::CTimedCumulantMgr( CScada& scada ):CScada(scada),
		m_q(cumulantQueue()){
}

CTimedCumulantMgr& CTimedCumulantMgr::ins(){
	static CTimedCumulantMgr i(CScada::ins());
	return i;
}

}
}



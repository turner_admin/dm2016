﻿/*
 * remotectldescmgr.cpp
 *
 *  Created on: 2017年12月3日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_SCADA DM_API_EXPORT

#include <dm/scada/remotectldescmgr.hpp>
#include <dm/scada/scadainfo.hpp>

namespace dm{
namespace scada{

CRemoteCtlDescMgr::CRemoteCtlDescMgr( CScada& scada ):CScada(scada),
		m_cfg(scada.remoteCtlDescInfos(),scada.info()->remoteCtlDescCount()){
}

CRemoteCtlDescMgr& CRemoteCtlDescMgr::ins(){
	static CRemoteCtlDescMgr i(CScada::ins());
	return i;
}

/**
 * 修改描述
 * @param idx
 * @param str
 * @return
 */
bool CRemoteCtlDescMgr::setDesc( const index_t& idx,const char* str ){
	SRemoteCtlDescInfo* p = m_cfg.at(idx);
	if( p ){
		p->desc = str;
		return true;
	}else
		return false;
}

}
}



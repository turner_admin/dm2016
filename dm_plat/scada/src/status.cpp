﻿/*
 * status.cpp
 *
 *  Created on: 2017年3月24日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_SCADA DM_API_EXPORT

#include <dm/scada/status.hpp>

namespace dm{
namespace scada{

CStatus& CStatus::operator =( const CStatus& s ){
	m_v = s.m_v;
	m_raw = s.m_raw;
	return *this;
}

CStatus& CStatus::operator =( volatile const CStatus& s ){
	m_v = s.m_v;
	m_raw = s.m_raw;
	return *this;
}

bool CStatus::update( const CStatus& s ){
	bool r = (m_v!=s.m_v);
	m_v = s.m_v;
	m_raw = s.m_raw;
	return r;
}

bool CStatus::update( const CStatus& s )volatile{
	bool r = (m_v!=s.m_v);
	m_v = s.m_v;
	m_raw = s.m_raw;
	return r;
}



}
}


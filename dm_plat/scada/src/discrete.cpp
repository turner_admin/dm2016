﻿/*
 * discrete.cpp
 *
 *  Created on: 2017年3月27日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_SCADA DM_API_EXPORT

#include <dm/scada/discrete.hpp>

namespace dm{
namespace scada{

/**
 * 更新
 * @param d
 * @return 是否更新成功
 * - true 更新了原值
 * - false 值相等，未更新原值。
 */
bool CDiscrete::update( const CDiscrete& d ){
	if( m_v==d.m_v )
		return false;
	m_v = d.m_v;
	m_r = d.m_r;
	return true;
}

bool CDiscrete::update( const CDiscrete& d )volatile{
	if( m_v==d.m_v )
		return false;
	m_v = d.m_v;
	m_r = d.m_r;
	return true;
}

}
}



#include <dm/export.hpp>

#define DM_API_SCADA DM_API_EXPORT

#include <dm/scada/model/model_mgr.hpp>
#include <dm/scada/model/loader.hpp>
#include <dm/env/cfg.hpp>

#include <dm/os/log/logger.hpp>

namespace dm{
namespace scada{
namespace model{

static const char* logModule = "CModelMgr.model.scada.dm";

CModelMgr::CModelMgr(){
    log().debug(THISMODULE "创建对象");

    dm::env::CCfg& cfg = dm::env::CCfg::ins();

    dm::TScopedPtr<dm::os::CDb> db(dm::os::createDb(cfg.scada_dbName().c_str(),cfg.scada_dbHost().c_str(),cfg.scada_dbEngine().c_str(),cfg.scada_dbPort()));
    if( !db->connect(cfg.scada_dbUsr().c_str(),cfg.scada_dbPwd().c_str()) ){
        log().error(THISMODULE "连接数据库失败");
        return;
    }

    CLoader loader(*db);

    m_bufMgr = new buf_mgr_t(logModule);
    while( m_bufMgr->loadNew(loader));
}

CModelMgr::~CModelMgr(){
    if( m_bufMgr )
        delete m_bufMgr;
}

CModelMgr& CModelMgr::ins(){
    static CModelMgr ins;
    return ins;
}

size_t CModelMgr::size(){
    return m_bufMgr->count();
}

void CModelMgr::reset(){
}
}
}
}

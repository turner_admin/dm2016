﻿/*
 * measure.cpp
 *
 *  Created on: 2017年3月27日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_SCADA DM_API_EXPORT

#include <dm/scada/measure.hpp>

namespace dm{
namespace scada{

bool CMeasure::update( const CMeasure& m ){
	if( m_v==m.m_v )
		return false;

	m_v = m.m_v;
	m_r = m.m_r;
	return true;
}

bool CMeasure::update( const CMeasure& m )volatile{
	if( m_v==m.m_v )
		return false;

	m_v = m.m_v;
	m_r = m.m_r;
	return true;
}

}
}



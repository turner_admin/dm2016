﻿/*
 * discretedescmgr.cpp
 *
 *  Created on: 2017年12月2日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_SCADA DM_API_EXPORT

#include <dm/scada/discretedescmgr.hpp>
#include <dm/scada/scadainfo.hpp>

namespace dm{
namespace scada{

CDiscreteDescMgr::CDiscreteDescMgr( CScada& scada ):CScada(scada),m_cfg(discreteDescInfos(),scada.info()->discreteDescCount()){
}

CDiscreteDescMgr& CDiscreteDescMgr::ins(){
	static CDiscreteDescMgr i(CScada::ins());
	return i;
}

/**
 * 设置描述
 * @param idx
 * @param str
 * @return
 */
bool CDiscreteDescMgr::setDesc( const index_t& idx,const char* str ){
	SDiscreteDescInfo* p = m_cfg.at(idx);
	if( p ){
		p->desc = str;
		return true;
	}else
		return false;
}

}
}




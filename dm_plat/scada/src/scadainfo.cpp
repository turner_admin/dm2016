﻿/*
 * scadainfo.cpp
 *
 *  Created on: 2017年11月21日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_SCADA DM_API_EXPORT

#include <dm/scada/scadainfo.hpp>
#include <dm/os/db/db.hpp>
#include <dm/os/db/resultset.hpp>
#include <dm/scoped_ptr.hpp>

#include <dm/os/log/logger.hpp>
#include <dm/env/cfg.hpp>

namespace dm{
namespace scada{

static const char* logModule = "CScadaInfo.scada.dm";

CScadaInfo::CScadaInfo():
	m_deviceCount(-1),m_statusCount(-1),m_statusDescCount(-1),
	m_discreteCount(-1),m_discreteDescCount(-1),m_measureCount(-1),
	m_cumulantCount(-1),m_remoteCtlCount(-1),m_remoteCtlDescCount(-1),
	m_parameterCount(-1),m_actionCount(-1),m_actionDescCount(-1),
	m_logicDeviceCount(-1),m_logicStatusCount(-1),m_logicDiscreteCount(-1),
	m_logicMeasureCount(-1),m_logicCumulantCount(-1),m_logicRemoteCtlCount(-1),
	m_logicParameterCount(-1),m_logicActionCount(-1){

	dm::env::CCfg& cfg = dm::env::CCfg::ins();

	dm::TScopedPtr<dm::os::CDb> db(dm::os::createDb(cfg.scada_dbName().c_str(),cfg.scada_dbHost().c_str(),cfg.scada_dbEngine().c_str()));
	if( !db->connect(cfg.scada_dbUsr().c_str(),cfg.scada_dbPwd().c_str()) ){
		log().error( THISMODULE "连接数据库失败 %s:%s@%s.%s",cfg.scada_dbEngine().c_str(),cfg.scada_dbUsr().c_str(),cfg.scada_dbHost().c_str(),cfg.scada_dbName().c_str());
		return;
	}

	m_deviceCount = getDbCount(*db,"t_device");

	m_statusCount = getDbCount(*db,"t_status");
	m_statusDescCount = getDbCount(*db,"t_status_desc");

	m_discreteCount = getDbCount(*db,"t_discrete");
	m_discreteDescCount = getDbCount(*db,"t_discrete_desc");

	m_measureCount = getDbCount(*db,"t_measure");

	m_cumulantCount = getDbCount(*db,"t_cumulant");

	m_remoteCtlCount = getDbCount(*db,"t_remote_ctl");
	m_remoteCtlDescCount = getDbCount(*db,"t_remote_ctl_desc");

	m_parameterCount = getDbCount(*db,"t_parameter");

	m_actionCount = getDbCount(*db,"t_action");
	m_actionDescCount = getDbCount(*db,"t_action_desc");

	m_logicDeviceCount = getDbCount(*db,"t_logic_device");

	m_logicStatusCount = getDbCount(*db,"t_logic_status");
	m_logicDiscreteCount = getDbCount(*db,"t_logic_discrete");
	m_logicMeasureCount = getDbCount(*db,"t_logic_measure");
	m_logicCumulantCount = getDbCount(*db,"t_logic_cumulant");
	m_logicRemoteCtlCount = getDbCount(*db,"t_logic_remotectl");
	m_logicParameterCount = getDbCount(*db,"t_logic_parameter");
	m_logicActionCount = getDbCount(*db,"t_logic_action");

	log().debug(THISMODULE "dev:%d status:%d discrete:%d measure:%d cumulant:%d rmtctl:%d paras:%d action:%d \nldev:%d lstatus:%d ldiscrete:%d lmeasure:%d lcumulant:%d lrmtctl:%d lparas:%d laction:%d",
	m_deviceCount,m_statusCount,m_discreteCount,m_measureCount,m_cumulantCount,m_remoteCtlCount,m_parameterCount,m_actionCount,
	m_logicDeviceCount,m_logicStatusCount,m_logicDiscreteCount,m_logicMeasureCount,m_logicCumulantCount,m_logicRemoteCtlCount,m_logicParameterCount,m_logicActionCount);
}

int CScadaInfo::getDbCount( dm::os::CDb& db,const char* table )const{
	char sql[1024];
	sprintf(sql,"select count(*) from %s",table);

	dm::TScopedPtr<dm::os::CResultSet> rs(db.query(sql));

	if( !rs ){
		log().error( THISMODULE "数据库表(%s)查询失败",table);
		return -1;
	}
	rs->next();
	return rs->asInt(0);
}

}
}



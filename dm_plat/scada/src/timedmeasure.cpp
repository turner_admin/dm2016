﻿/*
 * timedmeasure.cpp
 *
 *  Created on: 2016年11月26日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_SCADA DM_API_EXPORT

#include <dm/scada/timedmeasure.hpp>

namespace dm{
namespace scada{

CTimedMeasure::CTimedMeasure( const index_t&  idx,const value_t& value, const ts_t& ts ):m_idx(idx),m_value(value),m_ts(ts){
}

CTimedMeasure::CTimedMeasure( const CTimedMeasure& tm ):m_idx(tm.m_idx),m_value(tm.m_value),m_ts(tm.m_ts){
}

CTimedMeasure& CTimedMeasure::operator =( const CTimedMeasure& tm ){
	m_idx = tm.m_idx;
	m_value = tm.m_value;
	m_ts = tm.m_ts;
	return *this;
}

}
}



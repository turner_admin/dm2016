﻿/*
 * devicestate.cpp
 *
 *  Created on: 2017年9月15日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_SCADA DM_API_EXPORT

#include <dm/scada/devicestate.hpp>

namespace dm{
namespace scada{

CDeviceState::CDeviceState():m_lastState(ESSysInit),m_lastStateTime(dm::CRunTimeStamp::cur()),m_stateLifeInSec(10),
		m_rxCnt(),m_txCnt(),m_rxStart(dm::CRunTimeStamp::cur()),m_txStart(dm::CRunTimeStamp::cur()),
		m_lastRx(),m_lastTx(){
}

/**
 * 更新设备状态
 * 应该定时周期更新设备状态。否则会检测到设备不在线
 * @param state
 * @param now
 */
void CDeviceState::updateState( const EState& state,const dm::CRunTimeStamp& now ){
	// todo 需要增加事件产生
	m_lastState = state;
	m_lastStateTime = now;
}

void CDeviceState::setStateLifeInSec( const int& sec ){
	m_stateLifeInSec = sec;
}

/**
 * 接收流量统计重新计数
 * @param n
 */
void CDeviceState::rxRecont( const dm::CRunTimeStamp& n ){
	m_rxCnt.reset();
	m_rxStart = n;
}

/**
 * 发送流量统计重新计数
 * @param n
 */
void CDeviceState::txRecont( const dm::CRunTimeStamp& n ){
	m_txCnt.reset();
	m_txStart = n;
}

/**
 * 累计接收字节数，并更新接收时刻
 * @param num
 * @param n
 */
void CDeviceState::rxed( dm::uint32 num,const dm::CRunTimeStamp& n ){
	m_rxCnt += num;
	m_lastRx = n;
}

/**
 * 累计发送字节数,并更新发送时刻
 * @param num
 * @param n
 */
void CDeviceState::txed( dm::uint32 num,const dm::CRunTimeStamp& n ){
	m_txCnt += num;
	m_lastTx = n;
}

}
}



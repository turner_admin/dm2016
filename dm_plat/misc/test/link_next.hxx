﻿/*
 * link_next.hxx
 *
 *  Created on: 2020年2月22日
 *      Author: Dylan
 */

#ifndef MISC_TEST_LINK_NEXT_HXX_
#define MISC_TEST_LINK_NEXT_HXX_

#include "cxxtest/TestSuite.h"
#include <dm/link_next.hpp>
#include <iostream>

using namespace std;

class TestLinkNext:public CxxTest::TestSuite{
	typedef dm::TCLinkNext<int> link_t;
public:
	void testOperator(){
		link_t* v = new link_t(10);

		TSM_ASSERT_EQUALS("默认值",v->get(),10);
		TSM_ASSERT("默认值",v->next()==NULL);
		TSM_ASSERT_EQUALS("默认值",v->isRing(),false);

		v->pushBack(20);
		TSM_ASSERT_EQUALS("默认值",v->size(),2);
		showLink(v);

		v = v->reverse();
		showLink(v);

		v->insert(30);
		showLink(v);

		v->pushBack(v);
		showLink(v);
	}

protected:
	void showLink( const link_t* l ){
		cout <<"单向链表:";
		const link_t* r = l->getRingNode();
		if( r )
			cout <<"(环形)";

		unsigned int s = l->size();
		cout <<s<<endl;

		for( unsigned int i=0;i<s;++i ){
			cout <<i<<":\t"<<l->get()<<endl;
			l = l->next();
		}
	}
};

#endif /* MISC_TEST_LINK_NEXT_HXX_ */

/*
 * bitsref.hxx
 *
 *  Created on: 2024年4月9日
 *      Author: Dylan.Gao
 */

#ifndef DM_PLAT_MISC_TEST_BITSREF_HXX_
#define DM_PLAT_MISC_TEST_BITSREF_HXX_

#include "cxxtest/TestSuite.h"
#include <dm/bitsref.hpp>

class TestBitsref:public CxxTest::TestSuite{
public:
	void testDefault(){
		dm::uint8 bits;
		dm::CBitsRef<dm::uint8> ref(bits);

		for( int i=0;i<4;++i ){
			ref.set(i*2);
			ref.clr(i*2+1);
		}

		TSM_ASSERT_EQUALS("数据设置",ref.getRef(),0x55);
		for( int i=0;i<4;++i ){
			TSM_ASSERT("校验奇数位",ref.isSet(i*2));
			TSM_ASSERT("校验偶数位",!ref.isSet(i*2+1));
			TSM_ASSERT("校验奇数位",!ref.isClr(i*2));
			TSM_ASSERT("校验偶数位",ref.isClr(i*2+1));
		}

		for( int i=0;i<4;++i ){
			ref.set(i*2+1);
			ref.clr(i*2);
		}

		for( int i=0;i<4;++i ){
			TSM_ASSERT("校验奇数位",ref.isSet(i*2+1));
			TSM_ASSERT("校验偶数位",!ref.isSet(i*2));
			TSM_ASSERT("校验奇数位",!ref.isClr(i*2+1));
			TSM_ASSERT("校验偶数位",ref.isClr(i*2));
		}
	}
};

#endif /* DM_PLAT_MISC_TEST_BITSREF_HXX_ */

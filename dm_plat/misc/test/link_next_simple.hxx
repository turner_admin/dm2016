﻿/*
 * link_next_simple.hxx
 *
 *  Created on: 2020年2月23日
 *      Author: Dylan.Gao
 */

#ifndef MISC_TEST_LINK_NEXT_SIMPLE_HXX_
#define MISC_TEST_LINK_NEXT_SIMPLE_HXX_

#include "cxxtest/TestSuite.h"
#include <iostream>
using namespace std;

#include <dm/link_next_simple.hpp>

class TestLinkNextSimple:public CxxTest::TestSuite{
	typedef dm::TCLinkNextSimple<int> link_t;
public:
	void testOperator(){
		link_t* v = new link_t(10);

		TSM_ASSERT("默认值",v->get());
		TSM_ASSERT("默认值",v->next()==NULL);
		TSM_ASSERT_EQUALS("默认值",v->size(),1);
		showLink(v);

		for( int i=0;i<10;++i )
			v->pushBack(i+1);
		showLink(v);

		v = v->reverse();
		showLink(v);
	}

protected:
	void showLink( link_t* l ){
		cout <<"\n输出列表:\n";
		unsigned i = 0;
		while( l ){
			cout <<i<<":\t";
			if( l->get())
				cout << l->get()<<endl;
			else
				cout <<"空"<<endl;

			l = l->next();
			++i;
		}
	}
};

#endif /* MISC_TEST_LINK_NEXT_SIMPLE_HXX_ */

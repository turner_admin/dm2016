﻿/*
 * sparce_map.hxx
 *
 *  Created on: 2020年2月23日
 *      Author: Dylan.Gao
 */

#ifndef MISC_TEST_SPARCE_MAP_HXX_
#define MISC_TEST_SPARCE_MAP_HXX_

#include "cxxtest/TestSuite.h"
#include <iostream>
using namespace std;

#include <dm/sparce_map.hpp>

class TestSparceMap:public CxxTest::TestSuite{
	typedef dm::TCSparceMap<int,int> map_t;
public:
	void testOperator(){
		map_t v;

		TSM_ASSERT_EQUALS("默认值",v.size(),0);

		for( int i=0;i<5;++i )
			v.add(i,i);
		showMap(v);

		TSM_ASSERT_EQUALS("默认值",v.size(),5);

		for( int i=0;i<5;++i )
			v.add(i,i);

		TSM_ASSERT_EQUALS("默认值",v.size(),5);

		for( int i=0;i<5;++i )
			v.add(i*2,i*2);

		showMap(v);
		TSM_ASSERT_EQUALS("默认值",v.size(),7);
	}

protected:
	void showMap( map_t& m ){
		unsigned s = m.size();
		cout <<"\n记录数:"<<s<<endl;

		for( unsigned int i=0;i<s;++i ){
			const map_t::STagValue* v = m.getByIndex(i);
			cout <<i<<":\tkey:"<<v->k<<" value:"<<v->v<<endl;
		}
	}
};



#endif /* MISC_TEST_SPARCE_MAP_HXX_ */

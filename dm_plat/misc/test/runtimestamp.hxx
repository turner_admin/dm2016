﻿/*
 * runtimestamp.hxx
 *
 *  Created on: 2020年4月27日
 *      Author: work
 */

#ifndef MISC_TEST_RUNTIMESTAMP_HXX_
#define MISC_TEST_RUNTIMESTAMP_HXX_

#include "cxxtest/TestSuite.h"
#include <dm/runtimestamp.hpp>

class TestRunTimeStamp:public CxxTest::TestSuite{
public:
	void testCur(){
		dm::CRunTimeStamp rts1 = dm::CRunTimeStamp::cur();
		dm::CRunTimeStamp rts2 = dm::CRunTimeStamp::cur();
		TSM_ASSERT_LESS_THAN_EQUALS("时序",rts1,rts2);

		TSM_ASSERT_EQUALS("相等",rts1,rts2=rts1);
		rts2 = dm::CRunTimeStamp::cur();

		TSM_ASSERT_LESS_THAN("小于",rts1,rts2);
		TSM_ASSERT("大于",rts2>rts1);
	}
};



#endif /* MISC_TEST_RUNTIMESTAMP_HXX_ */

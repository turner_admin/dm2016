﻿/*
 * magic.hxx
 *
 *  Created on: 2023年6月14日
 *      Author: 70361
 */

#ifndef DM_PLAT_MISC_TEST_MAGIC_HXX_
#define DM_PLAT_MISC_TEST_MAGIC_HXX_

#include "cxxtest/TestSuite.h"
#include <dm/magic.hpp>

class TestMagic:public CxxTest::TestSuite{
public:
	void testOperator(){
		volatile dm::CMagic::magic_t vmagic;

		dm::CMagic magic1(&vmagic);


		TSM_ASSERT_EQUALS("未初始化",magic1.ifValid(),false);

		magic1.init();
		TSM_ASSERT("初始化",magic1.ifValid());
		TSM_ASSERT("初始化后的同步",magic1.ifUpdated());

		magic1.destroy();
		TSM_ASSERT_EQUALS("销毁",magic1.ifValid(),false);
	}

	void testSync(){
		volatile dm::CMagic::magic_t vmagic;

		dm::CMagic magic1(&vmagic);
		dm::CMagic magic2(&vmagic);

		magic1.init();

		TSM_ASSERT("未初始化",!magic2.ifValid());

		TSM_ASSERT("未同步",!magic2.ifUpdated());

		magic2.update();

		TSM_ASSERT("同步初始化",magic2.ifValid());

		magic2.refresh();
		TSM_ASSERT("本地刷新",magic2.ifValid());

		TSM_ASSERT("异地刷新",!magic1.ifUpdated());
	}
};



#endif /* DM_PLAT_MISC_TEST_MAGIC_HXX_ */

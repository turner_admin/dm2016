﻿/*
 * sparce_color.hxx
 *
 *  Created on: 2020年2月24日
 *      Author: Dylan.Gao
 */

#ifndef MISC_TEST_SPARCE_COLOR_HXX_
#define MISC_TEST_SPARCE_COLOR_HXX_

#include "cxxtest/TestSuite.h"
#include <iostream>
using namespace std;

#include <dm/sparce_color.hpp>

class TestSparceColor:public CxxTest::TestSuite{
public:
	void testOperator(){
		dm::TCSparceColor<dm::ptr_t> m;
		int * i = new int[10];

		TSM_ASSERT("增加",m.add(dm::ptr_t(i)));
		TSM_ASSERT("增加",m.add(dm::ptr_t(i+1)));
		TSM_ASSERT("增加",m.add(dm::ptr_t(i+2)));

		show(m);

		TSM_ASSERT("重复增加",m.add(dm::ptr_t(i))==false);
		TSM_ASSERT("查找",m.find(dm::ptr_t(i)));
		TSM_ASSERT("查找",m.find(dm::ptr_t(i+3))==false);

		TSM_ASSERT("删除",m.del(dm::ptr_t(i)));
		TSM_ASSERT("删除",m.del(dm::ptr_t(i))==false);
		TSM_ASSERT("删除",m.del(dm::ptr_t(i+3))==false);

		show(m);
	}

	void show( const dm::TCSparceColor<dm::ptr_t>& m ){
		unsigned int s = m.size();
		cout <<"\n着色器:"<<s<<endl;
		for( unsigned int i=0;i<s;++i ){
			cout <<i<<":\t"<<m.getByIndex(i)<<endl;
		}
	}
};

#endif /* MISC_TEST_SPARCE_COLOR_HXX_ */

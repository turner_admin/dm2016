﻿/*
 * ptrvector.hxx
 *
 *  Created on: 2023年9月22日
 *      Author: Dylan.Gao
 */

#ifndef DM_PLAT_MISC_TEST_PTRVECTOR_HXX_
#define DM_PLAT_MISC_TEST_PTRVECTOR_HXX_

#include "cxxtest/TestSuite.h"
#include <dm/ptrvector.hpp>
#include <string>

class CTestPtrVector:public CxxTest::TestSuite{
public:
	void testDefault(){
		class CInfo{
		public:
			CInfo(const char* item):m_info(item){
				TS_WARN("构造对象");
				TS_WARN(item);
			}

			~CInfo(){
				TS_WARN("析构对象");
				TS_WARN(m_info.c_str());
			}

			std::string m_info;
		};

		dm::TCPtrVector<CInfo> vs;
		vs.append(new CInfo("对象1"));
		vs.append(new CInfo("对象2"));
		vs.append(new CInfo("对象3"));

		TS_TRACE("OK");
	}
};

#endif /* DM_PLAT_MISC_TEST_PTRVECTOR_HXX_ */

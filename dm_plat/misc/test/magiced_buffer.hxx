﻿/*
 * magiced_buffer.hxx
 *
 *  Created on: 2023年9月5日
 *      Author: Dylan.Gao
 */

#ifndef DM_PLAT_MISC_TEST_MAGICED_BUFFER_HXX_
#define DM_PLAT_MISC_TEST_MAGICED_BUFFER_HXX_

#include <dm/magiced_buffer.hpp>
#include "cxxtest/TestSuite.h"

/**
 * 含魔数的缓冲区单元测试
 */
class TestMagicedBuffer:public CxxTest::TestSuite{
public:
	void testOperator(){
		dm::TSMagicedBuffer<int> buffer;
		buffer.init();

		TSM_ASSERT_EQUALS("数据大小",buffer.size(),32);
		TSM_ASSERT_EQUALS("初始成员数量",buffer.count(),0);
		TSM_ASSERT_EQUALS("无标记判断",buffer.ifNoneMasked(),true);
		TSM_ASSERT_EQUALS("全标记判断",buffer.ifAllMasked(),false);
		for( int i=0;i<buffer.size();++i ){
			TSM_ASSERT_EQUALS("按索引标记判断",buffer.ifMasked(i),false);
		}
		TSM_ASSERT_EQUALS("正向标记查找",buffer.nextMasked(),buffer.size());
		TSM_ASSERT_EQUALS("反向标记查找",buffer.lastMasked(),buffer.size());
		TSM_ASSERT_EQUALS("正向非标记查找",buffer.nextUnmask(),0);
		TSM_ASSERT_EQUALS("反向非标记查找",buffer.lastUnmask(),buffer.size()-1);
	}

	void testMask(){
		dm::TSMagicedBuffer<int> buffer;
		buffer.init();

		// 对没一位进行测试
		for( int i=0;i<buffer.size();++i ){
			buffer.mask(i);
			TSM_ASSERT_EQUALS("初始成员数量",buffer.count(),1);
			TSM_ASSERT_EQUALS("无标记判断",buffer.ifNoneMasked(),false);
			TSM_ASSERT_EQUALS("全标记判断",buffer.ifAllMasked(),false);
			for( int j=0;j<buffer.size();++j ){
				TSM_ASSERT_EQUALS("按索引标记判断",buffer.ifMasked(j),i==j);
			}

			TSM_ASSERT_EQUALS("正向标记查找",buffer.nextMasked(),i);
			TSM_ASSERT_EQUALS("反向标记查找",buffer.lastMasked(),i);

			buffer.unmask(i);
			TSM_ASSERT_EQUALS("初始成员数量",buffer.count(),0);
			TSM_ASSERT_EQUALS("无标记判断",buffer.ifNoneMasked(),true);
			TSM_ASSERT_EQUALS("全标记判断",buffer.ifAllMasked(),false);
			for( int i=0;i<buffer.size();++i ){
				TSM_ASSERT_EQUALS("按索引标记判断",buffer.ifMasked(i),false);
			}
			TSM_ASSERT_EQUALS("正向标记查找",buffer.nextMasked(),buffer.size());
			TSM_ASSERT_EQUALS("反向标记查找",buffer.lastMasked(),buffer.size());
			TSM_ASSERT_EQUALS("正向非标记查找",buffer.nextUnmask(),0);
			TSM_ASSERT_EQUALS("反向非标记查找",buffer.lastUnmask(),buffer.size()-1);
		}
	}
};

#endif /* DM_PLAT_MISC_TEST_MAGICED_BUFFER_HXX_ */

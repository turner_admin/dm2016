/*
 * ptrchars.cpp
 *
 *  Created on: 2023年4月23日
 *      Author: Dylan.Gao
 */

#include <dm/ptrchars.hpp>
#include <dm/string/stringref.hpp>

namespace dm{

CPtrChars::CPtrChars( char* p ):m_p(p){
}

CPtrChars::~CPtrChars(){
	if( m_p )
		delete [] m_p;
}

CPtrChars& CPtrChars::operator =( char* p ){
	if( m_p )
		delete [] m_p;
	m_p = p;
	return *this;
}

CPtrChars& CPtrChars::operator=( const char* p ){
	dm::string::CStringRef ref(p);
	ref.dumpAutoMalloc(&m_p);
	return *this;
}

void CPtrChars::clear(){
	if( m_p ){
		delete [] m_p;
		m_p = 0;
	}
}
}

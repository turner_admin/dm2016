/*
 * timestamp.cpp
 *
 *  Created on: 2020-11-16
 *      Author: Dylan.Gao
 */

#include <dm/export.hpp>

#define DM_API_MISC DM_API_EXPORT

#include <dm/timestamp.hpp>

#ifdef UNIX
#include <sys/time.h>
#else
#include <sys/timeb.h>
#endif

namespace dm {

#ifdef UNIX
	CTimeStamp CTimeStamp::cur() {
		struct timeval tv;
		gettimeofday(&tv, 0);
		return CTimeStamp(tv.tv_sec, tv.tv_usec);
	}
#else
	CTimeStamp CTimeStamp::cur() {
		timeb tb;
		ftime(&tb);

		return CTimeStamp(tb.time, tb.millitm*1000);
	}
#endif

}
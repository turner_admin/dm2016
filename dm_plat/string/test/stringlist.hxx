﻿/*
 * stringlist.hxx
 *
 *  Created on: 2023年10月14日
 *      Author: Dylan.Gao
 */

#ifndef DM_PLAT_STRING_TEST_STRINGLIST_HXX_
#define DM_PLAT_STRING_TEST_STRINGLIST_HXX_

#include <cxxtest/TestSuite.h>
#include <dm/string/stringlist.hpp>

class CTestStringList:public CxxTest::TestSuite{
public:
	void testTotalSize(){
		dm::string::CStringList strList;

		TSM_ASSERT_EQUALS("空长度",strList.totalSize(),0);

		strList.append("hello");
		TSM_ASSERT_EQUALS("单记录长度",strList.totalSize(),5);

		strList.append("hello");
		strList.append("hello");

		TSM_ASSERT_EQUALS("多记录长度",strList.totalSize(),15);
	}

	void testToString(){
		dm::string::CStringList strList;
		TSM_ASSERT_EQUALS("空串",strList.toString().len(),0);

		strList.append("hello");
		TSM_ASSERT("单串",strList.toString()=="hello");

		strList.append("hello");
		strList.append("hello");
		TSM_ASSERT("多串",strList.toString()=="hellohellohello");

		TSM_ASSERT("多串",strList.toString('-')=="hello-hello-hello");

		TSM_ASSERT("多串",strList.toString("->")=="hello->hello->hello");
	}
};

#endif /* DM_PLAT_STRING_TEST_STRINGLIST_HXX_ */

﻿/*
 * stringref.hxx
 *
 *  Created on: 2020年7月30日
 *      Author: work
 */

#ifndef STRING_TEST_STRINGREF_HXX_
#define STRING_TEST_STRINGREF_HXX_

#include <cxxtest/TestSuite.h>
#include <dm/string/stringref.hpp>

class CTestStringRef:public CxxTest::TestSuite{
public:
	void testDefault(){
		dm::string::CStringRef sr("hello");
		dm::string::CStringRef sr1("hello1");
		dm::string::CStringRef sr2("kk");

		TSM_ASSERT("比较==",!(sr==sr1));
		TSM_ASSERT("比较==",!(sr==sr2));
		TSM_ASSERT("比较!=",sr!=sr1);
		TSM_ASSERT("比较==",sr!=sr2);

		TSM_ASSERT("比较>",sr1>sr);
		TSM_ASSERT("比较>",!(sr>sr1));
		TSM_ASSERT("比较>",!(sr>sr));
		TSM_ASSERT("比较>",sr2>sr);

		TSM_ASSERT("比较>=",sr1>=sr);
		TSM_ASSERT("比较>=",!(sr>=sr1));

		TSM_ASSERT("比较>=",sr1>=sr1);
		TSM_ASSERT("比较>=",sr2>=sr1);

		TSM_ASSERT("比较<",sr<sr1);
		TSM_ASSERT("比较<",!(sr1<sr));
		TSM_ASSERT("比较<",!(sr1<sr1));
		TSM_ASSERT("比较<",sr<sr2);

		TSM_ASSERT("比较<=",sr<=sr1);
		TSM_ASSERT("比较<=",!(sr1<=sr));
		TSM_ASSERT("比较<=",sr1<=sr1);
		TSM_ASSERT("比较<=",sr<sr2);

		sr = sr1;
		TSM_ASSERT("比较<=", sr <= sr1);
		TSM_ASSERT("比较<=", sr1 <= sr);
		TSM_ASSERT("比较<=", !(sr < sr1));
	}

};



#endif /* STRING_TEST_STRINGREF_HXX_ */

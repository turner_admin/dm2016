﻿/*
 * signal_handler.hxx
 *
 *  Created on: 2020年11月18日
 *      Author: Dylan.Gao
 */

#ifndef _TEST_OS_COMMON_SIGNAL_HANDLER_HXX_
#define _TEST_OS_COMMON_SIGNAL_HANDLER_HXX_

#include <cxxtest/TestSuite.h>
#include <dm/os/signal_handler.hpp>
#include <iostream>

#ifdef WIN32
#include <windows.h>
#endif

class CTestOsSignalHandler:public CxxTest::TestSuite{
public:
	void testDefault(){
		dm::os::CSignalHandler& sh = dm::os::CSignalHandler::ins();

		TSM_ASSERT("默认无信号",!sh.ifSignal());
		TS_WARN("等待信号");
		while (!sh.ifSignal());
		TS_WARN("捕获到信号");
		std::cout <<"sig:"<<sh.signal()<<" ctl:"<<sh.ctlType()<<std::endl;

#ifdef WIN32
		Sleep(4000);
#endif
		sh.clearSignals();
		TSM_ASSERT("清除信号", !sh.ifSignal());
	}

};



#endif /* STRING_TEST_STRINGREF_HXX_ */

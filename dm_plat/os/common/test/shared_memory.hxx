/*
 * shared_memory.hxx
 *
 *  Created on: 2023年9月22日
 *      Author: Dylan.Gao
 */

#ifndef DM_PLAT_OS_COMMON_TEST_SHARED_MEMORY_HXX_
#define DM_PLAT_OS_COMMON_TEST_SHARED_MEMORY_HXX_

#include <cxxtest/TestSuite.h>
#include <dm/os/shared_memory.hpp>
#include <dm/scoped_ptr.hpp>

class TestSharedMemory:public CxxTest::TestSuite{
public:
	void testDefault(){
		struct SInfo{
			int a;
			long b;
			float c;
			double d;

			unsigned char v[100];
		};

		volatile SInfo* pvInfo;

		dm::TScopedPtr<boost::interprocess::mapped_region> region (dm::os::getSharedMemory<SInfo>("testShareMemory",dm::os::CUnrestrictedPermissions(), &pvInfo));
		char info[128];
		std::sprintf(info,"页面大小%l,起始地址:%p",boost::interprocess::mapped_region::get_page_size(),const_cast<SInfo*>(pvInfo));
		TS_TRACE(info);
		TSM_ASSERT("测试分配内存",region);
		TS_TRACE("设置数据");
		pvInfo->a = 1;
		pvInfo->b = 2;
		pvInfo->c = 3;
		pvInfo->d = 4;
		for( int i=0;i<100;++i )
			pvInfo->v[i] = i;

		region = dm::os::getSharedMemory<SInfo>("testShareMemory1",dm::os::CUnrestrictedPermissions(), &pvInfo);
		std::sprintf(info,"页面大小0x%X,起始地址:0x%p",boost::interprocess::mapped_region::get_page_size(),pvInfo);
		TS_TRACE(info);

		TSM_ASSERT("测试分配内存",region);
		TS_TRACE("设置数据1");
		pvInfo->a = 1;
		pvInfo->b = 2;
		pvInfo->c = 3;
		pvInfo->d = 4;
		for( int i=0;i<100;++i )
			pvInfo->v[i] = i;
	}
};

#endif /* DM_PLAT_OS_COMMON_TEST_SHARED_MEMORY_HXX_ */

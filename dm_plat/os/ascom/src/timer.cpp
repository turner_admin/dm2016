﻿/*
 * timer.cpp
 *
 *  Created on: 2016年12月19日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_OS_ASCOM DM_API_EXPORT

#include <dm/os/ascom/timer.hpp>
#include <dm/os/log/logger.hpp>

namespace dm{
namespace os{
namespace ascom{

static const char* logModule = "CTimer.ascom.os.dm";

CTimer::CTimer( com::ios_t& ios ):com::CTimer(ios){
	log().debug(THISMODULE "创建对象");
}

CTimer::~CTimer(){
	log().debug(THISMODULE "销毁对象");
}

void CTimer::on_timeout(){
	sig_timeout();
}

}
}
}



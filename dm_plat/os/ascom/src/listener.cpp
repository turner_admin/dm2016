﻿/*
 * listener.cpp
 *
 *  Created on: 2017年3月8日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_OS_ASCOM DM_API_EXPORT

#include <dm/os/ascom/listener.hpp>
#include <dm/os/log/logger.hpp>

namespace dm{
namespace os{
namespace ascom{

static const char* logModule = "CListener.ascom.os.dm";

CListener::CListener():com::CListener(){
	log().debug(THISMODULE "创建对象");
}

CListener::~CListener(){
	log().debug(THISMODULE "销毁对象");
}

void CListener::onNewConnection( com::CListener::device_t* connect ){
	log().debug(THISMODULE "发送信号");
	sig_newConnect( connect );
}

}
}
}



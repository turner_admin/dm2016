﻿/*
 * device.cpp
 *
 *  Created on: 2017年2月23日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_OS_ASCOM DM_API_EXPORT

#include <dm/os/ascom/device.hpp>
#include <dm/os/log/logger.hpp>

namespace dm{
namespace os{
namespace ascom{

static const char* logModule = "CDevice.ascom.os.dm";

CDevice::CDevice( const size_t& rxBufSize ):
		com::CDevice(rxBufSize){
	log().debug(THISMODULE "创建对象");
}

CDevice::~CDevice(){
	log().debug(THISMODULE "销毁对象");
}

bool CDevice::onConnected(){
	com::CDevice::onConnected();
	sig_connected();

	return true;
}

void CDevice::onConnectFail(){
	log().debug( THISMODULE "发送信号");
	sig_connectFail();
}

void CDevice::onReceived( const dm::uint8* buf,const size_t& len ){
	sig_received(buf,len);
}

void CDevice::onReceiveFail(){
	log().debug( THISMODULE "发送信号");
	sig_receiveFail();
}

void CDevice::onSendSuccess(){
	sig_sendSuccess();
}

void CDevice::onSendFail(){
	log().debug( THISMODULE "发送信号");
	sig_sendFail();
}

void CDevice::onDisconnected(){
	log().debug( THISMODULE "发送信号");
	sig_disconnected();
}

void CDevice::onDisconnectFail(){
	log().debug( THISMODULE "发送信号");
	sig_disconnectFail();
}

}
}
}

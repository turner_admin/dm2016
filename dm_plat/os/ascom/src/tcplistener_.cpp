﻿/*
 * tcplistener_.cpp
 *
 *  Created on: 2017年3月2日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_OS_ASCOM DM_API_EXPORT

#include <dm/os/ascom/tcplistener_.hpp>
#include <dm/os/log/logger.hpp>
#include <dm/os/ascom/tcpconnecteddevice.hpp>

namespace dm{
namespace os{
namespace ascom{

static const char* logModule = "CTcpListener_.ascom.os.dm";

CTcpListener_::CTcpListener_( com::ios_t& ios ):com::CTcpListener(ios){
	log().debug(THISMODULE "创建对象");
}

CTcpListener_::~CTcpListener_(){
	log().debug(THISMODULE "销毁对象");
}

CTcpListener_::device_t* CTcpListener_::startAccept( const size_t& bufSize ){
	CTcpConnectedDevice* con = new CTcpConnectedDevice(*m_ios,bufSize);
	log().debug(THISMODULE "申请一个新的设备");

	try{
		m_acceptor.async_accept(con->m_dev,m_peer,
				boost::bind(&CTcpListener_::handler_connection,this,boost::asio::placeholders::error));

		return con;
	}catch( std::exception& e ){
		log().error(THISMODULE "异步接受连接失败:%s",e.what());
		delete con;
		return 0;
	}
}

}
}
}



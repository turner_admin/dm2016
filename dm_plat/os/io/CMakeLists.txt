cmake_minimum_required(VERSION 3.10)

cmake_policy(SET CMP0074 NEW)
find_package(Boost REQUIRED)

include_directories(${Boost_INCLUDE_DIRS})

aux_source_directory(src SRC)

if(NOT (CMAKE_SYSTEM_NAME MATCHES "Windows") )
	aux_source_directory(unix_src UNIX_SRC)
endif()

add_library(osio SHARED ${SRC} ${UNIX_SRC}) 

target_link_libraries(osio misc tinyxml json ${Boost_LIBRARIES})

set_target_properties(osio PROPERTIES WIN32_MANIFEST "NO")

install(TARGETS osio LIBRARY)
/*
 * tcpconnect.cpp
 *
 *  Created on: 2017年7月15日
 *      Author: work
 */

#include <dm/os/io/tcpconnect.hpp>

namespace dm{
namespace os{
namespace io{

CTcpConnect::type_t CTcpConnect::type()const{
	return com::CAddress::TcpConnect;
}

}
}
}

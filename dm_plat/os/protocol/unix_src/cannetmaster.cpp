﻿/*
 * cannetmaster.cpp
 *
 *  Created on: 2017年4月1日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_OS_PROTOCOL DM_API_EXPORT

#include <dm/os/protocol/cannetmaster.hpp>
#include <dm/os/log/logger.hpp>

namespace dm{
namespace os{
namespace protocol{

static const char* logModule = "CCannetMaster.protocol.os.dm";

CCannetMaster::CCannetMaster():dm::protocol::CProtocol(),
		dm::protocol::CCannetMaster(),dm::os::protocol::CProtocolMasters(){
	log().debug(THISMODULE "创建对象");
}

CCannetMaster::~CCannetMaster(){
	log().debug(THISMODULE "销毁对象");
}

}
}
}

﻿/*
 * protocolmaster.cpp
 *
 *  Created on: 2017年3月18日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_OS_PROTOCOL DM_API_EXPORT

#include <dm/os/protocol/protocolmaster.hpp>
#include <dm/scada/remotectlmgr.hpp>
#include <dm/scada/parametermgr.hpp>
#include <dm/scada/statusmgr.hpp>
#include <dm/scada/discretemgr.hpp>
#include <dm/scada/measuremgr.hpp>
#include <dm/scada/cumulantmgr.hpp>
#include <dm/scada/actionmgr.hpp>
#include <dm/scada/scadainfo.hpp>
#include <dm/os/log/logger.hpp>

namespace dm{
namespace os{
namespace protocol{

static const char* logModule = "CProtocolMaster.protocol.os.dm";

using namespace dm::os::msg;

CProtocolMaster::CProtocolMaster():CProtocolMasterBase(),m_devAgent(NULL){
	log().debug(THISMODULE "创建对象");
	enableAnswerMsgCheck(true);
	enableRequestMsgCheck(true);
}

CProtocolMaster::~CProtocolMaster(){
	log().debug(THISMODULE "销毁对象");
	if( m_devAgent )
		delete m_devAgent;
}

bool CProtocolMaster::setDevice( const char* name,const dm::scada::CDeviceAgent::SSignalsSize* size ){
	if( m_devAgent==NULL )
		m_devAgent = new dm::scada::CDeviceAgent;

	if( size ){
		return m_devAgent->setByName(name,*size);
	}else
		return m_devAgent->setByName(name);
}

bool CProtocolMaster::setDevice( const int& id,const dm::scada::CDeviceAgent::SSignalsSize* size ){
	if( m_devAgent==NULL )
		m_devAgent = new dm::scada::CDeviceAgent;

	if( size ){
		return m_devAgent->setById(id,*size);
	}else
		return m_devAgent->setById(id);
}

bool CProtocolMaster::isLocalRemoteCtl( const dm::msg::index_t& idx )const{
	if( m_devAgent )
		return m_devAgent->isMyRemoteCtl(idx);
	else
		return dm::scada::CRemoteCtlMgr::ins().size()>idx && idx>=0;
}

bool CProtocolMaster::isLocalPara( const dm::msg::index_t& idx )const{
	if( m_devAgent )
		return m_devAgent->isMyParameter(idx);
	else
		return dm::scada::CParameterMgr::ins().size()>idx && idx>=0;
}

void CProtocolMaster::onLinkSettled( const ts_t& ts,const rts_t& rts ){
	if( m_devAgent && m_devAgent->state() )
		m_devAgent->state()->updateState(dm::scada::CDeviceState::ESComConnted);

	CProtocolMasterBase::onLinkSettled(ts,rts);
}

void CProtocolMaster::onLinkClosed( const ts_t& ts,const rts_t& rts ){
	if( m_devAgent && m_devAgent->state() )
		m_devAgent->state()->updateState(dm::scada::CDeviceState::ESComDiscon);

	CProtocolMasterBase::onLinkClosed(ts,rts);
}

void CProtocolMaster::updateRecvTime( dm::uint32 bytes,const rts_t& rts ){
	if( m_devAgent && m_devAgent->state() ){
		m_devAgent->state()->rxed(bytes,rts);
		m_devAgent->state()->updateState(dm::scada::CDeviceState::ESComConnted);
	}

	CProtocolMasterBase::updateRecvTime(bytes,rts);
}

void CProtocolMaster::updateSendTime( dm::uint32 bytes,const rts_t& rts ){
	if( m_devAgent && m_devAgent->state() )
		m_devAgent->state()->txed(bytes,rts);
	CProtocolMasterBase::updateSendTime(bytes,rts);
}

bool CProtocolMaster::updateStatus( const index_t& status,const dm::scada::CStatus& value,const dm::scada::EDataSource& ds,const dm::CTimeStamp& ts ){
	if( m_devAgent )
		return m_devAgent->updateStatus(status,value,ds,ts);
	else
		return dm::scada::CStatusMgr::ins().update(status,value,ds,ts);
}

bool CProtocolMaster::updateDiscrete( const index_t& discrete,const dm::scada::CDiscrete& value,const dm::scada::EDataSource& ds,const dm::CTimeStamp& ts ){
	if( m_devAgent )
		return m_devAgent->updateDiscrete(discrete,value,ds,ts);
	else
		return dm::scada::CDiscreteMgr::ins().update(discrete,value,ds,ts);
}

bool CProtocolMaster::updateMeasure( const index_t& measure,const dm::scada::CMeasure::raw_t& raw,const dm::scada::EDataSource& ds,const dm::CTimeStamp& ts ){
	if( m_devAgent )
		return m_devAgent->updateMeasure(measure,raw,ds,ts);
	else
		return dm::scada::CMeasureMgr::ins().update(measure,raw,ds,ts);
}

bool CProtocolMaster::updateCumulant( const index_t& cumulant,const dm::scada::CCumulant::raw_t& raw,const dm::scada::EDataSource& ds,const dm::CTimeStamp& ts ){
	if( m_devAgent )
		return m_devAgent->updateCumulant(cumulant,raw,ds,ts);
	else
		return dm::scada::CCumulantMgr::ins().update(cumulant,raw,ds,ts);
}

bool CProtocolMaster::genAction( const index_t& action,const dm::scada::CAction::value_t& value,const dm::CTimeStamp& ts ){
	if( m_devAgent )
		return m_devAgent->genAction(action,value,ts);
	else
		return dm::scada::CActionMgr::ins().update(action,value,ts);
}

CProtocolMaster::index_t CProtocolMaster::getLocalStatusIndex( const index_t& idx )const{
	if( m_devAgent )
		return m_devAgent->getLocalStatusIndex(idx);
	else
		return idx>=dm::scada::CStatusMgr::ins().size()?-1:idx;
}

CProtocolMaster::index_t CProtocolMaster::getLocalDiscreteIndex( const index_t& idx )const{
	if( m_devAgent )
		return m_devAgent->getLocalDiscreteIndex(idx);
	else
		return idx>=dm::scada::CDiscreteMgr::ins().size()?-1:idx;
}

CProtocolMaster::index_t CProtocolMaster::getLocalMeasureIndex( const index_t& idx )const{
	if( m_devAgent )
		return m_devAgent->getLocalMeasureIndex(idx);
	else
		return idx>=dm::scada::CMeasureMgr::ins().size()?-1:idx;
}

CProtocolMaster::index_t CProtocolMaster::getLocalCumulantIndex( const index_t& idx )const{
	if( m_devAgent )
		return m_devAgent->getLocalCumulantIndex(idx);
	else
		return idx>=dm::scada::CCumulantMgr::ins().size()?-1:idx;
}

CProtocolMaster::index_t CProtocolMaster::getLocalRemoteControlIndex( const index_t& idx )const{
	if( m_devAgent )
		return m_devAgent->getLocalRemoteControlIndex(idx);
	else
		return idx>=dm::scada::CRemoteCtlMgr::ins().size()?-1:idx;
}

CProtocolMaster::index_t CProtocolMaster::getLocalParamterIndex( const index_t& idx )const{
	if( m_devAgent )
		return m_devAgent->getLocalParamterIndex(idx);
	else
		return idx>=dm::scada::CParameterMgr::ins().size()?-1:idx;
}

CProtocolMaster::index_t CProtocolMaster::getLocalActionIndex( const index_t& idx )const{
	if( m_devAgent )
		return m_devAgent->getLocalActionIndex(idx);
	else
		return idx>=dm::scada::CActionMgr::ins().size()?-1:idx;
}

CProtocolMaster::index_t CProtocolMaster::getStatusIndex( const index_t& idx )const{
	if( m_devAgent )
		return m_devAgent->getStatusIndex(idx);
	else
		return idx>=dm::scada::CStatusMgr::ins().size()?-1:idx;
}

CProtocolMaster::index_t CProtocolMaster::getDiscreteIndex( const index_t& idx )const{
	if( m_devAgent )
		return m_devAgent->getDiscreteIndex(idx);
	else
		return idx>=dm::scada::CDiscreteMgr::ins().size()?-1:idx;
}

CProtocolMaster::index_t CProtocolMaster::getMeasureIndex( const index_t& idx )const{
	if( m_devAgent )
		return m_devAgent->getMeasureIndex(idx);
	else
		return idx>=dm::scada::CMeasureMgr::ins().size()?-1:idx;
}

CProtocolMaster::index_t CProtocolMaster::getCumulantIndex( const index_t& idx )const{
	if( m_devAgent )
		return m_devAgent->getCumulantIndex(idx);
	else
		return idx>=dm::scada::CCumulantMgr::ins().size()?-1:idx;
}

CProtocolMaster::index_t CProtocolMaster::getRemoteControlIndex( const index_t& idx )const{
	if( m_devAgent )
		return m_devAgent->getRemoteControlIndex(idx);
	else
		return idx>=dm::scada::CRemoteCtlMgr::ins().size()?-1:idx;
}

CProtocolMaster::index_t CProtocolMaster::getParameterIndex( const index_t& idx )const{
	if( m_devAgent )
		return m_devAgent->getParameterIndex(idx);
	else
		return idx>=dm::scada::CParameterMgr::ins().size()?-1:idx;
}

CProtocolMaster::index_t CProtocolMaster::getActionIndex( const index_t& idx )const{
	if( m_devAgent )
		return m_devAgent->getActionIndex(idx);
	else
		return idx>=dm::scada::CActionMgr::ins().size()?-1:idx;
}

CProtocolMaster::index_t CProtocolMaster::sizeOfStatus()const{
	if( m_devAgent )
		return m_devAgent->info()->sizeOfStatus;
	const dm::scada::CScada& scada = dm::scada::CScada::ins();
	return scada.info()->statusCount();
}

CProtocolMaster::index_t CProtocolMaster::sizeOfDiscrete()const{
	if( m_devAgent )
		return m_devAgent->info()->sizeOfDiscrete;
	const dm::scada::CScada& scada = dm::scada::CScada::ins();
	return scada.info()->discreteCount();
}

CProtocolMaster::index_t CProtocolMaster::sizeOfMeasure()const{
	if( m_devAgent )
		return m_devAgent->info()->sizeOfMeasure;
	const dm::scada::CScada& scada = dm::scada::CScada::ins();
	return scada.info()->measureCount();
}

CProtocolMaster::index_t CProtocolMaster::sizeOfCumulant()const{
	if( m_devAgent )
		return m_devAgent->info()->sizeOfCumulant;
	const dm::scada::CScada& scada = dm::scada::CScada::ins();
	return scada.info()->cumulantCount();
}

CProtocolMaster::index_t CProtocolMaster::sizeOfRmtctl()const{
	if( m_devAgent )
		return m_devAgent->info()->sizeOfTeleCtl;
	const dm::scada::CScada& scada = dm::scada::CScada::ins();
	return scada.info()->remoteCtlCount();
}

CProtocolMaster::index_t CProtocolMaster::sizeOfParameter()const{
	if( m_devAgent )
		return m_devAgent->info()->sizeOfPara;
	const dm::scada::CScada& scada = dm::scada::CScada::ins();
	return scada.info()->parameterCount();
}

CProtocolMaster::index_t CProtocolMaster::sizeOfAction()const{
	if( m_devAgent )
		return m_devAgent->info()->sizeOfAction;

	const dm::scada::CScada& scada = dm::scada::CScada::ins();
	return scada.info()->actionCount();
}

}
}
}



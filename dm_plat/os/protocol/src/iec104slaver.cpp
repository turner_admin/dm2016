﻿/*
 * iec104slaver.cpp
 *
 *  Created on: 2017年3月2日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_OS_PROTOCOL DM_API_EXPORT

#include <dm/os/protocol/iec104slaver.hpp>
#include <dm/protocol/frame104ldu.hpp>
#include <dm/os/log/logger.hpp>

namespace dm{
namespace os{
namespace protocol{

static const char* logModule = "CIec104Slaver.protocol.os.dm";

CIec104Slaver::CIec104Slaver():dm::protocol::CIec104Slaver(),dm::os::protocol::CProtocolSlaver(){
	log().debug(THISMODULE "创建对象");
}

CIec104Slaver::~CIec104Slaver(){
	log().debug(THISMODULE "销毁对象");
}

void CIec104Slaver::reset( const ts_t& ts,const rts_t& rts ){
	dm::protocol::CIec104Slaver::reset(ts,rts);
	dm::os::protocol::CProtocolSlaver::reset(ts,rts);
}

}
}
}



﻿/*
 * protocollistenor.cpp
 *
 *  Created on: 2017年3月7日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_OS_PROTOCOL DM_API_EXPORT

#include <dm/os/protocol/protocollistenor.hpp>

#include <dm/os/log/logger.hpp>
#include <dm/os/protocol/protocolcontrolor.hpp>

namespace dm{
namespace os{
namespace protocol{

static const char* logModule = "CProtocolListenor.protocol.os.dm";

CProtocolListenor::CProtocolListenor():
	m_listenor(0),m_controlors(),m_bufSize(1024){
	log().debug(THISMODULE "创建对象");
}

CProtocolListenor::~CProtocolListenor(){
	log().debug(THISMODULE "销毁对象");

	if( m_listenor!=0 ){
		log().debug(THISMODULE "销毁监听器");
		delete m_listenor;
	}
	freeAllProtocolControlor();
}

void CProtocolListenor::setListenor( listenor_t* listenor ){
	if( m_listenor!=0 ){
		log().debug(THISMODULE "释放监听器");
		delete m_listenor;
	}

	m_listenor = listenor;
	if( m_listenor!=0 ){
		m_listenor->sig_newConnect.connect( boost::bind(&CProtocolListenor::onNewConnect,this, _1) );
	}
}

bool CProtocolListenor::addProtocolControlor( CProtocolControlor* controlor ){
	if( controlor==0 ){
		log().warnning(THISMODULE "不能添加空控制器");
		return false;
	}

	m_controlors.push_back( controlor );
	log().debug( THISMODULE "增加规约控制器%d",m_controlors.size());

	controlor->sig_devDisconnected.connect( boost::bind(&CProtocolListenor::onProtocolControlorDisconnected,this,controlor) );
	controlor->setAutoConnect(false);
	return true;
}

void CProtocolListenor::freeAllProtocolControlor(){
	int i = 0;
	for( controlor_list_t::iterator it=m_controlors.begin();it!=m_controlors.end();++it,++i ){
		if( *it!=0 ){
			log().debug(THISMODULE "释放规约控制器%d",i);
			delete *it;
		}
	}

	m_controlors.clear();
}

/**
 * 处理接收到新的连接
 * @param connect
 */
void CProtocolListenor::onNewConnect( connect_t connect ){
	bool dis = false;	// 是否被分配

	controlor_list_t::iterator it = m_controlors.begin();
	for(;it!=m_controlors.end();++it ){
		if( (*it)->isDeviceSetted()==false ){
			log().debug(THISMODULE "将新连接分配给控制器%d",it-m_controlors.begin());
			(*it)->setDevice(dynamic_cast<ascom::CDevice*>(connect));
			dis = true;
			break;
		}
	}

	if( !dis ){
		log().warnning(THISMODULE "新连接没有被分配，将释放掉");
		delete connect;
	}else{
		++it;
		for(;it!=m_controlors.end();++it ){
			if( (*it)->isDeviceSetted()==false ){
				log().debug(THISMODULE "当前至少控制器%d空闲，开启接收",it-m_controlors.begin());
				if( !readyForAcceptNewConnection() ){
					log().warnning(THISMODULE "开启接收失败");
					return;
				}
			}
		}
	}
}

void CProtocolListenor::onProtocolControlorDisconnected( CProtocolControlor* protocolControlor ){
	log().debug( THISMODULE "有一个控制器断开连接");

	// 移除该规约控制器的通信设备
	protocolControlor->setDevice(0);	// 控制器负责通信设备资源的释放

	if( readyForAcceptNewConnection() )
		log().debug( THISMODULE "开始新的监听");
	else
		log().warnning( THISMODULE "开始新的监听失败");
}

bool CProtocolListenor::startListen(){
	if( m_listenor==0 || m_controlors.size()<=0 )
		return false;

	return m_listenor->startListen();
}

bool CProtocolListenor::stopListen(){
	if( m_listenor!=0 )
		return m_listenor->stopListen();
	return false;
}

/**
 * 获取一个空闲的控制器
 * @return
 */
CProtocolListenor::controlor_t CProtocolListenor::getAnIdleControlor(){
	for( controlor_list_t::iterator it=m_controlors.begin();it!=m_controlors.end();++it ){
		if( (*it)->isDeviceSetted() )
			return *it;
	}

	return 0;
}

bool CProtocolListenor::readyForAcceptNewConnection(){
	if( m_listenor!=0 )
		return m_listenor->readyForAccept(m_bufSize);
	return false;
}
}
}
}



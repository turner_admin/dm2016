﻿/*
 * telegram.cpp
 *
 *  Created on: 2017年3月3日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_OS_PROTOCOL DM_API_EXPORT

#include <dm/os/protocol/telegram.hpp>
#include <cstring>

namespace dm{
namespace os{
namespace protocol{

CTelegram::CTelegram():CTelegramMgr(){
#ifdef WIN32
	m_pid = GetCurrentProcessId();
#else
	m_pid = getpid();
#endif
}

CTelegram& CTelegram::ins(){
	static CTelegram i;
	return i;
}

void CTelegram::resetPid(){
#ifdef WIN32
	m_pid = GetCurrentProcessId();
#else
	m_pid = getpid();
#endif
}

void CTelegram::push( const char* protocol,const info_t::EType& type,const dm::uint8* gram,const dm::uint& len ){
	info_t info;
	info.pid = m_pid;
	info.protocol = protocol;
	info.type = type;
	info.ts = dm::CTimeStamp::cur();

	info.len = len;
	if( info.len>0 ){
		if( info.len>info_t::LenOfGram )
			std::memcpy(info.gram,gram,info_t::LenOfGram);
		else
			std::memcpy(info.gram,gram,info.len);
	}

	m_q->push(info);
}

}
}
}



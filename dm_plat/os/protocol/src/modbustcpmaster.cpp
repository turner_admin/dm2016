﻿/*
 * modbustcpmaster.cpp
 *
 *  Created on: 2017年10月30日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_OS_PROTOCOL DM_API_EXPORT

#include <dm/os/protocol/modbustcpmaster.hpp>
#include <dm/os/log/logger.hpp>

namespace dm{
namespace os{
namespace protocol{

static const char* logModule = "CModbusTcpMaster.protocol.os.dm";

CModbusTcpMaster::CModbusTcpMaster():dm::protocol::CProtocol(),
		dm::protocol::CModbusTcpMaster(),dm::os::protocol::CProtocolMaster(){
	log().debug(THISMODULE "创建对象");
}

CModbusTcpMaster::~CModbusTcpMaster(){
	log().debug(THISMODULE "销毁对象");
}

void CModbusTcpMaster::reset( const ts_t& ts,const rts_t& rts ){
	dm::protocol::CModbusTcpMaster::reset(ts,rts);
	dm::os::protocol::CProtocolMaster::reset(ts,rts);
}

CModbusTcpMaster::EAction CModbusTcpMaster::taskEnd_remoteControl( const ts_t& ts,const rts_t& rts ){
	EAction action = dm::os::protocol::CProtocolMaster::taskEnd_remoteControl(ts,rts);
	enableRequestMsgCheck(false);
	return action;
}

CModbusTcpMaster::EAction CModbusTcpMaster::taskEnd_parameters( const ts_t& ts,const rts_t& rts ){
	EAction action = dm::os::protocol::CProtocolMaster::taskEnd_parameters(ts,rts);
	enableRequestMsgCheck(false);
	return action;
}

CModbusTcpMaster::EAction CModbusTcpMaster::taskEnd_call( const ts_t& ts,const rts_t& rts ){
	EAction action = dm::os::protocol::CProtocolMaster::taskEnd_call(ts,rts);
	enableRequestMsgCheck(true);
	return action;
}

}
}
}



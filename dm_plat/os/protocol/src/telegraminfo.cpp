﻿/*
 * telegraminfo.cpp
 *
 *  Created on: 2017年3月3日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_OS_PROTOCOL DM_API_EXPORT

#include <dm/os/protocol/telegraminfo.hpp>

namespace dm{
namespace os{
namespace protocol{

CTelegramInfo::CTelegramInfo():pid(0),ts(),type(Rx),protocol(),len(0){
}

CTelegramInfo::CTelegramInfo( const CTelegramInfo& info ):
		pid(info.pid),ts(info.ts),type(info.type),protocol(info.protocol),len(info.len){
	if( len>LenOfGram )
		len = LenOfGram;

	std::memcpy(gram,info.gram,len);
}

CTelegramInfo& CTelegramInfo::operator =( const CTelegramInfo& info ){
	pid = info.pid;
	ts = info.ts;
	type = info.type;
	protocol = info.protocol;
	len = info.len;
	if( len>LenOfGram )
		len = LenOfGram;

	std::memcpy(gram,info.gram,len);

	return *this;
}
}
}
}

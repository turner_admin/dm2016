﻿/*
 * telegrammgr.cpp
 *
 *  Created on: 2017年3月3日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_OS_PROTOCOL DM_API_EXPORT

#include <boost/interprocess/managed_shared_memory.hpp>
#include <dm/os/protocol/telegrammgr.hpp>
#include <iostream>

namespace dm{
namespace os{
namespace protocol{

using namespace boost::interprocess;

CTelegramMgr::CTelegramMgr(){
	try{
		permissions p;
		p.set_unrestricted();
		static managed_shared_memory s(open_or_create,"dmtelegram",sizeof(queue_t)+1024,0,p);
		m_q = s.find_or_construct<queue_t>(unique_instance)();
	}catch( interprocess_exception& e ){
		std::cerr <<__FILE__<<e.what();
	}
}

void CTelegramMgr::remove(){
	shared_memory_object::remove("dmtelegram");
}

}
}
}

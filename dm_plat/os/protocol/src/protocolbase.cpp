﻿/*
 * protocolbase.cpp
 *
 *  Created on: 2018年7月18日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_OS_PROTOCOL DM_API_EXPORT

#include <dm/os/protocol/protocolbase.hpp>
#include <dm/os/log/logger.hpp>

namespace dm{
namespace os{
namespace protocol{

static const char* logModule = "CProtocolBase.protocol.os.dm";

CProtocolBase::CProtocolBase():dm::protocol::CProtocol(),
		m_msgAgent(),m_checkMsgRequest(false),m_checkMsgAnswer(false),
		m_lastCheckRequest(false){
}

void CProtocolBase::reset( const ts_t& ts,const rts_t& rts ){
	log().info(THISMODULE "复位消息队列 清除10秒之前的数据");
	m_msgAgent.resetAnswer(10,ts);
	m_msgAgent.resetRequest(10,ts);

	CProtocol::reset(ts,rts);
}

/**
 * 检测消息
 * @param txFrame
 * @return
 */
CProtocolBase::EAction CProtocolBase::refresh_msg( const ts_t& ts,const rts_t& rts ){
	dm::os::msg::CMsgInfo msg;

	if( m_lastCheckRequest && m_checkMsgAnswer ){
		m_lastCheckRequest = false;
		while( m_msgAgent.tryGetAnswer(msg) ){
			switch( msg.getType() ){
			case dm::msg::Msg_SyncClock:
				if( !filterMsgAnswer_syncClock(msg) )
					return onMsgAnswer_syncClock(msg,ts,rts);
				break;

			case dm::msg::Msg_Call:
				if( !filterMsgAnswer_call(msg) )
					return onMsgAnswer_call(msg,ts,rts);
				break;

			case dm::msg::Msg_RemoteCtl:
				if( !filterMsgAnswer_rmtCtl(msg) )
					return onMsgAnswer_rmtCtl(msg,ts,rts);

				break;
			case dm::msg::Msg_Set:
				if( !filterMsgAnswer_set(msg) )
					return onMsgAnswer_set(msg,ts,rts);

				break;
			case dm::msg::Msg_Get:
				if( !filterMsgRequest_get(msg) )
					return onMsgAnswer_get(msg,ts,rts);

				break;
			case dm::msg::Msg_Update:
				if( !filterMsgAnswer_update(msg) )
					return onMsgAnswer_update(msg,ts,rts);

				break;
			case dm::msg::Msg_SyncData:
				if( !filterMsgAnswer_syncData(msg) )
					return onMsgAnswer_syncData(msg,ts,rts);

				break;
			case dm::msg::Msg_RemoteReset:
				if( !filterMsgAnswer_remoteReset(msg) )
					return onMsgAnswer_remoteReset(msg,ts,rts);

				break;
			default:
				log().warnning(THISMODULE "应答队列中，有未知消息类型%d",msg.getType());
			}
		}
	}

	if( m_checkMsgRequest ){
		m_lastCheckRequest = true;

		while( m_msgAgent.tryGetRequeset(msg) ){
			switch( msg.getType() ){
			case dm::msg::Msg_SyncClock:
				if( !filterMsgRequest_syncClock(msg) )
					return onMsgRequest_syncClock(msg,ts,rts);

				break;
			case dm::msg::Msg_Call:
				if( !filterMsgRequest_call(msg) )
					return onMsgRequest_call(msg,ts,rts);

				break;
			case dm::msg::Msg_RemoteCtl:
				if( !filterMsgRequest_rmtCtl(msg) )
					return onMsgRequest_rmtCtl(msg,ts,rts);

				break;
			case dm::msg::Msg_Set:
				if( !filterMsgRequest_set(msg) )
					return onMsgRequest_set(msg,ts,rts);

				break;
			case dm::msg::Msg_Get:
				if( !filterMsgRequest_get(msg) )
					return onMsgRequest_get(msg,ts,rts);

				break;
			case dm::msg::Msg_Update:
				if( !filterMsgRequest_update(msg) )
					return onMsgRequest_update(msg,ts,rts);

				break;
			case dm::msg::Msg_SyncData:
				if( !filterMsgRequest_syncData(msg) )
					return onMsgRequest_syncData(msg,ts,rts);

				break;
			case dm::msg::Msg_RemoteReset:
				if( !filterMsgRequest_remoteReset(msg) )
					return onMsgRequest_remoteReset(msg,ts,rts);

				break;
			default:
				log().warnning(THISMODULE "请求队列中，有未知消息类型%d",msg.getType());
			}
		}
	}

	return Action_Nothing;
}

}
}
}



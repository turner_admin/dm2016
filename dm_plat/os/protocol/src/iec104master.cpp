﻿/*
 * iec104master.cpp
 *
 *  Created on: 2017年3月9日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_OS_PROTOCOL DM_API_EXPORT

#include <dm/os/protocol/iec104master.hpp>
#include <dm/os/log/logger.hpp>

namespace dm{
namespace os{
namespace protocol{

static const char* logModule = "CIec104Master.protocol.os.dm";

CIec104Master::CIec104Master():dm::protocol::CProtocol(),
		dm::protocol::CIec104Master(),
		dm::os::protocol::CProtocolMaster(){
	log().debug(THISMODULE "创建对象");
}

CIec104Master::~CIec104Master(){
	log().debug(THISMODULE "销毁对象");
}

void CIec104Master::onLinkSettled( const ts_t& ts,const rts_t& rts ){
	dm::protocol::CIec104Master::onLinkSettled(ts,rts);
	dm::os::protocol::CProtocolMaster::onLinkSettled(ts,rts);
}

void CIec104Master::reset( const ts_t& ts,const rts_t& rts ){
	dm::protocol::CIec104Master::reset(ts,rts);
	dm::os::protocol::CProtocolMaster::reset(ts,rts);
}

}
}
}

/*
 * resultsqt_mysql.cpp
 *
 *  Created on: 2016年12月6日
 *      Author: work
 */

#include <dm/os/db/resultset_mysql.hpp>

namespace dm{
namespace os{

CResultSetMysql::CResultSetMysql():m_rst(NULL),m_st(NULL){

}

CResultSetMysql::~CResultSetMysql(){
	if( m_rst!=NULL )
		delete m_rst;
	if( m_st!=NULL )
		delete m_st;
}

int CResultSetMysql::size()const{
	if( m_rst!=NULL )
		return m_rst->rowsCount();

	return 0;
}

bool CResultSetMysql::next(){
	if( m_rst!=NULL )
		return m_rst->next();

	return false;
}

void CResultSetMysql::reset(){
	if( m_rst!=NULL )
		m_rst->beforeFirst();
}

bool CResultSetMysql::asBool( int col )const{
	return m_rst->getBoolean(col+1);
}

bool CResultSetMysql::asBool( const char* col )const{
	return m_rst->getBoolean(col);
}

int CResultSetMysql::asInt( int col )const{
	return m_rst->getInt(col+1);
}

int CResultSetMysql::asInt( const char* col )const{
	return m_rst->getInt(col);
}

dm::uint CResultSetMysql::asUint( int col )const{
	return m_rst->getUInt(col+1);
}

dm::uint CResultSetMysql::asUint( const char* col )const{
	return m_rst->getUInt(col);
}

dm::int64 CResultSetMysql::asInt64( int col )const{
	return m_rst->getInt64(col+1);
}

dm::int64 CResultSetMysql::asInt64( const char* col )const{
	return m_rst->getInt64(col);
}

double CResultSetMysql::asDouble( int col )const{
	return m_rst->getDouble(col+1);
}

double CResultSetMysql::asDouble( const char* col )const{
	return m_rst->getDouble(col);
}

std::string CResultSetMysql::asString( int col )const{
	return m_rst->getString(col+1);
}

std::string CResultSetMysql::asString( const char* col )const{
	return m_rst->getString(col);
}

bool CResultSetMysql::isNull( int col )const{
	return m_rst->isNull(col+1);
}

bool CResultSetMysql::isNull( const char* col )const{
	return m_rst->isNull(col);
}

void CResultSetMysql::set( sql::ResultSet* rst,sql::Statement* st ){
	free();
	m_rst = rst;
	m_st = st;
}

int CResultSetMysql::columnSize()const{
	if( m_rst!=NULL ){
		sql::ResultSetMetaData* rm = m_rst->getMetaData();
		if( rm )
			return rm->getColumnCount();
	}

	return 0;
}

std::string CResultSetMysql::valueString( int col )const{
	if( m_rst!=NULL )
		return m_rst->getString(col).c_str();
	return NULL;
}

std::string CResultSetMysql::valueString( const char* col )const{
	if( m_rst!=NULL )
		return m_rst->getString(col).c_str();
	return NULL;
}

void CResultSetMysql::free(){
	if( m_rst!=NULL )
		delete m_rst;
	if( m_st!=NULL )
		delete m_st;

	m_rst = NULL;
	m_st = NULL;
}
}
}



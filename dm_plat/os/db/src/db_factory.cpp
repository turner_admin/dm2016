﻿#include <dm/export.hpp>

#define DM_API_OS_DB DM_API_EXPORT

#ifndef NO_SQLITE
#include <dm/os/db/db_sqlite.hpp>
#endif

#ifndef NO_MYSQL
#include <dm/os/db/db_mysql.hpp>
#endif

#ifndef NO_POSTGRESQL
#include <dm/os/db/db_pg.hpp>
#endif

#include <dm/os/db/db.hpp>
#include <cstring>

namespace dm{
namespace os{

CDb* createDb( const char* dbName,const char* host,const char* engine,short port ){
    if( engine==0 ){
        #ifdef DB_POSTGRESQL
        return new CDbPg(dbName,host,port);
        #endif

        #ifdef DB_MYSQL
        return new CDbMysql(dbName,host,port);
        #endif

        return new CDbSqlite(dbName,host,port);
    }else{
        if( std::strcmp(engine,"postgresql")==0 ){
        #ifndef NO_POSTGRESQL
            return new CDbPg(dbName,host,port);
        #else
            return NULL;
        #endif
        }else if( std::strcmp(engine,"hgdb")==0 ){
		#ifndef NO_POSTGRESQL
			return new CDbPg(dbName,host,port,CDbPg::ExHgdb);
		#else
			return NULL;
		#endif
		}else if( std::strcmp(engine,"mysql")==0 ){
        #ifndef NO_MYSQL
            return new CDbMysql(dbName,host,port);
        #else
            return NULL;
        #endif
        }else if( std::strcmp(engine,"sqlite3")==0){
            return new CDbSqlite(dbName,host,port);
        }else{
            return NULL;
        }
    }
}

}
}

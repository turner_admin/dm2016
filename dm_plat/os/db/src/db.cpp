﻿/*
 * db.cpp
 *
 *  Created on: 2022年11月7日
 *      Author: dylan
 */

#include <dm/export.hpp>

#define DM_API_OS_DB DM_API_EXPORT

#include <dm/os/db/db.hpp>

namespace dm{
namespace os{

CDb::CDb(const char* dbName,const char* host,short port):m_host(host),m_dbName(dbName),m_port(port){
}

CDb::~CDb(){}

}

}



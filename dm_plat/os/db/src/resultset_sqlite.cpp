﻿/*
 * resultset_sqlite.cpp
 *
 *  Created on: 2017年3月11日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_OS_DB DM_API_EXPORT

#include <dm/os/db/resultset_sqlite.hpp>
#include <dm/os/log/logger.hpp>

namespace dm{
namespace os{

static const char* logModule = "CResultSetSqlite.os.dm";

CResultSetSqlite::CResultSetSqlite():m_stmt(0),m_size(0),m_r(-1){
}

CResultSetSqlite::~CResultSetSqlite(){
	if( m_stmt!=0 )
		sqlite3_finalize(m_stmt);
}

int CResultSetSqlite::size()const{
	if( m_stmt!=0 ){
		return m_size;
	}else
		return 0;
}

bool CResultSetSqlite::next(){
	if( m_stmt!=0 ){
		if( SQLITE_ROW==sqlite3_step(m_stmt) ){
			++m_r;
			return true;
		}
	}

	return false;
}

void CResultSetSqlite::reset(){
	if( m_stmt!=0 ){
		sqlite3_reset(m_stmt);
		m_r = -1;
	}
}

int CResultSetSqlite::currentRow()const{
	return m_r;
}

int columnByName( sqlite3_stmt* stmt,const char* name ){
	if( stmt!=0 ){
		int count = sqlite3_column_count(stmt);
		for( int i=0;i<count;++i )
			if( strcmp(sqlite3_column_name(stmt,i),name)==0 )
				return i;
	}

	return -1;
}

bool CResultSetSqlite::asBool( int col )const{
	if( m_stmt!=0 )
		return sqlite3_column_int(m_stmt,col);

	return false;
}

bool CResultSetSqlite::asBool( const char* col )const{
	int index = columnByName(m_stmt,col);
	if( index<0 ){
		log().error(THISMODULE "没有找到列名%s",col);
		return false;
	}else
		return asBool(index);
}

int CResultSetSqlite::asInt( int col )const{
	if( m_stmt!=0 )
		return sqlite3_column_int(m_stmt,col);
	return 0;
}

int CResultSetSqlite::asInt( const char* col )const{
	int index = columnByName(m_stmt,col);
	if( index<0 ){
		log().error(THISMODULE "没有找到列名%s",col);
		return 0;
	}else
		return asInt(index);
}

dm::uint CResultSetSqlite::asUint( int col )const{
	if( m_stmt!=0 )
		return sqlite3_column_int64(m_stmt,col);
	return 0;
}

dm::uint CResultSetSqlite::asUint( const char* col )const{
	int index = columnByName(m_stmt,col);
	if( index<0 ){
		log().error(THISMODULE "没有找到列名%s",col);
		return 0;
	}else
		return asUint(index);
}

double CResultSetSqlite::asDouble( int col )const{
	if( m_stmt!=0 )
		return sqlite3_column_double(m_stmt,col);
	return 0;
}

double CResultSetSqlite::asDouble( const char* col )const{
	int index = columnByName(m_stmt,col);
	if( index<0 ){
		log().error(THISMODULE "没有找到列名%s",col);
		return 0;
	}else
		return asDouble(index);
}

std::string CResultSetSqlite::asString( int col )const{
	if( m_stmt!=0 )
		return (const char*)sqlite3_column_text(m_stmt,col);
	return "";
}

std::string CResultSetSqlite::asString( const char* col )const{
	int index = columnByName(m_stmt,col);
	if( index<0 ){
		log().error(THISMODULE "没有找到列名%s",col);
		return "";
	}else
		return asString(index);
}

dm::int64 CResultSetSqlite::asInt64( int col )const{
	if( m_stmt!=0 )
		return sqlite3_column_int64(m_stmt,col);
	return 0;
}

dm::int64 CResultSetSqlite::asInt64( const char* col )const{
	int index = columnByName(m_stmt,col);
	if( index<0 ){
		log().error(THISMODULE "没有找到列名%s",col);
		return 0;
	}else
		return asInt64(index);
}

bool CResultSetSqlite::isNull( int col )const{
	if( m_stmt!=0 )
		return sqlite3_column_type(m_stmt,col)==SQLITE_NULL;
	return true;
}

bool CResultSetSqlite::isNull( const char* col )const{
	if( m_stmt!=0 ){
		int index = columnByName(m_stmt,col);
		if( index<0 ){
			log().error(THISMODULE "没有找到列名%s",col);
			return true;
		}else
			return isNull(index);
	}else
		return true;
}

int CResultSetSqlite::columnSize()const{
	if( m_stmt!=0 )
		return sqlite3_column_count(m_stmt);
	return 0;
}

std::string CResultSetSqlite::valueString( int col )const{
	if( m_stmt==0 )
		return "";
	char buf[64];

	switch( sqlite3_column_type(m_stmt,col) ){
	case SQLITE_INTEGER:
		std::sprintf(buf,"%lld",sqlite3_column_int64(m_stmt,col));
		return buf;
	case SQLITE_FLOAT:
		std::sprintf(buf,"%f",sqlite3_column_double(m_stmt,col));
		return buf;
	case SQLITE_TEXT:
		return (const char*)sqlite3_column_text(m_stmt,col);
		break;
	default:
		return "";
	}
}

std::string CResultSetSqlite::valueString( const char* col )const{
	int index = columnByName(m_stmt,col);
	if( index<0 ){
		log().error(THISMODULE "没有找到列名%s",col);
		return "";
	}else
		return valueString(index);
}

void CResultSetSqlite::free(){
	if( m_stmt!=0 ){
		sqlite3_finalize(m_stmt);
		m_stmt = 0;
	}
}
}
}



﻿/*
 * logger.cpp
 *
 *  Created on: 2016年12月6日
 *      Author: Dylan.Gao
 */

#include <dm/export.hpp>

#define DM_API_OS_LOG DM_API_EXPORT

#include <dm/os/log/logger.hpp>
#include <cstdio>
#include <cstdarg>

using namespace std;

namespace dm{
namespace os{

CLogger::CLogger():CLogMgr(){
#ifdef WIN32
	m_pid = GetCurrentProcessId();
#else
	m_pid = getpid();
#endif
}

CLogger& CLogger::ins(){
	static CLogger i;
	return i;
}

void CLogger::resetPid(){
#ifdef WIN32
	m_pid = GetCurrentProcessId();
#else
	m_pid = getpid();
#endif
}

void CLogger::logmsg( const dm::CTimeStamp& ts,const char* module,const obj_t& obj,const level_t& level,const char* fun,const char* msg ){
	CLogInfo info;
	info.pid = m_pid;
	info.module = module;
	info.level = level;
	info.t = ts;
	info.fun = fun;
	info.obj = obj;
	info.msg = msg;
	m_q->push(info);
}

void CLogger::log( const dm::CTimeStamp& ts,const char* module,const obj_t& obj,const CLogInfo::ELevel& level,const char* fun,const char* fmt,... ){
	char msg[CLogInfo::LenOfMsg];
	va_list ap;
	va_start(ap,fmt);
	if( -1==vsnprintf(msg,CLogInfo::LenOfMsg,fmt,ap) ){
		logmsg(ts,module,obj,level,fun,"日志缓冲区溢出");
		return;
	}
	va_end(ap);
	logmsg(ts,module,obj,level,fun,msg);
}

void CLogger::log( const char* module,const obj_t& obj,const CLogInfo::ELevel& level,const char* fun,const char* fmt,... ){
	char msg[CLogInfo::LenOfMsg];
	va_list ap;
	va_start(ap,fmt);
	if( -1==vsnprintf(msg,CLogInfo::LenOfMsg,fmt,ap) ){
		logmsg(module,obj,level,fun,"日志缓冲区溢出");
		return;
	}
	va_end(ap);
	logmsg(module,obj,level,fun,msg);
}

void CLogger::error( const dm::CTimeStamp& ts, const char* module,const obj_t& obj,const char* fun,const char* fmt,...){
	char msg[CLogInfo::LenOfMsg];
	va_list ap;
	va_start(ap,fmt);
	if( -1==vsnprintf(msg,CLogInfo::LenOfMsg,fmt,ap) ){
		logmsg(ts,module,obj,CLogInfo::Error,fun,"日志缓冲区溢出");
		return;
	}
	va_end(ap);
	logmsg(ts,module,obj,CLogInfo::Error,fun,msg);
}

void CLogger::error( const char* module,const obj_t& obj,const char* fun,const char* fmt,...){
	char msg[CLogInfo::LenOfMsg];
	va_list ap;
	va_start(ap,fmt);
	if( -1==vsnprintf(msg,CLogInfo::LenOfMsg,fmt,ap) ){
		logmsg(module,obj,CLogInfo::Error,fun,"日志缓冲区溢出");
		return;
	}
	va_end(ap);
	logmsg(module,obj,CLogInfo::Error,fun,msg);
}

void CLogger::warnning( const dm::CTimeStamp& ts,const char* module, const obj_t& obj,const char* fun,const char* fmt,...){
	char msg[CLogInfo::LenOfMsg];
	va_list ap;
	va_start(ap,fmt);
	if( -1==vsnprintf(msg,CLogInfo::LenOfMsg,fmt,ap) ){
		logmsg(ts,module,obj,CLogInfo::Warnning,fun,"日志缓冲区溢出");
		return;
	}
	va_end(ap);
	logmsg(ts,module,obj,CLogInfo::Warnning,fun,msg);
}

void CLogger::warnning( const char* module, const obj_t& obj,const char* fun,const char* fmt,...){
	char msg[CLogInfo::LenOfMsg];
	va_list ap;
	va_start(ap,fmt);
	if( -1==vsnprintf(msg,CLogInfo::LenOfMsg,fmt,ap) ){
		logmsg(module,obj,CLogInfo::Warnning,fun,"日志缓冲区溢出");
		return;
	}
	va_end(ap);
	logmsg(module,obj,CLogInfo::Warnning,fun,msg);
}

void CLogger::info( const dm::CTimeStamp& ts,const char* module,const obj_t& obj, const char* fun,const char* fmt,...){
	char msg[CLogInfo::LenOfMsg];
	va_list ap;
	va_start(ap,fmt);
	if( -1==vsnprintf(msg,CLogInfo::LenOfMsg,fmt,ap) ){
		logmsg(ts,module,obj,CLogInfo::Info,fun,"日志缓冲区溢出");
		return;
	}
	va_end(ap);
	logmsg(ts,module,obj,CLogInfo::Info,fun,msg);
}

void CLogger::info( const char* module,const obj_t& obj, const char* fun,const char* fmt,...){
	char msg[CLogInfo::LenOfMsg];
	va_list ap;
	va_start(ap,fmt);
	if( -1==vsnprintf(msg,CLogInfo::LenOfMsg,fmt,ap) ){
		logmsg(module,obj,CLogInfo::Info,fun,"日志缓冲区溢出");
		return;
	}
	va_end(ap);
	logmsg(module,obj,CLogInfo::Info,fun,msg);
}

void CLogger::debug( const dm::CTimeStamp& ts,const char* module,const obj_t& obj,const char* fun,const char* fmt,...){
	char msg[CLogInfo::LenOfMsg];
	va_list ap;
	va_start(ap,fmt);
	if( -1==vsnprintf(msg,CLogInfo::LenOfMsg,fmt,ap) ){
		logmsg(ts,module,obj,CLogInfo::Debug,fun,"日志缓冲区溢出");
		return;
	}
	va_end(ap);
	logmsg(ts,module,obj,CLogInfo::Debug,fun,msg);
}

void CLogger::debug( const char* module,const obj_t& obj,const char* fun,const char* fmt,...){
	char msg[CLogInfo::LenOfMsg];
	va_list ap;
	va_start(ap,fmt);
	if( -1==vsnprintf(msg,CLogInfo::LenOfMsg,fmt,ap) ){
		logmsg(module,obj,CLogInfo::Debug,fun,"日志缓冲区溢出");
		return;
	}
	va_end(ap);
	logmsg(module,obj,CLogInfo::Debug,fun,msg);
}

}
}


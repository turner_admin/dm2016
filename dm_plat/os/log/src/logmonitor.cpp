﻿/*
 * logmonitor.cpp
 *
 *  Created on: 2016年12月6日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_OS_LOG DM_API_EXPORT

#include <dm/os/log/logmonitor.hpp>
#include <dm/os/signal_handler.hpp>
#include <iostream>
#include <dm/datetime.hpp>

using namespace std;

namespace dm{
namespace os{
	CLogMonitor::CLogMonitor():CLogMgr(){
		m_p = m_q->getCurPos();
	}

	void CLogMonitor::onNew( const ringpos_t& pos,const CLogInfo& info,const bool& overflow )const{

		cout <<pos.getCycle()<<"."<<pos.getPos()<<"["<<dm::CDateTime(info.t).toString()<<"]";
		if( overflow )
			cout <<"(of)";

		cout <<"pid:"<<info.pid<<" ";
		printf(" obj:%p L:",info.obj);

		switch( info.level ){
		case CLogInfo::Debug:
			cout <<"Debug ";
			break;
		case CLogInfo::Info:
			cout <<"Info ";
			break;
		case CLogInfo::Warnning:
			cout <<"Warn ";
			break;
		case CLogInfo::Error:
			cout <<"Error ";
			break;
		default:
			cout <<"Unknow ";
		}

		cout <<info.module.c_str()
			<<"::"<<info.fun.c_str()
				<<"{"<<info.msg.c_str()
				<<"}"<<endl;
	}

	bool CLogMonitor::filter( const ringpos_t& /*pos*/,const CLogInfo& /*info*/,const bool& /*overflow*/ )const{
		return false;
	}

	void CLogMonitor::run(){
		CLogInfo info;
		bool overflow;

		while( !dm::os::CSignalHandler::ins().ifSignal() ){
			if (m_q->waitAndCheckOver(m_p, overflow, info,1))
				continue;
			if( !filter(m_p,info,overflow) )
				onNew(m_p,info,overflow);
			if( overflow )
				m_p = m_q->getCurPos();
		}
	}

	bool CLogMonitor::tryGet( CLogInfo& info,bool& overflow ){
		return m_q->popAndCheckOver( m_p,overflow,info);
	}

	void CLogMonitor::get( CLogInfo& info,bool& overflow ){
		m_q->waitAndCheckOver( m_p,overflow,info);
	}

	void CLogMonitor::reset(){
		m_p = m_q->getCurPos();
	}
}
}


﻿/*
 * loginfo.cpp
 *
 *  Created on: 2016年12月6日
 *      Author: Dylan.Gao
 */

#include <dm/export.hpp>

#define DM_API_OS_LOG DM_API_EXPORT

#include <dm/os/log/loginfo.hpp>

namespace dm{
namespace os{

CLogInfo::CLogInfo():pid(0),t(),level(CLogInfo::Debug),module(),fun(),obj(0),msg(){
}

CLogInfo::CLogInfo( const CLogInfo& logInfo ):pid(logInfo.pid),
	t(logInfo.t),level(logInfo.level),
	module(logInfo.module),fun(logInfo.fun),obj(logInfo.obj),msg(logInfo.msg){
}

CLogInfo& CLogInfo::operator =( const CLogInfo& loginfo ){
	pid = loginfo.pid;
	t = loginfo.t;
	level = loginfo.level;
	module = loginfo.module;
	fun = loginfo.fun;
	obj = loginfo.obj;
	msg = loginfo.msg;

	return *this;
}

}
}


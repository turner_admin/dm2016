﻿/*
 * logmgr.cpp
 *
 *  Created on: 2016年12月6日
 *      Author: Dylan.Gao
 */

#include <dm/export.hpp>

#define DM_API_OS_LOG DM_API_EXPORT

#include <boost/interprocess/managed_shared_memory.hpp>
#include <dm/os/log/logmgr.hpp>
#include <dm/os/signal_handler.hpp>
#include <iostream>

namespace dm{
namespace os{

using namespace boost::interprocess;
CLogMgr::CLogMgr(){
	CSignalHandler::ins();

	permissions p;
	p.set_unrestricted();
	try{
		static boost::interprocess::managed_shared_memory s(open_or_create,"dmlog",sizeof(queue_t)+1024,0,p);
		m_q = s.find_or_construct<queue_t>(unique_instance)();

	}catch( interprocess_exception& e ){
		std::cerr <<__FILE__<<e.what();
	}
}

void CLogMgr::remove(){
	boost::interprocess::shared_memory_object::remove("dmlog");
}

}
}



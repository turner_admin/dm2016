/*
 * shared_rt_mgr.hxx
 *
 *  Created on: 2023年10月16日
 *      Author: Dylan.Gao
 */

#ifndef DM_PLAT_OS_TEST_SHARED_RT_MGR_HXX_
#define DM_PLAT_OS_TEST_SHARED_RT_MGR_HXX_

#include <cxxtest/TestSuite.h>
#include <dm/os/shared_rt_mgr.hpp>
#include <dm/os/shared_rt_loader.hpp>
#include <dm/os/sys/host_record.hpp>

class CTestOsSharedRtMgr:public CxxTest::TestSuite{
public:
	typedef dm::os::TCSharedRtMgr<dm::os::sys::SHostInfo,dm::os::sys::SHostState> mgr_t;
	typedef dm::os::TCSharedRtLoader<dm::os::sys::SHostInfo,dm::os::sys::SHostState> loader_t;

	class CMyLoader:public loader_t{
	protected:
		const char* dbEngine( const dm::env::CCfg& cfg )const{
			return cfg.sys_dbEngine().c_str();
		}

		const char* dbName( const dm::env::CCfg& cfg )const{
			return cfg.sys_dbName().c_str();
		}

		const char* dbHost( const dm::env::CCfg& cfg )const{
			return cfg.sys_dbHost().c_str();
		}

		short dbPort( const dm::env::CCfg& cfg )const{
			return cfg.sys_dbPort();
		}

		const char* dbUser( const dm::env::CCfg& cfg )const{
			return cfg.sys_dbUsr().c_str();
		}

		const char* dbPasswd( const dm::env::CCfg& cfg )const{
			return cfg.sys_dbPwd().c_str();
		}

		const char* sqlCreateSqlite3()const{
			return "CREATE TABLE t_host_temp (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, name varchar(64) NOT NULL UNIQUE, descr varchar(255) NOT NULL, sound varchar(255), group_id integer(10), net_a integer(10), net_b integer(10), com_a integer(10), com_b integer(10), com_c integer(10))";
		}

		const char* sqlCreateMysql()const{
			return "CREATE TABLE t_host (id int(10) NOT NULL AUTO_INCREMENT, name varchar(64) NOT NULL UNIQUE, descr varchar(255) NOT NULL, sound varchar(255), group_id int(10), net_a int(10), net_b int(10), com_a int(10), com_b int(10), com_c int(10), PRIMARY KEY (id))";
		}

		const char* sqlCreatePg()const{
			return "CREATE TABLE t_host_temp (id SERIAL NOT NULL, name varchar(64) NOT NULL UNIQUE, descr varchar(255) NOT NULL, sound varchar(255), group_id int4, net_a int4, net_b int4, com_a int4, com_b int4, com_c int4, PRIMARY KEY (id))";
		}

		const char* sqlCreateHgdb()const{
			return sqlCreatePg();
		}

		const char* sqlSelect()const{
			return "select id,name,descr,net_a,net_b from t_host_temp";
		}

		void loadRecord( dm::os::CResultSet& rs,dm::os::sys::SHostInfo& info,dm::os::sys::SHostState& value ){
			info.id = rs.asInt64(0);
			info.name = rs.asString(1);
			info.desc = rs.asString(2,"");
			info.net1_ip.set(rs.asString(3,"").c_str());
			info.net2_ip.set(rs.asString(4,"").c_str());

			value.init();
		}
	};

	void testDefault(){
//		CMyLoader loader;
//		mgr_t mgr("testSharedRtMgr",loader);
	}
};

#endif /* DM_PLAT_OS_TEST_SHARED_RT_MGR_HXX_ */

/*
 * shared_rt_record.hxx
 *
 *  Created on: 2023年9月7日
 *      Author: Dylan.Gao
 */

#ifndef DM_PLAT_OS_COMMON_TEST_SHARED_RT_RECORD_HXX_
#define DM_PLAT_OS_COMMON_TEST_SHARED_RT_RECORD_HXX_

#include <cxxtest/TestSuite.h>
#include <dm/os/shared_rt_record.hpp>
#include <string>

class CTestOsSharedRtRecord:public CxxTest::TestSuite{
public:
	void testDefault(){
		typedef dm::os::TSSharedRtRecord<std::string,int> record_t;
		class CTestLoader:public record_t::CLoader{
		public:
			bool loadRecord(std::string& i,int& v,dm::CTimeStamp& ts,dm::CRunTimeStamp& rts,const dm::CTimeStamp& tsNow,const dm::CRunTimeStamp& rtsNow ){
				i = "test";
				v = 0;
				ts = tsNow;
				rts = rtsNow;

				return true;
			}
		};

		record_t record;

		CTestLoader loader;

		TSM_ASSERT_EQUALS("加载",record.reload(loader),true);

		TS_WARN("OK");
	}
};



#endif /* DM_PLAT_OS_COMMON_TEST_SHARED_RT_RECORD_HXX_ */

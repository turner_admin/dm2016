/*
 * shared_buf.hxx
 *
 *  Created on: 2024年3月28日
 *      Author: Dylan.Gao
 */

#ifndef DM_PLAT_OS_TEST_SHARED_BUF_HXX_
#define DM_PLAT_OS_TEST_SHARED_BUF_HXX_

#include <cxxtest/TestSuite.h>
#include <dm/os/shared_buf.hpp>

class CTestOsSharedBuf:public CxxTest::TestSuite{
public:
	void testEnum(){
		dm::os::TSSharedBuf<int,dm::uint8,4> buf;
		dm::os::TSSharedBuf<int,dm::uint16,8> buf16;
		dm::os::TSSharedBuf<int,dm::uint64,16> buf64;

		TSM_ASSERT_EQUALS("掩码长度 8",buf.BitsOfMask,8);
		TSM_ASSERT_EQUALS("掩码长度16",buf16.BitsOfMask,16);
		TSM_ASSERT_EQUALS("掩码长度64",buf64.BitsOfMask,64);

		TSM_ASSERT_EQUALS("记录个数 8",buf.SizeOfRecords,8*4);
		TSM_ASSERT_EQUALS("记录个数16",buf16.SizeOfRecords,16*8);
		TSM_ASSERT_EQUALS("记录个数64",buf64.SizeOfRecords,64*16);
	}

	void testDefault(){
		dm::os::TSSharedBuf<int> buf;
		TSM_ASSERT("空",buf.isEmpty());
	}
};

#endif /* DM_PLAT_OS_TEST_SHARED_BUF_HXX_ */

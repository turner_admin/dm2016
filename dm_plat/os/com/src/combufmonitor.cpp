﻿/*
 * combufmonitor.cpp
 *
 *  Created on: 2016年12月16日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_OS_COM DM_API_EXPORT

#include <dm/os/com/combufmonitor.hpp>
#include <cstring>
#include <typeinfo>

namespace dm{
namespace os{
namespace com{

CComBufMonitor::CComBufMonitor( const CComBuf* buf ):m_buf(buf){
	m_buf->getPos(m_p);
}

int CComBufMonitor::pop( dm::uint8* b,int size ){
	if( m_buf==NULL )
		throw std::bad_typeid();
	return m_buf->getDataAndMovePos(b,size,m_p);
}

}
}
}



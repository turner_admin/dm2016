﻿/*
 * tcpserveraddr.cpp
 *
 *  Created on: 2016年12月11日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_OS_COM DM_API_EXPORT

#include <dm/os/com/tcpserveraddr.hpp>
#include <dm/os/log/logger.hpp>
#include <dm/string/stringlist.hpp>
#include <cstdio>
#include <cstdlib>

namespace dm{
namespace os{
namespace com{

CTcpServerAddr::CTcpServerAddr( const char* host,short p ):CAddr(),
		m_localHost(host),m_localPort(p){
}

CTcpServerAddr::CTcpServerAddr( const CTcpServerAddr& addr ):CAddr(addr),
		m_localHost(addr.m_localHost),m_localPort(addr.m_localPort){
}

CTcpServerAddr& CTcpServerAddr::operator =( const CTcpServerAddr& addr ){
	m_localHost = addr.m_localHost;
	m_localPort = addr.m_localPort;
	return *this;
}

bool CTcpServerAddr::operator ==( const CTcpServerAddr& addr )const{
	return m_localHost==addr.m_localHost && m_localPort==addr.m_localPort;
}

void CTcpServerAddr::output( std::ostream& ostr )const{
	ostr << m_localHost.c_str() <<':'
			<< m_localPort;
}

bool CTcpServerAddr::fromStringList( const dm::string::CStringList& list ){
	if( list.size()!=2 )
		return false;

	m_localHost = list.at(0).c_str();

	bool ok;
	m_localPort = list.at(1).toUint16(&ok);

	return ok;
}

}
}
}



﻿/*
 * udpconnectaddr.cpp
 *
 *  Created on: 2017年2月21日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_OS_COM DM_API_EXPORT

#include <dm/os/com/udpconnectaddr.hpp>

namespace dm{
namespace os{
namespace com{

CUdpConnectAddr& CUdpConnectAddr::operator =( const CUdpConnectAddr& addr ){
	m_peerHost = addr.m_peerHost;
	m_peerPort = addr.m_peerPort;

	return *this;
}

bool CUdpConnectAddr::operator ==( const CUdpConnectAddr& addr )const{
	return m_peerHost==addr.m_peerHost && m_peerPort==addr.m_peerPort;
}

void CUdpConnectAddr::output( std::ostream& ostr )const{
	ostr <<m_peerHost.c_str()<<":"<<m_peerPort;
}

bool CUdpConnectAddr::fromStringList( const dm::string::CStringList& list ){
	if( list.size()!=2 )
		return false;
	m_peerHost = list.at(0).c_str();
	bool ok;
	m_peerPort = list.at(1).toInt16(&ok);
	return ok;
}

}
}
}



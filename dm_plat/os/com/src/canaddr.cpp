﻿/*
 * canaddr.cpp
 *
 *  Created on: 2017年3月21日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_OS_COM DM_API_EXPORT

#include <dm/os/com/canaddr.hpp>

namespace dm{
namespace os{
namespace com{

CCanAddr::CCanAddr( const char* dev,const dm::uint32& bps,const can_id_t& id,const can_id_t& mask ):
	CAddr(),m_dev(dev),m_id(id),m_mask(mask),m_bps(bps){
}

CCanAddr::CCanAddr( const CCanAddr& addr ):CAddr(addr),
		m_dev(addr.m_dev),m_id(addr.m_id),m_mask(addr.m_mask),m_bps(addr.m_bps){
}

CCanAddr& CCanAddr::operator =( const CCanAddr& addr ){
	m_dev = addr.m_dev;
	m_bps = addr.m_bps;
	m_id = addr.m_id;
	m_mask = addr.m_mask;

	return *this;
}

bool CCanAddr::operator ==( const CCanAddr& addr )const{
	return m_dev==addr.m_dev && m_id==addr.m_id && m_mask == addr.m_mask;
}

void CCanAddr::output( std::ostream& ostr )const{
	ostr <<m_dev.c_str()<<":"<<m_bps<<":"<<m_id<<":"<<m_mask;
}

bool CCanAddr::fromStringList( const dm::string::CStringList& list ){
	if( list.size()!=4 )
		return false;

	int i = 0;
	m_dev = list.at(i++).c_str();

	bool ok;

	m_bps = list.at(i++).toUint32(&ok);
	if( !ok )
		return false;

	m_id = list.at(i++).toInt32(&ok);
	if( !ok )
		return false;

	m_mask = list.at(i++).toInt32(&ok);

	return ok;
}

}
}
}



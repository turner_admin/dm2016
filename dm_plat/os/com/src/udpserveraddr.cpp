﻿/*
 * udpserveraddr.cpp
 *
 *  Created on: 2016年12月16日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_OS_COM DM_API_EXPORT

#include <dm/os/com/udpserveraddr.hpp>
#include <cstdlib>

namespace dm{
namespace os{
namespace com{

CUdpServerAddr::CUdpServerAddr( const char* host,short port ):CAddr(),
		m_localHost(host),m_localPort(port){
}

CUdpServerAddr::CUdpServerAddr( const CUdpServerAddr& addr ):CAddr(addr),
		m_localHost(addr.m_localHost),m_localPort(addr.m_localPort){
}

CUdpServerAddr& CUdpServerAddr::operator =( const CUdpServerAddr& addr ){
	m_localHost = addr.m_localHost;
	m_localPort = addr.m_localPort;
	return *this;
}

bool CUdpServerAddr::operator ==( const CUdpServerAddr& addr )const{
	return m_localHost==addr.m_localHost && m_localPort==addr.m_localPort;
}

void CUdpServerAddr::output( std::ostream& ostr )const{
	ostr << m_localHost.c_str() <<":"<<m_localPort;
}

bool CUdpServerAddr::fromStringList( const dm::string::CStringList& list ){
	if( list.size()!=2 )
		return false;

	m_localHost = list.at(0).c_str();

	bool ok;
	m_localPort = list.at(1).toUint16(&ok);
	return ok;
}

}
}
}



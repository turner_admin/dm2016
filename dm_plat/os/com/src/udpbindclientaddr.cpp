﻿/*
 * udpbindclient.cpp
 *
 *  Created on: 2016年12月16日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_OS_COM DM_API_EXPORT

#include <dm/os/com/udpbindclientaddr.hpp>
#include <sstream>
#include <cstdio>

namespace dm{
namespace os{
namespace com{

CUdpBindClientAddr::CUdpBindClientAddr( const char* localhost,short localPort,const char* server,short serverPort ):CAddr(),
		m_localHost(localhost),m_localPort(localPort),m_serverHost(server),m_serverPort(serverPort){
}

CUdpBindClientAddr::CUdpBindClientAddr( const CUdpBindClientAddr& addr ):CAddr(addr),
	m_localHost(addr.m_localHost),m_localPort(addr.m_localPort),m_serverHost(addr.m_serverHost),m_serverPort(addr.m_serverPort){
}

CUdpBindClientAddr& CUdpBindClientAddr::operator =( const CUdpBindClientAddr& addr ){
	m_localHost = addr.m_localHost;
	m_localPort = addr.m_localPort;
	m_serverHost = addr.m_serverHost;
	m_serverPort = addr.m_serverPort;

	return *this;
}

bool CUdpBindClientAddr::operator ==( const CUdpBindClientAddr& addr )const{
	return m_localHost==addr.m_localHost && m_localPort==addr.m_localPort && m_serverHost==addr.m_serverHost && m_serverPort==addr.m_serverPort;
}

void CUdpBindClientAddr::output( std::ostream& ostr )const{
	ostr <<m_localHost.c_str()<<':'
			<<m_localPort<<':'
			<<m_serverHost.c_str()<<':'
			<<m_serverPort;
}

bool CUdpBindClientAddr::fromStringList( const dm::string::CStringList& list ){
	if( list.size()!=4 )
		return false;

	bool ok;
	m_localHost = list.at(0).c_str();
	m_localPort = list.at(1).toUint16(&ok);
	if( !ok )
		return false;

	m_serverHost = list.at(2).c_str();
	m_serverPort = list.at(3).toUint16(&ok);

	return ok;
}

}
}
}



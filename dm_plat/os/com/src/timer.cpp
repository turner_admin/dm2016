﻿/*
 * timer.cpp
 *
 *  Created on: 2016年12月19日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_OS_COM DM_API_EXPORT

#include <dm/os/com/timer.hpp>
#include <dm/os/log/logger.hpp>

namespace dm{
namespace os{
namespace com{

//static const char* logModule = "CTimer.com.os.dm";

void CTimer::start( bool a ){
//	log().debug(THISMODULE "启动定时器 %s",a?"自动":"手动");
	m_auto = a;
	start_timer();
}

void CTimer::start( bool a,time_t interval ){
//	log().debug(THISMODULE "启动定时器 %s ms:%ld",a?"自动":"手动",interval.total_milliseconds());
	m_auto = a;
	m_d = interval;
	start_timer();
}

void CTimer::async_wait( const boost::system::error_code& error ){
	if( error )
		on_timercancel();
	else{
		on_timeout();
		if( m_auto ){
			start_timer();
		}
	}
}

void CTimer::on_timeout(){

}

void CTimer::on_timercancel(){
}

}
}
}



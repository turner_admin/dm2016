﻿/*
 * tcpclientaddr.cpp
 *
 *  Created on: 2016年12月17日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_OS_COM DM_API_EXPORT

#include <dm/os/com/tcpclientaddr.hpp>
#include <dm/string/string.hpp>

namespace dm{
namespace os{
namespace com{

CTcpClientAddr::CTcpClientAddr( const char* host,short p ):CAddr(),
		m_serverHost(host),m_serverPort(p){
}

CTcpClientAddr::CTcpClientAddr( const CTcpClientAddr& addr ):CAddr(addr),
		m_serverHost(addr.m_serverHost),m_serverPort(addr.m_serverPort){
}

CTcpClientAddr& CTcpClientAddr::operator =( const CTcpClientAddr& addr ){
	m_serverHost = addr.m_serverHost;
	m_serverPort = addr.m_serverPort;
	return *this;
}

bool CTcpClientAddr::operator ==( const CTcpClientAddr& addr )const{
	return m_serverHost==addr.m_serverHost && m_serverPort==addr.m_serverPort;
}

void CTcpClientAddr::output( std::ostream& ostr )const{
	ostr << m_serverHost.c_str()<<':'
			<<m_serverPort;
}

bool CTcpClientAddr::fromStringList( const dm::string::CStringList& list ){
	if( list.size()!=2 )
		return false;

	m_serverHost = list.at(0).c_str();

	bool ok;
	m_serverPort = list.at(1).toUint16(&ok);

	return ok;
}

}
}
}



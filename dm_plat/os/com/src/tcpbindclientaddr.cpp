﻿/*
 * tcpbindclientaddr.cpp
 *
 *  Created on: 2017年2月13日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_OS_COM DM_API_EXPORT

#include <dm/os/com/tcpbindclientaddr.hpp>

namespace dm{
namespace os{
namespace com{

CTcpBindClientAddr::CTcpBindClientAddr( const char* local,const short& localPort,const char* server,const short& serverPort ):CAddr(),
		m_localHost(local),m_localPort(localPort),m_serverHost(server),m_serverPort(serverPort){
}

CTcpBindClientAddr::CTcpBindClientAddr( const CTcpBindClientAddr& addr ):CAddr(addr),
		m_localHost(addr.m_localHost),m_localPort(addr.m_localPort),m_serverHost(addr.m_serverHost),m_serverPort(addr.m_serverPort){
}

CTcpBindClientAddr& CTcpBindClientAddr::operator =( const CTcpBindClientAddr& addr ){
	m_localHost = addr.m_localHost;
	m_localPort = addr.m_localPort;

	m_serverHost = addr.m_serverHost;
	m_serverPort = addr.m_serverPort;

	return *this;
}

bool CTcpBindClientAddr::operator ==( const CTcpBindClientAddr& addr )const{
	return m_serverHost==addr.m_serverHost && m_serverPort==addr.m_serverPort &&
			m_localHost==addr.m_localHost && m_localPort==addr.m_localPort;
}

void CTcpBindClientAddr::output( std::ostream& ostr )const{
	ostr <<m_localHost.c_str()<<":"<<m_localPort<<":"
			<<m_serverHost.c_str()<<":"<<m_serverPort;

}

bool CTcpBindClientAddr::fromStringList( const dm::string::CStringList& list ){
	if( list.size()!=4 )
		return false;
	m_localHost = list.at(0).c_str();

	bool ok;
	m_localPort = list.at(1).toInt16(&ok);
	if( !ok )
		return false;

	m_serverHost = list.at(2).c_str();
	m_serverPort = list.at(3).toInt16(&ok);
	return ok;
}
}
}
}


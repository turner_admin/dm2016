﻿/*
 * serialaddr.cpp
 *
 *  Created on: 2016年12月11日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_OS_COM DM_API_EXPORT

#include <dm/os/com/serialaddr.hpp>
#include <sstream>

namespace dm{
namespace os{
namespace com{

CSerialAddr::CSerialAddr(
		const char* dev,
		CSerialAddr::baud_t bps,
		CSerialAddr::bits_t charBits,
		CSerialAddr::EStopBits stopBits,
		CSerialAddr::EParity partity,
		CSerialAddr::EFlowCtl flowCtl ):CAddr(),
				m_device(dev),m_bps(bps),m_charbits(charBits),
				m_stop(stopBits),m_parity(partity),m_flowCtl(flowCtl)
{
}

CSerialAddr::CSerialAddr( const CSerialAddr& addr ):CAddr(addr),
						m_device(addr.m_device),m_bps(addr.m_bps),
						m_charbits(addr.m_charbits),
						m_stop(addr.m_stop),m_parity(addr.m_parity),m_flowCtl(addr.m_flowCtl)
{
}

CSerialAddr& CSerialAddr::operator =( const CSerialAddr& addr ){
	m_device = addr.m_device;
	m_bps = addr.m_bps;
	m_charbits = addr.m_charbits;
	m_stop = addr.m_stop;
	m_parity = addr.m_parity;
	m_flowCtl = addr.m_flowCtl;

	return *this;
}

bool  CSerialAddr::operator ==( const CSerialAddr& addr )const{
	return m_device==addr.m_device && m_bps==addr.m_bps \
			&& m_charbits==addr.m_charbits && m_stop==addr.m_stop \
			&& m_parity==addr.m_parity && m_flowCtl==addr.m_flowCtl;
}

void CSerialAddr::output( std::ostream& ostr )const{
	ostr << m_device.c_str() <<':'
			<< m_bps <<':'
			<< (int)m_charbits <<':'
			<< (int)m_stop <<':'
			<< (int)m_parity <<':'
			<< (int)m_flowCtl;
}

bool CSerialAddr::fromStringList( const dm::string::CStringList& list ){
	if( list.size()!=6 )
		return false;

	int i = 0;
	m_device = list.at(i++).c_str();

	bool ok;

	m_bps = list.at(i++).toInt32(&ok);
	if( !ok )
		return false;

	m_charbits = list.at(i++).toUint8(&ok);
	if( !ok )
		return false;

	m_stop = EStopBits(list.at(i++).toUint8(&ok));
	if( !ok )
		return false;

	m_parity = EParity(list.at(i++).toUint8(&ok));
	if( !ok )
		return false;

	m_flowCtl = EFlowCtl(list.at(i++).toUint8(&ok));

	return ok;
}

}
}
}



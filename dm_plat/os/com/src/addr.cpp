﻿/*
 * addr.cpp
 *
 *  Created on: 2016年12月17日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_OS_COM DM_API_EXPORT

#include <dm/os/com/addr.hpp>
#include <dm/string/stringref.hpp>
#include <sstream>

namespace dm{
namespace os{
namespace com{

int CAddr::getString( char* buf,int size )const{
	std::string str = toString();
	dm::string::CStringRef ref(str.c_str());
	if( ref.dump(buf,size) )
		return ref.len();
	else
		return -1;
}

std::string CAddr::toString()const{
	std::ostringstream ostr;
	output(ostr);
	return ostr.str();
}

bool CAddr::fromString( const char* buf,const int& len ){
	dm::string::CStringList list;
	if( len==0 ){
		list.split(buf,':');
	}else
		list.split(buf,':',len);

	if( fromStringList(list) )
		return true;
	else
		return false;
}

}
}
}



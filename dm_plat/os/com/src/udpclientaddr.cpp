﻿/*
 * udpclientaddr.cpp
 *
 *  Created on: 2016年12月16日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_OS_COM DM_API_EXPORT

#include <dm/os/com/udpclientaddr.hpp>
#include <sstream>
#include <cstdio>

namespace dm{
namespace os{
namespace com{

CUdpClientAddr::CUdpClientAddr( const char* host,short port ):CAddr(),
		m_serverHost(host),m_serverPort(port){
}

CUdpClientAddr::CUdpClientAddr( const CUdpClientAddr& addr ):CAddr(addr),
		m_serverHost(addr.m_serverHost),m_serverPort(addr.m_serverPort){
}

CUdpClientAddr& CUdpClientAddr::operator =( const CUdpClientAddr& addr ){
	m_serverHost = addr.m_serverHost;
	m_serverPort = addr.m_serverPort;
	return *this;
}

bool CUdpClientAddr::operator ==( const CUdpClientAddr& addr )const{
	return (m_serverHost==addr.m_serverHost) && ( m_serverPort==addr.m_serverPort);
}

void CUdpClientAddr::output( std::ostream& ostr )const{
	ostr <<m_serverHost.c_str() <<':'
			<< m_serverPort;
}

bool CUdpClientAddr::fromStringList( const dm::string::CStringList& list ){
	if( list.size()!=2 )
		return false;

	bool ok;
	m_serverHost = list.at(0).c_str();

	m_serverPort = list.at(1).toUint16(&ok);
	return ok;
}

}
}
}



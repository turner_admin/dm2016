/*
 * stdindevice.cpp
 *
 *  Created on: 2016年12月19日
 *      Author: work
 */

#include <dm/os/com/stdindevice.hpp>
#include <dm/os/log/logger.hpp>

namespace dm{
namespace os{
namespace com{

static const char* logModule = "CStdInDevice.com.os.dm";

CStdInDevice::CStdInDevice( ios_t& ios,const size_t& rxMaxSize ):CDevice(rxMaxSize),
		m_dev(ios,::dup(STDIN_FILENO)){
	log().debug( THISMODULE "创建对象");
	handler_connect();
}

CStdInDevice::~CStdInDevice(){
	log().debug( THISMODULE "销毁对象");
	m_dev.close();
}

void CStdInDevice::start_rx(){
	try{
		m_dev.async_read_some(
				boost::asio::buffer(m_rxBuf.get(),m_rxBufSize),
				boost::bind(&CStdInDevice::handler_rx,this,boost::asio::placeholders::error,boost::asio::placeholders::bytes_transferred())
		);
	}catch( std::exception& ec ){
		log().error( THISMODULE "开启监听失败%s",ec.what());
	}
}

void CStdInDevice::cancelDev(){
	try{
		m_dev.cancel();
	}catch( std::exception& ec ){
		log().error( THISMODULE "取消设备失败%s",ec.what());
	}
}

}
}
}



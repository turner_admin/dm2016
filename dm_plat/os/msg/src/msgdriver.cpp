﻿/*
 * msgdriver.cpp
 *
 *  Created on: Aug 19, 2017
 *      Author: vte
 */
#include <dm/export.hpp>

#define DM_API_OS_MSG DM_API_EXPORT

#include <dm/os/msg/msgdriver.hpp>
#include <dm/os/msg/msgagent.hpp>

namespace dm{
namespace os{
namespace msg{

static CMsgAgent* msg_agent;

CMsgDriver::CMsgDriver(){
	msg_agent = new CMsgAgent;
}

CMsgDriver& CMsgDriver::ins(){
	static CMsgDriver i;
	return i;
}

void CMsgDriver::sendMsg( CMsgInfo& msg ){
	msg_agent->sendMsg(msg);
}

bool CMsgDriver::tryGet( CMsgInfo& msg ){
	return msg_agent->tryGet(msg);
}

bool CMsgDriver::tryGetRequeset( CMsgInfo& msg ){
	return msg_agent->tryGetRequeset(msg);
}

bool CMsgDriver::tryGetAnswer( CMsgInfo& msg ){
	return msg_agent->tryGetAnswer(msg);
}

void CMsgDriver::wait( CMsgInfo& msg ){
	msg_agent->wait(msg);
}

void CMsgDriver::wait4requset( CMsgInfo& msg ){
	msg_agent->wait4requset(msg);
}

void CMsgDriver::wait4answer( CMsgInfo& msg ){
	msg_agent->wait4answer(msg);
}

void CMsgDriver::resetRequest(){
	msg_agent->resetRequest();
}

void CMsgDriver::resetAnswer(){
	msg_agent->resetAnswer();
}

void CMsgDriver::requestTest( bool needAck ){
	CMsgInfo msg;
	msg.setRequest_test(needAck);
	sendMsg(msg);
}

void CMsgDriver::answerTest( dm::msg::EMsgAck ack ){
	CMsgInfo msg;
	msg.setAnswer_test(ack);
	sendMsg(msg);
}

void CMsgDriver::requesetCall( dm::msg::SCallInfo::group_t group,bool needAck ){
	CMsgInfo msg;
	msg.setRequest_call(group,needAck);
	sendMsg(msg);
}

void CMsgDriver::answerCall( dm::msg::SCallInfo::group_t group,dm::msg::EMsgAck ack ){
	CMsgInfo msg;
	msg.setAnswer_call(group,ack);
	sendMsg(msg);
}

void CMsgDriver::requestSyncClock( bool needAck ){
	CMsgInfo msg;
	msg.setRequest_syncClock(needAck);
	sendMsg(msg);
}

void CMsgDriver::answerSyncClock( dm::msg::EMsgAck ack ){
	CMsgInfo msg;
	msg.setAnswer_syncClock(ack);
	sendMsg(msg);
}

void CMsgDriver::requestRmtCtl( dm::msg::index_t idx,dm::msg::ERemoteControl ctl,bool needAck ){
	CMsgInfo msg;
	msg.setRequest_remoteCtl(idx,ctl,needAck);
	sendMsg(msg);
}

void CMsgDriver::requestRmtCtl_preOpen( dm::msg::index_t idx,bool needAck ){
	CMsgInfo msg;
	msg.setRequest_remoteCtl(idx,dm::msg::Rc_PreOpen,needAck);
	sendMsg(msg);
}

void CMsgDriver::requestRmtCtl_open( dm::msg::index_t idx,bool needAck ){
	CMsgInfo msg;
	msg.setRequest_remoteCtl(idx,dm::msg::Rc_Open,needAck);
	sendMsg(msg);
}

void CMsgDriver::requestRmtCtl_close( dm::msg::index_t idx,bool needAck ){
	CMsgInfo msg;
	msg.setRequest_remoteCtl(idx,dm::msg::Rc_Close,needAck);
	sendMsg(msg);
}

void CMsgDriver::requestRmtCtl_preClose( dm::msg::index_t idx,bool needAck ){
	CMsgInfo msg;
	msg.setRequest_remoteCtl(idx,dm::msg::Rc_PreClose,needAck);
	sendMsg(msg);
}

void CMsgDriver::answerRmtCtl( dm::msg::index_t idx,dm::msg::ERemoteControl ctl,dm::msg::EMsgAck ack ){
	CMsgInfo msg;
	msg.setAnswer_remoteCtl(idx,ctl,ack);
	sendMsg(msg);
}

void CMsgDriver::answerRmtCtl_preOpen( dm::msg::index_t idx,dm::msg::EMsgAck ack ){
	CMsgInfo msg;
	msg.setAnswer_remoteCtl(idx,dm::msg::Rc_PreOpen,ack);
	sendMsg(msg);
}

void CMsgDriver::answerRmtCtl_open( dm::msg::index_t idx,dm::msg::EMsgAck ack ){
	CMsgInfo msg;
	msg.setAnswer_remoteCtl(idx,dm::msg::Rc_Open,ack);
	sendMsg(msg);
}

void CMsgDriver::answerRmtCtl_close( dm::msg::index_t idx,dm::msg::EMsgAck ack ){
	CMsgInfo msg;
	msg.setAnswer_remoteCtl(idx,dm::msg::Rc_Close,ack);
	sendMsg(msg);
}

void CMsgDriver::answerRmtCtl_preClose( dm::msg::index_t idx,dm::msg::EMsgAck ack ){
	CMsgInfo msg;
	msg.setAnswer_remoteCtl(idx,dm::msg::Rc_PreClose,ack);
	sendMsg(msg);
}

}
}
}

﻿/*
 * msgmgr.cpp
 *
 *  Created on: 2017年3月17日
 *      Author: work
 */
#include <dm/export.hpp>

#define DM_API_OS_MSG DM_API_EXPORT

#include <dm/os/msg/msgmgr.hpp>
#include <boost/interprocess/managed_shared_memory.hpp>
#include <iostream>

namespace dm{
namespace os{
namespace msg{

static const char* mname_request = "dmmsgrequest";
static const char* mname_answer  = "dmmsganswer";

using namespace boost::interprocess;

void CMsgMgr::remove(){
	shared_memory_object::remove(mname_request);
	shared_memory_object::remove(mname_request);
}

CMsgMgr::CMsgMgr(){
	try{
		permissions p;
		p.set_unrestricted();
		static managed_shared_memory request(open_or_create,mname_request,sizeof(queue_t)+1024,0,p);
		static managed_shared_memory answer(open_or_create,mname_answer,sizeof(queue_t)+1024,0,p);

		m_qRequest = request.find_or_construct<queue_t>(unique_instance)();
		m_qAnswer = answer.find_or_construct<queue_t>(unique_instance)();

	}catch( boost::interprocess::interprocess_exception& e ){
		std::cerr <<__FILE__<<e.what();
	}
}

void CMsgMgr::push( const CMsgInfo& msg ){
	if( msg.getAck()==dm::msg::MaAck || msg.getAck()==dm::msg::MaNoAck )
		m_qRequest->push(msg);
	else
		m_qAnswer->push(msg);
}

}
}
}


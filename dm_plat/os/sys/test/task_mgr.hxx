/*
 * task_mgr.hxx
 *
 *  Created on: 2023年9月21日
 *      Author: Dylan.Gao
 */

#ifndef DM_PLAT_OS_SYS_TEST_TASK_MGR_HXX_
#define DM_PLAT_OS_SYS_TEST_TASK_MGR_HXX_

#include <cxxtest/TestSuite.h>
#include <dm/os/sys/task_mgr.hpp>
#include <dm/os/sys/task.hpp>
#include <dm/os/sys/task_loader.hpp>
#include <dm/scoped_ptr.hpp>

class CTestSysTaskMgr:public CxxTest::TestSuite{
public:
	void testCreateDb(){
		dm::os::sys::CTaskLoader loader;
		loader.createTable();
	}

	void testConstructor(){
		dm::os::sys::CTaskMgr& mgr = dm::os::sys::CTaskMgr::ins();
        mgr.loadNew();
		TSM_ASSERT_EQUALS("获取数量",mgr.count(),2);
	}

	void testDefault(){
		CxxTest::setAbortTestOnFail(true);

		dm::os::sys::CTaskMgr& mgr = dm::os::sys::CTaskMgr::ins();
		int count = mgr.count();

		dm::os::sys::CTaskMgr::pos_t pos;

		for( int i=0;i<count;++i ){
			if( i==0 )
                mgr.firstPos(pos);
			else
                mgr.next(pos);

			TSM_ASSERT("获取记录位置失败",pos.isValid());

			dm::os::sys::CTask task(pos);

			TSM_ASSERT_EQUALS("任务信息刷新失败",task.refresh(),true);
		}
	}

	void testTask(){
		CxxTest::setAbortTestOnFail(true);

		dm::os::sys::CTask task(dm::os::sys::CTaskMgr::ins().firstPos());

		TSM_ASSERT_EQUALS("刷新任务",task.refresh(),true);

		dm::os::sys::STaskState state;
		dm::CTimeStamp tsNow = dm::CTimeStamp::cur();
		dm::CRunTimeStamp rtsNow = dm::CRunTimeStamp::cur();

		TSM_ASSERT("任务更新时间验证",task.getState().rtsRefresh!=rtsNow);
		TSM_ASSERT("任务时间验证",task.getTimeStamp()!=tsNow);
		TSM_ASSERT("任务运行时间验证",task.getRunTimeStamp()!=rtsNow);

		state.pid = task.getState().pid;
		state.state = task.getState().state;

		TSM_ASSERT("更新未变任务",!task.updateState(dm::os::sys::STaskState::Stoped, tsNow, rtsNow));
		TSM_ASSERT_EQUALS("刷新任务",task.refresh(),true);
		TSM_ASSERT("任务更新时间验证",task.getState().rtsRefresh==rtsNow);
	}

	void testFind(){
		CxxTest::setAbortTestOnFail(true);

		dm::os::sys::CTask task(dm::os::sys::CTaskMgr::ins().getPos(1));
		TSM_ASSERT_EQUALS("刷新任务",task.refresh(),true);
		TSM_ASSERT_EQUALS("刷新任务",task.getInfo().id,1);
	}

	void testUnload(){
		CxxTest::setAbortTestOnFail(true);
		dm::os::sys::CTaskMgr& mgr = dm::os::sys::CTaskMgr::ins();

		dm::os::sys::CTask task(mgr.getPos(1));

		TSM_ASSERT_EQUALS("卸载任务",mgr.unload(1),true);
	}

	void testReload(){
		CxxTest::setAbortTestOnFail(true);
		dm::os::sys::CTaskMgr& mgr = dm::os::sys::CTaskMgr::ins();

		TSM_ASSERT_EQUALS("重新加载任务",mgr.reload(1),true);
		dm::os::sys::CTask task(mgr.getPos(1));
		TSM_ASSERT("查找任务",task.refresh());
	}
};



#endif /* DM_PLAT_OS_SYS_TEST_TASK_MGR_HXX_ */

/*
 * host_data_mgr.hxx
 *
 *  Created on: 2024年7月8日
 *      Author: Dylan.Gao
 */

#ifndef DM_PLAT_OS_SYS_TEST_HOST_DATA_MGR_HXX_
#define DM_PLAT_OS_SYS_TEST_HOST_DATA_MGR_HXX_

#include <cxxtest/TestSuite.h>
#include <dm/os/sys/host_data_mgr.hpp>

class CTestHostDataMgr:public CxxTest::TestSuite{
public:
	struct SInfo{
		SInfo():i(10),f(1.0){
		}

		int i;
		float f;
		char s[10];
	};

	void testDefault(){
		dm::os::CHostDataMgr& hostDataMgr = dm::os::CHostDataMgr::ins();
		SInfo* p = hostDataMgr.get<SInfo>();

		TSM_ASSERT("获取指针",p);
		TSM_ASSERT_EQUALS("指针读数",p->i,10);

		p->f = 2.0;
		TSM_ASSERT_EQUALS("指针读数",p->f,2.0);

		hostDataMgr.free<SInfo>(p);

		p = hostDataMgr.get<SInfo>();
		TSM_ASSERT_EQUALS("指针读数",p->f,1.0);
	}
};

#endif /* DM_PLAT_OS_SYS_TEST_HOST_DATA_MGR_HXX_ */

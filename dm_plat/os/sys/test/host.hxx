/*
 * host.hxx
 *
 *  Created on: 2023年10月15日
 *      Author: Dylan.Gao
 */

#ifndef DM_PLAT_OS_SYS_TEST_HOST_HXX_
#define DM_PLAT_OS_SYS_TEST_HOST_HXX_

#include <cxxtest/TestSuite.h>
#include <dm/os/sys/host_mgr.hpp>
#include <dm/os/sys/host.hpp>

class CTestSysHost:public CxxTest::TestSuite{
public:
	void testDefault(){
		CxxTest::setAbortTestOnFail(true);
		dm::os::sys::CHostMgr& mgr = dm::os::sys::CHostMgr::ins();

		dm::os::sys::CHostMgr::pos_t pos = mgr.firstPos();
		TSM_ASSERT("无记录",pos.isValid() );

		dm::os::sys::CHost host(pos);
		TSM_ASSERT("刷新记录失败",host.refresh() );

		// 更新结果
		dm::os::sys::SHostState::EState state = dm::os::sys::SHostState::SBacking;
		TSM_ASSERT("更新记录",host.updateState(&state, nullptr, nullptr));

		TSM_ASSERT("刷新记录失败",host.refresh() );
		TSM_ASSERT_EQUALS("状态错误",host.getState().state,state);
	}
};

#endif /* DM_PLAT_OS_SYS_TEST_HOST_HXX_ */

/*
 * host_mgr.hxx
 *
 *  Created on: 2023年9月14日
 *      Author: Dylan.Gao
 */

#ifndef DM_PLAT_OS_SYS_TEST_HOST_MGR_HXX_
#define DM_PLAT_OS_SYS_TEST_HOST_MGR_HXX_

#include <cxxtest/TestSuite.h>
#include <dm/os/sys/host_mgr.hpp>
#include <dm/os/sys/host_loader.hpp>

class CTestSysHostMgr:public CxxTest::TestSuite{
public:
	void testCreateDb(){
		dm::os::sys::CHostLoader loader;
		loader.createTable();
	}

	void testDefault(){
		CxxTest::setAbortTestOnFail(true);
		dm::os::sys::CHostMgr& mgr = dm::os::sys::CHostMgr::ins();
        TSM_ASSERT("加载新记录:无",mgr.loadNews());

		int count = mgr.count();

		dm::os::sys::CHostLoader loader;
		loader.delHost("cxxtest");
		loader.delHost("cxxtest1");
		loader.delHost("cxxtest2");

		dm::os::id_t id;
		TSM_ASSERT("增加记录",loader.addHost(id, "cxxtest", "单元测试主机"));
        TSM_ASSERT("加载新记录:1",mgr.loadNew());
		TSM_ASSERT_EQUALS("获取数量",mgr.count(),count+1);

		TSM_ASSERT("增加记录1",loader.addHost(id, "cxxtest1", "单元测试主机1"));
		TSM_ASSERT("增加记录2",loader.addHost(id, "cxxtest2", "单元测试主机2"));
        TSM_ASSERT("加载新记录:2",mgr.loadNew());
		TSM_ASSERT_EQUALS("获取数量",mgr.count(),count+3);

		TSM_ASSERT("删除记录",loader.delHost(id));
		loader.delHost("cxxtest1");
		loader.delHost("cxxtest");

        TSM_ASSERT("加载新记录:无",mgr.loadNew());
		TSM_ASSERT_EQUALS("获取数量",mgr.count(),count+3);

		TSM_ASSERT("卸载记录",mgr.unload(id));
		TSM_ASSERT("卸载记录:cxxtest1",mgr.unload("cxxtest1"));
		TSM_ASSERT("卸载记录:cxxtest2",mgr.unload("cxxtest2"));

		TSM_ASSERT_EQUALS("获取数量",mgr.count(),count);
	}
};

#endif /* DM_PLAT_OS_SYS_TEST_HOST_MGR_HXX_ */

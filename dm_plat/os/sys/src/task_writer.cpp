/*
 * task_writer.cpp
 *
 *  Created on: 2023年10月18日
 *      Author: Dylan.Gao
 */

#include <dm/export.hpp>

#define DM_API_OS_SYS DM_API_EXPORT

#include <dm/os/sys/task_writer.hpp>
#include <dm/os/log/logger.hpp>

namespace dm{
namespace os{
namespace sys{

static const char* logModule = "CTaskWriter.sys.os.dm";

bool CTaskWriter::writeRecord( value_t& value,CTaskMgr::store_record_t& record ){
    CTaskMgr::store_record_t::CNormalValueWriter valueWriter;
    return record.writeValue(value.e1, valueWriter, *(value.e2), *(value.e3));
}

}
}
}

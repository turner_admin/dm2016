/*
 * task_updater.cpp
 *
 *  Created on: 2023年9月22日
 *      Author: Dylan.Gao
 */

#include <dm/export.hpp>

#define DM_API_OS_SYS DM_API_EXPORT

#include <dm/os/sys/task_updater.hpp>
#include <dm/os/log/logger.hpp>

namespace dm{
namespace os{
namespace sys{

static const char* logModule = "CTaskUpdater.sys.os.dm";

bool CTaskUpdater::updateRecord( value_t& value,CTaskMgr::store_record_t& record ){
    CTaskMgr::store_record_t::CNormalValueUpdater valueUpdater;
    return record.updateValue(value.e1, valueUpdater, *(value.e2), *value.e3);
}
}
}
}

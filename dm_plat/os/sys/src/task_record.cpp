/*
 * task_record.cpp
 *
 *  Created on: 2023年9月20日
 *      Author: Dylan.Gao
 */

#include <dm/export.hpp>

#define DM_API_OS_SYS DM_API_EXPORT

#include <dm/os/sys/task_record.hpp>

namespace dm{
namespace os{
namespace sys{

/**
 * 任务类型字符串
 */
static const char* TaskTypeString[STaskInfo::TypeMax] = {
		"Normal",
		"Device",
		"Forward",
		"Caculate",
		"Ui",
		"System"
};

/**
 * 任务类型串转换成字符串
 * @param type
 * @return
 */
const char* STaskInfo::type2string( STaskInfo::EType type ){
	if( type<0 || type>=TypeMax )
		return nullptr;
	return TaskTypeString[type];
}

/**
 * 字符串转换成任务类型编码
 * @param str
 * @return
 */
STaskInfo::EType STaskInfo::string2type( const char* str ){
	for( int i=0;i<STaskInfo::TypeMax;++i ){
		if( std::strcmp(str,TaskTypeString[i])==0 )
			return EType(i);
	}

	return TypeMax;
}

/**
 * 任务启动模式字符串
 */
static const char* TaskStartModeString[STaskInfo::StartSundays+1] = {
		"Deny","Daemon","Single","Manual",
		"Secondly","Minutely","Hourly","Daily","Monthly","Yearly",
		"Mondays","Tuesdays","Wendnesdays","Thursdays","Fridays","Saturdays","Sundays"
};

/**
 * 获取启动模式字符串
 * @param startMode
 * @return
 */
const char* STaskInfo::startMode2string( STaskInfo::EStartMode startMode ){
	if( startMode<0 || startMode>StartSundays )
		return nullptr;
	return TaskStartModeString[startMode];
}

/**
 * 字符串转换成启动模式编码
 * @param str
 * @return
 */
STaskInfo::EStartMode STaskInfo::string2startMode( const char* str ){
	for( int i=0;i<=StartSundays;++i ){
		if( std::strcmp(str,TaskStartModeString[i])==0 )
			return EStartMode(i);
	}

	return StartDeny;
}

static const char* TaskStateString[STaskState::Stoping+1] = {
		"未初始化","已经停止","正在启动","运行中","在其他主机上启动","在其他主机上运行","停止中"
};

const char* STaskState::state2string( EState state ){
	if( state<0 || state>Stoping )
		return nullptr;
	return TaskStateString[state];
}

STaskState::EState STaskState::string2state( const char* str ){
	for( int i=0;i<=Stoping;++i ){
		if( std::strcmp(str,TaskStateString[i])==0 )
			return EState(i);
	}

	return Uninit;
}

}
}
}

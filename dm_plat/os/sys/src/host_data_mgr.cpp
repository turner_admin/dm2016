/*
 * host_data_mgr.cpp
 *
 *  Created on: 2024年7月8日
 *      Author: Dylan.Gao
 */

#include <dm/os/sys/host_data_mgr.hpp>

using namespace boost::interprocess;

namespace dm{
namespace os{

#ifndef DM_HOST_DATA_MAX_SIZE
#define DM_HOST_DATA_MAX_SIZE (1024*1024)
#endif

CHostDataMgr::CHostDataMgr():m_mmgr(open_or_create,"DmHostData",DM_HOST_DATA_MAX_SIZE){
}

static CHostDataMgr* p = nullptr;

CHostDataMgr& CHostDataMgr::ins(){
	if( p==nullptr )
		p = new CHostDataMgr();
	return *p;
}

void CHostDataMgr::reset(){
	shared_memory_object::remove("DmHostData");
	if( p )
		delete p;
	p = nullptr;
}

}
}

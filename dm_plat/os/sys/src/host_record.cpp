/*
 * host_record.cpp
 *
 *  Created on: 2023年10月15日
 *      Author: Dylan.Gao
 */

#include <dm/export.hpp>

#define DM_API_OS_SYS DM_API_EXPORT

#include <dm/os/sys/host_record.hpp>

namespace dm{
namespace os{
namespace sys{

static const char* stateString[SHostState::SShutDown+1] = {
		"未知",
		"离线",
		"连接中",
		"主运行",
		"备运行",
		"主切换中",
		"备切换中",
		"待机"
};

static const char* netStateString[SHostState::NsOffLine+1] = {
		"未知",
		"直连在线",
		"在线",
		"离线"
};

const char* SHostState::state2string( EState state ){
	if( state<0 || state>SShutDown )
		return nullptr;

	return stateString[state];
}

SHostState::EState SHostState::string2state( const char* str ){
	if( !str )
		return SUnknow;
	for( int i=0;i<=SShutDown;++i )
		if( strcmp(str,stateString[i])==0 )
			return EState(i);

	return SUnknow;
}

const char* SHostState::netState2string( ENetState netState ){
	if( netState<0 || netState>NsOffLine )
		return nullptr;

	return netStateString[netState];
}

SHostState::ENetState SHostState::string2netState( const char* str ){
	if( !str )
		return NsUnknow;
	for( int i=0;i<=NsOffLine;++i )
		if( strcmp(str,netStateString[i])==0 )
			return ENetState(i);

	return NsUnknow;
}

}
}
}

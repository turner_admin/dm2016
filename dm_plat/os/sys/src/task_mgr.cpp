/*
 * task_mgr.cpp
 *
 *  Created on: 2023年9月19日
 *      Author: Dylan.Gao
 */

#include <dm/export.hpp>

#define DM_API_OS_SYS DM_API_EXPORT

#include <dm/os/sys/task_loader.hpp>
#include <dm/os/sys/task_finder.hpp>
#include <dm/os/sys/task_mgr.hpp>
#include <dm/os/log/logger.hpp>

namespace dm{
namespace os{
namespace sys{

static const char* logModule = "CTaskMgr.sys.os.dm";

CTaskMgr::CTaskMgr(){
	log().debug(THISMODULE "构造对象");
    m_bufMgr = new buf_mgr_t("sysTaskMgr");
}

CTaskMgr& CTaskMgr::ins(){
	static CTaskMgr mgr;
	return mgr;
}

bool CTaskMgr::loadNew(){
	CTaskLoader loader;
    return m_bufMgr->loadNew(loader).isValid();
}

bool CTaskMgr::find( const id_t& id,pos_t& pos ){
	CTaskFinder finder;
    return m_bufMgr->find(id,finder,pos);
}

CTaskMgr::pos_t CTaskMgr::getPos(const id_t& id ){
    CTaskFinder finder;
    pos_t pos;
    m_bufMgr->find(id,finder,pos);
    return pos;
}

bool CTaskMgr::reload( pos_t& pos ){
	if( !pos.isValid() ){
		log().info(THISMODULE "无效的位置(%d,%d)",pos.getBufferIndex(),pos.getBufferOffset());
		return false;
	}

	CTaskLoader reloader;
	return m_bufMgr->reload(pos, reloader);
}

bool CTaskMgr::reload( const id_t& id ){
	CTaskFinder finder;

    pos_t pos = m_bufMgr->find(id, finder);
	if( !pos.isValid() ){
		log().info(THISMODULE "未找到记录(id:%d)",id);
		return false;
	}

	CTaskLoader reloader;
	return m_bufMgr->reload(pos, reloader);
}

bool CTaskMgr::unload( pos_t& pos ){
	if( !pos.isValid() ){
		log().info(THISMODULE "无效的位置(%d,%d)",pos.getBufferIndex(),pos.getBufferOffset());
		return false;
	}

	buf_mgr_t::CUnloader unloader;
	return m_bufMgr->unload(pos, unloader);
}

bool CTaskMgr::unload( const id_t& id ){
	CTaskFinder finder;

    pos_t pos = m_bufMgr->find(id, finder);
	if( !pos.isValid() ){
		log().info(THISMODULE "未找到记录(id:%d)",id);
		return false;
	}

	buf_mgr_t::CUnloader unloader;
	return m_bufMgr->unload(pos, unloader);
}

void CTaskMgr::reset(){
	buf_mgr_t::CUnloader unloader;
	m_bufMgr->remove(unloader);
}

}
}
}

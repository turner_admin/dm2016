/*
 * task_reader.cpp
 *
 *  Created on: 2023年9月20日
 *      Author: Dylan.Gao
 */

#include <dm/export.hpp>

#define DM_API_OS_SYS DM_API_EXPORT

#include <dm/os/sys/task_reader.hpp>
#include <dm/os/sys/task.hpp>
#include <dm/os/log/logger.hpp>

namespace dm{
namespace os{
namespace sys{

static const char* logModule = "CTaskReader.sys.os.dm";

/**
 * 读取任务信息
 * @param record
 * @param value
 * @return
 */
bool CTaskReader::readRecord( CTaskMgr::store_record_t& record,value_t& value ){
    CTaskMgr::store_record_t::CNormalReader reader;
    return record.get(NULL, reader, value.e1, value.e2, value.e3, value.e4);
}

}
}
}



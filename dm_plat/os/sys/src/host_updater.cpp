/*
 * host_updater.cpp
 *
 *  Created on: 2023年10月14日
 *      Author: Dylan.Gao
 */

#include <dm/export.hpp>

#define DM_API_OS_SYS DM_API_EXPORT

#include <dm/os/sys/host_updater.hpp>
#include <dm/os/log/logger.hpp>

namespace dm{
namespace os{
namespace sys{

static const char* logModule = "CHostUpdater.sys.os.dm";

bool CHostUpdater::updateRecord( value_t& value,CHostMgr::store_record_t& record ){
    CHostMgr::store_record_t::CNormalValueUpdater valueUpdater;
    return record.updateValue(value.e1, valueUpdater, *(value.e2), *(value.e3));
}

}
}
}

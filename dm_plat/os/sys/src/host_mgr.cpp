﻿/*
 * host_mgr.cpp
 *
 *  Created on: 2023年9月14日
 *      Author: Dylan.Gao
 */

#include <dm/export.hpp>

#define DM_API_OS_SYS DM_API_EXPORT

#include <dm/os/sys/host_loader.hpp>
#include <dm/os/sys/host_finder.hpp>
#include <dm/os/sys/host_mgr.hpp>
#include <dm/os/log/logger.hpp>

namespace dm{
namespace os{
namespace sys{

static const char* logModule = "CHostMgr.sys.os.dm";

CHostMgr::CHostMgr(){
	log().debug(THISMODULE "创建对象");
    m_bufMgr = new buf_mgr_t("sysHostMgr");
}

CHostMgr& CHostMgr::ins(){
	static CHostMgr mgr;
	return mgr;
}

/**
 * 加载新记录
 * @return
 */
bool CHostMgr::loadNews(){
	CHostLoader hostLoader;
    return m_bufMgr->loadNews(hostLoader);
}

CHostMgr::pos_t CHostMgr::loadNew(){
	CHostLoader hostLoader;
    return m_bufMgr->loadNew(hostLoader);
}

CHostMgr::pos_t CHostMgr::getPos( const id_t& id ){
    CHostFinder finder;
    pos_t pos;
    m_bufMgr->find(id,finder,pos);
    return pos;
}

CHostMgr::pos_t CHostMgr::getPos( const char* name ){
    CHostFinder finder;
    pos_t pos;
    m_bufMgr->find(name,finder,pos);
    return pos;
}

/**
 * 根据ID查找主机信息位置
 * @param id
 * @return
 */
bool CHostMgr::getPos( const id_t& id,pos_t& pos ){
	CHostFinder finder;
    return m_bufMgr->find(id,finder,pos);
}

/**
 * 根据名字来查找主机信息位置
 * @param name
 * @return
 */
bool CHostMgr::getPos( const char* name,pos_t& pos ){
	CHostFinder finder;
    return m_bufMgr->find(name,finder,pos);
}

bool CHostMgr::reload( pos_t& pos ){
	CHostLoader loader;
	return m_bufMgr->reload(pos, loader);
}

/**
 * 重新加载主机信息
 * @param id
 * @return
 */
bool CHostMgr::reload( id_t id ){
	CHostFinder finder;
	CHostLoader loader;

	return m_bufMgr->reload(&id,nullptr,nullptr,finder,loader,nullptr,nullptr,nullptr);
}

bool CHostMgr::reload( const char* name ){
	CHostFinder finder;
	CHostLoader reloader;
	return m_bufMgr->reload(nullptr,name,nullptr,finder,reloader,nullptr,nullptr,nullptr);
}

bool CHostMgr::unload( pos_t& pos ){
	if( !pos.isValid() ){
		log().info(THISMODULE "无效的位置(%d,%d)",pos.getBufferIndex(),pos.getBufferOffset());
		return false;
	}

	buf_mgr_t::CUnloader unloader;
    return m_bufMgr->unload(pos, unloader);
}

/**
 * 卸载主机
 * @param id
 * @return
 */
bool CHostMgr::unload( const id_t& id ){
	CHostFinder finder;

    pos_t pos;
    if( !m_bufMgr->find(id, finder,pos) ){
		log().info(THISMODULE "未找到记录(id:%d)",id);
		return false;
	}

	buf_mgr_t::CUnloader unloader;
	return m_bufMgr->unload(pos, unloader);
}

bool CHostMgr::unload( const char* name ){
	CHostFinder finder;

    pos_t pos;
    if( !m_bufMgr->find(name, finder,pos) ){
		log().info(THISMODULE "未找到记录(name:%s)",name);
		return false;
	}

	buf_mgr_t::CUnloader unloader;
	return m_bufMgr->unload(pos, unloader);
}

/**
 * 删除系统
 * @return
 */
void CHostMgr::reset(){
	buf_mgr_t::CUnloader unloader;
	m_bufMgr->remove(unloader);
}

}
}
}


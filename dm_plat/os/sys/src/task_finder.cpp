/*
 * task_finder.cpp
 *
 *  Created on: 2023年10月23日
 *      Author: Dylan.Gao
 */

#include <dm/export.hpp>

#define DM_API_OS_SYS DM_API_EXPORT

#include <dm/os/sys/task_finder.hpp>

namespace dm{
namespace os{
namespace sys{

bool CTaskFinder::compareId( CTaskMgr::store_record_t& record,const id_t& id ){
    CTaskMgr::store_record_t::CIdComparer comparer;
	return record.compareById(id, comparer);
}

}
}
}

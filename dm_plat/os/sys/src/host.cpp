﻿/*
 * host.cpp
 *
 *  Created on: 2023年10月15日
 *      Author: Dylan.Gao
 */

#include <dm/export.hpp>

#define DM_API_OS_SYS DM_API_EXPORT

#include <dm/os/sys/host.hpp>

#include <dm/os/sys/host_mgr.hpp>
#include <dm/os/sys/host_reader.hpp>
#include <dm/os/sys/host_updater.hpp>
#include <dm/os/log/logger.hpp>

namespace dm{
namespace os{
namespace sys{

static const char* logModule = "CHost.sys.os.dm";

CHost::CHost():m_pos(){
	log().debug(THISMODULE "创建默认对象");
}

CHost::CHost( const pos_t& pos ):m_pos(pos){
	log().debug(THISMODULE "创建对象");
}

CHost::~CHost(){
	log().debug(THISMODULE "销毁对象");
}

void CHost::setPos( const pos_t& pos ){
	m_pos = pos;
}

bool CHost::refresh(){
	CHostMgr& mgr = CHostMgr::ins();

	CHostReader reader;
	CHostReader::value_t value(&m_info,&m_state,&m_ts,&m_rts);
    return mgr.m_bufMgr->get(m_pos, &value,reader);
}

bool CHost::updateState(const state_t* state,const net_state_t* net1State,const net_state_t* net2State,const dm::CTimeStamp& tsNow,const dm::CRunTimeStamp& rtsNow){
	if( state )
		m_state.state = *state;
	if( net1State )
		m_state.net1State = *net1State;
	if( net2State )
		m_state.net2State = *net2State;

	log().debug(THISMODULE "更新状态ID:%d 主机名:%s 描述:%s :%s 网络1:%s 网络2:%s",m_info.id,m_info.name.c_str(),m_info.desc.c_str(),stateString(),net1stateString(),net2stateString());

	m_ts = tsNow;
	m_rts = rtsNow;

	CHostUpdater hostUpdater;
	CHostUpdater::value_t value(&m_state,&m_ts,&m_rts);

	CHostMgr& mgr = CHostMgr::ins();
    return mgr.m_bufMgr->update(m_pos, &value,hostUpdater);
}

}
}
}

/*
 * task.cpp
 *
 *  Created on: 2023年9月20日
 *      Author: Dylan.Gao
 */

#include <dm/export.hpp>

#define DM_API_OS_SYS DM_API_EXPORT

#include <dm/os/sys/task.hpp>
#include <dm/os/sys/task_mgr.hpp>
#include <dm/os/sys/task_reader.hpp>
#include <dm/os/sys/task_updater.hpp>
#include <dm/os/sys/host_mgr.hpp>
#include <dm/os/log/logger.hpp>

namespace dm{
namespace os{
namespace sys{

static const char* logModule = "CTask.sys.os.dm";

CTask::CTask(){
	log().debug(THISMODULE "创建默认对象");
}

CTask::CTask(const pos_t& pos):m_pos(pos){
	log().debug(THISMODULE "创建对象");
}

CTask::~CTask(){
	log().debug(THISMODULE "销毁对象");
}

void CTask::setPos( const pos_t& pos ){
	m_pos = pos;
}

bool CTask::refresh(){
	CTaskMgr& mgr = CTaskMgr::ins();

	CTaskReader reader;
	CTaskReader::value_t value(&m_info,&m_state,&m_ts,&m_rts);
    return mgr.m_bufMgr->get(m_pos, &value,reader);
}

bool CTask::updateState( const STaskState& state,const dm::CTimeStamp& tsNow,const dm::CRunTimeStamp& rtsNow ){
	m_state = state;
	m_ts = tsNow;
	m_rts = rtsNow;

	CTaskMgr& mgr = CTaskMgr::ins();

	CTaskUpdater updater;
	CTaskUpdater::value_t value(&m_state,&m_ts,&m_rts);
    return mgr.m_bufMgr->update(m_pos, &value,updater);
}

bool CTask::updateState( const state_t::EState* pState,const process_t* pPid,const dm::CTimeStamp* pTsBegin,const dm::CTimeStamp* pTsStop,const dm::CTimeStamp* pTsExec,const dm::CTimeStamp* pTsCheck,const dm::CRunTimeStamp* pRtsRefresh,const dm::CTimeStamp& tsNow,const dm::CRunTimeStamp& rtsNow ){
	if( pState )
		m_state.state = *pState;
	if( pPid )
		m_state.pid = *pPid;
	if( pTsBegin )
		m_state.tsBegin = *pTsBegin;
	if( pTsStop )
		m_state.tsStop = *pTsStop;
	if( pTsExec )
		m_state.tsExec = *pTsExec;
	if( pTsCheck )
		m_state.tsCheck = *pTsCheck;
	if( pRtsRefresh )
		m_state.rtsRefresh = *pRtsRefresh;

	m_ts = tsNow;
	m_rts = rtsNow;

	CTaskMgr& mgr = CTaskMgr::ins();

	CTaskUpdater updater;
	CTaskUpdater::value_t value(&m_state,&m_ts,&m_rts);
    return mgr.m_bufMgr->update(m_pos, &value,updater);
}

bool CTask::updateState( state_t::EState state,const dm::CTimeStamp& tsNow,const dm::CRunTimeStamp& rtsNow ){
	switch( state ){
	case state_t::Uninit:
		m_state.state = state;
		m_state.pid = -1;
		m_state.tsBegin = dm::CTimeStamp();
		m_state.tsStop = dm::CTimeStamp();
		m_state.tsExec = dm::CTimeStamp();
		m_state.tsCheck = dm::CTimeStamp();
		m_state.rtsRefresh = rtsNow;
		break;
	case state_t::Stoped:
		m_state.state = state;
		m_state.pid = -1;
		m_state.tsStop = tsNow;
		m_state.rtsRefresh = rtsNow;
		break;
	case state_t::Starting:
		m_state.state = state;
		m_state.tsExec = tsNow;
		m_state.rtsRefresh = rtsNow;
		break;
	case state_t::Running:
		m_state.state = state;
		m_state.tsBegin = tsNow;
		m_state.rtsRefresh = rtsNow;
		break;
	case state_t::OtherStarting:
		m_state.state = state;
		m_state.pid = -1;
		m_state.tsExec = tsNow;
		m_state.rtsRefresh = rtsNow;
		break;
	case state_t::OtherRunning:
		m_state.state = state;
		m_state.pid = -1;
		m_state.tsBegin = tsNow;
		m_state.rtsRefresh = rtsNow;
		break;
	case state_t::Stoping:
		m_state.state = state;
		m_state.pid = -1;
		m_state.rtsRefresh = rtsNow;
		break;
	default:
		log().debug(THISMODULE "未知类型:%d",state);
		return false;
	}

	m_ts = tsNow;
	m_rts = rtsNow;
	CTaskMgr& mgr = CTaskMgr::ins();

	CTaskUpdater updater;
	CTaskUpdater::value_t value(&m_state,&m_ts,&m_rts);
    return mgr.m_bufMgr->update(m_pos, &value, updater );
}

CHost CTask::mainHost(){
	if( m_info.mainHost==-1 )
		return CHost();
    CHostMgr::pos_t pos;
    if( CHostMgr::ins().getPos(m_info.mainHost,pos) )
        return CHost(pos);

    return CHost();
}

CHost CTask::backHost(){
	if( m_info.backHost==-1 )
		return CHost();
    return CHost(CHostMgr::ins().getPos(m_info.backHost));
}

CHost CTask::runningHost(){
	if( m_state.state==STaskState::Starting || m_state.state==STaskState::Running )
		return mainHost();
	if( m_state.state==STaskState::OtherStarting || m_state.state==STaskState::OtherRunning )
		return backHost();

	return CHost();
}

}
}
}

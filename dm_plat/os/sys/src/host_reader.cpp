/*
 * host_reader.cpp
 *
 *  Created on: 2023年10月14日
 *      Author: 高德绵
 */

#include <dm/export.hpp>

#define DM_API_OS_SYS DM_API_EXPORT

#include <dm/os/sys/host_reader.hpp>
#include <dm/os/log/logger.hpp>

namespace dm{
namespace os{
namespace sys{

static const char* logModule = "CHostReader.sys.os.dm";

bool CHostReader::readRecord( CHostMgr::store_record_t& record,value_t& value ){
    CHostMgr::store_record_t::CNormalReader reader;
    return record.get(nullptr, reader, value.e1, value.e2, value.e3, value.e4);
}

}
}
}



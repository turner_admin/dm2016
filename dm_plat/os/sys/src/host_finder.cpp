/*
 * host_finder.cpp
 *
 *  Created on: 2023年10月14日
 *      Author: Dylan.Gao
 */

#include <dm/export.hpp>

#define DM_API_OS_SYS DM_API_EXPORT

#include <dm/os/sys/host_finder.hpp>

namespace dm{
namespace os{
namespace sys{

bool CHostFinder::compareId( CHostMgr::store_record_t& record,const id_t& id ){
    CHostMgr::store_record_t::CIdComparer comparer;
	return record.compareById(id, comparer);
}

bool CHostFinder::compareName( CHostMgr::store_record_t& record,const char* name ){
    CHostMgr::store_record_t::CNameComparer comparer;
	return record.compareByName(name, comparer);
}

}
}
}

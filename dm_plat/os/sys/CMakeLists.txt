cmake_minimum_required(VERSION 3.10)

cmake_policy(SET CMP0074 NEW)
find_package(Boost REQUIRED COMPONENTS date_time)

include_directories(${Boost_INCLUDE_DIRS})

aux_source_directory(src SRC)

add_library(dmsys SHARED ${SRC}) 

target_link_libraries(dmsys options oslog env ${Boost_LIBRARIES})

find_package(CxxTest)
if(CXXTEST_FOUND)
    include_directories(${CXXTEST_INCLUDE_DIR})
    enable_testing()
    CXXTEST_ADD_TEST(test_sys test_sys.cpp 
    	${CMAKE_CURRENT_SOURCE_DIR}/test/host_data_mgr.hxx
    	${CMAKE_CURRENT_SOURCE_DIR}/test/task_mgr.hxx
    	${CMAKE_CURRENT_SOURCE_DIR}/test/host_mgr.hxx
    	${CMAKE_CURRENT_SOURCE_DIR}/test/host.hxx
    	)
	target_link_libraries(test_sys dmsys oslog dmos osdb )
	set_target_properties(test_sys PROPERTIES WIN32_MANIFEST "NO")
endif()

set_target_properties(dmsys PROPERTIES WIN32_MANIFEST "NO")

install(TARGETS dmsys LIBRARY)
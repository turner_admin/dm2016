/*
 * cannet.cpp
 *
 *  Created on: 2017年3月22日
 *      Author: work
 */

#include <dm/protocol/cannet.hpp>
#include <dm/os/log/logger.hpp>

namespace dm{
namespace protocol{

static const char* logModule = "CCannet.protocol.dm";

CCannet::CCannet():CProtocol(){
	log().debug(THISMODULE "创建对象");
}

CCannet::~CCannet(){
	log().debug(THISMODULE "销毁对象");
}

CFrame& CCannet::getRxFrame(){
	return m_rxFrame;
}

const CFrame& CCannet::getTxFrame(){
	return m_txFrame;
}

CCannet::EAction CCannet::dealRxFrame( const ts_t& ts,const rts_t& rts ){
	if( rxFrame().can().can_id&CAN_ERR_FLAG ){
		log().debug(THISMODULE "收到错误帧");
		return dealRxErrFrame(rxFrame(),txFrame(),ts,rts);
	}else if( rxFrame().can().can_id&CAN_RTR_FLAG ){
		log().debug( THISMODULE "收到远程帧");
		return dealRxRemoteFrame(rxFrame(),txFrame(),ts,rts);
	}else if( rxFrame().can().can_id&CAN_EFF_FLAG ){
//		log().debug( THISMODULE "收到扩展帧");
		return dealRxExternFrame(rxFrame(),txFrame(),ts,rts);
	}else{
//		log().debug( THISMODULE "收到标准帧");
		return dealRxStandardFrame( rxFrame(),txFrame(),ts,rts);
	}
}

CCannet::EAction CCannet::dealRxErrFrame( const CFrameCan& rf,CFrameCan&,const ts_t& /*ts*/,const rts_t& /*rts*/ ){
	log().debug(THISMODULE "默认不处理 id=%04X len=%d",rf.can().can_id,rf.can().can_dlc);
	return Action_Nothing;
}

CCannet::EAction CCannet::dealRxRemoteFrame( const CFrameCan& rf,CFrameCan&,const ts_t& /*ts*/,const rts_t& /*rts*/ ){
	log().debug(THISMODULE "默认不处理 id=%04X len=%d",rf.can().can_id,rf.can().can_dlc);
	return Action_Nothing;
}

CCannet::EAction CCannet::dealRxExternFrame( const CFrameCan& rf,CFrameCan&,const ts_t& /*ts*/,const rts_t& /*rts*/ ){
	log().debug(THISMODULE "默认不处理 id=%04X len=%d",rf.can().can_id,rf.can().can_dlc);
	return Action_Nothing;
}

CCannet::EAction CCannet::dealRxStandardFrame( const CFrameCan& rf,CFrameCan&,const ts_t& /*ts*/,const rts_t& /*rts*/ ){
	log().debug(THISMODULE "默认不处理 id=%04X len=%d",rf.can().can_id,rf.can().can_dlc);
	return Action_Nothing;
}

}
}



/*
 * cannetmaster.cpp
 *
 *  Created on: 2017年4月1日
 *      Author: work
 */

#include <dm/protocol/cannetmaster.hpp>
#include <dm/os/log/logger.hpp>

namespace dm{
namespace protocol{

static const char* logModule = "CCannetMaster.protocol.dm";

CCannetMaster::CCannetMaster():CProtocol(),CCannet(){
	log().debug(THISMODULE "创建对象");
}

CCannetMaster::~CCannetMaster(){
	log().debug( THISMODULE "销毁对象");
}

CCannetMaster::EAction CCannetMaster::taskStart_call( frame_t* txFrame ){
	return taskStart_call( * dynamic_cast<CFrameCan*>(txFrame) );
}

CCannetMaster::EAction CCannetMaster::taskDo_call( frame_t* txFrame ){
	return taskDo_call( * dynamic_cast<CFrameCan*>(txFrame) );
}

CCannetMaster::EAction CCannetMaster::taskStart_syncClock( frame_t* txFrame ){
	return taskStart_syncClock( * dynamic_cast<CFrameCan*>(txFrame) );
}

CCannetMaster::EAction CCannetMaster::taskDo_syncClock( frame_t* txFrame ){
	return taskDo_syncClock( * dynamic_cast<CFrameCan*>(txFrame) );
}

CCannetMaster::EAction CCannetMaster::taskStart_remoteControl( frame_t* txFrame ){
	return taskStart_remoteControl( * dynamic_cast<CFrameCan*>(txFrame) );
}

CCannetMaster::EAction CCannetMaster::taskDo_remoteControl( frame_t* txFrame ){
	return taskDo_remoteControl( * dynamic_cast<CFrameCan*>(txFrame) );
}

CCannetMaster::EAction CCannetMaster::taskStart_parameters( frame_t* txFrame ){
	return taskStart_parameters( * dynamic_cast<CFrameCan*>(txFrame) );
}

CCannetMaster::EAction CCannetMaster::taskDo_parameters( frame_t* txFrame ){
	return taskDo_parameters( * dynamic_cast<CFrameCan*>(txFrame) );
}

CCannetMaster::EAction CCannetMaster::taskStart_call( CFrameCan& ){
	log().debug(THISMODULE "默认无处理");
	m_taskCall = Started;
	return Action_Nothing;
}

CCannetMaster::EAction CCannetMaster::taskDo_call( CFrameCan&  ){
	log().debug(THISMODULE "默认无处理");
	m_taskCall = Success;
	return Action_Nothing;
}

CCannetMaster::EAction CCannetMaster::taskStart_syncClock( CFrameCan& ){
	log().debug(THISMODULE "默认无处理");
	m_taskClock = Started;
	return Action_Nothing;
}

CCannetMaster::EAction CCannetMaster::taskDo_syncClock( CFrameCan& ){
	log().debug(THISMODULE "默认无处理");
	m_taskClock = Success;
	return Action_Nothing;
}

CCannetMaster::EAction CCannetMaster::taskStart_remoteControl( CFrameCan& ){
	log().debug(THISMODULE "默认无处理");
	m_taskRmtCtl = Started;
	return Action_Nothing;
}

CCannetMaster::EAction CCannetMaster::taskDo_remoteControl( CFrameCan& ){
	log().debug(THISMODULE "默认无处理");
	m_taskRmtCtl = Success;
	return Action_Nothing;
}

CCannetMaster::EAction CCannetMaster::taskStart_parameters( CFrameCan& ){
	log().debug(THISMODULE "默认无处理");
	m_taskParas = Started;
	return Action_Nothing;
}

CCannetMaster::EAction CCannetMaster::taskDo_parameters( CFrameCan& ){
	log().debug(THISMODULE "默认无处理");
	m_taskParas = Success;
	return Action_Nothing;
}

}
}


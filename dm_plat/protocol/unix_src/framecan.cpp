/*
 * framecan.cpp
 *
 *  Created on: 2017年3月22日
 *      Author: work
 */

#include <dm/protocol/framecan.hpp>

namespace dm{
namespace protocol{

CFrameCan::CFrameCan():CFrame(){
	m_can.can_id = 0;
	m_can.can_dlc = 0;
}

CFrameCan::CFrameCan( const CFrameCan& frame ):CFrame(frame){
	m_can.can_id = frame.m_can.can_id;
	m_can.can_dlc = frame.m_can.can_dlc;
	for( int i=0;i<m_can.can_dlc;++i )
		m_can.data[i] = frame.m_can.data[i];
}

CFrameCan& CFrameCan::operator =( const CFrameCan& frame ){
	m_can.can_id = frame.m_can.can_id;
	m_can.can_dlc = frame.m_can.can_dlc;
	for( int i=0;i<m_can.can_dlc;++i )
		m_can.data[i] = frame.m_can.data[i];

	return *this;
}

bool CFrameCan::encode( txbuf_t& buf )const{
	const dm::uint8* p = (const dm::uint8*)&m_can;
	for( dm::uint i=0;i<sizeof(m_can);++i )
		buf.push(p[i]);
	return true;
}

bool CFrameCan::decode( const rxbuf_t& buf,const pos_t& end,pos_t& start,const CFrame* ){
	if( end-start<sizeof(m_can) )
		return false;
	dm::uint8* p = (dm::uint8*)&m_can;
	for( dm::uint i=0;i<sizeof(m_can);++i,start++ )
		p[i] = buf.dataAt(start);

	return true;
}

void CFrameCan::set( dm::uint32 id,const uint8* buf,int len ){
	m_can.can_id = id;
	if( len>8 )
		len = 8;
	if( len<0 )
		len = 0;

	m_can.can_dlc = len;
	for( int i=0;i<len;++len )
		m_can.data[i] = buf[i];
}

}
}



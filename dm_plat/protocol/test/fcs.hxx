/*
 * fcs.hxx
 *
 *  Created on: 2020年2月26日
 *      Author: Dylan.Gao
 */

#ifndef PROTOCOL_TEST_FCS_HXX_
#define PROTOCOL_TEST_FCS_HXX_

#include <dm/protocol/fcs.hpp>

#include <cxxtest/TestSuite.h>

/**
 * 测试FCS校验
 */
class TestFcs:public CxxTest::TestSuite{
public:
	void testFcs16(){
		dm::uint8 buf[10];
		dm::uint16 cs = dm::protocol::calcFcs16(buf,10);
		TSM_ASSERT("FCS16校验",dm::protocol::checkFcs16(buf,10,cs));
	}
};

#endif /* PROTOCOL_TEST_FCS_HXX_ */

﻿/*
 * modbusmapdataitem.cpp
 *
 *  Created on: 2019-4-14
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_PROTOCOL DM_API_EXPORT

#include <dm/protocol/modbusmapdataitem.hpp>

namespace dm{
namespace protocol{

CModbusMapDataItem::CModbusMapDataItem():CModbusMapItem(),
		m_store(true),m_genTimed(true),m_reportTimed(true){
}

CModbusMapDataItem::~CModbusMapDataItem(){
}

bool CModbusMapDataItem::init( const TiXmlElement* cfg,const bool& plc ){
	if( !CModbusMapItem::init(cfg,plc) )
		return false;

	const char* c = cfg->Attribute("store");
	if( c!=NULL ){
		if( strcmp("false",c)==0 ){
			m_store = false;
		}else if( strcmp("true",c)==0 ){
			m_store = true;
		}else{
			return false;
		}
	}

	c = cfg->Attribute("gen-timed");
	if( c!=NULL ){
		if( strcmp("false",c)==0 ){
			m_genTimed = false;
		}else if( strcmp("true",c)==0 ){
			m_genTimed = true;
		}else{
			return false;
		}
	}

	c = cfg->Attribute("report-timed");
	if( c!=NULL ){
		if( strcmp("false",c)==0 ){
			m_reportTimed = false;
		}else if( strcmp("true",c)==0 ){
			m_reportTimed = true;
		}else{
			return false;
		}
	}

	return true;
}

}
}

﻿/*
 * modbusmap_rmtctl.cpp
 *
 *  Created on: 2019-4-14
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_PROTOCOL DM_API_EXPORT

#include <dm/protocol/modbusmap_rmtctl.hpp>

namespace dm{
namespace protocol{

CModbusMapRmtctl::CModbusMapRmtctl():CModbusMapItem(),
		m_bitOffset(0),m_bitLen(1),
		m_valueOpen(0),m_valueClose(1),m_valuePreOpen(0),m_valuePreClose(0),
		m_refFun(dm::protocol::CFrameModbus::UnknowFunc),m_refAddr(0xFFFF),
		m_openDesc("控分"),m_closeDesc("控合"),m_preOpenDesc("预控分"),m_preCloseDesc("预控合"){
}

bool CModbusMapRmtctl::init( const TiXmlElement* cfg,const bool& plc ){
	if( CModbusMapItem::init(cfg,plc)==false )
		return false;

	const char* c;
	int i;

	if( cfg->Attribute("bit-offset",&i) ){
		if( i<0 || i>15 )
			return false;
		m_bitOffset = i;
	}

	if( cfg->Attribute("bits",&i) ){
		if( i<0 )
			return false;
		m_bitLen = i;
	}

	if( cfg->Attribute("value-open",&i) ){
		if( i<0 )
			return false;
		m_valueOpen = i;
	}

	if( cfg->Attribute("value-close",&i) ){
		if( i<0 )
			return false;
		m_valueClose = i;
	}

	if( cfg->Attribute("value-preOpen",&i) ){
		if( i<0 )
			return false;
		m_valuePreOpen = i;
	}

	if( cfg->Attribute("value-preClose",&i) ){
		if( i<0 )
			return false;
		m_valuePreClose = i;
	}

	if( cfg->Attribute("ref-fun",&i) ){
		if( i<0 )
			return false;
		m_refFun = fun_t(i);
	}

	if( cfg->Attribute("ref-reg",&i) ){
		if( plc )
			--i;

		if( i<0 )
			return false;
		m_refAddr = i;
	}

	c = cfg->Attribute("desc-open");
	if( c ){
		m_openDesc = c;
	}

	c = cfg->Attribute("desc-close");
	if( c ){
		m_closeDesc = c;
	}

	c = cfg->Attribute("desc-preOpen");
	if( c ){
		m_preOpenDesc = c;
	}

	c = cfg->Attribute("desc-preClose");
	if( c ){
		m_preCloseDesc = c;
	}

	return true;
}
}
}

﻿/*
 * modbusmap_discrete.cpp
 *
 *  Created on: 2019-4-14
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_PROTOCOL DM_API_EXPORT

#include <dm/protocol/modbusmap_discrete.hpp>

namespace dm{
namespace protocol{

CModbusMapDiscrete::CModbusMapDiscrete():CModbusMapDataItem(),
		m_bitOffset(0),m_bitLen(16),m_valueBase(0),m_invalidValue(-1){
}

bool CModbusMapDiscrete::init( const TiXmlElement* cfg,const bool& plc ){
	if( false==CModbusMapDataItem::init(cfg,plc) ){
		return false;
	}

	const char* c;
	int i;

	if( cfg->Attribute("bit-offset",&i) ){
		if( i<0 || i>15 )
			return false;
		m_bitOffset = i;
	}

	if( cfg->Attribute("bits",&i) ){
		if( i<0 )
			return false;
		m_bitLen = i;
	}

	if( cfg->Attribute("value-base",&i) ){
		if( i<0 )
			return false;
		m_valueBase = i;
	}

	if( cfg->Attribute("invalid-value",&i) ){
		if( i<0 )
			return false;
		m_invalidValue = i;
	}

	cfg = cfg->FirstChildElement("value");
	while( cfg ){
		c = cfg->Attribute("desc");
		if( c )
			m_valueDescs.push_back(c);
		else
			return false;
	}

	if( m_valueDescs.size()<=0 )
		return false;

	return true;
}

}
}

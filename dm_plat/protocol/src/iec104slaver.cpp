﻿/*
 * iec104slaver.cpp
 *
 *  Created on: 2017年5月11日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_PROTOCOL DM_API_EXPORT

#include <dm/protocol/iec104slaver.hpp>
#include <dm/os/log/logger.hpp>

namespace dm{
namespace protocol{

static const char* logModule = "CIec104Slaver.protocol.dm";

CIec104Slaver::CIec104Slaver():CIec104(){
	log().debug(THISMODULE "创建对象");
}

CIec104Slaver::~CIec104Slaver(){
	log().debug(THISMODULE "销毁对象");
}

/**
 * Slaver模式用
 * @param
 * @return
 */
CIec104Slaver::EAction CIec104Slaver::dealStopDt( const ts_t& ts,const rts_t& /*rts*/ ){
	log().info( ts,THISMODULE "停止链路传输");
	m_dtStarted = false;
	txFrame().setUFrame();
	txFrame().setStopDt_confirm(true);

	return Action_SendAndReTime;
}

CIec104Slaver::EAction CIec104Slaver::dealStopDtConf( const ts_t& ts,const rts_t& /*rts*/ ){
	log().debug( ts,THISMODULE "停止链路传输确认,不支持");

	return Action_Nothing;
}

CIec104Slaver::EAction CIec104Slaver::dealStartDt( const ts_t& ts,const rts_t& /*rts*/ ){
	log().info( ts,THISMODULE "开启数据传输");
	m_dtStarted = true;
	txFrame().setUFrame();
	txFrame().setStartDt_confirm(true);

	return Action_SendAndReTime;
}

CIec104Slaver::EAction CIec104Slaver::dealStartDtConf( const ts_t& ts,const rts_t& /*rts*/ ){
	log().debug(ts,THISMODULE "启动链路传输确认,不支持");
	return Action_Nothing;
}

/**
 * 接收到召唤任务
 * @param rf
 * @param tf
 * @return
 */
CIec104Slaver::EAction CIec104Slaver::dealAsdu_C_IC_NA_1( const frame_apdu_t& rf,frame_apdu_t& tf,const ts_t& ts,const rts_t& rts ){
	tf = rf;

	if( rf.getTransCause()==frame_apdu_t::FACause_act ){
		if( setTask_call( ts,rts,rf.getInfo_100_C_IC_NA_1()->all-20 ) ){
			log().info(ts,THISMODULE "接收到召唤报文:%d",rf.getInfoAddr_100_C_IC_NA_1());
			tf.setCauseOfTrans( frame_apdu_t::FACause_actcon );
		}else{
			log().warnning(ts,THISMODULE "启动召唤任务失败:%d",rf.getInfoAddr_100_C_IC_NA_1());
			tf.setCauseOfTrans( frame_apdu_t::FACause_actterm );
		}
	}else if( rf.getTransCause()==frame_apdu_t::FACause_deact ){
		log().info( ts,THISMODULE "接收到停止召唤");
		m_taskCall = None;
		tf.setCauseOfTrans( frame_apdu_t::FACause_deactcon );
	}

	return Action_SendAndReTime;
}

/**
 * 接收到时钟同步命令
 * @param rf
 * @param tf
 * @return
 */
CIec104Slaver::EAction CIec104Slaver::dealAsdu_C_CS_NA_1( const frame_apdu_t& rf,frame_apdu_t& tf,const ts_t& ts,const rts_t& rts ){
	tf = rf;
	if( rf.getTransCause()==frame_apdu_t::FACause_act ){
		if( setCurrentTime( *rf.getInfo<frame_apdu_t::UCp56Time2a>(0)) ){
			log().info( ts,THISMODULE "时钟同步");
			setTask_syncClock(ts,rts);

			frame_apdu_t::UCp56Time2a t;
			getCurrentTime(t);
			tf.makeFrame_103_C_CS_NA_1(t,frame_apdu_t::FACause_actcon);
		}else{
			tf.setCauseOfTrans( frame_apdu_t::FACause_actterm );
		}

		return Action_SendAndReTime;
	}

	return Action_Nothing;
}

/**
 * 接收到远控命令
 * @param rf
 * @param tf
 * @return
 */
CIec104Slaver::EAction CIec104Slaver::dealAsdu_C_DC_NA_1( const frame_apdu_t& /*rf*/,frame_apdu_t& /*tf*/,const ts_t& /*ts*/,const rts_t& /*rts*/ ){
	return Action_Nothing;
}

// 参数读
CIec104Slaver::EAction CIec104Slaver::dealAsdu_C_RD_NA_1( const frame_apdu_t& /*rf*/,frame_apdu_t& /*tf*/,const ts_t& /*ts*/,const rts_t& /*rts*/ ){
	return Action_Nothing;
}

// 参数写
CIec104Slaver::EAction CIec104Slaver::dealAsdu_C_SE_NB_1( const frame_apdu_t& /*rf*/,frame_apdu_t& /*tf*/,const ts_t& /*ts*/,const rts_t& /*rts*/ ){
	return Action_Nothing;
}

/**
 * 总召结束
 * @param ts
 * @param rts
 * @return
 */
CIec104Slaver::EAction CIec104Slaver::taskEnd_call( const ts_t &ts,const rts_t &rts){
	log().debug(ts,THISMODULE "发送召唤激活终止");

	txFrame().getApdu().makeFrame_100_C_IC_NA_1(m_callGroup+20,frame_apdu_t::FACause_actterm);

	m_taskCall = None;

	return Action_SendAndReTime;
}

}
}



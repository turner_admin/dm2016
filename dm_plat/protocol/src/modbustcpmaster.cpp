﻿/*
 * modbustcpmaster.cpp
 *
 *  Created on: 2017年10月30日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_PROTOCOL DM_API_EXPORT

#include <dm/protocol/modbustcpmaster.hpp>
#include <dm/os/log/logger.hpp>

namespace dm{
namespace protocol{

static const char* logModule = "CModbusTcpMaster.protocol.dm";

CModbusTcpMaster::CModbusTcpMaster():CModbusTcp(),m_transFlag(0),m_unitFlag(1){
	setWaitMaxTimes(3);
	setWaitTimeout(5);

	m_rxFrame.setUpFrame();
	m_txFrame.setDnFrame();

	log().debug(THISMODULE "创建对象。默认等待时间%d 次数%d",m_waitTimeout,m_timeoutMaxTimes);
}

CModbusTcpMaster::~CModbusTcpMaster(){
	log().debug(THISMODULE "销毁对象");
}

void CModbusTcpMaster::reset( const ts_t& ts,const rts_t& rts ){
	CModbusTcp::reset(ts,rts);
	m_transFlag = 0;
}

CModbusTcpMaster::EAction CModbusTcpMaster::taskStart_call( CFrameModbus& /*tf*/,const ts_t& ts,const rts_t& rts ){
	return setFrameTransflag(taskStart_call(m_txFrame, ts, rts), m_txFrame);
}

CModbusTcpMaster::EAction CModbusTcpMaster::taskDo_call( CFrameModbus& /*tf*/,const ts_t& ts,const rts_t& rts ){
	return setFrameTransflag(taskDo_call(m_txFrame, ts, rts), m_txFrame);
}

CModbusTcpMaster::EAction CModbusTcpMaster::taskStart_syncClock( CFrameModbus& /*tf*/,const ts_t& ts,const rts_t& rts ){
	return setFrameTransflag(taskStart_syncClock(m_txFrame, ts, rts), m_txFrame);
}
CModbusTcpMaster::EAction CModbusTcpMaster::taskDo_syncClock( CFrameModbus& /*tf*/,const ts_t& ts,const rts_t& rts ){
	return setFrameTransflag(taskDo_syncClock(m_txFrame, ts, rts), m_txFrame);
}

CModbusTcpMaster::EAction CModbusTcpMaster::taskStart_remoteControl( CFrameModbus& /*tf*/,const ts_t& ts,const rts_t& rts ){
	return setFrameTransflag(taskStart_remoteControl(m_txFrame, ts, rts), m_txFrame);
}
CModbusTcpMaster::EAction CModbusTcpMaster::taskDo_remoteControl( CFrameModbus& /*tf*/,const ts_t& ts,const rts_t& rts ){
	return setFrameTransflag(taskDo_remoteControl(m_txFrame, ts, rts), m_txFrame);
}

CModbusTcpMaster::EAction CModbusTcpMaster::taskStart_parameters( CFrameModbus& /*tf*/,const ts_t& ts,const rts_t& rts ){
	return setFrameTransflag(taskStart_parameters(m_txFrame, ts, rts), m_txFrame);
}
CModbusTcpMaster::EAction CModbusTcpMaster::taskDo_parameters( CFrameModbus& /*tf*/,const ts_t& ts,const rts_t& rts ){
	return setFrameTransflag(taskDo_parameters(m_txFrame, ts, rts), m_txFrame);
}

CModbusTcpMaster::EAction CModbusTcpMaster::taskStart_call( CFrameModbusTcp& tf,const ts_t& ts,const rts_t& rts ){
	log().debug(THISMODULE "默认无操作");
	m_taskCall = Started;
	return Action_Nothing;
}

CModbusTcpMaster::EAction CModbusTcpMaster::taskDo_call( CFrameModbusTcp& tf,const ts_t& ts,const rts_t& rts ){
	log().debug(THISMODULE "默认无操作");
	m_taskCall = Success;
	return Action_Nothing;
}

CModbusTcpMaster::EAction CModbusTcpMaster::taskStart_syncClock( CFrameModbusTcp& tf,const ts_t& ts,const rts_t& rts ){
	log().debug(THISMODULE "默认无操作");
	m_taskClock = Started;
	return Action_Nothing;
}

CModbusTcpMaster::EAction CModbusTcpMaster::taskDo_syncClock( CFrameModbusTcp& tf,const ts_t& ts,const rts_t& rts ){
	log().debug(THISMODULE "默认无操作");
	m_taskClock = Success;
	return Action_Nothing;
}

CModbusTcpMaster::EAction CModbusTcpMaster::taskStart_remoteControl( CFrameModbusTcp& tf,const ts_t& ts,const rts_t& rts ){
	log().debug(THISMODULE "默认无操作");
	m_taskRmtCtl = Started;
	return Action_Nothing;
}

CModbusTcpMaster::EAction CModbusTcpMaster::taskDo_remoteControl( CFrameModbusTcp& tf,const ts_t& ts,const rts_t& rts ){
	log().debug(THISMODULE "默认无操作");
	m_taskRmtCtl = Success;
	return Action_Nothing;
}

CModbusTcpMaster::EAction CModbusTcpMaster::taskStart_parameters( CFrameModbusTcp& tf,const ts_t& ts,const rts_t& rts ){
	log().debug(THISMODULE "默认无操作");
	m_taskParas = Started;
	return Action_Nothing;
}

CModbusTcpMaster::EAction CModbusTcpMaster::taskDo_parameters( CFrameModbusTcp& tf,const ts_t& ts,const rts_t& rts ){
	log().debug(THISMODULE "默认无操作");
	m_taskParas = Success;
	return Action_Nothing;
}

CModbusTcpMaster::EAction CModbusTcpMaster::setFrameTransflag( const CModbusTcpMaster::EAction& act,CFrameModbusTcp& tf ){
	if( act==Action_Send || act==Action_SendAndReTime ){
		++m_transFlag;
		tf.setTransFlag(m_transFlag);
	}

	tf.setUnitFlag(m_unitFlag);
	return act;
}

}
}



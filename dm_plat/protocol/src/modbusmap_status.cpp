﻿/*
 * modbusmap_status.cpp
 *
 *  Created on: 2019-4-14
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_PROTOCOL DM_API_EXPORT

#include <dm/protocol/modbusmap_status.hpp>

namespace dm{
namespace protocol{

CModbusMapStatus::CModbusMapStatus():CModbusMapDataItem(),
		m_bitOffset(0),m_bitLen(1),m_invalidValue(4),
		m_valueOn(1),m_valueOff(0),m_valueInvOn(0),m_valueInvOff(0),
		m_onDesc("合"),m_offDesc("分"),m_invOnDesc("无效合"),m_invOffDesc("无效分"),m_level(0){
}

CModbusMapStatus::~CModbusMapStatus(){
}

bool CModbusMapStatus::init( const TiXmlElement* cfg,const bool& plc ){
	if( CModbusMapDataItem::init(cfg,plc)==false )
		return false;

	const char* c;
	int i;

	if( cfg->Attribute("bit-offset",&i) ){
		if( i<0 || i>15 )
			return false;
		m_bitOffset = i;
	}

	if( cfg->Attribute("bits",&i) ){
		if( i<0 )
			return false;
		m_bitLen = i;
	}

	if( cfg->Attribute("invalid-value",&i) ){
		if( i<0 )
			return false;
		m_invalidValue = i;
	}

	if( cfg->Attribute("value-on",&i) ){
		if( i<0 )
			return false;
		m_valueOn = i;
	}

	if( cfg->Attribute("value-off",&i) ){
		if( i<0 )
			return false;
		m_valueOff = i;
	}

	if( cfg->Attribute("value-invOn",&i) ){
		if( i<0 )
			return false;
		m_valueInvOn = i;
	}

	if( cfg->Attribute("value-invOff",&i) ){
		if( i<0 )
			return false;
		m_valueInvOff = i;
	}

	if( cfg->Attribute("level",&i) ){
		if( i<0 )
			return false;
		m_level = i;
	}

	c = cfg->Attribute("desc-on");
	if( c ){
		m_onDesc = c;
	}

	c = cfg->Attribute("desc-off");
	if( c ){
		m_offDesc = c;
	}

	c = cfg->Attribute("desc-invOn");
	if( c ){
		m_invOnDesc = c;
	}

	c = cfg->Attribute("desc-invOff");
	if( c ){
		m_invOffDesc = c;
	}

	return true;
}
}
}

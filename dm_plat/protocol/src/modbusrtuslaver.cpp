﻿///*
// * modbusrtuslaver.cpp
// *
// *  Created on: 2017年3月18日
// *      Author: work
// */
//
//#include <dm/export.hpp>
//
//#define DM_API_PROTOCOL DM_API_EXPORT
//
//#include <dm/protocol/modbusrtuslaver.hpp>
//#include <dm/os/log/logger.hpp>
//
//namespace dm{
//namespace protocol{
//
//static const char* logModule = "CModbusRtuSlaver.protocol.dm";
//
//CModbusRtuSlaver::CModbusRtuSlaver():CProtocol(),CModbusRtu(){
//	log().debug(THISMODULE "创建对象");
//}
//
//CModbusRtuSlaver::~CModbusRtuSlaver(){
//	log().debug(THISMODULE "销毁对象");
//}
//
//CFrameModbusRtu* CModbusRtuSlaver::genRxTempFrame(){
//	CFrameModbusRtu* f = new CFrameModbusRtu;
//	f->setDirDn();
//	return f;
//}
//
//CFrameModbusRtu* CModbusRtuSlaver::genTxTempFrame(){
//	CFrameModbusRtu* f = new CFrameModbusRtu;
//	f->setDirUp();
//	return f;
//}
//
//CModbusRtuSlaver::EAction CModbusRtuSlaver::dealRxModbusRtFrame( const CFrameModbusRtu&,CFrameModbusRtu& ){
//	return Action_Nothing;
//}
//
//CModbusRtuSlaver::EAction CModbusRtuSlaver::dealRx_readCoils( const CFrameModbusRtu& /*rf*/,CFrameModbusRtu& /*tf*/,const CRunTimeStamp& ){
//	log().debug(THISMODULE "默认无操作");
//	return Action_Nothing;
//}
//
//CModbusRtuSlaver::EAction CModbusRtuSlaver::dealRx_readDiscreteInputs( const CFrameModbusRtu& /*rf*/,CFrameModbusRtu& /*tf*/,const CRunTimeStamp& ){
//	log().debug(THISMODULE "默认无操作");
//	return Action_Nothing;
//}
//
//CModbusRtuSlaver::EAction CModbusRtuSlaver::dealRx_readHoldingRegisters( const CFrameModbusRtu& /*rf*/,CFrameModbusRtu& /*tf*/,const CRunTimeStamp& ){
//	log().debug(THISMODULE "默认无操作");
//	return Action_Nothing;
//}
//
//CModbusRtuSlaver::EAction CModbusRtuSlaver::dealRx_readInputRegisters( const CFrameModbusRtu& /*rf*/,CFrameModbusRtu& /*tf*/,const CRunTimeStamp& ){
//	log().debug(THISMODULE "默认无操作");
//	return Action_Nothing;
//}
//
//CModbusRtuSlaver::EAction CModbusRtuSlaver::dealRx_writeSingleCoil( const CFrameModbusRtu& /*rf*/,CFrameModbusRtu& /*tf*/,const CRunTimeStamp& ){
//	log().debug(THISMODULE "默认无操作");
//	return Action_Nothing;
//}
//
//CModbusRtuSlaver::EAction CModbusRtuSlaver::dealRx_writeSingleRegister( const CFrameModbusRtu& /*rf*/,CFrameModbusRtu& /*tf*/,const CRunTimeStamp& ){
//	log().debug(THISMODULE "默认无操作");
//	return Action_Nothing;
//}
//
//CModbusRtuSlaver::EAction CModbusRtuSlaver::dealRx_writeMultipleCoils( const CFrameModbusRtu& /*rf*/,CFrameModbusRtu& /*tf*/,const CRunTimeStamp& ){
//	log().debug(THISMODULE "默认无操作");
//	return Action_Nothing;
//}
//
//CModbusRtuSlaver::EAction CModbusRtuSlaver::dealRx_writeMultipleRegisters( const CFrameModbusRtu& /*rf*/,CFrameModbusRtu& /*tf*/,const CRunTimeStamp& ){
//	log().debug(THISMODULE "默认无操作");
//	return Action_Nothing;
//}
//
//}
//}
//
//

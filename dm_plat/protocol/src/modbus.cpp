/*
 * modbus.cpp
 *
 *  Created on: 2024年3月29日
 *      Author: Dylan.Gao
 */

#include <dm/export.hpp>

#define DM_API_PROTOCOL DM_API_EXPORT

#include <dm/protocol/modbus.hpp>

namespace dm{
namespace protocol{

CModbus::CModbus():CProtocol(){
}

CModbus::~CModbus(){
}

const CFrame& CModbus::getTxFrame(){
	return getTxModbusFrame();
}

CFrame& CModbus::getRxFrame(){
	return getRxModbusFrame();
}

}
}

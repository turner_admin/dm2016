﻿/*
 * framemodbusrtu.cpp
 *
 *  Created on: 2017年3月1日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_PROTOCOL DM_API_EXPORT

#include <dm/protocol/framemodbusrtu.hpp>

#include <dm/os/log/logger.hpp>

namespace dm{
namespace protocol{

static const char* logModule = "CFrameModbusRtu.protocol.dm";

CFrameModbusRtu::CFrameModbusRtu():CFrameModbus(),
		m_address(0){
	log().debug( THISMODULE "创建对象");
}

CFrameModbusRtu::CFrameModbusRtu( const CFrameModbusRtu& frame ):
		CFrameModbus(frame),
		m_address(frame.m_address){
	log().debug( THISMODULE "构造对象");
}

CFrameModbusRtu& CFrameModbusRtu::operator =( const CFrameModbusRtu& frame ){
	m_address = frame.m_address;
	CFrameModbus::operator =(frame);

	return *this;
}

bool CFrameModbusRtu::encode( txbuf_t& buf )const{
	dm::uint s = buf.getPos();
	buf.push(m_address);
	buf.push(getFunc());
	if( m_len>0 )
		buf.push(m_data,m_len);

	dm::uint8 crc[2];
	crcCalc( buf.getData()+s,buf.getPos()-s,crc );

	buf.push(crc,2);

	return true;
}

bool CFrameModbusRtu::decode( const rxbuf_t& buf,const pos_t& end,pos_t& start,const CFrame* txFrame ){
	if( isUpFrame() ){	// 子站返回的报文
		if( txFrame==0 ){
			// 没有发送报文，不应该有回复报文
			log().warnning(THISMODULE "子站返回报文没有发送帧");
			start = end;
			return false;
		}

		const CFrameModbusRtu* tf = dynamic_cast<const CFrameModbusRtu*>(txFrame);

		if( tf->isUpFrame() ){
			log().warnning(THISMODULE "子站返回报文,但是发送帧不是下行报文");
			start = end;	// 不是下行报文
			return false;
		}

		// 查找地址和功能码
		while( start!=end ){
			if( buf.dataAt(start)!=tf->m_address ){
				++start;
				continue;
			}

			if( (start+1)!=end && (buf.dataAt(start+1)&0x7F)!=tf->getFunc() ){
				++start;
				continue;
			}

			int len = end - start;
			if( len<5 )
					return false;

			if(  !(buf.dataAt(start+1)&0x80) ){
				// 非错误帧
				int rlen;
				switch( buf.dataAt(start+1) ){
				case ReadCoilStatus:
				case ReadInputStatus:
				case ReadHoldReg:
				case ReadInputReg:
					rlen = 1 + 1 + 1 + buf.dataAt(start+2) + 2;
					break;
				case ForceSigCoil:
				case PresetSigReg:
				case ForceMtlCoil:
				case PresetMtlReg:
					rlen = 1 + 1 + 4 + 2;
					break;
				default:
					++start;
					continue;
				}

				if( rlen>len )
					return false;

				len = rlen;
			}else{
				// 错误帧,有可能解析不正确
				++start;
				continue;
			}

			dm::uint8 crc[2];
			crc[0] = buf.dataAt(start+len-2);
			crc[1] = buf.dataAt(start+len-1);

			if( !crcCheck(buf,start,len-2,crc) ){
				if( len>=MaxBufLen ){
					start = end;
					return false;
				}

				++start;
				continue;
			}

			m_address = buf.dataAt(start);
			setFunc(buf.dataAt(start+1));

			buf.getDataAndMovePos(m_data, m_len, start);

			return true;
		}
	}else{
		// 主站下发的报文，暂时不支持作为子站
	}

	return false;
}

static dm::uint8 auchCRCHi[] = {
	0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0,
		0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
		0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
		0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
		0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1,
		0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
		0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1,
		0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
		0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0,
		0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40,
		0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1,
		0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
		0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0,
		0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40,
		0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
		0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
		0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0,
		0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
		0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
		0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
		0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
		0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40,
		0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1,
		0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
		0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0,
		0x80, 0x41, 0x00, 0xC1, 0x81, 0x40
};

static dm::uint8 auchCRCLo[] = {
	0x00, 0xC0, 0xC1, 0x01, 0xC3, 0x03, 0x02, 0xC2, 0xC6, 0x06,
		0x07, 0xC7, 0x05, 0xC5, 0xC4, 0x04, 0xCC, 0x0C, 0x0D, 0xCD,
		0x0F, 0xCF, 0xCE, 0x0E, 0x0A, 0xCA, 0xCB, 0x0B, 0xC9, 0x09,
		0x08, 0xC8, 0xD8, 0x18, 0x19, 0xD9, 0x1B, 0xDB, 0xDA, 0x1A,
		0x1E, 0xDE, 0xDF, 0x1F, 0xDD, 0x1D, 0x1C, 0xDC, 0x14, 0xD4,
		0xD5, 0x15, 0xD7, 0x17, 0x16, 0xD6, 0xD2, 0x12, 0x13, 0xD3,
		0x11, 0xD1, 0xD0, 0x10, 0xF0, 0x30, 0x31, 0xF1, 0x33, 0xF3,
		0xF2, 0x32, 0x36, 0xF6, 0xF7, 0x37, 0xF5, 0x35, 0x34, 0xF4,
		0x3C, 0xFC, 0xFD, 0x3D, 0xFF, 0x3F, 0x3E, 0xFE, 0xFA, 0x3A,
		0x3B, 0xFB, 0x39, 0xF9, 0xF8, 0x38, 0x28, 0xE8, 0xE9, 0x29,
		0xEB, 0x2B, 0x2A, 0xEA, 0xEE, 0x2E, 0x2F, 0xEF, 0x2D, 0xED,
		0xEC, 0x2C, 0xE4, 0x24, 0x25, 0xE5, 0x27, 0xE7, 0xE6, 0x26,
		0x22, 0xE2, 0xE3, 0x23, 0xE1, 0x21, 0x20, 0xE0, 0xA0, 0x60,
		0x61, 0xA1, 0x63, 0xA3, 0xA2, 0x62, 0x66, 0xA6, 0xA7, 0x67,
		0xA5, 0x65, 0x64, 0xA4, 0x6C, 0xAC, 0xAD, 0x6D, 0xAF, 0x6F,
		0x6E, 0xAE, 0xAA, 0x6A, 0x6B, 0xAB, 0x69, 0xA9, 0xA8, 0x68,
		0x78, 0xB8, 0xB9, 0x79, 0xBB, 0x7B, 0x7A, 0xBA, 0xBE, 0x7E,
		0x7F, 0xBF, 0x7D, 0xBD, 0xBC, 0x7C, 0xB4, 0x74, 0x75, 0xB5,
		0x77, 0xB7, 0xB6, 0x76, 0x72, 0xB2, 0xB3, 0x73, 0xB1, 0x71,
		0x70, 0xB0, 0x50, 0x90, 0x91, 0x51, 0x93, 0x53, 0x52, 0x92,
		0x96, 0x56, 0x57, 0x97, 0x55, 0x95, 0x94, 0x54, 0x9C, 0x5C,
		0x5D, 0x9D, 0x5F, 0x9F, 0x9E, 0x5E, 0x5A, 0x9A, 0x9B, 0x5B,
		0x99, 0x59, 0x58, 0x98, 0x88, 0x48, 0x49, 0x89, 0x4B, 0x8B,
		0x8A, 0x4A, 0x4E, 0x8E, 0x8F, 0x4F, 0x8D, 0x4D, 0x4C, 0x8C,
		0x44, 0x84, 0x85, 0x45, 0x87, 0x47, 0x46, 0x86, 0x82, 0x42,
		0x43, 0x83, 0x41, 0x81, 0x80, 0x40
};

void CFrameModbusRtu::crcCalc( const dm::uint8* buf,const int& size,dm::uint8* sum )const{
	sum[0] = 0xff;
	sum[1] = 0xff;
	dm::uint8 ucIndex;
	for(int i = 0 ; i < size; i++)
	{
		ucIndex = sum[0] ^ buf[i];
		sum[0] = sum[1] ^ auchCRCHi[ucIndex];
		sum[1] = auchCRCLo[ucIndex];
	}
}

bool CFrameModbusRtu::crcCheck( const rxbuf_t& buf,const pos_t& start,const int& size,const dm::uint8* sum )const{
	dm::uint8 c[2];
	dm::uint8 ucIndex;

	c[0] = 0xff;
	c[1] = 0xff;

	pos_t p = start;
	for(int i = 0 ; i < size; i++)
	{
		ucIndex = c[0] ^ buf.dataAt(p);
		c[0] = c[1] ^ auchCRCHi[ucIndex];
		c[1] = auchCRCLo[ucIndex];
		++p;
	}

	return (c[0]==sum[0] && c[1]==sum[1]);
}

}
}



﻿/*
 * modbustcp.cpp
 *
 *  Created on: 2017年10月30日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_PROTOCOL DM_API_EXPORT

#include <dm/protocol/modbustcp.hpp>
#include <dm/os/log/logger.hpp>

namespace dm{
namespace protocol{

static const char* logModule = "CModbusTcp.protocol.dm";

CModbusTcp::CModbusTcp():CModbus(){
	log().debug(THISMODULE "创建对象");
}

CModbusTcp::~CModbusTcp(){
	log().debug(THISMODULE "销毁对象");
}

dm::protocol::CFrameModbus& CModbusTcp::getTxModbusFrame(){
	return m_txFrame;
}

dm::protocol::CFrameModbus& CModbusTcp::getRxModbusFrame(){
	return m_rxFrame;
}

}
}



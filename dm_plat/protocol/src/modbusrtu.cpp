﻿/*
 * modbusrtu.cpp
 *
 *  Created on: 2017年3月14日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_PROTOCOL DM_API_EXPORT

#include <dm/protocol/modbusrtu.hpp>
#include <dm/os/log/logger.hpp>

namespace dm{
namespace protocol{

static const char* logModule = "CModbusRtu.protocol.dm";

CModbusRtu::CModbusRtu():CModbus(),m_curAddr(0){
	log().debug(THISMODULE "创建对象，初始化地址为0");
}

CModbusRtu::~CModbusRtu(){
	log().debug(THISMODULE "销毁对象");
}


/**
 * 过滤非本地帧报文
 * @return
 */
bool CModbusRtu::filterRxFrame()const{
	address_t fa = m_rxFrame.getAddress();
	if( fa!=m_curAddr && fa!=CFrameModbusRtu::Address_All ){
		return true;
	}else
		return false;
}

CFrameModbus& CModbusRtu::getRxModbusFrame(){
	return m_rxFrame;
}

CFrameModbus& CModbusRtu::getTxModbusFrame(){
	return m_txFrame;
}

}
}



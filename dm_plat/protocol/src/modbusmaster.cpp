/*
 * modbusmaster.cpp
 *
 *  Created on: 2024年4月10日
 *      Author: Dylan.Gao
 */

#include <dm/export.hpp>

#define DM_API_PROTOCOL DM_API_EXPORT

#include <dm/protocol/modbusmaster.hpp>
#include <dm/os/log/logger.hpp>

namespace dm{
namespace protocol{

static const char* logModule = "CModbusMaster.protocol.dm";

CModbusMaster::CModbusMaster():CModbus(){
	log().debug(THISMODULE "create object");
}

CModbusMaster::~CModbusMaster(){
	log().debug(THISMODULE "destroy object");
}

CModbusMaster::EAction CModbusMaster::dealRxFrame( const ts_t& ts,const rts_t& rts ){
	noWait();

	CFrameModbus& rxFrame = getRxModbusFrame();
	CFrameModbus& txFrame = getTxModbusFrame();

	if( txFrame.getFunc()!=rxFrame.getFunc() ){
		log().debug(THISMODULE "接收到的功能码%02X与发送%02X不一致",getRxModbusFrame().getFunc(),getTxModbusFrame().getFunc());
		return Action_Nothing;
	}

	switch( rxFrame.getFunc() ){
	case CFrameModbus::ReadCoilStatus:
		log().debug(THISMODULE "ReadCoilStatus");
		return dealRxResponse_readCoils( rxFrame,txFrame,ts,rts );

	case CFrameModbus::ReadInputStatus:
		log().debug(THISMODULE "ReadInputStatus");
		return dealRxResponse_readDiscreteInputs( rxFrame,txFrame,ts,rts );

	case CFrameModbus::ReadHoldReg:
		log().debug(THISMODULE "ReadHoldReg");
		return dealRxResponse_readHoldingRegisters(rxFrame,txFrame,ts,rts );

	case CFrameModbus::ReadInputReg:
		log().debug(THISMODULE "ReadInputReg");
		return dealRxResponse_readInputRegisters(rxFrame,txFrame,ts,rts );

	case CFrameModbus::ForceSigCoil:
		log().debug(THISMODULE "ForceSigCoil");
		return dealRxResponse_writeSingleCoil(rxFrame,txFrame,ts,rts );

	case CFrameModbus::PresetSigReg:
		log().debug(THISMODULE "PresetSigReg");
		return dealRxResponse_writeSingleRegister(rxFrame,txFrame,ts,rts );

	case CFrameModbus::ForceMtlCoil:
		log().debug(THISMODULE "ForceMtlCoil");
		return dealRxResponse_writeMultipleCoils( rxFrame,txFrame,ts,rts );

	case CFrameModbus::PresetMtlReg:
		log().debug(THISMODULE "PresetMtlReg");
		return dealRxResponse_writeMultipleRegisters( rxFrame,txFrame,ts,rts );

	default:
		log().debug(THISMODULE "未支持功能码%02X",rxFrame.getFunc());
		return Action_Nothing;
	}
}

/**
 * 开始召唤任务。
 * 默认处理函数设置启动标志，无其他操作。子类应该需要重载该函数。
 * @param
 * @return
 */
CModbusMaster::EAction CModbusMaster::taskStart_call( CFrameModbus&,const ts_t& /*ts*/,const rts_t& /*rts*/ ){
	log().debug(THISMODULE "默认无操作");
	m_taskCall = Started;
	return Action_Nothing;
}

/**
 * 执行召唤任务
 * 默认处理函数设置召唤成功标志，无其他操作。
 * @param
 * @return
 */
CModbusMaster::EAction CModbusMaster::taskDo_call( CFrameModbus&,const ts_t& /*ts*/,const rts_t& /*rts*/ ){
	log().debug(THISMODULE "默认无操作");
	m_taskCall = Success;
	return Action_Nothing;
}

/**
 * 开启时钟同步任务
 * 默认处理函数设置开始标志，无其他操作。
 * 一般子类应该设置对时报文，并设置启动标志。
 * @param
 * @return
 */
CModbusMaster::EAction CModbusMaster::taskStart_syncClock( CFrameModbus&,const ts_t& /*ts*/,const rts_t& /*rts*/ ){
	log().debug(THISMODULE "默认无操作");
	m_taskClock = Started;
	return Action_Nothing;
}

CModbusMaster::EAction CModbusMaster::taskDo_syncClock( CFrameModbus&,const ts_t& /*ts*/,const rts_t& /*rts*/ ){
	log().debug(THISMODULE "默认无操作");
	m_taskClock = Success;
	return Action_Nothing;
}

CModbusMaster::EAction CModbusMaster::taskStart_remoteControl( CFrameModbus&,const ts_t& /*ts*/,const rts_t& /*rts*/ ){
	log().debug(THISMODULE "默认无操作");
	m_taskRmtCtl = Started;
	return Action_Nothing;
}

CModbusMaster::EAction CModbusMaster::taskDo_remoteControl( CFrameModbus&,const ts_t& /*ts*/,const rts_t& /*rts*/ ){
	log().debug(THISMODULE "默认无操作");
	m_taskRmtCtl = Success;
	return Action_Nothing;
}

CModbusMaster::EAction CModbusMaster::taskStart_parameters( CFrameModbus&,const ts_t& /*ts*/,const rts_t& /*rts*/ ){
	log().debug(THISMODULE "默认无操作");
	m_taskParas = Started;
	return Action_Nothing;
}

CModbusMaster::EAction CModbusMaster::taskDo_parameters( CFrameModbus&,const ts_t& /*ts*/,const rts_t& /*rts*/ ){
	log().debug(THISMODULE "默认无操作");
	m_taskParas = Success;
	return Action_Nothing;
}

CModbusMaster::EAction CModbusMaster::dealRxResponse_readCoils( const CFrameModbus& rf,CFrameModbus& tf,const ts_t& ts,const rts_t& rts ){
	dm::uint16 startAddr = tf.getData_uint16(0);
	dm::uint16 quantity = tf.getData_uint16(2);

	if( rf.isErrorUpFrame() ){
		CFrameModbus::EErrCode errCode;
		rf.getError(errCode);

		log().info(THISMODULE "收到错误帧:%02X",errCode);
		for( dm::uint16 i=0;i<quantity;++i ){
			coils_01_error( startAddr+i,errCode,ts,rts );
		}
	}else{
		if( rf.getData_uint8()!=tf.getBytes(quantity) ){
			log().warnning(THISMODULE "收到数据字节数%d 与请求数量%d->%d不匹配",rf.getData_uint8(),quantity,tf.getBytes(quantity));
			return Action_Nothing;
		}

		for( dm::uint16 i=0;i<quantity;++i )
			coils_01( startAddr+i,rf.getData_bit(i,1)?0x01:0x00,ts,rts);
	}

	return timedRefresh(ts,rts);
}

CModbusMaster::EAction CModbusMaster::dealRxResponse_readDiscreteInputs( const CFrameModbus& rf,CFrameModbus& tf,const ts_t& ts,const rts_t& rts ){
	dm::uint16 startAddr = tf.getData_uint16(0);
	dm::uint16 quantity = tf.getData_uint16(2);

	if( rf.isErrorUpFrame() ){
		CFrameModbus::EErrCode errCode;
		rf.getError(errCode);
		log().info(THISMODULE "收到错误帧:%02X",errCode);
		for( dm::uint16 i=0;i<quantity;++i ){
			discreteInputs_02_error( startAddr+i,errCode,ts,rts );
		}
	}else{
		if( rf.getData_uint8()!=tf.getBytes(quantity) ){
			log().warnning(THISMODULE "收到数据字节数%d 与请求数量%d->%d不匹配",rf.getData_uint8(),quantity,tf.getBytes(quantity));
			return Action_Nothing;
		}

		for( dm::uint16 i=0;i<quantity;++i )
			discreteInputs_02( startAddr+i,rf.getData_bit(i,1),ts,rts);
	}

	return timedRefresh(ts,rts);
}

CModbusMaster::EAction CModbusMaster::dealRxResponse_readHoldingRegisters( const CFrameModbus& rf,CFrameModbus& tf,const ts_t& ts,const rts_t& rts ){
	dm::uint16 startAddr = tf.getData_uint16(0);
	dm::uint16 quantity = tf.getData_uint16(2);

	if( rf.isErrorUpFrame() ){
		CFrameModbus::EErrCode errCode;
		rf.getError(errCode);
		log().info(THISMODULE "收到错误帧:%02X",errCode);
		for( dm::uint16 i=0;i<quantity;++i ){
			holdingRegisters_03_error( startAddr+i,errCode,ts,rts );
		}
	}else{
		if( rf.getData_uint8()!=(quantity*2) ){
			log().warnning(THISMODULE "收到数据字节数%d 与请求数量%d->%d不匹配",rf.getData_uint8(),quantity,quantity*2);
			return Action_Nothing;
		}

		for( dm::uint16 i=0;i<quantity;++i )
			holdingRegisters_03( startAddr+i,rf.getData_uint16(1+i*2),ts,rts );
	}

	return timedRefresh(ts,rts);
}

CModbusMaster::EAction CModbusMaster::dealRxResponse_readInputRegisters( const CFrameModbus& rf,CFrameModbus& tf,const ts_t& ts,const rts_t& rts ){
	dm::uint16 startAddr = tf.getData_uint16(0);
	dm::uint16 quantity = tf.getData_uint16(2);

	if( rf.isErrorUpFrame() ){
		CFrameModbus::EErrCode errCode;
		rf.getError(errCode);
		log().info(THISMODULE "收到错误帧:%02X",errCode);
		for( dm::uint16 i=0;i<quantity;++i ){
			inputRegisters_04_error( startAddr+i,errCode,ts,rts );
		}
	}else{
		if( rf.getData_uint8()!=(quantity*2) ){
			log().warnning(THISMODULE "收到数据字节数%d 与请求数量%d->%d不匹配",rf.getData_uint8(),quantity,quantity*2);
			return Action_Nothing;
		}

		for( dm::uint16 i=0;i<quantity;++i )
			inputRegisters_04( startAddr+i,rf.getData_uint16(1+i*2),ts,rts);
	}

	return timedRefresh(ts,rts);
}

CModbusMaster::EAction CModbusMaster::dealRxResponse_writeSingleCoil( const CFrameModbus& rf,CFrameModbus& /*tf*/,const ts_t& ts,const rts_t& rts ){
	if( rf.isErrorUpFrame() ){
		log().info(THISMODULE "收到错误帧");
	}

	return timedRefresh(ts,rts);
}

CModbusMaster::EAction CModbusMaster::dealRxResponse_writeSingleRegister( const CFrameModbus& rf,CFrameModbus& /*tf*/,const ts_t& ts,const rts_t& rts ){
	if( rf.isErrorUpFrame() ){
		log().info(THISMODULE "收到错误帧");
	}

	return timedRefresh(ts,rts);
}

CModbusMaster::EAction CModbusMaster::dealRxResponse_writeMultipleCoils( const CFrameModbus& rf,CFrameModbus& /*tf*/,const ts_t& ts,const rts_t& rts ){
	if( rf.isErrorUpFrame() ){
		log().info(THISMODULE "收到错误帧");
	}

	return timedRefresh(ts,rts);
}

CModbusMaster::EAction CModbusMaster::dealRxResponse_writeMultipleRegisters( const CFrameModbus& rf,CFrameModbus& /*tf*/,const ts_t& ts,const rts_t& rts ){
	if( rf.isErrorUpFrame() ){
		log().info(THISMODULE "收到错误帧");
	}

	return timedRefresh(ts,rts);
}

void CModbusMaster::coils_01_error( const dm::uint16& address,const CFrameModbus::EErrCode& ec,const ts_t& /*ts*/,const rts_t& /*rts*/ ){
	log().info(THISMODULE "默认无处理。地址：%d,失败原因%d",address,ec);
}

/**
 * 处理返回的数据
 * 子类应该重载该函数
 * @param address
 * @param value
 */
void CModbusMaster::coils_01( const dm::uint16& address,const dm::uint8& value,const ts_t& /*ts*/,const rts_t& /*rts*/ ){
	log().info(THISMODULE "默认无处理。地址：%d,值:%d",address,value);
}

void CModbusMaster::discreteInputs_02_error( const dm::uint16& address,const CFrameModbus::EErrCode& ec,const ts_t& /*ts*/,const rts_t& /*rts*/ ){
	log().info(THISMODULE "默认无处理。地址：%d,失败原因%d",address,ec);
}

/**
 * 处理返回的数据
 * 子类应该重载该函数
 * @param address
 * @param value
 */
void CModbusMaster::discreteInputs_02( const dm::uint16& address,const dm::uint8& value,const ts_t& /*ts*/,const rts_t& /*rts*/ ){
	log().info(THISMODULE "默认无处理。地址：%d,值:%d",address,value);
}

void CModbusMaster::holdingRegisters_03_error( const dm::uint16& address,const CFrameModbus::EErrCode& ec,const ts_t& /*ts*/,const rts_t& /*rts*/ ){
	log().info(THISMODULE "默认无处理。地址：%d,失败原因%d",address,ec);
}

/**
 * 处理返回的数据
 * 子类应该重载该函数
 * @param address
 * @param value
 */
void CModbusMaster::holdingRegisters_03( const dm::uint16& address,const dm::uint16& value,const ts_t& /*ts*/,const rts_t& /*rts*/ ){
	log().info(THISMODULE "默认无处理。地址：%d,值:%d",address,value);
}

void CModbusMaster::inputRegisters_04_error( const dm::uint16& address,const CFrameModbus::EErrCode& ec,const ts_t& /*ts*/,const rts_t& /*rts*/ ){
	log().info(THISMODULE "默认无处理。地址：%d,失败原因%d",address,ec);
}

/**
 * 处理返回的数据
 * 子类应该重载该函数
 * @param address
 * @param value
 */
void CModbusMaster::inputRegisters_04( const dm::uint16& address,const dm::uint16& value,const ts_t& /*ts*/,const rts_t& /*rts*/ ){
	log().info(THISMODULE "默认无处理。地址：%d,值:%d",address,value);
}

/**
 * 开始召唤任务。
 * 这个函数被转交给专用规约处理函数。子类不应该再重载本函数。
 * @param txFrame
 * @return
 */
CModbusMaster::EAction CModbusMaster::taskStart_call( const ts_t& ts,const rts_t& rts ){
	EAction act = checkWait(rts);
	if( act!=Action_Nothing )
		return act;

	act = taskStart_call(getTxModbusFrame(),ts,rts);
	if( act==Action_Send || act==Action_SendAndReTime )
		wait(rts);

	return act;
}

/**
 * 执行召唤任务。
 * 这个函数被转交给规约专用处理函数。子类不应该再重载本函数。
 * @param txFrame
 * @return
 */
CModbusMaster::EAction CModbusMaster::taskDo_call( const ts_t& ts,const rts_t& rts ){
	EAction act = checkWait(rts);
	if( act!=Action_Nothing )
		return act;

	act = taskDo_call(getTxModbusFrame(),ts,rts);
	if( act==Action_Send || act==Action_SendAndReTime )
		wait(rts);

	return act;
}

/**
 * 开始时钟同步任务。
 * 这个函数被转交给规约专用处理函数。子类不应该再重载本函数。
 * @param txFrame
 * @return
 */
CModbusMaster::EAction CModbusMaster::taskStart_syncClock( const ts_t& ts,const rts_t& rts ){
	EAction act = checkWait( rts );
	if( act!=Action_Nothing )
		return act;

	act = taskStart_syncClock(getTxModbusFrame(),ts,rts);
	if( act==Action_Send || act==Action_SendAndReTime )
		wait(rts);

	return act;
}

/**
 * 执行时钟同步任务
 * 这个函数被转交给规约专用处理函数。子类不应该再重载本函数。
 * @param txFrame
 * @return
 */
CModbusMaster::EAction CModbusMaster::taskDo_syncClock( const ts_t& ts,const rts_t& rts ){
	EAction act = checkWait( rts );
	if( act!=Action_Nothing )
		return act;

	act = taskDo_syncClock(getTxModbusFrame(),ts,rts);
	if( act==Action_Send || act==Action_SendAndReTime )
		wait(rts);

	return act;
}

/**
 * 开启远控任务
 * 这个函数被转交给规约专用处理函数。子类不应该再重载本函数。
 * @param txFrame
 * @return
 */
CModbusMaster::EAction CModbusMaster::taskStart_remoteControl( const ts_t& ts,const rts_t& rts ){
	EAction act = checkWait( rts );
	if( act!=Action_Nothing )
		return act;

	act = taskStart_remoteControl(getTxModbusFrame(),ts,rts);
	if( act==Action_Send || act==Action_SendAndReTime )
		wait(rts);

	return act;
}

CModbusMaster::EAction CModbusMaster::taskDo_remoteControl( const ts_t& ts,const rts_t& rts ){
	EAction act = checkWait( rts );
	if( act!=Action_Nothing )
		return act;

	act = taskDo_remoteControl(getTxModbusFrame(),ts,rts);
	if( act==Action_Send || act==Action_SendAndReTime )
		wait(rts);

	return act;
}

CModbusMaster::EAction CModbusMaster::taskStart_parameters( const ts_t& ts,const rts_t& rts ){
	EAction act = checkWait( rts );
	if( act!=Action_Nothing )
		return act;

	act = taskStart_parameters(getTxModbusFrame(),ts,rts);
	if( act==Action_Send || act==Action_SendAndReTime )
		wait(rts);

	return act;
}

CModbusMaster::EAction CModbusMaster::taskDo_parameters( const ts_t& ts,const rts_t& rts ){
	EAction act = checkWait( rts );
	if( act!=Action_Nothing )
		return act;

	act = taskDo_parameters(getTxModbusFrame(),ts,rts);
	if( act==Action_Send || act==Action_SendAndReTime )
		wait(rts);

	return act;
}

}
}

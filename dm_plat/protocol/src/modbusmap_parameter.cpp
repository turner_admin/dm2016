﻿/*
 * modbusmap_parameter.cpp
 *
 *  Created on: 2019-4-14
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_PROTOCOL DM_API_EXPORT

#include <dm/protocol/modbusmap_parameter.hpp>

namespace dm{
namespace protocol{

CModbusMapParameter::CModbusMapParameter():CModbusMapItem(),
		m_type(DtUint16){
}

bool CModbusMapParameter::init( const TiXmlElement* cfg,const bool& plc ){
	if( CModbusMapItem::init(cfg,plc)==false )
		return false;

	const char* c;

	c = cfg->Attribute("data-type");
	if( c ){
		if( strcmp(c,"int16")==0 )
			m_type = DtInt16;
		else if( strcmp(c,"uint16")==0 )
			m_type = DtUint16;
		else if( strcmp(c,"int32")==0 )
			m_type = DtInt32;
		else if( strcmp(c,"uint32")==0 )
			m_type = DtUint32;
		else if( strcmp(c,"int32inv")==0 )
			m_type = DtInt32Inv;
		else if( strcmp(c,"uint32inv")==0 )
			m_type = DtUint32Inv;
		else if( strcmp(c,"int64")==0 )
			m_type = DtInt64;
		else if( strcmp(c,"uint64")==0 )
			m_type = DtUint64;
		else if( strcmp(c,"int64inv")==0 )
			m_type = DtInt64Inv;
		else if( strcmp(c,"uint64inv")==0 )
			m_type = DtUint64Inv;
		else if( strcmp(c,"float32")==0 )
			m_type = DtFloat32;
		else if( strcmp(c,"float32inv")==0 )
			m_type = DtFloat32Inv;
		else if( strcmp(c,"float64")==0 )
			m_type = DtFloat64;
		else if( strcmp(c,"float64inv")==0 )
			m_type = DtFloat64Inv;
		else if( strcmp(c,"bcdUint16")==0 )
			m_type = DtBcdUint16;
		else if( strcmp(c,"bcdUint32")==0 )
			m_type = DtBcdUint32;
		else if( strcmp(c,"bcdUint32Inv")==0 )
			m_type = DtBcdUint32Inv;
		else if( strcmp(c,"bcdUint64")==0 )
			m_type = DtBcdUint64;
		else if( strcmp(c,"bcdUint64Inv")==0 )
			m_type = DtBcdUint64Inv;
		else
			return false;
	}

	return true;
}

}
}

﻿/*
 * protocolbase.cpp
 *
 *  Created on: 2017年4月13日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_PROTOCOL DM_API_EXPORT

#include <dm/protocol/protocolbase.hpp>

namespace dm{
namespace protocol{

CProtocolBase::CProtocolBase():
		m_refreshInterval(100),
		m_lastSendTime(),
		m_lastRecvTime(),m_lastLinkDsct(),m_lastLinkStl(),
		m_lastWait(),m_waitTimeout(0),m_timeoutTimes(0),m_timeoutMaxTimes(0){
}

CProtocolBase::~CProtocolBase(){
}

/**
 * 链路建立时的操作
 * 默认复位规约
 */
void CProtocolBase::onLinkSettled( const ts_t& ts,const rts_t& rts ){
	reset( ts,rts );
}

/**
 * 链路断开时的操作
 * 默认复位规约
 */
void CProtocolBase::onLinkClosed( const ts_t& /*ts*/,const rts_t& /*rts*/ ){
}

/**
 * 过滤接收到的帧
 * @param
 * @return 是否不处理该帧
 * - true 过滤不处理
 * - false 有效帧
 */
bool CProtocolBase::filterRxFrame()const{
	return false;
}

/**
 * 延时刷新
 * @param txFrame 准备发送的帧
 * @return
 */
CProtocolBase::EAction CProtocolBase::timedRefresh( const ts_t& /*ts*/,const rts_t& /*rts*/ ){
	return Action_Nothing;
}

/**
 * 复位规约
 * @param now
 */
void CProtocolBase::reset( const ts_t& /*ts*/,const rts_t& rts ){
	m_lastSendTime = rts;
	m_lastRecvTime = rts;
	noWait();
}

/**
 * 更新接收时刻
 * 在接收到报文后调用该函数
 */
void CProtocolBase::updateRecvTime( dm::uint32 bytes,const rts_t& rts ){
	m_rxCnt += bytes;
	m_lastRecvTime = rts;
}

/**
 * 更新发送时刻
 * 在发送完报文后调用该函数
 */
void CProtocolBase::updateSendTime( dm::uint32 bytes,const rts_t& rts ){
	m_txCnt += bytes;
	m_lastSendTime = rts;
}

/**
 * 这个函数实现了简单的超时判断
 * @param now
 * @return
 */
CProtocolBase::EAction CProtocolBase::checkWait( const rts_t& rts ){
	if( ifWait() ){
		if( !ifWaitTimeout(rts) )
			return Action_ReTime;

		if( ifWaitOverTimes() )
			return Action_CloseLink;

		m_lastWait.setInv();
		return Action_ResetBuf;
	}

	return Action_Nothing;
}

}
}



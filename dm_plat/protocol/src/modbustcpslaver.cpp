﻿///*
// * modbustcpslaver.cpp
// *
// *  Created on: 2017年10月30日
// *      Author: work
// */
//
//#include <dm/export.hpp>
//
//#define DM_API_PROTOCOL DM_API_EXPORT
//
//#include <dm/protocol/modbustcpslaver.hpp>
//#include <dm/os/log/logger.hpp>
//
//namespace dm{
//namespace protocol{
//
//static const char* logModule = "CModbusTcpSlaver.protocol.dm";
//
//CModbusTcpSlaver::CModbusTcpSlaver():CProtocol(),CModbusTcp(){
//	log().debug(THISMODULE "创建对象");
//}
//
//CModbusTcpSlaver::~CModbusTcpSlaver(){
//	log().debug(THISMODULE "销毁对象");
//}
//
///**
// * 处理接收到报文
// * @param now
// * @return
// */
//CModbusTcpSlaver::EAction CModbusTcpSlaver::dealRxFrame( const dm::CRunTimeStamp &/*now*/ ){
//	// todo
//	return Action_Nothing;
//}
//
//CFrameModbusTcp* CModbusTcpSlaver::genRxTempFrame(){
//	CFrameModbusTcp* f = new CFrameModbusTcp;
//	f->setDnFrame();
//	log().debug(THISMODULE "设在下行报文");
//	return f;
//}
//
//CFrameModbusTcp* CModbusTcpSlaver::genTxTempFrame(){
//	CFrameModbusTcp* f = new CFrameModbusTcp;
//	f->setUpFrame();
//	log().debug(THISMODULE "设在上行报文");
//	return f;
//}
//
//CModbusTcpSlaver::EAction CModbusTcpSlaver::dealRx_readCoils( const CFrameModbusTcp& /*rf*/,CFrameModbusTcp& /*tf*/,const CRunTimeStamp& /*now*/ ){
//	return Action_Nothing;
//}
//
//CModbusTcpSlaver::EAction CModbusTcpSlaver::dealRx_readDiscreteInputs( const CFrameModbusTcp& /*rf*/,CFrameModbusTcp& /*tf*/,const CRunTimeStamp& /*now*/ ){
//	return Action_Nothing;
//}
//
//CModbusTcpSlaver::EAction CModbusTcpSlaver::dealRx_readHoldingRegisters( const CFrameModbusTcp& /*rf*/,CFrameModbusTcp& /*tf*/,const CRunTimeStamp& /*now*/ ){
//	return Action_Nothing;
//}
//
//CModbusTcpSlaver::EAction CModbusTcpSlaver::dealRx_readInputRegisters( const CFrameModbusTcp& /*rf*/,CFrameModbusTcp& /*tf*/,const CRunTimeStamp& /*now*/ ){
//	return Action_Nothing;
//}
//
//CModbusTcpSlaver::EAction CModbusTcpSlaver::dealRx_writeSingleCoil( const CFrameModbusTcp& /*rf*/,CFrameModbusTcp& /*tf*/,const CRunTimeStamp& /*now*/ ){
//	return Action_Nothing;
//}
//
//CModbusTcpSlaver::EAction CModbusTcpSlaver::dealRx_writeSingleRegister( const CFrameModbusTcp& /*rf*/,CFrameModbusTcp& /*tf*/,const CRunTimeStamp& /*now*/ ){
//	return Action_Nothing;
//}
//
//CModbusTcpSlaver::EAction CModbusTcpSlaver::dealRx_writeMultipleCoils( const CFrameModbusTcp& /*rf*/,CFrameModbusTcp& /*tf*/,const CRunTimeStamp& /*now*/ ){
//	return Action_Nothing;
//}
//
//CModbusTcpSlaver::EAction CModbusTcpSlaver::dealRx_writeMultipleRegisters( const CFrameModbusTcp& /*rf*/,CFrameModbusTcp& /*tf*/,const CRunTimeStamp& /*now*/ ){
//	return Action_Nothing;
//}
//
//}
//}

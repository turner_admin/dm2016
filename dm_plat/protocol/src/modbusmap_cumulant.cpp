﻿/*
 * modbusmap_cumulant.cpp
 *
 *  Created on: 2019-4-14
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_PROTOCOL DM_API_EXPORT

#include <dm/protocol/modbusmap_cumulant.hpp>

namespace dm{
namespace protocol{

CModbusMapCumulant::CModbusMapCumulant():CModbusMapDataItem(),
		m_type(DtUint16),m_maxCode(0),m_coef(1.0),m_delta(0){
}

bool CModbusMapCumulant::init( const TiXmlElement* cfg,const bool& plc ){
	if( CModbusMapDataItem::init(cfg,plc)==false )
		return false;

	const char* c;
	double f;

	c = cfg->Attribute("data-type");
	if( c ){
		if( strcmp(c,"int16")==0 )
			m_type = DtInt16;
		else if( strcmp(c,"uint16")==0 )
			m_type = DtUint16;
		else if( strcmp(c,"int32")==0 )
			m_type = DtInt32;
		else if( strcmp(c,"uint32")==0 )
			m_type = DtUint32;
		else if( strcmp(c,"int32inv")==0 )
			m_type = DtInt32Inv;
		else if( strcmp(c,"uint32inv")==0 )
			m_type = DtUint32Inv;
		else if( strcmp(c,"int64")==0 )
			m_type = DtInt64;
		else if( strcmp(c,"uint64")==0 )
			m_type = DtUint64;
		else if( strcmp(c,"int64inv")==0 )
			m_type = DtInt64Inv;
		else if( strcmp(c,"uint64inv")==0 )
			m_type = DtUint64Inv;
		else if( strcmp(c,"float32")==0 )
			m_type = DtFloat32;
		else if( strcmp(c,"float32inv")==0 )
			m_type = DtFloat32Inv;
		else if( strcmp(c,"float64")==0 )
			m_type = DtFloat64;
		else if( strcmp(c,"float64inv")==0 )
			m_type = DtFloat64Inv;
		else if( strcmp(c,"bcdUint16")==0 )
			m_type = DtBcdUint16;
		else if( strcmp(c,"bcdUint32")==0 )
			m_type = DtBcdUint32;
		else if( strcmp(c,"bcdUint32Inv")==0 )
			m_type = DtBcdUint32Inv;
		else if( strcmp(c,"bcdUint64")==0 )
			m_type = DtBcdUint64;
		else if( strcmp(c,"bcdUint64Inv")==0 )
			m_type = DtBcdUint64Inv;
		else
			return false;
	}

	if( cfg->Attribute("max",&f) ){
		m_maxCode = f;
	}

	if( cfg->Attribute("coef",&f) ){
		m_coef = f;
	}

	if( cfg->Attribute("delta",&f) ){
		m_delta = f;
	}

	return true;
}

}
}

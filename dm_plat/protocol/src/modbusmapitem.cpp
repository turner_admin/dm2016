﻿/*
 * modbusmapitem.cpp
 *
 *  Created on: 2019-4-13
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_PROTOCOL DM_API_EXPORT

#include <dm/protocol/modbusmapitem.hpp>

namespace dm{
namespace protocol{

static const char* DtString[CModbusMapItem::DtUnknow] = {
		"int16","uint16",
		"int32","int32inv","uint32","uint32inv",
		"int64","int64inv","uint64","uint64inv",
		"float32","float32inv","float64","float64inv",
		"bcdUint16",
		"bcdUint32","bcdUint32inv",
		"bcdUint64","bcdUint64inv"
};

const char* CModbusMapItem::typeString( const EDataType& type ){
	if( type<0 || type>=DtUnknow )
		return "Unknow";
	return DtString[type];
}

int CModbusMapItem::typeWords( const EDataType& type ){
	switch( type ){
	case DtInt16:
	case DtUint16:
	case	DtBcdUint16:
		return 1;

	case DtInt32:
	case DtInt32Inv:
	case DtUint32:
	case DtUint32Inv:
	case	DtFloat32:
	case	DtFloat32Inv:
	case	DtBcdUint32:
	case	DtBcdUint32Inv:
		return 2;

	case DtInt64:
	case DtInt64Inv:
	case DtUint64:
	case DtUint64Inv:
	case DtFloat64:
	case DtFloat64Inv:
	case DtBcdUint64:
	case DtBcdUint64Inv:
		return 4;
	default:
		return 0;
	}
}

CModbusMapItem::CModbusMapItem():m_name(),m_desc(),m_fun(dm::protocol::CFrameModbus::ReadCoilStatus),m_addr(0xFFFF){
}

CModbusMapItem::~CModbusMapItem(){
}

bool CModbusMapItem::init( const TiXmlElement* cfg,const bool& plc ){
	const char* a =	cfg->Attribute("name");
	if( a==NULL )
		return false;
	m_name = a;

	a = cfg->Attribute("desc");
	if( a==NULL )
		return false;
	m_desc = a;

	int i;
	if( cfg->Attribute("fun",&i)==NULL )
		return false;

	if( i<0 )
		return false;
	m_fun = fun_t(i);

	if( cfg->Attribute("address",&i)==NULL )
		return false;

	if( plc )
		--i;

	if( i<0 )
		return false;
	m_addr = i;

	return true;
}

}
}

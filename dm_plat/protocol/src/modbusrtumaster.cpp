﻿/*
 * modbusrtumaster.cpp
 *
 *  Created on: 2017年3月14日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_PROTOCOL DM_API_EXPORT

#include <dm/protocol/modbusrtumaster.hpp>
#include <dm/os/log/logger.hpp>

namespace dm{
namespace protocol{

static const char* logModule = "CModbusRtuMaster.protocol.dm";

CModbusRtuMaster::CModbusRtuMaster():CModbusRtu(),CModbusMaster(){
	setWaitTimeout(5);
	setWaitMaxTimes(3);
	m_rxFrame.setUpFrame();
	m_txFrame.setDnFrame();

	log().debug(THISMODULE "创建对象,默认等待响应时间%d,重试次数%d",m_waitTimeout,m_timeoutMaxTimes);
}

CModbusRtuMaster::~CModbusRtuMaster(){
	log().debug(THISMODULE "销毁对象");
}

}
}

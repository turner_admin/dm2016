﻿/*
 * protocol.cpp
 *
 *  Created on: 2017年2月27日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_PROTOCOL DM_API_EXPORT

#include <dm/protocol/protocol.hpp>
#include <dm/os/log/logger.hpp>

namespace dm{
namespace protocol{

static const char* logModule = "CProtocol.protocol.dm";

CProtocol::CProtocol():CProtocolBase(),
		m_taskCall(None),m_taskClock(None),m_taskRmtCtl(None),m_taskParas(None),m_taskSync(None),m_taskReset(None),
		m_callGroup(0),m_rmtctl(dm::msg::Rc_None),m_rmtctlIndex(0),
		m_paraSet(false),m_syncType(dm::msg::SDT_None),
		m_timeout_call(10),m_timeout_clock(10),m_timeout_rmtclt(5),m_timeout_paras(5),m_timeout_sync(10),m_timeout_reset(10),
		m_ackCall(false),m_ackClock(false),m_ackRmtCtl(false),m_ackParas(false),m_ackSync(false),m_ackReset(false){
	log().debug(THISMODULE "创建对象");
}

CProtocol::~CProtocol(){
	log().debug(THISMODULE "销毁对象");
}

/**
 * 时钟刷新任务。
 * 实现了任务的状态流转。 一般在子类中不需要重载该函数
 * @param txFrame
 * @return
 */
CProtocol::EAction CProtocol::timedRefresh( const ts_t& ts,const rts_t& rts ){
	EAction action = refresh_msg(ts,rts);
	if( action!=Action_Nothing )
		return action;

	//! 先检测是否有新的任务被触发。
	checkNewTask(ts,rts);

	//! 优先执行复位任务。
	//! 如果复位任务不需要执行动作，则继续执行其他任务。否则函数返回。
	action = refresh_reset(ts,rts);
	if( action!=Action_Nothing )
		return action;

	//! 然后执行时钟同步任务。
	//! 如果不需要执行动作，则继续执行其他任务。否则函数返回。
	action = refresh_syncClock(ts,rts);
	if( action!=Action_Nothing )
		return action;

	//! 然后执行远控任务。
	//! 如果不需要执行动作，则继续执行其他任务。否则函数返回。
	action = refresh_remoteCtl(ts,rts);
	if( action!=Action_Nothing )
		return action;

	//! 然后执行参数任务。
	//! 如果不需要执行动作，则继续执行其他任务。否则函数返回。
	action = refresh_paras(ts,rts);
	if( action!=Action_Nothing )
		return action;

	//! 然后执行召唤任务。
	//! 如果不需要执行动作，则继续执行其他任务。否则函数返回。
	action = refresh_call(ts,rts);
	if( action!=Action_Nothing )
		return action;

	//! 然后执行数据同步任务。
	//! 如果不需要执行动作，则继续执行其他任务。否则函数返回。
	action = refresh_syncData(ts,rts);
	if( action!=Action_Nothing )
		return action;

	//! 如所有任务都不需要执行动作，则执行空闲任务
	return task_none(ts,rts);
}

/**
 * 复位规约
 * 子类中有自身的参数需要复位时，重载该函数，并调用基类函数。
 */
void CProtocol::reset( const ts_t& ts,const rts_t& rts ){
	log().debug(THISMODULE "复位");
	CProtocolBase::reset(ts,rts);
	m_taskCall = None;
	m_taskClock = None;
	m_taskRmtCtl = None;
	m_taskParas = None;
	m_taskSync = None;
	m_taskReset = None;
	noWait();
}

/**
 * 设置任务标识。
 * 通常由启动任务的函数调用来触发。
 * 一般子类不需要重载该函数。
 * @param group
 * @return
 */
bool CProtocol::setTask_call( const ts_t& /*ts*/,const rts_t& /*rts*/,const group_t& group,const sec_t& timeout ){
	if( m_taskCall!=None ){
		log().warnning(THISMODULE "当前有召唤任务");
		return false;
	}

	m_callGroup = group;
	if( timeout!=0 ){
		log().debug(THISMODULE "设置超时时长%d秒",timeout);
		m_timeout_call = timeout;
	}

	m_taskCall = Settled;

	return true;
}

bool CProtocol::setTask_syncClock( const ts_t& /*ts*/,const rts_t& /*rts*/,const sec_t& timeout ){
	if( m_taskClock!=None ){
		log().warnning(THISMODULE "当前有时钟同步任务");
		return false;
	}

	if( timeout!=0 )
		m_timeout_clock = timeout;

	m_taskClock = Settled;

	return true;
}

bool CProtocol::setTask_remoteControl( const ts_t& /*ts*/,const rts_t& /*rts*/,const rmtctl_t& rc,const index_t& address,const sec_t& timeout ){
	if( m_taskRmtCtl!=None ){
		log().warnning(THISMODULE "当前有远控任务");
		return false;
	}

	m_rmtctl = rc;
	m_rmtctlIndex = address;

	if( timeout!=0 )
		m_timeout_rmtclt = timeout;

	m_taskRmtCtl = Settled;

	return true;
}

bool CProtocol::setTask_parasSetUint8( const ts_t& /*ts*/,const rts_t& /*rts*/,const index_t& addr,const index_t& size,const dm::uint8* data,const sec_t& timeout ){
	if( m_taskParas!=None ){
		log().warnning(THISMODULE "当前有参数任务");
		return false;
	}

	if( m_paraData.maxNum_uint8()<size ){
		log().info(THISMODULE "参数个数%d超出范围%d",size,m_paraData.maxNum_uint8());
		return false;
	}

	m_paraSet = true;
	m_paraData.index = addr;
	m_paraData.num = size;
	m_paraData.type = m_paraData.DtUint8;
	for( int i=0;i<size;++i )
		m_paraData.as_uint8()[i] = data[i];

	if( timeout!=0 )
		m_timeout_paras = timeout;

	m_taskParas = Settled;

	return true;
}

bool CProtocol::setTask_parasSetInt8( const ts_t& /*ts*/,const rts_t& /*rts*/,const index_t& addr,const index_t& size,const dm::int8* data,const sec_t& timeout ){
	if( m_taskParas!=None ){
		log().warnning(THISMODULE "当前有参数任务");
		return false;
	}

	if( m_paraData.maxNum_int8()<size ){
		log().info(THISMODULE "参数个数%d超出范围%d",size,m_paraData.maxNum_int8());
		return false;
	}

	m_paraSet = true;
	m_paraData.index = addr;
	m_paraData.num = size;
	m_paraData.type = m_paraData.DtInt8;
	for( int i=0;i<size;++i )
		m_paraData.as_int8()[i] = data[i];

	if( timeout!=0 )
		m_timeout_paras = timeout;

	m_taskParas = Settled;

	return true;
}

bool CProtocol::setTask_parasSetUint16( const ts_t& /*ts*/,const rts_t& /*rts*/,const index_t& addr,const index_t& size,const dm::uint16* data,const sec_t& timeout ){
	if( m_taskParas!=None ){
		log().warnning(THISMODULE "当前有参数任务");
		return false;
	}

	if( m_paraData.maxNum_uint16()<size ){
		log().info(THISMODULE "参数个数%d超出范围%d",size,m_paraData.maxNum_uint16());
		return false;
	}

	m_paraSet = true;
	m_paraData.index = addr;
	m_paraData.num = size;
	m_paraData.type = m_paraData.DtUint16;
	for( int i=0;i<size;++i )
		m_paraData.as_uint16()[i] = data[i];

	if( timeout!=0 )
		m_timeout_paras = timeout;

	m_taskParas = Settled;

	return true;
}

bool CProtocol::setTask_parasSetInt16( const ts_t& /*ts*/,const rts_t& /*rts*/,const index_t& addr,const index_t& size,const dm::int16* data,const sec_t& timeout ){
	if( m_taskParas!=None ){
		log().warnning(THISMODULE "当前有参数任务");
		return false;
	}

	if( m_paraData.maxNum_int16()<size ){
		log().info(THISMODULE "参数个数%d超出范围%d",size,m_paraData.maxNum_int16());
		return false;
	}

	m_paraSet = true;
	m_paraData.index = addr;
	m_paraData.num = size;
	m_paraData.type = m_paraData.DtInt16;
	for( int i=0;i<size;++i )
		m_paraData.as_int16()[i] = data[i];

	if( timeout!=0 )
		m_timeout_paras = timeout;

	m_taskParas = Settled;

	return true;
}

bool CProtocol::setTask_parasSetUint32( const ts_t& /*ts*/,const rts_t& /*rts*/,const index_t& addr,const index_t& size,const dm::uint32* data,const sec_t& timeout ){
	if( m_taskParas!=None ){
		log().warnning(THISMODULE "当前有参数任务");
		return false;
	}

	if( m_paraData.maxNum_uint32()<size ){
		log().info(THISMODULE "参数个数%d超出范围%d",size,m_paraData.maxNum_uint32());
		return false;
	}

	m_paraSet = true;
	m_paraData.index = addr;
	m_paraData.num = size;
	m_paraData.type = m_paraData.DtUint32;
	for( int i=0;i<size;++i )
		m_paraData.as_uint32()[i] = data[i];

	if( timeout!=0 )
		m_timeout_paras = timeout;

	m_taskParas = Settled;

	return true;
}

bool CProtocol::setTask_parasSetInt32( const ts_t& /*ts*/,const rts_t& /*rts*/,const index_t& addr,const index_t& size,const dm::int32* data,const sec_t& timeout ){
	if( m_taskParas!=None ){
		log().warnning(THISMODULE "当前有参数任务");
		return false;
	}

	if( m_paraData.maxNum_int32()<size ){
		log().info(THISMODULE "参数个数%d超出范围%d",size,m_paraData.maxNum_int32());
		return false;
	}

	m_paraSet = true;
	m_paraData.index = addr;
	m_paraData.num = size;
	m_paraData.type = m_paraData.DtInt32;
	for( int i=0;i<size;++i )
		m_paraData.as_int32()[i] = data[i];

	if( timeout!=0 )
		m_timeout_paras = timeout;

	m_taskParas = Settled;

	return true;
}

bool CProtocol::setTask_parasSetUint64( const ts_t& /*ts*/,const rts_t& /*rts*/,const index_t& addr,const index_t& size,const dm::uint64* data,const sec_t& timeout ){
	if( m_taskParas!=None ){
		log().warnning(THISMODULE "当前有参数任务");
		return false;
	}

	if( m_paraData.maxNum_uint64()<size ){
		log().info(THISMODULE "参数个数%d超出范围%d",size,m_paraData.maxNum_uint64());
		return false;
	}

	m_paraSet = true;
	m_paraData.index = addr;
	m_paraData.num = size;
	m_paraData.type = m_paraData.DtUint64;
	for( int i=0;i<size;++i )
		m_paraData.as_uint64()[i] = data[i];

	if( timeout!=0 )
		m_timeout_paras = timeout;

	m_taskParas = Settled;

	return true;
}

bool CProtocol::setTask_parasSetInt64( const ts_t& /*ts*/,const rts_t& /*rts*/,const index_t& addr,const index_t& size,const dm::int64* data,const sec_t& timeout ){
	if( m_taskParas!=None ){
		log().warnning(THISMODULE "当前有参数任务");
		return false;
	}

	if( m_paraData.maxNum_int64()<size ){
		log().info(THISMODULE "参数个数%d超出范围%d",size,m_paraData.maxNum_int64());
		return false;
	}

	m_paraSet = true;
	m_paraData.index = addr;
	m_paraData.num = size;
	m_paraData.type = m_paraData.DtInt64;
	for( int i=0;i<size;++i )
		m_paraData.as_int64()[i] = data[i];

	if( timeout!=0 )
		m_timeout_paras = timeout;

	m_taskParas = Settled;

	return true;
}

bool CProtocol::setTask_parasSetFloat32( const ts_t& /*ts*/,const rts_t& /*rts*/,const index_t& addr,const index_t& size,const dm::float32* data,const sec_t& timeout ){
	if( m_taskParas!=None ){
		log().warnning(THISMODULE "当前有参数任务");
		return false;
	}

	if( m_paraData.maxNum_float32()<size ){
		log().info(THISMODULE "参数个数%d超出范围%d",size,m_paraData.maxNum_float32());
		return false;
	}

	m_paraSet = true;
	m_paraData.index = addr;
	m_paraData.num = size;
	m_paraData.type = m_paraData.DtFloat32;
	for( int i=0;i<size;++i )
		m_paraData.as_float32()[i] = data[i];

	if( timeout!=0 )
		m_timeout_paras = timeout;

	m_taskParas = Settled;

	return true;
}

bool CProtocol::setTask_parasSetFloat64( const ts_t& /*ts*/,const rts_t& /*rts*/,const index_t& addr,const index_t& size,const dm::float64* data,const sec_t& timeout ){
	if( m_taskParas!=None ){
		log().warnning(THISMODULE "当前有参数任务");
		return false;
	}

	if( m_paraData.maxNum_float64()<size ){
		log().info(THISMODULE "参数个数%d超出范围%d",size,m_paraData.maxNum_float64());
		return false;
	}

	m_paraSet = true;
	m_paraData.index = addr;
	m_paraData.num = size;
	m_paraData.type = m_paraData.DtFloat64;
	for( int i=0;i<size;++i )
		m_paraData.as_float64()[i] = data[i];

	if( timeout!=0 )
		m_timeout_paras = timeout;

	m_taskParas = Settled;

	return true;
}

bool CProtocol::setTask_parasGet( const ts_t& /*ts*/,const rts_t& /*rts*/,const index_t& addr,const index_t& size,const sec_t& timeout ){
	if( m_taskParas!=None ){
		log().warnning(THISMODULE "当前有参数任务");
		return false;
	}

	m_paraSet = false;
	m_paraData.index = addr;
	m_paraData.num = size;

	if( timeout!=0 )
		m_timeout_paras = timeout;

	m_taskParas = Settled;

	return true;
}

bool CProtocol::setTask_syncData( const ts_t& /*ts*/,const rts_t& /*rts*/,const sync_t& type,const sec_t& timeout ){
	if( m_taskSync!=None ){
		log().warnning(THISMODULE "当前有同步任务");
		return false;
	}

	m_syncType = type;

	if( timeout!=0 )
		m_timeout_sync = timeout;

	m_taskSync = Settled;

	return true;
}

bool CProtocol::setTask_reset( const ts_t& /*ts*/,const rts_t& /*rts*/,const sec_t& timeout ){
	if( m_taskReset!=None ){
		log().warnning(THISMODULE "当前有复位任务");
		return false;
	}

	if( timeout!=0 )
		m_timeout_reset = timeout;

	m_taskReset = Settled;

	return true;
}

/**
 * 设置任务结束
 * 通常由任务执行的函数调用来触发。
 * 一般子类不需要重载该函数。
 * @param result
 */
bool CProtocol::endTask_call( const ts_t& /*ts*/,const rts_t& /*rts*/,const ETaskState& result ){
	if( m_taskCall==Started && isEndState(result) ){
		m_taskCall = result;
		m_tsCall = m_tsCall.cur();
		return true;
	}else
		return false;
}

bool CProtocol::endTask_syncClock( const ts_t& /*ts*/,const rts_t& /*rts*/,const ETaskState& result ){
	if( m_taskClock==Started && isEndState(result) ){
		m_taskClock = result;
		m_tsClock = m_tsClock.cur();
		return true;
	}else
		return false;
}

bool CProtocol::endTask_remoteControl( const ts_t& /*ts*/,const rts_t& /*rts*/,const ETaskState& result ){
	if( m_taskRmtCtl==Started && isEndState(result) ){
		m_taskRmtCtl = result;
		m_tsRmtCtl = m_tsRmtCtl.cur();
		return true;
	}else
		return false;
}

bool CProtocol::endTask_paraSet( const ts_t& /*ts*/,const rts_t& /*rts*/,const ETaskState& result ){
	if( m_taskParas==Started && isEndState(result) && m_paraSet ){
		m_taskParas = result;
		m_tsParas = m_tsParas.cur();
		return true;
	}else
		return false;
}

bool CProtocol::endTask_paraGetUint8( const ts_t& /*ts*/,const rts_t& /*rts*/,const ETaskState& result,const index_t& size,const dm::uint8* data ){
	if( m_taskParas==Started && isEndState(result) && m_paraSet==false ){
		if( m_paraData.maxNum_uint8()<size ){
			log().info(THISMODULE "参数个数%d超出范围%d",size,m_paraData.maxNum_uint8());
			return false;
		}

		m_paraData.num = size;
		m_paraData.type = m_paraData.DtUint8;
		for( int i=0;i<size;++i )
			m_paraData.as_uint8()[i] = data[i];

		m_tsParas = m_tsParas.cur();
		m_taskParas = result;
		return true;
	}else
		return false;
}

bool CProtocol::endTask_paraGetInt8( const ts_t& /*ts*/,const rts_t& /*rts*/,const ETaskState& result,const index_t& size,const dm::int8* data ){
	if( m_taskParas==Started && isEndState(result) && m_paraSet==false ){
		if( m_paraData.maxNum_int8()<size ){
			log().info(THISMODULE "参数个数%d超出范围%d",size,m_paraData.maxNum_uint8());
			return false;
		}

		m_paraData.num = size;
		m_paraData.type = m_paraData.DtInt8;
		for( int i=0;i<size;++i )
			m_paraData.as_int8()[i] = data[i];

		m_tsParas = m_tsParas.cur();
		m_taskParas = result;
		return true;
	}else
		return false;
}

bool CProtocol::endTask_paraGetUint16( const ts_t& /*ts*/,const rts_t& /*rts*/,const ETaskState& result,const index_t& size,const dm::uint16* data ){
	if( m_taskParas==Started && isEndState(result) && m_paraSet==false ){
		if( m_paraData.maxNum_uint16()<size ){
			log().info(THISMODULE "参数个数%d超出范围%d",size,m_paraData.maxNum_uint16());
			return false;
		}

		m_paraData.num = size;
		m_paraData.type = m_paraData.DtUint16;
		for( int i=0;i<size;++i )
			m_paraData.as_uint16()[i] = data[i];

		m_tsParas = m_tsParas.cur();
		m_taskParas = result;
		return true;
	}else
		return false;
}

bool CProtocol::endTask_paraGetInt16( const ts_t& /*ts*/,const rts_t& /*rts*/,const ETaskState& result,const index_t& size,const dm::int16* data ){
	if( m_taskParas==Started && isEndState(result) && m_paraSet==false ){
		if( m_paraData.maxNum_int16()<size ){
			log().info(THISMODULE "参数个数%d超出范围%d",size,m_paraData.maxNum_int16());
			return false;
		}

		m_paraData.num = size;
		m_paraData.type = m_paraData.DtInt16;
		for( int i=0;i<size;++i )
			m_paraData.as_int16()[i] = data[i];

		m_tsParas = m_tsParas.cur();
		m_taskParas = result;
		return true;
	}else
		return false;
}

bool CProtocol::endTask_paraGetUint32( const ts_t& /*ts*/,const rts_t& /*rts*/,const ETaskState& result,const index_t& size,const dm::uint32* data ){
	if( m_taskParas==Started && isEndState(result) && m_paraSet==false ){
		if( m_paraData.maxNum_uint32()<size ){
			log().info(THISMODULE "参数个数%d超出范围%d",size,m_paraData.maxNum_uint32());
			return false;
		}

		m_paraData.num = size;
		m_paraData.type = m_paraData.DtUint32;
		for( int i=0;i<size;++i )
			m_paraData.as_uint32()[i] = data[i];

		m_tsParas = m_tsParas.cur();
		m_taskParas = result;
		return true;
	}else
		return false;
}

bool CProtocol::endTask_paraGetInt32( const ts_t& /*ts*/,const rts_t& /*rts*/,const ETaskState& result,const index_t& size,const dm::int32* data ){
	if( m_taskParas==Started && isEndState(result) && m_paraSet==false ){
		if( m_paraData.maxNum_int32()<size ){
			log().info(THISMODULE "参数个数%d超出范围%d",size,m_paraData.maxNum_int32());
			return false;
		}

		m_paraData.num = size;
		m_paraData.type = m_paraData.DtInt32;
		for( int i=0;i<size;++i )
			m_paraData.as_int32()[i] = data[i];

		m_tsParas = m_tsParas.cur();
		m_taskParas = result;
		return true;
	}else
		return false;
}

bool CProtocol::endTask_paraGetUint64( const ts_t& /*ts*/,const rts_t& /*rts*/,const ETaskState& result,const index_t& size,const dm::uint64* data ){
	if( m_taskParas==Started && isEndState(result) && m_paraSet==false ){
		if( m_paraData.maxNum_uint64()<size ){
			log().info(THISMODULE "参数个数%d超出范围%d",size,m_paraData.maxNum_uint64());
			return false;
		}

		m_paraData.num = size;
		m_paraData.type = m_paraData.DtUint64;
		for( int i=0;i<size;++i )
			m_paraData.as_uint64()[i] = data[i];

		m_tsParas = m_tsParas.cur();
		m_taskParas = result;
		return true;
	}else
		return false;
}

bool CProtocol::endTask_paraGetInt64( const ts_t& /*ts*/,const rts_t& /*rts*/,const ETaskState& result,const index_t& size,const dm::int64* data ){
	if( m_taskParas==Started && isEndState(result) && m_paraSet==false ){
		if( m_paraData.maxNum_int64()<size ){
			log().info(THISMODULE "参数个数%d超出范围%d",size,m_paraData.maxNum_int64());
			return false;
		}

		m_paraData.num = size;
		m_paraData.type = m_paraData.DtInt64;
		for( int i=0;i<size;++i )
			m_paraData.as_int64()[i] = data[i];

		m_tsParas = m_tsParas.cur();
		m_taskParas = result;
		return true;
	}else
		return false;
}

bool CProtocol::endTask_paraGetFloat32( const ts_t& /*ts*/,const rts_t& /*rts*/,const ETaskState& result,const index_t& size,const dm::float32* data ){
	if( m_taskParas==Started && isEndState(result) && m_paraSet==false ){
		if( m_paraData.maxNum_float32()<size ){
			log().info(THISMODULE "参数个数%d超出范围%d",size,m_paraData.maxNum_float32());
			return false;
		}

		m_paraData.num = size;
		m_paraData.type = m_paraData.DtFloat32;
		for( int i=0;i<size;++i )
			m_paraData.as_float32()[i] = data[i];

		m_tsParas = m_tsParas.cur();
		m_taskParas = result;
		return true;
	}else
		return false;
}

bool CProtocol::endTask_paraGetFloat64( const ts_t& /*ts*/,const rts_t& /*rts*/,const ETaskState& result,const index_t& size,const dm::float64* data ){
	if( m_taskParas==Started && isEndState(result) && m_paraSet==false ){
		if( m_paraData.maxNum_float64()<size ){
			log().info(THISMODULE "参数个数%d超出范围%d",size,m_paraData.maxNum_float64());
			return false;
		}

		m_paraData.num = size;
		m_paraData.type = m_paraData.DtFloat64;
		for( int i=0;i<size;++i )
			m_paraData.as_float64()[i] = data[i];

		m_tsParas = m_tsParas.cur();
		m_taskParas = result;
		return true;
	}else
		return false;
}

bool CProtocol::endTask_syncData( const ts_t& /*ts*/,const rts_t& /*rts*/,const ETaskState& result ){
	if( m_taskSync==Started && isEndState(result) ){
		m_tsSync = m_tsSync.cur();
		m_taskSync = result;
		return true;
	}else
		return false;
}

bool CProtocol::endTask_reset( const ts_t& /*ts*/,const rts_t& /*rts*/,const ETaskState& result ){
	if( m_taskReset==Started && isEndState(result) ){
		m_tsReset = m_tsReset.cur();
		m_taskReset = result;
		return true;
	}else
		return false;
}

/**
 * 消息检测接口
 * @param txFrame
 * @return
 */
CProtocol::EAction CProtocol::refresh_msg( const ts_t& /*ts*/,const rts_t& /*rts*/ ){
	return Action_Nothing;
}

/**
 * 刷新召唤任务状态机
 * 如果任务启动后，长时间没有执行完成，将会自动超时。
 * @param txFrame
 * @return
 */
CProtocol::EAction CProtocol::refresh_call( const ts_t& ts,const rts_t& rts ){
	EAction action = Action_Nothing;

	switch( m_taskCall ){
	case None:
		break;
	case Settled:
		action = taskStart_call(ts,rts);
		if( m_taskCall==Started ){
			m_tsCall = rts;
		}
		break;
	case Started:
		action = taskDo_call(ts,rts);
		if( m_taskCall==Started && m_timeout_call>0){
			if( m_tsCall.isTimeout_sec(m_timeout_call,rts) ){
				log().warnning(THISMODULE "召唤任务超时");
				m_taskCall = Timeout;
			}
		}
		break;
	case Success:
	case Fail:
	case Permit:
	case Timeout:
		action = taskEnd_call(ts,rts);
		break;
	default:
		log().warnning(THISMODULE "召唤任务状态异常:%d",m_taskCall);
		m_taskCall = None;
	}

	return action;
}

/**
 * 刷新时钟同步状态机
 * 如果任务启动后，长时间没有执行完成，将会自动超时。
 * @param txFrame
 * @return
 */
CProtocol::EAction CProtocol::refresh_syncClock( const ts_t& ts,const rts_t& rts ){
	EAction action = Action_Nothing;

	switch( m_taskClock ){
	case None:
		break;
	case Settled:
		action = taskStart_syncClock(ts,rts);
		if( m_taskClock==Started ){
			log().debug(THISMODULE "时钟同步任务启动");
			m_tsClock = rts;
		}
		break;
	case Started:
		action = taskDo_syncClock(ts,rts);
		if( m_taskClock==Started && m_timeout_clock>0){
			if( m_tsClock.isTimeout_sec(m_timeout_clock,rts) ){
				log().warnning(THISMODULE "时钟同步任务超时");
				m_taskClock = Timeout;
			}
		}
		break;
	case Success:
	case Fail:
	case Permit:
	case Timeout:
		action = taskEnd_syncClock(ts,rts);
		break;
	default:
		log().warnning(THISMODULE "时钟同步任务状态异常:%d",m_taskClock);
		m_taskClock = None;
	}

	return action;
}

/**
 * 刷新远控任务状态机
 * 如果任务启动后，长时间没有执行完成，将会自动超时。
 * @param txFrame
 * @return
 */
CProtocol::EAction CProtocol::refresh_remoteCtl( const ts_t& ts,const rts_t& rts ){
	EAction action = Action_Nothing;

	switch( m_taskRmtCtl ){
	case None:
		break;
	case Settled:
		action = taskStart_remoteControl(ts,rts);
		if( m_taskRmtCtl==Started ){
			log().debug(THISMODULE "远控任务启动");
			m_tsRmtCtl = rts;
		}
		break;
	case Started:
		action = taskDo_remoteControl(ts,rts);
		if( m_taskRmtCtl==Started && m_timeout_rmtclt>0){
			if( m_tsRmtCtl.isTimeout_sec(m_timeout_rmtclt,rts) ){
				log().warnning(THISMODULE "远控任务超时");
				m_taskRmtCtl = Timeout;
			}
		}
		break;
	case Success:
	case Fail:
	case Permit:
	case Timeout:
		action = taskEnd_remoteControl(ts,rts);
		break;
	default:
		log().warnning(THISMODULE "远控任务状态异常:%d",m_taskRmtCtl);
		m_taskRmtCtl = None;
	}

	return action;
}

/**
 * 刷新参数任务状态机
 * 如果任务启动后，长时间没有执行完成，将会自动超时。
 * @param txFrame
 * @return
 */
CProtocol::EAction CProtocol::refresh_paras( const ts_t& ts,const rts_t& rts ){
	EAction action = Action_Nothing;

	switch( m_taskParas ){
	case None:
		break;
	case Settled:
		action = taskStart_parameters(ts,rts);
		if( m_taskParas==Started ){
			log().debug(THISMODULE "参数任务启动");
			m_tsParas = rts;
		}
		break;
	case Started:
		action = taskDo_parameters(ts,rts);
		if( m_taskParas==Started && m_timeout_paras>0){
			if( m_tsParas.isTimeout_sec(m_timeout_paras,rts) ){
				log().warnning(THISMODULE "参数任务超时");
				m_taskParas = Timeout;
			}
		}
		break;
	case Success:
	case Fail:
	case Permit:
	case Timeout:
		action = taskEnd_parameters(ts,rts);
		break;
	default:
		log().warnning(THISMODULE "参数任务状态异常:%d",m_taskParas);
		m_taskParas = None;
	}

	return action;
}

/**
 * 刷新同步任务状态机
 * 如果任务启动后，长时间没有执行完成，将会自动超时。
 * @param txFrame
 * @return
 */
CProtocol::EAction CProtocol::refresh_syncData( const ts_t& ts,const rts_t& rts ){
	EAction action = Action_Nothing;

	switch( m_taskSync ){
	case None:
		break;
	case Settled:
		action = taskStart_syncData(ts,rts);
		if( m_taskSync==Started ){
			log().debug(THISMODULE "同步任务启动");
			m_tsSync = rts;
		}
		break;
	case Started:
		action = taskDo_syncData(ts,rts);
		if( m_taskSync==Started && m_timeout_sync>0){
			if( m_tsSync.isTimeout_sec(m_timeout_sync,rts) ){
				log().warnning(THISMODULE "同步任务超时");
				m_taskSync = Timeout;
			}
		}
		break;
	case Success:
	case Fail:
	case Timeout:
		action = taskEnd_syncData(ts,rts);
		break;
	default:
		log().warnning(THISMODULE "同步任务状态异常:%d",m_taskSync);
		m_taskSync = None;
	}

	return action;
}

/**
 * 刷新复位任务状态机
 * 如果任务启动后，长时间没有执行完成，将会自动超时。
 * @param txFrame
 * @return
 */
CProtocol::EAction CProtocol::refresh_reset( const ts_t& ts,const rts_t& rts ){
	EAction action = Action_Nothing;

	switch( m_taskReset ){
	case None:
		break;
	case Settled:
		action = taskStart_reset(ts,rts);
		if( m_taskReset==Started ){
			log().debug(THISMODULE "复位任务启动");
			m_tsReset = rts;
		}
		break;
	case Started:
		action = taskDo_reset(ts,rts);
		if( m_taskReset==Started && m_timeout_reset>0){
			if( m_tsReset.isTimeout_sec(m_timeout_reset,rts) ){
				log().warnning(THISMODULE "复位任务超时");
				m_taskReset = Timeout;
			}
		}
		break;
	case Success:
	case Fail:
	case Timeout:
		action = taskEnd_reset(ts,rts);
		break;
	default:
		log().warnning(THISMODULE "复位任务状态异常:%d",m_taskReset);
		m_taskReset = None;
	}

	return action;
}

/**
 * 检测新的任务。
 * 子类有任务抢占需求时，可以重载该函数。
 */
void CProtocol::checkNewTask( const ts_t& /*ts*/,const rts_t& /*rts*/ ){
}

/**
 * 空闲时处理
 * 对于需要自动触发任务的，可以重载该函数，添加自有的工作。
 * @param
 * @return
 */
CProtocol::EAction CProtocol::task_none( const ts_t& /*ts*/,const rts_t& /*rts*/ ){
	return Action_Nothing;
}

CProtocol::EAction CProtocol::taskStart_call( const ts_t& /*ts*/,const rts_t& /*rts*/ ){
	log().debug(THISMODULE "启动总召");
	m_taskCall = Started;
	return Action_Nothing;
}

CProtocol::EAction CProtocol::taskStart_syncClock( const ts_t& /*ts*/,const rts_t& /*rts*/ ){
	log().debug(THISMODULE "启动对时");
	m_taskClock = Started;
	return Action_Nothing;
}

CProtocol::EAction CProtocol::taskStart_remoteControl( const ts_t& /*ts*/,const rts_t& /*rts*/ ){
	log().debug(THISMODULE "启动远控");
	m_taskRmtCtl = Started;
	return Action_Nothing;
}

CProtocol::EAction CProtocol::taskStart_parameters( const ts_t& /*ts*/,const rts_t& /*rts*/ ){
	log().debug(THISMODULE "启动参数");
	m_taskParas = Started;
	return Action_Nothing;
}

CProtocol::EAction CProtocol::taskStart_syncData( const ts_t& /*ts*/,const rts_t& /*rts*/ ){
	log().debug(THISMODULE "启动同步数据");
	m_taskSync = Started;
	return Action_Nothing;
}

CProtocol::EAction CProtocol::taskStart_reset( const ts_t& /*ts*/,const rts_t& /*rts*/ ){
	log().debug(THISMODULE "启动复位");
	m_taskReset = Started;
	return Action_Nothing;
}

CProtocol::EAction CProtocol::taskDo_call( const ts_t& /*ts*/,const rts_t& /*rts*/ ){
	log().debug(THISMODULE "执行总召成功");
	m_taskCall = Success;
	return Action_Nothing;
}

CProtocol::EAction CProtocol::taskDo_syncClock( const ts_t& /*ts*/,const rts_t& /*rts*/ ){
	log().debug(THISMODULE "执行对时成功");
	m_taskClock = Success;
	return Action_Nothing;
}

CProtocol::EAction CProtocol::taskDo_remoteControl( const ts_t& /*ts*/,const rts_t& /*rts*/ ){
	log().debug(THISMODULE "执行远控成功");
	m_taskRmtCtl = Success;
	return Action_Nothing;
}

CProtocol::EAction CProtocol::taskDo_parameters( const ts_t& /*ts*/,const rts_t& /*rts*/ ){
	log().debug(THISMODULE "执行参数成功");
	m_taskParas = Success;
	return Action_Nothing;
}

CProtocol::EAction CProtocol::taskDo_syncData( const ts_t& /*ts*/,const rts_t& /*rts*/ ){
	log().debug(THISMODULE "执行同步数据成功");
	m_taskSync = Success;
	return Action_Nothing;
}

CProtocol::EAction CProtocol::taskDo_reset( const ts_t& /*ts*/,const rts_t& /*rts*/ ){
	log().debug(THISMODULE "启动复位成功");
	m_taskReset = Success;
	return Action_Nothing;
}

CProtocol::EAction CProtocol::taskEnd_call( const ts_t& /*ts*/,const rts_t& /*rts*/ ){
	log().debug(THISMODULE "结束总召");
	m_taskCall = None;
	return Action_Nothing;
}

CProtocol::EAction CProtocol::taskEnd_syncClock( const ts_t& /*ts*/,const rts_t& /*rts*/ ){
	log().debug(THISMODULE "结束对时");
	m_taskClock = None;
	return Action_Nothing;
}

CProtocol::EAction CProtocol::taskEnd_remoteControl( const ts_t& /*ts*/,const rts_t& /*rts*/ ){
	log().debug(THISMODULE "结束远控");
	m_taskRmtCtl = None;
	return Action_Nothing;
}

CProtocol::EAction CProtocol::taskEnd_parameters( const ts_t& /*ts*/,const rts_t& /*rts*/ ){
	log().debug(THISMODULE "结束参数");
	m_taskParas = None;
	return Action_Nothing;
}

CProtocol::EAction CProtocol::taskEnd_syncData( const ts_t& /*ts*/,const rts_t& /*rts*/ ){
	log().debug(THISMODULE "结束数据同步");
	m_taskSync = None;
	return Action_Nothing;
}

CProtocol::EAction CProtocol::taskEnd_reset( const ts_t& /*ts*/,const rts_t& /*rts*/ ){
	log().debug(THISMODULE "结束复位");
	m_taskReset = None;
	return Action_Nothing;
}

}
}



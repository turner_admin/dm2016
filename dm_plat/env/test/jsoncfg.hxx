﻿/*
 * jsoncfg.hxx
 *
 *  Created on: 2020年3月18日
 *      Author: Dylan.Gao
 */

#ifndef ENV_TEST_JSONCFG_HXX_
#define ENV_TEST_JSONCFG_HXX_

#include <cxxtest/TestSuite.h>
#include <dm/env/jsoncfg.hpp>

class TestJsonCfg:public CxxTest::TestSuite{
public:
	void testOperator(){
		dm::env::CJsonCfg cfg;
		Json::String errs;

		cfg["info"]="This is a test";

		TSM_ASSERT("导出到文件",cfg.dump("test_cfg.json"));
		TSM_ASSERT_EQUALS("导入不存在的文件",cfg.load("test1_cfg.json",errs),-1);
		TSM_ASSERT_EQUALS("导入文件",cfg.load("test_cfg.json",errs),0);
		TSM_ASSERT("内容",cfg["info"].asString()=="This is a test");
	}
};

#endif /* ENV_TEST_JSONCFG_HXX_ */

﻿/*
 * env.cpp
 *
 *  Created on: 2016-7-22
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_ENV DM_API_EXPORT

#include <dm/env/env.hpp>
#include <cstdlib>

namespace dm{
namespace env{
	using namespace std;

	string getDmDir(){
#ifdef WIN32
		char* p = NULL;
		size_t sz = 0;
		if (_dupenv_s(&p, &sz, "DM2016_RT") == 0 && p != NULL) {
			string r(p);
			free(p);
			return r;
		}
		else
			return ".";
#else
		const char* p = getenv("DM2016_RT");
		if( p==NULL )
			return ".";
		else
			return p;
#endif
	}

	string getCfgDir(){
		return getDmDir()+"/etc";
	}

	string getDataDir(){
		return getDmDir()+"/var/data";
	}

	string getLogDir(){
		return getDmDir()+"/var/log";
	}

	string getSvgDir(){
		return getDmDir()+"/svgs";
	}
}
}



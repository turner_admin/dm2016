/*
 * license.cpp
 *
 *  Created on: 2017年6月5日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_ENV DM_API_EXPORT

#include <dm/env/license.hpp>
#include <tinyxml.h>
#include <dm/env/env.hpp>
#include <dm/utility/md5.hpp>
#include <fstream>

#ifdef _ARM_
#define LICENSE_NO_CPUID 1
#endif

#ifndef LICENSE_NO_CPUID

#ifdef WIN32
#include <intrin.h>
#else
#include <cpuid.h>
#endif

static void getcpuid(unsigned int CPUInfo[4], unsigned int InfoType)
{
#ifdef WIN32
	__cpuid((int*)(void*)CPUInfo, (int)(InfoType));
#else
    __cpuid(InfoType, CPUInfo[0],CPUInfo[1],CPUInfo[2],CPUInfo[3]);
#endif
}

static std::string getId(){
	unsigned int buf[4];
	getcpuid(buf,0);

	char str[128];
#ifdef WIN32
	sprintf_s<128>(str, "%x,%x,%x,%x", buf[0], buf[1], buf[2], buf[3]);
#else
	sprintf(str,"%x,%x,%x,%x",buf[0],buf[1],buf[2],buf[3]);
#endif
	return std::string(str);
}

#endif

namespace dm{
namespace env{

CLicense::CLicense():m_project(),m_client(),m_manufacturer(),
		m_date(),m_sn(){
	TiXmlDocument doc(getCfgDir()+"/license.xml");
	if( doc.LoadFile() ){
		TiXmlElement* dm = doc.FirstChildElement("dm");
		if( dm ){
		    const char* li;

		    if( (li = dm->Attribute("project")) )
		    	m_project = li;

		    if( (li = dm->Attribute("client")) )
		    	m_client = li;

		    if( (li = dm->Attribute("manufacturer")) )
		    	m_manufacturer = li;

		    if( (li = dm->Attribute("date")) )
				m_date = li;

		    if( (li = dm->Attribute("license")) )
				m_sn = li;
	    }
	}
}

CLicense::~CLicense(){
}

bool CLicense::isLicensed()const{
	dm::utility::CMd5 md5;

	std::string raw = getMachineId();
	if( !m_project.empty() )
		raw += m_project;

	if( !m_client.empty() )
		raw += m_client;

	if( !m_manufacturer.empty() )
		raw += m_manufacturer;

	if( !m_date.empty() )
		raw += m_date;

	md5.update("dm"+raw+"2016V2.0");

	return md5.toString()==m_sn;
}

std::string CLicense::getMachineId()const{
	dm::utility::CMd5 md5;

	md5.update("matchine");

	std::ifstream istr;

// 云涌科技
	istr.open("/sys/fsl_otp/HW_OCOTP_CFG0",std::ifstream::in);
	if( istr.is_open() )
		md5.update(istr);

	istr.open("/sys/fsl_otp/HW_OCOTP_CFG1",std::ifstream::in);
	if( istr.is_open() )
		md5.update(istr);

	istr.open("/sys/fsl_otp/HW_OCOTP_CFG2",std::ifstream::in);
	if( istr.is_open() )
		md5.update(istr);

	istr.open("/sys/fsl_otp/HW_OCOTP_CFG3",std::ifstream::in);
	if( istr.is_open() )
		md5.update(istr);

	istr.open("/sys/fsl_otp/HW_OCOTP_CFG4",std::ifstream::in);
	if( istr.is_open() )
		md5.update(istr);

	istr.open("/sys/fsl_otp/HW_OCOTP_CFG5",std::ifstream::in);
	if( istr.is_open() )
		md5.update(istr);

	istr.open("/sys/fsl_otp/HW_OCOTP_CFG6",std::ifstream::in);
	if( istr.is_open() )
		md5.update(istr);

#ifndef LICENSE_NO_CPUID
	md5.update(getId());
#endif

#ifndef LICENSE_NO_NETADDR
#ifndef LICENSE_NETADDR_LEN
#define LICENSE_NETADDR_LEN 1024
#endif
	char buf[LICENSE_NETADDR_LEN]={'0'};
	for( int i=0;i<5;++i ){
#ifdef WIN32
		sprintf_s<LICENSE_NETADDR_LEN>(buf, "/sys/class/net/eth%d/address", i);
#else
		sprintf(buf,"/sys/class/net/eth%d/address",i);
#endif
		istr.open(buf,std::ifstream::in);
		if( istr.is_open() )
			md5.update(istr);
	}
#endif

	return md5.toString();
}

}
}



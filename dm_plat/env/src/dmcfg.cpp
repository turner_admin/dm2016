﻿/*
 * dmcfg.cpp
 *
 *  Created on: 2016-8-1
 *      Author: Dylan
 */

#include <dm/export.hpp>

#define DM_API_ENV DM_API_EXPORT

#include <dm/env/env.hpp>
#include <boost/interprocess/managed_mapped_file.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <dm/env/dmcfgreader.hpp>
#include <dm/env/dmcfg.hpp>
#include <fstream>
#include <iostream>

namespace dm{
namespace env{

using namespace boost::interprocess;

struct SDmCfg{
	SDmCfg(){
		// scada
		scada_sstatus_size = 64;
		scada_dstatus_size = 64;
		scada_samples_size = 64;
		scada_reals_size = 64;
		scada_measures_size = 64;

		scada_timequeue_sstatus_size = 1024;
		scada_timequeue_dstatus_size = 1024;
		scada_timequeue_samples_size = 1024;
		scada_timequeue_reals_size = 1024;
		scada_timequeue_measures_size = 1024;
	}

	// scada
	unsigned int scada_sstatus_size;
	unsigned int scada_dstatus_size;
	unsigned int scada_samples_size;
	unsigned int scada_reals_size;
	unsigned int scada_measures_size;

	unsigned int scada_timequeue_sstatus_size;
	unsigned int scada_timequeue_dstatus_size;
	unsigned int scada_timequeue_samples_size;
	unsigned int scada_timequeue_reals_size;
	unsigned int scada_timequeue_measures_size;
};

static SDmCfg* DmCfg = 0;

CDmCfgReader::CDmCfgReader(){
	static managed_mapped_file managed_fmap(open_or_create,getCfgFilePath("dm.cfg").c_str(),sizeof(SDmCfg)+256);
	DmCfg = managed_fmap.find_or_construct<SDmCfg>(unique_instance)();
}

CDmCfgReader& CDmCfgReader::ins(){
	static CDmCfgReader i;
	return i;
}

const unsigned int& CDmCfgReader::getScada_sStatusSize()const{
	return DmCfg->scada_sstatus_size;
}

const unsigned int& CDmCfgReader::getScada_dStatusSize()const{
	return DmCfg->scada_dstatus_size;
}

const unsigned int& CDmCfgReader::getScada_samplesSize()const{
	return DmCfg->scada_samples_size;
}

const unsigned int& CDmCfgReader::getScada_realsSize()const{
	return DmCfg->scada_reals_size;
}

const unsigned int& CDmCfgReader::getScada_measuresSize()const{
	return DmCfg->scada_measures_size;
}

const unsigned int& CDmCfgReader::getScada_timeQueue_sStatusSize()const{
	return DmCfg->scada_timequeue_sstatus_size;
}

const unsigned int& CDmCfgReader::getScada_timeQueue_dStatusSize()const{
	return DmCfg->scada_timequeue_dstatus_size;
}

const unsigned int& CDmCfgReader::getScada_timeQueue_samplesSize()const{
	return DmCfg->scada_timequeue_samples_size;
}

const unsigned int& CDmCfgReader::getScada_timeQueue_realsSize()const{
	return DmCfg->scada_timequeue_reals_size;
}

const unsigned int& CDmCfgReader::getScada_timeQueue_measuresSize()const{
	return DmCfg->scada_timequeue_measures_size;
}

///////////////////////////////////////////////

// scada
void setScada_sStatusSize( unsigned int n ){
	DmCfg->scada_sstatus_size = n;
}

void setScada_dStatusSize( unsigned int n ){
	DmCfg->scada_dstatus_size = n;
}

void setScada_samplesSize( unsigned int n ){
	DmCfg->scada_samples_size = n;
}

void setScada_realsSize( unsigned int n ){
	DmCfg->scada_reals_size = n;
}

void setScada_measuresSize( unsigned int n ){
	DmCfg->scada_measures_size = n;
}

void setScada_timeQueue_sStatusSize( unsigned int n ){
	DmCfg->scada_timequeue_sstatus_size = n;
}

void setScada_timeQueue_dStatusSize( unsigned int n ){
	DmCfg->scada_timequeue_dstatus_size = n;
}

void setScada_timeQueue_samplesSize( unsigned int n ){
	DmCfg->scada_timequeue_samples_size = n;
}

void setScada_timeQueue_realsSize( unsigned int n ){
	DmCfg->scada_timequeue_reals_size = n;
}

void setScada_timeQueue_measuresSize( unsigned int n ){
	DmCfg->scada_timequeue_measures_size = n;
}
}
}



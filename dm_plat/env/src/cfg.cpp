﻿/*
 * cfg.cpp
 *
 *  Created on: 2017年12月2日
 *      Author: work
 */

#include <dm/export.hpp>

#define DM_API_ENV DM_API_EXPORT

#include <dm/env/cfg.hpp>

#include <tinyxml.h>
#include <dm/env/env.hpp>
#include <dm/utility/md5.hpp>
#include <fstream>

namespace dm{
namespace env{

CCfg::CCfg(){
}

CCfg& CCfg::ins(){
	static CCfg i;
	return i;
}

int CCfg::log_size()const{
	static const int defaultSize = 1024;

	TiXmlDocument doc(getCfgDir()+"/cfg.xml");
	if( !doc.LoadFile() ){
		return defaultSize;
	}

	TiXmlElement* log = doc.FirstChildElement("log");
	if( log==NULL ){
		return defaultSize;
    }

   const char* element;
   element = log->Attribute("size");
	if( element==NULL ){
		return defaultSize;
    }

	return atoi(element);
}

int CCfg::task_size()const{
	static const int defaultSize = 6;

	TiXmlDocument doc(getCfgDir()+"/cfg.xml");
	if( !doc.LoadFile() ){
		return defaultSize;
	}

	TiXmlElement* task = doc.FirstChildElement("task");
	if( task==NULL ){
		return defaultSize;
    }

   const char* element;
   element = task->Attribute("size");
	if( element==NULL ){
		return defaultSize;
    }

	return atoi(element);
}

static int scada_element( const char* ce,const int& defaultValue ){
	TiXmlDocument doc(getCfgDir()+"/cfg.xml");
	if( !doc.LoadFile() ){
		return defaultValue;
	}

	TiXmlElement* scada = doc.FirstChildElement("scada");
	if( scada==NULL ){
		return defaultValue;
    }

   const char* element;
   element = scada->Attribute(ce);
	if( element==NULL ){
		return defaultValue;
    }

	return atoi(element);
}

int CCfg::scada_allocate()const{
	return scada_element("allocate",1024);
}

int CCfg::scada_eventQueueSize()const{
	return scada_element("event-queue",1024);
}

int CCfg::scada_measureQueueSize()const{
	return scada_element("measure-queue",1024);
}

int CCfg::scada_cumulantQueueSize()const{
	return scada_element("cumulant-queue",1024);
}

static const char* scada_element( const char* ce,const char* defaultValue ){
	TiXmlDocument doc(getCfgDir()+"/cfg.xml");
	if( !doc.LoadFile() ){
		return defaultValue;
	}

	TiXmlElement* scada = doc.FirstChildElement("scada");
	if( scada==NULL ){
		return defaultValue;
    }

   const char* element;
   element = scada->Attribute(ce);
	if( element==NULL ){
		return defaultValue;
    }

	return element;
}

std::string CCfg::scada_dbEngine()const{
	return scada_element("db-engine","sqlite3");
}

std::string CCfg::scada_dbHost()const{
	return scada_element("db-host","localhost");
}

short CCfg::scada_dbPort()const{
	return scada_element("db-port",-1);
}

std::string CCfg::scada_dbName()const{
	return scada_element("db-name","dm_scada");
}

std::string CCfg::scada_dbUsr()const{
	return scada_element("db-usr","dm-scada");
}

std::string CCfg::scada_dbPwd()const{
	return scada_element("db-pwd","dm");
}

std::string CCfg::his_dbEngine()const{
	return scada_element("his-engine","sqlite3");
}

std::string CCfg::his_dbHost()const{
	return scada_element("his-host","localhost");
}

short CCfg::his_dbPort()const{
	return scada_element("his-port",-1);
}

std::string CCfg::his_dbName()const{
	return scada_element("his-name","dm_his");
}

std::string CCfg::his_dbUsr()const{
	return scada_element("his-usr","dm-his");
}

std::string CCfg::his_dbPwd()const{
	return scada_element("his-pwd","dm");
}

std::string CCfg::sys_dbEngine()const{
	return scada_element("sys-engine","sqlite3");
}

std::string CCfg::sys_dbHost()const{
	return scada_element("sys-host","localhost");
}

short CCfg::sys_dbPort()const{
	return scada_element("sys-port",-1);
}

std::string CCfg::sys_dbName()const{
	return scada_element("sys-name","dm_sys");
}

std::string CCfg::sys_dbUsr()const{
	return scada_element("sys-usr","dm-sys");
}

std::string CCfg::sys_dbPwd()const{
	return scada_element("sys-pwd","dm");
}

std::string CCfg::option_dbEngine()const{
	return scada_element("option-engine","sqlite3");
}

std::string CCfg::option_dbHost()const{
	return scada_element("option-host","localhost");
}

short CCfg::option_dbPort()const{
	return scada_element("option-port",-1);
}

std::string CCfg::option_dbName()const{
	return scada_element("option-name","dm_options");
}

std::string CCfg::option_dbUsr()const{
	return scada_element("option-usr","dm-options");
}

std::string CCfg::option_dbPwd()const{
	return scada_element("option-pwd","dm");
}

std::string CCfg::user_dbEngine()const{
	return scada_element("user-engine","sqlite3");
}

std::string CCfg::user_dbHost()const{
	return scada_element("user-host","localhost");
}

short CCfg::user_dbPort()const{
	return scada_element("user-port",-1);
}

std::string CCfg::user_dbName()const{
	return scada_element("user-name","dm_user");
}

std::string CCfg::user_dbUsr()const{
	return scada_element("user-usr","dm-user");
}

std::string CCfg::user_dbPwd()const{
	return scada_element("user-pwd","dm");
}

std::string CCfg::ui_dbEngine()const{
	return scada_element("ui-engine","sqlite3");
}

std::string CCfg::ui_dbHost()const{
	return scada_element("ui-host","localhost");
}

short CCfg::ui_dbPort()const{
	return scada_element("ui-port",-1);
}

std::string CCfg::ui_dbName()const{
	return scada_element("ui-name","dm_ui");
}

std::string CCfg::ui_dbUsr()const{
	return scada_element("ui-usr","dm-ui");
}

std::string CCfg::ui_dbPwd()const{
	return scada_element("ui-pwd","dm");
}

}
}

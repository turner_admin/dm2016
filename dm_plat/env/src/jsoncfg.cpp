﻿/*
 * jsoncfg.cpp
 *
 *  Created on: 2020年3月17日
 *      Author: Dylan.Gao
 */

#include <dm/export.hpp>

#define DM_API_ENV DM_API_EXPORT

#include <dm/env/jsoncfg.hpp>
#include <fstream>
#include <dm/env/env.hpp>

namespace dm{
namespace env{

CJsonCfg::CJsonCfg():Json::Value(){
}

/**
 * 从文件加载配置项
 * @param file
 * @return 执行结果
 * - 0 成功
 * - -1 打开文件失败
 * - -2 解析文件失败
 */
int CJsonCfg::load( const char* file,Json::String& errs ){
	std::ifstream istr;
	istr.open(dm::env::getCfgFilePath(file).c_str(),std::ifstream::in);
	if( !istr.is_open() )
		return -1;

	Json::CharReaderBuilder builder;
	if( Json::parseFromStream(builder,istr,this,&errs) )
		return 0;

	return -2;
}

/**
 * 导出配置到文件
 * @param file
 * @return
 */
bool CJsonCfg::dump( const char* file )const{
	std::ofstream ostr;
	ostr.open( dm::env::getCfgFilePath(file).c_str(),std::ofstream::trunc);
	if( !ostr.is_open() )
		return false;
	Json::StreamWriterBuilder builder;
	std::unique_ptr<Json::StreamWriter> writer(builder.newStreamWriter());
	return 0==writer->write(*this,&ostr);
}

}
}

# 快速使用

> 讨论沟通：钉钉群号：44720487

## 获取开发环境
开发环境可以直接从docker获取镜像

```
docker run -it -p 3389:3389 --name dm2016-dev -v /mnt/hgfs/git/dm2016/dm_runtime:/home/dm/dm_runtime -v /mnt/hgfs/git/:/home/dm/source dylangao22/dm2016-dev:v1.1 /bin/bash
```

进入容器执行命令

容器也可以自行构建。目前支持qt界面开发
```
docker build -t dylangao22/dm2016-dev:v1.1 .
```

## 编译平台
```
mkdir -p /home/dm/build-plat
cd /home/dm/build-plat
cmake /home/dm/source/dm_plat
make
```

## 编译工具
```
mkdir -p /home/dm/build-tools
cd /home/dm/build-tools
cmake /home/dm/source/dm_tools
make
```

## 使用windows远程桌面连接容器
首先需要设置dm用户账号
然后使用远程桌面连接
登录成功后，在菜单 application》setting》preferred Application中设置终端模拟器为xfce模拟器

# 目录结构
## dm_runtime

